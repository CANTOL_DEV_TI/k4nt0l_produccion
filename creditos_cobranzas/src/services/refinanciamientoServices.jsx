import {Url} from "../constants/global"; 
 
const meServidorBackend = Url; 
 
const url = `${meServidorBackend}`; 
 
const cabecera = {'Content-type': 'application/json; charset=UTF-8'} 
 
export async function GetConsulta(pFiltros) 
{ 
    const requestOptions = { 
        method: 'POST', 
        headers: cabecera ,
        body : JSON.stringify(pFiltros)
    } 
    const response = await fetch(`${url}/creditoscobranzas/renovaciones/listado/`,requestOptions) 
    const responseJson = await response.json()     
    return responseJson 
} 

export async function getClientes(pCia,pRazonSocial)
{
    console.log(url)
    const requestOptions = {
        method : 'GET',
        headers : cabecera
    }

    const response = await fetch(`${url}/creditoscobranzas/renovaciones/listaclientes/${pCia}-${pRazonSocial}/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function ReFinanciamiento(pDocs)
{
    const requestOptions = {
        method : 'POST',
        headers : cabecera,
        body : JSON.stringify(pDocs)
    }

    const response = await fetch(`${url}/creditoscobranzas/renovaciones/refinanciar/`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getPrevioReFinanciamiento(pDocs)
{
    const requestOptions = {
        method : 'PUT',
        headers : cabecera,
        body : JSON.stringify(pDocs)
    }

    const response = await fetch(`${url}/creditoscobranzas/renovaciones/refinanciarprevio/`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}