import {Url} from "../constants/global"; 
 
const meServidorBackend = Url; 
 
const url = `${meServidorBackend}`; 
 
const cabecera = {'Content-type': 'application/json; charset=UTF-8'} 

export async function getCondPago(pCia)
{
    const requestOptions = {
        method : 'GET',
        headers : cabecera
    }

    const response = await fetch(`${url}/creditoscobranzas/condpago/listado/${pCia}/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}