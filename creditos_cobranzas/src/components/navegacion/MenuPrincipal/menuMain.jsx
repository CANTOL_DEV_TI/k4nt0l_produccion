import React, { useState } from "react";
import ti from "../assets/iconos/ti.gif";
import calidad from "../assets/iconos/calidad.gif";
import finanzas from "../assets/iconos/finanzas.gif";
import gdh from "../assets/iconos/gdh.gif";
import mantenimiento from "../assets/iconos/mantenimiento.gif";
import produccion from "../assets/iconos/produccion.gif";
import salida from "../assets/iconos/salida.gif";
import logistica from "../assets/iconos/logistica.gif";

import {
    Titulo1,
    Titulo2,
    Container,
    LogoContainer,
    Wrapper,
    Menu,
    MenuItem,
    MobileIcon,
    MenuItemLink, Block, MeLogo
} from "./menuMain.elements";

import {Link, NavLink } from "react-router-dom";

const MenuPrincipal = () => {
    const [showMobileMenu, setShowMobileMenu] = useState(false);

    return (
        <Wrapper>
            <Container>                
               
            
            <div className="container">
                <div className="row">
                    <div className="col-md-1" aria-colspan='3'>
                    
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 col-centered">
                        <NavLink className="produccion" to='/produccion'>
                            <div className="card">
                                <img src={produccion} alt="" />
                                <div className="card-body col-centered">
                                    <h6 className="card-title">PRODUCCION</h6>
                                    <p className="card-text text-primary"></p>

                                </div>
                            </div>
                        </NavLink>
                    </div>


                    <div className="col-md-3 col-centered">
                        <NavLink className="finanzas" to="/finanzas">
                            <div className="card">
                                <img src={finanzas} alt="" />
                                <div className="card-body">

                                    <h6 className="card-title, center" >FINANZAS</h6>
                                    <p className="card-text text-primary"></p>

                                </div>
                            </div>
                        </NavLink>
                    </div>

                    <div className="col-md-3 col-centered">
                        <NavLink className="contabilidad" to='/presupuesto/'>
                            <div className="card">
                                <img src={calidad} alt="" />
                                <div className="card-body">
                                    <h6 className="card-title">CALIDAD</h6>
                                    <p className="card-text text-primary"></p>                                   
                                </div>
                            </div>
                        </NavLink>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 col-centered">
                        <NavLink className="logistica" to='/logistica'>
                            <div className="card">
                                <img src={mantenimiento} alt="" />

                                <div className="card-body">
                                    <h6 className="card-title">MANTENIMIENTO</h6>
                                    <p className="card-text text-primary"></p>                                    
                                </div>

                            </div>
                        </NavLink>
                    </div>

                    <div className="col-md-3 col-centered">
                        <NavLink className="seguridadti" to='/seguridad/menu'>
                            <div className="card">
                                <img src={ti} alt="" />

                                <div className="card-body">
                                    <h6 className="card-title">TECNOLOGIA DE LA INFORMACION</h6>   
                                    <p className="card-text text-primary"></p>                                 
                                </div>

                            </div>
                        </NavLink>
                    </div>

                    <div className="col-md-3 col-centered">
                        <NavLink className="salir" to='/salir'>
                            <div className="card">
                                <img src={salida} alt="" />

                                <div className="card-body">
                                    <h6 className="card-title">SALIDA</h6>
                                    <p className="card-text text-primary"></p>
                                </div>

                            </div>
                        </NavLink>
                    </div>
                </div>

            </div>
            </Container>
        </Wrapper >
    );
};

export default MenuPrincipal;

//<img src={titulo_imagen} width={970}/>