//import React,{ useEffect } from 'react'
import React from 'react'
import Modal from 'react-bootstrap/Modal';

const VentanaModal = ({ children, show, handleClose, titulo, size }) => {
    /*useEffect(() => {
        const timer = setTimeout(() => {
          setCount('Timeout called!');
        }, 5000);
        return () => clearTimeout(timer);
      }, []);*/
      let verCerrar = false

      if(titulo===""){
        verCerrar = false
      }else{
        verCerrar = true
      }

    return (
        <Modal show={show} onHide={handleClose} size={size}> { /*backdrop="static" keyboard={false} */}
            { /*size="sm,lg,xl" */}
            <Modal.Header closeButton:verCerrar>
                <Modal.Title>{titulo}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {children}
            </Modal.Body>
            {/* <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
            </Modal.Footer>  */}
        </Modal>
    );
};
export default VentanaModal