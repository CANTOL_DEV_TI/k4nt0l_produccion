import React from "react";
import { Login } from "../../services/loginServices";
import { BotonLogin, BotonOlvide } from "../../components/Botones";
import VentanaModal from "../../components/VentanaModal";
import Bienvenido from "./bienvenido";
import { ProductionContext } from "../../Context/ProduccionContext";
import ResetClaveUsuario from "./resetclave";
import { ResetClave } from "../../services/usuarioServices";

class LoginV extends React.Component {
    static contextType = ProductionContext
    constructor(props) {
        super(props);

        this.state = { usuario_login: '', password_login: '', cia_login: '', showModal: false, Titulo: "", Mensaje: "", reset: false }

    }

    handleChange = (e) => {
        if (e.target.name === 'txtLogin') {
            this.setState({ usuario_login: e.target.value })
        }

        if (e.target.name === 'txtPassword') {
            this.setState({ password_login: e.target.value })
        }

        if (e.target.name === 'cboCia') {
            this.setState({ cia_login: e.target.value })
        }

    }


    handleSubmit = async (e) => {
        console.log(e)

        const responseJson = await Login(this.state.usuario_login, this.state.password_login, this.state.cia_login)
        //console.log(responseJson) 

        if (!responseJson.detail) {
            sessionStorage.setItem("CDTToken", responseJson);
            sessionStorage.setItem("Logo", this.state.cia_login)
            this.context.refresh_Logo(sessionStorage.getItem("Logo"));
            this.setState({ Mensaje: "Bienvenido " + this.state.usuario_login });
            this.setState({ Titulo: "Sistema de Gestion CANTOL-ERP" });
        } else {
            this.setState({ Titulo: "Error al iniciar" });
            this.setState({ Mensaje: responseJson.detail });
        }

        this.setState({ showModal: true, tipoOpe: 'Find' })

    }

    handleOlvide = async (e) => {
        this.setState({ reset: true })
    }

    handleClose = (e) => {
        this.setState({ showModal: false })
    }

    handleVolver = async (e) => {
        console.log(e)
        if (e !== '') {
            console.log(e)
            const reset = await ResetClave(e);

            console.log(reset)
            alert(reset)
        }
        this.setState({ reset: false })
    }


    render() {
        const { Titulo, Mensaje } = this.state

        return (
            <React.Fragment>
                {this.state.reset == true ?
                    <ResetClaveUsuario EnviarReset={this.handleVolver} />
                    :
                    <>
                        <div className="container text-center mt-5">
                            <div className="row justify-content-center">
                                <div className="col-xl-4 col-lg-6">
                                    <div className="card mt-4">
                                        <div className="card-body p-4 bg-exclamation">
                                            <h1 className="h3 mb-3 fw-normal">Ingresar a Sistema Web Cantol.</h1>
                                            <div className="form">
                                                <select className="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="cboCia" onChange={this.handleChange}>
                                                    <option value="NON">Empresa</option>
                                                    <option value="TCN">Tecnopress</option>
                                                    <option value="CNT">Cantol</option>
                                                    <option value="DTM">Distrimax</option>
                                                </select>
                                            </div>
                                            <div className="form-floating">
                                                <p />
                                            </div>
                                            <div className="form-floating">
                                                <input type="text" name="txtLogin" className="form-control" id="floatingInput" placeholder="Usuario..." autoFocus onChange={this.handleChange} value={this.state.usuario_login} />
                                                <label>Usuario</label>
                                            </div>
                                            <div className="form-floating">
                                                <p />
                                            </div>
                                            <div className="form-floating">
                                                <input type="password" name="txtPassword" className="form-control" id="floatingPassword" placeholder="Contraseña..." onChange={this.handleChange} value={this.state.password_login} />
                                                <label>Contraseña</label>
                                            </div>
                                            <div className="form-floating">
                                                <p />
                                            </div>
                                            <div className="form-floating">
                                                <BotonLogin sw_habilitado={true} texto={"Inicio de Sesion"} eventoClick={this.handleSubmit} />
                                            </div>
                                            <div className="form-floating">
                                                <BotonOlvide sw_habilitado={true} texto={"Olvide mi contraseña"} eventoClick={this.handleOlvide} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.state.Titulo === "Error al iniciar" ?
                            <VentanaModal
                                show={this.state.showModal}
                                handleClose={this.handleClose}
                                titulo={this.state.Titulo}
                                setMensaje={this.state.Mensaje}
                                size={"xl"}
                            >
                            </VentanaModal>
                            :
                            <VentanaModal
                                show={this.state.showModal}
                                handleClose={this.handleClose}
                                titulo={this.state.Titulo}
                                setMensaje={this.state.Mensaje}
                                size={"xl"}
                                children={<Bienvenido />}
                            >
                            </VentanaModal>
                        }
                    </>
                }
            </React.Fragment >
        )
    }
    async componentDidMount() {
        //localStorage.setItem("CDTToken", ""); 
    }
};

export default LoginV;