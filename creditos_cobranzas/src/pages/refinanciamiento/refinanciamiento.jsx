import React from "react";
import { GetConsulta, ReFinanciamiento,getPrevioReFinanciamiento } from "../../services/refinanciamientoServices";
import Busqueda from "./components/Busqueda";
import { Validar } from "../../services/ValidaSesion.jsx";
import TablaPrincipal from "./components/TablaResultados.jsx";
import VentanaModal from "../../components/VentanaModal.jsx";
import Configurar from "./components/configurarRefinanciamiento.jsx";
import { getCondPago } from "../../services/condicionpagoServices.jsx";
import Carga_Imagen from "./components/carga.jsx";

class ListadoDocumentos extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            Cia: "",
            dataMasivo: [],
            Usuario: '',
            Total_Pagos: 0,
            Cant_Pagos: 0,
            showModalConfig: false,
            listaCP: [],
            showModalLoad : false
        }
    }

    handleBusqueda = async (pCliente, pDesde, pHasta) => {
        this.setState({isFetch : true });
        let xFiltro = { "Compañia": this.state.Cia, "Proveedor": pCliente, "Desde": pDesde, "Hasta": pHasta }        
        const responseJson = await GetConsulta(xFiltro);
        this.setState({ resultados: responseJson, isFetch : false });

    }

    handleGetRow = async (e) => {
        const Seleccionado = {
            "cia": this.state.Cia,
            "indice": e.indice, "docentry": e.DocEntry, "seriesname": e.SeriesName, "lictradnum": e.LicTradNum, "cardcode": e.CardCode, "cardname": e.CardName,
            "doccur": e.DocCur, "docduedate": e.DocDueDate, "paidtodate": Number.parseFloat(e.PaidToDate), "docnum": e.DocNum, "foliopref": e.FolioPref, "folionum": e.FolioNum,
            "doctotal": Number.parseFloat(e.DocTotal), "paytocode": e.PayToCode, "address": e.Address, "comments": e.Remark, "condp" : "", "shiptocode" : e.ShipToCode
        }

        this.setState({ dataMasivo: [...this.state.dataMasivo, Seleccionado] })
    }

    handleDeleteRow = (NroInterno) => {
        let items = this.state.dataMasivo
        let targetIndex = items.findIndex(items => items.indice === NroInterno)
        items.splice(targetIndex, 1)
        this.setState({ dataMasivo: items })
    }

    handleCiaNombre = async (e) => {
        if (e === "CNT") {
            let xNombre = "CANTOL S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "DTM") {
            let xNombre = "DISTRIMAX S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "TCN") {
            let xNombre = "TECNOPRESS S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }
    }

    handleConfigurar = async (e) => {
        let Docs = this.state.dataMasivo
        //console.log(Docs.length)
        if (Docs.length === 0) {
            alert("Debes escoger al menos un pago para poder efectuar el refinanciamiento")
        } else {
            //console.log(Docs)

            //const XData = await ReFinanciamiento(Docs)
            //console.log(XData)
            //alert(XData)
            this.setState({ showModalConfig: true })
        }
    }

    handleRefinanciar = async (e) => {       
        this.setState({ showModalConfig: false })     
        this.setState({showModalLoad : true})   
        const XData = await ReFinanciamiento(e)
        console.log(XData)
        
        alert(XData)
        this.setState({showModalLoad : false,isFetch : false})
        this.setState({dataMasivo : []})      
        this.handleBusqueda("","","")  
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad } = this.state

        return (
            <React.Fragment>
                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}
                <Busqueda handleFiltrarPor={this.handleBusqueda} handleCancelarDocs={this.handleConfigurar} />
                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                            <tr>
                                <th className="align-middle"></th>
                                <th className="align-middle">Serie</th>
                                <th className="align-middle">RUC</th>
                                <th className="align-middle">Codigo</th>
                                <th className="align-middle">Razon Social</th>
                                <th className="align-middle">Moneda</th>
                                <th className="align-middle">Fecha Vencimiento</th>
                                <th className="align-middle">Pagado a la fecha</th>
                                <th className="align-middle">Numero SAP</th>
                                <th className="align-middle">Serie Fiscal</th>
                                <th className="align-middle">Nro Fiscal</th>
                                <th className="align-middle">Total</th>
                                <th className="align-middle">Comentario</th>
                                <th className="align-middle">Pagar a </th>
                                <th className="align-middle">Direccion</th>
                            </tr>
                        </thead>

                        {resultados.map((registro, index) =>
                            <TablaPrincipal
                                key={index}
                                Fila={registro}
                                eventoMasivo={() => this.handleGetRow(registro)}
                                eventoQuitar={() => this.handleDeleteRow(registro.indice)}
                            />
                        )}

                        <tfoot>

                        </tfoot>

                    </table>
                    <VentanaModal show={this.state.showModalConfig} size={"ml"} handleClose={() => this.setState({ "showModalConfig": false })} titulo={"Configurar Refinanciamiento"}>
                        <Configurar xLCP={this.state.listaCP} xDocs={this.state.dataMasivo} handleRefinanciar={this.handleRefinanciar} />
                    </VentanaModal>
                    <VentanaModal show={this.state.showModalLoad} size={"sm"} >
                        <Carga_Imagen/>
                    </VentanaModal>

                </div>
            </React.Fragment>
        )
    }
    async componentDidMount() {
        const ValSeguridad = await Validar("TEREFI")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar, "Cia": ValSeguridad.cia, "Usuario": ValSeguridad.usuario })

        let LCP = await getCondPago(ValSeguridad.cia)
        this.setState({ "listaCP": LCP })
        //console.log(LCP)
    }
}

export default ListadoDocumentos;