import React from "react";
import { useEffect, useState } from "react";
import { BotonSeleccionar,BotonConsultar } from "../../../components/Botones";
import { getClientes } from "../../../services/refinanciamientoServices";

export function BusquedaClientes({Cia, ResultadosBusqueda}) {
    const [listClientes, setlistClientes] = useState([])
    const sCia = Cia

    useEffect(() => {

    })
    
    console.log(Cia,sCia)

    const handleFind = async (e) => {
        const responseJson = await getClientes(sCia, e.target.value)
        setlistClientes(responseJson)
    }

    return(
        <div>
            <div className="card-header cardalign w-50rd"> 
                <div className="card-header border border-dashed border-end-0 border-start-0"> 
                    <div className="form-group"> 
                        <div className="form-group row"> 
                            <div className="col col-form-label"> 
                                Razon Social : <input type="text" style={{width:400}} name="tClientes" onChange={handleFind} /> 
                                <BotonConsultar sw_habilitado={true} eventoClick={() => handleFind()} /> 
                            </div> 
                        </div> 
                        <div className="form-group row"> 
                            <div className="table-responsive"> 
                                < table className="table table-hover table-sm table-bordered" > 
                                    <thead className="table-secondary text-center table-sm"> 
                                        <tr> 
                                            <td className="col-sm-auto col-form-label">Codigo</td> 
                                            <td className="col-sm-auto col-form-label">Nombre</td> 
                                            <td className="col-sm-auto col-form-label">R.U.C.</td>
                                            <td className="col-sm-auto"></td> 
                                        </tr> 
                                    </thead> 
                                    <tbody className="list"> 
                                        {listClientes.length > 0 && 
                                            listClientes.map((registro) => 
                                                <tr> 
                                                    <td className="td-cadena" id={registro.Codigo}>{registro.Codigo}</td> 
                                                    <td className="td-cadena" >{registro.Razon_Social}</td> 
                                                    <td className="td-cadena" >{registro.NroDoc}</td> 
                                                    <td className="td-cadema" > 
                                                        <span className="actions"> 
                                                            <button onClick={() => ResultadosBusqueda(registro.Codigo, registro.Razon_Social)}> 
                                                                <BotonSeleccionar /> 
                                                            </button> 
                                                        </span> 
 
                                                    </td> 
                                                </tr> 
                                            )} 
                                    </tbody> 
                                </table> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div>
        </div>
    )
}