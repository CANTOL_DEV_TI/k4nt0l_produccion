import React, { Component } from "react";

class TablaPrincipal extends React.Component {
    constructor(props) {
        super(props);

    }

    handleCheck = (e) => { 
        if (e.target.checked === true) { 
            this.props.eventoMasivo(e) 
        } 
 
        if (e.target.checked === false) { 
            //console.log(e.target.name) 
            this.props.eventoQuitar(e.target.name) 
        } 
    } 

    render() {
        const { Key, Fila, eventoCerrar, eventoMasivo, eventoQuitar } = this.props
        return (
            <tbody className="list">
                <tr>
                    <td className={"td-cadena"}><input type="checkbox" name={Fila.indice} onChange={this.handleCheck} /> </td>
                    <td className="td-cadena" id={Fila.DocEntry}>{Fila.SeriesName}</td>
                    <td className="td-cadena">{Fila.LicTradNum}</td>
                    <td className="td-cadena">{Fila.CardCode}</td>
                    <td className="td-cadena">{Fila.CardName}</td>
                    <td className="td-cadena">{Fila.DocCur}</td>
                    <td className="td-cadena">{Fila.DocDueDate}</td>
                    <td className="td-cadena">{Fila.PaidToDate}</td>
                    <td className="td-cadena">{Fila.DocNum}</td>
                    <td className="td-cadena">{Fila.FolioPref}</td>
                    <td className="td-cadena">{Fila.FolioNum}</td>
                    <td className="td-cadena">{Fila.DocTotal.toLocaleString('en-EN')}</td>
                    <td className="td-cadena">{Fila.Remark}</td>
                    <td className="td-cadena">{Fila.PayToCode}</td>
                    <td className="td-cadena">{Fila.Address}</td>
                </tr>

            </tbody>
        )
    }

}

export default TablaPrincipal;