import React from "react";
import Title from "../../../components/Titulo";
import { BotonAdd, BotonGrabar } from "../../../components/Botones";
import { getPrevioReFinanciamiento } from "../../../services/refinanciamientoServices";

class Configurar extends React.Component {
    constructor(props) {
        super(props);
        this.state = { cia: "", cant_letras: 0, listaCP: this.props.xLCP, condp: "0", listaDocs: this.props.xDocs, listaPrevia: [] }
    }

    handleChange = (e) => {
        if (e.target.name === "sCondicionPago")
            this.setState(condp = e.target.value)
        let DocsCP = this.state.listaDocs

        DocsCP[0]["condp"] = e.target.value

        this.setState({ listaDocs: DocsCP })

    }
    handleLetraPrevio = async (e) => {
        const VistaPrevia = await getPrevioReFinanciamiento(this.state.listaDocs)
        this.setState({ listaPrevia: VistaPrevia })
        //console.log(VistaPrevia)
    }

    render() {
        const { handleRefinanciar, xLCP, xDocs, handlePrevio } = this.props

        return (
            <React.Fragment>
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header border border-dashed border-end">
                            <div className="row align-items-center gy-3">
                                <div className="col-sm-auto">
                                    <Title>Configurar la emision de letras Refinanciadas</Title>
                                </div>
                            </div>
                            <div className="row align-items-center gy-3">
                                <div className="col-sm-auto">
                                    <div className="d-flex flex-wrap gap-1">
                                        Condicion de pago : <select id="sCondicionPago" onChange={this.handleChange}>
                                            <option value="0">----Seleccione Condicion----</option>
                                            {
                                                xLCP.map((v_CP) =>
                                                    <option value={v_CP.Codigo}>{`${v_CP.Nombre}`}</option>
                                                )
                                            }
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="row align-items-center gy-3">
                                <div className="col-sm-auto">
                                    <div className="d-flex flex-wrap gap-1">
                                        <BotonAdd title="Previo" textoBoton="Generar Previo" eventoClick={this.handleLetraPrevio} />
                                    </div>
                                </div>
                            </div>
                            <div className="row align-items-center gy-3">
                                <div className="col-sm-auto">
                                    <div className="d-flex flex-wrap gap-1">
                                        Cant. Letras :
                                        <table className="table table-hover table-sm table-bordered">
                                            <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                                                <tr>
                                                    <td className="td-cadena">Orden</td>
                                                    <td className="td-cadena">Fecha</td>
                                                    <td className="td-cadena">Monto</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.listaPrevia.map((registro, index) =>
                                                        <tr>
                                                            <td>{registro.Orden}</td>
                                                            <td>{registro.Fecha_Letra}</td>
                                                            <td>{registro.Monto_Letra}</td>
                                                        </tr>
                                                    )
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div className="row align-items-center gy-3">
                                <div className="col-sm-auto">
                                    <div className="d-flex flex-wrap gap-1">
                                        <BotonGrabar title="Generar Refinanciamiento" textoBoton="Generar Refinanciamiento" eventoClick={()=>handleRefinanciar(this.state.listaDocs)} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Configurar;