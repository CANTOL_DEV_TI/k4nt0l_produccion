import React, { useState } from "react";
import Title from "../../../components/Titulo";
import { BotonBuscar, BotonConsultar, BotonGrabar } from "../../../components/Botones";
import VentanaModal from "../../../components/VentanaModal";
import { BusquedaClientes } from "./listaClientes";
import { Validar } from "../../../services/ValidaSesion";

class Busqueda extends React.Component {

    constructor(props) {
        super(props);

        this.state = { Compañia: '', Cliente: '', Desde: '', Hasta: '', Razon_Social: "", showModalCliente: false }
    }

    handleChange = (e) => {

        if (e.target.name === "tCliente") {
            this.setState({ Cliente: e.target.value })
            console.log(this.state.Cliente)
        }

        if (e.target.name === "tDesde") {
            this.setState({ Desde: e.target.value })
            console.log(this.state.Desde)
        }

        if (e.target.name === "tHasta") {
            this.setState({ Hasta: e.target.value })
            console.log(this.state.Hasta)
        }

    }

    handleShowListaClientes = (e) => {
        this.setState({ "showModalCliente": true })
    }

    handleSettings = (xCodigo, xNombre) => {
        this.setState({ Cliente: xCodigo, Razon_Social: xNombre, showModalCliente: false })

    }

    render() {

        const { handleFiltrarPor, handleCancelarDocs } = this.props

        return (            
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm-auto">
                                <Title>Listado de Letras / Notas de Debito</Title>
                            </div>
                        </div>
                        <div className="row align-items-center gy-3">
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    Cliente :
                                    <input name="tCliente" readOnly style={{ width: 150 }} placeholder="Seleccione un Cliente..." onChange={this.handleChange} value={this.state.Cliente} />
                                    -<input name="tClienteR" readOnly style={{ width: 400 }} placeholder="Seleccione un Cliente..." onChange={this.handleChange} value={this.state.Razon_Social} />
                                    <BotonConsultar textoBoton={"Clientes"} sw_habilitado={true} eventoClick={this.handleShowListaClientes} />
                                    Desde : <input name="tDesde" type="date" placeholder="Seleccione Desde..." onChange={this.handleChange} value={this.state.Desde} />
                                    - Hasta : <input name="tHasta" type="date" placeholder="Seleccione Hasta..." onChange={this.handleChange} value={this.state.Hasta} />
                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => this.props.handleFiltrarPor(this.state.Cliente, this.state.Desde, this.state.Hasta)} />

                                </div>
                            </div>
                        </div>
                        <div className="row align-items-center gy-3">
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                     <p></p>
                                </div>
                            </div>
                        </div>
                        <div className="row align-items-center gy-3">
                            <div className="col-sm-auto">
                                <BotonGrabar textoBoton={"ReFinanciar los Docs Seleccionados"} sw_habilitado={true} eventoClick={handleCancelarDocs} />
                            </div>
                        </div>
                    </div>
                </div>
                <VentanaModal show={this.state.showModalCliente} size={"ml"} handleClose={() => this.setState({ "showModalCliente": false })} titulo={"Seleccione un cliente"}>
                    <BusquedaClientes Cia={this.state.Compañia} ResultadosBusqueda={this.handleSettings} />
                </VentanaModal>
            </div>
        )
    }
    async componentDidMount() {
        const ValSeguridad = await Validar("TEREFI")
        this.setState({ "Compañia": ValSeguridad.cia, "Usuario": ValSeguridad.usuario })
    }
}

export default Busqueda;