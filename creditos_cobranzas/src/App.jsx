import React from 'react'
import { BrowserRouter } from "react-router-dom";
import { MiRutas } from "./routers/rutas"
import NavbarV1 from "./components/navegacion/Navbar_V1/NavbarV1"
import { ProduccionContext } from './Context/ProduccionContext';

function App() {
  return (
    <BrowserRouter basename="Creditos_Cobranzas">
      <ProduccionContext>
        <div className="page-content">
          <NavbarV1 />
          <div className="container-fluid">
            <MiRutas />
          </div>
        </div>
      </ProduccionContext>
    </BrowserRouter >
  )
}

export default App
