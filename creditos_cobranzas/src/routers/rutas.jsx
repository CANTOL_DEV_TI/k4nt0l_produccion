import React from "react"; 
import { Routes, Route } from "react-router-dom"; 
import LoginV from "../pages/login/login"; 
import ActuaPassword from "../pages/login/cambiocontraseña"; 
import ListadoDocumentos from "../pages/refinanciamiento/refinanciamiento";
import ProtectedRoute from "../components/utils/ProtectedRoute"; 
 
 
 
export function MiRutas() { 
 
  return ( 
    <Routes> 
      <Route path="/" element={<LoginV />} /> 
      <Route element={<ProtectedRoute Redirige={"/"} />} > 
        <Route path="/cambiocontrasena" element={<ActuaPassword />} /> 
      </Route> 
      <Route element={<ProtectedRoute Redirige={"/"} />} > 
        <Route path="/listadoletras" element={<ListadoDocumentos/>}  /> 
      </Route> 
    </Routes> 
  ); 
}