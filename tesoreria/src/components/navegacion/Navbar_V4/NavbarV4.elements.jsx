import styled from "styled-components";


export const Container = styled.header`
  //border: 1px solid #E96479;
  padding: 0;
  margin: 0;
`;



export const LogoContainer = styled.div`
  margin-left: 10px;
  display: inline-block;
  align-items: center;
  font-size: 1.2rem;
  font-family: sans-serif;
  
  //border: 1px solid #00a1d4;
  
  svg {
    fill: #2A6DAD;
    //margin-right: 0.5rem;
    margin: 14px 0.5rem 14px 0;
    width: 100px;
    height: 50px;
    
    //border: 1px solid #4D455D;
  }
  
`;



export const Nav = styled.nav`
  width: 100%;
  height: 100%;
  position: fixed;
  background-color: var(--azul);
  overflow: hidden;
  max-height: 0;
  transition: max-height .5s ease-out;
  
  //border: 1px solid yellow;
  //margin: 0;
  //padding: 0;
  
  @media screen and (min-width: 920px) {  
    max-height: none;
    top: 0;
    position: relative;
    float: right;
    width: fit-content;
    background-color: transparent;
  }
  
  
`;

export const Menu = styled.ul`
  //border: 1px solid blue;
  //margin: 0;
  //padding: 0;
  
   
  a{
    display: block;
    padding: 30px 0;
    
    color: var(--white);
    text-align: center;
    vertical-align: middle;
    //background-color: var(--white);
      
    //border: 1px solid black;
    @media screen and (min-width: 920px) {
      width: 200px;
      color: var(--black);
    }
      
    &:hover{
      background-color: var(--azul);
      color: var(--black);
      
      @media screen and (min-width: 920px) {
        //background-color: transparent;
        color: var(--white);
        //background-color: var(--azul);
        
      }
    }
    
  }
  
  li{
    @media screen and (min-width: 920px) {
      float: left;
    }
  }
` ;

export const Hamb = styled.label`
  cursor: pointer;
  float: right;
  padding: 40px 20px;
  
  @media screen and (min-width: 920px) {
    display: none;
  }
  
`;

export const HambLine = styled.span`
    background: var(--azul);
    display: block;
    height: 2px;
    position: relative;
    width: 24px;
  
  &:before{
    top: 5px;
  }
  
  &:after{
    top: -5px;
  }
  
  &:before, &:after{
    background: var(--azul);
    content: '';
    display: block;
    height: 100%;
    position: absolute;
    transition: all .2s ease-out;
    width: 100%;
  }
`;

export const Li = styled.li`
  //margin: 0;
  //padding: 0;
  //border: 1px solid red;
`;
 


export const A = styled.a`
  //border: 1px solid black;
`;

export const SubNav = styled.li`
  
  //border: 1px solid red;
  //margin: 0;
  //padding: 0;
  
    & p{
      
    }
  
  
`;

export const SubNavBtn = styled.p`
  text-align: center;
  
  color: var(--white);
  
  
  @media screen and (min-width: 920px) {
    width: 200px;
    padding: 30px 0;
    color: var(--black);
  }
  
  
  &:hover{
    background-color: var(--azul);
    color: var(--black);
    
    @media screen and (min-width: 920px) {
      color: var(--white);
    }
    
  }
  
  
      //border: 1px solid black;
`;

export const SubNavContent = styled.div`
    background-color:  var(--white);
    width: 100%;
    z-index: 1;
    padding: 30px 0 ;
    display: none;
  
  
  & a{
    color: var(--black);
    text-decoration: none;
    padding: 10px 0;
    margin: 0;
    text-align: center;
    
    &:hover{
      color: var(--white);
    }
    
    @media screen and (min-width: 920px) {
      color: white;
    }
  }
  
  @media screen and (min-width: 920px) {
    padding: 20px 0 ;
    display: none;
    background-color:  var(--black);
  }
`;