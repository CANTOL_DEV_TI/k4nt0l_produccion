import React, {useState} from "react";
import {
    Container,
    LogoContainer,
    Menu,
    SubNav, SubNavContent, Nav, A, Li, SubNavBtn, Hamb, HambLine
} from "./NavbarV4.elements";


import {FaBars, FaTimes, FaHome, FaUserAlt, FaBriefcase, FaGlasses} from "react-icons/fa";
import {IconContext} from "react-icons";

// import {ReactComponent as ReactLogo} from './static/logoSVG.svg';

import './navbarV4.css'

import {NavLink} from "react-router-dom";


const NavbarV4 = () => {
    const [showMobileMenu, setShowMobileMenu] = useState(false);

    return (


        <Container className="header">

            {/*<a href="#" className="logo">LR</a>*/}

            {/*<LogoContainer>*/}
            {/*    <ReactLogo/>*/}
            {/*</LogoContainer>*/}

            <input className="side-menu" type="checkbox" id="side-menu"/>
            <Hamb className="hamb" htmlFor="side-menu"><HambLine className="hamb-line"></HambLine></Hamb>

            <Nav className="nav">
                <Menu className="menu">
                    <Li>
                        <NavLink to='/'>
                                INICIO
                        </NavLink>
                    </Li>
                    <Li>
                        <NavLink to='/vendedores'>
                                VENDEDORES
                        </NavLink>
                    </Li>

                    <SubNav className="subnav">
                        <SubNavBtn className="subnavbtn">CONTACT <i className="fa fa-caret-down"></i></SubNavBtn>
                        <SubNavContent className="subnav-content">
                            <NavLink to='#'>Email</NavLink>
                            <NavLink to='#'>Twitter</NavLink>
                            <NavLink to='#'>Phone</NavLink>
                        </SubNavContent>
                    </SubNav>

                    <Li>
                        <NavLink to='/salir'>
                                CERRAR SESION
                        </NavLink>
                    </Li>
                </Menu>

            </Nav>
        </Container>
    );
};

export default NavbarV4;