import React from "react";
import Image from "react-bootstrap/Image";

const Carga_Imagen = () => {
    return (
        <React.Fragment>
            <Image src="https://cdn-icons-gif.flaticon.com/10282/10282582.gif" width={180} height={170} rounded/>   
        </React.Fragment>                                     
    );
};

export default Carga_Imagen;