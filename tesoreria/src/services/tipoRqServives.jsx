import {Url} from '../constants/global'

// const meServidorBackend = 'http://192.168.5.21:8080'
const meServidorBackend = Url

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function get_listaTipoRQ(pFiltro)
{
    if (pFiltro.trim() === '') {
        pFiltro = '%20'
    }

    const requestOptions = {
        method: 'GET',
        headers: cabecera     
    }

    const response = await fetch(`${meServidorBackend}/tesoreria/tiporequerimiento/${pFiltro}`,requestOptions)    
    const responseJson = await response.json()    
    return responseJson
    
}

export async function save_TipoRQ(Data)
{
    const requestOptions = {
        method : 'POST',
        headers : cabecera,
        body : JSON.stringify(Data)
    }

    const response = await fetch(`${meServidorBackend}/tesoreria/tiporequerimiento/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function update_TipoRQ(Data)
{
    const requestOptions = {
        method : 'PUT',
        headers : cabecera,
        body : JSON.stringify(Data)
    }

    const response = await fetch(`${meServidorBackend}/tesoreria/tiporequerimiento/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function delete_TipoRQ(Data)
{
    const requestOptions = {
        method : 'DELETE',
        headers : cabecera,
        body : JSON.stringify(Data)
    }

    const response = await fetch(`${meServidorBackend}/tesoreria/tiporequerimiento`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}