import { json } from "react-router-dom";
import { Url } from "../constants/global";

const meServidorBackend = Url

const cabecera = { 'Content-type': 'application/json; charset=UTF-8' }

export async function getLista_Presupuestos(pFiltros) {

    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(pFiltros)
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/presupuestoxusuario/`, requestOptions)
    const responseJson = await response.json()
    //console.log(responseJson)
    return responseJson

}

export async function getDetalle_Presupuesto(pFiltros) {

    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(pFiltros)
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/presupuestoxcodigo/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getLista_PresupuestosTotal(pFiltros) {
    console.log(pFiltros)
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(pFiltros)
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/presupuestototal/`, requestOptions)
    const responseJson = await response.json()
    //console.log(responseJson)
    return responseJson

}

export async function getDetalle_PresupuestoTotal(pFiltros) {

    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(pFiltros)
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/presupuestototaldetalle/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}
//#region  Borradores

export async function ListadoBorradores(pData) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(pData)
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/Borradores/Listado/`, requestOptions)

    const responseJson = await response.json()
    return responseJson
}

export async function AgregarBorrador(pData) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(pData)
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/Agregar/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function ActualizarBorrador(pData) {
    const requestOptions = {
        method: 'PUT',
        headers: cabecera,
        body: JSON.stringify(pData)
    }
    console.log("entro a actualizar")
    console.log(pData)
    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/Actualizar/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function EliminarBorrador(pData) {
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,
        body: JSON.stringify(pData)
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/Eliminar/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getDetalle_Borrador(pFiltro) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(pFiltro)
    }
    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/Detalle/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getCuentaContableSAP(pCia, pDescripcion) {
    const requestOptions = {
        method: 'GET',
        headers: cabecera

    }
    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/CuentasSAP/${pCia}-${pDescripcion}/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getCentroCostoSAP(pCia, pDescripcion) {
    const requestOptions = {
        method: 'GET',
        headers: cabecera

    }
    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/CentroCostoSAP/${pCia}-${pDescripcion}/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getCategoriaPresupuestalSAP(pCia, pDescripcion, pCuenta) {
    const requestOptions = {
        method: 'GET',
        headers: cabecera

    }
    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/CategoriaPreSAP/${pCia}-${pDescripcion}-${pCuenta}/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getBandejaBorradores(pCia, pCode, pEjercicio, pUsuario) {
    console.log(pCia)
    console.log(pCode)
    console.log(pEjercicio)
    console.log(pUsuario)
    
    if (pCode == "") {
        pCode = "20%"
    }

    if (pUsuario == "") {
        pUsuario = "20%"
    }
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/Bandeja/${pCia}/${pCode}/${pEjercicio}/${pUsuario}/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function AdministraBorrador(pData) {
    const requestOptions = {
        method: 'PUT',
        headers: cabecera,
        body: JSON.stringify(pData)
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/Autorizar/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}
//#endregion

export async function HistoralBorradores(pCia, pCode) {
    const requestOptions = {
        method: 'GET',
        header: cabecera
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/Historial/${pCia}/${pCode}`, requestOptions)
    const responseJson = await response.json()
    return responseJson

}

export async function ValidarCentroCosto(pCia, pCentroC) {
    const requestOptions = {
        method: 'GET',
        header: cabecera
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/ValidaCC/${pCia}/${pCentroC}/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function ValidarCuentaContable(pCia, pCta) {
    const requestOptions = {
        method: 'GET',
        header: cabecera
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/ValidaCta/${pCia}/${pCta}`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function ValidarImportacion(pData) {        
    let det = {detalles : pData}
    
    const requestOptions = {
        method : 'POST',
        headers : cabecera,
        body : JSON.stringify(det)
    }
    
    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/ValidarImportacion/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
    
}

export async function MigrarSAP(pData) {

    const requestOptions = {
        method : 'POST',
        headers : cabecera,
        body : JSON.stringify(pData)
    }
    
    const response = await fetch(`${meServidorBackend}/presupuesto/Borrador/MigracionSAP/`,requestOptions)
    const responseJson = await response.json()
    return responseJson

}

export async function ReportePresupuestosX(pCia,pUsuario) {
    const requestOptions = {
        method: 'GET',
        header: cabecera
    }

    const response = await fetch(`${meServidorBackend}/presupuesto/reportepresupuestos/${pCia}/${pUsuario}`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}