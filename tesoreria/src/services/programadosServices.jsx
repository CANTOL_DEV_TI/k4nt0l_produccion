import {Url} from '../constants/global'

const meServidorBackend = Url

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function getLista_Programados(Filtros ) {
    
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body : JSON.stringify(Filtros)
    }
    const response = await fetch(`${meServidorBackend}/tesoreria/pagosprogramados/`,requestOptions)
    
    const responseJson = await response.json()    
    return responseJson
}

export async function Terminar_Programados(Data) {
    const requestOptions = {
        method : 'POST',
        headers : cabecera,
        body : JSON.stringify(Data)
    }
    const response = await fetch(`${meServidorBackend}/tesoreria/pagosprogramados/terminar/`, requestOptions)
    const responseJson = await response.json()
    console.log(responseJson)
    return responseJson
}

export async function ReProgramar_Pagos(Data) {
    const requestOptions = {
        method : 'PUT',
        headers : cabecera,
        body : JSON.stringify(Data)
    }
    const response = await fetch(`${meServidorBackend}/tesoreria/pagosprogramados/reprogramar/`, requestOptions)

    const responseJson = await response.json()
    return responseJson
}

export async function Anular_Programacion(Data) {
    const requestOptions = {
        method : 'DELETE',
        headers : cabecera,
        body : JSON.stringify(Data)
    }
    const response = await fetch(`${meServidorBackend}/tesoreria/pagosprogramados/anular/`, requestOptions)

    const responseJson = await response.json()
    return responseJson
}

export async function Historial_Programacion(Data){
    const requestOptions = {
        method : 'POST',
        headers : cabecera,
        body : JSON.stringify(Data)
    }

    const response = await fetch(`${meServidorBackend}/tesoreria/pagosprogramados/historial/`, requestOptions)

    const responseJson = await response.json()
    return responseJson

}

export async function getTotalesLista_Programados(Filtros ) {
    
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body : JSON.stringify(Filtros)
    }
    const response = await fetch(`${meServidorBackend}/tesoreria/pagosprogramados/totales/`,requestOptions)
    
    const responseJson = await response.json()    
    return responseJson
}

export async function getPagosHoy(pCia) {
    if (pCia.trim() === '') {
        pCia = '%20'
    }
    
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/tesoreria/pagosprogramados/hoy/${pCia}`,requestOptions)
    const responseJson = await response.json()       
    return responseJson
}