import { Url } from '../constants/global' 

const meServidorBackend = Url 
 
const cabecera = { 'Content-type': 'application/json; charset=UTF-8' } 

export async function get_CategoriasPre(pCia, pFiltro) { 
    console.log(pCia,pFiltro) 
    if (pFiltro === undefined) { 
        pFiltro = '%20' 
    } else { 
 
        if (pFiltro.trim() === '') { 
            pFiltro = '%20' 
        } 
    } 
    const requestOptions = { 
        method: 'GET', 
        headers: cabecera 
 
    } 
    const response = await fetch(`${meServidorBackend}/presupuesto/CategoriasPresupuestales/${pCia}/${pFiltro}`, requestOptions) 
    console.log(`${meServidorBackend}/presupuesto/CategoriasPresupuestales/${pCia}/${pFiltro}`)    
    const responseJson = await response.json() 
    return responseJson 
}

export async function saveCategoriaPresupuestal(pData) {         
    const requestOptions = { 
        method: 'POST', 
        headers: cabecera ,
        body : JSON.stringify(pData)
    } 
    const response = await fetch(`${meServidorBackend}/presupuesto/CategoriaPresupuestal/`, requestOptions)     
    const responseJson = await response.json() 
    return responseJson 
}

export async function updateCategoriaPresupuestal(pData) {         
    const requestOptions = { 
        method: 'PATCH', 
        headers: cabecera ,
        body : JSON.stringify(pData)
    } 
    const response = await fetch(`${meServidorBackend}/presupuesto/CategoriaPresupuestal/`, requestOptions)     
    const responseJson = await response.json() 
    return responseJson 
}

export async function deleteCategoriaPresupuestal(pData) {         
    const requestOptions = { 
        method: 'DELETE', 
        headers: cabecera ,
        body : JSON.stringify(pData)
    } 
    const response = await fetch(`${meServidorBackend}/presupuesto/CategoriaPresupuestal/`, requestOptions)     
    const responseJson = await response.json() 
    return responseJson 
}

