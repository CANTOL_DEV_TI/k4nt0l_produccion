import { Url } from '../constants/global' 
 
// const meServidorBackend = 'http://192.168.5.21:8080' 
const meServidorBackend = Url 
 
const cabecera = { 'Content-type': 'application/json; charset=UTF-8' } 
 
export async function get_Bancos(pCia) { 
 
    const requestOptions = { 
        method: 'GET', 
        headers: cabecera 
 
    } 
    const response = await fetch(`${meServidorBackend}/tesoreria/bancos/${pCia}`, requestOptions) 
    const responseJson = await response.json() 
    return responseJson 
} 
 
export async function get_CuentasBancarias(pCia, pBanco) { 
    console.log(pCia,pBanco) 
    if (pBanco === undefined) { 
        pBanco = '%20' 
    } else { 
 
        if (pBanco.trim() === '') { 
            pBanco = '%20' 
        } 
    } 
    const requestOptions = { 
        method: 'GET', 
        headers: cabecera 
 
    } 
    const response = await fetch(`${meServidorBackend}/tesoreria/bancos/Cuentas/${pCia}-${pBanco}`, requestOptions) 
    console.log(`${meServidorBackend}/tesoreria/bancos/Cuentas/${pCia}-${pBanco}`) 
    const responseJson = await response.json() 
    return responseJson 
} 
