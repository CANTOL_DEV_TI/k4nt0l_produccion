import { Url } from '../constants/global' 
 
// const meServidorBackend = 'http://192.168.5.21:8080' 
const meServidorBackend = Url 
 
const cabecera = { 'Content-type': 'application/json; charset=UTF-8' } 
 
export async function get_CCXUsuario(pCia,pUsuario) { 
 
    const requestOptions = { 
        method: 'GET', 
        headers: cabecera 
 
    } 
    const response = await fetch(`${meServidorBackend}/tesoreria/ccxusuario/${pCia}/${pUsuario}`, requestOptions) 
    const responseJson = await response.json() 
    return responseJson 
}