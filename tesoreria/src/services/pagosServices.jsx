import { Url } from '../constants/global'

// const meServidorBackend = 'http://192.168.5.21:8080'
const meServidorBackend = Url

const cabecera = { 'Content-type': 'application/json; charset=UTF-8' }

export async function getFilter_PagosPendientes(pCia, pDesde, pHasta, pProveedor, pMoneda, pOrdena, pTipoOrden, pDetraccion) {

    if (pProveedor.trim() === '') {
        pProveedor = ' '
    }

    const datos = { "Cia": pCia, "Desde": pDesde, "Hasta": pHasta, "Proveedor": pProveedor, "Moneda": pMoneda, "Ordenar": pOrdena, "TipoOrden": pTipoOrden, "Detraccion": pDetraccion }


    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(datos)
    }
    const response = await fetch(`${meServidorBackend}/tesoreria/pagos/ListadoPendientes/`, requestOptions)
    //console.log(`${meServidorBackend}/tesoreria/pagos/ListadoPendientes/`)
    const responseJson = await response.json()
    //console.log(responseJson)
    return responseJson
}

export async function saveProgramacion(Data) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(Data)
    }

    const response = await fetch(`${meServidorBackend}/tesoreria/pagos/`, requestOptions)
    const responseJson = await response.json()
    return responseJson

}

export async function TotalesPagosPendientes(pCia, pDesde, pHasta, pProveedor, pMoneda, pDetraccion) {
    if (pProveedor.trim() === '') {
        pProveedor = ' '
    }

    const datos = { "Cia": pCia, "Desde": pDesde, "Hasta": pHasta, "Proveedor": pProveedor, "Moneda": pMoneda, "Detraccion": pDetraccion }


    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(datos)
    }
    const response = await fetch(`${meServidorBackend}/tesoreria/pagos/TotalesPendientes/`, requestOptions)
    //console.log(`${meServidorBackend}/tesoreria/pagos/ListadoPendientes/`)
    const responseJson = await response.json()
    //console.log(responseJson)
    return responseJson
}

export async function GuardarComentarioSAP(pCia, pID, pComentario) {
    let trama = { "emp_codigo": pCia, "nro_interno": pID, "swc_comentarios": pComentario }

    const requestOptions = {
        method:'PATCH',
        headers:cabecera,
        body: JSON.stringify(trama)
    }

    const response = await fetch(`${meServidorBackend}/tesoreria/pagosprogramados/guardarcomentario/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}