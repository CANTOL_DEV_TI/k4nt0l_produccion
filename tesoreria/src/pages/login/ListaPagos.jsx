import React from "react";

class ListadoProg extends React.Component {
    constructor(props) {
        super(props);

        this.state = { Desde: '', Hasta: '', Proveedor: '' }
    }
    
    render() {
        const { Key, Fila } = this.props
        return (
            <tbody>
                <tr>
                    <td className="td-cadena" id={Fila.planificacion_id}>{Fila.planificacion_id}</td>
                    <td className="td-cadena" >{Fila.nro_documento}</td>
                    <td className="td-cadena" >{Fila.serie}</td>
                    <td className="td-cadena" >{Fila.nro}</td>
                    <td className="td-cadena" >{Fila.ruc}</td>
                    <td className="td-cadena" >{Fila.proveedor}</td>
                    <td className="td-cadena" >{Fila.fecha}</td>
                    <td className="td-cadena" >{Fila.monto_programado}</td>
                    <td className="td-cadena" >{Fila.tiporq_nombre}</td>                    
                    <td className="td-cadena" >{Fila.cond_pago}</td>
                </tr>
            </tbody>
        )
    }
}
export default ListadoProg;