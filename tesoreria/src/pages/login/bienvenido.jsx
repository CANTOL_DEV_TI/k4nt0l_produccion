import React from "react";
import "../../assets/css/styleStandar.css"
import {Link, NavLink } from "react-router-dom";
import { Validar } from "../../services/ValidaSesion";
import { getPagosHoy } from "../../services/programadosServices";
import ListadoProg from "./ListaPagos";
import { AvisoInicial } from "../../services/usuarioServices";

class Bienvenido extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            resultados: [],
            Aviso : 0            
        }
    }
    render(){
        const {Lista} = this.state
        return(
            <React.Fragment>
                <div>
                    <p>
                        <NavLink to={"/"}> ir al Inicio</NavLink>

                    </p>
                    { this.state.Aviso === 1 ?                     
                    <table className="table table-hover table-sm table-bordered table-striped" >
                        <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                            <tr >
                                <th colSpan={10} >Listado de Pagos para el dia de hoy</th>
                            </tr>
                            <tr>                                
                                <th className="align-middle" >PlaID</th>
                                <th className="align-middle" >Nro Documento</th>
                                <th className="align-middle" >Serie</th>
                                <th className="align-middle" >Nro</th>
                                <th className="align-middle" >Ruc</th>
                                <th className="align-middle" >Razon Social</th>
                                <th className="align-middle" >Fecha</th>
                                <th className="align-middle" >Monto</th>
                                <th className="align-middle" >Tipo RQ</th>                                
                                <th className="align-middle" >Cond Pago</th>    
                            </tr>
                        </thead>

                        {this.state.resultados.map((registro, index) =>
                            <ListadoProg
                                key={index}
                                Fila={registro}                                
                            />
                        )}
                        <tfoot>

                        </tfoot>
                    </table>
                    : <div></div>}
                </div>
            </React.Fragment>
        )
    }
    async componentDidMount(){
        const ValSeguridad = await Validar("")
       
        const Response = await getPagosHoy(ValSeguridad.cia)
        this.setState({resultados : Response})

        const AvisoI = await AvisoInicial(ValSeguridad.usuario,"GRUTES")
        this.setState({Aviso : AvisoI[0].AvisoInicial})
        console.log(this.state.Aviso)
    }
}


export default Bienvenido;