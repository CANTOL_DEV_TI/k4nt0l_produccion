import React from "react";
import {GrEdit,GrAmazon,GrCodeSandbox,GrTask} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({tiporq_id,                        
                        tiporq_nombre,                        
                        eventoEditar,
                        eventoEliminar                       
                        }) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena" id={tiporq_id}>{tiporq_id}</td>                
                <td className="td-cadena" >{tiporq_nombre}</td>                
                <td style={{textAlign:"center"}}><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td style={{textAlign:"center"}}><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                
            </tr>
        </tbody>
    )

export default ResultadoTabla;