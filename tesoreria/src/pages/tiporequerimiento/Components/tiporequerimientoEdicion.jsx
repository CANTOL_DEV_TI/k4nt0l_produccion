import React from "react";
import { BotonGuardar } from "../../../components/Botones";

class TipoRQEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { tiporq_id: 0, tiporq_nombre: ''}
        } else {
            this.state = { tiporq_id: this.props.dataToEdit.tiporq_id, tiporq_nombre: this.props.dataToEdit.tiporq_nombre}
        }
    }

    handleChange = (e) => {
        
        if (e.target.name === 'tiporq_id') {
            this.setState({ tiporq_id: e.target.value })
        }

        if (e.target.name === 'tiporq_nombre') {
            this.setState({ tiporq_nombre: e.target.value })
        }

    }


    render() {
        const { eventOnclick, nameOpe } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">                        
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-2 col-form-label"><input className="form-control form-control-mb" type="text" readOnly name="tiporq_id" onChange={this.handleChange} value={this.state.tiporq_id} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Nombre : </div>
                            <div className="col-sm-6 col-form-label"><input className="form-control form-control-mb" type="text" name="tiporq_nombre" onChange={this.handleChange} value={this.state.tiporq_nombre} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>                        
                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "tiporq_id": this.state.tiporq_id, "tiporq_nombre": this.state.tiporq_nombre})} texto={nameOpe} />
            </div>
        )
    }

}

export default TipoRQEdicion;