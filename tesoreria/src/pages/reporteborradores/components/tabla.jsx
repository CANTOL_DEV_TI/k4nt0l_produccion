import React from "react";
import { BotonConsultar } from "../../../components/Botones";

const ListadoTabla = ({
    cia,
    code,
    usuario,
    centro_costo,
    estado,
    total_presupuesto,
    detalle,
    eventoVer
}) =>
(
    <tbody>
        <tr>
            <td className="td-cadena ">{code}</td>
            <td className="td-cadena " >{usuario}</td>
            <td className="td-cadena " >{centro_costo}</td>
            <td className="td-cadena " >{estado}</td>
            <td className="td-cadena " >{total_presupuesto}</td>
            <td className="td-cadena" >
                <BotonConsultar textoBoton={"Ver"} sw_habilitado={true} eventoClick={() => eventoVer(detalle)} />
            </td>
        </tr>
    </tbody>
)

export default ListadoTabla;