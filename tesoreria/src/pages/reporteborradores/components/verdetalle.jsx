import React from "react";
import { BotonExcel } from "../../../components/Botones";

const ListadoDetalle = ({
    data
}) =>
(<div className="table-responsive" style={{ height: '800px', display: '-ms-flexbox' }}>
    <table className="table table-hover table-sm table-borderer table-striped">
        <thead className="table-secundary text-center" style={{ position: "sticky", top: 0 }}>
            <tr>
                <th colSpan={16}>
                    <BotonExcel textoBoton={"Exportar"} sw_habilitado={data.length > 0 ? true : false} listDatos={data} nombreArchivo={"Detalle de Presupuesto"}/>
                </th>
            </tr>
            <tr>
                <th className="align-middle">Centro Costo</th>
                <th className="align-middle">Cuenta</th>
                <th className="align-middle">Categoria Pre.</th>
                <th className="align-middle">Ene</th>
                <th className="align-middle">Feb</th>
                <th className="align-middle">Mar</th>
                <th className="align-middle">Abr</th>
                <th className="align-middle">May</th>
                <th className="align-middle">Jun</th>
                <th className="align-middle">Jul</th>
                <th className="align-middle">Ago</th>
                <th className="align-middle">Set</th>
                <th className="align-middle">Oct</th>
                <th className="align-middle">Nov</th>
                <th className="align-middle">Dic</th>
                <th className="align-middle">Total</th>

            </tr>
        </thead>
        <tbody>
            {data.map((registrod, index) =>
                <tr id={index}>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.U_MSS_CC2}-{registrod.U_MSS_NC2}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.U_MSS_CUENTA}-{registrod.U_MSS_DESC}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.U_MSS_DETCA}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Enero}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Febrero}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Marzo}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Abril}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Mayo}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Junio}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Julio}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Agosto}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Septiembre}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Octubre}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Noviembre}</td>
                    <td className="align-middle" style={{fontSize:10}}>{registrod.Diciembre}</td>
                    <td className="align-middle" style={{fontSize:10, alignContent:"Right", alignItems:"Rigth"}}>{registrod.Total_Linea}</td>
                </tr>
            )}
        </tbody>
    </table>
</div>
)

export default ListadoDetalle;