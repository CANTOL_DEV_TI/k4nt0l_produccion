import React from "react";
import { ReportePresupuestosX } from "../../services/presupuestoServices";
import ListadoTabla from "./components/tabla";
import { Validar } from "../../services/ValidaSesion";
import VentanaBloqueo from "../../components/VentanaBloqueo";
import VentanaModal from "../../components/VentanaModal";
import ListadoDetalle from "./components/verdetalle";

class ReporteBorradores extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            detalles: [],
            isFetch: false,
            dataVer: null,
            showModal: false,
            tipoOpe: 'Find',
            Cia: '',
            CiaNombre: '',
            Usuario: '',
            bloqueo: false,
            VentanaSeguridad: false
        }
    }

    handleConsultar = async (xCia, xUser) => {
        this.setState({ resultados: [], isFetch: true })
        let xReporte = await ReportePresupuestosX(xCia, xUser)
        this.setState({ resultados: xReporte, isFetch: false })
    }

    handleVerDetalle = async (data) => {
        this.setState({ dataVer: data, tipoOpe: 'ver', showModal: true })
        console.log(data)
    }

    render() {
        const { isFetch, resultados, detalles, showModal, VentanaSeguridad, Cia, Usuario } = this.state
        return (
            <React.Fragment>
                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}
                <div className="table-responsive" style={{ height: '800px', display: '-ms-flexbox' }}>
                    <table className="table table-hover table-sm table-borderer table-striped">
                        <thead className="table-secundary text-center" style={{ position: "sticky", top: 0 }}>
                            <tr>
                                <th className="align-middle">Codigo Borrador</th>
                                <th className="align-middle">Centro de Costo</th>
                                <th className="align-middle">Usuario</th>
                                <th className="align-middle">Estado</th>
                                <th className="align-middle">Total Presupuesto</th>
                                <th className="align-middle"></th>
                            </tr>
                        </thead>
                        {resultados.map((registro, index) =>
                            <ListadoTabla
                                key={index + 1}
                                cia={registro.cia}
                                code={registro.code}
                                usuario={registro.usuario}
                                centro_costo={registro.centro_costo + "-" + registro.cc_desc}
                                estado={registro.estado}
                                detalle={registro.detalle}
                                total_presupuesto={Number(registro.Total).toLocaleString("en-US")}
                                eventoVer={this.handleVerDetalle}
                            />
                        )}
                        <tfoot>

                        </tfoot>
                    </table>
                    <VentanaModal show={this.state.showModal} handleClose={() => this.setState({ showModal: false })} size={"xl"} titulo={"Detallado del Presupuesto"}>
                        <ListadoDetalle data={this.state.dataVer} />
                    </VentanaModal>
                    <VentanaBloqueo
                        show={this.state.VentanaSeguridad}
                    />
                </div>
            </React.Fragment>
        )
    }
    async componentDidMount() {
        const ValSeguridad = await Validar("PREBOR")
        console.log(ValSeguridad)
        this.setState({ VentanaSeguridad: ValSeguridad.Validar, Cia: ValSeguridad.cia, Usuario: ValSeguridad.usuario })
        this.handleConsultar(ValSeguridad.cia, ValSeguridad.usuario)

    }
}

export default ReporteBorradores;