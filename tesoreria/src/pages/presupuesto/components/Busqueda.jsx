import React from "react";
import Title from "../../../components/Titulo";
import { BotonBuscar } from "../../../components/Botones";

class Busqueda extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { handleBusqueda, handleModal, xCia, xCiaCodigo } = this.props

        return (
            <React.Fragment>
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header border border-dashed border-end-0 border-start-0">
                            <div className="row align-items-center gy-3">
                                <div className="col-sm">
                                    <Title>Lista de Presupuestos Asignados : {xCia}</Title>
                                </div>
                                <div>                                    
                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda()} />
                                </div>
                            </div>
                            <div>

                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )

    }
}

export default Busqueda