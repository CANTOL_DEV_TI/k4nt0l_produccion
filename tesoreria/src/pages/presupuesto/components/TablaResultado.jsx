import React, { Component } from "react";
import { AgGridReact } from 'ag-grid-react';
import "ag-grid-community";
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';
import 'ag-grid-community/styles/ag-theme-quartz.css';
import { GroupCellRenderer } from "ag-grid-community";
import 'ag-grid-enterprise';
import DetallePresupuesto from "./Detalle";

class TablaResultados extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            columnDefs: [
                { headerName: "Codigo", field: "Code", cellRenderer: 'agGroupCellRenderer' },
                { headerName: "Ejercicio", field: "U_MSS_EJERCI" },
                { headerName: "Moneda", field: "U_MSS_MONEDA" },
                { headerName: "Estado", field: "Estado" },
                { headerName: "Total Pre.", field: "Total_Presupuestado" },
                { headerName: "Total Eje", field: "Total_Ejecutado" }
            ],
            rowData: this.props.Datos,
            onGridReady: false,
            defColumnsDefs: { flex: 1 }
        }

        const defColumnsDefs = { flex: 1 };
    }
    render() {
        const { Datos } = this.props
        return (
            <div className="ag-theme-quartz"
                style={{
                    height: "100%",
                    width: "100%"
                }}>
                <AgGridReact
                    columnDefs={this.state.columnDefs}
                    rowData={this.props.Datos}
                    defColumnsDefs={{ flex: 1, filter: true }}
                    onGridReady={true}
                    sideBar={false}
                    masterDetail={true}
                    detailRowHeight={700}
                    detailCellRenderer={(props) => <DetallePresupuesto{...props} />}                                                       
                />

            </div>
        )
    }
}
export default TablaResultados