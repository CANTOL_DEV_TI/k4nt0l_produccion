import { AgGridReact } from "ag-grid-react";
import React from "react";

const DetallePresupuesto = (props) => {

    const { data } = props;
    let hoy = new Date;
    let Mes = hoy.getMonth();
    //console.log(Mes)

    let verenero = true;
    let verfebrero = true;
    let vermarzo = true;
    let verabril = true;
    let vermayo = true;
    let verjunio = true;
    let verjulio = true;
    let veragosto = true;
    let verseptiembre = true;
    let veroctubre = true;
    let vernoviembre = true;
    let verdiciembre = true;
    
    
        if (Mes === 0){
           verenero = false;
        }

        if (Mes === 1){
            verfebrero = false;
        }

        if (Mes ===  2){
            vermarzo = false;
        }

        if (Mes ===  3){
            verabril = false;
        }

        if (Mes ===  4){
            vermayo = false;
        }

        if (Mes === 5) {
            verjunio = false;
        }

        if (Mes ===  6){
            verjulio = false;
        }

        if (Mes === 7){
            veragosto = false;
        }

        if (Mes ===  8){
            verseptiembre = false;
        }

        if (Mes ===  9){
            veroctubre = false;
        }

        if (Mes ===  10){
            vernoviembre = false;
        }
        
        if (Mes ===  11){
            verdiciembre = false;
        }

    const columns = [
        { headerName: "Codigo", field: "Code", hide: true, autosize: true },
        { headerName: "Linea", field: "LineId", autosize: true },
        { headerName: "Cuenta", field: "U_MSS_CUENTA", autosize: true },
        { headerName: "Cta Nombre", field: "U_MSS_DESC" },
        { headerName: "C.C. 1", field: "U_MSS_CC1", hide: true },
        { headerName: "C.C. 2", field: "U_MSS_CC2", hide: true },
        { headerName: "Tipo", field: "U_MSS_TIPO" },
        { headerName: "Codigo C.P.", field: "U_MSS_CAPRE" },
        { headerName: "Cate. Pres.", field: "U_MSS_DETCA" },
        { headerName: "Enero P.", field: "U_MSS_ENE", hide: verenero },
        { headerName: "Enero G.", field: "U_MSS_ENE1", hide: verenero },
        { headerName: "Enero B.", field: "U_MSS_ENEG", hide: verenero },
        { headerName: "Febrero P.", field: "U_MSS_FEB", hide: verfebrero },
        { headerName: "Febrero G.", field: "U_MSS_FEB2", hide: verfebrero },
        { headerName: "Febrero B.", field: "U_MSS_FEBG", hide: verfebrero },
        { headerName: "Marzo P.", field: "U_MSS_MAR", hide: vermarzo },
        { headerName: "Marzo G.", field: "U_MSS_MAR2", hide: vermarzo },
        { headerName: "Marzo B.", field: "U_MSS_MARG", hide: vermarzo },
        { headerName: "Abril P.", field: "U_MSS_ABR", hide: verabril },
        { headerName: "Abril G.", field: "U_MSS_ABR2", hide: verabril },
        { headerName: "Abril B.", field: "U_MSS_ABRG", hide: verabril },
        { headerName: "Mayo P.", field: "U_MSS_MAY", hide: vermayo },
        { headerName: "Mayo G.", field: "U_MSS_MAY2", hide: vermayo },
        { headerName: "Mayo B.", field: "U_MSS_MAYG", hide: vermayo },
        { headerName: "Junio P.", field: "U_MSS_JUN", hide: verjunio },
        { headerName: "Junio G.", field: "U_MSS_JUN2", hide: verjunio },
        { headerName: "Junio B.", field: "U_MSS_JUNG", hide: verjunio },
        { headerName: "Julio P.", field: "U_MSS_JUL", hide: verjulio },
        { headerName: "Julio G.", field: "U_MSS_JUL2", hide: verjulio },
        { headerName: "Julio B.", field: "U_MSS_JULG", hide: verjulio },
        { headerName: "Agosto P.", field: "U_MSS_AGO", hide: veragosto },
        { headerName: "Agosto G.", field: "U_MSS_AGO2", hide: veragosto },
        { headerName: "Agosto B.", field: "U_MSS_AGOG", hide: veragosto },
        { headerName: "Septiembre P.", field: "U_MSS_SEP", hide: verseptiembre },
        { headerName: "Septiembre G.", field: "U_MSS_SEP2", hide: verseptiembre },
        { headerName: "Septiembre B.", field: "U_MSS_SEPG", hide: verseptiembre },
        { headerName: "Octubre P.", field: "U_MSS_OCT", hide: veroctubre },
        { headerName: "Octubre G.", field: "U_MSS_OCT2", hide: veroctubre },
        { headerName: "Octubre B.", field: "U_MSS_OCTG", hide: veroctubre },
        { headerName: "Noviembre P.", field: "U_MSS_NOV", hide: vernoviembre },
        { headerName: "Noviembre G.", field: "U_MSS_NOV2", hide: vernoviembre },
        { headerName: "Noviembre B.", field: "U_MSS_NOVG", hide: vernoviembre },
        { headerName: "Diciembre P.", field: "U_MSS_DIC", hide: verdiciembre },
        { headerName: "Diciembre G.", field: "U_MSS_DIC2", hide: verdiciembre },
        { headerName: "Diciembre B.", field: "U_MSS_DICG", hide: verdiciembre }
    ]



    return (

        <div className="ag-theme-quartz"
            style={{
                height: "100%",
                width: "100%"
            }}>
            <AgGridReact
                rowData={data.Detalle}
                columnDefs={columns}
                defaultColDef={{ flex: 1, filter: true, width: 100, autoHeight: true }}
                onGridReady={true}
                //masterDetail={true}                
                detailRowHeight={500}

            />
        </div>
    );

};

export default DetallePresupuesto;
