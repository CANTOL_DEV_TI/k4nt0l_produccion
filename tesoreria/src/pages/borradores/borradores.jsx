import React from "react";
import { Validar } from "../../services/ValidaSesion";
import VentanaBloqueo from "../../components/VentanaBloqueo";
import { ListadoBorradores, AgregarBorrador, ActualizarBorrador, EliminarBorrador, HistoralBorradores } from "../../services/presupuestoServices";
import Busqueda from "../borradores/components/Busqueda";
import ResultadoTabla from "./components/TablaPrincipal";
import ResultadoTablaHistorial from "./components/TablaHistorial";
import { BotonNuevo } from "../../components/Botones";
import BorradorEdicion from "./components/EdicionBorrador";
import VentanaModal from "../../components/VentanaModal";


class Borradores extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            Cia: '',
            CiaNombre: '',
            Usuario: '',
            resultadosHistorial: [],
            resultadosCC : [],
            CC : '',
            showModalH: false,
            showModalCC : false,
            total_enero: 0,
            total_febrero: 0,
            total_marzo: 0,
            total_abril: 0,
            total_mayo: 0,
            total_junio: 0,
            total_julio: 0,
            total_agosto: 0,
            total_septiembre: 0,
            total_octubre: 0,
            total_noviembre: 0,
            total_diciembre: 0
        }
    }

    handleCiaNombre = async (e) => {
        if (e === "CNT") {
            let xNombre = "CANTOL S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "DTM") {
            let xNombre = "DISTRIMAX S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "TCN") {
            let xNombre = "TECNOPRESS S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }
    }

    handleBusqueda = async () => {
        this.setState({ isFetch: "Cargando" })
        const filtros = { "Compañia": this.state.Cia, "Usuario": this.state.Usuario }
        const resultados_lista = await ListadoBorradores(filtros)

        this.setState({
            resultados: resultados_lista, isFetch: ""
        })
    }

    handleVerHistorial = async (e) => {
        const resultados_historial = await HistoralBorradores(this.state.Cia, e.Codigo)
        this.setState({ resultadosHistorial: resultados_historial, showModalH: true })
    }

    handleAtras = async (e) => {
        this.setState({ dataRegistro: null })
        this.setState({ tipoOpe: 'Find' })
    }

    handleNuevoBorrador = async (e) =>{       
        this.setState({tipoOpe: 'nuevo'})
    }

    handleSubmit = async (e) => {
        let valido = true
        //console.log(e)
        if (e.codigo === '') {
            valido = false
            alert("El Codigo no debe ir vacio")
        }

        if (e.año === '') {
            valido = false
            alert("El año no debe ir vacio")
        }

        if (e.detalles.length === 0) {
            this.valido = false
            alert("No puede grabar sin detalles")
        }
        //console.log(this.state.tipoOpe)

        if (valido === true) {

            let mensaje = {}

            if (this.state.tipoOpe === 'nuevo') {    
                //console.log(e)                            
                const responseJson = await AgregarBorrador(e)
                mensaje = responseJson
            }

            if (this.state.tipoOpe === 'actualizar') {
                const responseJson = await ActualizarBorrador(e)
                mensaje = responseJson
            }

            if (this.state.tipoOpe === 'eliminar') {
                if (this.state.dataRegistro.activo === "Migrado") {
                    alert("Lo que se encuentra en estado MIGRADO no se puede eliminar")
                } else {
                    if (confirm("Realmente desea eliminar este borrador de presupuesto")) {
                        const responseJson = await EliminarBorrador(e)
                        mensaje = responseJson
                    }
                }
            }

            this.setState({ showModal: false, tipoOpe: 'Find', dataRegistro: null })

            alert(mensaje.Mensaje)

            this.handleBusqueda()

        }

    }

    render() {
        const { isFetch, resultados, resultadosHistorial, showModal, VentanaSeguridad, Cia, Usuario } = this.state
        return (
            <React.Fragment>
                {this.state.tipoOpe == 'nuevo' || this.state.tipoOpe == 'actualizar' || this.state.tipoOpe == 'eliminar' || this.state.tipoOpe == 'ver' ?
                    <BorradorEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnClick={this.handleSubmit}
                        nameOpe={'Grabar'}
                        showDetalle={false}
                        EventoVolver={this.handleAtras}
                        xCia={this.state.Cia}
                        xUsuario={this.state.Usuario}
                        Estado={this.state.Estado}
                    />
                    :
                    <>
                        <Busqueda xCia={this.state.CiaNombre} handleBusqueda={this.handleBusqueda} />
                        {isFetch && 'Cargando'}
                        {(!isFetch && !resultados.length) && 'Sin Informacion'}
                        <div className="table-responsive" style={{ height: '400px', display: "-ms-flexbox" }}>
                            <table className="table table-hover table-sm table-bordered table-striped" >
                                <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                                    <tr >
                                        <th colSpan={7} ><BotonNuevo textoBoton={"Crear Nuevo"} sw_habilitado={true} eventoClick={this.handleNuevoBorrador} /></th>
                                    </tr>
                                    <tr >
                                        <th className="align-middle" >Codigo Borrador</th>
                                        <th className="align-middle" >Año</th>
                                        <th className="align-middle" >Usuario</th>
                                        <th className="align-middle" >Moneda</th>
                                        <th className="align-middle" >Estado</th>
                                        <th className="align-middle" >Total Presupuesto</th>
                                        <th className="align-middle" >Acciones</th>
                                    </tr>
                                </thead>

                                {resultados.map((registro) =>
                                    <ResultadoTabla
                                        key={registro.Cia}
                                        codigo={registro.Codigo}
                                        año={registro.Año}
                                        usuario={registro.Usuario}
                                        moneda={registro.Moneda}
                                        estado={registro.Estado_d}
                                        total_presupuesto={registro.Total_Presupuesto}
                                        eventoVer={() => this.setState({ dataRegistro: registro, tipoOpe: 'actualizar' })}
                                        eventoEliminar={() => this.setState({ dataRegistro: registro, tipoOpe: 'eliminar' })}
                                        verHistorial={() => this.handleVerHistorial(registro)}
                                    />
                                )}
                                <tfoot>

                                </tfoot>
                            </table>
                            <VentanaModal show={this.state.showModalH} size="xl" handleClose={() => this.setState({ showModalH: false })}>
                                <table className="table table-hover table-sm table-bordered table-striped" >
                                    <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                                        <tr >
                                            <th className="align-middle" >Codigo Borrador</th>
                                            <th className="align-middle" >Codigo Presupuesto</th>
                                            <th className="align-middle" >Año</th>
                                            <th className="align-middle" >Moneda</th>
                                            <th className="align-middle" >Total Presupuesto</th>
                                        </tr>
                                    </thead>
                                    {resultadosHistorial.map((reg) =>
                                        <ResultadoTablaHistorial
                                            key={reg.Code}
                                            cia={reg.Cia}
                                            codigo={reg.Code}
                                            fecha_version={reg.fecha_version}
                                            año={reg.U_MSS_EJERCI}
                                            moneda={reg.U_MSS_MONEDA}
                                            total_presupuesto={reg.U_MSS_TOP}
                                        />
                                    )}
                                </table>
                            </VentanaModal>
                            
                            <VentanaBloqueo
                                show={this.state.VentanaSeguridad}

                            />
                        </div>
                    </>
                }

            </React.Fragment>
        )
    }
    async componentDidMount() {
        const ValSeguridad = await Validar("PREBOR")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar, "Cia": ValSeguridad.cia, "Usuario": ValSeguridad.usuario })
        this.handleCiaNombre(ValSeguridad.cia)
    }
}

export default Borradores;