import React from "react";
import { useEffect,useState } from "react";
import { getCentroCostoSAP } from "../../../services/presupuestoServices";
import { BotonConsultar, BotonSeleccionar } from "../../../components/Botones";

export function BusquedaCentroCostoSAP({pCia, ResultadosBusqueda}) {
    const [ListCC, setListCC] = useState([])

    useEffect(() => {

    })

    const handleFind = async (e) => {
        const responseJson = await getCentroCostoSAP(pCia, e.target.value)
        setListCC(responseJson)
    }

    return(
        <div>
            <div className="card-header cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col col-form-label">
                                Centro de Costo : <input type="text" autoFocus style={{ width: 400 }} name="tCentroC" onChange={handleFind} />
                                <BotonConsultar sw_habilitado={true} eventoClick={() => handleFind()} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="table-responsive">
                                <table className="table table-hover table-sm table-bordered">
                                    <thead className="table-secondary text-center table-sm">
                                        <tr>
                                            <td className="col-sm-auto col-form-label">Centro de Costo</td>
                                            <td className="col-sm-auto col-form-label">Descripcion</td>
                                            <td className="col-sm-auto"></td>
                                        </tr>
                                    </thead>
                                    <tbody className="list">
                                        {ListCC.length > 0 &&
                                            ListCC.map((registro) =>
                                                <tr>
                                                    <td className="td-cadena" id={registro.CC1}>{registro.CC2}</td>
                                                    <td className="td-cadena" >{registro.CC_Desc}</td>
                                                    <td>
                                                        <span className="actions">
                                                            <button onClick={() => ResultadosBusqueda({ "ccosto1": registro.CC1, "ccosto2" : registro.CC2 ,"cc_desc": registro.CC_Desc })}>
                                                                <BotonSeleccionar />
                                                            </button>
                                                        </span>
                                                    </td>
                                                </tr>
                                            )
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}