import React from "react";
import { useEffect,useState } from "react";
import { getCategoriaPresupuestalSAP } from "../../../services/presupuestoServices";
import { BotonConsultar, BotonSeleccionar } from "../../../components/Botones";

export function BusquedaCatPresupuestalSAP({pCia, ResultadosBusqueda, pCuenta}) {
    const [ListCC, setListCC] = useState([])
    const [Busqueda,setBusqueda] = useState(" ")

   

    const handleFind = async (e) => {                
        let texto = ""

        if((e === undefined)||(e === "")||(e.target.value==="")){
            texto = "%20"            
        }else{
            texto = e.target.value
        }

        setBusqueda(texto)

        const responseJson = await getCategoriaPresupuestalSAP(pCia,Busqueda, pCuenta)
        setListCC(responseJson)
    }

    useEffect(() => {
        setBusqueda(" ");
        handleFind(Busqueda);
    },[])

    return(
        <div>
            <div className="card-header cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col col-form-label">
                                Categoria Presupuestal : <input type="text" autoFocus style={{ width: 400 }} name="tCategoria" onChange={handleFind} />
                                <BotonConsultar sw_habilitado={true} eventoClick={() => handleFind()} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="table-responsive">
                                <table className="table table-hover table-sm table-bordered">
                                    <thead className="table-secondary text-center table-sm">
                                        <tr>
                                            <td className="col-sm-auto col-form-label">Categoria</td>
                                            <td className="col-sm-auto col-form-label">Descripcion</td>
                                            <td className="col-sm-auto"></td>
                                        </tr>
                                    </thead>
                                    <tbody className="list">
                                        {ListCC.length > 0 &&
                                            ListCC.map((registro) =>
                                                <tr>
                                                    <td className="td-cadena" id={registro.Cat_Codigo}>{registro.Cat_Codigo}</td>
                                                    <td className="td-cadena" >{registro.Cat_Desc}</td>
                                                    <td>
                                                        <span className="actions">
                                                            <button onClick={() => ResultadosBusqueda({ "cat_codigo": registro.Cat_Codigo, "cat_desc" : registro.Cat_Desc  })}>
                                                                <BotonSeleccionar />
                                                            </button>
                                                        </span>
                                                    </td>
                                                </tr>
                                            )
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}