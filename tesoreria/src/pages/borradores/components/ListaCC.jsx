import {  BsSend } from "react-icons/bs"

export function ListaCentroCosto({ datosRow, xCia, Seleccionar }) {

    return (
        <div className="table-responsive" style={{ height: '400px', display: "-ms-flexbox" }}>
            < table className="table table-hover table-sm table-bordered" >
                <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                    <tr>
                        <td className="align-middle">Centro Costo </td>
                        <td className="align-middle">C.C. Nombre</td>
                        <td className="align-middle"></td>
                    </tr>
                </thead>
                <tbody className="list">
                    {datosRow.map((datos, index) =>
                        <tr key={index + 1}>
                            <td className="td-cadena " >{datos.codigo}</td>
                            <td className="td-cadena " >{datos.nombre}</td>
                            <td><button><BsSend className="baja-btn" data-bs-t onClick={() => Seleccionar(datos)} /></button></td>
                        </tr>
                    )
                    }

                </tbody>
            </table>
        </div>
    )
};