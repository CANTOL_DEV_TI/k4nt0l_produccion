import React from "react";
import { BsReception4 } from "react-icons/bs";
const ResultadoTablaHistorial = ({
    fecha_version,
    cia,
    codigo,
    año,
    usuario,
    moneda,
    estado,
    total_presupuesto,
    eventoRestaurar }) =>
(
    <tbody>
        <tr>
            <td className="td-cadena " id={cia}>{fecha_version}</td>
            <td className="td-cadena " >{codigo}</td>
            <td className="td-cadena " >{año}</td>            
            <td className="td-cadena " >{moneda}</td>            
            <td style={{ textAlign: "right" }} >{Number(total_presupuesto).toLocaleString("en")}</td>            
        </tr>
    </tbody>
)

export default ResultadoTablaHistorial;