import React from "react";
import { useEffect, useState } from "react";
import { getCuentaContableSAP } from "../../../services/presupuestoServices";
import { BotonConsultar, BotonSeleccionar } from "../../../components/Botones";
import Button from 'react-bootstrap/Button';

export function BusquedaCuentasContables({ pCia, ResultadosBusqueda }) {
    const [ListCC, setListCC] = useState([])

    useEffect(() => {

    })

    const handleFind = async (e) => {
        //console.log(pCia)
        const responseJson = await getCuentaContableSAP(pCia, e.target.value)
        console.log(responseJson)
        setListCC(responseJson)
    }

    return (
        <div>
            <div className="card-header cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div style={{ textAlign: "center" }}>Gastos de </div>
                        </div>
                        <div className="form-group row">
                            <div style={{ textAlign: "center" }}>
                                <td> <Button variant="primary">Produccion : 91*</Button> </td>
                                <td> <Button variant="primary">Administrativo : 94*</Button> </td>
                                <td> <Button variant="primary">Ventas : 95* </Button></td>
                                <td> <Button variant="primary">Financieros : 97* </Button></td>
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col col-form-label">
                                Cuenta Contable : <input type="text" autoFocus style={{ width: 400 }} name="tCuentaC" onChange={handleFind} />
                                <BotonConsultar sw_habilitado={true} eventoClick={() => handleFind()} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="table-responsive">
                                <table className="table table-hover table-sm table-bordered">
                                    <thead className="table-secondary text-center table-sm">
                                        <tr>
                                            <td className="col-sm-auto col-form-label">Codigo Cuenta</td>
                                            <td className="col-sm-auto col-form-label">Descripcion Cuenta</td>
                                            <td className="col-sm-auto"></td>
                                        </tr>
                                    </thead>
                                    <tbody className="list">
                                        {ListCC.length > 0 &&
                                            ListCC.map((registro) =>
                                                <tr>
                                                    <td className="td-cadena" id={registro.Cta}>{registro.Cta}</td>
                                                    <td className="td-cadena" >{registro.Cta_Desc}</td>
                                                    <td>
                                                        <span className="actions">
                                                            <button onClick={() => ResultadosBusqueda({ "cuenta": registro.Cta, "cuenta_desc": registro.Cta_Desc })}>
                                                                <BotonSeleccionar />
                                                            </button>
                                                        </span>
                                                    </td>
                                                </tr>
                                            )
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}