import React from "react";

export function ListaAnalisis({ datosRow }) {
    return (
        <div className="table-responsive" style={{ height: '400px', display: "-ms-flexbox" }}>
            < table className="table table-hover table-sm table-bordered" >
                <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                    <tr>
                        <td className="align-middle" style={{ minWidth: '250px' }}>Mensaje</td>
                    </tr>
                </thead>
                <tbody className="list">
                    {datosRow.map((datos, index) =>
                        <tr key={index + 1}>
                            <td className="td-cadena " >{datos.mensaje}</td>
                        </tr>
                    )
                    }
                </tbody>
            </table>
        </div>
    )
};