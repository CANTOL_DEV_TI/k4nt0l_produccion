import React from "react";
import { BotonBuscar,BotonEliminar,BotonHistorial } from "../../../components/Botones";

const tab = <>&nbsp;&nbsp;&nbsp;&nbsp;</>;

const ResultadoTabla = ({                                                
                        codigo,
                        año,
                        usuario,
                        moneda,
                        estado,
                        total_presupuesto,
                        eventoVer,
                        eventoEliminar,
                        verHistorial}) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena " style={{ textAlign: "center" ,width:"35%"}} id={codigo}>{codigo}</td>                
                <td className="td-cadena " >{año}</td>
                <td className="td-cadena " >{usuario}</td>
                <td className="td-cadena " style={{ textAlign: "center" ,width:"5%"}}>{moneda}</td>
                <td className="td-cadena " style={{ textAlign: "center" ,width:"5%"}}>{estado}</td>
                <td style={{ textAlign: "right", width:"10%" }} >{Number(total_presupuesto).toLocaleString('en-US', {minimumFractionDigits: 2})}</td> 
                <td style={{ textAlign: "center" ,width:"25%"}}>
                    <BotonBuscar textoBoton={"ver Detalle"} sw_habilitado={true} eventoClick={() => eventoVer(true)}/>  {tab}
                    <BotonEliminar textoBoton={"Eliminar"} sw_habilitado={true} eventoClick={() => eventoEliminar(true)} />{tab}
                    <BotonHistorial textoBoton={"Historial"} sw_habilitado={true} eventoClick={() => verHistorial(true)} />
                </td>                
            </tr>
        </tbody>        
    )

export default ResultadoTabla;