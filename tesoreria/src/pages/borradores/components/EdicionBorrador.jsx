import React from "react";
import { BotonAtras, BotonExcel, BotonGuardar, BotonAgregarLinea } from "../../../components/Botones";
import { ListaDetalle } from "./TablaListado";
import { getDetalle_Borrador, ValidarImportacion } from "../../../services/presupuestoServices";
import VentanaModal from "../../../components/VentanaModal";
import NuevaLineaBorrador from "./EdicionLineaBorrador";
import { BsFileExcel } from "react-icons/bs";
import Carga_Imagen from "../../../components/Carga";
import { ListaAnalisis } from "./TablaAnalisis";
import * as XLSX from 'xlsx';
import { get_CCXUsuario } from "../../../services/centrocostoServices";
import { ListaCentroCosto } from "./ListaCC";

class BorradorEdicion extends React.Component {
    constructor(props) {
        super(props);

        if ((this.props.dataToEdit === null)||(this.props.dataToEdit === undefined)) {
            //console.log("1")
            this.state = {
                cia: this.props.xCia, codigo: '', año: 0, moneda: 'S/', activo: 'A', total_presupuesto: 0, usuario: this.props.xUsuario, Estado: "B", CC: '', CC_des: '', detalles: [],
                showNL: false, showModalCC: false, resultadosCC: [], formato_excel: [{
                    cia: 'x', codigo: 'x',
                    lineaid: 0, cuenta: "9XXXXXX", cuenta_desc: "XXXXXX", cc1: "CC1", cc2: "CC2", ccosto_desc: "",
                    catpre: "", catpre_desc: "", tipo: "", concepto: "", enero: 0, febrero: 0, marzo: 0, abril: 0, mayo: 0, junio: 0, julio: 0,
                    agosto: 0, septiembre: 0, octubre: 0, noviembre: 0, diciembre: 0
                }], showModalWait: false, showModalResultado: false, Analisis: [], Totales: {
                    total_enero: 0, total_febrero: 0,
                    total_marzo: 0, total_abril: 0, total_mayo: 0, total_junio: 0, total_julio: 0, total_agosto: 0, total_septiembre: 0, total_octubre: 0,
                    total_noviembre: 0, total_diciembre: 0, LineToEdit: null, index : -1
                }
            }
        } else {
            //console.log("2")
            this.state = {
                cia: this.props.dataToEdit.Cia, codigo: this.props.dataToEdit.Codigo, año: this.props.dataToEdit.Año, moneda: this.props.dataToEdit.Moneda, CC: '', CC_des: '',
                activo: this.props.dataToEdit.Estado, usuario: this.props.dataToEdit.Usuario, Estado: this.props.dataToEdit.Estado, total_presupuesto: this.props.dataToEdit.Total_Presupuesto,
                detalles: [], showNL: false, showModalCC: false, resultadosCC: [], formato_excel: [{
                    cia: 'x', codigo: 'x',
                    lineaid: 0, cuenta: "9XXXXXX", cuenta_desc: "XXXXXX", cc1: "CC1", cc2: "CC2", ccosto_desc: "",
                    catpre: "", catpre_desc: "", tipo: "", concepto: "", enero: 0, febrero: 0, marzo: 0, abril: 0, mayo: 0, junio: 0, julio: 0,
                    agosto: 0, septiembre: 0, octubre: 0, noviembre: 0, diciembre: 0
                }], showModalWait: false, showModalResultado: false, Analisis: [], Totales: {
                    total_enero: 0, total_febrero: 0,
                    total_marzo: 0, total_abril: 0, total_mayo: 0, total_junio: 0, total_julio: 0, total_agosto: 0, total_septiembre: 0, total_octubre: 0,
                    total_noviembre: 0, total_diciembre: 0, LineToEdit: null, index : -1
                }
            }
        }

    }

    handleGetCC = async (e) => {
        //console.log(e)
        this.setState({ CC: e.codigo, CC_des: e.nombre, showModalCC: false })
        //console.log(this.state.CC)
        //console.log(this.state.CC_des)

    }

    handleFile = (e) => {
        if (this.state.año != "") {
            let fileTypes = ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocuments.spreadsheetml.sheet', 'text/csv'];
            let selectedFile = e.target.files[0];

            if (selectedFile) {
                if (selectedFile && fileTypes.includes(selectedFile.type)) {
                    //setTypeError(null);
                    let reader = new FileReader();
                    reader.readAsArrayBuffer(selectedFile);
                    reader.onload = (e) => {
                        const workbook = XLSX.read(e.target.result, { type: 'buffer' });
                        const worksheetName = workbook.SheetNames[0];
                        const worksheet = workbook.Sheets[worksheetName];
                        const data = XLSX.utils.sheet_to_json(worksheet);

                        this.setState({ detalles: data });
                        this.handleValidacion(data);
                        /*Calculos */
                        if (data.length > 0) {
                            this.handleCalcularTotalesPie(data)
                        }
                        /* */
                    }
                } else {

                }
            } else {

            }
        }
    }

    handleValidacion = async (e) => {
        this.setState({ showModalWait: true })

        const Validacion = await ValidarImportacion(e)
        //console.log(Validacion)
        if (Validacion.codigo === 0) {
            alert("Se realizo la importación sin conflictos")
        } else {
            this.setState({ showModalResultado: true })
            this.setState({ Analisis: Validacion.resultado })
        }

        this.setState({ showModalWait: false })
    }

    handleChange = (e) => {
        if (e.target.name === 'tcodigo') {
            this.setState({ codigo: e.target.value })
        }

        if (e.target.name === 'taño') {
            this.setState({ año: e.target.value })
        }

        if (e.target.name === 'lmoneda') {
            this.setState({ moneda: e.target.value })
        }
    }

    handleShowNuevaLinea = (e) => {
        if (this.state.año !== "") {
            this.setState({ showNL: !this.state.showNL })
        } else {
            alert("Debes ingresar el año para agregar detalles.")
        }
    }

    handleSubmit = (newRow) => {
        //console.log(newRow)
        if (newRow.linea === -1 /*&& this.state.codigo === ""*/) {
            //console.log("entro 1")
            let items = this.state.detalles

            items = [...items, newRow]            

            this.setState({ showNL: false, detalles: items, LineToEdit : null })

            this.handleCalcularTotalesPie(items)
        } else {
            //console.log("entro 2")

            let itemsA = this.state.detalles            
            let indice = Number(newRow.linea)
            //console.log(indice)            
            itemsA.splice(indice,1)
            itemsA.push(newRow)
            
            this.setState({detalles : itemsA, showNL : false , LineToEdit : null})
        }
    }

    HandleBorrarFila = (targetIndex) => {
        let items = this.state.detalles
        items.splice(targetIndex, 1)
        this.setState({ detalles: items })
        this.handleCalcularTotalesPie(items)
    }

    handleEditarFila = (Registro,indice) => {
        this.setState({ showNL: true, LineToEdit: Registro, index : indice })
    }

    handleCalcularTotalesPie = (pData) => {
        let t_Enero = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.enero), 0);
        let t_Febrero = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.febrero), 0);
        let t_Marzo = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.marzo), 0);
        let t_Abril = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.abril), 0);
        let t_Mayo = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.mayo), 0);
        let t_Junio = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.junio), 0);
        let t_Julio = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.julio), 0);
        let t_Agosto = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.agosto), 0);
        let t_Septiembre = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.setiembre), 0);
        let t_Octubre = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.octubre), 0);
        let t_Noviembre = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.noviembre), 0);
        let t_Diciembre = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.diciembre), 0);

        let dTotales = {
            total_enero: t_Enero, total_febrero: t_Febrero, total_marzo: t_Marzo, total_abril: t_Abril,
            total_mayo: t_Mayo, total_junio: t_Junio, total_julio: t_Julio, total_agosto: t_Agosto, total_septiembre: t_Septiembre, total_octubre: t_Octubre,
            total_noviembre: t_Noviembre, total_diciembre: t_Diciembre
        };

        let t_Total = Number(t_Enero) + Number(t_Febrero) + Number(t_Marzo) + Number(t_Abril) + Number(t_Mayo) + Number(t_Junio) + Number(t_Julio) + Number(t_Agosto) + Number(t_Septiembre) + Number(t_Octubre) + Number(t_Noviembre) + Number(t_Diciembre);

        this.setState({ Totales: dTotales, total_presupuesto: t_Total });
    }

    render() {
        const { Cia, dataToEdit, eventOnClick, nameOpe, EventoVolver, xCia, xUsuario, Estado } = this.props
        const tab = <>&nbsp;&nbsp;&nbsp;&nbsp;</>;
        let ccT = "";
        let cc_desT = this.state.CC_des;
        ccT = this.state.CC
        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label"></div>
                            <div className="col-sm-1 col-form-label">Codigo</div>
                            <div className="col-sm-2 col-form-label">{cc_desT}</div>
                            <div className="col-sm-3 col-form-label">
                                <input type="text" name="tcodigo" style={{ width: 450 }} onChange={this.handleChange} disabled value={this.state.codigo} /></div>
                            <div className="col-sm-5 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label"></div>
                            <div className="col-sm-1 col-form-label">Año</div>
                            <div className="col-sm-1 col-form-label">
                                <select name="taño" id="taño" onChange={this.handleChange} value={this.state.año}>
                                    <option value={""}>----</option>
                                    <option value={2024}>2024</option>
                                    <option value={2025}>2025</option>
                                    <option value={2026}>2026</option>
                                    <option value={2027}>2027</option>
                                    <option value={2028}>2028</option>
                                    <option value={2029}>2029</option>
                                    <option value={2030}>2030</option>
                                    <option value={2031}>2031</option>
                                    <option value={2032}>2032</option>
                                    <option value={2033}>2033</option>
                                    <option value={2034}>2034</option>
                                    <option value={2035}>2035</option>
                                    <option value={2036}>2036</option>
                                    <option value={2037}>2037</option>
                                    <option value={2038}>2038</option>
                                    <option value={2039}>2039</option>
                                    <option value={2040}>2040</option>
                                </select>
                            </div>
                            <div className="col-sm-1 col-form-label">Moneda</div>
                            <div className="col-sm-1 col-form-label">
                                <select name="lmoneda" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.moneda} disabled>
                                    <option value={"S/"}>Soles</option>
                                    <option value={"$"}>Dolares</option>
                                </select>
                            </div>

                            <div className="col-sm-1 col-form-label">Total</div>
                            <div className="col-sm-1 col-form-label">
                                <input style={{ textAlign: "right" }} disabled name="ttotal" onChange={this.handleChange} value={this.state.total_presupuesto.toLocaleString('en-US', { minimumFractionDigits: 2 })} />
                            </div>
                            <div className="col-sm-5 col-form-label"></div>
                        </div>
                        <div className="form-group">
                            <div className="form-group row">
                                <div className="col-sm-3 col-form-label">
                                </div>
                                <div className="col-sm-6 col-form-label">
                                    <BotonAgregarLinea sw_habilitado={(this.state.año != "" && (this.state.Estado == "B" || this.state.Estado == "R")) ? true : false} textoBoton={"Agregar Detalle"} eventoClick={this.handleShowNuevaLinea} />
                                    {tab}
                                    <BotonGuardar sw_habilitado={(this.state.detalles.length > 0 && (this.state.Estado == "B" || this.state.Estado == "R")) ? true : false} texto={nameOpe + " Borrador"}
                                        eventoClick={() => eventOnClick({ "cia": this.state.cia, "code": this.state.codigo, "año": this.state.año, "activo": this.state.activo, "moneda": this.state.moneda, "usuario": this.state.usuario, "usuario_edicion": this.state.usuario, "detalles": this.state.detalles, "cc": this.state.CC })} />
                                    {tab}
                                    <BotonExcel textoBoton={"Formato"} sw_habilitado={true} listDatos={this.state.formato_excel} nombreArchivo={"formato_carga_presupuesto"} />
                                    {tab}
                                    <label htmlFor="file_excel" className="btn btn-success btn-sm" ><i className="align-bottom me-1"><BsFileExcel /></i> Cargar Excel</label>
                                    <input type="file" id="file_excel" name="file_excel" accept=".xlsx, .xls, .csv" style={{ visibility: 'hidden', maxWidth: '1px' }} onChange={this.handleFile} />
                                    {tab}
                                    <BotonAtras sw_habilitado={true} textoBoton={"Atras"} eventoClick={EventoVolver} />
                                </div>
                                <div className="col-sm-5 col-form-label">
                                </div>
                            </div>
                            <ListaDetalle datosRow={this.state.detalles} borrarFilas={this.HandleBorrarFila} xCia={this.state.cia} dTotales={this.state.Totales} EditarFila={this.handleEditarFila} />
                            <VentanaModal show={this.state.showNL} handleClose={() => this.setState({ showNL: false })} size={"lg"} titulo={"Agregar Linea de Borrador Presupuesto"}>
                                <NuevaLineaBorrador gCia={this.state.cia} onSubmit={this.handleSubmit} nameOpe={"agregar"} dataToEdit={this.state.LineToEdit} Indice={this.state.index}/>
                            </VentanaModal>
                            <VentanaModal show={this.state.showModalWait} size={"sm"} titulo={""}>
                                <Carga_Imagen />
                            </VentanaModal>
                            <VentanaModal show={this.state.showModalResultado} handleClose={() => this.setState({ showModalResultado: false })} size={"lg"} titulo={"Resultado"}>
                                <ListaAnalisis datosRow={this.state.Analisis} />
                            </VentanaModal>
                            <VentanaModal size={"xm"} show={this.state.showModalCC} handleClose={() => this.setState({ showModalCC: false })}>
                                <ListaCentroCosto datosRow={this.state.resultadosCC} Seleccionar={this.handleGetCC} />
                            </VentanaModal>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    async componentDidMount() {

        let iFiltro = { Compañia: this.state.cia, Codigo: this.state.codigo }

        let ListadoBorrador = await getDetalle_Borrador(iFiltro)
        this.setState({ detalles: ListadoBorrador })

        this.handleCalcularTotalesPie(ListadoBorrador)

        if (this.props.dataToEdit === null) {
            //console.log("NUEVO!!!!")
            let LCC = await get_CCXUsuario(this.state.cia, this.state.usuario)
            //console.log(LCC)
            //console.log(LCC.length)
            if (LCC.length === 1) {
                LCC.map((datos, index) =>
                    this.setState({ CC: datos.codigo, CC_des: datos.nombre })
                )
            } else {
                //console.log("MAS DE UN CC")
                this.setState({ showModalCC: true, resultadosCC: LCC })
            }
        }

    }
}

export default BorradorEdicion;