import React from "react";
import {GrEdit,GrAmazon,GrCodeSandbox,GrTask} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({code,                        
                        nombre,
                        U_MSS_CUCON,
                        cuenta_nombre,                                                
                        eventoEditar,
                        eventoEliminar}) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena" id={code}>{code}</td>                
                <td className="td-cadena" >{nombre}</td>
                <td className="td-cadena" >{U_MSS_CUCON}</td>                
                <td className="td-cadena" >{cuenta_nombre}</td>
                <td style={{textAlign:"center"}}><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td style={{textAlign:"center"}}><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                
            </tr>
        </tbody>
    )

export default ResultadoTabla;