import React from "react";
import { BotonGuardar } from "../../../components/Botones";
import LoadingOverlay from 'react-loading-overlay';

class CatePresu_Edicion extends React.Component {
    constructor(props) {
        super(props);

        if (this.props.dataToEdit === null) {
            this.state = { Code: '', Name: '', U_MSS_CUCON: '', bloqueo: false }
        } else {
            this.state = { Code: this.props.dataToEdit.Code, Name: this.props.dataToEdit.Name, U_MSS_CUCON: this.props.dataToEdit.U_MSS_CUCON, bloqueo: false }
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'tname') {
            this.setState({ Name: e.target.value.toUpperCase() })
        }

        if (e.target.name === 'tcuenta') {
            this.setState({ U_MSS_CUCON: e.target.value })
        }
    }

    render() {
        const { dataToEdit, eventOnClick, nameOpe, Cia, bloqueo_boton } = this.props;
        return (
            <React.Fragment>
                <LoadingOverlay active={this.state.bloqueo} spinner text="Procesando la operación...">
                    <div className="card cardalign w-50rd">
                        <div className="card-header border border-dashed border-end-0 border-start-0">
                            <div className="form-group">
                                <div className="form-group row">

                                    <div className="col-sm-3 col-form-label">Codigo</div>
                                    <div className="col-sm-3 col-form-label">
                                        <input type="text" name="tcode" className="form-control form-control-sm" onChange={this.handleChange} disabled value={this.state.Code} /></div>
                                    <div className="col-sm-1 col-form-label"></div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-3 col-form-label">Nombre</div>
                                    <div className="col-sm-6 col-form-label">
                                        <input type="text" name="tname" className="form-control form-control-sm" onChange={this.handleChange} autoFocus value={this.state.Name} />
                                    </div>

                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-3 col-form-label">Cuenta Contable</div>
                                    <div className="col-sm-3 col-form-label">
                                        <input type="number" name="tcuenta" className="form-control form-control-sm" onChange={this.handleChange} value={this.state.U_MSS_CUCON} /></div>
                                    <div className="col-sm-1 col-form-label"></div>
                                </div>
                            </div>
                        </div>
                        <BotonGuardar sw_habilitado={true} texto={nameOpe} eventoClick={() => eventOnClick({ "cia": this.props.Cia, "Code": this.state.Code, "Name": this.state.Name, "U_MSS_CUCON": this.state.U_MSS_CUCON })} />
                    </div>
                </LoadingOverlay>
            </React.Fragment>
        )
    }

}

export default CatePresu_Edicion;


