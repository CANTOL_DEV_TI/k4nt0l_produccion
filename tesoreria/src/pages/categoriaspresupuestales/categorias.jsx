import React from "react";
import { saveCategoriaPresupuestal, updateCategoriaPresupuestal, deleteCategoriaPresupuestal, get_CategoriasPre } from "../../services/categoriapresupuestalServices.jsx";
import ResultadoTabla from "./components/listado";
import VentanaModal from "../../components/VentanaModal.jsx"
import VentanaSeguridad from "../../components/VentanaBloqueo.jsx"
import Busqueda from "./components/busqueda.jsx"
import { Validar } from "../../services/ValidaSesion";
import VentanaBloqueo from "../../components/VentanaBloqueo";
import CatePresu_Edicion from "./components/edicion.jsx";
import LoadingOverlay from 'react-loading-overlay';

class CategoriaPresupuestal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: false,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            Cia: '',
            Usuario: '',
            bloqueo: false
        }
    }

    handleBusqueda = async (e) => {
        this.setState({ isFetch: true, bloqueo: true })
        const data = await get_CategoriasPre(this.state.Cia, e)
        //console.log(data)
        this.setState({ isFetch: false, resultados: data, bloqueo: false })
    }

    handleNuevo = async (e) => {
        this.setState({ dataRegistro: null, tipoOpe: 'Nuevo', showModal: true })
    }

    handleSubmit = async (e) => {
        //console.log(e)
        let mensaje = ""

        this.setState({ bloqueo: true })

        if (this.state.tipoOpe === 'Nuevo') {
            this.setState({ showModal: false})
            const guardar = await saveCategoriaPresupuestal(e)
            //console.log(guardar)
            mensaje = guardar
        }

        if (this.state.tipoOpe === 'Actualizar') {
            this.setState({ showModal: false})
            const actualizar = await updateCategoriaPresupuestal(e)
            //console.log(actualizar)
            mensaje = actualizar
        }

        if (this.state.tipoOpe === 'Eliminar') {
            this.setState({ showModal: false})
            const eliminar = await deleteCategoriaPresupuestal(e)
            mensaje = eliminar
        }
        this.setState({ showModal: false, nameOpe: 'Find', bloqueo: false })

        alert(mensaje[0].mensaje)

        this.handleBusqueda("")
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad, Cia, Usuario } = this.state;
        return (
            <LoadingOverlay active={this.state.bloqueo} spinner text="Procesando la operación..." >
                <React.Fragment>
                    <Busqueda handleBusqueda={this.handleBusqueda} handleModal={this.handleNuevo} />
                    {isFetch && 'Cargando'}
                    {(!isFetch && !resultados.length) && 'Sin Informacion'}
                    <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                        <table className="table table-hover table-sm table-bordered">
                            <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                                <tr>
                                    <th className="align-middle">Codigo</th>
                                    <th className="align-middle">Nombre</th>
                                    <th className="align-middle">Cta Cont</th>
                                    <th className="align-middle">Cta Cont Nombre</th>
                                    <th className="align-middle">Editar</th>
                                    <th className="align-middle">Eliminar</th>
                                </tr>
                            </thead>

                            {this.state.resultados.map((registro) =>
                                <ResultadoTabla
                                    key={registro.Code}
                                    code={registro.Code}
                                    nombre={registro.Name}
                                    U_MSS_CUCON={registro.U_MSS_CUCON}
                                    cuenta_nombre={registro.Cuenta_Nombre}
                                    eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                    eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}
                                />
                            )}
                        </table>
                    </div>
                    <VentanaModal show={this.state.showModal} handleClose={() => this.setState({ showModal: false })} size={"xl"} titulo={this.state.tipoOpe}>
                        <CatePresu_Edicion dataToEdit={this.state.dataRegistro} nameOpe={this.state.tipoOpe} Cia={this.state.Cia} eventOnClick={this.handleSubmit} bloqueo_boton={this.state.bloqueo} />
                    </VentanaModal>
                    <VentanaBloqueo show={this.state.VentanaSeguridad} />
                </React.Fragment>
            </LoadingOverlay>
        )
    }
    async componentDidMount() {
        const ValSeguridad = await Validar("PRECAT")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar, "Cia": ValSeguridad.cia, "Usuario": ValSeguridad.usuario })
    }
}

export default CategoriaPresupuestal;