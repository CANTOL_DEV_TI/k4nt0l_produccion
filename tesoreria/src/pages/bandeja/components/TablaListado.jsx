import React from "react";
import { BotonBuscar } from "../../../components/Botones";
import { BsFillTrashFill } from "react-icons/bs"

export function ListaDetalle({ datosRow, borrarFilas, dTotales }) {
    console.log(datosRow)
    return (
        < div className="table-responsive" style={{ height: '400px', display: "-ms-flexbox" }}>
            < table className="table table-hover table-sm table-bordered" >
                <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                    <tr>
                        <td className="align-middle">Linea</td>
                        <td className="align-middle" style={{ minWidth: '50px' }}>Cuenta</td>
                        <td className="align-middle" style={{ minWidth: '100px' }}>Cuenta Desc</td>                        
                        <td className="align-middle">Centro Costo</td>
                        <td className="align-middle">C.C. Nombre</td>
                        <td className="align-middle">Categoria</td>
                        <td className="align-middle" style={{ minWidth: '200px' }}>Cat. Descripcion</td>
                        <td className="align-middle">Tipo</td>
                        <td className="align-middle">Concepto</td>
                        <td className="align-middle">Enero</td>
                        <td className="align-middle">Febrero</td>
                        <td className="align-middle">Marzo</td>
                        <td className="align-middle">Abril</td>
                        <td className="align-middle">Mayo</td>
                        <td className="align-middle">Junio</td>
                        <td className="align-middle">Julio</td>
                        <td className="align-middle">Agosto</td>
                        <td className="align-middle">Setiembre</td>
                        <td className="align-middle">Octubre</td>
                        <td className="align-middle">Noviembre</td>
                        <td className="align-middle">Diciembre</td>
                        <td className="align-middle">Total</td>
                        <td className="align-middle"></td>
                    </tr>
                </thead>
                <tbody className="list">
                    {datosRow.map((datos, index) =>
                        <tr key={index + 1}>
                            <td className="td-cadena " id={datos.codigo}>{datos.lineaid}</td>
                            <td className="td-cadena " >{datos.cuenta}</td>
                            <td className="td-cadena " >{datos.cuenta_desc}</td>                            
                            <td className="td-cadena " >{datos.cc2}</td>
                            <td className="td-cadena " >{datos.ccosto_desc}</td>
                            <td className="td-cadena " >{datos.catpre.length > 6 ? datos.catpre : String(datos.catpre).padStart(6,'0')}</td>
                            <td className="td-cadena " >{datos.catpre_desc}</td>
                            <td className="td-cadena " >{datos.tipo}</td>
                            <td className="td-cadena " >{datos.concepto}</td>
                            <td className="td-cadena " >{datos.enero.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{datos.febrero.toLocaleString('en-US',{minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{datos.marzo.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{datos.abril.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{datos.mayo.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{datos.junio.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{datos.julio.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{datos.agosto.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{datos.setiembre.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{datos.octubre.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{datos.noviembre.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{datos.diciembre.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="btn btn-sm btn-dark" >{(datos.enero + datos.febrero + datos.marzo + datos.abril + datos.mayo + datos.junio +
                                datos.julio + datos.agosto + datos.setiembre + datos.octubre + datos.noviembre + datos.diciembre).toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td><button><BsFillTrashFill className="baja-btn" data-bs-t onClick={() => borrarFilas(index)} /></button></td>
                        </tr>
                    )
                    }

                </tbody>
                <tfoot className="table-secondary text-center table-sm" style={{position:"sticky", bottom:0}}>
                    <tr>
                    <td className="td-cadena " ></td>
                            <td className="td-cadena " ></td>
                            <td className="td-cadena " ></td>                            
                            <td className="td-cadena " ></td>
                            <td className="td-cadena " ></td>
                            <td className="td-cadena " ></td>
                            <td className="td-cadena " ></td>
                            <td className="td-cadena " ></td>
                            <td className="td-cadena " ></td>                                                        
                            <td className="td-cadena " >{dTotales.total_enero.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{dTotales.total_febrero.toLocaleString('en-US',{minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{dTotales.total_marzo.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{dTotales.total_abril.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{dTotales.total_mayo.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{dTotales.total_junio.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{dTotales.total_julio.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{dTotales.total_agosto.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{dTotales.total_septiembre.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{dTotales.total_octubre.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{dTotales.total_noviembre.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                            <td className="td-cadena " >{dTotales.total_diciembre.toLocaleString('en-US', {minimumFractionDigits: 2})}</td>                              
                            <td></td>
                            <td></td>  
                    </tr>
                </tfoot>
            </table>
        </div>
    )
};