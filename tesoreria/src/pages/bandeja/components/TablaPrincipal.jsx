import React from "react";
import { BotonAprobar, BotonBuscar, BotonEliminar, BotonGuardarMasivo } from "../../../components/Botones";

const ResultadoTabla = ({
    Code,
    año,
    usuario,
    estado,
    estado_d,
    total_presupuesto,
    aprobado_por,
    aprobado_el,
    editado_por,
    editado_el,
    rechazado_por,
    rechazado_el,
    eventoVer,
    eventoEditar,
    eventoAprobar,
    eventoRechazar }) =>
(
    <tbody>
        <tr>
            <td className="td-cadena " style={{ textAlign: "center" ,width:"35%"}} id={Code}>{Code}</td>
            <td className="td-cadena " >{año}</td>
            <td className="td-cadena " >{usuario}</td>
            <td className="td-cadena " >{estado_d}</td>
            <td className="td-cadena " >{total_presupuesto}</td>
            <td className="td-cadena " >{aprobado_por}</td>
            <td className="td-cadena " >{aprobado_el}</td>
            <td className="td-cadena " >{editado_por}</td>
            <td className="td-cadena " >{editado_el}</td>
            <td className="td-cadena " >{rechazado_por}</td>
            <td className="td-cadena " >{rechazado_el}</td>
            <td>
                <BotonBuscar textoBoton={"Ver Detalle"} sw_habilitado={true} eventoClick={() => eventoVer(true)} />
            </td>
            <td>
                <BotonGuardarMasivo texto={"Edicion"} sw_habilitado={estado != 'B' ? false : true} eventoClick={() => eventoEditar(true)} />
            </td>
            <td>
                <BotonAprobar textoBoton={"Aprobar"} sw_habilitado={estado != 'B' ? false : true} eventoClick={() => eventoAprobar(true)} />

            </td>
            <td>
                <BotonEliminar textoBoton={"Rechazar"} sw_habilitado={estado != 'B' ? false : true} eventoClick={() => eventoRechazar(true)} />
            </td>
        </tr>
    </tbody>
)

export default ResultadoTabla;