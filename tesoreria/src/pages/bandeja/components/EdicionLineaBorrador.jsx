import React from "react";
import { BotonAdd, BotonBuscar, BotonGuardar, BotonGuardarMasivo } from "../../../components/Botones";
import VentanaModal from "../../../components/VentanaModal";
import { BusquedaCuentasContables } from "./BusquedaCuentaContableSAP";
import { BusquedaCentroCostoSAP } from "./BusquedaCentroCostoSAP";
import { BusquedaCatPresupuestalSAP } from "./BusquedaCatPresupuestal";

class NuevaLineaBorrador extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props.gCia)
        this.state = {
            cia: this.props.gCia, codigo: "", linea: 0, cuenta: "", cuenta_desc: "", cc1: "", cc2: "", ccosto_desc: "", concepto : "TERCEROS",
            catpre: "", catpre_desc: "", tipo: "G", enero: 0, febrero: 0, marzo: 0, abril: 0, mayo: 0,
            junio: 0, julio: 0, agosto: 0, setiembre: 0, octubre: 0, noviembre: 0, diciembre: 0,
            showModalCtaC: false, showModalCCos: false, showModalCatP: false
        }

    }

    validateForm = () => {
        if (this.state.cuenta && this.state.cuenta_desc && this.state.cc1 && this.state.cc2 && this.state.catpre && this.state.catpre_desc && this.state.enero && this.state.febrero && this.state.marzo && this.state.abril && this.state.mayo && this.state.junio && this.state.julio && this.state.agosto && this.state.setiembre && this.state.octubre && this.state.noviembre && this.state.diciembre) {
            return true;
        } else {
            return false;
        }
    }

    handleSubmit = (e) => {
        //e.preventDefault();
        if (!this.validateForm) return;

        this.props.onSubmit(this.state);
    }

    handleChange = (e) => {
        if (e.target.name === "tcuenta") {
            this.setState({ cuenta: e.target.value })
        }

        if (e.target.name === "tcuenta_desc") {
            this.setState({ cuenta_desc: e.target.value })
        }

        if (e.target.name === "tccosto1") {
            this.setState({ cc1: e.target.value })
        }

        if (e.target.name === "tccosto2") {
            this.setState({ cc2: e.target.value })
        }

        if (e.target.name === "tcatpre") {
            this.setState({ catpre: e.target.value })
        }

        if (e.target.name === "tcatpre_desc") {
            this.setState({ catpre_desc: e.target.value })
        }

        if (e.target.name === "tenero") {
            this.setState({ enero: e.target.value })
        }

        if (e.target.name === "tfebrero") {
            this.setState({ febrero: e.target.value })
        }

        if (e.target.name === "tmarzo") {
            this.setState({ marzo: e.target.value })
        }

        if (e.target.name === "tabril") {
            this.setState({ abril: e.target.value })
        }

        if (e.target.name === "tmayo") {
            this.setState({ mayo: e.target.value })
        }

        if (e.target.name === "tjunio") {
            this.setState({ junio: e.target.value })
        }

        if (e.target.name === "tjulio") {
            this.setState({ julio: e.target.value })
        }

        if (e.target.name === "tagosto") {
            this.setState({ agosto: e.target.value })
        }

        if (e.target.name === "tsetiembre") {
            this.setState({ setiembre: e.target.value })
        }

        if (e.target.name === "toctubre") {
            this.setState({ octubre: e.target.value })
        }

        if (e.target.name === "tnoviembre") {
            this.setState({ noviembre: e.target.value })
        }

        if (e.target.name === "tdiciembre") {
            this.setState({ diciembre: e.target.value })
        }

        if (e.target.name === "lconcepto"){
            this.setState({ concepto : e.target.value })
        }
    }

    handleClose = (e) => {
        this.props.closeDetalle = false
    }

    handleVerListaCuentas = (e) => {
        //console.log(this.props.gCia)
        this.setState({ showModalCtaC: true })
    }

    handleVerListaCentroCosto = (e) => {
        //console.log(this.props.gCia)
        this.setState({ showModalCCos: true })
    }

    handleVerListaCategoriaPre = (e) => {
        //console.log(this.props.gCia)
        this.setState({ Cia: this.props.gCia })
        this.setState({ showModalCatP: true })
    }

    handleSetCuentaContable = (e) => {
        this.setState({ cuenta: e.cuenta })
        this.setState({ cuenta_desc: e.cuenta_desc })
        this.setState({ showModalCtaC: false })
    }

    handleSetCentroCosto = (e) => {
        this.setState({ cc1: e.ccosto1 })
        this.setState({ cc2: e.ccosto2 })
        this.setState({ ccosto_desc: e.cc_desc })
        this.setState({ showModalCCos: false })
    }

    handleSetCategoriaPresupuestal = (e) => {
        this.setState({ catpre: e.cat_codigo })
        this.setState({ catpre_desc: e.cat_desc })
        this.setState({ showModalCatP: false })
    }

    handleReply = (e) => {
        if (confirm("Desea Copiar el monto a los demas meses?") == true) {
            this.setState({ febrero: this.state.enero })
            this.setState({ marzo: this.state.enero })
            this.setState({ abril: this.state.enero })
            this.setState({ mayo: this.state.enero })
            this.setState({ junio: this.state.enero })
            this.setState({ julio: this.state.enero })
            this.setState({ agosto: this.state.enero })
            this.setState({ setiembre: this.state.enero })
            this.setState({ octubre: this.state.enero })
            this.setState({ noviembre: this.state.enero })
            this.setState({ diciembre: this.state.enero })
        }
    }

    render() {
        const { dataToEdit, onSubmit, nameOpe, gCia } = this.props

        return (
            <div className="card cardalign w-75rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Cuenta</div>
                            <div className="col-sm-4 col-form-label">
                                <input className="form-control form-control-sm" id="tcuenta" name="tcuenta" type="text" placeholder="Seleccione nro cuenta" disabled onChange={this.handleChange} value={this.state.cuenta} />
                            </div>
                            <div className="col-sm-2 col-form-label">
                                <BotonBuscar sw_habilitado={true} eventoClick={this.handleVerListaCuentas} />
                                <VentanaModal show={this.state.showModalCtaC} handleClose={() => this.setState({ showModalCtaC: false })}>
                                    <BusquedaCuentasContables pCia={this.state.cia} ResultadosBusqueda={this.handleSetCuentaContable} />
                                </VentanaModal>
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Cuenta Desc.</div>
                            <div className="col-sm-6 col-form-label"><input className="form-control form-control-sm" id="tcuenta_desc" name="tcuenta_desc" type="text" disabled placeholder="nombre de la cuenta" onChange={this.handleChange} value={this.state.cuenta_desc} /></div>

                        </div>
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Centro de Costo</div>
                            <div className="col-sm-4 col-form-label">
                                <input className="form-control form-control-sm" id="tccosto2" name="tccosto2" type="text" placeholder="Seleccione centro costo..." disabled onChange={this.handleChange} value={this.state.cc2} />

                            </div>
                            <div className="col-sm-2 col-form-label">
                                <BotonBuscar sw_habilitado={true} eventoClick={this.handleVerListaCentroCosto} />
                                <VentanaModal show={this.state.showModalCCos} handleClose={() => this.setState({ showModalCCos: false })}>
                                    <BusquedaCentroCostoSAP pCia={this.state.cia} ResultadosBusqueda={this.handleSetCentroCosto} />
                                </VentanaModal>
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Cuenta Desc.</div>
                            <div className="col-sm-6 col-form-label">
                                <input className="form-control form-control-sm" id="tccosto_desc" name="tccosto_desc" type="text" placeholder="Centro Costo Descripcion..." disabled onChange={this.handleChange} value={this.state.ccosto_desc} />
                            </div>

                        </div>
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Categoria Presupuestal</div>
                            <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tcatpre" name="tcatpre" type="text" disabled placeholder="Seleccione Codigo Presupuestal" onChange={this.handleChange} value={this.state.catpre} /></div>
                            <div className="col-sm-2 col-form-label">
                                <BotonBuscar sw_habilitado={true} eventoClick={this.handleVerListaCategoriaPre} />
                                <VentanaModal show={this.state.showModalCatP} handleClose={() => this.setState({ showModalCatP: false })}>
                                    <BusquedaCatPresupuestalSAP pCia={this.props.gCia} ResultadosBusqueda={this.handleSetCategoriaPresupuestal} pCuenta={this.state.cuenta} />
                                </VentanaModal>
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Cat. Presupuestal Desc.</div>

                            <div className="col-sm-6 col-form-label"><input className="form-control form-control-sm" id="tcatpre_desc" name="tcatpre_desc" type="text" disabled placeholder="Codigo Presupuestal Descripcion" onChange={this.handleChange} value={this.state.catpre_desc} /></div>

                        </div>
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Concepto</div>

                            <div className="col-sm-6 col-form-label">
                                <select className="form-control form-control-sm" id="lconcepto" name="lconcepto" onChange={this.handleChange} value={this.state.concepto}>
                                    <option value={"TERCEROS"}>TERCEROS</option>
                                    <option value={"PERSONAL"}>PERSONAL</option>
                                    <option value={"FINANCIEROS"}>FINANCIEROS</option>
                                    <option value={"GESTION"}>GESTION</option>
                                    <option value={"DEPRECIACION"}>DEPRECIACION</option>
                                    <option value={"TRIBUTOS"}>TRIBUTOS</option>
                                </select>                                
                            </div>

                        </div>
                        <div className="form-group row">
                            <div className="form-group">
                                <div className="form-group row">
                                    <div className="col-sm-4 col-form-label"> Enero </div>
                                    <div className="col-sm-4 col-form-label"> Febrero </div>
                                    <div className="col-sm-4 col-form-label"> Marzo </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tenero" name="tenero" type="number" placeholder="Enero..." value={this.state.enero} onChange={this.handleChange} /> </div>
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tfebrero" name="tfebrero" type="number" placeholder="Febrero..." value={this.state.febrero} onChange={this.handleChange} />  </div>
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tmarzo" name="tmarzo" type="number" placeholder="Marzo..." value={this.state.marzo} onChange={this.handleChange} />  </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-4 col-form-label"> Abril </div>
                                    <div className="col-sm-4 col-form-label"> Mayo </div>
                                    <div className="col-sm-4 col-form-label"> Junio </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tabril" name="tabril" type="number" placeholder="Abril..." value={this.state.abril} onChange={this.handleChange} /> </div>
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tmayo" name="tmayo" type="number" placeholder="Mayo..." value={this.state.mayo} onChange={this.handleChange} />  </div>
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tjunio" name="tjunio" type="number" placeholder="Junio..." value={this.state.junio} onChange={this.handleChange} />  </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-4 col-form-label"> Julio </div>
                                    <div className="col-sm-4 col-form-label"> Agosto </div>
                                    <div className="col-sm-4 col-form-label"> Setiembre </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tjulio" name="tjulio" type="number" placeholder="Julio..." value={this.state.julio} onChange={this.handleChange} /> </div>
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tagosto" name="tagosto" type="number" placeholder="Agosto..." value={this.state.agosto} onChange={this.handleChange} />  </div>
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tsetiembre" name="tsetiembre" type="number" placeholder="Setiembre..." value={this.state.setiembre} onChange={this.handleChange} />  </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-4 col-form-label"> Octubre </div>
                                    <div className="col-sm-4 col-form-label"> Noviembre </div>
                                    <div className="col-sm-4 col-form-label"> Diciembre </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="toctubre" name="toctubre" type="number" placeholder="Octubre..." value={this.state.octubre} onChange={this.handleChange} /> </div>
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tnoviembre" name="tnoviembre" type="number" placeholder="Noviembre..." value={this.state.noviembre} onChange={this.handleChange} />  </div>
                                    <div className="col-sm-4 col-form-label"><input className="form-control form-control-sm" id="tdiciembre" name="tdiciembre" type="number" placeholder="Diciembre..." value={this.state.diciembre} onChange={this.handleChange} />  </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <BotonAdd sw_habilitado={true} textoBoton={"Replicar el monto de Enero a todos los meses"} eventoClick={this.handleReply}></BotonAdd>
                <BotonGuardar sw_habilitado={true} texto={"Agregar Linea Borrador"} eventoClick={this.handleSubmit}></BotonGuardar>
            </div>
        )
    }

}

export default NuevaLineaBorrador;