import React from "react";
import Title from "../../../components/Titulo";
import { BotonBuscar } from "../../../components/Botones";

class Busqueda extends React.Component {
    constructor(props) {
        super(props);
        this.state = { pCia: "", pCode: "", pEjercicio: 0, pUsuario: "" }
        console.log(this.state)
    }

    handleSet = (e) => {
        if (e.target.name === "tCode") {
            this.setState({ pCode: e.target.value })
        }

        if (e.target.name === "sAño") {
            this.setState({ pEjercicio: e.target.value })
        }

        if (e.target.name === "tUsuario") {
            this.setState({ pUsuario: e.target.value })            
        }
    }

    render() {
        const { handleBusqueda, handleModal, xCia, xCiaCodigo } = this.props

        return (
            <React.Fragment>
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header border border-dashed border-end-0 border-start-0">
                            <div className="row align-items-center gy-3">
                                <div className="col-sm">
                                    <Title>Bandeja de Borradores : {xCia} </Title>
                                </div>
                                <div className="col-sm-auto">
                                    <div className="d-flex flex-wrap gap-1">
                                        
                                        Año : 
                                        <select name="sAño" id = "sAño" onChange={this.handleSet} value={this.state.pEjercicio}>
                                            <option value={2024} color="navy">2024</option>
                                            <option value={2025}>2025</option>
                                            <option value={2026}>2026</option>
                                            <option value={2027}>2027</option>
                                            <option value={2028}>2028</option>
                                            <option value={2029}>2029</option>
                                            <option value={2030}>2030</option>
                                            <option value={2031}>2031</option>
                                            <option value={2032}>2032</option>
                                            <option value={2033}>2033</option>
                                            <option value={2034}>2034</option>
                                            <option value={2035}>2035</option>
                                            <option value={2036}>2036</option>
                                            <option value={2037}>2037</option>
                                            <option value={2038}>2038</option>
                                            <option value={2039}>2039</option>
                                            <option value={2040}>2040</option>
                                        </select>
                                        Codigo : <input name="tCode" id="tCode" onChange={this.handleSet} value={this.state.pCode} />
                                        Usuario : <input name="tUsuario" id="tUsuario" onChange={this.handleSet} value={this.state.pUsuario} />
                                        <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda(this.state)} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )

    }
    async componentDidMount() {
        console.log(this.props.xCiaCodigo)
    }
}

export default Busqueda