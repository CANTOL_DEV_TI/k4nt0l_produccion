import React from "react";
import { BotonAdd, BotonAgregarLinea, BotonAtras, BotonGuardar } from "../../../components/Botones";
import { ListaDetalle } from "./TablaListado";
import { getDetalle_Borrador } from "../../../services/presupuestoServices";
import VentanaModal from "../../../components/VentanaModal";
import NuevaLineaBorrador from "./EdicionLineaBorrador";
import { Validar } from "../../../services/ValidaSesion";

class BorradorEdicionBandeja extends React.Component {
    constructor(props) {
        super(props);

        //console.log(this.props.xCia)

        //console.log(this.props.dataToEdit)

        if (this.props.dataToEdit === null) {
            this.state = {
                cia: this.props.xCia, codigo: '', año: 0, moneda: 'S/', activo: 'A', total_presupuesto: 0, usuario: this.props.xUsuario, detalles: [], showNL: false, edita: this.props.nameOpe, Totales: {
                    total_enero: 0, total_febrero: 0,
                    total_marzo: 0, total_abril: 0, total_mayo: 0, total_junio: 0, total_julio: 0, total_agosto: 0, total_septiembre: 0, total_octubre: 0,
                    total_noviembre: 0, total_diciembre: 0
                }
            }
        } else {
            this.state = {
                cia: this.props.dataToEdit.Cia, codigo: this.props.dataToEdit.Code, año: this.props.dataToEdit.Año, moneda: this.props.dataToEdit.Moneda, activo: this.props.dataToEdit.Estado, usuario: this.props.dataToEdit.Usuario, total_presupuesto: this.props.dataToEdit.Total_Presupuesto, detalles: [], showNL: false, edita: this.props.nameOpe, usuario_edita: "", Totales: {
                    total_enero: 0, total_febrero: 0, total_marzo: 0, total_abril: 0, total_mayo: 0, total_junio: 0,
                    total_julio: 0, total_agosto: 0, total_septiembre: 0, total_octubre: 0, total_noviembre: 0, total_diciembre: 0
                }
            }
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'tcodigo') {
            this.setState({ codigo: e.target.value })
        }

        if (e.target.name === 'taño') {
            this.setState({ año: e.target.value })
        }

        if (e.target.name === 'lmoneda') {
            this.setState({ moneda: e.target.value })
        }
    }

    handleShowNuevaLinea = (e) => {
        if (this.state.año !== "") {
            this.setState({ showNL: !this.state.showNL })
        } else {
            alert("Debes ingresar el año para agregar detalles.")
        }
    }

    handleSubmit = (newRow) => {
        let items = this.state.detalles

        items = [...items, newRow]

        //this.setState({ showNL: false, detalles: [...this.state.detalles, newRow] })
        this.setState({ showNL: false, detalles: items })

        this.handleCalcularTotalesPie(items)
    }

    HandleBorrarFila = (targetIndex) => {
        const items = this.state.detalles
        items.splice(targetIndex, 1)
        this.setState({ detalles: items })
        this.handleCalcularTotalesPie(items)
    }

    handleCalcularTotalesPie = (pData) => {
        let t_Enero = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.enero), 0);
        let t_Febrero = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.febrero), 0);
        let t_Marzo = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.marzo), 0);
        let t_Abril = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.abril), 0);
        let t_Mayo = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.mayo), 0);
        let t_Junio = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.junio), 0);
        let t_Julio = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.julio), 0);
        let t_Agosto = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.agosto), 0);
        let t_Septiembre = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.setiembre), 0);
        let t_Octubre = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.octubre), 0);
        let t_Noviembre = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.noviembre), 0);
        let t_Diciembre = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.diciembre), 0);

        let dTotales = {
            total_enero: t_Enero, total_febrero: t_Febrero, total_marzo: t_Marzo, total_abril: t_Abril,
            total_mayo: t_Mayo, total_junio: t_Junio, total_julio: t_Julio, total_agosto: t_Agosto, total_septiembre: t_Septiembre, total_octubre: t_Octubre,
            total_noviembre: t_Noviembre, total_diciembre: t_Diciembre
        };
        let t_Total = Number(t_Enero) + Number(t_Febrero) + Number(t_Marzo) + Number(t_Abril) + Number(t_Mayo) + Number(t_Junio) + Number(t_Julio) + Number(t_Agosto) + Number(t_Septiembre) + Number(t_Octubre) + Number(t_Noviembre) + Number(t_Diciembre);

        this.setState({ Totales: dTotales, total_presupuesto: t_Total });
    }

    render() {
        const { Cia, dataToEdit, eventOnClick, nameOpe, EventoVolver, xCia, xUsuario } = this.props
        const tab = <>&nbsp;&nbsp;&nbsp;&nbsp;</>;
        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label"></div>
                            <div className="col-sm-2 col-form-label">Codigo</div>
                            <div className="col-sm-3 col-form-label"><input type="text" name="tcodigo" style={{ width: 600 }} onChange={this.handleChange} disabled value={this.state.codigo} /></div>
                            <div className="col-sm-3 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label"></div>
                            <div className="col-sm-1 col-form-label">Año</div>
                            <div className="col-sm-1 col-form-label">
                                <select name="taño" id="taño" onChange={this.handleChange} value={this.state.año}>
                                    <option value={""}>----</option>
                                    <option value={2024}>2024</option>
                                    <option value={2025}>2025</option>
                                    <option value={2026}>2026</option>
                                    <option value={2027}>2027</option>
                                    <option value={2028}>2028</option>
                                    <option value={2029}>2029</option>
                                    <option value={2030}>2030</option>
                                    <option value={2031}>2031</option>
                                    <option value={2032}>2032</option>
                                    <option value={2033}>2033</option>
                                    <option value={2034}>2034</option>
                                    <option value={2035}>2035</option>
                                    <option value={2036}>2036</option>
                                    <option value={2037}>2037</option>
                                    <option value={2038}>2038</option>
                                    <option value={2039}>2039</option>
                                    <option value={2040}>2040</option>
                                </select>
                            </div>
                            <div className="col-sm-1 col-form-label">Moneda</div>
                            <div className="col-sm-1 col-form-label">
                                <select name="lmoneda" onChange={this.handleChange} value={this.state.moneda} disabled>
                                    <option value={"S/"}>Soles</option>
                                    <option value={"$"}>Dolares</option>
                                </select>
                            </div>
                            <div className="col-sm-1 col-form-label">Total</div>
                            <div className="col-sm-1 col-form-label"><input style={{ textAlign: "right" }} disabled name="ttotal" onChange={this.handleChange} value={this.state.total_presupuesto.toLocaleString('en')} /></div>
                            <div className="col-sm-5 col-form-label"></div>
                        </div>
                        <div className="form-group">
                            <div className="form-group row">
                                <div className="col-sm-4 col-form-label">
                                </div>
                                <div className="col-sm-5 col-form-label">
                                    <BotonAgregarLinea sw_habilitado={this.props.nameOpe == "ver" ? false : true} textoBoton={"Agregar Detalle"} eventoClick={this.handleShowNuevaLinea} />
                                    {tab}
                                    <BotonGuardar sw_habilitado={this.props.nameOpe == "ver" ? false : true} texto={"Guardar Borrador"}
                                        eventoClick={() => eventOnClick({ "cia": this.state.cia, "code": this.state.codigo, "año": this.state.año, "activo": this.state.activo, "moneda": this.state.moneda, "usuario": this.state.usuario, "usuario_edicion": this.state.usuario_edita, "detalles": this.state.detalles })} />
                                    {tab}
                                    <BotonAtras sw_habilitado={true} textoBoton={"Atras"} eventoClick={EventoVolver} />
                                </div>
                                <div className="col-sm-1 col-form-label">
                                </div>
                            </div>
                            <ListaDetalle datosRow={this.state.detalles} borrarFilas={this.HandleBorrarFila} dTotales={this.state.Totales} />
                            <VentanaModal show={this.state.showNL} handleClose={() => this.setState({ showNL: false })} size={"lg"} titulo={"Agregar Linea de Borrador Presupuesto"}>
                                <NuevaLineaBorrador gCia={this.state.cia} onSubmit={this.handleSubmit} nameOpe={"agregar"} />
                            </VentanaModal>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    async componentDidMount() {

        let iFiltro = { Compañia: this.state.cia, Codigo: this.state.codigo }

        const ListadoBorrador = await getDetalle_Borrador(iFiltro)
        this.setState({ detalles: ListadoBorrador })
        this.handleCalcularTotalesPie(ListadoBorrador)

        const ValSeguridad = await Validar("PREBAN")
        this.setState({ usuario_edita: ValSeguridad.usuario })
        console.log(ValSeguridad.usuario)

    }
}

export default BorradorEdicionBandeja;