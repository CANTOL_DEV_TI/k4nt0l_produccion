import React from "react";
import { Validar } from "../../services/ValidaSesion";
import VentanaBloqueo from "../../components/VentanaBloqueo";
import ResultadoTabla from "./components/TablaPrincipal";
import { getBandejaBorradores, ActualizarBorrador, AdministraBorrador, MigrarSAP } from "../../services/presupuestoServices";
import Busqueda from "./components/Busqueda";
import BorradorEdicionBandeja from "./components/EdicionBorradorA";
import LoadingOverlay from 'react-loading-overlay';

class BandejaBorradores extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: false,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            Cia: '',
            CiaNombre: '',
            Usuario: '',
            bloqueo: false
        }
    }

    handleCiaNombre = async (e) => {
        if (e === "CNT") {
            let xNombre = "CANTOL S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "DTM") {
            let xNombre = "DISTRIMAX S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "TCN") {
            let xNombre = "TECNOPRESS S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }
    }

    handleBusquedaBorradores = async (e) => {
        this.setState({ isFetch: true })
        console.log(e)
        const Result = await getBandejaBorradores(this.state.Cia, e.pCode, e.pEjercicio, e.pUsuario)
        console.log(Result)
        this.setState({ resultados: Result, isFetch: false })
    }

    handleAprobar = async (e) => {
        this.setState({ tipoOpe: "aprobar" })
        let mensaje = {}

        if (confirm("El presupuesto sera aprobado, desea continuar?")) {
            this.setState({ Bloqueo: true })
            let dManeja = { "cia": this.state.Cia, "code": e.Code, "estado": "A", "usuario_aprueba": this.state.Usuario }
            const responseJson = await AdministraBorrador(dManeja)
            mensaje = responseJson
            let dPres = { "cia": this.state.Cia, "code": e.Code }
            const responseJsonM = await MigrarSAP(dPres)
            migrado = responseJsonM
            alert(mensaje.Mensaje)
            this.setState({ Bloqueo: false })
            this.handleBusquedaBorradores()
        }

    }

    handleRechazar = async (e) => {
        this.setState({ tipoOpe: "rechazar" })
        let mensaje = {}
        if (confirm("El presupuesto sera rechazado, desea continuar?")) {
            let dManeja = { "cia": this.state.Cia, "code": e.Code, "estado": "R", "usuario_rechaza": this.state.Usuario }
            const responseJson = await AdministraBorrador(dManeja)
            mensaje = responseJson

            alert(mensaje.Mensaje)
            this.handleBusquedaBorradores()
        }
    }

    handleSubmit = async (e) => {
        let valido = true
        console.log(e)
        if (e.codigo === '') {
            valido = false
            alert("El Codigo no debe ir vacio")
        }

        if (e.año === '') {
            valido = false
            alert("El año no debe ir vacio")
        }

        if (e.detalles.length === 0) {
            this.valido = false
            alert("No puede grabar sin detalles")
        }

        if (valido === true) {
            let mensaje = {}

            if (this.state.tipoOpe === 'editar') {
                const responseJson = await ActualizarBorrador(e)
                mensaje = responseJson
            }

            if (this.state.tipoOpe === 'migrar') {

                const responseJson = await MigrarSAP(e)
                mensaje = responseJson
            }

            this.setState({ showModal: false, tipoOpe: 'Find' })
            console.log(mensaje)
            alert(mensaje.Mensaje)
            this.handleBusquedaBorradores()
        }

    }

    handleAtras = async (e) => {
        this.setState({ dataRegistro: null })
        this.setState({ tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad, Cia, Usuario } = this.state
        return (
            <LoadingOverlay active={this.state.bloqueo} spinner text="Procesando la operación..." >
                <React.Fragment>
                    {this.state.tipoOpe == 'ver' || this.state.tipoOpe == 'editar' ?
                        <BorradorEdicionBandeja
                            dataToEdit={this.state.dataRegistro}
                            eventOnClick={this.handleSubmit}
                            nameOpe={this.state.tipoOpe == 'ver' ? 'ver' : 'Grabar'}
                            showDetalle={false}
                            EventoVolver={this.handleAtras}
                            xCia={this.state.Cia}
                            xUsuario={this.state.Usuario}
                        />
                        :
                        <>
                            <Busqueda xCiaCodigo={this.state.Cia} xCia={this.state.CiaNombre} handleBusqueda={this.handleBusquedaBorradores} />
                            {isFetch && 'Cargando'}
                            {(!isFetch && !resultados.length) && 'Sin Información'}
                            <div className="table-responsive" style={{ height: '800px', display: '-ms-flexbox' }}>
                                <table className="table table-hover table-sm table-borderer table-striped">
                                    <thead className="table-secundary text-center" style={{ position: "sticky", top: 0 }}>
                                        <tr>
                                            <th colSpan={21}>

                                            </th>
                                        </tr>
                                        <tr>
                                            <th className="align-middle">Codigo Borrador</th>
                                            <th className="align-middle">Año</th>
                                            <th className="align-middle">Usuario</th>
                                            <th className="align-middle">Estado</th>
                                            <th className="align-middle">Total Presupuesto</th>
                                            <th className="align-middle">Aprobado por</th>
                                            <th className="align-middle">Aprobado el</th>
                                            <th className="align-middle">Editado por</th>
                                            <th className="align-middle">Editado el</th>
                                            <th className="align-middle">Rechazado por</th>
                                            <th className="align-middle">Rechazado el</th>
                                            <th className="align-middle"></th>
                                            <th className="align-middle"></th>
                                            <th className="align-middle"></th>
                                            <th className="align-middle"></th>
                                        </tr>
                                    </thead>
                                    {resultados.map((registro) =>
                                        <ResultadoTabla
                                            key={registro.Cia}
                                            Code={registro.Code}
                                            año={registro.Año}
                                            usuario={registro.Usuario}
                                            estado={registro.Estado}
                                            estado_d={registro.Estado_d}
                                            total_presupuesto={registro.Total_Presupuesto.toLocaleString("en-US")}
                                            aprobado_por={registro.Aprobado_por}
                                            aprobado_el={registro.Aprobado_el}
                                            editado_por={registro.Editado_por}
                                            editado_el={registro.Editado_el}
                                            rechazado_por={registro.Rechazado_por}
                                            rechazado_el={registro.Rechazado_el}
                                            eventoVer={() => this.setState({ dataRegistro: registro, tipoOpe: 'ver' })}
                                            eventoEditar={() => this.setState({ dataRegistro: registro, tipoOpe: 'editar' })}
                                            eventoAprobar={() => this.handleAprobar(registro)}
                                            eventoRechazar={() => this.handleRechazar(registro)}
                                        />
                                    )}
                                    <tfoot>

                                    </tfoot>
                                </table>
                                <VentanaBloqueo
                                    show={this.state.VentanaSeguridad}
                                />
                            </div>
                        </>
                    }
                </React.Fragment>
            </LoadingOverlay >
        )
    }
    async componentDidMount() {
        const ValSeguridad = await Validar("PREBAN")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar, Cia: ValSeguridad.cia, "Usuario": ValSeguridad.usuario })
        this.handleCiaNombre(ValSeguridad.cia)
    }
}

export default BandejaBorradores;