import React from "react";
import { BotonGuardar, BotonAtras } from "../../../components/Botones";
import { get_CuentasBancarias } from "../../../services/bancosServices";

class PagosMasivos extends React.Component {
    constructor(props) {
        super(props);
        this.state = { ListaPagos: [], TRQ: "", FechaProgramado: "", ListaCuentas: [], Banco: "", Cia: this.props.Cia , Cuenta : ""}
    }

    handleChange = async (e) => {

        if (e.target.name === 'cRQ') {
            this.setState({ TRQ: e.target.value })
        }

        if (e.target.name === 'FechaProgramarMasivo') {
            this.setState({ FechaProgramado: e.target.value })
        }

        if (e.target.name === 'cBancos') {
            this.setState({ Banco: e.target.value })
            const ListaCuentas = await get_CuentasBancarias(this.state.Cia, e.target.value)
            this.setState({ "ListaCuentas": ListaCuentas })
            console.log(ListaCuentas)
        }

        if (e.target.name === 'cCuentas') {
            this.setState({ Cuenta : e.target.value })
        }
    }

    render() {
        const { dataToEdit, eventOnclick, nameOpe, EventoVolver, listaMasivos, listaTRQ, Usuario, Cia, listaBancos } = this.props

        return (
            <React.Fragment>
                <div className="card cardalign w-80rd">
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="form-group">
                            <div className="form-group row">
                                <div className="col-sm-12 col-form-label">
                                    <p>Para realizar la programacion masiva debe : </p>
                                    <p> - Ingresar una fecha a Programar.</p>
                                    <p> - Seleccionar un Banco y su respectiva cuenta.</p>
                                    <p>Debe tener en cuenta que se programara con el 100% del pago por factura</p>
                                </div>
                                <p> <input type="date" name="FechaProgramarMasivo" onChange={this.handleChange} />
                                    <select name="cRQ" defaultValue={'0'} onChange={this.handleChange}>
                                        <option value='0' selected >Seleccionar...</option>
                                        {
                                            this.props.listaTRQ.map((v_item) =>
                                                <option value={v_item.tiporq_id}>{`${v_item.tiporq_nombre}`}</option>
                                            )
                                        }
                                    </select>
                                </p>

                                <div className="form-group row">
                                    <div className="col-sm-4 col-form-label">
                                        Banco
                                    </div>
                                    <div className="col-sm-8 col-form-label">
                                        <select name="cBancos" defaultValue={'0'} onChange={this.handleChange} style={{ width: 240 }}>
                                            <option value='0' selected >Seleccionar...</option>
                                            {
                                                this.props.listaBancos.map((v_itembancos) =>
                                                    <option value={v_itembancos.Codigo}>{`${v_itembancos.Nombre}`}</option>
                                                )
                                            }
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-4 col-form-label">
                                        Cuenta Bancaria
                                    </div>
                                    <div className="col-sm-8 col-form-label">
                                        <select name="cCuentas" defaultValue={'0'} onChange={this.handleChange} style={{ width: 240 }}>
                                            {
                                                this.state.ListaCuentas.map((v_itemcuentas) =>
                                                    <option value={v_itemcuentas.Codigo}>{`${v_itemcuentas.Nombre}`}</option>
                                                )
                                            }
                                        </select>
                                    </div>
                                </div>
                                <BotonGuardar sw_habilitado={true} texto={"Programar Pagos"} eventoClick={() => eventOnclick({ "Masivo": this.props.listaMasivos, 
                                "TRQ": this.state.TRQ, "FechaRQ": this.state.FechaProgramado, "usuario": this.props.Usuario, "banco" : this.state.Banco, 
                                "cuenta" : this.state.Cuenta })} ></BotonGuardar>
                                <BotonAtras sw_habilitado={true} textoBoton="Cancelar" ></BotonAtras>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
    async componentDidMount() {
        const ListaCuentas = await get_CuentasBancarias(this.state.Cia, this.state.Banco)
        this.setState({ "ListaCuentas": ListaCuentas })

    }
}

export default PagosMasivos;