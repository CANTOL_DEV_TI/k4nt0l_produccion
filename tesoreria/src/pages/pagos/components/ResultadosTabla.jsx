import React from "react";
import { GrEdit, GrUpload, GrCodeSandbox, GrTask, GrCalendar, GrChat } from "react-icons/gr";

class ResultadosTabla extends React.Component {
    constructor(props) {
        super(props);

        this.state = { Desde: '', Hasta: '', Proveedor: '' }
    }

    handleCheck = async (e) => {
        if (e.target.checked === true) {
            let marcar = await this.props.eventoMasivo()
            console.log(marcar)
            console.log(e.target.name)     
            e.target.checked = marcar
        }

        if (e.target.checked === false) {             
            this.props.eventoQuitar(e.target.name)
        }
    }

    render() {
        const { Key, Fila, eventoProgramar, eventoMasivo, eventoQuitar, eventoVerComentario } = this.props
        return (
            <tbody>
                <tr>
                    <td className={"td-cadena"}><input type="checkbox" name={Fila.indice} onChange={this.handleCheck} /> </td>
                    <td style={{ textAlign: "center" }}><button className="btn btn-success" onClick={() => eventoProgramar(true)}><GrUpload /></button></td>
                    <td style={{ textAlign: "center" }}><button className="btn btn-info" onClick={()=> eventoVerComentario(Fila.swc_comentario)}><GrChat /></button></td>
                    <td ><button className={Fila.Atraso_dias <= 0 ? "btn btn-success" : Fila.Atraso_dias <= 30 ? "btn btn-warning" : "btn btn-danger"} ><GrCalendar /> </button></td>
                    <td className={"td-cadena-4"} id={Fila.indice}>{Fila.Estado}</td>
                    <td style={{font:"4"}} >{Fila.CC1_Nombre}</td>
                    <td style={{font:"4"}} >{Fila.Tipo_Documento}</td>
                    <td style={{font:"4"}} >{Fila.Cliente_RS}</td>
                    <td style={{font:"4"}} >{Fila.Serie}</td>
                    <td style={{font:"6"}} >{Fila.Nro}</td>
                    <td style={{font:"6"}} >{Fila.Fecha}</td>
                    <td style={{font:"6"}} >{Fila.Cond_Pago}</td>
                    <td style={{font:"6"}} >{Fila.Fecha_Registro}</td>
                    <td style={{font:"6"}} >{Fila.Fecha_Venc}</td>
                    <td style={{ textAlign: "right",font:"6" }} >{Number(Fila.Porc_Total).toLocaleString('en-US', { minimumFractionDigits: 2 })}</td>
                    <td style={{font:"6"}} >{Fila.Atraso_dias}</td>
                    <td style={{font:"6"}} >{Fila.Moneda}</td>
                    <td style={{ textAlign: "right",font:"6" }} >{Number(Fila.Total_Doc).toLocaleString('en-US', { minimumFractionDigits: 2 })}</td>
                    <td style={{font:"6"}} >{Fila.Aplica_Detraccion}</td>
                    <td style={{ textAlign: "right",font:"6" }} >{Number(Fila.Retencion).toLocaleString('en-US', { minimumFractionDigits: 2 })}</td>
                    <td style={{ textAlign: "right",font:"6" }} >{Number(Fila.Pagado).toLocaleString('en-US', { minimumFractionDigits: 2 })}</td>
                    <td style={{ textAlign: "right",font:"6" }} >{Number(Fila.total_programado).toLocaleString('en-US', { minimumFractionDigits: 2 })}</td>
                </tr>
            </tbody>
        )
    }
}
export default ResultadosTabla;