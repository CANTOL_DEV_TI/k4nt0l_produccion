import React from "react";
import { BotonGuardar } from "../../../components/Botones";
import { get_CuentasBancarias } from "../../../services/bancosServices";

class AgregarPago extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props.dataToEdit)
        if ((this.props.dataToEdit == null) || (this.props.dataToEdit == undefined)) {
            this.state = {
                Cia: '', Nro_Interno: '', Nro_Documento: '', Cliente_Cod: '', CC1_Codigo: '', CC1_Nombre: '', Tipo_Documento: '', RUC: '', Cliente_RS: '',
                Serie: '', Nro: '', Motivo: '', Fecha: '', Cond_Pago: '', Fecha_Venc: '', Porc: 0, Porc_Total: 0, Atraso_Dias: 0, Mes_Pago: 0, Ano_Pago: 0,
                Moneda: '', Total_Doc: 0, Aplica_Detraccion: '', Pagado: 0, CC2: '', fecha_programado: '', monto_programado: 0, tiporq_id: 0, retencion: 0,
                Banco: '', cuentaBanco: '', Usuario: '', ListaCuentas: [], Programado: 0
            }
        } else {
            this.state = {
                Cia: this.props.dataToEdit.Cia, Nro_Interno: this.props.dataToEdit.Nro_Interno, Nro_Documento: this.props.dataToEdit.Nro_Documento, Cliente_Cod: this.props.dataToEdit.Cliente_Cod,
                CC1_Codigo: this.props.dataToEdit.CC1_Codigo, CC1_Nombre: this.props.dataToEdit.CC1_Nombre, Tipo_Documento: this.props.dataToEdit.Tipo_Documento,
                RUC: this.props.dataToEdit.RUC, Cliente_RS: this.props.dataToEdit.Cliente_RS, Serie: this.props.dataToEdit.Serie, Nro: this.props.dataToEdit.Nro,
                Motivo: this.props.dataToEdit.Motivo, Fecha: this.props.dataToEdit.Fecha, Cond_Pago: this.props.dataToEdit.Cond_Pago, Fecha_Venc: this.props.dataToEdit.Fecha_Venc,
                Porc: this.props.dataToEdit.Porc, Porc_Total: this.props.dataToEdit.Porc_Total, Atraso_dias: this.props.dataToEdit.Atraso_dias, Mes_Pago: this.props.dataToEdit.Mes_Pago,
                Ano_Pago: this.props.dataToEdit.Ano_Pago, Moneda: this.props.dataToEdit.Moneda, Total_Doc: this.props.dataToEdit.Total_Doc,
                Aplica_Detraccion: this.props.dataToEdit.Aplica_Detraccion, Pagado: this.props.dataToEdit.Pagado, CC2: this.props.dataToEdit.CC2,
                fecha_programado: this.props.dataToEdit.Fecha_Venc, monto_programado: this.props.dataToEdit.Porc_Total - this.props.dataToEdit.total_programado, tiporq_id: this.props.dataToEdit.tiporq_id,
                Retencion: this.props.dataToEdit.Retencion, fecha_registro: this.props.dataToEdit.Fecha_Registro, Banco: "", cuentaBanco: "", Usuario: "", ListaCuentas: [],
                Programado: this.props.dataToEdit.total_programado, Estado : this.props.dataToEdit.Estado
            }
        }
    }

    handleChange = async (e) => {
        if (e.target.name === "cBancos") {
            this.setState({ Banco: e.target.value })
            const ListaCuentas = await get_CuentasBancarias(this.state.Cia, e.target.value)
            this.setState({ "ListaCuentas": ListaCuentas })
            console.log(ListaCuentas)
        }

        if (e.target.name === "cCuentas") {
            this.setState({ cuentaBanco: e.target.value })
        }

        if (e.target.name === "tFechaProgramacion") {
            this.setState({ fecha_programado: e.target.value })
        }

        if (e.target.name === "tMontoProgramacion") {
            this.setState({ monto_programado: e.target.value })
        }

        if (e.target.name === "cRQ") {
            this.setState({ tiporq_id: e.target.value })
        }
    }

    render() {
        const { dataToEdit, eventOnclick, nameOpe, EventoVolver, listaTRQ, Usuario, listaBancos, listaCuentas } = this.props

        return (
            <div className="card cardalign w-80rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Ruc : </div>
                            <div className="col-sm-4 col-form-label">
                                <input className='form-control form-control-sm' name="tRUC" size="sm" type="text" readOnly value={this.state.RUC} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Proveedor : </div>
                            <div className="col-sm-8 col-form-label">
                                <input className='form-control form-control-sm' name="tNomCli" size="sm" type="text" readOnly value={this.state.Cliente_RS} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Centro de Costo</div>

                            <div className="col-sm-8 col-form-label">
                                <input className='form-control form-control-sm' name="tCCosto1" size="sm" type="text" readOnly value={this.state.CC1_Codigo} />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-1 col-form-label">
                                Serie
                            </div>
                            <div className="col-sm-1 col-form-label">
                                <input className='form-control form-control-sm' name="tSerie" size="sm" type="text" readOnly value={this.state.Serie} />
                            </div>

                            <div className="col-sm-1 col-form-label">
                                Nro
                            </div>
                            <div className="col-sm-2 col-form-label">
                                <input className='form-control form-control-sm' name="tNro" size="sm" type="text" readOnly value={this.state.Nro} />
                            </div>
                            <div className="col-sm-1 col-form-label">
                                Emisión
                            </div>
                            <div className="col-sm-2 col-form-label">
                                <input className='form-control form-control-sm' name="tFecha" size="sm" type="text" readOnly value={this.state.Fecha} />
                            </div>
                            <div className="col-sm-1 col-form-label">
                                Moneda
                            </div>
                            <div className="col-sm-1 col-form-label">
                                <input className='form-control form-control-sm' name="tMoneda" size="sm" type="text" readOnly value={this.state.Moneda} />
                            </div>
                            <div className="col-sm-4 col-form-label">
                                Detracción
                            </div>
                            <div className="col-sm-2 col-form-label">
                                <input className='form-control form-control-sm' name="tDetraccion" size="sm" type="text" readOnly value={this.state.Aplica_Detraccion} />
                            </div>
                            <div className="col-sm-3 col-form-label">
                                Retención
                            </div>
                            <div className="col-sm-3 col-form-label">
                                <input className='form-control form-control-sm' name="tRetencion" size="sm" type="text" readOnly value={Number(this.state.Retencion).toLocaleString('en-US', { minimumFractionDigits: 2 })} />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">
                                Motivo
                            </div>
                            <div className="col-sm-8 col-form-label">
                                <input className='form-control form-control-sm' name="tGlosa" size="sm" type="text" readOnly value={this.state.Motivo} />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">
                                Cond. Pago
                            </div>
                            <div className="col-sm-8 col-form-label">
                                <input className='form-control form-control-sm' name="tCondPago" size="sm" type="text" readOnly value={this.state.Cond_Pago} />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">
                                Fecha Registro
                            </div>
                            <div className="col-sm-2 col-form-label">
                                <input className='form-control form-control-sm' name="tFechaPago" size="sm" type="text" readOnly value={this.state.fecha_registro} />
                            </div>

                            <div className="col-sm-4 col-form-label">
                                Fecha Pago
                            </div>
                            <div className="col-sm-2 col-form-label">
                                <input className='form-control form-control-sm' name="tFechaPago" size="sm" type="text" readOnly value={this.state.Fecha_Venc} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">
                                Monto a Pagar
                            </div>
                            <div className="col-sm-3 col-form-label">
                                <input className='form-control form-control-sm' name="tMontoaPagar" size="sm" type="text" readOnly value={Number(this.state.Porc_Total).toLocaleString('en-US', { minimumFractionDigits: 2 })} />
                            </div>
                            <div className="col-sm-3 col-form-label">
                                Pagado
                            </div>
                            <div className="col-sm-3 col-form-label">
                                <input className='form-control form-control-sm' name="tPagado" size="sm" type="text" readOnly value={Number(this.state.Pagado).toLocaleString('en-US', { minimumFractionDigits: 2 })} />
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div className="card-header cardalign w-50rd">
                        <div className="card-header border border-dashed border-end-0 border-start-0">
                            <div className="form-group">
                                <div className="form-group row">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="form-group">
                    <div className="form-group row">
                        <div className="col-sm-4 col-form-label">
                            Banco
                        </div>
                        <div className="col-sm-8 col-form-label">
                            <select name="cBancos" defaultValue={'0'} onChange={this.handleChange} style={{ width: 240 }}>
                                <option value='0' selected >Seleccionar...</option>
                                {
                                    this.props.listaBancos.map((v_itembancos) =>
                                        <option value={v_itembancos.Codigo}>{`${v_itembancos.Nombre}`}</option>
                                    )
                                }
                            </select>
                        </div>

                        <div className="col-sm-4 col-form-label">
                            Cuenta Bancaria
                        </div>
                        <div className="col-sm-8 col-form-label">
                            <select name="cCuentas" defaultValue={'0'} onChange={this.handleChange} style={{ width: 240 }}>
                                {
                                    this.state.ListaCuentas.map((v_itemcuentas) =>
                                        <option value={v_itemcuentas.Codigo}>{`${v_itemcuentas.Nombre}`}</option>
                                    )
                                }
                            </select>
                        </div>
                    </div>
                    <div className="form-group row">
                        <div className="col-sm-4 col-form-label">
                            Tipo Requerimiento
                        </div>
                        <div className="col-sm-8 col-form-label">
                            <select name="cRQ" defaultValue={'0'} onChange={this.handleChange}>
                                <option value='0' selected >Seleccionar...</option>
                                {
                                    this.props.listaTRQ.map((v_item) =>
                                        <option value={v_item.tiporq_id}>{`${v_item.tiporq_nombre}`}</option>
                                    )
                                }
                            </select>
                        </div>
                    </div>
                    <div className="form-group row">
                        <div className="col-sm-4 col-form-label">

                            Programado a
                        </div>
                        <div className="col-sm-4 col-form-label">
                            <input className='form-control form-control-sm' name="tFechaProgramacion" size="sm" type="date" value={this.state.fecha_programado} onChange={this.handleChange} />
                        </div>
                    </div>

                    <div className="form-group row">
                        <div className="col-sm-4 col-form-label">
                            Monto a Pagar
                        </div>
                        <div className="col-sm-4 col-form-label">
                            <input className='form-control form-control-sm' type="number" name="tMontoProgramacion" size="sm" value={this.state.monto_programado} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="card-header cardalign w-50rd">
                        <div className="card-header border border-dashed border-end-0 border-start-0">
                            <div className="form-group">
                                <div className="form-group row">
                                    <BotonGuardar texto={"Guardar Programacion"} sw_habilitado={true} eventoClick={() => eventOnclick({
                                        "emp_codigo": this.state.Cia, "nro_interno": this.state.Nro_Interno,
                                        "nro_documento": this.state.Nro_Documento, "cliente_cod": this.state.Cliente_Cod, "cc1_codigo": this.state.CC1_Codigo,
                                        "cc1_nombre": this.state.CC1_Nombre, "tipo_documento": this.state.Tipo_Documento, "ruc": this.state.RUC, "cliente_rs": this.state.Cliente_RS,
                                        "serie": this.state.Serie, "nro": this.state.Nro, "motivo": this.state.Motivo, "fecha": this.state.fecha_registro, "cond_pago": this.state.Cond_Pago,
                                        "fecha_venc": this.state.Fecha_Venc, "porc": this.state.Porc, "porc_total": this.state.Porc_Total, "atraso_dias": this.state.Atraso_dias,
                                        "mes_pago": this.state.Mes_Pago, "ano_pago": this.state.Ano_Pago, "moneda": this.state.Moneda, "total_doc": this.state.Total_Doc,
                                        "aplica_detraccion": this.state.Aplica_Detraccion, "pagado": this.state.Pagado, "cc2": this.state.CC2,
                                        "fecha_programado": this.state.fecha_programado, "monto_programado": Number(this.state.monto_programado) - Number(this.state.Programado), "tiporq_id": this.state.tiporq_id,
                                        "retencion": this.state.Retencion, "banco": this.state.Banco, "usuario": this.props.Usuario, "cuenta": this.state.cuentaBanco,
                                        "fecha_emision": this.state.Fecha, "programado": Number(this.state.Programado),"estado":this.state.Estado
                                    })} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    async componentDidMount() {
        const ListaCuentas = await get_CuentasBancarias(this.state.Cia, this.state.Banco)
        this.setState({ "ListaCuentas": ListaCuentas })

    }
}

export default AgregarPago;