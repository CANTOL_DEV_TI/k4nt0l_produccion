import React from "react";

class Mensaje extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            msj: ""
        }
    }
    render() {
        const { MensajeMostrar } = this.props
        return (
            <div class="alert alert-success">
                <strong>Completo!</strong> {this.props.MensajeMostrar}.
            </div>
        )
    }

}

export default Mensaje