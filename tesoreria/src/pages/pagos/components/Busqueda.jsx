import React, { useState } from "react";
import Title from "../../../components/Titulo";
import { BotonExcel, BotonNuevo, BotonBuscar, BotonGuardar, BotonGuardarMasivo } from "../../../components/Botones";

class Busqueda extends React.Component {

    constructor(props) {
        super(props);
        
        let hoy = new Date(Date.now())
        //console.log(hoy)

        this.state = { Desde: hoy.toISOString().slice(0, 10), Hasta: hoy.toISOString().slice(0, 10), Proveedor: '', Moneda: 'S/', Orden: '16', TipoOrden: 'ASC' ,Detraccion:'N'}
    }

    handleChange = (e) => {
        if (e.target.name === "tDesde") {
            this.setState({ Desde: e.target.value })
        }

        if (e.target.name === "tHasta") {
            this.setState({ Hasta: e.target.value })
        }

        if (e.target.name === "tProveedor") {
            this.setState({ Proveedor: e.target.value })
        }

        if (e.target.name === "sMoneda") {
            this.setState({ Moneda: e.target.value })
        }

        if (e.target.name === "sOrdenar") {
            this.setState({ Orden: e.target.value })
        }

        if (e.target.name === "sTipoOrden") {
            this.setState({ TipoOrden: e.target.value })
        }

        if (e.target.name === "sDetraccion") {
            this.setState({ Detraccion : e.target.value})
        }
    }


    render() {

        const { handleBusqueda, handleModal, xCia, HandleMasivo } = this.props

        return (
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Lista de Pagos Pendientes {xCia}</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    Moneda :
                                    <select name="sMoneda" onChange={this.handleChange} value={this.state.Moneda}>
                                        <option value={"S/"}>Soles</option>
                                        <option value={"US$"}>Dolares</option>
                                    </select>
                                    Proveedor : <input name="tProveedor" placeholder="Ingrese Busqueda..." onChange={this.handleChange} value={this.state.Proveedor} />
                                    Desde :
                                    <input type="date"
                                        name="tDesde"
                                        placeholder="Desde"
                                        onChange={this.handleChange}
                                        value={this.state.Desde}
                                    />
                                    Hasta :
                                    <input type="date"
                                        name="tHasta"
                                        placeholder="Hasta"
                                        onChange={this.handleChange}
                                        value={this.state.Hasta}
                                    />
                                    Detraccion :
                                    <select name="sDetraccion" onChange={this.handleChange} value={this.state.Detraccion}>                                        
                                        <option value={"N"}>No</option>
                                        <option value={"Y"}>Si</option>
                                    </select>
                                    Ordenar por :
                                    <select name="sOrdenar" onChange={this.handleChange} value={this.state.Ordenar}>
                                        <option value={"16"}>Fecha Emision</option>
                                        <option value={"12"}>Fecha Registro</option>
                                        <option value={"18"}>Fecha Vencimiento</option>
                                        <option value={"20"}>Por Pagar</option>
                                        <option value={"21"}>Dias Atraso</option>
                                        <option value={"25"}>Total Doc</option>
                                        <option value={"8"}>Centro Costo</option>
                                        <option value={"9"}>Tipo Documento</option>
                                    </select>

                                    <select name="sTipoOrden" onChange={this.handleChange} value={this.state.TipoOrdenar}>
                                        <option value={"ASC"}>Asc</option>
                                        <option value={"DESC"}>Desc</option>
                                    </select>

                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda(this.state.Desde, this.state.Hasta, this.state.Proveedor, this.state.Moneda, this.state.Orden, this.state.TipoOrden, this.state.Detraccion)} />
                                    <BotonGuardarMasivo texto={"Programacion Masiva"} sw_habilitado={true} eventoClick={() => HandleMasivo()} />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Busqueda;