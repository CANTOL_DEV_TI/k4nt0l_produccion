import React from "react";
import Title from "../../components/Titulo";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"
import Busqueda from "./components/Busqueda.jsx";
import VentanaModal from "../../components/VentanaModal.jsx"
import { getFilter_PagosPendientes, saveProgramacion, TotalesPagosPendientes, GuardarComentarioSAP } from "../../services/pagosServices.jsx";
import ResultadosTabla from "./components/ResultadosTabla.jsx";
import AgregarPago from "./components/AgregarPago.jsx";
import PagosMasivos from "./components/PagosMasivos.jsx";
import { get_listaTipoRQ } from "../../services/tipoRqServives.jsx";
import { get_Bancos } from "../../services/bancosServices.jsx";
import Mensaje from "./components/Mensaje.jsx";
import { BotonExcel, BotonGrabar } from "../../components/Botones.jsx";
import LoadingOverlay from 'react-loading-overlay';

class ListadoPagosPendientes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            resultados: [],
            isFetch: false,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            Cia: '',
            CiaNombre: '',
            showModalPM: false,
            showModalMe: false,
            dataMasivo: [],
            ListaTRQ: [],
            Usuario: '',
            mensajeTotal: '',
            Total_Pagos: 0,
            Cant_Pagos: 0,
            Bloqueo: false,
            showModalCo: false,
            swc_comentario: "",
            nro_interno: 0,
            Total_Sel: 0,
            Cant_Sel: 0
        }
    }

    handleBusqueda = async (pDesde, pHasta, pProveedor, pMoneda, pOrden, pTipoOrden, pDetraccion) => {
        this.setState({ isFetch: true, Bloqueo: true, dataMasivo: [] })
        const responseJson = await getFilter_PagosPendientes(this.state.Cia, pDesde, pHasta, pProveedor, pMoneda, pOrden, pTipoOrden, pDetraccion)
        this.setState({ resultados: responseJson })
        const responseJsonT = await TotalesPagosPendientes(this.state.Cia, pDesde, pHasta, pProveedor, pMoneda, pDetraccion)
        this.setState({ Cant_Pagos: responseJsonT.Cant_Pagos, Total_Pagos: responseJsonT.Total, isFetch: false, Bloqueo: false })
    }

    handleProgMasivo = async (e) => {
        this.setState({ tipoOpe: 'Masivo', showModalPM: true })
    }

    handleSubmit = async (e) => {
        let totalprog = 0

        totalprog = e.total_doc - (e.monto_programado + e.pagado)

        if ((e.estado === 'Sin Programacion') || (e.estado === 'Programado Parcial')) {
            if (totalprog >= 0.00) {

                if (this.state.tipoOpe === "Programar") {
                    //console.log(e)
                    this.setState({ Bloqueo: true })
                    const guardar_programacion = await saveProgramacion(e)
                    //console.log(guardar_programacion)
                    alert(guardar_programacion.Mensaje + " : Se registro la programacion")
                    this.setState({ showModal: false, tipoOpe: 'Find', Bloqueo: false, dataMasivo: [] })
                }
            } else {
                alert("El monto a pagar es superior al adeudado.")
            }
        } else {
            //console.log(e.programado)
            alert("Ya tiene una programacion asignada.")
        }

    }

    handleSubmitMasivo = async (e) => {
        let mensajeT = ""

        if (this.state.tipoOpe === "Masivo") {
            e.Masivo.map(async (fila) => {
                let nuevo = {
                    "emp_codigo": fila.emp_codigo, "nro_interno": fila.nro_interno, "nro_documento": fila.nro_documento, "cliente_cod": fila.cliente_cod,
                    "cc1_codigo": fila.cc1_codigo, "cc1_nombre": fila.cc1_nombre, "tipo_documento": fila.tipo_documento, "ruc": fila.ruc, "cliente_rs": fila.cliente_rs,
                    "serie": fila.serie, "nro": fila.nro, "motivo": fila.motivo, "fecha": fila.fecha, "cond_pago": fila.cond_pago, "fecha_venc": fila.fecha_venc,
                    "porc": fila.porc, "porc_total": fila.porc_total, "atraso_dias": fila.atraso_dias, "mes_pago": fila.mes_pago, "ano_pago": fila.ano_pago,
                    "moneda": fila.moneda, "total_doc": fila.total_doc, "aplica_detraccion": fila.aplica_detraccion, "pagado": fila.pagado, "cc2": fila.cc2,
                    "fecha_programado": e.FechaRQ, "monto_programado": fila.porc_total, "tiporq_id": e.TRQ, "usuario": e.usuario, "retencion": fila.retencion,
                    "banco": e.banco, "cuenta": e.cuenta, "fecha_emision": fila.fecha_emision, "swc_comentario": fila.swc_comentario, "Programado": fila.Programado
                }


                let guardar_programacion = await saveProgramacion(nuevo)
                let mensaje = guardar_programacion.Mensaje + "-" + fila.serie + "-" + fila.nro + " : Se registro la programacion. "


                this.setState({ mensajeTotal: [...this.state.mensajeTotal, mensaje] })
            }
            )

            this.setState({ showModalPM: false, tipoOpe: 'Find', showModalMe: true })
        }
    }

    handleAlert = (e) => {
        setTimeout(() => this.setState({ showModalMe: false }), 3000)
    }

    handleAtras = async (e) => {
        this.setState({ showModalE: false })
        this.setState({ showModal: false, tipoOpe: 'Find' })
    }

    handleGetRow = async (e) => {
        console.log(e)
    
        if ((e.Estado === 'Sin Programacion') || (e.Estado === 'Programado Parcial')) {
            //if (totalprog >= 0.00) {
                let Seleccionado = {
                    "indice": e.indice, "emp_codigo": e.Cia, "nro_interno": e.Nro_Interno, "nro_documento": e.Nro_Documento, "cliente_cod": e.Cliente_Cod, "cc1_codigo": e.CC1_Codigo,
                    "cc1_nombre": e.CC1_Nombre, "tipo_documento": e.Tipo_Documento, "ruc": e.RUC, "cliente_rs": e.Cliente_RS, "serie": e.Serie, "nro": e.Nro,
                    "motivo": e.Motivo, "fecha": e.Fecha_Registro, "cond_pago": e.Cond_Pago, "fecha_venc": e.Fecha_Venc, "porc": e.Porc, "porc_total": e.Porc_Total,
                    "atraso_dias": e.Atraso_dias, "mes_pago": e.Mes_Pago, "ano_pago": e.Ano_Pago, "moneda": e.Moneda, "total_doc": e.Total_Doc,
                    "aplica_detraccion": e.Aplica_Detraccion, "pagado": e.Pagado, "cc2": e.CC2, "fecha_programado": e.fecha_programado, "monto_programado": e.monto_programado,
                    "tiporq_id": e.tiporq_id, "usuario": e.Usuario, "retencion": e.Retencion, "fecha_emision": e.Fecha
                }
                let Filas = this.state.dataMasivo
                Filas = [...Filas, Seleccionado]
                this.setState({ dataMasivo: Filas })
                this.HandleCalcularTotalesSeleccionados(Filas)
                return true;
            //}
        } else {
            alert("Ya cuenta con programación total o parcial...")
            return false;
        }
    }

    handleDeleteRow = async (NroInterno) => {
        const items = this.state.dataMasivo
        const targetIndex = items.findIndex(items => items.indice === NroInterno)
        items.splice(targetIndex, 1)
        this.setState({ dataMasivo: items })
        this.HandleCalcularTotalesSeleccionados(items)
    }

    HandleCalcularTotalesSeleccionados = async (pData) => {
        let Cant_Seleccionados = pData.length;
        let Total_Seleccionados = pData.reduce((dTotal, ActValor) => dTotal + Number(ActValor.porc_total), 0);
        this.setState({ Total_Sel: Total_Seleccionados, Cant_Sel: Cant_Seleccionados });
    }

    handleCiaNombre = async (e) => {
        if (e === "CNT") {
            let xNombre = "CANTOL S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "DTM") {
            let xNombre = "DISTRIMAX S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "TCN") {
            let xNombre = "TECNOPRESS S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }
    }

    handleVerComentario = async (pID, pComentario) => {
        this.setState({ showModalCo: true, swc_comentario: pComentario, nro_interno: pID })
    }

    handleChange = async (e) => {
        this.setState({ swc_comentario: e.target.value })
    }

    handleGuardarComentario = async () => {
        const response = await GuardarComentarioSAP(this.state.Cia, this.state.nro_interno, this.state.swc_comentario)
        alert(response)
        this.setState({ showModalCo: false, Nro_Interno: 0, swc_comentario: '' })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad, Cia, Usuario } = this.state

        return (
            <LoadingOverlay active={this.state.Bloqueo} spinner text="Procesando la operación..." >
                <React.Fragment>
                    {this.state.tipoOpe == 'nuevo' || this.state.tipoOpe == 'Actualizar' /*|| this.state.tipoOpe == 'Eliminar'*/ ?

                        <AgregarPago
                            dataToEdit={this.state.dataRegistro}
                            eventOnclick={this.handleSubmit}
                            nameOpe={'Grabar'}
                            showDetalle={false}
                            EventoVolver={this.handleAtras} />

                        :
                        <>
                            <Busqueda handleBusqueda={this.handleBusqueda} xCia={this.state.CiaNombre} showComponente={'nuevo'}
                                handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Find' })} HandleMasivo={this.handleProgMasivo} />

                            {isFetch && 'Cargando...'}
                            {(!isFetch && !resultados.length) && 'Sin Informacion'}

                            <div className="table-responsive" style={{ height: '400px', display: "-ms-flexbox" }}>
                                <table className="table table-hover table-sm table-bordered table-striped" >
                                    <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                                        <tr >
                                            <th colSpan={22} ></th>
                                        </tr>
                                        <tr>
                                            <th className="align-middle" > </th>
                                            <th className="align-middle" > </th>
                                            <th className="align-middle" > </th>
                                            <th className="align-middle" > </th>
                                            <th className="align-middle" > </th>
                                            <th className="align-middle" >CC1 Nombre</th>
                                            <th className="align-middle" style={{ minWidth: '100px' }}>Tipo Documento</th>
                                            <th className="align-middle" style={{ font: 4, minWidth: '220px' }}>Proveedor</th>
                                            <th className="align-middle" >Serie</th>
                                            <th className="align-middle" style={{ minWidth: '50px' }}>Nro</th>
                                            <th className="align-middle" style={{ minWidth: '100px' }}>Fecha Emi.</th>
                                            <th className="align-middle" style={{ minWidth: '150px' }}>Cond. Pago</th>
                                            <th className="align-middle" style={{ minWidth: '100px' }}>Fecha Reg.</th>
                                            <th className="align-middle" style={{ minWidth: '100px' }}>Fecha Venc.</th>
                                            <th className="align-middle" >Por Pagar</th>
                                            <th className="align-middle" >Atraso dias</th>
                                            <th className="align-middle" >Moneda</th>
                                            <th className="align-middle" >Total Doc.</th>
                                            <th className="align-middle" >Ap. Detra</th>
                                            <th className="align-middle" >Retencion</th>
                                            <th className="align-middle" >Pagado</th>
                                            <th className="align-middle" >Planificado</th>

                                        </tr>
                                    </thead>

                                    {resultados.map((registro, index) =>
                                        <ResultadosTabla
                                            key={index}
                                            Fila={registro}
                                            eventoProgramar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Programar' })}
                                            eventoMasivo={() => this.handleGetRow(registro)}
                                            eventoQuitar={() => this.handleDeleteRow(registro.indice)}
                                            eventoVerComentario={() => this.handleVerComentario(registro.Nro_Interno, registro.swc_comentario)}
                                        />
                                    )}
                                    <tfoot >


                                    </tfoot>
                                </table>

                                <VentanaBloqueo
                                    show={this.state.VentanaSeguridad}
                                />
                                <VentanaModal
                                    show={this.state.showModal}
                                    size={"xl"}
                                    titulo={"Programacion de Pago"}
                                    handleClose={() => this.setState({ showModal: false })}
                                >
                                    <AgregarPago
                                        //show={this.state.showModal} 
                                        dataToEdit={this.state.dataRegistro}
                                        listaTRQ={this.state.ListaTRQ}
                                        listaBancos={this.state.ListaBancos}
                                        nameOpe={"programar"}
                                        eventOnclick={this.handleSubmit}
                                        Usuario={this.state.Usuario}
                                    />

                                </VentanaModal>
                                <VentanaModal
                                    show={this.state.showModalPM}
                                    size={"xm"}
                                    titulo={"Agendar Pagos Masivos"}
                                    handleClose={() => this.setState({ showModalPM: false })}
                                >
                                    <PagosMasivos nameOpe='Masivo' listaMasivos={this.state.dataMasivo} listaTRQ={this.state.ListaTRQ} eventOnclick={this.handleSubmitMasivo} Usuario={this.state.Usuario} Cia={this.state.Cia} listaBancos={this.state.ListaBancos} />
                                </VentanaModal>
                                <VentanaModal
                                    show={this.state.showModalMe}
                                    size={"xs"}
                                    titulo={""}
                                    handleClose={setTimeout(() => this.setState({ showModalMe: false }), 30000)}
                                >
                                    <Mensaje MensajeMostrar={this.state.mensajeTotal} />
                                </VentanaModal>
                                <VentanaModal
                                    show={this.state.showModalCo}
                                    size={"xm"}
                                    titulo={"Comentario"}
                                    handleClose={() => this.setState({ showModalCo: false })}
                                >
                                    <textarea rows={5} cols={50} name="swc_comentario" value={this.state.swc_comentario} onChange={this.handleChange} />
                                    <BotonGrabar textoBoton="Guardar Comentario" sw_habilitado={true} eventoClick={() => this.handleGuardarComentario()} />
                                </VentanaModal>
                            </div>

                            <div className="col-lg-12">
                                <div className="card">
                                    <div className="card-header border border-dashed border-end-0 border-start-0">
                                        <div className="row align-items-center gy-3">
                                            <div className="col-sm">
                                                <Title>Totales </Title>
                                                <div className="col-sm-auto">
                                                    <div className="d-flex flex-wrap gap-1">
                                                        CANTIDAD DOCUMENTOS <input name="tCant" readOnly value={Number(this.state.Cant_Pagos).toLocaleString("en")} /> ----
                                                        TOTAL DOCUMENTOS : <input name="tTotal" readOnly value={Number(this.state.Total_Pagos).toLocaleString("en")} />
                                                        <BotonExcel textoBoton={"Exportar"} sw_habilitado={resultados.length > 0 ? true : false} listDatos={resultados} nombreArchivo={"Listado de Pagos"} />
                                                        CANTIDAD SELECCIONADOS <input name="tCantSel" readOnly value={Number(this.state.Cant_Sel).toLocaleString("en")} /> ----
                                                        TOTAL SELECCIONADOS <input name="tTotalSel" readOnly value={Number(this.state.Total_Sel).toLocaleString("en")} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                    }
                </React.Fragment>
            </LoadingOverlay>
        )

    }
    async componentDidMount() {
        const ValSeguridad = await Validar("TESPRO")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar, "Cia": ValSeguridad.cia, "Usuario": ValSeguridad.usuario })

        console.log(ValSeguridad)

        this.handleCiaNombre(ValSeguridad.cia)

        const ListaTRQ_ = await get_listaTipoRQ(" ")
        this.setState({ "ListaTRQ": ListaTRQ_ })

        const ListaBancos = await get_Bancos(this.state.Cia)
        this.setState({ "ListaBancos": ListaBancos })

    }

}
export default ListadoPagosPendientes;