import React from "react";
import Title from "../../../components/Titulo";
import { BotonBuscar } from "../../../components/Botones";

class Busqueda extends React.Component {
    constructor(props) {
        super(props);
        this.state = { Ejercicio : ''}
    }
    
    handleChange=(e)=>{
        console.log(e.target.value)
        this.setState({"Ejercicio" : e.target.value})
    }
    render() {
        const { handleBusqueda, handleModal, xCia, xCiaCodigo } = this.props

        return (
            <React.Fragment>
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header border border-dashed border-end-0 border-start-0">
                            <div className="row align-items-center gy-3">
                                <div className="col-sm">
                                    <Title>Lista de Presupuestos : {xCia}</Title>
                                </div>
                                <div> 
                                    <input type="number" placeholder="Ingrese Ejercicio..." onChange={this.handleChange} value={this.state.Ejercicio} />
                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda(this.state.Ejercicio)} />
                                </div>
                            </div>
                            <div>

                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )

    }
}

export default Busqueda