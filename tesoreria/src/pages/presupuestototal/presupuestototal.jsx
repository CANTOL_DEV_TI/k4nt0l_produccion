import React from "react";
import Title from "../../components/Titulo";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"
import Busqueda from "./components/Busqueda.jsx";
import VentanaModal from "../../components/VentanaModal.jsx"
import TablaResultados from "./components/TablaResultado.jsx";
import { getLista_PresupuestosTotal } from "../../services/presupuestoServices.jsx";

class ListadoPresupuestoTotal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            Cia: '',
            CiaNombre: ''
        }
    }

    handleCiaNombre = async (e) => {
        if (e === "CNT") {
            let xNombre = "CANTOL S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "DTM") {
            let xNombre = "DISTRIMAX S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "TCN") {
            let xNombre = "TECNOPRESS S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }
    }

    handleBusqueda = async (xEjercicio) => {
        console.log(xEjercicio)
        if(xEjercicio!==""){
            const filtros = { "Compañia": this.state.Cia, "Ejercicio" : xEjercicio }
            const resultados_lista = await getLista_PresupuestosTotal(filtros)
            this.setState({ resultados: resultados_lista })
        }else{
            alert("Debe ingresar el ejercicio para realizar la busqueda...")
        }
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad, Cia, Usuario, hist } = this.state
        return (
            <React.Fragment>
                <Busqueda xCia={this.state.CiaNombre} handleBusqueda={this.handleBusqueda} />
                <div className="table-responsive" style={{ height: '800px', display: "-ms-flexbox" }}>
                    <TablaResultados Datos={this.state.resultados} />
                </div>
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}
                />
            </React.Fragment>

        )
    }
    async componentDidMount() {
        const ValSeguridad = await Validar("PREGEN")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar, "Cia": ValSeguridad.cia, "Usuario": ValSeguridad.usuario })

        //console.log(ValSeguridad) 

        this.handleCiaNombre(ValSeguridad.cia)


    }
}

export default ListadoPresupuestoTotal