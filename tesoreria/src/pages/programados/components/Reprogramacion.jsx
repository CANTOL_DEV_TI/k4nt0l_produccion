import React from "react"; 
import { BotonGuardar } from "../../../components/Botones"; 
import { get_CuentasBancarias } from "../../../services/bancosServices"; 
import { get_Bancos } from "../../../services/bancosServices"; 
 
 
class ReProgramacion extends React.Component { 
    constructor(props) { 
        super(props); 
        if (this.props.DataToView === null) { 
 
        } else { 
            this.state = { 
                ano_pago: this.props.DataToView.ano_pago, aplica_detraccion: this.props.DataToView.aplica_detraccion, atraso_dias: this.props.DataToView.atraso_dias, 
                cc1_codigo: this.props.DataToView.cc1_codigo, cc1_nombre: this.props.DataToView.cc1_nombre, cc2: this.props.DataToView.cc2, cliente_cod: this.props.DataToView.cliente_cod, 
                cliente_rs: this.props.DataToView.cliente_rs, cond_pago: this.props.DataToView.cond_pago, emp_nombre: this.props.DataToView.emp_nombre, fecha: this.props.DataToView.fecha, 
                fecha_programado: this.props.DataToView.fecha_programado, fecha_registrado: this.props.DataToView.fecha_registrado, fecha_venc: this.props.DataToView.fecha_venc, 
                indice: this.props.DataToView.indice, mes_pago: this.props.DataToView.mes_pago, moneda: this.props.DataToView.moneda, monto_programado: this.props.DataToView.monto_programado, 
                motivo: this.props.DataToView.motivo, nro: this.props.DataToView.nro, nro_documento: this.props.DataToView.nro_documento, nro_interno: this.props.DataToView.nro_interno, 
                pagado: this.props.DataToView.pagado, planificacion_id: this.props.DataToView.planificacion_id, porc: this.props.DataToView.porc, porc_total: this.props.DataToView.porc_total, 
                ruc: this.props.DataToView.ruc, serie: this.props.DataToView.serie, tipo_documento: this.props.DataToView.tipo_documento, tiporq_id: this.props.DataToView.tiporq_id, 
                tiporq_nombre: this.props.DataToView.tiporq_nombre, total_doc: this.props.DataToView.total_doc, usuario: this.props.Usuario, Operacion: this.props.Operacion, 
                ListaCuentas: [], ListaBancos : [], cia: this.props.xCia, Retencion: this.props.DataToView.Retencion 
            } 
        } 
 
 
    } 
    handleChange = async (e) => { 
        if (e.target.name === 'tFechaProgramacion') { 
            console.log(e.target.value) 
            this.setState({ fecha_programado: e.target.value }) 
        } 
 
        if (e.target.name === 'cBancos') { 
            this.setState({ banco: e.target.value }) 
            console.log(this.state.cia) 
            console.log(e.target.value) 
            const ListaCuentas = await get_CuentasBancarias(this.state.cia, e.target.value) 
            this.setState({ "ListaCuentas": ListaCuentas }) 
            console.log(ListaCuentas) 
        } 
 
        if (e.target.name === 'cCuentas') { 
            this.setState({ cuenta: e.target.value }) 
        } 
 
    } 
    render() { 
        const { DataToView, EventoActualizar, Operacion, Usuario, listaBancos, xCia } = this.props 
 
        return ( 
            <React.Fragment> 
                <div className="card cardalign w-80rd"> 
                    <div className="card-header border border-dashed border-end-0 border-start-0"> 
                        <div className="form-group"> 
                            <div className="form-group row"> 
                                <div className="col-sm-4 col-form-label">Ruc : </div> 
                                <div className="col-sm-4 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tRUC" size="sm" type="text" readOnly value={this.state.ruc} /> 
                                </div> 
                            </div> 
                            <div className="form-group row"> 
                                <div className="col-sm-4 col-form-label">Proveedor : </div> 
                                <div className="col-sm-8 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tNomCli" size="sm" type="text" readOnly value={this.state.cliente_rs} /> 
                                </div> 
                            </div> 
                            <div className="form-group row"> 
                                <div className="col-sm-4 col-form-label">Centro de Costo</div> 
 
                                <div className="col-sm-8 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tCCosto1" size="sm" type="text" readOnly value={this.state.cc1_nombre} /> 
                                </div> 
                            </div> 
                        </div> 
                        <div className="form-group"> 
                            <div className="form-group row"> 
                                <div className="col-sm-2 col-form-label"> 
                                    Serie 
                                </div> 
                                <div className="col-sm-3 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tSerie" size="sm" type="text" readOnly value={this.state.serie} /> 
                                </div> 
 
                                <div className="col-sm-2 col-form-label"> 
                                    Nro 
                                </div> 
                                <div className="col-sm-4 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tNro" size="sm" type="text" readOnly value={this.state.nro} /> 
                                </div> 
                                <div className="col-sm-2 col-form-label"> 
                                    Fecha 
                                </div> 
                                <div className="col-sm-4 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tFecha" size="sm" type="text" readOnly value={this.state.fecha} /> 
                                </div> 
                                <div className="col-sm-3 col-form-label"> 
                                    Moneda 
                                </div> 
                                <div className="col-sm-2 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tMoneda" size="sm" type="text" readOnly value={this.state.moneda} /> 
                                </div> 
                                <div className="col-sm-4 col-form-label"> 
                                    Detraccion 
                                </div> 
                                <div className="col-sm-2 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tDetraccion" size="sm" type="text" readOnly value={this.state.aplica_detraccion} /> 
                                </div> 
                                <div className="col-sm-3 col-form-label"> 
                                    Retención 
                                </div> 
                                <div className="col-sm-3 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tRetencion" size="sm" type="text" readOnly value={this.state.Retencion} /> 
                                </div> 
                            </div> 
                        </div> 
                        <div className="form-group"> 
                            <div className="form-group row"> 
                                <div className="col-sm-4 col-form-label"> 
                                    Motivo 
                                </div> 
                                <div className="col-sm-8 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tGlosa" size="sm" type="text" readOnly value={this.state.motivo} /> 
                                </div> 
                            </div> 
                        </div> 
                        <div className="form-group"> 
                            <div className="form-group row"> 
                                <div className="col-sm-4 col-form-label"> 
                                    Cond. Pago 
                                </div> 
                                <div className="col-sm-8 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tCondPago" size="sm" type="text" readOnly value={this.state.cond_pago} /> 
                                </div> 
                            </div> 
                        </div> 
                        <div className="form-group"> 
                            <div className="form-group row"> 
                                <div className="col-sm-4 col-form-label"> 
                                    Fecha Pago 
                                </div> 
                                <div className="col-sm-4 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tFechaPago" size="sm" type="text" readOnly value={this.state.fecha_venc} /> 
                                </div> 
                            </div> 
                            <div className="form-group row"> 
                                <div className="col-sm-3 col-form-label"> 
                                    Monto a Pagar 
                                </div> 
                                <div className="col-sm-3 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tMontoaPagar" size="sm" type="text" readOnly value={this.state.porc_total} /> 
                                </div> 
                                <div className="col-sm-3 col-form-label"> 
                                    Pagado 
                                </div> 
                                <div className="col-sm-3 col-form-label"> 
                                    <input className='form-control form-control-sm' name="tPagado" size="sm" type="text" readOnly value={this.state.pagado} /> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div> 
                        <div className="card-header cardalign w-50rd"> 
                            <div className="card-header border border-dashed border-end-0 border-start-0"> 
                                <div className="form-group"> 
                                    <div className="form-group row"> 
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div className="form-group"> 
                        <div className="form-group row"> 
                            <div className="col-sm-4 col-form-label"> 
                                Banco 
                            </div> 
                            <div className="col-sm-8 col-form-label"> 
                                <select name="cBancos" defaultValue={'0'} onChange={this.handleChange} style={{ width: 240 }}> 
                                    <option value='0' selected >Seleccionar...</option> 
                                    { 
                                        this.state.ListaBancos.map((v_itembancos) => 
                                            <option value={v_itembancos.Codigo}>{`${v_itembancos.Nombre}`}</option> 
                                        ) 
                                    } 
                                </select> 
                            </div> 
                        </div> 
                        <div className="form-group row"> 
                            <div className="col-sm-4 col-form-label"> 
                                Cuenta Bancaria 
                            </div> 
                            <div className="col-sm-8 col-form-label"> 
                                <select name="cCuentas" defaultValue={'0'} onChange={this.handleChange} style={{ width: 240 }}> 
                                    { 
                                        this.state.ListaCuentas.map((v_itemcuentas) => 
                                            <option value={v_itemcuentas.Codigo}>{`${v_itemcuentas.Nombre}`}</option> 
                                        ) 
                                    } 
                                </select> 
                            </div> 
                        </div> 
                        <div className="form-group row"> 
                            <div className="col-sm-4 col-form-label"> 
                                Tipo Requerimiento 
                            </div> 
                            <div className="col-sm-8 col-form-label"> 
                                <input className='form-control form-control-sm' name="tNombreRQ" size="sm" readOnly value={this.state.tiporq_nombre} onChange={this.handleChange} /> 
                            </div> 
                        </div> 
                        <div className="form-group row"> 
                            <div className="col-sm-4 col-form-label"> 
 
                                Programado a 
                            </div> 
                            <div className="col-sm-4 col-form-label"> 
                                <input className='form-control form-control-sm' type="date" name="tFechaProgramacion" size="sm" value={this.state.fecha_programado} onChange={this.handleChange} /> 
                            </div> 
                        </div> 
                        <div className="form-group row"> 
                            <div className="col-sm-4 col-form-label"> 
                                Monto a Pagar 
                            </div> 
                            <div className="col-sm-4 col-form-label"> 
                                <input className='form-control form-control-sm' name="tMontoProgramacion" size="sm" readOnly value={this.state.monto_programado} onChange={this.handleChange} /> 
                            </div> 
                        </div> 
                        <div className="form-group row"> 
                            <div className="col-sm-4 col-form-label"> 
 
                            </div> 
                            <BotonGuardar texto={this.state.Operacion + " Pago Programado"} sw_habilitado={true} eventoClick={() => EventoActualizar(this.state)} ></BotonGuardar> 
                        </div> 
                    </div> 
                </div> 
            </React.Fragment> 
        ) 
 
    } 
    async componentDidMount() { 
         
        const ListaBancos2 = await get_Bancos("TCN") 
        this.setState({"ListaBancos" : ListaBancos2}) 
        console.log("EMPRESA") 
        console.log(this.props.xCia) 
        const ListaBancos = await get_Bancos(this.props.xCia) 
        this.setState({"ListaBancos" : ListaBancos}) 
        console.log(ListaBancos) 
 
        const ListaCuentas = await get_CuentasBancarias(this.props.xCia, "XXXX") 
        this.setState({ "ListaCuentas": ListaCuentas }) 
 
    } 
} 
 
export default ReProgramacion;