import React from "react";
import { GrEdit, GrUpload, GrCodeSandbox, GrTask,GrView,GrMoney, GrArchive } from "react-icons/gr";

class ResultadosTabla extends React.Component {
    constructor(props) {
        super(props);

        this.state = { Desde: '', Hasta: '', Proveedor: '' }
    }

    handleCheck = (e) => {
        if (e.target.checked === true) {
            this.props.eventoMasivo()
        }

        if (e.target.checked === false) {            
            this.props.eventoQuitar(e.target.name)
        }
    }

    render() {
        const { Key, Fila, eventoVer,eventoTerminar, eventoAnular,eventoRepro,eventoHistorial } = this.props
        return (
            <tbody>
                <tr>
                    <td className={"td-cadena"}><input type="checkbox" name={Fila.indice} onChange={this.handleCheck} /> </td>
                    <td style={{ textAlign: "center" }}><button className="btn btn-info" onClick={() => eventoVer()}><GrView /></button></td> 
                    <td style={{ textAlign: "center" }}><button className="btn btn-success" onClick={() => eventoTerminar()}><GrMoney /></button></td>       
                    <td style={{ textAlign: "center" }}><button className="btn btn-warning" onClick={() => eventoRepro()}><GrMoney /></button></td>       
                    <td style={{ textAlign: "center" }}><button className="btn btn-danger" onClick={() => eventoAnular()}><GrMoney /></button></td>
                    <td style={{ textAlign: "center" }}><button className="btn btn-ligth" onClick={() => eventoHistorial()}><GrArchive /></button></td>
                    <td style={{ textAlign: "center" }}><button className={Fila.estado==="TERMINADO"?"btn btn-success":Fila.estado==="REPROGRAMADO"?"btn btn-warning":Fila.estado==="ANULADO"?"btn btn-danger":"btn btn-primary" }>{Fila.estado}</button> </td>       
                    <td className="td-cadena" id={Fila.indice}>{Fila.tiporq_nombre}</td>
                    <td className="td-cadena" >{Fila.fecha_programado}</td>
                    <td className="td-cadena" >{Fila.ruc}</td>
                    <td className="td-cadena" >{Fila.cc1_nombre}</td>
                    <td className="td-cadena" >{Fila.cliente_rs}</td>
                    <td className="td-cadena" >{Fila.tipo_documento}</td>
                    <td className="td-cadena" >{Fila.serie}</td>
                    <td className="td-cadena" >{Fila.nro}</td>
                    <td style={{ textAlign: "right" }} >{Number(Fila.monto_programado).toLocaleString('en-US', {minimumFractionDigits: 2})}</td>
                    <td className="td-cadena" >{Fila.usuario}</td>                    
                </tr>
            </tbody>
        )
    }
}
export default ResultadosTabla;

