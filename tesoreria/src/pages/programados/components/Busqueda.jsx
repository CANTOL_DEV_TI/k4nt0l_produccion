import React from "react";
import Title from "../../../components/Titulo";
import { BotonExcel, BotonNuevo, BotonBuscar, BotonGuardar, BotonGuardarMasivo } from "../../../components/Botones";
import { get_Bancos, get_CuentasBancarias } from "../../../services/bancosServices";

class Busqueda extends React.Component {

    constructor(props) {
        super(props);
        
        let hoy = new Date(Date.now())

        this.state = {
            emp_codigo: this.props.xCiaCodigo, todo_fpr: 'S', fprdesde: hoy.toISOString().slice(0, 10), fprdesde_d: hoy.toISOString().slice(0, 10), 
            fprhasta: hoy.toISOString().slice(0, 10), fprhasta_d: hoy.toISOString().slice(0, 10), todo_fv: 'S', fvdesde_d: hoy.toISOString().slice(0, 10), 
            fvdesde: hoy.toISOString().slice(0, 10), fvhasta_d: hoy.toISOString().slice(0, 10), fvhasta: hoy.toISOString().slice(0, 10), todo_proveedor: 'S', proveedor: '', todo_cc: 'S',
            cc: '', todo_cp: 'S', cond_pago: '', todo_usuario: 'S', usuario: '', moneda: "S/", todo_bancos: 'S', banco: '', todo_cuentas: 'S', cuenta: '', ListaBancos: [], ListaCuentas: []
        }


    }

    handleChange = async (e) => {
        this.setState({ emp_codigo: this.props.xCiaCodigo })

        if (e.target.name === "sMoneda") {
            this.setState({ moneda: e.target.value })
        }

        if (e.target.name === "cTProveedor") {
            if (e.target.checked === true) {
                this.setState({ todo_proveedor: 'N' })
            } else {
                this.setState({ todo_proveedor: 'S', proveedor: ' ' })
            }
        }

        if (e.target.name === "cTodoCC") {
            if (e.target.checked === true) {
                this.setState({ todo_cc: 'N' })
            } else {
                this.setState({ todo_cc: 'S', cc: ' ' })
            }
        }

        if (e.target.name === "cTodoCP") {
            if (e.target.checked === true) {
                this.setState({ todo_cp: 'N' })
            } else {
                this.setState({ todo_cp: 'S', cond_pago: ' ' })
            }
        }

        if (e.target.name === "cTodoUsuario") {
            if (e.target.checked === true) {
                this.setState({ todo_usuario: 'N' })
            } else {
                this.setState({ todo_usuario: 'S', usuario: ' ' })
            }
        }

        if (e.target.name === "cTodoFPR") {
            if (e.target.checked === true) {
                this.setState({ todo_fpr: 'N' })
            } else {
                this.setState({ todo_fpr: 'S', fprdesde: '', fprhasta: '' })
            }
        }

        if (e.target.name === "cTodoFV") {
            if (e.target.checked === true) {
                this.setState({ todo_fv: 'N' })
            } else {
                this.setState({ todo_fv: 'S', fvdesde: '', fvhasta: '' })
            }
        }

        if (e.target.name === "tProveedor") {
            this.setState({ proveedor: e.target.value })
        }

        if (e.target.name === "tCC") {
            this.setState({ cc: e.target.value })
        }

        if (e.target.name === "tCP") {
            this.setState({ cond_pago: e.target.value })
        }

        if (e.target.name === "tUsuario") {
            this.setState({ usuario: e.target.value })
        }

        if (e.target.name === "tDesdeFPR") {
            const desde_v = e.target.value.replace('-', '')
            const desde_v2 = desde_v.replace('-', '')
            this.setState({ fprdesde_d: e.target.value })
            this.setState({ fprdesde: desde_v2 })
        }

        if (e.target.name === "tHastaFPR") {
            const hasta_v = e.target.value.replace('-', '')
            const hasta_v2 = hasta_v.replace('-', '')
            this.setState({ fprhasta_d: e.target.value })
            this.setState({ fprhasta: hasta_v2 })
        }

        if (e.target.name === "tDesdeFV") {

            const desde_a = e.target.value.replace('-', '')
            const desde_b = desde_a.replace('-', '')

            this.setState({ fvdesde_d: e.target.value })
            this.setState({ fvdesde: desde_b })
        }

        if (e.target.name === "tHastaFV") {
            const hasta_a = e.target.value.replace('-', '')
            const hasta_b = hasta_a.replace('-', '')

            this.setState({ fvhasta_d: e.target.value })
            this.setState({ fvhasta: hasta_b })
        }

        if (e.target.name === "cTodoBAN") {
            if (e.target.checked === true) {
                this.setState({ todo_bancos: 'N' })
            } else {
                this.setState({ todo_bancos: 'S', banco: '' })
            }
        }

        if (e.target.name === "sBancos") {
            this.setState({ banco: e.target.value })
            const ListaBancosCuentas = await get_CuentasBancarias(this.props.xCiaCodigo, e.target.value)
            this.setState({ "ListaCuentas": ListaBancosCuentas })
            console.log(ListaBancosCuentas)
        }

        if (e.target.name === "cTodoCTA") {
            if (e.target.checked === true) {
                this.setState({ todo_cuentas: 'N' })
            } else {
                this.setState({ todo_cuentas: 'S', cuenta: '' })
            }
        }

        if (e.target.name === "sCuentas") {
            this.setState({ cuenta: e.target.value })
        }

    }


    render() {

        const { handleBusqueda, handleModal, xCia, xCiaCodigo, listaBancos } = this.props

        return (
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Lista de Pagos Programados : {xCia}</Title>
                            </div>
                        </div>
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <div className="d-flex flex-wrap gap-1">
                                    Moneda :
                                    <select name="sMoneda" onChange={this.handleChange}>
                                        <option value={"S/"}>Soles</option>
                                        <option value={"US$"}>Dolares</option>
                                    </select>
                                    Proveedor :
                                    <input type="checkbox" name="cTProveedor" onChange={this.handleChange} />
                                    <input name="tProveedor" placeholder="Ingrese Proveedor" onChange={this.handleChange} value={this.state.Proveedor} />
                                    Centro Costo
                                    <input type="checkbox" name="cTodoCC" onChange={this.handleChange} />
                                    <input name="tCC" placeholder="Ingrese CC" onChange={this.handleChange} value={this.state.cc} />                                
                                    Condicion de pago
                                    <input type="checkbox" name="cTodoCP" onChange={this.handleChange} />
                                    <input name="tCP" placeholder="Ingrese Condicion" onChange={this.handleChange} value={this.state.cond_pago} />

                                    Usuario
                                    <input type="checkbox" name="cTodoUsuario" onChange={this.handleChange} />
                                    <input name="tUsuario" placeholder="Ingrese Usuario" onChange={this.handleChange} value={this.state.usuario} />
                                </div>
                                <div>
                                    <p></p>
                                </div>
                                <div className="d-flex flex-wrap gap-1">
                                    Programado
                                    <input type="checkbox" name="cTodoFPR" onChange={this.handleChange} />
                                    Desde :
                                    <input type="date" Name="tDesdeFPR" placeholder="Desde" onChange={this.handleChange} value={this.state.fprdesde_d} />
                                    Hasta :
                                    <input type="date" name="tHastaFPR" placeholder="Hasta" onChange={this.handleChange} value={this.state.fprhasta_d} />

                                    Vencimiento
                                    <input type="checkbox" name="cTodoFV" onChange={this.handleChange} />
                                    Desde :
                                    <input type="date" Name="tDesdeFV" placeholder="Desde" onChange={this.handleChange} value={this.state.fvdesde_d} />
                                    Hasta :
                                    <input type="date" name="tHastaFV" placeholder="Hasta" onChange={this.handleChange} value={this.state.fvhasta_d} />
                                </div>
                                <div>
                                    <p></p>
                                </div>
                                <div className="d-flex flex-wrap gap-1">
                                    Banco
                                    <input type="checkbox" name="cTodoBAN" onChange={this.handleChange} />
                                    <select name="sBancos" onChange={this.handleChange}>
                                        <option value='0' selected >Seleccionar...</option>
                                        {
                                            this.state.ListaBancos.map((v_itembancos) =>
                                                <option value={v_itembancos.Codigo}>{`${v_itembancos.Nombre}`}</option>
                                            )
                                        }
                                    </select>
                                    Cuenta
                                    <input type="checkbox" name="cTodoCTA" onChange={this.handleChange} />
                                    <select name="sCuentas" onChange={this.handleChange}>
                                        {
                                            this.state.ListaCuentas.map((v_itemcuentas) =>
                                                <option value={v_itemcuentas.Codigo}>{`${v_itemcuentas.Nombre}`}</option>
                                            )
                                        }
                                    </select>

                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda(this.state)} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }

    async componentDidMount() {

        const ListaBancos = await get_Bancos('TCN')
        this.setState({ "ListaBancos": ListaBancos })

        const ListaBancos2 = await get_Bancos(this.props.xCiaCodigo)
        this.setState({ "ListaBancos": ListaBancos2 })

        const ListaBancosCuentas = await get_CuentasBancarias(this.props.xCiaCodigo, 'XXX')
        this.setState({ "ListaCuentas": ListaBancosCuentas })

    }

}

export default Busqueda;