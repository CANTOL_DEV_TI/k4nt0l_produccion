import React from "react";

class HistorialTabla extends React.Component {
    constructor(props) {
        super(props);

        this.state = { Desde: '', Hasta: '', Proveedor: '' }
    }

    render() {
        const { Key, Fila} = this.props
        return (
            <tbody>
                <tr>                 
                    <td className="td-cadena" >{Fila.fecha_programado}</td>
                    <td className="td-cadena" >{Fila.fecha_cambio}</td>
                    <td className="td-cadena" >{Fila.usuario}</td>                    
                </tr>
            </tbody>
        )
    }
}
export default HistorialTabla;