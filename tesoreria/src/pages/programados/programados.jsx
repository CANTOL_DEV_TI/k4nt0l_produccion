import React from "react";
import Title from "../../components/Titulo";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"
import Busqueda from "./components/Busqueda.jsx";
import VentanaModal from "../../components/VentanaModal.jsx"
import ResultadosTabla from "./components/ResultadosTabla.jsx";
import { getLista_Programados, Terminar_Programados, ReProgramar_Pagos, Anular_Programacion, Historial_Programacion, getTotalesLista_Programados } from "../../services/programadosServices.jsx";
import VerProgramacion from "./components/VerProgramacion.jsx";
import ReProgramacion from "./components/Reprogramacion.jsx";
import HistorialTabla from "./components/Historial.jsx";
import { get_Bancos } from "../../services/bancosServices.jsx";
import { BotonExcel } from "../../components/Botones.jsx";
import LoadingOverlay from 'react-loading-overlay';

class ListadoProgramados extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            resultados: [],
            isFetch: false,
            dataRegistro: null,
            showModal: false,
            showModalR: false,
            showModalH: false,
            tipoOpe: 'Find',
            Cia: '',
            CiaNombre: '',
            Historial: [],
            Total_Prog: 0,
            Cant_Prog: 0,
            ListaBancos: [],
            Bloqueo: false
        }
    }

    handleOperations = async (e) => {
        //console.log(e) 
        this.setState({Bloqueo : true})
        this.setState({ showModal: false })
        this.setState({ showModalR: false })
        let resultado = ""

        if (this.state.tipoOpe === 'Terminar') {            
            //console.log("terminar")            
            let responseJson = await Terminar_Programados(e)            
            resultado = responseJson
        }

        if (this.state.tipoOpe === 'Reprogramar') {            
            let responseJson = await ReProgramar_Pagos(e)
            resultado = responseJson.Mensaje
            console.log(resultado)
        }

        if (this.state.tipoOpe === 'Anular') {
            let responseJson = await Anular_Programacion(e)
            resultado = responseJson.Mensaje
        }

        alert(resultado)
        this.setState({ Bloqueo: false })
        
    }

    handleHistorial = async (e) => {
        const HistorialLista = await Historial_Programacion(e)
        this.setState({ Historial: HistorialLista, showModalH: true })        
    }

    handleBusqueda = async (e) => {       
        this.setState({ isFetch: true })
        const responseJson = await getLista_Programados(e)
        this.setState({ resultados: responseJson })        

        const responseJsonT = await getTotalesLista_Programados(e)
        this.setState({ Cant_Prog: responseJsonT.Cant_Prog, Total_Prog: responseJsonT.Total_Prog })

        this.setState({ isFetch: false })

    }

    handleCiaNombre = async (e) => {
        if (e === "CNT") {
            let xNombre = "CANTOL S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "DTM") {
            let xNombre = "DISTRIMAX S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }

        if (e === "TCN") {
            let xNombre = "TECNOPRESS S.A.C.";
            this.setState({ CiaNombre: xNombre });
        }
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad, Cia, Usuario, hist } = this.state

        return (
            <LoadingOverlay active={this.state.Bloqueo} spinner text="Procesando la operación..." >
                <React.Fragment>
                    <Busqueda handleBusqueda={this.handleBusqueda} xCia={this.state.CiaNombre} showComponente={'nuevo'} xCiaCodigo={this.state.Cia}
                        handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Find' })} HandleMasivo={this.handleProgMasivo} />

                    {isFetch && 'Cargando...'}
                    {(!isFetch && !resultados.length) && 'Sin Informacion...'}

                    <div className="table-responsive" style={{ height: '400px', display: "-ms-flexbox" }}>
                        <table className="table table-hover table-sm table-bordered table-striped" >
                            <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                                <tr >
                                    <th colSpan={12} ></th>
                                </tr>
                                <tr>
                                    <th className="align-middle" > </th>
                                    <th className="align-middle" >Ver</th>
                                    <th className="align-middle" >Terminar</th>
                                    <th className="align-middle" >Repro</th>
                                    <th className="align-middle" >Anular</th>
                                    <th className="align-middle" >Historial</th>
                                    <th className="align-middle" >Estado</th>
                                    <th className="align-middle" >Tipo RQ</th>
                                    <th className="align-middle" >Fecha Programado</th>
                                    <th className="align-middle" >Cliente Codigo</th>
                                    <th className="align-middle" >CC1 Nombre</th>
                                    <th className="align-middle" style={{ minWidth: '250px' }}>Proveedor R.S.</th>
                                    <th className="align-middle" style={{ minWidth: '120px' }}>Tipo Documento</th>
                                    <th className="align-middle" >Serie</th>
                                    <th className="align-middle" >Nro</th>
                                    <th className="align-middle" >Monto a Pagar</th>
                                    <th className="align-middle" >Usuario</th>
                                </tr>
                            </thead>

                            {resultados.map((registro, index) =>
                                <ResultadosTabla
                                    key={index}
                                    Fila={registro}
                                    eventoAnular={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Anular' })}
                                    eventoVer={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Ver' })}
                                    eventoTerminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Terminar' })}
                                    eventoRepro={() => this.setState({ showModalR: true, dataRegistro: registro, tipoOpe: 'Reprogramar' })}
                                    eventoHistorial={() => this.handleHistorial(registro)}
                                />
                            )}
                            <tfoot>

                            </tfoot>
                        </table>
                        <VentanaModal
                            show={this.state.showModal}
                            size={"xm"}
                            titulo={"Programacion de Pago"}
                            handleClose={() => this.setState({ showModal: false })}
                        >
                            <VerProgramacion
                                DataToView={this.state.dataRegistro}
                                Operacion={this.state.tipoOpe}
                                EventoActualizar={this.handleOperations}
                            />
                        </VentanaModal>
                        <VentanaModal
                            show={this.state.showModalR}
                            size={"xm"}
                            titulo={"Reprogramacion de pago"}
                            handleClose={() => this.setState({ showModalR: false })}
                        >
                            <ReProgramacion
                                DataToView={this.state.dataRegistro}
                                Operacion={this.state.tipoOpe}
                                EventoActualizar={this.handleOperations}
                                Usuario={this.state.Usuario}
                                xCia={this.state.Cia}
                            />
                        </VentanaModal>
                        <VentanaModal
                            show={this.state.showModalH}
                            size={"xs"}
                            titulo={"Historial de Reprogramaciones"}
                            handleClose={() => this.setState({ showModalH: false })}
                        >
                            <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                                <table className="table table-hover table-sm table-bordered table-striped" >
                                    <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                                        <tr >
                                            <th colSpan={3} ></th>
                                        </tr>
                                        <tr>
                                            <th className="align-middle" >Programado a </th>
                                            <th className="align-middle" >Cambiado el </th>
                                            <th className="align-middle" >Usuario</th>
                                        </tr>
                                    </thead>
                                    {this.state.Historial.map((registro, index) =>
                                        <HistorialTabla
                                            key={index}
                                            Fila={registro}
                                        />
                                    )}

                                </table>
                            </div>
                        </VentanaModal>
                        <VentanaBloqueo
                            show={this.state.VentanaSeguridad}
                        />

                    </div>
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header border border-dashed border-end-0 border-start-0">
                                <div className="row align-items-center gy-3">
                                    <div className="col-sm">
                                        <Title>TOTALES </Title>
                                        <div className="col-sm-auto">
                                            <div className="d-flex flex-wrap gap-1">
                                                CANTIDAD PROGRAMADOS <input name="tCant" readOnly value={this.state.Cant_Prog} /> ---- TOTAL PROGRAMADOS : <input name="tTotal" readOnly value={Number(this.state.Total_Prog).toLocaleString('en-US', { minimumFractionDigits: 2 })} />
                                                <BotonExcel textoBoton={"Exportar"} sw_habilitado={resultados.length > 0 ? true : false} listDatos={resultados} nombreArchivo={"Listado de Pagos"} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
            </LoadingOverlay>
        )
    }
    async componentDidMount() {
        const ValSeguridad = await Validar("TESAGE")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar, "Cia": ValSeguridad.cia, "Usuario": ValSeguridad.usuario })

        console.log(ValSeguridad)

        this.handleCiaNombre(ValSeguridad.cia)


    }
}

export default ListadoProgramados;