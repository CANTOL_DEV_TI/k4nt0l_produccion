import { Routes, Route } from "react-router-dom";

import LoginV from "../pages/login/login";
import ActuaPassword from "../pages/login/cambiocontraseña";
import ListadoPagosPendientes from "../pages/pagos/pagoslista";
import TipoRequerimiento from "../pages/tiporequerimiento/tiporequerimiento";
import ListadoProgramados from "../pages/programados/programados";
import React, { Suspense } from "react";
import ProtectedRoute from "../components/utils/ProtectedRoute.jsx"
import ListadoPresupuesto from "../pages/presupuesto/presupuesto.jsx";
import ListadoPresupuestoTotal from "../pages/presupuestototal/presupuestototal.jsx";
import Borradores from "../pages/borradores/borradores.jsx";
import BandejaBorradores from "../pages/bandeja/bandeja.jsx";
import CategoriaPresupuestal from "../pages/categoriaspresupuestales/categorias.jsx";
import ReporteBorradores from "../pages/reporteborradores/reporteBorradores.jsx";

export function MiRutas() {
    return (
        <Routes>
            <Route path="/" element={<LoginV />} />
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/cambiocontrasena" element={<ActuaPassword />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/programacion" element={<ListadoPagosPendientes />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/tiporq" element={<TipoRequerimiento />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/programados" element={<ListadoProgramados />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />}>
                <Route path="/presupuestolistado" element={<ListadoPresupuesto />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />}>
                <Route path="/presupuestototal" element={<ListadoPresupuestoTotal />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />}>
                <Route path="/listaborradores" element={<Borradores />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />}>
                <Route path="/bandeja" element={<BandejaBorradores />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />}>
                <Route path="/categoria" element={<CategoriaPresupuestal />} />
            </Route>            
            <Route element={<ProtectedRoute Redirige={"/"} />}>
                <Route path="/reporteborradores" element={<ReporteBorradores />} />
            </Route>
        </Routes>
    );
}