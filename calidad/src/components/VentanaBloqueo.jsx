import React from 'react'
import Modal from 'react-bootstrap/Modal';
import { useNavigate, useNavigation } from 'react-router-dom';

const VentanaBloqueo = ({ children, show, handleClose, titulo, size }) => {
    
    const Navegacion = useNavigate();

    return (
        <Modal show={show} onHide={handleClose} size={'xl'}> { /*backdrop="static" keyboard={false} */}
            { /*size="sm,lg,xl" */}
            <Modal.Header closeButton>
                <Modal.Title>{"Seguridad"}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {                                        
                    <>
                        <p>No tiene permiso, volver a la ventana anterior <button onClick={() => Navegacion(-1)}>Retroceder</button></p>
                    </>
                }                    
            </Modal.Body>
            {/* <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
            </Modal.Footer>  */}
        </Modal>
    );
};
export default VentanaBloqueo

/*<button onClick={() => history.goBack()}>No tiene permiso, volver a la ventana anterior</button> */