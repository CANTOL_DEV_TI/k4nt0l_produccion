import React from 'react'

export const InputTextBuscar = ({ onBuscarChange }) => {
    return (
        <input type="search" className="form-control" style={{ maxWidth: '210px', minWidth: '100px' }} placeholder="Buscar" onChange={onBuscarChange} name = "txt_buscar"/>
    );
};