import React from "react";
import { BotonGuardar } from "../../../components/Botones";

class TipoCaracteristicaEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { tipcar_codigo: '', tipcar_nombre: ''}
        } else {
            this.state = { tipcar_codigo: this.props.dataToEdit.tipcar_codigo, tipcar_nombre: this.props.dataToEdit.tipcar_nombre}
        }
    }

    handleChange = (e) => {
        
        if (e.target.name === 'tipcar_codigo') {
            this.setState({ tipcar_codigo: e.target.value })
        }

        if (e.target.name === 'tipcar_nombre') {
            this.setState({ tipcar_nombre: e.target.value })
        }

    }


    render() {
        const { eventOnclick, nameOpe } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">                        
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="tipcar_codigo" onChange={this.handleChange} value={this.state.tipcar_codigo} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Nombre : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="tipcar_nombre" onChange={this.handleChange} value={this.state.tipcar_nombre} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>                        
                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "tipcar_codigo": this.state.tipcar_codigo, "tipcar_nombre": this.state.tipcar_nombre})} texto={nameOpe} />
            </div>
        )
    }

}

export default TipoCaracteristicaEdicion;