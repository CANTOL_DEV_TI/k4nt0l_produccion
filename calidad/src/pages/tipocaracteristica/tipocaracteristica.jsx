import React from "react";


import {getFilter_TipoCaracteristica,save_TipoCaracteristica,update_TipoCaracteristica,delete_TipoCaracteristica} from "../../services/tipocaracteristicaService.jsx"
import Busqueda from "../tipocaracteristica/components/Busqueda";
import ResultadoTabla from "../tipocaracteristica/components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal";
import TipoCaractesticaEdicion from "./components/tipocaracteristicaEdicion.jsx";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo";

class TipoCaracteristica extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find'
        }
    }

    handleBusqueda = async (txtFind) => {
        const responseJson = await getFilter_TipoCaracteristica(txtFind)        
        this.setState({ resultados: responseJson, isFetch: false })        
    }

    handleSubmit = async (e) => {
        
        if (this.state.tipoOpe === 'Grabar') {
            const responseJson = await save_TipoCaracteristica(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar') {
            const responseJson = await update_TipoCaracteristica(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar') {
            const responseJson = await delete_TipoCaracteristica(e)
            console.log(responseJson)
        }


        this.setState({ showModal: false, tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad} = this.state

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} />


                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>                                
                                <th className="align-middle">Codigo</th>
                                <th className="align-middle">Nombre</th>                                
                                <th className="align-middle">Editar</th>
                                <th className="align-middle">Eliminar</th>                                
                            </tr>
                        </thead>
                        
                        {resultados.length > 0 &&
                        resultados.map((registro) =>
                            <ResultadoTabla
                                key={registro.tipcar_codigo}                                
                                tipcar_codigo={registro.tipcar_codigo}
                                tipcar_nombre={registro.tipcar_nombre}                                    
                                eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}                                
                            />
                        )}
                    </table>
                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({ showModal: false })}
                    titulo="Edición Tipo Caracteristica">
                    <TipoCaractesticaEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={this.state.tipoOpe}
                    />
                </VentanaModal>    
                <VentanaBloqueo show={VentanaSeguridad} />
            </React.Fragment>
        )

    }
    async componentDidMount() {
        const ValSeguridad = await Validar("CALTCA")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
    }
}

export default TipoCaracteristica;