import React from "react";


import {getPropiedadesArticulo,save_Propiedades,update_Propiedades,delete_Propiedades} from "../../services/articulospropiedadesServices.jsx"
import Busqueda from "../propiedades/components/Busqueda";
import ResultadoTabla from "../propiedades/components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal";
import PropiedadesEdicion from "./components/propiedadEdicion.jsx";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo";

class Propiedades extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find'
        }
    }

    handleBusqueda = async (txtFind) => {
        const responseJson = await getPropiedadesArticulo(txtFind)        
        this.setState({ resultados: responseJson, isFetch: false })        
    }

    handleSubmit = async (e) => {
        
        if (this.state.tipoOpe === 'Grabar') {
            console.log(e)
            const responseJson = await save_Propiedades(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar') {
            const responseJson = await update_Propiedades(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar') {
            const responseJson = await delete_Propiedades(e)
            console.log(responseJson)
        }


        this.setState({ showModal: false, tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad} = this.state

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} />


                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>
                                <th className="align-middle">Codigo Art</th>                                
                                <th className="align-middle">Articulo</th>
                                <th className="align-middle">C. Control</th>                                
                                <th className="align-middle">LIE</th>                                
                                <th className="align-middle">LIC</th>                                
                                <th className="align-middle">OBJ</th>                                
                                <th className="align-middle">LSC</th>                                
                                <th className="align-middle">LSE</th>                                
                                <th className="align-middle">Editar</th>
                                <th className="align-middle">Eliminar</th>                                
                            </tr>
                        </thead>
                        
                        {resultados.length > 0 &&
                        resultados.map((registro) =>
                            <ResultadoTabla
                                key={registro.cod_articulo + ' ' + registro.carcon_codigo}                                
                                cod_articulo={registro.cod_articulo}
                                carcon_codigo={registro.carcon_codigo}    
                                articulo={registro.articulo}                                
                                carcon_nombre={registro.carcon_nombre}
                                LIE={registro.LIE}                                    
                                LIC={registro.LIC}                                    
                                OBJ={registro.OBJ}                                    
                                LSC={registro.LSC}                                    
                                LSE={registro.LSE}                                    
                                eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}                                
                            />
                        )}
                    </table>
                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({ showModal: false })}
                    titulo="Edición Propiedades">
                    <PropiedadesEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={this.state.tipoOpe}
                    />
                </VentanaModal>    
                <VentanaBloqueo show={VentanaSeguridad} />
            </React.Fragment>
        )

    }
    async componentDidMount() {
        const ValSeguridad = await Validar("CALPRO")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
    }
}

export default Propiedades;