import React from "react";
import {GrEdit,GrAmazon,GrCodeSandbox,GrTask} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({cod_articulo,                        
                        articulo,                        
                        carcon_codigo,
                        carcon_nombre,
                        LIE,
                        LIC,
                        OBJ,
                        LSC,
                        LSE,
                        eventoEditar,
                        eventoEliminar                       
                        }) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena">{cod_articulo} </td>
                <td className="td-cadena">{articulo}</td>                
                <td className="td-cadena">{carcon_nombre}</td>
                <td className="td-cadena">{LIE}</td>
                <td className="td-cadena">{LIC}</td>
                <td className="td-cadena">{OBJ}</td>
                <td className="td-cadena">{LSC}</td>
                <td className="td-cadena">{LSE}</td>                
                <td style={{textAlign:"center"}}><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td style={{textAlign:"center"}}><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                
            </tr>
        </tbody>
    )

export default ResultadoTabla;