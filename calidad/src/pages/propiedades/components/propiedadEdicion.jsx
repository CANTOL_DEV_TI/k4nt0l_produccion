import React from "react";
import { BotonGuardar, BotonConsultar } from "../../../components/Botones";
import { BusquedaArticulos } from "../../inspeccion/components/BusquedaArticulos";
import { listaCaracteristicaControl } from "../../../services/inspeccionService";
import { getNombre_Item } from "../../../services/articulospropiedadesServices";
import VentanaModal from "../../../components/VentanaModal";

class PropiedadesEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { cod_articulo: '', carcon_codigo: '', LIE: 0, LIC: 0, OBJ: 0, LSC: 0, LSE: 0, articulo: '', showModalBA: false , listCC : []}
        } else {
            this.state = {
                cod_articulo: this.props.dataToEdit.cod_articulo, carcon_codigo: this.props.dataToEdit.carcon_codigo, LIE: this.props.dataToEdit.LIE,
                LIC: this.props.dataToEdit.LIC, OBJ: this.props.dataToEdit.OBJ, LSC: this.props.dataToEdit.LSC, LSE: this.props.dataToEdit.LSE,
                articulo: '', showModalBA: false, listCC : []
            }
        }
    }

    handleChange = (e) => {

        if (e.target.name === 'cod_articulo') {
            this.setState({ cod_articulo: e.target.value })
        }

        if (e.target.name === 'carcon_codigo') {
            this.setState({ carcon_codigo: e.target.value })
        }

        if (e.target.name === 'lie') {
            this.setState({ LIE: e.target.value })
            console.log("ENTRO")
        }

        if (e.target.name === 'lic') {
            this.setState({ LIC: e.target.value })
        }

        if (e.target.name === 'obj') {
            this.setState({ OBJ: e.target.value })
        }

        if (e.target.name === 'lsc') {
            this.setState({ LSC: e.target.value })
        }

        if (e.target.name === 'lse') {
            this.setState({ LSE: e.target.value })
        }

    }

    handleBusquedaArticulo = async (xCodigo, xNombre, xPlano) => {
        this.setState({ cod_articulo: xCodigo })
        this.setState({ articulo: xNombre })
        this.setState({ id_art_plano: xPlano })
        this.setState({ showModalBA: false })
    }

    handleShowBA = (e) => {
        this.setState({ showModalBA: true })
    }

    handleFindName = async (txtFind) => {
        const responseJson = await getNombre_Item(txtFind)        
        this.setState({ articulo: responseJson[0].nombre })
    }

    render() {
        const { eventOnclick, nameOpe } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Articulo : </div>
                            <div className="col-sm-2 col-form-label">
                                <input type="text" name="cod_articulo" onChange={this.handleChange} value={this.state.cod_articulo} />
                            </div>
                            <div className="col-sm-4 col-form-label">
                                <input type="text" name="articulo" style={{ minWidth: '250px' }} readOnly onChange={this.handleChange} value={this.state.articulo} /></div>
                            <div className="col-sm-2 col-form-label">
                                <BotonConsultar sw_habilitado={true} eventoClick={() => this.handleShowBA()} texto="SAP" />
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Caracteristica de Control : </div>
                            <div className="col-sm-2 col-form-label">
                                <select name="carcon_codigo" onChange={this.handleChange}>
                                    <option value="NONE">Seleccione...</option>
                                    {this.state.listCC.map((v_CC) =>
                                        this.state.carcon_codigo === v_CC.codigo ?
                                            <option selected value={v_CC.codigo}>{`${v_CC.nombre}`}</option>
                                            :
                                        <option value={v_CC.codigo}>{`${v_CC.nombre}`}</option>
                                    )
                                    }
                                </select>
                            </div>
                            {/*<input type="text" name="carcon_codigo" onChange={this.handleChange} value={this.state.carcon_codigo} />*/}
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="card-header cardalign w-50rd">
                                <div className="card-header border border-dashed border-end-0 border-start-0">
                                    <div className="form-group">
                                        <div className="form-group row">
                                            <div className="col-sm-1">

                                            </div>
                                            <div className="col-sm-2">
                                                <label className="input-limite"> LIE :</label>
                                                <input type="number" name="lie" className="form-control form-control-sm" value={this.state.LIE} onChange={this.handleChange}/>
                                            </div>
                                            <div className="col-sm-2">
                                                <label className="input-permitido"> LIC : </label>
                                                <input name="lic" className="form-control form-control-sm" value={this.state.LIC} onChange={this.handleChange}/>
                                            </div>
                                            <div className="col-sm-2">
                                                <label className="input-optimo"> Obj :</label>
                                                <input name="obj" className="form-control form-control-sm" value={this.state.OBJ} onChange={this.handleChange}/>
                                            </div>
                                            <div className="col-sm-2">
                                                <label className="input-permitido"> LSC : </label>
                                                <input name="lsc" className="form-control form-control-sm" value={this.state.LSC} onChange={this.handleChange}/>
                                            </div>
                                            <div className="col-sm-2">
                                                <label className="input-limite"> LSE : </label>
                                                <input name="lse" className="form-control form-control-sm" value={this.state.LSE} onChange={this.handleChange}/>
                                            </div>
                                            <div className="col-sm-2">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({
                    "cod_articulo": this.state.cod_articulo, "carcon_codigo": this.state.carcon_codigo,
                    "LIE": this.state.LIE, "LIC": this.state.LIC, "OBJ": this.state.OBJ, "LSC": this.state.LSC, "LSE": this.state.LSE
                })} texto={nameOpe} />
                <VentanaModal
                    show={this.state.showModalBA}
                    handleClose={() => this.setState({ showModalBA: false })}
                    titulo="Busqueda Articulo">
                    <BusquedaArticulos ResultadosBusqueda={this.handleBusquedaArticulo} />
                </VentanaModal>
            </div>
        )
    }
    async componentDidMount(){
        const ListaCC = await listaCaracteristicaControl();
        this.setState({ listCC: ListaCC })

        if (this.props.dataToEdit != null) {
            this.handleFindName(this.props.dataToEdit.cod_articulo)
        }
    }

}

export default PropiedadesEdicion;