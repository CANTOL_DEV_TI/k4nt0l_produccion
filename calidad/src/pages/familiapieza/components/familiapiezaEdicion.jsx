import React from "react";
import { BotonGuardar } from "../../../components/Botones";

class FamiliaPiezaEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { fampie_codigo: '', fampie_nombre: ''}
        } else {
            this.state = { fampie_codigo: this.props.dataToEdit.fampie_codigo, fampie_nombre: this.props.dataToEdit.fampie_nombre}
        }
    }

    handleChange = (e) => {
        
        if (e.target.name === 'fampie_codigo') {
            this.setState({ fampie_codigo: e.target.value })
        }

        if (e.target.name === 'fampie_nombre') {
            this.setState({ fampie_nombre: e.target.value })
        }

    }


    render() {
        const { eventOnclick, nameOpe } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">                        
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="fampie_codigo" onChange={this.handleChange} value={this.state.fampie_codigo} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Nombre : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="fampie_nombre" onChange={this.handleChange} value={this.state.fampie_nombre} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>                        
                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "fampie_codigo": this.state.fampie_codigo, "fampie_nombre": this.state.fampie_nombre})} texto={nameOpe} />
            </div>
        )
    }

}

export default FamiliaPiezaEdicion;