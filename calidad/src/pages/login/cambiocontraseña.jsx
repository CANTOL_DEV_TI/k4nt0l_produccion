import React from "react";

import { ActPassword } from "../../services/usuarioServices";

import { BotonLogin } from "../../components/Botones";

import VentanaModal from "../../components/VentanaModal";

import { validaToken } from "../../services/usuarioServices";


class ActuaPassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = { usuario_login: '', usuario_password_a: '', usuario_password_n: '', usuario_password_v : '' , showModal: false, Titulo: "", Mensaje: "" }

    }

    handleChange = (e) => {
        if (e.target.name === 'txtContraA') {
            this.setState({ usuario_password_a: e.target.value })
        }

        if (e.target.name === 'txtContraN') {
            this.setState({ usuario_password_n: e.target.value })
        }

        if (e.target.name === 'txtContraNV') {
            this.setState({ usuario_password_v: e.target.value })
        }

    }


    handleSubmit = async (e) => {
        console.log(e)
        const data = this.state
        const valida = Boolean

        this.valida = true

        console.log(data)

        if(data.usuario_password_a === data.usuario_password_n){
            this.setState({ Titulo: "Error al cambiar" });
            this.setState({ Mensaje: "Las nuevas contraseña nueva no debe ser igual a la antigua, revisar por favor" });
            this.valida = false
        }

        if(data.usuario_password_n !== data.usuario_password_v){
            this.setState({ Titulo: "Error al cambiar" });
            this.setState({ Mensaje: "Las nuevas contraseñas no coinciden, revisar por favor" });
            this.valida = false
        }
        
        console.log(this.valida)
        
        if(this.valida===true)
        {
            console.log(data)
            const responseJson = await ActPassword(data)
            console.log(responseJson)

            if (!responseJson.detail) {                  
                this.setState({ Mensaje: "Accion realizada - " + this.state.usuario_login + " - cambio de contraseña efectuado"});
                this.setState({ Titulo: "Sistema de Gestion CANTOL-ERP" });
            } else {
                this.setState({ Titulo: "Error de seguridad" });
                this.setState({ Mensaje: responseJson.detail });
            }
        }
    
        this.setState({ showModal: true, tipoOpe: 'Find' })

    }

    handleClose = (e) => {
        this.setState({ showModal: false })
    }

    render() {
        const { Titulo, Mensaje } = this.state

        return (
            <React.Fragment>
                <div className="container text-center mt-5">
                    <div className="row justify-content-center">
                        <div className="col-xl-4 col-lg-6">
                            <div className="card mt-4">
                                <div className="card-body p-4 bg-exclamation">
                                    {/*<img src={logocantol} className="img-fluid" width="164" />*/}
                                    <h1 className="h3 mb-3 fw-normal">Solicitud de Cambio de Contraseña</h1>
                                    <div className="form">
                                        <div className="form-floating">
                                            <input type="password" name="txtContraA" className="form-control" id="txtContraA" placeholder="Contraseña Antigua..." autoFocus onChange={this.handleChange} value={this.state.contraseña_antigua} />
                                            <label>Antigua Contraseña</label>
                                        </div>
                                        <div className="form-floating">
                                            <p />
                                        </div>
                                        <div className="form-floating">
                                            <input type="password" name="txtContraN" className="form-control" id="txtContraN" placeholder="Contraseña Nueva..." onChange={this.handleChange} value={this.state.contraseña_nueva} />
                                            <label>Contraseña Nueva</label>
                                        </div>
                                        <div className="form-floating">
                                            <p />
                                        </div>
                                        <div className="form-floating">
                                            <input type="password" name="txtContraNV" className="form-control" id="txtContraNV" placeholder="Validar Contraseña Nueva..." onChange={this.handleChange} value={this.state.contraseña_validar} />
                                            <label>Validar Contraseña Nueva</label>
                                        </div>
                                        <div className="form-floating">
                                            <p />
                                        </div>
                                        <div className="form-floating">
                                            <BotonLogin sw_habilitado={true} texto={"Cambio de Contraseña"} eventoClick={this.handleSubmit} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <VentanaModal
                    show={this.state.showModal}
                    handleClose={this.handleClose}
                    titulo={this.state.Titulo}
                    children={this.state.Mensaje}
                    size={"xl"}
                >
                </VentanaModal>
            </React.Fragment >
        )
    }
    async componentDidMount(){
        const token = sessionStorage.getItem("CDTToken");
        const valor = await validaToken(token);        
        this.state.usuario_login = valor.sub.substr(3)        
    }
};

export default ActuaPassword;