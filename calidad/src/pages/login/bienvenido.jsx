import React from "react";
import "../../assets/css/styleStandar.css"
import {Link, NavLink } from "react-router-dom";

class Bienvenido extends React.Component{

    render(){        
        return(
            <React.Fragment>
                <div>
                    <p>                        
                        <NavLink to={"/"}> ir al Inicio</NavLink>
                    </p>
                </div>
            </React.Fragment>
        )
    }
}

export default Bienvenido;