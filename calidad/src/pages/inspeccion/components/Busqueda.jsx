import React, { useState } from "react";
import Title from "../../../components/Titulo";
import { BotonExcel, BotonNuevo, BotonBuscar } from "../../../components/Botones";

class Busqueda extends React.Component {

    constructor(props) {
        super(props);

        this.state = { Desde: '', Hasta:'', Articulo:'' }
    }

    handleChange = (e) => {
        if(e.target.name==="tDesde")
        {
            this.setState({ Desde : e.target.value })
        }

        if(e.target.name==="tHasta")
        {
            this.setState({ Hasta : e.target.value})
        }

        if(e.target.name==="tArticulo")
        {
            this.setState({Articulo : e.target.value})
        }
    }


    render() {

        const { handleBusqueda, handleModal } = this.props

        return (
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Lista de Inspecciones</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    Articulo : <input name="tArticulo" placeholder="Ingrese Nombre" onChange={this.handleChange} value={this.state.Articulo}/>
                                    Desde : 
                                    <input type="date"
                                        name="tDesde"
                                        placeholder="Desde"
                                        onChange={this.handleChange}
                                        value={this.state.Desde}
                                    />
                                    Hasta : 
                                    <input type="date"
                                        name="tHasta"
                                        placeholder="Hasta"
                                        onChange={this.handleChange}
                                        value={this.state.Hasta}
                                    />

                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda(this.state.Desde,this.state.Hasta,this.state.Articulo)} />
                                                                                                        
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Busqueda;

//<BotonNuevo textoBoton={"Crear Nuevo"} sw_habilitado={true} eventoClick={() => handleModal(true)} />