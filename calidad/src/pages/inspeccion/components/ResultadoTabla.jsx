import React from "react"; 
import {GrEdit,GrAmazon,GrCodeSandbox,GrTask} from "react-icons/gr"; 
import {AiFillDelete,AiFillGoogleSquare} from "react-icons/ai"; 
 
 
const ResultadoTabla = ({inspeccionid,                         
                        fecha_inspeccion,  
                        fecha_registro,                        
                        familia_piezas, 
                        cod_articulo, 
                        articulo, 
                        id_art_plano, 
                        inspector, 
                        comentarios, 
                        maquina, 
                        estado, 
                        eventoEditar, 
                        eventoEliminar                        
                        }) => 
    ( 
        <tbody> 
            <tr> 
                <td className="td-cadena" id={inspeccionid}>{inspeccionid}</td>                 
                <td className="td-cadena" >{fecha_inspeccion}</td>                 
                <td className="td-cadena" >{fecha_registro}</td>    
                <td className="td-cadena" >{familia_piezas}</td> 
                <td className="td-cadena" >{cod_articulo}</td> 
                <td className="td-cadena" >{articulo}</td> 
                <td className="td-cadena" >{id_art_plano}</td> 
                <td className="td-cadena" >{inspector}</td>    
                <td className="td-cadena" >{maquina}</td>                  
                <td className="td-cadena"><label className={estado === 'CONFORME' ?  "btn btn-success btn-sm" : "btn btn-warning btn-sm"}>{estado}</label></td>                                
                <td style={{textAlign:"center"}}><button onClick={() => eventoEditar(true)}><GrEdit/></button></td> 
                <td style={{textAlign:"center"}}><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                 
            </tr> 
        </tbody> 
    ) 
 
export default ResultadoTabla;