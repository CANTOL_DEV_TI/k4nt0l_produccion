import React from "react" 
import { BotonGuardar, BotonConsultar, BotonAdd, BotonAtras } from "../../../components/Botones"; 
import { post_Formulas_Torno, get_propiedades } from "../../../services/formulasService"; 
//({ FilaNueva, BorrarFila, list_TC, list_CC }) 
class AddInspeccionDetalle extends React.Component { 
 
    constructor(props) { 
        super(props); 
        this.state = { 
            LIE: 0, LIC: 0, LSC: 0, LSE: 0, OBJ: 0, Tipo_Caracteristica: "", Caracteristica_Control: "", referencias: "", 
            instrumento: "", frecuencia_inspeccion_hora: 0, cantidad_muestra: 0, valor_inspeccion: 0, valor_inspeccion_2: 0, valor_inspeccion_3: 0 , color_muestra_1 : ""            
        } 
    } 
    /* 
        handleCalcular = async (e) => { 
            if (e.target.value !== "") { 
                const valores = { "valor_1": e.target.value } 
                const responseCalc = await post_Formulas_Torno(valores); 
                console.log(responseCalc) 
                this.setState({ LIE: responseCalc.LIE }) 
                this.setState({ LIC: responseCalc.LIC }) 
                this.setState({ LSC: responseCalc.LSC }) 
                this.setState({ LSE: responseCalc.LSE }) 
                this.setState({ OBJ: e.target.value }) 
            } 
        } 
    */ 
    handleGetPropiedades = async (pCarCon) => { 
        //console.log("entro", pCarCon) 
        if (pCarCon !== "") { 
            const filtros = { "cod_articulo": this.props.Cod_Articulo, "carcon_codigo": pCarCon } 
            const getpropiedades = await get_propiedades(filtros); 
            //console.log(getpropiedades) 
            this.setState({ LIE: getpropiedades.LIE }) 
            this.setState({ LIC: getpropiedades.LIC }) 
            this.setState({ LSC: getpropiedades.LSC }) 
            this.setState({ LSE: getpropiedades.LSE }) 
            this.setState({ OBJ: getpropiedades.OBJ }) 
        } 
    } 
 
    handleChange = (e) => { 
 
        if (e.target.name === "Tipo_Caracteristica") { 
            this.setState({ tipcar_codigo: e.target.value }) 
        } 
 
        if (e.target.name === "Caracteristica_Control") { 
            this.setState({ carcon_codigo: e.target.value }) 
            //this.handleGetPropiedades(this.state.carcon_codigo) 
 
        } 
 
        if (e.target.name === "referencias") { 
            this.setState({ referencias: e.target.value }) 
        } 
 
        if (e.target.name === "instrumento") { 
            this.setState({ instrumento: e.target.value }) 
        } 
 
        if (e.target.name === "frecuencia_inspeccion_hora") { 
            this.setState({ frecuencia_inspeccion_hora: e.target.value }) 
        } 
 
        if (e.target.name === "cantidad_muestra") { 
            this.setState({ cantidad_muestra: e.target.value }) 
        } 
 
        if (e.target.name === "valor_inspeccion") { 
            this.setState({ valor_inspeccion: e.target.value })             
        } 
 
        if (e.target.name === "valor_inspeccion_2") { 
            this.setState({ valor_inspeccion_2: e.target.value }) 
        } 
 
        if (e.target.name === "valor_inspeccion_3") { 
            this.setState({ valor_inspeccion_3: e.target.value }) 
        } 
 
    }; 
 
 
    handleSubmit = (e) => { 
        //console.log("valores", this.state) 
        this.props.OnSubmit(this.state) 
    }; 
 
    handleClose = (e) => { 
        this.props.closeDetalle = false 
    } 
 
    render() { 
        const { dataToEdit, nameOpe, OnSubmit, list_TC, list_CC, Cod_Articulo } = this.props 
        return ( 
            <div> 
                <div className="card-header cardalign w-50rd"> 
                    <div className="card-header border border-dashed border-end-0 border-start-0"> 
                        <div className="form-group"> 
                            <div className="form-group row"> 
                                <div className="col col-form-label"> 
                                    Tipo Caracteristica : <select name="Tipo_Caracteristica" onChange={this.handleChange}> 
                                        <option value="NONE">Seleccione...</option> 
                                        { 
 
                                            list_TC.map((v_TC) => 
                                                //this.state.fampie_codigo === v_TC.codigo ? 
                                                //    <option selected value={v_TC.codigo}>{`${v_item.nombre}`}</option> 
                                                //    : 
                                                <option value={v_TC.codigo}>{`${v_TC.nombre}`}</option> 
                                            ) 
                                        } 
                                    </select> 
                                </div> 
                            </div> 
                            <div className="form-group row"> 
                                <div className="col col-form-label"> 
                                    Caracteristica Control : <select name="Caracteristica_Control" onChange={this.handleChange}> 
                                        <option value="NONE">Seleccione...</option> 
                                        {list_CC.map((v_CC) => 
                                            //this.state.fampie_codigo === v_TC.codigo ? 
                                            //    <option selected value={v_TC.codigo}>{`${v_item.nombre}`}</option> 
                                            //    : 
                                            <option value={v_CC.carcon_codigo}>{`${v_CC.carcon_nombre}`}</option> 
                                        ) 
                                        } 
                                    </select> 
                                    <BotonConsultar sw_habilitado={true} eventoClick={() => this.handleGetPropiedades(this.state.carcon_codigo)} texto="SAP" /> 
                                </div> 
                            </div> 
                            <div className="form-group row"> 
                                <div className="card-header cardalign w-50rd"> 
                                    <div className="card-header border border-dashed border-end-0 border-start-0"> 
                                        <div className="form-group"> 
                                            <div className="form-group row"> 
                                                <div className="col-sm-1"> 
 
                                                </div> 
                                                <div className="col-sm-2"> 
                                                    <label className="input-limite"> LIE :</label> 
                                                    <input name="lie" className="form-control form-control-sm" readOnly value={this.state.LIE} /> 
                                                </div> 
                                                <div className="col-sm-2"> 
                                                    <label className="input-permitido"> LIC : </label> 
                                                    <input name="lic" className="form-control form-control-sm" readOnly value={this.state.LIC} /> 
                                                </div> 
                                                <div className="col-sm-2"> 
                                                    <label className="input-optimo"> Obj :</label> 
                                                    <input name="obj" className="form-control form-control-sm" readOnly value={this.state.OBJ} /> 
                                                </div> 
                                                <div className="col-sm-2"> 
                                                    <label className="input-permitido"> LSC : </label> 
                                                    <input name="lsc" className="form-control form-control-sm" readOnly value={this.state.LSC} /> 
                                                </div> 
                                                <div className="col-sm-2"> 
                                                    <label className="input-limite"> LSE : </label> 
                                                    <input name="lse" className="form-control form-control-sm" readOnly value={this.state.LSE} /> 
                                                </div> 
                                                <div className="col-sm-2"> 
 
                                                </div> 
                                            </div> 
                                        </div> 
                                    </div> 
                                </div> 
                            </div> 
                            <div className="form-group row"> 
                                <div className="col-sm-12"><span> . </span> </div> 
                            </div> 
                            <div className="form-group row"> 
                                <div className="col-sm-4"> 
                                    <label className={ parseFloat(this.state.valor_inspeccion) < parseFloat(this.state.LIE) || parseFloat(this.state.valor_inspeccion) > parseFloat(this.state.LSE) ? "btn btn-danger" : 
                                     (parseFloat(this.state.valor_inspeccion) > parseFloat(this.state.LIE) && parseFloat(this.state.valor_inspeccion) <= parseFloat(this.state.LIC)) ||  
                                     ((parseFloat(this.state.valor_inspeccion) < parseFloat(this.state.LSE) && parseFloat(this.state.valor_inspeccion) >= parseFloat(this.state.LSC))) ? "btn btn-warning": 
                                    (parseFloat(this.state.valor_inspeccion) > parseFloat(this.state.LIC) && parseFloat(this.state.valor_inspeccion) < parseFloat(this.state.LSC)) ? "btn btn-success" :  
                                     parseFloat(this.state.valor_inspeccion === parseFloat(this.state.OBJ))? "btn btn-success" : "btn btn-danger" } >Muestra 1</label> 
                                    <input type="number" name="valor_inspeccion" className="form-control form-control-sm" value={this.state.valor_inspeccion} onChange={this.handleChange} /> 
                                </div> 
                                <div className="col-sm-4"> 
                                    <label className={parseFloat(this.state.valor_inspeccion_2) < parseFloat(this.state.LIE) || parseFloat(this.state.valor_inspeccion_2) > parseFloat(this.state.LSE) ? "btn btn-danger" : 
                                     (parseFloat(this.state.valor_inspeccion_2) > parseFloat(this.state.LIE) && parseFloat(this.state.valor_inspeccion_2) <= parseFloat(this.state.LIC)) ||  
                                     ((parseFloat(this.state.valor_inspeccion_2) < parseFloat(this.state.LSE) && parseFloat(this.state.valor_inspeccion_2) >= parseFloat(this.state.LSC))) ? "btn btn-warning": 
                                    (parseFloat(this.state.valor_inspeccion_2) > parseFloat(this.state.LIC) && parseFloat(this.state.valor_inspeccion_2) < parseFloat(this.state.LSC)) ? "btn btn-success" :  
                                     parseFloat(this.state.valor_inspeccion_2 === parseFloat(this.state.OBJ))? "btn btn-success" : "btn btn-danger"  }>Muestra 2</label> 
                                    <input type="number" name="valor_inspeccion_2" className="form-control form-control-sm" value={this.state.valor_inspeccion_2} onChange={this.handleChange} /> 
                                </div> 
                                <div className="col-sm-4"> 
                                    <label className={parseFloat(this.state.valor_inspeccion_3) < parseFloat(this.state.LIE) || parseFloat(this.state.valor_inspeccion_3) > parseFloat(this.state.LSE) ? "btn btn-danger" : 
                                     (parseFloat(this.state.valor_inspeccion_3) > parseFloat(this.state.LIE) && parseFloat(this.state.valor_inspeccion_3) <= parseFloat(this.state.LIC)) ||  
                                     ((parseFloat(this.state.valor_inspeccion_3) < parseFloat(this.state.LSE) && parseFloat(this.state.valor_inspeccion_3) >= parseFloat(this.state.LSC))) ? "btn btn-warning": 
                                    (parseFloat(this.state.valor_inspeccion_3) > parseFloat(this.state.LIC) && parseFloat(this.state.valor_inspeccion_3) < parseFloat(this.state.LSC)) ? "btn btn-success" :  
                                     parseFloat(this.state.valor_inspeccion_3 === parseFloat(this.state.OBJ))? "btn btn-success" : "btn btn-danger"  }>Muestra 3</label> 
                                    <input type="number" name="valor_inspeccion_3" className="form-control form-control-sm" value={this.state.valor_inspeccion_3} onChange={this.handleChange} /> 
                                </div> 
                            </div> 
                            <div className="form-group row"> 
                                <div className="col col-form-label"> 
                                    Referencias : 
                                </div> 
                            </div> 
                            <div className="form-group row"> 
                                <div className="col-10"> 
                                    <textarea rows="5" cols="50" name="referencias" onChange={this.handleChange} ></textarea> 
                                </div> 
                            </div>                                                                           
                            <div className="form-group row"> 
                                <div className="col col-form-label"> 
                                    <BotonAdd textoBoton={"Agregar Calculo"} sw_habilitado={true} eventoClick={this.handleSubmit}></BotonAdd> 
                                </div> 
                            </div> 
                        </div> 
 
                    </div> 
                </div> 
            </div> 
        ) 
    } 
} 
export default AddInspeccionDetalle;