import React from "react";
import {GrEdit,GrAmazon,GrCodeSandbox,GrTask} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoBusqueda = ({cod_articulo,                        
                        articulo,                        
                        plano,
                        version,
                        estado,                        
                        eventoEditar,
                        eventoEliminar                       
                        }) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena" id={cod_articulo}>{cod_articulo}</td>                
                <td className="td-cadena" >{articulo}</td>                
                <td className="td-cadena" >{plano}</td>
                <td className="td-cadena" >{version}</td>
                <td className="td-cadena" >{estado}</td>                
                <td style={{textAlign:"center"}}><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td style={{textAlign:"center"}}><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                
            </tr>
        </tbody>
    )

export default ResultadoBusqueda;