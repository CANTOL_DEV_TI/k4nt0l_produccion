import React from "react";
import { useEffect, useState } from "react";
import { listaArticulos } from "../../../services/inspeccionService";
import ResultadoBusqueda from "./ResultadoBusquedaArticulo";
import { BotonSeleccionar } from "../../../components/Botones";

export function BusquedaArticulos({ ResultadosBusqueda }) {
    const [listArt, setlistArt] = useState([])

    useEffect(() => {

    })

    const handleFind = async (e) => {
        const responseJson = await listaArticulos(e.target.value)
        setlistArt(responseJson)
    }

    return (
        <div>
            <div className="card-header cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col col-form-label">
                                Articulo : <input type="text" name="tArticulo" onChange={handleFind} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="table-responsive">
                                < table className="table table-hover table-sm table-bordered" >
                                    <thead className="table-secondary text-center table-sm">
                                        <tr>
                                            <td className="col-sm-auto col-form-label">Codigo</td>
                                            <td className="col-sm-auto col-form-label">Nombre</td>
                                            <td className="col-sm-auto col-form-label">Plano</td>
                                            <td className="col-sm-auto col-form-label">Version</td>
                                            <td className="col-sm-auto col-form-label">Estado</td>
                                            <td className="col-sm-auto"></td>
                                        </tr>
                                    </thead>
                                    <tbody className="list">
                                        {listArt.length > 0 &&
                                            listArt.map((registro) =>
                                                <tr>
                                                    <td className="td-cadena" id={registro.codigo}>{registro.codigo}</td>
                                                    <td className="td-cadena" >{registro.nombre}</td>
                                                    <td className="td-cadena" >{registro.plano}</td>
                                                    <td className="td-cadena" >{registro.version}</td>
                                                    <td className="td-cadena" >{registro.estado}</td>
                                                    <td className="td-cadema" >
                                                        <span className="actions">
                                                            <button onClick={() => ResultadosBusqueda(registro.codigo, registro.nombre, registro.plano)}>
                                                                <BotonSeleccionar />
                                                            </button>
                                                        </span>

                                                    </td>
                                                </tr>
                                            )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


/*
onChange={this.handleFind}
eventoClick={() => handleBusqueda(registro.codigo,registro.nombre,registro.plano)}
eventoClick={() => borrarFilas(index)}
*/