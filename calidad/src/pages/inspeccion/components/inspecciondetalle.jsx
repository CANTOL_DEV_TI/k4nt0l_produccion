import React from "react";
import { BsFillTrashFill } from "react-icons/bs"

export function InspeccionDetalle({ datosRow, borrarFilas }) {

    return (
        < div className="table-responsive">
            {/* INICIO TABLA */}
            < table className="table table-hover table-sm table-bordered" >
                <thead className="table-secondary text-center table-sm">
                    <tr>                        
                        <td className="col-sm-1 col-form-label" style={{ minWidth: '50px' }}>Tipo Caracteristica</td>
                        <td className="col-sm-1 col-form-label" style={{ minWidth: '50px' }}>Caracteristica Control</td>                        
                        <td className="col-sm-auto col-form-label">LIE</td>
                        <td className="col-sm-auto col-form-label">LIC</td>
                        <td className="col-sm-auto col-form-label">OBJ</td>
                        <td className="col-sm-auto col-form-label">LSC</td>
                        <td className="col-sm-auto col-form-label">LSE</td>                        
                        <td className="col-sm-auto col-form-label">V.I. 1</td>                        
                        <td className="col-sm-auto col-form-label">V.I. 2</td>                        
                        <td className="col-sm-auto col-form-label">V.I. 3</td>                                                
                    </tr>
                </thead>
                <tbody className="list">

                    {datosRow.length > 0 &&
                        datosRow.map((datos, index) =>
                            <tr key={index + 1}>
                                <td className="td-cadena">{datos.tipcar_codigo}</td>                                                                
                                <td className="td-cadena">{datos.carcon_codigo}</td>                                
                                <td className="td-num">{datos.LIE}</td>                                
                                <td className="td-num">{datos.LIC}</td>
                                <td className="td-num">{datos.OBJ}</td>
                                <td className="td-num">{datos.LSC}</td>
                                <td className="td-num">{datos.LSE}</td>
                                <td className="td-num"><label className={parseFloat(datos.valor_inspeccion) === parseFloat(datos.OBJ) ?  "btn btn-success btn-sm" : parseFloat(datos.LIC) >= parseFloat(datos.valor_inspeccion) || parseFloat(datos.valor_inspeccion) <= parseFloat(datos.LSC)  ? parseFloat(datos.valor_inspeccion) <= parseFloat(datos.LIE) ? "btn btn-danger btn-sm" :  "btn btn-warning btn-sm" : "btn btn-danger btn-sm"}>{datos.valor_inspeccion}</label></td>     
                                <td className="td-num"><label className={parseFloat(datos.valor_inspeccion_2) === parseFloat(datos.OBJ) ?  "btn btn-success btn-sm" : parseFloat(datos.LIC) >= parseFloat(datos.valor_inspeccion_2) || parseFloat(datos.valor_inspeccion_2) <= parseFloat(datos.LSC)  ? parseFloat(datos.valor_inspeccion_2) <= parseFloat(datos.LIE) ? "btn btn-danger btn-sm" :  "btn btn-warning btn-sm" : "btn btn-danger btn-sm"}>{datos.valor_inspeccion_2}</label></td>
                                <td className="td-num"><label className={parseFloat(datos.valor_inspeccion_3) === parseFloat(datos.OBJ) ?  "btn btn-success btn-sm" : parseFloat(datos.LIC) >= parseFloat(datos.valor_inspeccion_3) || parseFloat(datos.valor_inspeccion_3) <= parseFloat(datos.LSC)  ? parseFloat(datos.valor_inspeccion_3) <= parseFloat(datos.LIE) ? "btn btn-danger btn-sm" :  "btn btn-warning btn-sm" : "btn btn-danger btn-sm"}>{datos.valor_inspeccion_3}</label></td>
                               
                                <td>
                                    <span className="actions">
                                        <button><BsFillTrashFill className="baja-btn" data-bs-t onClick={() => borrarFilas(index)} /></button>
                                    </span>
                                </td>
                            </tr>
                        )
                    }
                </tbody>
            </table >

            {/* FIN TABLA */}
        </div >
    );
}

/*
<td className="col-sm-auto col-form-label" >Instrumento</td>
                        <td className="col-sm-auto col-form-label" >F.I.H.</td>
                        <td className="col-sm-auto col-form-label" >Cant. Muestra</td>

 <td className="td-cadena">{datos.instrumento}</td>
                                <td className="td-cadena">{datos.frecuencia_inspeccion_hora}</td>
                                <td className="td-num">{datos.cantidad_muestra}</td>
*/