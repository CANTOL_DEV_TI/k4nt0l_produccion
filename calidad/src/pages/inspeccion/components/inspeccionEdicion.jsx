import React from "react";
import { BotonGuardar, BotonConsultar, BotonAdd, BotonAtras } from "../../../components/Botones";
import VentanaModal from "../../../components/VentanaModal";
import { getDetalleInspeccion, listaFamiliaPiezas, listaTipoCaracteristica, ListaEquipos,ListaEquiposxArticulo, listaCaracteristicaControlxArticulo } from "../../../services/inspeccionService";
import { InspeccionDetalle } from "./inspecciondetalle";
import { BusquedaArticulos } from "./BusquedaArticulos";
import AddInspeccionDetalle from "./inspeccionadddetalle";
import { GetInspector } from "../../../services/ValidaSesion";


class InspeccionEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = {
                inspeccionid: '', cod_articulo: '', fampie_codigo: '', id_art_plano: '', articulo: '', fecha_inspeccion: '', inspector: '', comentario: '', id_equipo: 0,
                detalles: [],
                showModal: true, listFP: [], listTC: [], listCC: [], listEQ: [], showModalBA: false, showModalDI: false
            }
        } else {
            this.state = {
                inspeccionid: this.props.dataToEdit.inspeccionid, cod_articulo: this.props.dataToEdit.cod_articulo, fampie_codigo: this.props.dataToEdit.fampie_codigo,
                id_art_plano: this.props.dataToEdit.id_art_plano, articulo: this.props.dataToEdit.articulo, fecha_inspeccion: this.props.dataToEdit.fecha_inspeccion,
                inspector: this.props.dataToEdit.inspector, comentario: this.props.dataToEdit.comentario, id_equipo: this.props.dataToEdit.id_equipo, detalles: [],
                showModal: true, listFP: [], listTC: [], listCC: [], listEQ: [], showModalBA: false, showModalDI: false
            }
        }
    }

    handleChange = (e) => {

        if (e.target.name === 'inspeccionid') {
            this.setState({ inspeccionid: e.target.value })
        }

        if (e.target.name === 'cod_articulo') {
            this.setState({ cod_articulo: e.target.value })
        }

        if (e.target.name === 'fampie_codigo') {
            this.setState({ fampie_codigo: e.target.value })
        }

        if (e.target.name === 'articulo') {
            this.setState({ articulo: e.target.value })
        }

        if (e.target.name === 'id_art_plano') {
            this.setState({ id_art_plano: e.target.value })
        }

        if (e.target.name === 'fecha_inspeccion') {
            this.setState({ fecha_inspeccion: e.target.value })
        }

        if (e.target.name === 'comentario') {

            this.setState({ comentario: e.target.value })
        }

        if (e.target.name === 'id_equipo') {
            this.setState({ id_equipo: e.target.value })
        }

    }

    handleBusquedaArticulo = async (xCodigo, xNombre, xPlano) => {
        this.setState({ cod_articulo: xCodigo })
        this.setState({ articulo: xNombre })
        this.setState({ id_art_plano: xPlano })
        this.setState({ showModalBA: false })

        const ListaEq = await ListaEquiposxArticulo(xCodigo);
        this.setState({ listEQ: ListaEq })
        
    }

    handleDetalle = async (txtID) => {
        const ListaDetalle = await fetch(getDetalleProceso(txtID))
        const rptaDet = await ListaDetalle.json()
        return rptaDet
    }

    handleSubmit = (newRow) => {
        console.log(newRow)
        this.setState({ detalles: [...this.state.detalles, newRow] })
        //[...Filas, newRow]

        //console.log('Todo')
        console.log(this.state.detalles)
        this.setState({ showModalDI: false })
    }

    handleShowNuevaLinea = async (e) => {
        
        console.log(this.state.cod_articulo)

        if (this.state.cod_articulo === "") {
            alert("Debes escoger un articulo antes de ingresar el detalle de la inspección.")
            this.setState({ showModalDI: false })
        } else {
            const ListaCC = await listaCaracteristicaControlxArticulo(this.state.cod_articulo);
            this.setState({ listCC: ListaCC })
            //console.log("LISTA",this.state.cod_articulo,this.state.listCC)
            
            this.setState({ showModalDI: true })
        }
        
    }

    HandleBorrarFila = (targetIndex) => {
        //console.log(this.state.detalles)
        //console.log(targetIndex)
        const items = this.state.detalles
        //delete items[targetIndex]
        items.splice(targetIndex, 1)
        //console.log(items)
        this.setState({ detalles: items })
    }

    handleShowBA = (e) => {
        this.setState({ showModalBA: true })
    }

    render() {
        const { dataToEdit, eventOnclick, nameOpe, EventoVolver } = this.props

        return (
            <div >
                <div className="card-header cardalign">
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="form-group">
                            <div className="form-group row auto">
                                <div className="col col-form-label ">
                                    <h1>Registro de Inspección</h1>
                                </div>
                            </div>
                            <div className="form-group row auto">
                                <div className="col-sm-auto"> </div>
                                <div className="col-sm-auto">
                                    <div className="row">
                                        <label>Nro de Inspección : </label>
                                    </div>
                                    <div className="row">
                                        <div class="col-sm-6">
                                            <input type="text" className="form-control form-control-sm" name="id_fmproceso" autoFocus onChange={this.handleChange} value={this.state.inspeccionid} readOnly />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <label>Familia Piezas : </label>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div class="col-sm-12">
                                            <select className="form-control form-control-sm" name="fampie_codigo" onChange={this.handleChange}>
                                                <option value='0'>Seleccionar...</option>
                                                {
                                                    this.state.listFP.map((v_item) =>
                                                        this.state.fampie_codigo === v_item.codigo ?
                                                            <option selected value={v_item.codigo}>{`${v_item.nombre}`}</option>
                                                            :
                                                            <option value={v_item.codigo}>{`${v_item.nombre}`}</option>
                                                    )
                                                }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div class="col-sm-auto">
                                            <label >Codigo Artículo : </label>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div class="col-sm-10">
                                            <input type="text" className="form-control form-control-sm" name="cod_articulo" onChange={this.handleChange} value={this.state.cod_articulo} />
                                        </div>
                                        <div class="col-sm-2">
                                            <BotonConsultar sw_habilitado={true} eventoClick={() => this.handleShowBA()} texto="SAP" />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div class="col-sm-auto">
                                            <label class="col-sm-auto ">Artículo : </label>
                                        </div>
                                        <div className="row">
                                            <div class="col-sm-12">
                                                <input type="text" className="form-control form-control-sm" name="articulo" onChange={this.handleChange} value={this.state.articulo} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div class="col">
                                            <label class="col-sm-auto">Codigo de Plano : </label>
                                        </div>
                                        <div className="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" className="form-control form-control-sm" name="id_art_plano" onChange={this.handleChange} value={this.state.id_art_plano} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div class="col-sm-auto">
                                            <label>Fecha Inspección : </label>
                                        </div>
                                        <div className="row">
                                            <div class="col-sm-10">
                                                <input type="date" className="form-control form-control-sm" name="fecha_inspeccion" onChange={this.handleChange}
                                                    value={this.state.fecha_inspeccion} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-auto">
                                            <label>Maquina : </label>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div class="col-sm-10">
                                            <select className="form-control form-control-sm" name="id_equipo" onChange={this.handleChange}>
                                                <option value='0'>Seleccionar...</option>
                                                {
                                                    this.state.listEQ.map((v_equipos) =>
                                                        this.state.id_equipo === v_equipos.id_equipo ?
                                                            <option selected value={v_equipos.id_equipo}>{`${v_equipos.equipo}`}</option>
                                                            :
                                                            <option value={v_equipos.id_equipo}>{`${v_equipos.equipo}`}</option>
                                                    )
                                                }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div class="col-sm-auto">
                                            <label>Inspector : </label>
                                        </div>
                                        <div className="row">
                                            <div class="col-sm-12">

                                                <input type="text" readOnly className="form-control form-control-sm" name="inspector" onChange={this.handleChange} value={this.state.inspector} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div class="col-sm-auto">
                                            <label>Comentario : </label>
                                        </div>
                                        <div className="row">
                                            <div class="col-sm-20">
                                                <textarea type="text" className="form-control form-control-sm" name="comentario" rows="5" cols={"20"} onChange={this.handleChange} value={this.state.comentario} />
                                            </div>
                                        </div>
                                    </div>

                                    <BotonAdd sw_habilitado={true} textoBoton={"Agregar Detalle"} eventoClick={this.handleShowNuevaLinea} />
                                </div>
                                <div className="col-sm-auto">
                                    <InspeccionDetalle datosRow={this.state.detalles} listaCC={this.state.listCC} listaTC={this.state.listTC} borrarFilas={this.HandleBorrarFila} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-1"></div>
                            <div className="col-sm-auto ">
                                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({
                                    "inspeccionid": this.state.inspeccionid, "cod_articulo": this.state.cod_articulo,
                                    "id_art_plano": this.state.id_art_plano, "fampie_codigo": this.state.fampie_codigo, "inspector": this.state.inspector,
                                    "fecha_inspeccion": this.state.fecha_inspeccion,
                                    "comentario": this.state.comentario, "id_equipo": this.state.id_equipo, "detalles": this.state.detalles
                                })} texto={"Guardar"} />
                                <BotonAtras sw_habilitado={true} textoBoton={"Atras"} eventoClick={EventoVolver} />
                            </div>
                            <div className="col-sm-auto"></div>
                        </div>
                    </div>
                </div>
                <VentanaModal
                    show={this.state.showModalBA}
                    handleClose={() => this.setState({ showModalBA: false })}
                    titulo="Busqueda Articulo">
                    <BusquedaArticulos ResultadosBusqueda={this.handleBusquedaArticulo} />
                </VentanaModal>
                <VentanaModal
                    show={this.state.showModalDI}
                    handleClose={() => this.setState({ showModalDI: false })}
                    titulo="Agregar Detalle"
                >
                    <AddInspeccionDetalle OnSubmit={this.handleSubmit} list_CC={this.state.listCC} list_TC={this.state.listTC} Cod_Articulo={this.state.cod_articulo} />
                </VentanaModal>
            </div>
        )
    }

    async componentDidMount() {
        const ListaFP = await listaFamiliaPiezas();
        this.setState({ listFP: ListaFP })

        const ListaTC = await listaTipoCaracteristica();
        this.setState({ listTC: ListaTC })

        //const ListaCC = await listaCaracteristicaControl();
        //this.setState({ listCC: ListaCC })

        const ListaCC = await listaCaracteristicaControlxArticulo("");
        this.setState({ listCC: ListaCC })

        //console.log(this.state.inspeccionid.length)
        if (this.state.inspeccionid.length > 0) {
            const ListaDetalle = await getDetalleInspeccion(this.state.inspeccionid)
            this.setState({ detalles: ListaDetalle })
            //console.log(ListaDetalle)
        }

        const aInspector = await GetInspector("")
        this.setState({ inspector: aInspector })

        //const ListaEq = await ListaEquiposxArticulo(this.state.cod_articulo);
        //this.setState({ listEQ: ListaEq })

        //console.log(this.props.dataToEdit.fecha_inspeccion)
    }

}

export default InspeccionEdicion;