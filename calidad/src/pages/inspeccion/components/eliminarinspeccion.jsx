import React from "react";
import { BotonGuardar,BotonCerrar } from "../../../components/Botones";

class EliminarInspeccion extends React.Component {

    render() {
        const { dataToEdit, eventOnclick, nameOpe, EventoVolver } = this.props
        return (
            <div>
                <div className="card-header cardalign w-50rd">
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="form-group">
                            <div className="form-group row">
                                <div className="col col-form-label">
                                    <h2>Desea eliminar la inspección {this.props.dataToEdit.inspeccionid}?</h2>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col col-form-label">
                                    <BotonGuardar texto={"Eliminar"} sw_habilitado={true} eventoClick={() =>eventOnclick(dataToEdit)}/>                                    
                                </div>
                                <div className="col col-form-label">
                                    <BotonCerrar textoBoton={"Cerrar..."} sw_habilitado={true} eventoClick={EventoVolver}/>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    async componentDidMount()
    {
        console.log(this.props.dataToEdit)
    }
}
export default EliminarInspeccion;