import React from "react"; 
import Busqueda from "../inspeccion/components/Busqueda.jsx" 
import ResultadoTabla from "../inspeccion/components/ResultadoTabla"; 
import InspeccionEdicion from "./components/inspeccionEdicion.jsx"; 
import { BotonNuevo } from "../../components/Botones.jsx"; 
import { GetInspector, Validar } from "../../services/ValidaSesion.jsx"; 
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"; 
import { delete_Inspeccion, getFilter_Inspeccion } from "../../services/inspeccionService.jsx"; 
import { save_Inspeccion,update_Inspeccion } from "../../services/inspeccionService.jsx"; 
import VentanaModal from "../../components/VentanaModal.jsx"; 
import EliminarInspeccion from "./components/eliminarinspeccion.jsx"; 
 
class Inspeccion extends React.Component { 
    constructor(props) { 
        super(props); 
 
        this.state = { 
            resultados: [], 
            isFetch: true, 
            dataRegistro: null, 
            tipoOpe: 'Find', 
            showComponente: 'Find', 
            Inspector: '', 
            Mensaje: '', 
            Usuario: '' 
        } 
    } 
 
    handleBusqueda = async (pDesde, pHasta, pArticulo) => { 
        const responseJson = await getFilter_Inspeccion(pDesde, pHasta, pArticulo) 
 
        this.setState({ resultados: responseJson, isFetch: false }) 
 
        console.log(this.state.resultados) 
 
    } 
 
    handleSubmit = async (e) => { 
        //console.log(e) 
        const valido = true 
 
        this.valido = true 
 
        if (e.cod_articulo === '') { 
            this.valido = false 
            alert("El codigo de articulo no puede ir vacio") 
        } 
 
        if (e.articulo === '') { 
            this.valido = false 
            alert("El nombre del articulo no puede ir vacio") 
        } 
 
        if (e.detalles.length === 0) { 
            this.valido = false 
            alert("No puede ir sin detalles") 
        } 
 
        if (e.fecha_inspeccion === ''){ 
            this.valido = false 
            alert("No puede ir sin fecha de inspección") 
        } 
 
        if (e.fampie_codigo === '') { 
            this.valido = false 
            alert("Seleccione la familia de piezas ") 
        } 
        console.log(this.valido,this.state.tipoOpe) 
        if (this.valido === true) { 
            if(this.state.tipoOpe === "nuevo"){ 
                const guardar_inspeccion = await save_Inspeccion(e)             
                alert(guardar_inspeccion) 
                this.setState({ showModal: false, tipoOpe: 'Find' }) 
            } 
            if(this.state.tipoOpe === "Actualizar"){ 
                const guardar_inspeccion = await update_Inspeccion(e)             
                alert(guardar_inspeccion) 
                this.setState({ showModal: false, tipoOpe: 'Find' }) 
            } 
        } 
    } 
 
    handleAtras = async (e) => {         
        this.setState({ showModalE: false }) 
        this.setState({ showModal: false, tipoOpe: 'Find' }) 
    } 
 
    handleDrop = async (e) => { 
        const Eliminar_Inspeccion = await delete_Inspeccion(e) 
        alert(Eliminar_Inspeccion) 
        this.setState({showModalE:false, tipoOpe:'Find'}) 
    } 
 
    render() { 
        const { isFetch, resultados, showModal, showModalE, VentanaSeguridad } = this.state 
 
        return ( 
            <React.Fragment> 
                {this.state.tipoOpe == 'nuevo' || this.state.tipoOpe == 'Actualizar' /*|| this.state.tipoOpe == 'Eliminar'*/ ? 
 
                    <InspeccionEdicion 
                        dataToEdit={this.state.dataRegistro} 
                        eventOnclick={this.handleSubmit} 
                        nameOpe={'Grabar'} 
                        showDetalle={false} 
                        EventoVolver={this.handleAtras} /> 
 
                    : 
                    <> 
                        <Busqueda handleBusqueda={this.handleBusqueda} showComponente={'nuevo'} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Find' })} /> 
 
                        {isFetch && 'Cargando'} 
                        {(!isFetch && !resultados.length) && 'Sin Informacion'} 
 
                        <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}> 
                            <table className="table table-hover table-sm table-bordered table-striped" > 
                                <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}> 
                                    <tr > 
                                        <th colSpan={12} ><BotonNuevo textoBoton={"Crear Nuevo"} sw_habilitado={true} eventoClick={() => this.setState({ tipoOpe: 'nuevo',dataRegistro : null })} /></th> 
                                    </tr> 
                                    <tr > 
                                        <th className="align-middle" >Inspeccion</th> 
                                        <th className="align-middle" >Fecha</th> 
                                        <th className="align-middle" >Registro</th> 
                                        <th className="align-middle" >Familia Piezas</th> 
                                        <th className="align-middle" >Codigo Articulo</th> 
                                        <th className="align-middle" >Nombre Articulo</th> 
                                        <th className="align-middle" >Plano</th> 
                                        <th className="align-middle" >Inspector</th> 
                                        <th className="align-middle" >Maquina</th> 
                                        <th className="align-middle" >Estado</th> 
                                        <th className="align-middle" >Editar</th> 
                                        <th className="align-middle" >Eliminar</th> 
                                    </tr> 
                                </thead> 
 
                                {resultados.map((registro) => 
                                    <ResultadoTabla 
                                        key={registro.inspeccionid} 
                                        inspeccionid={registro.inspeccionid} 
                                        fecha_inspeccion={registro.fecha_inspeccion} 
                                        fecha_registro={registro.fecha_registro} 
                                        familia_piezas={registro.familiapiezas} 
                                        cod_articulo={registro.cod_articulo} 
                                        articulo={registro.articulo} 
                                        id_art_plano={registro.id_art_plano} 
                                        inspector={registro.inspector} 
                                        maquina={registro.maquina} 
                                        estado={registro.estado} 
                                        eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })} 
                                        eventoEliminar={() => this.setState({ showModalE: true, dataRegistro: { "inspeccionid": registro.inspeccionid, "inspector": registro.inspector, "usuario": this.state.Inspector }, tipoOpe: 'Eliminar' })} 
                                    /> 
                                )} 
                                <tfoot> 
 
                                </tfoot> 
                            </table> 
                            <VentanaBloqueo 
                                //show={this.state.VentanaSeguridad} 
                                show={false} 
                            /> 
                            <VentanaModal 
                                show={this.state.showModalE} 
                            > 
                                <EliminarInspeccion  
                                dataToEdit={this.state.dataRegistro} 
                                eventOnclick={this.handleDrop} 
                                nameOpe={'Eliminar'} 
                                showDetalle={false} 
                                EventoVolver={this.handleAtras} ></EliminarInspeccion> 
                            </VentanaModal> 
                        </div> 
                    </> 
                } 
 
            </React.Fragment> 
        ) 
 
    } 
    async componentDidMount() {         
        const ValSeguridad = await Validar("CALINS") 
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar }) 
                 
        const InspectorAut = await GetInspector() 
        this.setState({ "Inspector": InspectorAut })         
    } 
} 
 
export default Inspeccion;