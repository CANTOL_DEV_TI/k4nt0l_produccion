import React from "react";
import { BotonGuardar } from "../../../components/Botones";

class CaracteristicaControlEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { carcon_codigo: '', carcon_nombre: ''}
        } else {
            this.state = { carcon_codigo: this.props.dataToEdit.carcon_codigo, carcon_nombre: this.props.dataToEdit.carcon_nombre}
        }
    }

    handleChange = (e) => {
        
        if (e.target.name === 'carcon_codigo') {
            this.setState({ carcon_codigo: e.target.value })
        }

        if (e.target.name === 'carcon_nombre') {
            this.setState({ carcon_nombre: e.target.value })
        }

    }


    render() {
        const { eventOnclick, nameOpe } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">                        
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="carcon_codigo" onChange={this.handleChange} value={this.state.carcon_codigo} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Nombre : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="carcon_nombre" onChange={this.handleChange} value={this.state.carcon_nombre} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>                        
                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "carcon_codigo": this.state.carcon_codigo, "carcon_nombre": this.state.carcon_nombre})} texto={nameOpe} />
            </div>
        )
    }

}

export default CaracteristicaControlEdicion;