import { validaToken, validaAcceso } from "./usuarioServices";
import { validaInspector } from "./inspectorServices";


export async function Validar(Opcion) {

    try {
        const Val = { Validar: false, usuario: "", cia: "" };

        const token = sessionStorage.getItem("CDTToken", "XXXXX");

        if (token === "") {
            Val.Validar = true

        } else {
            if (token === null) {
                Val.Validar = true

            } else {

                const valor = await validaToken(token);
                const usuario_login = valor.sub.substr(3)
                const usuario_empresa = valor.sub.substr(0, 3)

                const evaluar = { "usuario_login": usuario_login, "empresa_codigo": usuario_empresa, "modulo_codigo": Opcion }

                const responseAcc = await validaAcceso(evaluar)

                if (responseAcc.acceso === 0) {
                    if (Opcion !== "") {
                        Val.Validar = true
                        Val.cia = usuario_empresa
                        Val.usuario = ""
                    } else {
                        if (Opcion === "Sesion") {
                            Val.Validar = false
                        } else {
                            Val.Validar = true
                        }

                        Val.cia = usuario_empresa
                        Val.usuario = usuario_login
                    }
                } else {
                    Val.Validar = false
                    Val.cia = usuario_empresa
                    Val.usuario = usuario_login
                }
            }
        }

        return Val

    }
    catch
    {
        return { "Validar": true, "cia": "", "usuario": "" }
    }
};

export async function GetInspector() {
    const inspector = { "inspector": "" };
    const token = sessionStorage.getItem("CDTToken", "XXXXX");

    if (token === "") {
        inspector.inspector = "NONE"
        console.log("No entra Vacio")
    } else {
        if (token === null) {
            inspector.inspector = "NONE"
            console.log("No entra Null")
        } else {

            const valor = await validaToken(token);
            const usuario_login = valor.sub.substr(3)
            //const usuario_empresa = valor.sub.substr(0, 3)

            //const evaluar = { "usuario_login": usuario_login }

            const InspectorV = await validaInspector(usuario_login)

            console.log(InspectorV)

            if (InspectorV === 1) {
                inspector.inspector = usuario_login
            } else {
                inspector.inspector = "NONE"
            }

        }
    }

    return inspector.inspector

}