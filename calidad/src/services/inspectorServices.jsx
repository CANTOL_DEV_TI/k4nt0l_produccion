import {Url} from '../constants/global'

//const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url


const url = `${meServidorBackend}/calidad/inspector`;

//const token = await localStorage.getItem("CDTToken")    

//const cabecera = {'Content-type': 'application/json; charset=UTF-8',"Authorization": "Bearer " + token}
const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function validaInspector(pInspector){   
    const requestOptions = {
        method: 'GET',        
        headers: cabecera
        //body: JSON.stringify(meJson)
    };
    console.log(`${url}/ValidaInspector/${pInspector}`)
    const response = await fetch(`${url}/ValidaInspector/${pInspector}`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}