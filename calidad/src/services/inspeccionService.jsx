import {Url} from '../constants/global'

//const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url


const url = `${meServidorBackend}/calidad/inspeccion`;

//const token = await localStorage.getItem("CDTToken")    

//const cabecera = {'Content-type': 'application/json; charset=UTF-8',"Authorization": "Bearer " + token}
const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function getFilter_Inspeccion(pDesde,pHasta,pArticulo) {
    /*if (pDesde.trim() === '') {
        pHasta = '2020-01-01'
    }

    if (pDesde.trim() === '') {
        pHasta = '2025-12-31'
    }*/

    if (pArticulo.trim() === '') {
        pArticulo = '%20'
    }
    
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${url}/${pDesde} - ${pHasta} - ${pArticulo}`,requestOptions)
    const responseJson = await response.json()
    console.log(responseJson)
    return responseJson
}

export async function save_Inspeccion(meJson){   
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${url}/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function update_Inspeccion(meJson){    
    const requestOptions = {
        method: 'PUT',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${url}/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function delete_Inspeccion(meJson){  
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,//{ 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${url}/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function listaCaracteristicaControl()
{
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${url}/ListaCaracteristicaControl`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function listaFamiliaPiezas()
{
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }

    const response = await fetch(`${url}/ListaFamiliaPiezas`,requestOptions)
    const responseJson = await response.json()
    console.log(responseJson)
    return responseJson
}

export async function listaTipoCaracteristica()
{
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${url}/ListaTipoCaracteristica`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function listaArticulos(pFiltro)
{
    if (pFiltro.trim() === '') {
        pFiltro = '%20'
    }

    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${url}/ListaArticulos/${pFiltro}`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getDetalleInspeccion(pFiltro)
{
    if(pFiltro.trim() === ''){
        pFiltro='%20'
    }

    const requestOptions = {
        method : 'GET',
        headers : cabecera
    }
    console.log(`${url}/ListaDetalles/${pFiltro}`)
    const response = await fetch(`${url}/ListaDetalles/${pFiltro}`)
    const responseJson = await response.json()
    return responseJson
}

export async function ListaEquipos()
{
    const requestOptions = {
        method : 'GET',
        headers : cabecera
    }

    const response = await fetch(`${meServidorBackend}/produccion/Equipo/ListaEquipos/`)
    const responseJson = await response.json()
    return responseJson
}

export async function listaCaracteristicaControlxArticulo(pArticulo)
{
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/calidad/caracteristicacontrol/listaxarticulo/${pArticulo}`,requestOptions) 
    const responseJson = await response.json()
    return responseJson
}

export async function ListaEquiposxArticulo(pArticulo)
{
    const requestOptions = {
        method : 'GET',
        headers : cabecera
    }

    const response = await fetch(`${meServidorBackend}/calidad/inspeccion/listaEquiposxArticulo/${pArticulo}`)
    const responseJson = await response.json()
    return responseJson
}