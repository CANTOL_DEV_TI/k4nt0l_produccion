import {Url} from '../constants/global'

//const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url


const url = `${meServidorBackend}/calidad/familiapiezas`;

//const token = await localStorage.getItem("CDTToken")    

//const cabecera = {'Content-type': 'application/json; charset=UTF-8',"Authorization": "Bearer " + token}
const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function getFilter_FamiliaPiezas(txtFind) {
    if (txtFind.trim() === '') {
        txtFind = '%20'
    }
    
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${url}/${txtFind}`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function get_FamiliaPiezas_activo() {
    let url_id = `${url}/Activos/`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}


export async function save_FamiliaPiezas(meJson){   
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${url}/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function update_FamiliaPiezas(meJson){    
    const requestOptions = {
        method: 'PUT',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${url}/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function delete_FamiliaPiezas(meJson){  
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,//{ 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${url}/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}