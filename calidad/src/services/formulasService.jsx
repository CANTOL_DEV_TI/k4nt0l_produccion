import {Url} from '../constants/global'

const meServidorBackend = Url

const url = `${meServidorBackend}/calidad/formulas`;

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function post_Formulas_Torno(meJson)
{
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${url}/formula_torno`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function get_propiedades(meJson)
{
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${url}/propiedadesarticulo`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}