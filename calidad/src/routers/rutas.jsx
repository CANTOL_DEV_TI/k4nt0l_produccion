import React from "react";
import { Routes, Route} from "react-router-dom";
import LoginV from "../pages/login/login";
import CaracteriscaControl from "../pages/caracteristicacontrol/caracteristicacontrol";
import FamiliaPieza from "../pages/familiapieza/familiapieza";
import TipoCaracteristica from "../pages/tipocaracteristica/tipocaracteristica";
import Inspeccion from "../pages/inspeccion/inspeccion";
import ActuaPassword from "../pages/login/cambiocontraseña";
import Propiedades from "../pages/propiedades/propiedades";
import ProtectedRoute from "../components/utils/ProtectedRoute";

export function MiRutas() {

    return (
        <Routes>
            <Route path="/" element={<LoginV />} />
            <Route element={<ProtectedRoute Redirige={"/"}/>} >
              <Route path="/cambiocontrasena" element={<ActuaPassword />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"}/>} >
              <Route path="/caracteristicacontrol" element={<CaracteriscaControl />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"}/>} >
              <Route path="/familiapieza" element={<FamiliaPieza />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"}/>} >
              <Route path="/tipocaracteristica" element={<TipoCaracteristica />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"}/>} >
              <Route path="/inspecciones" element={<Inspeccion />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"}/>} >
              <Route path="/propiedades" element={<Propiedades />} />
            </Route>
          </Routes>
    );
}