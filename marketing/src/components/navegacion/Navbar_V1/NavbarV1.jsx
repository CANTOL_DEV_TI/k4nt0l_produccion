import React, { useState } from "react";

import {
    Header,
    Menu_bar, Bt_menu,
    Nav, Ul, Li, A, Span, Caret,
    Children, Children_Li, Children_A, Children_Span
} from "./NavbarV1.elements";

// import {ReactComponent as ReactLogo} from './static/logoSVG.svg';
import meLogo from '../assets/cantol.png';


import './navbarV1_Tecnopress.css'
import './static/fonts.css'
// import {AddLibrary} from "./upload_JsScript";

import { NavLink } from "react-router-dom";
import logo from "../assets/cantol.png";


const NavbarV1 = () => {

    const [activeNav, setActiveNav] = useState("nav_menu");

    const [activeChildren, setActiveChildren] = useState("children")
    const navToggle = () => {
        activeNav === "nav_menu"
            ? setActiveNav("nav_menu nav_active")
            : setActiveNav("nav_menu");
    };

    const childrenToggle = () => {
        activeChildren === "children"
            ? setActiveChildren("children children_active")
            : setActiveChildren("children");

        console.log(activeChildren)
    };

    const handleClear=()=>{
        sessionStorage.clear("")
    }
    
    return (
        <header>

            <div className={"menu_bar"} >                
                <a href="#" className={"bt-menu"}><span className={"icon-menu"} onClick={navToggle}></span><img src={meLogo} className="LogoEmpresa" /></a>
            </div>

            {/*<img src={meLogo} className="LogoEmpresa" />*/}

            <nav className={activeNav}>
                <ul>
                {/*<li><NavLink to="http://192.168.5.23/puntos/"><span className={"icon-home"}></span>Puntos</NavLink></li>*/}
                    <li><NavLink to="/familia">Maestro de artículos<span className={""}></span></NavLink></li>
                    <li className={"submenu"}>
                        <NavLink to="" onClick={() => { childrenToggle; }}><span className={"icon-rocket"}></span>Productos originales<span className={"caret icon-chevron-thin-down"}></span></NavLink>
                        <ul className={activeChildren}>
                            <li><NavLink to="/productos">Generación de QR<span className={"icon-dot-single"}></span></NavLink></li>
                            {/*<li><NavLink to="/familia">Maestro de artículos<span className={"icon-dot-single"}></span></NavLink></li>*/}
                            <li><NavLink to="/validacion">Validación de productos<span className={"icon-dot-single"}></span></NavLink></li>

                        </ul>
                    </li>               
                    <li className={"submenu"}>
                        <NavLink to="" onClick={childrenToggle}><span className={"icon-user"}></span>Usuario<span className={"caret icon-chevron-thin-down"}></span></NavLink>
                        {/*<ul className={'children'}>*/}
                        <ul className={activeChildren}>
                            <li><NavLink to="/cambiocontrasena">Cambio de Contraseña<span className={"icon-dot-single"}></span></NavLink></li>                            
                            <li><NavLink to="/login">Inicio de Sesion<span className={"icon-dot-single"}></span></NavLink></li>
                        </ul>
                    </li>                
                    {/*<li><NavLink to="http://192.168.5.21/" onClick={handleClear}><span className={"icon-mail"}></span>Salir</NavLink></li>*/}
                    <li><a href="http://192.168.5.21/" onClick={handleClear}><span className="icon-mail"></span>Salir</a></li>
                </ul>

            </nav>
        </header>
    );
};

export default NavbarV1;


