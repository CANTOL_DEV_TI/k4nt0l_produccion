import axios from 'axios'

import {Url_Productoori} from '../../../../../marketing/src/constants/global'
const BASE_URL = Url_Productoori

const API_URL_OBTENER_FAMILIA = BASE_URL + '/productooriginal/familia/obtenerfamilia'
const API_URL_LISTA_FAMILIA = BASE_URL + '/productooriginal/familia/listarfamilia'
const API_URL_LISTAR_OBTENER_PRODUCTO = BASE_URL + '/productooriginal/familia/obtenerproductofamilia'
const API_URL_BUSCAR_FAMILIA = BASE_URL + '/productooriginal/familia/buscarfamilia/'

const obtenerfamilia = async()=>{
    try{
        const response = await axios.get(API_URL_OBTENER_FAMILIA)
        console.log("pintame response:", response.data)
        return response.data
    }catch(error){
        throw new Error('Error al listar familia', error)
    }
}



const listarfamilia = async(page_num, pagesize) => {
    try{
        const response = await axios.get(`${API_URL_LISTA_FAMILIA}?page_num=${page_num}&page_size=${pagesize}`);
        console.log("pintame response:", response.data)
        return response.data
    }catch(error){
        throw new Error('Error al listar familia', error)
    }
}

const listarProductosPorFamilia = async(e) => {
    try{
        console.log("muestrame id antes de enviar:",e)
        const response = await axios.post(`${API_URL_LISTAR_OBTENER_PRODUCTO}?idfamilia=${e}`);
        return response.data
    }catch(error){
        throw new Error('Error al lista producto por familia', error)

    }
}

const buscarfamilia = async(e) => {
    try{
        const response = await axios.post(API_URL_BUSCAR_FAMILIA,e)
        return response.data
    }catch(error){
        throw new Error('Error al buscar categoria')
    }
}


export {listarfamilia, obtenerfamilia, listarProductosPorFamilia, buscarfamilia}