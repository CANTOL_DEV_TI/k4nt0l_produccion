import axios from 'axios'

import {Url_Productoori} from '../../../../../marketing/src/constants/global'
const BASE_URL = Url_Productoori

const API_URL_LISTAR = BASE_URL + '/productooriginal/cliente/listarcliente'
const API_URL_BUSCAR_DETALLE = BASE_URL + '/productooriginal/cliente/buscardetallecliente/'


const listarcliente = async(page_num, page_size, numeroserie) => {
    try{
        //const response = await axios.post(`${API_URL_LISTAR}?numeroserie=${e}`)
        const response = await axios.get(`${API_URL_LISTAR}?page_num=${page_num}&page_size=${page_size}&numeroserie=${numeroserie}`);
        console.log("pintame listas de cliente:",response)
        return response.data
    }catch(error){
        throw new Error('Error al listar cliente')
    }
}

const buscardetallecliente= async(numserie, e) => {
    try{
        console.log("muestrame busqueda:",e)
        const response = await axios.post(API_URL_BUSCAR_DETALLE, {
            "numserie": numserie,
            "data": e
        })
        return response.data
    }catch(error) {
        throw new Error("Error al buscar detalle del producto")

    }
}

export {listarcliente, buscardetallecliente}