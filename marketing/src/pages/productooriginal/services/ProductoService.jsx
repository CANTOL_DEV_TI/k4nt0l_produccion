import axios from 'axios'

import {Url_Productoori} from '../../../../../marketing/src/constants/global'
const BASE_URL = Url_Productoori

const API_URL_LISTAR = BASE_URL + '/productooriginal/productos/listarproducto'
const API_URL_LISTAR_CONSOLIDADO = BASE_URL + '/productooriginal/productos/listarproductototal'
const API_URL_LISTAR_DETALLE = BASE_URL + '/productooriginal/productos/listardetalle'
const API_URL_INSERTAR = BASE_URL + '/productooriginal/productos/registrarproducto/'
const API_URL_INSERTAR_FAMILIA = BASE_URL + '/productooriginal/productos/registrarcategoria/'
const API_URL_GENERAR_PDF= BASE_URL + '/productooriginal/productos/generarpdf/'
const API_URL_BUSCAR_PRODUCTO = BASE_URL + '/productooriginal/productos/buscarproducto/'
const API_URL_BUSCAR_PRODUCTO_VALIDADO = BASE_URL + '/productooriginal/productos/buscarproductovalidado/'
const API_URL_ACTUALIZAR_FAMILIA = BASE_URL + '/productooriginal/productos/actualizarproducto/'
const API_URL_OBTENER_CODIGO = BASE_URL + '/productooriginal/productos/obtenercodigo';
const API_URL_BUSCAR_DETALLE = BASE_URL + '/productooriginal/productos/buscardetalleproducto/'



const buscarproducto = async(e) => {
    try{
        const response = await axios.post(API_URL_BUSCAR_PRODUCTO, e)
        return response.data
    }catch(error){
        throw new Error('Error al buscar producto')

    }
}


const buscarproductovalidado = async(e) => {
    try{
        console.log("buscar producto validado:",e)
        const response = await axios.post(API_URL_BUSCAR_PRODUCTO_VALIDADO, e)
        console.log("respuesta de busqueda:",response)
        return response.data
    }catch(error){
        throw new Error('Error al buscar producto validado')

    }
}

/*const generarpdf = async(e) => {
    try{
        //const response = await axios.post(API_URL_GENERAR_PDF, e)
        const response = await axios.post(`${API_URL_GENERAR_PDF}?codigoregistro=${e}`);
        return response.data;
    }catch(error){
        throw new Error('Error al descargar pdf')
    }
}*/

const generarpdf = async(e) => {
    try {

        const response = await axios.post(
            `${API_URL_GENERAR_PDF}?codigoregistro=${e}`,
            null,
            {responseType : 'blob'}
        );
        return response.data
    }catch(error){
        throw new Error('Error al descargar pdf')
    }
}



const insertfamilia = async (e) => {
    try {
        const response = await axios.post(API_URL_INSERTAR_FAMILIA, e)
        return response.data;
    } catch (error) {
        throw new Error('Error al insertar la familia')
    }
}


/*const insertarproducto = async (e) => {
    try{
        const response = await axios.post(API_URL_INSERTAR, e);
        return response.data;
    }catch (error){
        throw new Error('Error al actualizar el producto');
    }
}*/

const insertarproducto = async (data) => {
    try {
        const response = await axios.post(API_URL_INSERTAR, data, {
            responseType: 'blob'  // Importante para recibir datos binarios
        });
        return response.data;
    } catch (error) {
        throw new Error('Error al insertar el producto');
    }
};


const listardetalleproducto = async (page_num, page_size, codigoregistro) => {
    try{
        //const response = await axios.post(`${API_URL_LISTAR_DETALLE}?codigoregistro=${e}`);
        const response = await axios.get(`${API_URL_LISTAR_DETALLE}?page_num=${page_num}&page_size=${page_size}&codigoregistro=${codigoregistro}`);

        return response.data;
    }catch(error){
        throw new Error('Error al listar detalle del producto')
    }
}

const listarproductoconsolidado = async (page_num, pagesize) => {
    try{
        //const response = await axios.get(API_URL_LISTAR_CONSOLIDADO)
        const response = await axios.get(`${API_URL_LISTAR_CONSOLIDADO}?page_num=${page_num}&page_size=${pagesize}`);
        return response.data
    }catch(error){
        throw new Error('Error al listar producto')
    }
}
 
const listarproducto = async (page_num, page_size) => {
    try {
        //const response = await axios.get(API_URL_LISTAR)
        const response = await axios.get(`${API_URL_LISTAR}?page_num=${page_num}&page_size=${page_size}`);
        console.log("pintame response:", response)
        return response.data;

    }catch (error) {
        throw new Error('Error al listar productos', error)
    }
}

/*const listafamilia = async () => {
    try{
        const response = await axios.get(API_URL_LISTAR_FAMILIA)
        console.log("pintame response:", response.data)
        return response.data;
    }catch (error){
        throw new Error('Error al listar categoria', error)
    }
}*/

const actualizarfamilia = async (e) => {
    try{
        const response = await axios.post(API_URL_ACTUALIZAR_FAMILIA,e)
        console.log("pintame response:", response.data)
        return response.data;
    }catch (error){
        throw new Error('Error al listar categoria', error)
    }
}

const obtenerCodigo = async () => {
    try {
        console.log("Entras a obtener codigo de registro:");
        const response = await axios.get(API_URL_OBTENER_CODIGO);
        return response.data; // Devuelve los datos del producto actualizado
    } catch (error) {
        throw new Error('Error al listar codigo', error); // Maneja el error correctamente
    }
};

const buscardetalleproducto = async(e) => {
    try{
        const response = await axios.post(API_URL_BUSCAR_DETALLE, e)
        return response.data
    }catch(error) {
        throw new Error("Error al buscar detalle del producto")

    }
}

export  {listarproducto,insertarproducto,insertfamilia,listardetalleproducto,listarproductoconsolidado, generarpdf, buscarproducto, buscarproductovalidado,actualizarfamilia,obtenerCodigo, buscardetalleproducto}