import React, { useState, useEffect } from 'react';

import Busqueda from "./componentes/BusquedaProducto.jsx";
import {ResultadoTablaProducto} from './componentes/ResultadoTabla.jsx';
import { listarproducto, insertarproducto, listarproductoconsolidado, generarpdf, buscarproducto, obtenerCodigo } from '../productooriginal/services/ProductoService.jsx'
import ModalInsertarProducto from '../productooriginal/modal/producto/ModalInsertar.jsx'
import Modaldetalleproducto from '../productooriginal/modal/producto/Modaldetalleproducto.jsx';
//import '../../../assets/css/loading.css'
import '../productooriginal/assets/css/loading.css'
import '../productooriginal/assets/css/familia.css'

import {Validar} from '../../../src/services/ValidaSesion.jsx'
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"



const Productos = () => {
    const [resultados, setResultados] = useState([]);
    const [isFetch, setIsFetch] = useState(true);
    const [modalData, setModalData] = useState(null);
    const [tipoOpe, setTipoOpe] = useState('Find');

    const [showInsertarModal, setShowInsertarModal] = useState(false);
    const [showLoadingModal, setShowLoadingModal] = useState(false);
    const [showDetalleProducto, setShowDetalleProducto] = useState(false);
    const [codigoproducto, setCodigoproducto] = useState(null)
    const [codigoregistro, setCodigoregistro] = useState(null)

    //paginado
    const [page, setPage] = useState(1)
    const [limit] = useState(50)
    const [total, setTotal] = useState(0)

     // ventana seguridad
     const [VentanaSeguridad, setVentanaSeguridad] = useState(false);

     const Seguridad = async () => {
         const ValSeguridad = await Validar("MARPRO")
         console.log("ValSeguridad:",ValSeguridad)
         setVentanaSeguridad(ValSeguridad.Validar)
     }

    useEffect(() => {
        const token = sessionStorage.getItem('CDTToken');
        if (token) {
            Seguridad()
            obtenercodigoregistro()
            Listar(page);


        }
    }, [page]);

    const Listar = async (page) => {
        try {
            const responseJson = await listarproductoconsolidado(page, limit);
            console.log("muestrame lista consolidado:", responseJson)
            setResultados(responseJson.data);
            setTotal(responseJson.total)
            setIsFetch(false);
        } catch (error) {
            console.error("Error al obtener los producto del usuario")
        }

    };

    const obtenercodigoregistro = async () => {
        try {
            const responseJson = await obtenerCodigo();
            console.log("muestrame codigo autogenerado:", responseJson)
            setCodigoregistro(responseJson['codigoregistro'])

        } catch (error) {
            console.log("muestrame el error que hay aqui:", error)
        }
    }

    const handleBusqueda = async (txtFind) => {
        const texto = { "datos": txtFind };
        const responseJson = await buscarproducto(texto);
        setResultados(responseJson);
        setIsFetch(false);
    };

    const handleSubmit = async (e) => {
        setShowLoadingModal(true) // Mostrar modal de carga antes de la solicitud al backend
        try {
            let responseBlob;
            if (tipoOpe === 'Insertar') {
                responseBlob = await insertarproducto(e);
                console.log("muestrame datos:", responseBlob)
                const url = window.URL.createObjectURL(new Blob([responseBlob], { type: 'application/zip' }));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', 'codigo_qr.zip');
                document.body.appendChild(link);
                link.click();
                link.parentNode.removeChild(link);  // Limpiar el enlace temporal
            }

            await Listar(page); //Refresca
            await obtenercodigoregistro()
        } catch (error) {
            console.error(`Error al ${tipoOpe.toLowerCase()} el producto:`, error);
        } finally {
            setShowLoadingModal(false); // Ocultar modal de carga al finalizar la operación
        }
    };

    const handleOpenModal = (registro, tipoOpe) => {
        setModalData(registro);
        setTipoOpe(tipoOpe)

        if (tipoOpe === 'Insertar') {
            setShowInsertarModal(true)
        }

    };


    const handleCloseModal = () => {
        setShowInsertarModal(false)
    };

    const handleChange = (e, fieldName) => {
        const updatedModalData = { ...modalData, [fieldName]: e.target.value };
        setModalData(updatedModalData);
    };

    const handleOpenModalProducto = (codigoregistro) => {
        setShowDetalleProducto(true);
        setCodigoproducto(codigoregistro)

    }

    const handleCloseModalProducto = () => {
        setShowDetalleProducto(false);
    }

    const downloadpdf = async (codigoregistro) => {
        setShowLoadingModal(true)
        try {
    
            const responseJson = await generarpdf(codigoregistro);
            console.log("muestrame datos:", responseJson)
            const url = window.URL.createObjectURL(new Blob([responseJson], { type: 'application/zip' }));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'codigo_qr.zip');
            link.click();
            document.body.appendChild(link);
            await Listar(page); //Refresca
        } catch (error) {
            console.error(`Error al ${tipoOpe.toLowerCase()} el zip:`, error);
        } finally {
            setShowLoadingModal(false)
        }

    }

    const handleNextPage = () => {
        setPage((prev) => prev + 1);
        /*if (page < Math.ceil(total / limit)) {
            setPage(page + 1);
        }*/
    }

    const handlePrevPage = () => {
        setPage((prev) => (prev > 1 ? prev - 1 : 1));
        /*if (page > 1) {
            setPage(page - 1);
        }*/
    }


    return (
        <React.Fragment>
            <Busqueda handleBusqueda={handleBusqueda}
                handleModal={
                    () => handleOpenModal(null, 'Insertar')
                }

            />

            {isFetch && 'Cargando'}
            {!isFetch && !resultados.length && 'Sin Informacion'}
            <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                        <tr>
                            <th className="align-middle">codigo de registro</th>
                            <th className="align-middle">producto</th>
                            <th className="align-middle">familia</th>
                            <th className="align-middle">cantidad</th>
                            <th className="align-middle">fecha de registro</th>
                            <th className="align-middle">generar pdf</th>
                            <th className="align-middle">ver detalle</th>

                        </tr>
                    </thead>
                    {resultados.length > 0 && resultados.map((registro) => (
                        <ResultadoTablaProducto
                            key={registro.id}
                            codigoregistro={registro.codigoregistro}
                            producto={registro.producto}
                            categoria={registro.categoria}
                            cantidad={registro.cantidad}
                            fecharegistro={registro.fecharegistro}
                            generarpdf={() => downloadpdf(registro.codigoregistro)}
                            detalleproducto={() => handleOpenModalProducto(registro.codigoregistro)}
                        />
                    ))}
                </table>
            </div>
            {total > limit && (
                <div className="pagination-controls">
                    <button onClick={handlePrevPage} disabled={page === 1}>anterior</button>
                    <button onClick={handleNextPage} disabled={page >= Math.ceil(total / limit)}>siguiente</button>
                </div>
            )}
            {/*<div className="pagination-controls">
                <button onClick={handlePrevPage} disabled={page === 1}>Previous</button>
                <button onClick={handleNextPage} disabled={page >= Math.ceil(total / limit)}>Next</button>
            </div>*/}
            <VentanaBloqueo show={VentanaSeguridad} />
            <ModalInsertarProducto
                showModal={showInsertarModal}
                modalData={modalData}
                onClose={handleCloseModal}
                handleSubmit={handleSubmit}
                handleChange={handleChange}
                codigo={codigoregistro}

            />

            <Modaldetalleproducto
                showModal={showDetalleProducto}
                codgioregistro={codigoproducto}
                onClose={handleCloseModalProducto}
            />



            {
                showLoadingModal && (
                    <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
                        <div className="modal-dialog modal-sm" role="document">
                            <div className="modal-content" style={{ top: '180px' }}>
                                <div className="modal-body text-center">
                                    <div class="loading-spinner mb-2"></div>
                                    <p>registrando</p>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }



        </React.Fragment>
    );
};

export default Productos;
