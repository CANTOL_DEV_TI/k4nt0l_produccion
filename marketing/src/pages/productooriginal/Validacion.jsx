import React, { useState, useEffect } from 'react';

import Busqueda from "./componentes/BusquedaValidacion.jsx";
import { ResultadoTablaValidacion } from './componentes/ResultadoTabla.jsx';
import { listarproducto, buscarproductovalidado } from '../productooriginal/services/ProductoService.jsx'
import Modaldetallecliente from '../productooriginal/modal/cliente/Modaldetallecliente.jsx';

import { Validar } from '../../../src/services/ValidaSesion.jsx'
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"


const Productovalidado = () => {
    const [resultados, setResultados] = useState([]);
    const [isFetch, setIsFetch] = useState(true);
    const [showDetalleCliente, setShowDetalleCliente] = useState(false);
    const [numeroserie, setNumeroserie] = useState(null)

    const [page, setPage] = useState(1);
    const [limit] = useState(50);
    const [total, setTotal] = useState(0)

    // ventana seguridad
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);

    const Seguridad = async () => {
        const ValSeguridad = await Validar("MARVAL")
        console.log("ValSeguridad:", ValSeguridad)
        setVentanaSeguridad(ValSeguridad.Validar)
    }

    useEffect(() => {
        const token = sessionStorage.getItem('CDTToken');
        if (token) {
            Seguridad()
            Listar(page);
        }
    }, [page]);

    const Listar = async () => {
        try {
            const responseJson = await listarproducto(page, limit);
            setResultados(responseJson.data);
            setTotal(responseJson.total)
            setIsFetch(false);
        } catch (error) {
            console.error("Error al obtener los producto del usuario")
        }

    };

    const handleBusqueda = async (txtFind) => {
        const texto = { "datos": txtFind };
        const responseJson = await buscarproductovalidado(texto);
        setResultados(responseJson);
        setIsFetch(false);
    };


    const handleOpenModalCliente = (numeroserie) => {
        setShowDetalleCliente(true);
        setNumeroserie(numeroserie)

    }

    const handleCloseModalCliente = () => {
        setShowDetalleCliente(false);
    }

    const handleNextPage = () => {
        setPage((prev) => prev + 1);
    }

    const handlePrevPage = () => {
        setPage((prev) => (prev > 1 ? prev - 1 : 1))
    }



    return (
        <React.Fragment>
            <Busqueda handleBusqueda={handleBusqueda}


            />

            {isFetch && 'Cargando'}
            {!isFetch && !resultados.length && 'Sin Informacion'}
            <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                        <tr>
                            <th className="align-middle">N° de serie</th>
                            <th className="align-middle">producto</th>
                            <th className="align-middle">familia</th>
                            <th className="align-middle">articulo</th>
                            <th className="align-middle">fecha de registro</th>
                            <th className="align-middle">Estado</th>
                            <th className="align-middle">Ver</th>

                        </tr>
                    </thead>
                    {resultados.length > 0 && resultados.map((registro) => (
                        <ResultadoTablaValidacion
                            key={registro.id}
                            numserie={registro.numeroserie}
                            producto={registro.producto}
                            categoria={registro.categoria ? registro.categoria : '-'}
                            tipo={registro.articulo}
                            fecharegistro={registro.fecharegistro}
                            estado={registro.estado === 0 ? 'SIN VALIDAR' : 'VALIDADO'}
                            detallecliente={() => handleOpenModalCliente(registro.numeroserie)}
                        />
                    ))}
                </table>
            </div>

            {total > limit && (
                <div className="pagination-controls">
                    <button onClick={handlePrevPage} disabled={page === 1}>Previous</button>
                    {/*<span style={{ margin: '0 10px' }}>Page {page}</span>*/}
                    <button onClick={handleNextPage} disabled={page >= Math.ceil(total / limit)}>Next</button>
                </div>
            )}


            <VentanaBloqueo show={VentanaSeguridad} />
            <Modaldetallecliente
                showModal={showDetalleCliente}
                numserie={numeroserie}
                onClose={handleCloseModalCliente}
            />







        </React.Fragment>
    );
};

export default Productovalidado;
