import React, { useEffect, useState } from 'react'
import { listarcliente, buscardetallecliente } from '../../services/ClienteService.jsx'
import '../../assets/css/modal-detail.css'


export const Modaldetallecliente = ({showModal, onClose, numserie}) => {
    const[data, setData] = useState([]);
    const[searchCliente, setSearchCliente] = useState('');
    const[loading, setLoading] = useState(true)
    const [txtInput, setTxtInput] = useState('');

    //paginado
    const [page, setPage] = useState(1);
    const [limit] = useState(50)
    const [total, setTotal] = useState(0)
    
    useEffect(()=>{
        if(!showModal){
            setPage(1)
        }
        listadetallecliente(page)
    }, [page, numserie, showModal])

    const listadetallecliente = async (page) => {
        try {
            const responsejson = await listarcliente(page, limit, numserie)
            setData(responsejson.data)
            setTotal(responsejson.total)
            setLoading(false)
        } catch (error) {
            console.log("Error al obtener el detalle del producto:", error)
        }
    }

    const handleKeyUp = () => {
        handlebusqueda(txtInput)
    }

    const handlebusqueda = async (txtFind) =>{
        try{
            //const texto = {"datos":txtFind};
            console.log("filtro:",txtFind)
            const responseJson = await buscardetallecliente(numserie,txtFind)
            setData(responseJson)
        }catch(error){
            console.error("Error al obtener el detalle del producto:", error)
        }
    }

    const handleNextPage = () => {
        setPage((prev) => prev + 1);
    }

    const handlePrevPage = () => {
        setPage((prev) => (prev > 1 ? prev - 1 : 1));
    }


    

    const handleSearchChange = (e) => {
        console.log("pintame:",e)
        setTxtInput(e.target.value)
    }

    /*const filteredData = data.filter(item => {
        console.log("item filterdata:",item)
        return item.nombre.toLowerCase().includes(searchCliente.toLowerCase());
    });*/

  return (
    <div className="modal-detail" style={{display:showModal ? 'block':'none'}}>
        <div className="modal-content-detail">
            <span className="close" onClick={onClose}>&times;</span>
            <div className="search-container" style={{textAlign:'right', marginRight:'20px', marginBottom:'5px'}}>
                <input 
                    type="text"
                    placeholder="Buscar cliente.."                    
                    onChange={handleSearchChange}
                    onKeyUp={handleKeyUp}
                    value={txtInput}
                 />
            </div>
            {loading ? (
                <div>Cargando...</div>
            ):(
                <div className="table-container">
                    <table className="table table-bordered table-hover table-sm">
                        <thead className="table-secondary text-center table-sm">
                            <tr>
                                <th>Nombre completo</th>
                                <th>DNI/RUC/CE</th>
                                <th>Email de contacto</th>
                                <th>Punto de venta</th>
                                <th>Ciudad y Provincia de compra</th>
                                <th>Fecha de registro</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((item, index) => (
                                <tr key={index}>
                                    <td>{item.nombre}</td>
                                    <td>{item.identificador}</td>
                                    <td>{item.email}</td>
                                    <td>{item.lugar}</td>
                                    <td>{item.ciudad}</td>
                                    <td>{item.fecha}</td>
                                    <td>{item.estado == 1 ? 'VALIDADO': 'FALSIFICADO'}</td>
                                </tr>
                            ))}
                        </tbody>

                    </table>

                </div>
            )

            }
             <div className="pagination-controls">
                    <button onClick={handlePrevPage} disabled={page === 1}>anterior</button>
                    <button onClick={handleNextPage} disabled={page >= Math.ceil(total / limit)}>siguiente</button>
            </div>

        </div>

    </div>
  )
}

export default Modaldetallecliente