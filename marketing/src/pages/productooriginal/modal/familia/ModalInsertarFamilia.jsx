import React, { useEffect, useState } from "react";

const ModalInsertarFamilia = ({ showModal, onClose, handleSubmit }) => {
    if (!showModal) return null;

    const [formData, setFormData] = useState({
        nombre: '',
        tipo: '',
        descripcion: ''
    });

    const [formCompleted, setFormCompleted] = useState(false)

    useEffect(() => {
        const isFormCompleted = Object.values(formData).every(value => value !== '' && value !==null);
        console.log("isFormCompleted:",isFormCompleted)
        setFormCompleted(isFormCompleted && formData.nombre !== null && formData.tipo !==null);
        console.log("formCompleted:",formCompleted)
    }, [formData]);

    const handleChange = (e, fieldName) => {
        const updatedFormData =  {...formData, [fieldName]: e.target.value};
        console.log("updatedFormData:",updatedFormData)
        setFormData(updatedFormData)
    }

    const handleSave = async () => {
        const formDataToSend = new FormData();
        formDataToSend.append('nombre',formData.nombre);
        formDataToSend.append('tipo', formData.tipo);
        formDataToSend.append('descripcion', formData.descripcion)

        handleSubmit(formDataToSend);
        onClose();

    }

    return(
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Insertar Familia</h5>
                        <button type="button" className="btn-close" onClick={onClose}></button>
                    </div>
                    <div className="modal-body">
                        <div className="form-group">
                            <label htmlFor="nombre">Nombre:</label>
                            <input type="text" className="form-control" id="nombre" onChange={(e) => handleChange(e, 'nombre')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="tipo">Tipo:</label>
                            <input type="text" className="form-control" id="tipo" onChange={(e) => handleChange(e, 'tipo')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="descripcion">Descripción:</label>
                            <textarea type="text" className="form-control" id="descripcion" onChange={(e) => handleChange(e, 'descripcion')} />
                        </div>
                    </div>
                    <div className="modal-footer">
                        {/*<button type="button" className="btn btn-primary btn-sm" onClick={onClose}>Cerrar</button>*/}
                        <button type="button" className="btn btn-warning btn-sm" onClick={handleSave} disabled={!formCompleted}>Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    )

    
}

export default ModalInsertarFamilia;