import React, { useState, useEffect } from "react";


const ModalEditarCategoria = ({ showModal, modalData, onClose, handleSubmit, handleChange }) => {
    console.log("entras abrir modal editar:",showModal)
    if (!showModal) return null;

    console.log("modal data:", modalData)

    /*const [formData, setFormData] = useState({
        categoria: '',
        nombre: '',
        tipo: '',
        descripcion: ''
    });

    const [formCompleted, setFormCompleted] = useState(false);*/

    useEffect(() => {
        // Verificar si los campos cantidadpuntos y precio tienen información
       /* const isFormCompleted = Object.values(formData).every(value => value !== '' && value !==null);
        setFormCompleted(isFormCompleted && formData.nombre !== null && formData.tipo !==null);*/
    }, [modalData]);

    //if (!showModal) return null;



    const handleEdit = async (e) => {
        console.log("muestrame información", e)
        const formDataToSend = new FormData();
        formDataToSend.append('categoria', e.idcategoria)
        formDataToSend.append('nombre', e.nombre)
        formDataToSend.append('tipo', e.tipo)
        formDataToSend.append('descripcion', e.descripcion)
        handleSubmit(formDataToSend)
        onClose();
    }


    return (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Modal de Edición</h5>
                        <button type="button" className="btn-close" onClick={onClose}></button>
                    </div>
                    <div className="modal-body">
                        <div className="form-group">
                            <label htmlFor="nombre">Nombre:</label>
                            <input type="text" className="form-control" id="nombre" value={modalData ? modalData.nombre : ''} onChange={(e) => handleChange(e, 'nombre')} />
                        </div>

                        <div className="form-group">
                            <label htmlFor="tipo">Tipo:</label>
                            <input type="text" className="form-control" id="tipo" value={modalData ? modalData.tipo : ''} onChange={(e) => handleChange(e, 'tipo')} />
                        </div>
                        
                        <div className="form-group">
                            <label htmlFor="descripcion">Descripcion:</label>
                            <textarea type="text" className="form-control" id="descripcion" value={modalData ? modalData.descripcion: ''} onChange={(e)=> handleChange(e, 'descripcion')}></textarea>
                        </div>
                    </div>
                    <div className="modal-footer">
                        {/*<button type="button" className="btn btn-primary btn-sm" onClick={onClose}>Cerrar</button>*/}
                        <button type="button" className="btn btn-warning btn-sm" onClick={() => handleEdit(modalData)} >Guardar</button>
                        {/*<button type="button" className="btn btn-warning btn-sm" onClick={() => handleEdit(modalData)}>Guardar</button>*/}

                    </div>
                </div>
            </div>
        </div>
    );
};

export default ModalEditarCategoria

