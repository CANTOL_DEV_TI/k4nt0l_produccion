import React, { useState, useEffect } from 'react'
import { buscardetalleproducto, listardetalleproducto } from '../../../productooriginal/services/ProductoService.jsx';
//import '../../../assets/css/modal-detail.css'
import '../../../productooriginal/assets/css/modal-detail.css'

export const Modaldetalleproducto = ({ showModal, onClose, codgioregistro }) => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [txtInput, setTxtInput] = useState('');

    //paginado
    const [page, setPage] = useState(1);
    const [limit] = useState(50)
    const [total, setTotal] = useState(0)

    useEffect(() => {
        if(!showModal){
            setPage(1)
        }
        listdetalleproducto(page)
    }, [page, codgioregistro, showModal])


    const listdetalleproducto = async (page) => {
        try {
            const responsejson = await listardetalleproducto(page, limit, codgioregistro)
            setData(responsejson.data)
            setTotal(responsejson.total)
            setLoading(false)
        } catch (error) {
            console.error("Error al obtener el detalle del producto:", error)

        }
    }

    const handleKeyUp = () => {
        handlebusqueda(txtInput)
    }

    const handlebusqueda = async (txtFind) =>{
        try{
            console.log("txtFind",txtFind)
            const texto = {"datos":txtFind};
            const responseJson = await buscardetalleproducto(texto)
            setData(responseJson)
        }catch(error){
            console.error("Error al obtener el detalle del producto:", error)
        }
    }

    

    const handleSearchChange = (e) => {
        console.log("cantidad data:", total)
        setTxtInput(e.target.value);
    };

    /*const filteredData = data.filter(item => {
        return item.numeroserie.toLowerCase().includes(searchProducto.toLowerCase());
    });*/

    const handleNextPage = () => {
        setPage((prev) => prev + 1);
    }

    const handlePrevPage = () => {
        setPage((prev) => (prev > 1 ? prev - 1 : 1));
    }

    return (
        <div className="modal-detail" style={{ display: showModal ? 'block' : 'none' }}>
            <div className="modal-content-detail">
                <span className="close" onClick={onClose}>&times;</span>
                <div className="search-container" style={{ textAlign: 'right', marginRight: '20px', marginBottom: '5px' }}>
                    <input
                        type="text"
                        placeholder="Buscar producto..."
                        onChange={handleSearchChange}
                        onKeyUp={handleKeyUp}
                        value={txtInput}
                    />
                </div>
                {loading ? (
                    <div>Cargando...</div>
                ) : (
                    <div className="table-container">
                        <table className="table table-bordered table-hover table-sm">
                            <thead className="table-secondary text-center table-sm">
                                <tr>
                                    <th>N° de serie	</th>
                                    <th>producto</th>
                                    <th>familia</th>
                                    <th>articulo</th>
                                    <th>fecha de registro</th>
                                    <th>Estado</th>

                                </tr>
                            </thead>
                            <tbody>
                                {data.map((item, index) => (
                                    <tr key={index}>
                                        <td>{item.numeroserie}</td>
                                        <td>{item.producto}</td>
                                        <td>{item.familia}</td>
                                        <td>{item.articulo}</td>
                                        <td>{item.fecharegistro}</td>
                                        <td>{item.estado == 0 ? 'SIN VALIDAR' : 'VALIDADO'}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>

                    </div>

                )
                }
                {total > limit && (
                    <div className="pagination-controls">
                    <button onClick={handlePrevPage} disabled={page === 1}>anterior</button>
                    <button onClick={handleNextPage} disabled={page >= Math.ceil(total / limit)}>siguiente</button>
                </div>
                )}
                {/*<div className="pagination-controls">
                    <button onClick={handlePrevPage} disabled={page === 1}>Previous</button>
                    <button onClick={handleNextPage} disabled={page >= Math.ceil(total / limit)}>Next</button>
                </div>*/}
            </div>

        </div>
    )
}


export default Modaldetalleproducto