import React, { useEffect, useState } from "react";
//import { listafamilia } from '../../servicios/ProductoService.jsx';
import { obtenerfamilia, listarProductosPorFamilia } from "../../../productooriginal/services/FamiliaService.jsx";

const ModalInsertarProducto = ({ showModal, onClose, handleSubmit, codigo }) => {
    if (!showModal) return null;

    const [formData, setFormData] = useState({
        codigoregistro: '',
        idfamilia: '',
        familianombre: '',
        producto: '',
        productonombre: '',
        codquivalente: '',
        cantidad: '',
        codequivalentereal: '',
    });

    const [formCompleted, setFormCompleted] = useState(false)
    const [familias, setFamilias] = useState([])
    const [productos, setProductos] = useState([]);
    const [cantidad, setCantidad] = useState('')


    useEffect(()=> {
        if(cantidad && (+cantidad < 0 || +cantidad > 50000)) {
            setCantidad('')
            console.log("cantidad:", cantidad)
        } 
    },[cantidad])

    useEffect(() => {
        setFormData(prevFormData => ({
            ...prevFormData,
            codigoregistro:codigo
        }))
        obtenerfamily()
    }, [codigo])

    useEffect(() => {
        console.log("muestrame formData:",formData)
        console.log("muestrame cantidad:",cantidad)
        const isFormCompleted = Object.values(formData).every(value => value !== '' && value !== null);

        setFormCompleted(isFormCompleted && formData.producto !== null && formData.cantidad !== null && Number(cantidad) > 0);

    }, [formData, cantidad]);

    const filtro = async (e) => {
        
        let tecla = e.key
        console.log("pintame tecla:",tecla)
        /*if(['.','e'].includes(tecla)){
            setCantidad(0)
        }*/

        if (!/^\d$/.test(tecla) && tecla !== 'Backspace' && tecla !== 'ArrowLeft' && tecla !== 'ArrowRight') {
            e.preventDefault();
            setCantidad('')
        }
    }

    const handleChange = async (e, fieldName) => {
        const { value } = e.target
        if (fieldName==='cantidad') {
            //setCantidad(value)
            let val = parseInt(value, 10)
            let maxLenght = 5
            let newValue = val < maxLenght ? val : parseInt(val.toString().substring(0,maxLenght))
            //setCantidad(newValue)

            if (value === '' || /^[0-9]*$/.test(value)) {
                console.log("entras aqui:", value)
                setCantidad(newValue);
            }
        }
        
        let updatedFormData;

        if (fieldName === 'familia') {
            console.log("pintame familias:",familias)
            const selectedFamilia= familias.find(familia => familia.idfamilia === value)

            console.log("muestrame selectedFamilia:",selectedFamilia)

            updatedFormData = {
                ...formData,
                idfamilia: selectedFamilia ? selectedFamilia.idfamilia : '',
                familianombre: selectedFamilia ? selectedFamilia.familia : '',

            }
            if(selectedFamilia){
                await obtenerProductosPorCategoria(selectedFamilia.idfamilia)
            }else{
                setProductos([])
            }

            console.log("muestrame updatedFormData:", updatedFormData)

        } else if (fieldName === 'producto') {
            console.log("pintame en value producto:", value)
            console.log("pintame en productos:", productos)
            const selectedProducto = productos.find(producto => producto.uuid_familia.toString() === value);
            console.log("Selected producto:", selectedProducto);

            if(selectedProducto){

                updatedFormData = {
                    ...formData,
                    producto:selectedProducto.uuid_familia,
                    productonombre:selectedProducto.nombreproducto,
                    codquivalente:selectedProducto.codigoequivalente,
                    codequivalentereal: selectedProducto.codequivalentereal
                }
                console.log("muestrame updatedFormData:", updatedFormData)
            }else{
                updatedFormData = {...formData, producto:''}
            }

        }else{
            updatedFormData = { ...formData, [fieldName]: value };
        }
        setFormData(updatedFormData)
    }

    const obtenerfamily = async () => {
        try {
            const responsejson = await obtenerfamilia();
            console.log("muestrame datos de la categoria:", responsejson)
            setFamilias(responsejson || [])
        } catch (error) {
            console.error("Error al obtener categorias:", error)

        }
    }

    const obtenerProductosPorCategoria = async(idfamilia) => {
        try{
            const responseJson = await listarProductosPorFamilia(idfamilia)
            console.log("muestrame los datos del producto:",responseJson)
            setProductos(responseJson || [])
        }catch(error){
            console.log("Error al obtener productos:", error);
        }
    }





    const handleSave = async () => {
        const formDataToSend = new FormData();
        formDataToSend.append('codigoregistro', formData.codigoregistro);
        formDataToSend.append('idfamilia', formData.idfamilia);
        formDataToSend.append('familianombre', formData.familianombre)
        formDataToSend.append('producto', formData.producto);
        formDataToSend.append('productonombre', formData.productonombre);
        formDataToSend.append('codquivalente', formData.codquivalente);
        formDataToSend.append('cantidad', formData.cantidad);
        formDataToSend.append('codequivalentereal', formData.codequivalentereal);

        console.log("show me:", formDataToSend)
        handleSubmit(formDataToSend);
        onClose();

    }

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Insertar Producto</h5>
                        <button type="button" className="btn-close" onClick={onClose}></button>
                    </div>
                    <div className="modal-body">
                        <div className="form-group">
                            <label htmlFor="codigoregistro">Codigo registro:</label>
                            <input type="text" className="form-control" id="codigoregistro" value={codigo} onChange={(e) => handleChange(e, 'codigoregistro')}  disabled/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="familia">Familia:</label>
                            <select
                                className="form-control"
                                id="familia"
                                value={formData.idfamilia}
                                onChange={(e) => handleChange(e, 'familia')}>

                                <option value="">Seleccione una familia</option>
                                {familias.map((familia) => (
                                    <option key={familia.idfamilia} value={familia.idfamilia}>
                                        {familia.familia ? familia.familia : '-'}
                                    </option>
                                ))}
                            </select>
                        </div>

                        <div className="form-group">
                            <label htmlFor="producto">Producto:</label>
                            <select
                                className="form-control"
                                id="producto"
                                value={formData.producto}
                                onChange={(e) => handleChange(e, 'producto')}>
                                <option value="">Seleccione un producto</option>
                                {productos.map((producto) => (
                                    <option key={producto.uuid_familia} value={producto.uuid_familia}>
                                        {producto.nombreproducto}
                                    </option>
                                ))}
                            </select>
                        </div>

                        

                        {/*<div className="form-group">
                            <label htmlFor="nombre">Producto:</label>
                            <input type="text" className="form-control" id="producto" onChange={(e) => handleChange(e, 'producto')} />
                        </div>*/}

                        <div className="form-group">
                            <label htmlFor="cantidad">Cantidad:</label>
                            <input type="number" 
                             className="form-control" 
                             id="cantidad"
                             value={cantidad}
                             onChange={(e) => handleChange(e, 'cantidad')}     
                             onKeyDown={(e) => filtro(e)}
                             inputMode="numeric"    
                             pattern="[0-9]*" //Este patron solo admite números enteros  
                            />
                        </div>

                    </div>
                    <div className="modal-footer">
                        {/*<button type="button" className="btn btn-primary btn-sm" onClick={onClose}>Cerrar</button>*/}
                        <button type="button" className="btn btn-warning btn-sm" onClick={handleSave} disabled={!formCompleted}>Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    )


}

export default ModalInsertarProducto;