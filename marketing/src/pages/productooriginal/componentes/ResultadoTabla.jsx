import React from "react";
import { GrEdit, GrAmazon, GrCodeSandbox, GrTask, GrImage, GrAdd, GrDescend, GrAddCircle, GrStatusGood, GrDocumentStore, GrDocumentPdf, GrFormView } from "react-icons/gr";
import { AiFillDelete } from "react-icons/ai";


const ResultadoTablaProducto = ({
  codigoregistro,
  producto,
  categoria,
  cantidad,
  fecharegistro,
  generarpdf,
  detalleproducto
}) =>
(
  <tbody>
    <tr>
      <td className='td-cadena'>{codigoregistro}</td>
      <td className='td-cadena'>{producto}</td>
      <td className='td-cadena'>{categoria}</td>
      <td className='td-cadena'>{cantidad}</td>
      <td className='td-cadena'>{fecharegistro}</td>
      <td style={{ textAlign: "center" }}><button onClick={() => generarpdf(true)}><GrDocumentPdf /></button></td>
      <td style={{ textAlign: "center" }}><button onClick={() => detalleproducto(true)}><GrFormView /></button></td>
    </tr>
  </tbody>
)


const ResultadoTablaFamilia = ({
  familia,
  articulo,
  nombre
  //editar
}) =>
(
  <tbody>
    <tr>
      <td className='td-cadena'>{familia}</td>
      <td className='td-cadena'>{articulo}</td>
      <td className='td-cadena'>{nombre}</td>
      {/*<td style={{textAlign:"center"}}><button onClick={() => editar(true)}><GrEdit/></button></td> */}
    </tr>
  </tbody>
)

const ResultadoTablaValidacion = ({
  numserie,
  producto,
  categoria,
  tipo,
  fecharegistro,
  estado,
  detallecliente
}) =>
(
  <tbody>
    <tr>
      <td className='td-cadena'>{numserie}</td>
      <td className='td-cadena'>{producto}</td>
      <td className='td-cadena'>{categoria}</td>
      <td className='td-cadena'>{tipo}</td>
      <td className='td-cadena'>{fecharegistro}</td>
      <td className='td-cadena'>{estado}</td>
      <td style={{ textAlign: "center" }}><button onClick={() => detallecliente(true)}><GrFormView /></button></td>
    </tr>
  </tbody>
)







export { ResultadoTablaProducto, ResultadoTablaFamilia, ResultadoTablaValidacion };