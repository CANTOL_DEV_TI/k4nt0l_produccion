import React, { useState } from "react";
import { BotonNuevo, BotonBuscar, BotonAtras } from "../../../components/Botones.jsx";
import Title from "../../../components/Titulo.jsx";

const Busqueda = ({ handleBusqueda, handleModal }) => {
    const [txtInput, setTxtInput] = useState('');

    const handleChange = (e) => {
        setTxtInput(e.target.value);
    };

    const handleKeyUp = () => {
        handleBusqueda(txtInput);
    };

    return (
        <div className="col-lg-12">
            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-sm">
                            <Title>Lista de productos validados</Title>
                        </div>
                        <div className="col-sm-auto">
                            <div className="d-flex flex-wrap gap-1">
                                <input
                                    placeholder="Ingrese número de serie"
                                    onChange={handleChange}
                                    onKeyUp={handleKeyUp}
                                    value={txtInput}
                                />
                                <BotonBuscar 
                                    textoBoton={"Buscar"} 
                                    sw_habilitado={true} 
                                    eventoClick={() => handleBusqueda(txtInput)} 
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Busqueda;
