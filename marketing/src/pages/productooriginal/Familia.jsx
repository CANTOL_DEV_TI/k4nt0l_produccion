import React, { useState, useEffect } from 'react';

import Busqueda from "./componentes/BusquedaFamilia.jsx";
import {ResultadoTablaFamilia} from './componentes/ResultadoTabla.jsx';
import { listarfamilia, buscarfamilia } from '../productooriginal/services/FamiliaService.jsx'
import ModalInsertarCategoria from '../productooriginal/modal/familia/ModalInsertarFamilia.jsx';
import ModalEditarCategoria from '../productooriginal/modal/familia/ModalEditarFamilia.jsx'; 
import '../productooriginal/assets/css/familia.css'
import {Validar} from '../../../src/services/ValidaSesion.jsx'
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"

const Familia = () => {
    const [resultados, setResultados] = useState([]);
    const [isFetch, setIsFetch] = useState(true);
    const [modalData, setModalData] = useState(null);
    const [tipoOpe, setTipoOpe] = useState('Find');
    const [showInsertarModal, setShowInsertarModal] = useState(false);
    const [showActualizarModal, setShowActualizarModal] = useState(false);

    //paginado
    const [page, setPage] = useState(1);
    const [limit] = useState(50)
    const [total, setTotal] = useState(0)

    // ventana seguridad
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);

    const Seguridad = async () => {
        const ValSeguridad = await Validar("MARFAM")
        console.log("ValSeguridad:",ValSeguridad)
        setVentanaSeguridad(ValSeguridad.Validar)
    }

    useEffect(() => {        
        const token = sessionStorage.getItem('CDTToken');
        if (token) {
            Seguridad()
            Listar(page);
        }
    }, [page]);

    const Listar = async (page) => {
        const responseJson = await listarfamilia(page, limit);
        setResultados(responseJson.data);
        setTotal(responseJson.total);
        setIsFetch(false);
    };

    const handleBusqueda = async (txtFind) => {
        const texto = { "datos": txtFind };
        const responseJson = await buscarfamilia(texto);
        setResultados(responseJson);
        setIsFetch(false);
    };

    const handleSubmit = async (e) => {
        try {
            let responseJson;
            if (tipoOpe === 'Insertar') {
                //responseJson = await insertfamilia(e);
            }
            else if (tipoOpe === 'Actualizar') {
                //responseJson = await actualizarfamilia(e);
            }
            await Listar(); //Refresca
        } catch (error) {
            console.error(`Error al ${tipoOpe.toLowerCase()} la categoria:`, error);
        }
    };

    const handleOpenModal = (registro, tipoOpe) => {
        console.log("entras abrir modal:", tipoOpe)
        setModalData(registro);
        setTipoOpe(tipoOpe)
        if (tipoOpe === 'Insertar') setShowInsertarModal(true)
        else if (tipoOpe === 'Actualizar') setShowActualizarModal(true)
    };


    const handleCloseModal = () => {
        setShowInsertarModal(false)
    };

    const handleCloseModalEditar = () => {
        setShowActualizarModal(false)
    }

    const handleChange = (e, fieldName) => {
        const updatedModalData = { ...modalData, [fieldName]: e.target.value };
        setModalData(updatedModalData);
    };

    const handleNextPage = () => {
        setPage((prev) => prev + 1);
        /*if (page < Math.ceil(total / limit)) {
            setPage(page + 1);
        }*/
    }

    const handlePrevPage = () => {
        setPage((prev) => (prev > 1 ? prev - 1 : 1));
        /*if (page > 1) {
            setPage(page - 1);
        }*/
    }

    return (
        <React.Fragment>
            <Busqueda handleBusqueda={handleBusqueda}
                handleModal={
                    () => handleOpenModal(null, 'Insertar')
                }
            />

            {isFetch && 'Cargando'}
            {!isFetch && !resultados.length && 'Sin Informacion'}
            <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                        <tr>
                            <th className="align-middle">Familia</th>
                            <th className="align-middle">Articulo</th>
                            <th className="align-middle">Producto</th>
                            {/*<th className="align-middle">Editar</th>*/}
                        </tr>
                    </thead>
                    {resultados.length > 0 && resultados.map((registro) => (
                        <ResultadoTablaFamilia
                            key={registro.uuid_familia}
                            familia={registro.familia ? registro.familia : '-'}
                            articulo={registro.articulo}
                            nombre={registro.nombre}
                        //editar={() => handleOpenModal(registro, 'Actualizar')}
                        />
                    ))}
                </table>
                
            </div>

            <div className="pagination-controls">
                <button onClick={handlePrevPage} disabled={page === 1}>anterior</button>
                <button onClick={handleNextPage} disabled={page >= Math.ceil(total / limit)}>siguiente</button>
            </div>

            <VentanaBloqueo show={VentanaSeguridad} />

            <ModalInsertarCategoria
                showModal={showInsertarModal}
                modalData={modalData}
                onClose={handleCloseModal}
                handleSubmit={handleSubmit}
                handleChange={handleChange}

            />
            <ModalEditarCategoria
                showModal={showActualizarModal}
                modalData={modalData}
                onClose={handleCloseModalEditar}
                handleSubmit={handleSubmit}
                handleChange={handleChange}
            />





        </React.Fragment>
    );
};

export default Familia;
