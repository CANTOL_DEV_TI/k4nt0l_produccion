import {Url} from '../constants/global'

// const meServidorBackend = 'http://192.168.5.21:8080'
const meServidorBackend = Url

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function migraAsiento(meJson){   
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };
    console.log("1")
    const response = await fetch(`${meServidorBackend}/gdh/asientos/`, requestOptions);
    console.log(`${meServidorBackend}/gdh/asientos/`)
    console.log(requestOptions)
    const responseJson = await response.json();
    return responseJson
}