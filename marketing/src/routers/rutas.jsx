import { Routes, Route, } from "react-router-dom";


import LoginV from "../pages/login/login.jsx";
import ActuaPassword from "../pages/login/cambiocontraseña.jsx";
import React from "react";
import ProtectedRoute from "../components/utils/ProtectedRoute.jsx";
import Productos from "../pages/productooriginal/Productos.jsx"
import Familia from "../pages/productooriginal/Familia.jsx"
import Productovalidado from "../pages/productooriginal/Validacion.jsx"


export function MiRutas() {
    return (
        <Routes>
            <Route path="/login" element={<LoginV />} />


            <Route element={<ProtectedRoute Redirige={"/login"} />} >
                <Route path="/cambiocontrasena" element={<ActuaPassword />} />
            </Route>


            <Route element={<ProtectedRoute Redirige={"/login"} />} >
                <Route path="/productos" element={<Productos/>} />
            </Route>

            <Route element={<ProtectedRoute Redirige={"/login"} />} >
                <Route path="/familia" element={<Familia/>} />
            </Route>

            <Route element={<ProtectedRoute Redirige={"/login"} />} >
                <Route path="/validacion" element={<Productovalidado/>} />
            </Route>
        </Routes>
    );
}