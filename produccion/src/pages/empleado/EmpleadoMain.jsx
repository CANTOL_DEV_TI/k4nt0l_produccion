import React from "react";

import VentanaModal from "../../components/VentanaModal";

import Busqueda from "./components/Busqueda";
import ResultadoTabla from "./components/ResultadoTabla";

import { getFilter_Empleado, save_Empleado, update_Empleado, delete_Empleado } from './services/EmpleadoServices.jsx'


import "../../assets/css/styleStandar.css"
import { get_subarea } from "../oee/services/oeeSubAreaServices.jsx";

import EmpleadoEdicion from "./EmpleadoEdicion"
import { delete_SubArea, save_SubArea, update_SubArea } from "../../services/subAreaServices.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";
import { Validar } from "../../services/ValidaSesion.jsx";



class EmpleadoMain extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            listAreas: [],

            resultados: [],
            txtInput: '',
            isFetch: true,

            cod_subarea: 0,

        }

    }


    handleFindSubArea = async () => {
        const responseJson = await get_subarea("00001")
        this.setState({ listAreas: responseJson })
    }

    handleBusqueda = async (cod_subarea, txtFind) => {
        if (cod_subarea !== "") {
            const responseJson = await getFilter_Empleado(cod_subarea, txtFind)
            // const responseJson = await getFilter_Empleado(3, txtFind)
            //console.log(responseJson)
            this.setState({ resultados: responseJson, isFetch: false, cod_subarea: cod_subarea })
        } else {
            alert("Debes escoger el subarea para filtrar")
        }
    }



    handleSubmit = async (e) => {
        console.log(e)


        if (this.state.tipoOpe === 'Grabar') {
            const responseJson = await save_Empleado(e)
            //console.log("################################")
            //console.log(responseJson)
            //console.log("################################")
        }

        if (this.state.tipoOpe === 'Actualizar') {
            const responseJson = await update_Empleado(e)
            //console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar') {
            const responseJson = await delete_Empleado(e)
            //console.log(responseJson)
        }

        this.setState({ showModal: false, tipoOpe: 'Find' })
    }




    render() {

        const { isFetch, resultados, showModal, VentanaSeguridad } = this.state

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} listAreas={this.state.listAreas} />

                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                            <tr className="cabecera">
                                <th className="meCabFin">Empleado</th>
                                <th className="meCab2 meCabCentro">Editar</th>
                                <th className="meCabIni">Eliminar</th>
                            </tr>
                        </thead>
                        {resultados.map((registro) =>
                            <ResultadoTabla
                                key={registro.cod_empleado}
                                cod_empleado={registro.cod_empleado}
                                nombre={registro.nombre}
                                cod_subarea={registro.cod_subarea}
                                sw_estado={registro.sw_estado}
                                eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}
                            />
                        )}
                    </table>
                </div>


                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({ showModal: false })}
                    titulo="Edicion Empleados OEEE">

                    <EmpleadoEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={this.state.tipoOpe}
                        listAreas={this.state.listAreas}
                    />

                </VentanaModal>

                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}

                />

            </React.Fragment>

        )
    }


    async componentDidMount() {
        const ValSeguridad = await Validar("PROSUA")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })

        this.handleFindSubArea()
    }


}


export default EmpleadoMain;



