import {Url} from '../../../constants/global.jsx'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url


const url = `${meServidorBackend}/produccion/empleados`;


export async function getFilter_Empleado(cod_subarea, txtFind) {
    if (txtFind.trim()===''){
        txtFind='%20'
    }
    const vLink = `${url}/${cod_subarea}/${txtFind}`
    console.log(vLink)
    const response = await fetch(vLink)
    const responseJson = await response.json()
    return responseJson
}



export async function save_Empleado(meJson){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const vLink = `${url}/`
    const response = await fetch(vLink, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function update_Empleado(meJson){
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const vLink = `${url}/`
    const response = await fetch(vLink, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function delete_Empleado(meJson){
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const vLink = `${url}/`
    const response = await fetch(vLink, requestOptions);
    const responseJson = await response.json();
    return responseJson
}




