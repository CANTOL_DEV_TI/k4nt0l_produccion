import React, {useState} from "react";
import Title from "../../../components/Titulo";
import {GrAdd, GrFormSearch} from "react-icons/gr";
import { BotonExcel, BotonNuevo, BotonBuscar } from "../../../components/Botones";

class Busqueda extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            txtInput: '',
            cod_subarea: ''
        }
    }

    handleChange = (e) =>{
        if (e.target.name === 'selectArea' ){
            this.setState({cod_subarea: e.target.value })
        }

        if (e.target.name === 'txtInput' ) {
            this.setState({txtInput: e.target.value})
        }
    }


    render() {

        const {handleBusqueda, handleModal} = this.props

        return(
            <div className="col-lg-12">

                <div className="card">

                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-xl-2 col-sm-2 p-1" style={{minWidth: "25%"}}>
                                <Title>Lista de Empleados</Title>
                            </div>

                            <div className="col-xl-8 col-sm-8 p-1" style={{minWidth: "70%"}}>
                                <div className="input-group">

                                    <select name="selectArea" className="form-control form-control-sm" onChange={this.handleChange} style={{marginRight: "5px"}}>
                                        <option value='0'>Seleccionar SubArea</option>
                                        {
                                            this.props.listAreas.map((v_item) =>
                                                this.state.codigo === v_item.codigo ?
                                                    <option selected
                                                            value={v_item.codigo}>{`${v_item.nombre}`}</option>
                                                    :
                                                    <option value={v_item.codigo}>{`${v_item.nombre}`}</option>
                                            )
                                        }
                                    </select>

                                    <input
                                        name="txtInput"
                                        placeholder="Ingrese Busqueda"
                                        onChange={this.handleChange}
                                        value={this.state.txtInput}
                                        style={{marginRight: "5px"}}
                                    />

                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true}
                                                 eventoClick={() => handleBusqueda(this.state.cod_subarea, this.state.txtInput)}
                                    />

                                    <BotonNuevo textoBoton={"Crear Nuevo"} sw_habilitado={true}
                                                eventoClick={() => handleModal(true)}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Busqueda;