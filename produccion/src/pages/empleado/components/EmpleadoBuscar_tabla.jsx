import { BotonAdd } from "../../../components/Botones"; 
 
export function EmpleadoBuscar_tabla({ datos_fila, eventoClick, txtFind }) {
    // RETORNAR INFORMACIÓN 
    return ( 
        <> 
            {/* INICIO TABLA */} 
            <div className="table-responsive"> 
                <table className="table table-hover table-sm table-bordered"> 
                    <thead className="table-secondary text-center table-sm"> 
                        <tr> 
                            <th className="align-middle" title="Opciones" style={{ width: '88px' }}>Opc</th> 
                            <th className="align-middle">Cod. Colaborador</th> 
                            <th className="align-middle">Nombre de Colaborador</th> 
                        </tr> 
                    </thead> 
                    <tbody className="list">
                    {console.log("@@@@@@@@@@")}
                    {console.log(txtFind.buscarDato)}
                    {console.log("@@@@@@@@@@")}


                        {datos_fila.length > 0 && txtFind.buscarDato.length === 8 ?
                            ( 
                                datos_fila.map((datos, index) => { 
                                    return <tr key={index + 1}> 
                                        <td> 
                                            <BotonAdd eventoClick={() => eventoClick(datos)} textoBoton={"Add"} title={"Seleccionar"} /> 
                                        </td> 
                                        <td className="td-cadena">{datos.cod_empleado}</td> 
                                        <td className="td-cadena">{datos.nombre}</td> 
                                    </tr> 
                                }) 
                            ) : 
                            ( 
                                <tr> 
                                    <td className="text-center" colSpan="3">Sin datos</td> 
                                </tr> 
                            ) 
                        } 
                    </tbody> 
                </table> 
            </div> 
            {/* FIN TABLA */} 
        </> 
    ); 
}