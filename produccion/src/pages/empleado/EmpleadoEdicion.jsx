import React from "react";
import { BotonGuardar, BotonConsultar } from "../../components/Botones";



class EmpleadoEdicion extends React.Component{

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null){
            this.state = {cod_empleado: 0,cod_subarea: 0, nombre: ''}
        }else {
            this.state = {cod_empleado: this.props.dataToEdit.cod_empleado, cod_subarea: this.props.dataToEdit.cod_subarea, nombre: this.props.dataToEdit.nombre}
        }
    }

    handleChange = (e) =>{
        if (e.target.name === 'cod_empleado'){
            this.setState({cod_empleado: e.target.value})
        }

        if (e.target.name === 'nombre'){
            this.setState({nombre: e.target.value})
        }

        if (e.target.name === 'selectArea'){
            this.setState({cod_subarea: e.target.value})
        }

    }


    render() {
        const {eventOnclick, nameOpe, listAreas} = this.props

        return(
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-3 col-form-label">SubArea : </div>
                            <div className="col-sm-6 col-form-label">
                                <select name="selectArea"  className="form-control form-control-sm"  onChange={this.handleChange}>
                                    <option value='0'>Seleccionar SubArea</option>
                                    {
                                        listAreas.map((v_item) =>
                                            this.state.cod_subarea===v_item.codigo ?
                                                <option selected value={v_item.codigo}>{`${v_item.nombre}`}</option>
                                            :
                                                <option value={v_item.codigo}>{`${v_item.nombre}`}</option>
                                        )
                                    }
                                </select>
                            </div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-3 col-form-label">
                                <input type="text" className="form-control form-control-sm" name="cod_empleado" onChange={this.handleChange} value={this.state.cod_empleado}/>
                            </div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-3 col-form-label">Nombre : </div>
                            <div className="col-sm-8 col-form-label">
                            <input type="text" className="form-control form-control-sm" name="nombre" onChange={this.handleChange} value={this.state.nombre}/>
                            </div>

                    </div>
                </div>
            </div>
            <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({"cod_empleado": this.state.cod_empleado, "cod_subarea": parseInt(this.state.cod_subarea), "nombre": this.state.nombre, "sw_estado": "1"})} texto={nameOpe} />
            </div>

        )
    }
}

export default EmpleadoEdicion;