import React, { useState, useEffect } from 'react' 

import { get_empleado_subarea } from '../../services/empleadoServices';
import { get_RegistrosEmpleado } from "../oee/services/oeeEmpleadosServices.jsx";

import { InputTextBuscar } from "../../components/InputTextBuscar";
import { EmpleadoBuscar_tabla } from "./components/EmpleadoBuscar_tabla"; 
 
export function EmpleadoBuscar({ eventoClick, cod_subarea = "0" }) { 
    // CONSUMIR DATOS 
    const [listDatos, setListDatos] = useState([]);
    useEffect(() => { 
        getData(); 
    }, []) 

    // Cargar datos a listado 
    const getData = () => {


        get_empleado_subarea(cod_subarea) 
            .then(res => { 
                setListDatos(res) 
                setCurrentPage(0); 
            }) 
    }; 
 
    // BUSQUEDA EN DOM 
    const [buscarDato, setBuscarDato] = useState(""); 
    const idBusqueda = ["cod_empleado", "nombre"]; 
    const search = (listDatos) => { 
        if (buscarDato.length < 8)
            return listDatos.slice(currentPage, currentPage + LimitPag);
 
        // SI HAY VALOR EN CAJA DE BUSQUEDA 
        const filtered = listDatos.filter((item) => 
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato)) 
        ); 
        return filtered.slice(currentPage, currentPage + LimitPag); 
    }; 
 
    // Seleccionar datos de tabla 
    const eventoSeleccionar = (datos) => { 
        eventoClick(datos); 
    }; 
 
    // PAGINACIÓN  
    let LimitPag = 10; 
    const [currentPage, setCurrentPage] = useState(0); 
    const nextPage = () => { 
        if (listDatos.length > currentPage + LimitPag) 
            setCurrentPage(currentPage + LimitPag); 
    } 
 
    const prevPage = () => { 
        if (currentPage > 0) 
            setCurrentPage(currentPage - LimitPag); 
    } 
 
    const onSearchChange = (e) => { 
        setCurrentPage(0); 
        setBuscarDato(e.target.value.toLowerCase().trim()); 
    } 
 
    // RETORNAR INFORMACIÓN 
    return ( 
        <div className="col-lg-12"> 
            <div className="card"> 
                {/* INICIO BARRA DE NAVEGACION */} 
                <div className="card-header border border-dashed border-end-0 border-start-0"> 
                    <div className="row align-items-center gy-3"> 
                        <div className="col-5">
                            <h5>Colaboradores</h5>
                        </div> 
                        <div className="col-sm"> 
                            <div className="d-flex justify-content-end gap-1"> 
                                <InputTextBuscar onBuscarChange={onSearchChange} /> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
 
                {/* FIN BARRA DE NAVEGACION */} 
 
                { /* INICIO TABLA */} 
                <EmpleadoBuscar_tabla datos_fila={search(listDatos)} eventoClick={eventoSeleccionar} txtFind={{buscarDato}} />
                {/* FIN TABLA */} 
 
                {/* INICIO PAGINACION */} 
                <div className="d-flex justify-content-end m-2"> 
                    <div className="pagination-wrap hstack gap-1"> 
                        Total Reg. {listDatos.length} 
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button> 
                        <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button> 
                    </div> 
                </div> 
                {/* FIN PAGINACION */} 
            </div> 
        </div> 
    ); 
}