import React from "react";
import { map } from "react-bootstrap/ElementChildren";

let listaDiaSemana = ["L", "M", "M", "J", "V", "S", "D"]


const TableCabecera = ({ key,
    calendario }) =>
(
    <thead>
        <tr className="cabecera">
            <th rowSpan='2' className="meCabIni static">Cod.Artículo</th>
            <th rowSpan='2' className="meCab2 meCabCentro">Artículo</th>
            {calendario.map((semana) =>
                semana.map((dia, index) =>
                    <th className="meCab2 meCabCentro">{listaDiaSemana[index]}</th>
                )
            )}
            <th rowSpan='2' className="meCab4 meCabCentro">Total Fab.</th>
            <th rowSpan='2' className="meCabFin meCabCentro">Obj. Fab</th>
        </tr >

        <tr className="cabecera">
            {calendario.map((semana) =>
                semana.map((fecha) =>
                    <th className="meCab2 meCabCentro">{fecha}</th>
                )
            )}
        </tr>
    </thead>
)
export default TableCabecera;