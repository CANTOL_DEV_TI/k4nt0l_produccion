import { BsPencilFill, BsXCircleFill } from "react-icons/bs";

export function PlanCapacidadPTPanel_tabla({ datos_fila, eventoClick }) {
    // RETORNAR INFORMACIÓN 
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" rowSpan="2" title="Código artículo">Cod.Artículo</th>
                            <th className="align-middle" rowSpan="2" title="Descripción Artículo">Descripción Artículo</th>
                            <th className="align-middle" title="Stock inicial (Und)">Stock</th>
                            <th className="align-middle" title="Cobertura inicial (Días)">Cobertura</th>
                            <th className="align-middle" colSpan="2" title="Cantidad producción planificado (Und)">Producción</th>
                            <th className="align-middle" title="Plan de venta (Und)">Plan</th>
                            <th className="align-middle" title="Stock final (Und)">Stock</th>
                            <th className="align-middle" title="Cobertura final (Días)">Cobertura</th>
                            <th className="align-middle" rowSpan="2" title="Clasificación ABC">ABC</th>
                            <th className="align-middle" rowSpan="2" title="Familia">Familia</th>
                            <th className="align-middle" colSpan="5" title="Stock minimo (Und)">Stock (Und)</th>
                            <th className="align-middle" colSpan="3" title="Cobertura (Días)">Cob.(Días)</th>
                            <th className="align-middle" rowSpan="2" title="Consumo promedio diario plan">CPDP</th>
                            <th className="align-middle" rowSpan="2" title="Producción de unidades por hora">Und.Hora</th>
                            <th className="align-middle" rowSpan="2" title="Cantidad planeado sugerido">Objetivo</th>
                        </tr>
                        <tr>
                            <th className="align-middle" title="Stock inicial (Und)">Inicial</th>
                            <th className="align-middle" title="Cobertura inicial (Días)">Inicial</th>
                            <th className="align-middle" title="Cantidad producción planificado (Und)">Plan</th>
                            <th className="align-middle" title="Cantidad producción planificado (Und)">Real</th>
                            <th className="align-middle" title="Plan de venta (Und)">Venta</th>
                            <th className="align-middle" title="Stock final (Und)">Final</th>
                            <th className="align-middle" title="Cobertura final (Días)">Final</th>
                            <th className="align-middle" title="Stock minimo (Und)">Minimo</th>
                            <th className="align-middle" title="Stock ideal minimo (Und)">Ideal.Min</th>
                            <th className="align-middle" title="Stock ideal (Und)">Ideal</th>
                            <th className="align-middle" title="Stock ideal maximo (Und)">Ideal.Max</th>
                            <th className="align-middle" title="Stock maximo (Und)">Maximo</th>
                            <th className="align-middle" title="Cobertura minimo (Días)">Min</th>
                            <th className="align-middle" title="Cobertura ideal (Días)">Ide</th>
                            <th className="align-middle" title="Cobertura maximo (Días)">Max</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td className="td-cadena">{datos.cod_articulo}</td>
                                        <td className="td-cadena">{datos.articulo}</td>
                                        <td className="td-num" style={{ backgroundColor: "#F2F3F4", minWidth: "60px" }}>
                                            <span className="badge" style={{ float: "left", backgroundColor: `${datos.semaforo_inicial}` }}>&nbsp;</span>
                                            {datos.stock_inicial_almacen.toLocaleString("en")}
                                        </td>
                                        <td className="td-num">{datos.cob_inicial_almacen.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#EBF5FB", minWidth: "50px" }}>{datos.cant_planificada.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#EBF5FB", minWidth: "50px" }}>{datos.cant_real.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.cant_plan_venta.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#F2F3F4", minWidth: "60px" }}>
                                            <span className="badge" style={{ float: "left", backgroundColor: `${datos.semaforo_final}` }}>&nbsp;</span>
                                            {datos.stock_final_almacen.toLocaleString("en")}
                                        </td>
                                        <td className="td-num">{datos.cob_final_almacen.toLocaleString("en")}</td>
                                        <td className="td-cadena">{datos.abc}</td>
                                        <td className="td-cadena">{datos.nom_familia}</td>
                                        <td className="td-num" style={{ backgroundColor: "#FADBD8" }}>{datos.stock_minimo.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#D4EFDF" }}>{datos.stock_ideal_min.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#D4EFDF" }}>{datos.stock_ideal.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#D4EFDF" }}>{datos.stock_ideal_max.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#D5D8DC" }}>{datos.stock_maximo.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#FADBD8" }}>{datos.cob_minimo_dia.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#D4EFDF" }}>{datos.cob_ideal_dia.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#D5D8DC" }}>{datos.cob_maximo_dia.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.cpdp.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.cant_und_hora.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.cant_objetivo.toLocaleString("en")}</td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="19">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}