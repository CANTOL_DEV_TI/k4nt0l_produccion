import React from "react";


class ProgramacionPtEdicion extends React.Component{

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null){
            this.state = {id_plan: 0, cod_articulo: '', nro_dia: 0, cantidad: 0, horasDisponibles: 0, horasConsumidosTotalXDia: 0, horasConsumidoFab: 0}
        }else {
            this.state = {id_plan: this.props.dataToEdit.id_plan, cod_articulo: this.props.dataToEdit.cod_articulo, nro_dia: this.props.dataToEdit.nro_dia, cantidad: this.props.dataToEdit.cantidad, horasDisponible: this.props.dataToEdit.horasDisponible, horasConsumidosTotalXDia: this.props.dataToEdit.horasConsumidosTotalXDia, horasConsumidoFab: this.props.dataToEdit.horasConsumidoFab}
        }
    }

    handleChange = (e) =>{
        if (e.target.name === 'id_plan'){
            this.setState({id_plan: e.target.value})
        }

        if (e.target.name === 'cod_articulo'){
            this.setState({cod_articulo: e.target.value})
        }

        if (e.target.name === 'nro_dia'){
            this.setState({nro_dia: e.target.value})
        }

        if (e.target.name === 'cantidad'){
            this.setState({cantidad: e.target.value})
        }

        if (e.target.name === 'nro_dia'){
            this.setState({horasDisponible: e.target.value})
        }


    }


    render() {
        const {eventOnclick, nameOpe} = this.props

        return(
            <table className="formModal">
                <tr>
                    <td className="tdLabel">Id Plan</td>
                    <td className="tdSeparador">:</td>
                    <td className="tdvalue" colspan="4"> <input type="text" name="id_plan" onChange={this.handleChange} value={this.state.id_plan} disabled/></td>
                </tr>

                <tr>
                    <td className="tdLabel">Cod Articulo</td>
                    <td className="tdSeparador">:</td>
                    <td className="tdvalue" colspan="4"><input type="text" name="cod_articulo" onChange={this.handleChange} value={this.state.cod_articulo} disabled/></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td className="tdLabel">Nro Dia</td>
                    <td className="tdSeparador">:</td>
                    <td className="tdvalue" colspan="4"><input type="text" name="nro_dia" onChange={this.handleChange} value={this.state.nro_dia} disabled/></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td className="tdLabel">H. Dia</td>
                    <td className="tdSeparador">:</td>
                    <td className="tdvalue"><input type="text" name="horas_dia" onChange={this.handleChange} value={this.state.horasDisponible} disabled/></td>

                    <td className="tdLabel">H. Consumo Total</td>
                    <td className="tdSeparador">:</td>
                    <td className="tdvalue"><input type="text" name="horas_dia" onChange={this.handleChange} value={this.state.horasConsumidosTotalXDia} disabled/></td>
                </tr>

                <tr>
                    <td className="tdLabel">Cantidad</td>
                    <td className="tdSeparador">:</td>
                    <td className="tdvalue"><input type="text" name="cantidad" onChange={this.handleChange} value={this.state.cantidad} /></td>

                    <td className="tdLabel">H. Consumo Fab</td>
                    <td className="tdSeparador">:</td>
                    <td className="tdvalue"><input type="text" name="horas_dia" onChange={this.handleChange} value={this.state.horasConsumidoFab} disabled/></td>
                </tr>

                <tr>
                    <td colspan="6" className="tdOperaciones"><button onClick={() => eventOnclick({"id_plan_capacidad": this.state.id_plan, "cod_articulo": this.state.cod_articulo, "nro_dia": this.state.nro_dia, "cant": this.state.cantidad})}>{nameOpe}</button></td>
                </tr>
            </table>
        )
    }
}

export default ProgramacionPtEdicion;
