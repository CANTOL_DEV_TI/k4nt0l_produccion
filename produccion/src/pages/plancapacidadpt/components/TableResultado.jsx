import React from "react";
import {GrEdit} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";

let listaDiaSemana = ["L", "M", "M", "J", "V", "S", "D"]

var horasAcumuladas = {}

function incrementar(meIndex){
    var diaSemana = listaDiaSemana[meIndex]
    meIndex += 1

}

function acumular(diaFecha, tiempoComprometido){
    if (!(diaFecha in horasAcumuladas)){
        horasAcumuladas[diaFecha] = 0
    }
    horasAcumuladas[diaFecha] = horasAcumuladas[diaFecha] + tiempoComprometido
}

var estadoProgramacion = "data";

const TableResultado = ({key,
                        resultados,
                        calendario,
                        eventoEditar,
                        horariosSemana,
                        horasConsumidosTotalXDia}) =>
    (



        <tbody>
        {resultados.map((registro)=>

            // <tr className="data">
            <tr className={
                (0<registro.totalFabricacion && registro.totalFabricacion<registro.objProduccion) ? "danger_Medio" :
                (0>=registro.totalFabricacion && registro.totalFabricacion<registro.objProduccion) ? "danger_Alto" :
                "data"}>
                <td className="colString static" id={registro.cod_articulo}>{registro.cod_articulo}</td>
                <td className="colString static" id={registro.nom_articulo}>{registro.nom_articulo}</td>

                {calendario.map((semana)=>
                    semana.map((fechaDia, index)=>
                        <td className="colCentro clickme" onClick={()=>eventoEditar({
                            cod_articulo: registro.cod_articulo,
                            dia:listaDiaSemana[index],
                            diaFecha: fechaDia,
                            cantProgramacion: registro.programacion[fechaDia]===undefined ? 0 :registro.programacion[fechaDia][0],
                            horasDisponible: horariosSemana[index],
                            horasConsumidosTotalXDia: horasConsumidosTotalXDia[fechaDia]===undefined ? 0 :horasConsumidosTotalXDia[fechaDia],
                            horasConsumidoFab: registro.programacion[fechaDia]===undefined ? 0 :registro.programacion[fechaDia][1]
                        })}>

                            {/*<td className="colCentro clickme" onClick={()=>eventoEditar(true)}>*/}

                            {registro.programacion[fechaDia]===undefined ? '0' :registro.programacion[fechaDia][0]}
                        </td>
                    )
                )}

                <td className="colNumero">{registro.totalFabricacion}</td>
                <td className="colNumero">{registro.objProduccion}</td>

            </tr>

        )}


        </tbody>
    )


export default TableResultado;


