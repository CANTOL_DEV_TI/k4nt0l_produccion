import React from "react";
import { BsSearch, BsCardChecklist, BsXCircleFill, BsPlusLg } from "react-icons/bs";


const ResultadoTabla = ({
    codigo_planificacion,
    codigo_plan_pp,
    periodo_planificacion,
    estado,
    eventoTotal,
    eventoDetalles,
    eventoTotalPP,
    eventoDetallePP,
    eventoOperacion }) =>
(
    <tbody>
        <tr>
            <td>
                <button className="btn btn-primary btn-sm ms-1" onClick={() => eventoTotal(true)}><BsSearch />Resumen</button>&nbsp;
                <button className="btn btn-outline-primary btn-sm ms-1" onClick={() => eventoDetalles(true)}><BsCardChecklist />Detalle</button>
                <button data-toggle="tooltip" title="Eliminar plan PT" className={codigo_plan_pp > 0 ? 'btn btn-danger btn-sm ms-2 disabled' : 'btn btn-danger btn-sm ms-2'}
                    onClick={() => eventoOperacion(codigo_planificacion, "DEL_PT")}>
                    <BsXCircleFill />
                </button>
            </td>
            <td>
                <button data-toggle="tooltip" title="Crear plan de Producto en Proceso" className={codigo_plan_pp > 0 ? 'btn btn-success btn-sm ms-2 disabled' : 'btn btn-success btn-sm ms-2'}
                    onClick={() => eventoOperacion(codigo_planificacion, "CREATE_PP")}>
                    <BsPlusLg />
                </button>
                <button className={codigo_plan_pp === 0 ? 'btn btn-secondary btn-sm ms-1 disabled' : 'btn btn-secondary btn-sm ms-1'} onClick={() => eventoTotalPP(true)}><BsSearch />Resumen</button>&nbsp;
                <button className={codigo_plan_pp === 0 ? 'btn btn-outline-secondary btn-sm ms-1 disabled' : 'btn btn-outline-secondary btn-sm ms-1'} onClick={() => eventoDetallePP(true)}><BsCardChecklist />Detalle</button>
                <button data-toggle="tooltip" title="Eliminar plan PP" className={codigo_plan_pp === 0 ? 'btn btn-danger btn-sm ms-1 disabled' : 'btn btn-danger btn-sm ms-1'}
                    onClick={() => eventoOperacion(codigo_plan_pp, "DEL_PP")}>
                    <BsXCircleFill />
                </button>
            </td>
            <td className="td-cadena" id={codigo_planificacion}>{codigo_planificacion}</td>
            <td className="td-cadena" id={codigo_plan_pp}>{codigo_plan_pp}</td>
            <td className="align-middle" >{periodo_planificacion}</td>
            <td className="align-middle">{estado}</td>
        </tr>
    </tbody>
)

export default ResultadoTabla;