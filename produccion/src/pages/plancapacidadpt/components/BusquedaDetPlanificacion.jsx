import React, { useState } from "react";

import {GrAdd, GrFormSearch} from "react-icons/gr";
class BusquedaDetPlanificacion extends React.Component {

    constructor(props) {
        super(props);

        this.state = { txtInput: '' }
    }

    handleChange = (e) => {
        this.setState({ txtInput: e.target.value })
    }


    render() {

        const { handleBusqueda, handleBack } = this.props

        return (
            <div className="busqueda-container">
                <div>
                    <input name="txtFind"
                        className="busqueda-input"
                        placeholder="Ingrese Busqueda"
                        onChange={this.handleChange}
                        value={this.state.txtInput}
                    />

                    <button
                        name="btnBuscar"
                        className="busqueda-btn"
                        onClick={() => handleBusqueda(this.state.txtInput)}
                    >
                        <GrFormSearch/>Buscar
                    </button>

                    <button name="btnNuevo"
                            className="nuevo-btn"
                            onClick={() => handleBack('aa')}>
                        <GrAdd/>Atras
                    </button>
                </div>
            </div>
        )
    }

}

export default BusquedaDetPlanificacion;