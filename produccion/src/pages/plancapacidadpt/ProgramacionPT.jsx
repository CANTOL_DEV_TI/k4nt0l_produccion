import React from "react";
import {getFilter_ProgramacionPT, update_ProgramacionPT} from "../../services/programacionPtServices";

import TableCabecera from "./components/TableCabecera";
import TableResultado from "./components/TableResultado";

import VentanaModal from "../../components/VentanaModal.jsx";

import "../../assets/css/styleStandar.css"
import {delete_Area, getFilter_Area, save_Area, update_Area} from "../../services/areaServices.jsx";

import ProgramacionPtEdicion from "./components/ProgramacionPtEdicion";

import BusquedaDetPlanificacion from "./components/BusquedaDetPlanificacion";

import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

class ProgramacionPt extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            calendario: [],
            horasDisponiblesSemana: [],
            horasConsumidosTotalXDia: null,
            resultados: [],
            isFetch: true,
            id_programacion: this.props.idPlan,
            // id_programacion: 1,
            showModal: false,
            dataRegistro: null,
            tipoOpe: 'Find'
        }
    }

    editarRegistro = async (props) => {
        this.setState({
            showModal: true,
            dataRegistro: {
                id_plan: this.state.id_programacion,
                cod_articulo: props.cod_articulo,
                nro_dia: props.diaFecha,
                cantidad: props.cantProgramacion,
                horasDisponible: props.horasDisponible,
                horasConsumidosTotalXDia: props.horasConsumidosTotalXDia,
                horasConsumidoFab: props.horasConsumidoFab===undefined ? 0 :props.horasConsumidoFab
            }
        })

        console.log(this.state.showModal)
        console.log(this.state.dataRegistro)
        // alert(props.diaFecha)
        // alert(props.cantProgramacion)
    }

    handleBusqueda = async(txtFind) => {
        const responseJson = await getFilter_ProgramacionPT(this.state.id_programacion, txtFind)
        console.log(responseJson)
        this.setState({
            resultados: responseJson['data'],
            isFetch: false,
            calendario: responseJson['calendario'],
            horasDisponiblesSemana: responseJson['horarios'],
            horasConsumidosTotalXDia: responseJson['horasConsumidos']}
        )

        console.log(this.state)
    }

    handleSubmit = async (e) =>{
        console.log(e)
        const responseJson = await update_ProgramacionPT(e)
        console.log(responseJson)
        this.setState({showModal: false, tipoOpe: 'Find'})
        // this.handleBusqueda('aaa')
    }


    render() {

        const {eventOnclick} = this.props
        const {isFetch, resultados, showModal, VentanaSeguridad} = this.state

        return(
            <React.Fragment>
                {/*<a href="#PT0106030014">click</a>*/}
                {/*<button onClick={() => eventOnclick('aa')}>Regresar</button>*/}
                <BusquedaDetPlanificacion handleBusqueda={this.handleBusqueda} handleBack={eventOnclick}/>
                <div className="resultado-container">
                    <table className="table-resultado_Stock">
                        <TableCabecera key='1' calendario={this.state.calendario}/>
                        <TableResultado eventoEditar={this.editarRegistro} key='1' calendario={this.state.calendario} resultados={this.state.resultados} horariosSemana={this.state.horasDisponiblesSemana} horasConsumidosTotalXDia={this.state.horasConsumidosTotalXDia}/>
                    </table>

                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={()=> this.setState({showModal: false})}
                    titulo="Ventana Edicion"
                >
                    <ProgramacionPtEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={'GUARDAR'}
                    />

                </VentanaModal>
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}

                />

            </React.Fragment>
        )
    }

    async componentDidMount__(){
                
        const ValSeguridad = await Validar("PROCPT")
        this.setState({ "VentanaSeguridad": ValSeguridad })
    }
}


export default ProgramacionPt;



