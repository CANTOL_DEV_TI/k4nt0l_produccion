import React from "react";

import { delete_plan_capacidad_pp, insert_plan_capacidad_pp } from '../../services/planCapacidadPPServices';
import { delete_plan_capacidad_pt } from '../../services/planCapacidadPTServices';

import { getFilter_PlanCapacidadPT } from "../../services/plancapacidadpt";

import Busqueda from "../plancapacidadpt/components/Busqueda";
import ResultadoTabla from "../plancapacidadpt/components/ResultadoTabla";


import ProgramacionPT from "../planCapacidadPT/ProgramacionPT.jsx";
import { PlanCapacidadPTPanel } from "./PlanCapacidadPTPanel.jsx";
import { PlanCapacidadPPPanel } from "../planCapacidadPP/PlanCapacidadPPPanel.jsx";
import { PlanCapacidadPPDetalle } from "../planCapacidadPP/PlanCapacidadPPDetalle";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

class PlancapacidadPT extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            tipoOpe: 'Find',
            showComponente: 'principal',
            idPlanComponente: 0,
            txtBusqueda: 0,
        }
    }

    handleBusqueda = async (txtFind) => {

        if (txtFind === "") {
            alert("Para la busqueda debe ingresar un número de ejercicio")
        } else {

            if (!isNaN(+txtFind) === true) {
                const responseJson = await getFilter_PlanCapacidadPT(txtFind)
                this.setState({ resultados: responseJson, isFetch: false, txtBusqueda: txtFind })
            } else {
                alert("Para la busqueda debe ingresar un número de ejercicio")
            }
        }
    }

    handleSubmit = (e) => {
        this.setState({ showComponente: 'principal', idPlanComponente: 0 })
    }

    handleService = (datos, opc = "") => {
        if (datos > 0) {
            if (opc === "DEL_PP") {
                console.log(datos)
                delete_plan_capacidad_pp(datos);
            }
            if (opc === "DEL_PT") {
                console.log(datos)
                delete_plan_capacidad_pt(datos);
            }
            if (opc === "CREATE_PP") {
                console.log(datos)
                insert_plan_capacidad_pp(datos);
            }
            this.handleBusqueda(this.state.txtBusqueda);
            this.handleSubmit;
        }
    }
    render() {
        const { isFetch, resultados, VentanaSeguridad } = this.state

        return (
            <React.Fragment>

                {this.state.showComponente != 'principal' ?
                    this.state.showComponente === 'detalle_PT' ?
                        <ProgramacionPT idPlan={this.state.idPlanComponente} eventOnclick={this.handleSubmit} />
                        : this.state.showComponente === 'detalle_PP' ?
                            <PlanCapacidadPPDetalle idPlan={this.state.idPlanComponente} eventOnclick={this.handleSubmit} />
                            : this.state.showComponente === 'total_PP' ?
                                <PlanCapacidadPPPanel idPlan={this.state.idPlanComponente} eventOnclick={this.handleSubmit} />
                                : <PlanCapacidadPTPanel idPlan={this.state.idPlanComponente} eventOnclick={this.handleSubmit} />
                    :
                    <>

                        <Busqueda handleBusqueda={this.handleBusqueda} showComponente={'nuevo'} handleModal={() => this.setState({ dataRegistro: null, tipoOpe: 'Grabar', showComponente: 'nuevo' })} />

                        {isFetch && 'Cargando'}
                        {(!isFetch && !resultados.length) && 'Sin Información'}

                        <div className="table-responsive">
                            <table className="table table-hover table-sm table-bordered">
                                <thead className="table-secondary text-center table-sm">
                                    <tr>
                                        <th className="align-middle" style={{ width: "240px" }}>Producto Terminado</th>
                                        <th className="align-middle" style={{ width: "280px" }}>Producto en Proceso</th>
                                        <th className="align-middle">Id Plan PT</th>
                                        <th className="align-middle">Id Plan PP</th>
                                        <th className="align-middle">Periodo</th>
                                        <th className="align-middle">Estado</th>
                                    </tr>
                                </thead>

                                {resultados.map((registro) =>
                                    <ResultadoTabla
                                        key={registro.codigo_planificacion}
                                        codigo_planificacion={registro.codigo_planificacion}
                                        codigo_plan_pp={registro.id_plan_capacidad_pp}
                                        periodo_planificacion={registro.periodo_planificacion}
                                        estado={registro.Estado}
                                        eventoDetalles={() => this.setState({ showComponente: 'detalle_PT', idPlanComponente: registro.codigo_planificacion })}
                                        eventoTotal={() => this.setState({ showComponente: 'total_PT', idPlanComponente: registro.codigo_planificacion })}
                                        eventoTotalPP={() => this.setState({ showComponente: 'total_PP', idPlanComponente: registro.id_plan_capacidad_pp })}
                                        eventoDetallePP={() => this.setState({ showComponente: 'detalle_PP', idPlanComponente: registro.id_plan_capacidad_pp })}
                                        eventoOperacion={this.handleService}
                                    />
                                )}
                            </table>
                        </div>

                    </>
                }
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}

                />
            </React.Fragment>
        )
    }
    async componentDidMount() {

        const ValSeguridad = await Validar("PROCPT")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
        
    }
}

export default PlancapacidadPT;