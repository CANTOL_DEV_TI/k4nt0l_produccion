import React, { useState, useEffect } from 'react'
import { get_familia_activo } from '../../services/familiaServices';
import { get_plan_capacidad_pt_resumen } from '../../services/planCapacidadPTServices';
import { BotonExcel } from "../../components/Botones";
import { InputTextBuscar } from "../../components/InputTextBuscar";
import { PlanCapacidadPTPanel_tabla } from "./components/PlanCapacidadPTPanel_tabla";
import ComboBox from "../../components/ComboBox";
import Title from "../../components/Titulo";
import { Validar } from '../../services/ValidaSesion.jsx';
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";
// CARGAR MES 
const listAbc = [
    { "codigo": "A", "nombre": "A" },
    { "codigo": "B", "nombre": "B" },
    { "codigo": "C", "nombre": "C" }
]

export function PlanCapacidadPTPanel({ idPlan, eventOnclick }) {
    
    // seguridad
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
    const Seguridad = async () => {
        const ValSeguridad = await Validar("PROCPT")
        setVentanaSeguridad(ValSeguridad.Validar)
    }
    

    // CONSUMIR DATOS
    const [dataToEdit, setDataToEdit] = useState(null);
    const [idPlanCapacidad, setIdPlanCapacidad] = useState(idPlan);
    const [idFamilia, setIdFamilia] = useState("0");
    const [abc, setAbc] = useState("0");

    const [listDatos, setListDatos] = useState([]);
    const [listDatosBoom, setListDatosBoom] = useState([]);
    useEffect(() => {
        getData();
    }, [idPlanCapacidad, abc, idFamilia])

    //
    const getData = () => {
        get_plan_capacidad_pt_resumen(idPlanCapacidad, idFamilia, abc)
            .then(res => {
                setListDatos(res)

                // guardar para boon
                const new_data = res.map(data => {
                    return {
                        "COD_ARTICULO": data.cod_articulo, "CANTIDAD": data.cant_planificada, "ARTICULO": data.articulo
                    }
                });
                setListDatosBoom(new_data)
                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM 
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_articulo", "articulo", "abc", "nom_familia"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA 
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // cargar combo familia     
    const [familia, setFamilia] = useState([])
    useEffect(() => {
        Seguridad();
        get_familia_activo()
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.id_familia, "nombre": data.nom_familia
                    }
                })
                setFamilia(cbo_data)
            })
    }, []);

    // Captura el evento y los valores de los inputs 
    const handleChange = (evento) => {
        if (evento.target.name === 'abc') {
            setAbc((evento.target.value).toString());
        }
        if (evento.target.name === 'id_familia') {
            setIdFamilia((evento.target.value).toString());
        }
    };

    // PAGINACIÓN  
    let LimitPag = 30;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // MOSTRAR MODAL

    // RETORNAR INFORMACIÓN 
    return (
        <div className="col-lg-12">

            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-sm-auto">
                            <Title>Capacidad de Producto Terminado (General)</Title>
                        </div>
                        <div className="col-sm">
                            <div className="d-flex justify-content-end flex-wrap gap-1">
                                <InputTextBuscar onBuscarChange={onSearchChange} />
                                <ComboBox
                                    datosRow={listAbc}
                                    nombre_cbo="abc"
                                    manejaEvento={handleChange}
                                    valor_ini="ABC"
                                    valor={abc}
                                />
                                <ComboBox
                                    datosRow={familia}
                                    nombre_cbo="id_familia"
                                    manejaEvento={handleChange}
                                    valor_ini="Familia"
                                    valor={idFamilia}
                                />
                                <BotonExcel textoBoton={"Exportar"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatos} nombreArchivo={"Plan Capacidad PT"} />
                                <BotonExcel textoBoton={"Plan PT"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatosBoom} nombreArchivo={"Cantidad plan"} />
                                <button className="btn btn-sm btn-dark" onClick={() => eventOnclick('aa')}>Regresar</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <PlanCapacidadPTPanel_tabla datos_fila={search(listDatos)} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-end m-2">
                    <div className="pagination-wrap hstack gap-1">
                        Total Reg. {listDatos.length}
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                        <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>
            
            {/* seguridad */}
            <VentanaBloqueo show={VentanaSeguridad} />
        </div>
    );
}