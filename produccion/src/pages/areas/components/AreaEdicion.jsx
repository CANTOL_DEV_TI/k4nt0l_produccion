import React from "react";
import { BotonGuardar, BotonConsultar } from "../../../components/Botones";

class AreaEdicion extends React.Component{

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null){
            this.state = {codArea: 0, nomArea: ''}
        }else {
            this.state = {codArea: this.props.dataToEdit.cod_area, nomArea: this.props.dataToEdit.nom_area}
        }
    }

    handleChange = (e) =>{
        if (e.target.name === 'codigo'){
            this.setState({codArea: e.target.value})
        }

        if (e.target.name === 'nombre'){
            this.setState({nomArea: e.target.value})
        }
    }


    render() {
        const {eventOnclick, nameOpe} = this.props

        return(
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Codigo : </div>                    
                            <div className="col-sm-4 col-form-label"> <input type="text" className="form-control form-control-sm" name="codigo" onChange={this.handleChange} value={this.state.codArea}/></div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Nombre : </div>
                            <div className="col-sm-8 col-form-label"><input type="text" className="form-control form-control-sm" name="nombre" onChange={this.handleChange} value={this.state.nomArea}/></div>
                        </div>
                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "cod_area": this.state.codArea, "nom_area": this.state.nomArea })} texto={nameOpe} />
            </div>            
        )
    }
}

export default AreaEdicion;
