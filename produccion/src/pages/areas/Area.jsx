import React from "react";

import {getFilter_Area, save_Area, update_Area, delete_Area} from "../../services/areaServices.jsx";

import Busqueda from "../areas/components/Busqueda";
import ResultadoTabla from "../areas/components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal";
import AreaEdicion from "../areas/components/AreaEdicion";
import {  Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

import "../../assets/css/styleStandar.css"

class Area extends React.Component{
    constructor(props) {
        super(props);

        this.state ={
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find'
        }
    }

    handleBusqueda = async(txtFind) => {
        const responseJson = await getFilter_Area(txtFind)
        console.log(responseJson)
        this.setState({resultados: responseJson, isFetch: false})
        console.log(this.state)
    }

    handleSubmit = async (e) =>{
        console.log(e)


        if (this.state.tipoOpe === 'Grabar'){
            const responseJson = await save_Area(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar'){
            const responseJson = await update_Area(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar'){
            const responseJson = await delete_Area(e)
            console.log(responseJson)
        }


        this.setState({showModal: false, tipoOpe: 'Find'})
    }

    render() {
        const {isFetch, resultados, showModal, VentanaSeguridad} = this.state

        return(
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({showModal: true, dataRegistro:null, tipoOpe: 'Grabar'})}/>


                { isFetch && 'Cargando'}
                { (!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>  
                                <th className="meCabFin">Nombre Area</th>
                                <th className="meCab2 meCabCentro">Edicion</th>                                
                                <th className="meCabIni">Eliminacion</th>                                
                            </tr>
                        </thead>
                        {resultados.map((registro)=>
                            <ResultadoTabla
                                key={registro.cod_area}
                                nombreArea={registro.nom_area}
                                eventoEditar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar'})}
                                eventoEliminar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar'})}
                            />
                        )}
                    </table>
                </div>


                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({showModal: false})}
                    titulo="Edicion Area">
                        <AreaEdicion
                            dataToEdit={this.state.dataRegistro}
                            eventOnclick={this.handleSubmit}
                            nameOpe={this.state.tipoOpe}
                        />
                </VentanaModal>
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}
                />

            </React.Fragment>
        )

    }
    async componentDidMount() {
        const ValSeguridad = await Validar("PROARE")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
    }
}

export default Area;