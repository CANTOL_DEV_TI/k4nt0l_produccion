import React from "react"; 
 
import { getFilter_Tareas, save_Tareas, update_Tareas, delete_Tareas } from "../../services/tareaprogramadaUsuarioServices.jsx"; 

import Busqueda from  "./components/Busqueda"
import ResultadoTabla from "./components/ResultadoTabla"; 
import VentanaModal from "../../components/VentanaModal"; 
import TareaprogramadaUsuarioEdicion from "./components/tareaProgramadaUsuarioEdicion";
import { Validar } from '../../services/ValidaSesion.jsx';
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";
 
class TareaProgramadaUsuario extends React.Component { 
    constructor(props) { 
        super(props); 
 
        this.state = { 
            resultados: [], 
            isFetch: true, 
            dataRegistro: null, 
            showModal: false, 
            tipoOpe: 'Find' 
        } 
    } 
 
    handleBusqueda = async (txtDesde) => {
        const responseJson = await getFilter_Tareas(txtDesde) 
        console.log(responseJson) 
        this.setState({ resultados: responseJson, isFetch: false }) 
        console.log(this.state) 
    } 
 
    handleSubmit = async (e) => { 
   
        if (this.state.tipoOpe === 'Grabar') { 
            const responseJson = await save_Tareas(e) 
            console.log(responseJson) 
        } 
 
        if (this.state.tipoOpe === 'Actualizar') { 
            const responseJson = await update_Tareas(e) 
            console.log(responseJson) 
        } 
 
        if (this.state.tipoOpe === 'Eliminar') { 
            const responseJson = await delete_Tareas(e) 
            console.log(responseJson) 
            if(responseJson.Error!==""){
                alert(responseJson.Error)
            }
        } 
 
 
        this.setState({ showModal: false, tipoOpe: 'Find' }) 
    } 
 
    render() { 
        const { isFetch, resultados, showModal , VentanaSeguridad} = this.state 
 
        return ( 
            <React.Fragment> 
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} /> 
 
 
                {isFetch && 'Cargando'} 
                {(!isFetch && !resultados.length) && 'Sin Informacion'} 
 
 
                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}> 
                    <table className="table table-hover table-sm table-bordered"> 
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}> 
                            <tr> 
                                <th className="align-middle">Ejercicio</th>
                                <th className="align-middle">Periodo</th>
                                <th className="align-middle">Tarea Programada</th> 
                                <th className="align-middle">Tipo</th> 
                                <th className="align-middle">Usuario</th>                                 
                                <th className="align-middle">Eliminar</th> 
                            </tr> 
                        </thead> 
                         
                        {resultados.map((registro) => 
                            <ResultadoTabla 
                                key={registro.id_tarea} 
                                periodo_tarea={registro.periodo_tarea}
                                ejercicio_tarea={registro.ejercicio_tarea}
                                fecha_tarea={registro.fecha_tarea} 
                                tipo_tarea={registro.tipo_tarea}
                                cod_usuario={registro.cod_usuario}                                 
                                eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })} 
                            /> 
                        )} 
                    </table> 
                </div> 
 
                <VentanaModal 
                    show={showModal} 
                    size="lg" 
                    handleClose={() => this.setState({ showModal: false })} 
                    titulo="Nueva Tarea Programada de Usuario"> 
                    <TareaprogramadaUsuarioEdicion  
                        dataToEdit={this.state.dataRegistro} 
                        eventOnclick={this.handleSubmit} 
                        nameOpe={this.state.tipoOpe} 
                    /> 
                </VentanaModal> 
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}

                />
            </React.Fragment> 
        ) 
 
    } 
    async componentDidMount(){
        
        const ValSeguridad = await Validar("PROPRT")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
    }
} 
 
export default TareaProgramadaUsuario;