import React, { useState } from "react"; 
import Title from "../../../components/Titulo"; 
import { BotonExcel, BotonNuevo, BotonBuscar } from "../../../components/Botones"; 
 
class Busqueda extends React.Component {
 
    constructor(props) { 
        super(props); 
 
        this.state = { txtDesde: "", fechav : ""} 
         
         
    } 
 
    handleChange = (e) => {     
        console.log("Desde")     
        this.setState({fechav : e.target.value}) 
        const fecha = e.target.value.split('-').join('') 
        this.setState({ txtDesde: fecha })      
        console.log(this.state.txtDesde)       
        console.log(this.state.fechav)         
        
    } 
 
    render() { 
 
        const { handleBusqueda, handleModal } = this.props 
 
        return ( 
            <div className="col-lg-12"> 
 
                <div className="card"> 
                    {/* INICIO BARRA DE NAVEGACION */} 
                    <div className="card-header border border-dashed border-end-0 border-start-0"> 
                        <div className="row align-items-center gy-3"> 
                            <div className="col-sm"> 
                                <Title>Lista de Tareas</Title> 
                            </div> 
                            <div className="col-sm-auto"> 
                                <div className="d-flex flex-wrap gap-1"> 
                                    <input type="date" 
                                        placeholder="Ingrese Busqueda"
                                        onChange={this.handleChange} 
                                        value={this.setState.fechav}                                         
                                    /> 
 
                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda(this.state.txtDesde)} /> 
 
                                    <BotonNuevo textoBoton={"Crear Nuevo"} sw_habilitado={true} eventoClick={() => handleModal(true)} /> 


                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        ) 
    } 
 
} 
 
export default Busqueda;