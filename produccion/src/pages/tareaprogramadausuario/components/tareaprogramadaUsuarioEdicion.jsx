import React from "react";
import { BotonGuardar } from "../../../components/Botones";

class TareaprogramadaUsuarioEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { id_tarea: 0, fecha_tarea: '', fecha_busqueda: '', cod_usuario: '', txtFecha: '', ejercicio_tarea: 0, periodo_tarea: 0, tipo_tarea: '' }
        } else {
            this.state = { id_tarea: this.props.dataToEdit.id_tarea, fecha_tarea: this.props.dataToEdit.fecha_tarea, fecha_busqueda: this.props.dataToEdit.fecha_busqueda, cod_usuario: this.props.dataToEdit.cod_usuario, ejercicio_tarea: this.props.dataToEdit.ejercicio_tarea, periodo_tarea: this.props.dataToEdit.periodo_tarea, tipo_tarea: this.props.dataToEdit.tipo_tarea }
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'codigo') {
            this.setState({ id_tarea: e.target.value })
            this.setState({ cod_usuario: 'ADMPRIME' })
        }

        if (e.target.name === 'fecha') {
            this.setState({ fecha_tarea: e.target.value.split('-').join('') })
            this.setState({ fecha_busqueda: e.target.value.split('-').join('') })
            this.setState({ cod_usuario: 'ADMPRIME' })

            const fecha = e.target.value
            this.setState({ txtFecha: fecha })
        }

        if (e.target.name === 'ejercicio') {
            this.setState({ ejercicio_tarea: e.target.value })
        }

        if (e.target.name === 'periodo') {
            this.setState({ periodo_tarea: e.target.value })
        }

        if (e.target.name === 'tipo') {
            this.setState({ tipo_tarea: e.target.value })
        }
    }


    render() {
        const { eventOnclick, nameOpe } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-3 col-form-label"><input type="text" name="codigo" onChange={this.handleChange} value={this.state.id_tarea} /></div>
                            <div className="col-sm-3 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Fecha : </div>
                            <div className="col-sm-3 col-form-label"><input type="date" name="fecha" autoFocus onChange={this.handleChange} value={this.state.txtFecha} /></div>
                            <div className="col-sm-3 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Ejercicio : </div>
                            <div className="col-sm-3 col-form-label"><input name="ejercicio" onChange={this.handleChange} value={this.state.ejercicio_tarea} /></div>
                            <div className="col-sm-3 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Periodo : </div>
                            <div className="col-sm-3 col-form-label"><input name="periodo" onChange={this.handleChange} value={this.state.periodo_tarea} /></div>
                            <div className="col-sm-3 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Tipo : </div>
                            <div className="col-sm-3 col-form-label">
                                <select name="tipo" onChange={this.handleChange} value={this.state.tipo_tarea}>
                                    <option value={'P'} >Proyectado</option>
                                    <option value={'A'} >Ajuste</option>
                                </select>
                            </div>
                            <div className="col-sm-3 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Nota : </div>
                            <div className="col-sm-3 col-form-label">
                                La tarea programada se ejecutara a las 23:00 Horas del día programado
                            </div>
                            <div className="col-sm-3 col-form-label"></div>
                        </div>
                    </div>
                </div>

                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "id_tarea": this.state.id_tarea, "fecha_tarea": this.state.fecha_tarea, "fecha_busqueda": this.state.fecha_tarea, "cod_usuario": 'ADMPRIME', "ejercicio_tarea" : this.state.ejercicio_tarea, "periodo_tarea" : this.state.periodo_tarea, "tipo_tarea" : this.state.tipo_tarea })} texto={nameOpe} />
            </div>
        )
    }
}

export default TareaprogramadaUsuarioEdicion;