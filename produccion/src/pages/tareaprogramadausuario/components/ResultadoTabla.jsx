import React from "react"; 
import {GrEdit} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";
 
 
const ResultadoTabla = ({id_tarea, 
                        ejercicio_tarea,
                        periodo_tarea,
                        fecha_tarea, 
                        cod_usuario, 
                        tipo_tarea,
                        eventoEditar, 
                        eventoEliminar}) => 
    ( 
        <tbody> 
            <tr> 
                <td className="td-cadena" id={id_tarea}>{ejercicio_tarea}</td>
                <td className="td-cadena" >{periodo_tarea}</td>
                <td className="td-fecha"> { fecha_tarea.substring(0,10)}</td><td>{tipo_tarea}</td><td>{cod_usuario}</td>                 
                <td><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                 
            </tr> 
        </tbody> 
    ) 
 
export default ResultadoTabla; 