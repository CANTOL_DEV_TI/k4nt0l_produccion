import React, { useState, useEffect } from 'react'
import { get_articulo_plano_historial, insert_articulo_plano_file, update_articulo_plano_estado, delete_articulo_plano } from '../../../services/produccion'
import { BotonExcel, BotonNuevo } from "../../../components/Botones"
import { useVentanaModal } from "../../../hooks/useVentanaModal";
import { Plano_histo_tabla } from "./Plano_histo_tabla"
import { Plano_crud } from "./Plano_crud"
import VentanaModal from "../../../components/VentanaModal";
import { generate_new_version } from '../../../services/produccion';

export function Plano_histo({ datos,usuario }) {
    const [dataToEdit, setDataToEdit] = useState(null);
    const [refrescar, setRefrescar] = useState(false);
    // CONSUMIR DATOS
    const [listDatos_h, setListDatos_h] = useState([])
    useEffect(() => {
        getData_histo();
        setRefrescar(false);
    }, [refrescar])

    // cargar datos desde el servicio
    const getData_histo = () => {
        get_articulo_plano_historial(datos.COD_ARTICULO,datos.tipo)
            .then(res => {
                setListDatos_h(res);
                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_articulo", "cod_plano", "glosa"];
    const search = (listDatos_h) => {
        if (buscarDato.length === 0)
            return listDatos_h.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA
        const filtered = listDatos_h.filter((item) =>
            idBusqueda.some((key) => String(item[key]).toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // INSERTAR DATOS
    const createData = async (data) => {
        console.log("createData")
        console.log(data)
        // generar nuevo id version
        let version = await generate_new_version(data.tipo,data.cod_articulo);
        const new_id = data.cod_articulo.concat('_',data.tipo,'_', (version).toString(), ".pdf");                
        //data["id_version"] = data.id_version + 1;
        data["id_version"] = version;
        data["file_name"] = new_id;
        data['usuario'] = usuario;
        //console.log(data);
        insert_articulo_plano_file(data);
        setRefrescar(true);
        handleClose(true);
    };

    // ACTUALIZAR DATOS
    const updateData_h = (data) => {
        update_articulo_plano_estado(data);
        setRefrescar(true);
        handleClose(true);
    };

    // ELIMINAR DATOS
    const deleteData = (id) => {
        delete_articulo_plano(id)
        setRefrescar(true);
    };


    // PAGINACIÓN 
    let LimitPag = 10;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos_h.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // MOSTRAR MODAL
    const [opcionCrud, setOpcionCrud] = useState(null);
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = (opc = "N", c_datos) => {
        console.log(c_datos)
        // N: NUEVO
        // E: EDITAR
        // C: CAMBIO ESTADO
        setOpcionCrud(opc);
        if (opc === "E") {
            if (c_datos.length === 0) {
                // cuando no tiene historial de planos
                const new_articulo = {
                    "id_art_plano": 0,
                    "cod_articulo": datos.COD_ARTICULO,
                    "articulo": datos.ARTICULO,
                    "id_version": 0,
                    "cod_plano": "",
                    "glosa": "",
                    "cod_usuario": usuario,
                    "file_plano": null,
                    "file_name": "",
                    "tipo" : "PT"
                };
                console.log(new_articulo)
                setDataToEdit(new_articulo);
            } else {
                let datos_one = c_datos[0];
                console.log(c_datos)
                console.log("N")
                console.log(c_datos[0])
                //datos_one["id_art_plano"] = 0;
                // creacion de variables
                datos_one["file_plano"] = null;
                datos_one["file_name"] = "";
                datos_one['tipo'] = "";
                datos_one['usuario'] = usuario
                console.log(datos_one)
                setDataToEdit(datos_one);
            }
        } else {
            setDataToEdit(c_datos);
        }
        handleShow(true)
    };

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">

            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-2">
                        {/*<div className="col-sm">
                            <Title>Total Gasto Real: S/. {totalReal.toLocaleString("en")}</Title>
                        </div>*/}
                        <div className="col-xxl-4 col-sm-4 p-1">
                            <input type="search" className="form-control" style={{ maxWidth: '210px', minWidth: '100px' }} placeholder="Buscar" onChange={onSearchChange} />
                        </div>
                        <div className="col-xxl-4 col-sm-3 p-1">
                            <BotonExcel textoBoton={"Exportar Excel"} sw_habilitado={listDatos_h.length > 0 ? true : false} listDatos={listDatos_h} nombreArchivo={"Plano Historial"} />
                        </div>
                        <div className="col-xxl-4 col-sm-2 p-1">
                            <BotonNuevo textoBoton={"Añadir Plano"} sw_habilitado={true} eventoClick={() => mostrarVentana("E", listDatos_h)} />
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                {/* INICIO TABLA */}
                <Plano_histo_tabla datos_fila={search(listDatos_h)} deleteData={deleteData} eventoClick={mostrarVentana} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-end m-2">
                    <div className="pagination-wrap hstack gap-1">
                        Total Reg. {listDatos_h.length}
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                        <button className={listDatos_h.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>

            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="xsm"
                handleClose={handleClose}
                titulo={opcionCrud === "C" ? "Cambiar Estado de Plano" : "Añadir Plano"}
            >
                <Plano_crud
                    createData={createData}
                    updateData={updateData_h}
                    dataToEdit={dataToEdit}
                    setDataToEdit={setDataToEdit}
                    opcionCrud={opcionCrud}
                />
            </VentanaModal>
            {/* FIN MODAL */}

        </div>
    );
}