import React, { useState, useEffect } from 'react'
import { BsPencilFill, BsXCircleFill, BsFillFileEarmarkPdfFill } from "react-icons/bs";
import { get_articulo_plano_file } from '../../../services/produccion';

export function Plano_histo_tabla({ datos_fila, deleteData, eventoClick }) {

    // ACTUALIZAR DATOS
    const [urlFilePlano, setUrlFilePlano] = useState("");
    useEffect(() => {
        let c_urlPlano = get_articulo_plano_file()
        c_urlPlano
            .then((val) => {
                setUrlFilePlano(val);
            })
    }, []);

    const get_file_plano = (c_cod_articulo, c_tipo, c_id_version) => {
        
        let nombre_plano = urlFilePlano + c_cod_articulo + "_" + c_tipo + "_" + c_id_version + ".pdf";        
        window.open(nombre_plano, '_blank');
        
    };

    // RETORNAR INFORMACIÓN
    return (
        < div className="table-responsive">
            {/* INICIO TABLA */}
            < table className="table table-hover table-sm table-bordered" >
                <thead className="table-secondary text-center table-sm">
                    <tr>
                        <td className="align-middle">ID</td>
                        <td className="align-middle">Cod.Artículo</td>
                        <td className="align-middle">Cod.Plano</td>
                        <td className="align-middle">Versión</td>
                        <td className="align-middle" style={{ minWidth: '200px' }}>Glosa</td>
                        <th className="align-middle" style={{ width: '100px' }}>Estado Plano</th>
                        <th className="align-middle" title="Opciones" style={{ width: '120px' }}>Opc</th>
                    </tr>
                </thead>
                <tbody className="list">
                    {datos_fila.length > 0 ?
                        (
                            datos_fila.map((datos, index) => {
                                return <tr key={index + 1}>
                                    <td className="td-cadena">{datos.id_art_plano}</td>
                                    <td className="td-cadena">{datos.cod_articulo}</td>
                                    <td className="td-cadena">{datos.cod_plano}</td>
                                    <td className="td-cadena">{datos.id_version}</td>
                                    <td className="td-cadena">{datos.glosa}</td>
                                    <td className="td-cadena">
                                        <span className={`text-uppercase${datos.sw_est_plano === 'P' ? ' badge bg-warning text-dark' : datos.sw_est_plano === 'O' ? ' badge bg-success' : datos.sw_est_plano === 'R' ? ' badge bg-danger' : ' badge bg-dark'}`}>{datos.sw_est_plano === 'P' ? 'preliminar' : datos.sw_est_plano === 'O' ? 'oficial' : datos.sw_est_plano === 'R' ? 'error' : 'alterno'}</span>
                                    </td>
                                    <td>
                                        <button data-toggle="tooltip" title="Ver Plano" className="btn btn-outline-dark btn-sm ms-1"
                                            onClick={() => get_file_plano(datos.cod_articulo, datos.tipo, datos.id_version)}>
                                            <BsFillFileEarmarkPdfFill />
                                        </button>
                                        <button data-toggle="tooltip" title="Modificar Estado" className="btn btn-outline-primary btn-sm ms-1"
                                            onClick={() => eventoClick("C", datos)}>
                                            <BsPencilFill />
                                        </button>
                                        <button data-toggle="tooltip" title="Eliminar Plano" className="btn btn-outline-danger btn-sm ms-1"
                                            onClick={() => deleteData(datos.id_art_plano)}>
                                            <BsXCircleFill />
                                        </button>
                                    </td>
                                </tr>
                            })
                        ) :
                        (
                            <tr>
                                <td className="text-center" colSpan="7">Sin datos</td>
                            </tr>
                        )
                    }
                </tbody>
            </table >
            {/* FIN TABLA */}
        </div >
    );
}
