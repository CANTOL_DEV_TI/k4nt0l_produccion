import React, { useState, useEffect } from "react";
import { BsSearch } from "react-icons/bs";
import { get_articulo_plano_codigo_nuevo } from '../../../services/produccion';
import ComboBox from "../../../components/ComboBox";

const iniFormulario = {
    id_art_plano: 0,
    cod_articulo: "",
    articulo: "",
    id_version: 0,
    cod_plano: "",
    glosa: "",
    sw_est_plano: "",
    sw_estado: "",
    cod_usuario: "1",
    file_plano: null,
    tipo: "PT"
};

export function Plano_crud({ createData, updateData, dataToEdit, setDataToEdit, opcionCrud }) {

    const [formulario, setFormulario] = useState(iniFormulario);

    useEffect(() => {
        if (dataToEdit) {
            setFormulario(dataToEdit);
        } else {
            setFormulario(iniFormulario);
        }
    }, [dataToEdit]);

    // CAPTURAR EL EVENTO DE LOS IMPUTS
    const handleChange = (evento) => {
        if (evento.target.name === 'file_plano') {
            setFormulario({
                ...formulario,
                [evento.target.name]: evento.target.files[0],
            });
        }
        else {
            //console.log(evento.target.name)
            //console.log(evento.target.value)
            setFormulario({
                ...formulario,
                [evento.target.name]: (evento.target.value).toUpperCase(),
            });
            //console.log(formulario)
        }
    };

    // CARGAR MES
    const listEstadoPlano = [
        { "codigo": "O", "nombre": "OFICIAL" },
        { "codigo": "P", "nombre": "PRELIMINAR" },
        { "codigo": "A", "nombre": "ALTERNO" },
        { "codigo": "R", "nombre": "ERROR" }
    ]

    // obtener nuevo codigo plano
    const getBtnNuevoPlano = () => {        
        get_articulo_plano_codigo_nuevo(formulario.tipo, formulario.cod_articulo)
            .then(res => {
                setFormulario({
                    ...formulario,
                    cod_plano: res[0].nuevo_codigo,
                });
            })
        console.log(formulario)
    };

    // GUARDAR INFORMACION
    const handleSubmit = (e) => {
        console.log(e)
        if (opcionCrud === "E") {
            if (formulario.cod_plano.length < 10) {
                alert("Problemas en generar código de plano");
                return;
            }
            if (formulario.file_plano === null) {
                alert("Seleccionar Archivo");
                return;
            }
            // validar extensión de archivo
            if ((formulario.file_plano["name"].split('.').pop()).toLowerCase() != "pdf") {
                alert("Seleccionar archivo (*.pdf)");
                return;
            };
            createData(formulario);
        } else {
            if (!formulario.id_art_plano) {
                alert("Datos incompletos");
                return;
            }
            if (formulario.sw_est_plano === "0" || formulario.sw_est_plano.length === 0) {
                alert("Seleccionar un estado valido");
                return;
            }
            // QUITAR ELEMENTOS
            /*delete formulario.cod_articulo;
            delete formulario.articulo;
            delete formulario.id_version;
            delete formulario.cod_plano;
            delete formulario.glosa;
            delete formulario.archivo;
            delete formulario.sw_estado;*/
            updateData(formulario);
        }

        handleReset();
    };

    // RESTABLECER VALORES
    const handleReset = (e) => {
        setFormulario(iniFormulario);
        setDataToEdit(null);
    };

    // RETORNAR INFORMACIÓN
    if (opcionCrud === "C") {
        return (
            <>
                {/* INICIO  FORM*/}
                <div className="mb-3">
                    <div className="row">
                        <div className="col">
                            <label className="form-label ">Código Artículo</label>
                            <div className="col-sm">
                                <input type="text" disabled readOnly className="form-control" name="cod_articulo" value={formulario.cod_articulo} />
                            </div>
                        </div>
                        <div className="col">
                            <label className="form-label ">Id Plano</label>
                            <div className="col-sm">
                                <input type="text" disabled readOnly className="form-control" name="id_art_plano" value={formulario.id_art_plano} />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="mb-3">
                    <label className="form-label">Código Plano</label>
                    <div className="col-sm-10">
                        <input type="text" disabled readOnly className="form-control" name="cod_plano" onChange={handleChange} value={formulario.cod_plano} />
                    </div>
                </div>
                <div className="mb-3">
                    <label className="form-label">Cambio Estado de Plano</label>
                    <div className="col-sm-10">
                        <ComboBox
                            datosRow={listEstadoPlano}
                            nombre_cbo="sw_est_plano"
                            manejaEvento={handleChange}
                            valor_ini="Estado de Plano"
                            valor={formulario.sw_est_plano}
                        />
                    </div>
                </div>
                <div className="modal-footer">
                    <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Actualizar</button>
                </div>
                {/* FIN FORM */}
            </>
        )
    } else {
        return (
            <>
                {/* INICIO  FORM */}

                <div className="mb-3">
                    <label className="form-label ">Código Artículo</label>
                    <div className="col-sm-12">
                        <input type="text" disabled readOnly className="form-control" name="cod_articulo" value={formulario.cod_articulo} />
                    </div>
                </div>
                <div className="mb-3">
                    <label className="form-label ">Tipo</label>
                    <div className="col-sm-12">
                        <select className="form-control" name="tipo" onChange={handleChange} value={formulario.tipo}>
                            <option value={"PT"}>Plano</option>
                            <option value={"ES"}>Especificación Tecnica</option>
                            <option value={"MA"}>Manual</option>
                        </select>
                    </div>
                </div>
                <div className="mb-3">
                    <label className="form-label">Código Plano</label>
                    <div className="col-sm-12">
                        <div className="input-group">
                            <input type="text" className="form-control" name="cod_plano" onChange={handleChange} value={formulario.cod_plano} title="PT: Plano Tecnico &#13;PP: Abreviatura de Grupo &#13;[-]: Guión &#13;ZZ: Abreviatura de Area &#13;001: Correlativo" />
                            <button className="btn btn-warning" onClick={getBtnNuevoPlano} ><BsSearch /> Generar Código</button>
                        </div>
                    </div>
                </div>
                <div className="mb-3">
                    <label className="form-label">Cargar Documento (*.pdf)</label>
                    <div className="col-sm-12">
                        <input type="file" className="form-control" name="file_plano" accept="application/pdf" onChange={handleChange} />
                    </div>
                </div>
                <div className="mb-3">
                    <label className="form-label">Comentarios</label>
                    <div className="col-sm-12">
                        <input type="text" className="form-control" name="glosa" onChange={handleChange} value={formulario.glosa} />
                    </div>
                </div>
                <div className="modal-footer">
                    <button type="submit" className="btn btn-primary" onClick={handleSubmit}>{`${opcionCrud === "E" ? 'Crear' : 'Actualizar'}`}</button>
                </div>
                {/* FIN FORM */}
            </>
        )
    };
}