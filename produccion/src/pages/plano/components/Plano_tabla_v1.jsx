import React, { useState, useEffect } from 'react'
import { BsPlusCircleFill, BsSearch, BsFillFileEarmarkPdfFill } from "react-icons/bs";
import { get_articulo_plano_file } from '../../../services/produccion';

export function Plano_tabla({ datos_fila, setDataToEdit, deleteData, eventoClick }) {

    // ACTUALIZAR DATOS
    const [urlFilePlano, setUrlFilePlano] = useState("");
    useEffect(() => {
        let c_urlPlano = get_articulo_plano_file()
        c_urlPlano
            .then((val) => {
                setUrlFilePlano(val);
            })
    }, [])

    const get_file_plano = (c_cod_articulo, c_id_version) => {
        let nombre_plano = urlFilePlano + c_cod_articulo + "_" + c_id_version + ".pdf";
        window.open(nombre_plano, '_blank');
    };

    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle">N°</th>
                            <th className="align-middle" title="Opciones" style={{ width: '86px' }}>Opc</th>
                            <th className="align-middle default">Cod.Artículo</th>
                            <th className="align-middle">Artículo</th>
                            <th className="align-middle">Area Responsable</th>
                            <th className="align-middle">Grupo</th>
                            <th className="align-middle">Cod.Plano</th>
                            <th className="align-middle">Versión</th>
                            <th className="align-middle" style={{ width: '100px' }}>Est.Plano</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td className="td-cadena">{datos.ROW_NUM}</td>
                                        <td>
                                            <button data-toggle="tooltip" title="Ver Plano" className="btn btn-outline-dark btn-sm ms-1"
                                                onClick={() => get_file_plano(datos.COD_ARTICULO, datos.id_version)}>
                                                <BsFillFileEarmarkPdfFill />
                                            </button>
                                            {/*<button data-toggle="tooltip" title="Añadir Plano" className="btn btn-outline-primary btn-sm ms-1"
                                                onClick={() => eventoClick("E", datos)}>
                                                <BsPlusCircleFill />
                                            </button>*/}
                                            <button data-toggle="tooltip" title="Ver Historial" className="btn btn-outline-info btn-sm ms-1"
                                                onClick={() => eventoClick("E", datos)}>
                                                <BsSearch />
                                            </button>
                                        </td>
                                        <td className="td-cadena">{datos.COD_ARTICULO}</td>
                                        <td className="td-cadena">{datos.ARTICULO}</td>
                                        <td className="td-cadena">{datos.AREA}</td>
                                        <td className="td-cadena">{datos.GRUPO}</td>
                                        <td className="td-cadena">{datos.cod_plano}</td>
                                        <td className="td-cadena">{datos.id_version}</td>
                                        <td className="td-cadena">
                                            <span className={`text-uppercase${datos.sw_est_plano === 'P' ? ' badge bg-warning text-dark' : datos.sw_est_plano === 'O' ? ' badge bg-success' : ''}`}>{datos.sw_est_plano === 'P' ? 'preliminar' : datos.sw_est_plano === 'O' ? 'oficial' : ''}</span>
                                        </td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="7">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}