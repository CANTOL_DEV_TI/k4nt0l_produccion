import React, { useState, useEffect } from 'react'
import {
    BsCheckLg,
    BsExclamationLg,
    BsQuestionLg,
} from "react-icons/bs";
import { get_area_responsable_sap, get_articulo_plano_sap } from '../../services/produccion';
import { get_grupo_sap } from '../../services/articuloGrupoService';
import { TarjetaTotal } from '../../components/TarjetaTotal';
import { BotonExcel } from "../../components/Botones";
import { InputTextBuscarV2 } from "../../components/InputTextBuscar";
import { useVentanaModal } from "../../hooks/useVentanaModal";
import { Plano_tabla } from "./components/Plano_tabla";
import { Plano_histo } from "./components/Plano_histo";
import { ComboBoxForm } from "../../components/ComboBox";
import VentanaModal from "../../components/VentanaModal";
//import { validaToken, validaAcceso } from "../../services/usuarioServices";
import { Validar } from '../../services/ValidaSesion.jsx';
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

const listFormulado = [
    { "codigo": "1", "nombre": "FORMULADO" },
    { "codigo": "2", "nombre": "NO FORMULADO" },
]

export function Plano() {
    // seguridad
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
    const [Usuario,setUsuario] = useState("");

    const Seguridad = async () => {        
        const ValSeguridad = await Validar("PROPLA")
        console.log(ValSeguridad)
		setVentanaSeguridad(ValSeguridad.Validar)
        setUsuario(ValSeguridad.usuario)
    }

    // CRUD ID
    const [dataToEdit, setDataToEdit] = useState(null);
    // BUSQUEDA GENERALES    
    const [formulado, setFormulado] = useState("1");
    const [grupo, setGrupo] = useState("0");
    const [area, setArea] = useState("0");
    function datosBusqueda(evento) {
        if (evento.target.name === 'cbo_area') {
            setArea(evento.target.value)
        }
        if (evento.target.name === 'cbo_formulado') {
            setFormulado(evento.target.value)
        }
        if (evento.target.name === 'cbo_grupo') {
            setGrupo(evento.target.value)
        }
    };

    // cargar combos area y grupo
    const [listArea, setListArea] = useState([]);
    const [listGrupo, setListGrupo] = useState([]);
    useEffect(() => {
        Seguridad();
        get_area_responsable_sap()
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.COD_AREA, "nombre": data.AREA
                    }
                })
                setListArea(cbo_data)
            });
        get_grupo_sap()
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.id_grupo, "nombre": data.grupo
                    }
                })
                setListGrupo(cbo_data)
            });
    }, [])

    // CONSUMIR DATOS
    const [totalOf, setTotalOf] = useState(0);
    const [totalPr, setTotalPr] = useState(0);
    const [totalSp, setTotalSp] = useState(0);
    const [refrescar, setRefrescar] = useState(true)
    const [listDatos, setListDatos] = useState([])
    useEffect(() => {
        setRefrescar(false);
        getData();
    }, [refrescar, formulado, grupo, area])

    //    
    const getData = () => {
        get_articulo_plano_sap(formulado, grupo, area)
            .then(res => {
                setListDatos(res)
                
                setTotalOf(0);
                setTotalPr(0);
                setTotalSp(0);
                if (res.length == 0) {
                    return;
                }

                // calculando el tablaro
                let c_tt = res.reduce((total, currItem) => total + 1, 0);
                let c_of = res.filter(xx => xx.sw_est_plano === "O").reduce((total, currItem) => total + 1, 0);
                let c_pr = res.filter(xx => xx.sw_est_plano === "P").reduce((total, currItem) => total + 1, 0);
                if (c_tt <= 0) { c_tt = 1 }

                setTotalOf(((c_of / c_tt) * 100).toFixed(2))
                setTotalPr(((c_pr / c_tt) * 100).toFixed(2))
                setTotalSp((c_tt - c_of - c_pr).toFixed(0))

                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["COD_ARTICULO", "ARTICULO", "AREA", "GRUPO", "cod_plano"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // ACTUALIZAR DATOS
    const deleteData = (id) => {
        delete_Plano(id)
        setRefrescar(true);
        getData();
    };

    // PAGINACIÓN 
    let LimitPag = 20;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // MOSTRAR MODAL
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = (opc = "N", datos) => {
        if (opc === "E") {
            setDataToEdit(datos);
        } else {
            setDataToEdit(null);
        }
        handleShow(true)
    };

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">

            {/* INICIO TARJETAS */}
            <div className="row align-items-center mb-2 g-3">
                <TarjetaTotal
                    str_concepto={"% Plano Oficial"}
                    obj_imagen={<BsCheckLg />}
                    str_monto={totalOf.toLocaleString("en")}
                />
                <TarjetaTotal
                    str_concepto={"% Plano Preliminar"}
                    obj_imagen={<BsExclamationLg />}
                    str_monto={totalPr.toLocaleString("en")}
                />
                <TarjetaTotal
                    str_concepto={"Artículos sin Plano"}
                    obj_imagen={<BsQuestionLg />}
                    str_monto={totalSp.toLocaleString("en")}
                />
            </div>
            {/* FIN TARJETAS */}

            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border-0 card-header">
                    <div className="align-items-center gy-3 row">
                        <div className="col-sm">
                            <h4 className="card-title mb-0">Lista de Planos</h4>
                        </div>
                        <div className="col-sm-auto">
                            <div className="d-flex gap-1 flex-wrap">
                                <BotonExcel textoBoton={"Excel"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatos} nombreArchivo={"Presupesto mensual"} />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="p-0 card-body">
                    <div className="mx-1 my-2 row">
                        <div className="col-sm-6 p-1">
                            <InputTextBuscarV2 onBuscarChange={onSearchChange} />
                        </div>
                        <div className="col-xxl-2 col-sm-6 p-1">
                            <ComboBoxForm
                                datosRow={listArea}
                                nombre_cbo="cbo_area"
                                manejaEvento={datosBusqueda}
                                valor_ini="Area Responsable"
                                valor={area}
                            />
                        </div>
                        <div className="col-xxl-2 col-sm-6 p-1">
                            <ComboBoxForm
                                datosRow={listGrupo}
                                nombre_cbo="cbo_grupo"
                                manejaEvento={datosBusqueda}
                                valor_ini="Grupo Artículo"
                                valor={grupo}
                            />
                        </div>
                        <div className="col-xxl-2 col-sm-6 p-1">
                            <ComboBoxForm
                                datosRow={listFormulado}
                                nombre_cbo="cbo_formulado"
                                manejaEvento={datosBusqueda}
                                valor_ini="Tipo Formulado"
                                valor={formulado}
                            />
                        </div>
                    </div>

                    {/* FIN BARRA DE NAVEGACION */}

                    { /* INICIO TABLA */}
                    <Plano_tabla datos_fila={search(listDatos)} setDataToEdit={setDataToEdit} deleteData={deleteData} eventoClick={mostrarVentana} />

                    {/* FIN TABLA */}

                    {/* INICIO PAGINACION */}
                    <div className="d-flex justify-content-end m-2">
                        <div className="pagination-wrap hstack gap-1">
                            Total Reg. {listDatos.length}
                            <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                            <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                        </div>
                    </div>
                    {/* FIN PAGINACION */}
                </div>
            </div>

            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="lg"
                handleClose={handleClose}
                titulo="Planos histórico de Artículo"
            >
                <Plano_histo
                    datos={dataToEdit}
                    usuario={Usuario}
                />
            </VentanaModal>
            {/* FIN MODAL */}

            {/* seguridad */}
            <VentanaBloqueo show={VentanaSeguridad} />

        </div >
    );
}