import React, {useState} from "react";


class Busqueda extends React.Component{

    constructor(props) {
        super(props);

        this.state = {txtInput: ''}
    }

    handleChange = (e) =>{
        this.setState({txtInput: e.target.value})
    }


    render() {

        const {handleBusqueda, handleModal} = this.props

        return(
            <div className="busqueda-container">
                <input
                    className="busqueda-input"
                    placeholder="Ingrese Busqueda"
                    onChange={this.handleChange}
                    value={this.state.txtInput}
                />

                <button
                    className="busqueda-btn"
                    onClick={()=> handleBusqueda(this.state.txtInput)}
                >
                 Buscar
                </button>

                <button
                    className="nuevo-btn"
                    onClick={()=> handleModal(true)}>Crear Nuevo</button>

            </div>
        )
    }

}

export default Busqueda;