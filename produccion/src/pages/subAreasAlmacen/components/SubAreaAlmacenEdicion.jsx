import React from "react";


class SubAreaAlmacenEdicion extends React.Component{

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null){
            this.state = {cod_subarea_almacen: 0, codAlmacen: 0, codSubArea: 0, nom_subarea_almacen: '', nomAlmacen: '', nomSubArea:''}
        }else {
            this.state = {
                cod_subarea_almacen: this.props.dataToEdit.cod_subarea_almacen,
                codAlmacen: this.props.dataToEdit.cod_almacen,
                codSubArea: this.props.dataToEdit.cod_subarea,
                nom_subarea_almacen: this.props.dataToEdit.nom_subarea_almacen,
                nomAlmacen: this.props.dataToEdit.nomAlmacen,
                nomSubArea: this.props.dataToEdit.nomSubArea,
            }
        }
    }

    handleChange = (e) =>{
        if (e.target.name === 'codigo'){
            this.setState({cod_subarea_almacen: e.target.value})
        }


        if (e.target.name === 'selectAlmacen'){
            this.setState({codAlmacen: e.target.value})

            let indexAlmacen = e.target.selectedIndex
            this.setState({nomAlmacen: e.target.options[indexAlmacen].text})

            this.setState({nom_subarea_almacen: e.target.value + "-" + this.state.nomSubArea })

        }



        if (e.target.name === 'selectSubArea'){
            this.setState({codSubArea: e.target.value})

            let indexSubArea = e.target.selectedIndex
            this.setState({nomSubArea: e.target.options[indexSubArea].text})

            this.setState({nom_subarea_almacen: this.state.codAlmacen + "-" + e.target.options[indexSubArea].text })

        }

    }


    render() {
        const {eventOnclick, nameOpe, listSubAreas, listAlmacenes} = this.props

        return(
            <table className="formModal">
                <tr>
                    <td className="tdLabel">Almacen</td>
                    <td className="tdSeparador">:</td>
                    <td className="tdvalue">
                        <select name="selectAlmacen" onChange={this.handleChange}>
                            <option value='0'>Seleccionar Almacen</option>
                            {
                                listAlmacenes.map((v_item) =>
                                    this.state.codAlmacen===v_item.cod_almacen ?
                                        <option selected value={v_item.cod_almacen}>{`${v_item.cod_almacen+"-"+v_item.nom_almacen}`}</option>
                                    :
                                        <option value={v_item.cod_almacen}>{`${v_item.cod_almacen+"-"+v_item.nom_almacen}`}</option>
                                )
                            }
                        </select>
                    </td>

                </tr>

                <tr>
                    <td className="tdLabel">SubArea</td>
                    <td className="tdSeparador">:</td>
                    <td className="tdvalue">
                        <select name="selectSubArea" onChange={this.handleChange}>
                            <option value='0'>Seleccionar Sub-Area</option>
                            {
                                listSubAreas.map((v_item) =>
                                    this.state.codSubArea===v_item.cod_subarea ?
                                        <option selected value={v_item.cod_subarea}>{`${v_item.nom_subarea}`}</option>
                                    :
                                        <option value={v_item.cod_subarea}>{`${v_item.nom_subarea}`}</option>
                                )
                            }
                        </select>
                    </td>

                </tr>


                <tr>
                    <td className="tdLabel">Codigo</td>
                    <td className="tdSeparador">:</td>
                    <td className="tdvalue"><input type="text" name="codigo" onChange={this.handleChange} value={this.state.cod_subarea_almacen}/></td>
                </tr>


                <tr>
                    <td className="tdLabel">nombre</td>
                    <td className="tdSeparador">:</td>
                    <td className="tdvalue"><input type="text" name="nombre" onChange={this.handleChange} value={this.state.nom_subarea_almacen}/></td>
                </tr>

                <tr>
                    <td colSpan="3" className="tdOperaciones"><button onClick={() => eventOnclick({"cod_subarea_almacen": this.state.cod_subarea_almacen, "cod_almacen": this.state.codAlmacen, "cod_subarea": this.state.codSubArea, "nom_subarea_almacen": this.state.nom_subarea_almacen})}>{nameOpe}</button></td>
                </tr>



            </table>
        )
    }
}

export default SubAreaAlmacenEdicion;
