import React from "react";
import {GrEdit} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({key,
                        nombreSubAreaAlmacen,
                        codAlmacen,
                        codSubArea,
                        eventoEditar,
                        eventoEliminar}) =>
    (
        <tbody>
            <tr className="data">
                <td className="colBtn"><button onClick={() => eventoEditar(true)}><GrEdit color="#011826"/></button></td>
                <td className="colBtn"><button onClick={() => eventoEliminar(true)}><AiFillDelete color="#b04129"/></button></td>
                <td className="colString" id={key}>{nombreSubAreaAlmacen}</td>
            </tr>
        </tbody>
    )


export default ResultadoTabla;


