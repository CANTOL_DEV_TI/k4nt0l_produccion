import React from "react";

import {getFilter_Almacen} from "../../services/almacenServices.jsx";
import {getFilter_SubArea} from "../../services/subAreaServices";
import {getFilter_SubAreaAlmacen, save_SubAreaAlmacen, update_SubAreaAlmacen, delete_SubAreaAlmacen} from "../../services/subAreaAlmacenServices";

import Busqueda from "./components/Busqueda";

import "../../assets/css/styleStandar.css"
import ResultadoTabla from "./components/ResultadoTabla"
import SubAreaAlmacenEdicion from "./components/SubAreaAlmacenEdicion"
import VentanaModal from "../../components/VentanaModal";



class SubAreaAlmacen extends React.Component{

    constructor(props) {
        super(props);

        this.state ={
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            listAlmacenes: null,
            listSubAreas: null
        }

    }


    handleBusqueda = async(txtFind) => {
        const responseJson = await getFilter_SubAreaAlmacen(txtFind)
        console.log(responseJson)
        this.setState({resultados: responseJson, isFetch: false})
        console.log(this.state)
    }


    handleSubmit = async (e) =>{
        console.log(e)


        if (this.state.tipoOpe === 'Grabar'){
            const responseJson = await save_SubAreaAlmacen(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar'){
            const responseJson = await update_SubAreaAlmacen(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar'){
            const responseJson = await delete_SubAreaAlmacen(e)
            console.log(responseJson)
        }

        this.setState({showModal: false, tipoOpe: 'Find'})
    }


    render() {

        const {isFetch, resultados, showModal} = this.state

        return(
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({showModal: true, dataRegistro:null, tipoOpe: 'Grabar'})}/>

                { isFetch && 'Cargando'}
                { (!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="resultado-container">
                    <table className="table-resultado_Stock">
                        <thead>
                            <tr className="cabecera">
                                <th className="meCabIni">Eli</th>
                                <th className="meCab2 meCabCentro">Edi</th>
                                <th className="meCabFin">Nombre SubArea-Almacen</th>
                            </tr>
                        </thead>
                        {resultados.map((registro)=>
                            <ResultadoTabla
                                key={registro.cod_subarea_almacen}
                                nombreSubAreaAlmacen={registro.nom_subarea_almacen}
                                codAlmacen={registro.cod_almacen}
                                codSubArea={registro.cod_subarea}
                                eventoEditar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar'})}
                                eventoEliminar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar'})}
                            />
                        )}
                    </table>
                </div>


                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({showModal: false})}
                    titulo="Ventana Edicion">
                        <SubAreaAlmacenEdicion
                            dataToEdit={this.state.dataRegistro}
                            eventOnclick={this.handleSubmit}
                            nameOpe={this.state.tipoOpe}
                            listSubAreas={this.state.listSubAreas}
                            listAlmacenes={this.state.listAlmacenes}
                        />
                </VentanaModal>


            </React.Fragment>
        )
    }

    async componentDidMount(){
        const responseJson = await getFilter_SubArea('')
        // console.log(responseJson)
        this.setState({listSubAreas: responseJson})


        const  responseJsonAlmacen = await getFilter_Almacen('')
        this.setState({listAlmacenes: responseJsonAlmacen})


    }


}

export default SubAreaAlmacen;

