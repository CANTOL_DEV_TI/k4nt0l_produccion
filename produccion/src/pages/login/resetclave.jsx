import React from "react";
import { ResetClave } from "../../services/usuarioServices";
import { BotonLogin,BotonBuscar } from "../../components/Botones";

class ResetClaveUsuario extends React.Component{
    constructor(props){
        super(props);
        this.state = {usuario : ''}
    }

    handleChange = async (e) => {
        this.setState({usuario : e.target.value})
    }

    render(){
        const {EnviarReset} = this.props
        return(
            <React.Fragment>
                <div className="container text-center mt-5">
                    <div className="row justify-content-center">
                        <div className="col-xl-4 col-lg-6">
                            <div className="card mt-4">
                                <div className="card-body p-4 bg-exclamation">
                                    <h1 className="h3 mb-3 fw-normal">Por favor ingrese su usuario o correo. para iniciar el reseteo de su contraseña.</h1>                                    
                                    <div className="form-floating">
                                        <input type="text" name="txtData" className="form-control" id="floatingInput" placeholder="Usuario o Correo..." autoFocus onChange={this.handleChange} value={this.state.usuario} />
                                        <label>Usuario</label>
                                    </div>                                    
                                    <div className="form-floating">
                                        <p />
                                    </div>
                                    <div className="form-floating">
                                        <BotonBuscar sw_habilitado={true} textoBoton={"Resetear Contraseña"} eventoClick={()=> EnviarReset(this.state.usuario)} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}
export default ResetClaveUsuario;