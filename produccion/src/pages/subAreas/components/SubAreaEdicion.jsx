import React from "react";
import { BotonGuardar, BotonConsultar } from "../../../components/Botones";

class SubAreaEdicion extends React.Component{

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null){
            this.state = {codSubArea: 0,codArea: 0, nomSubArea: ''}
        }else {
            this.state = {codSubArea: this.props.dataToEdit.cod_subarea, codArea: this.props.dataToEdit.cod_area, nomSubArea: this.props.dataToEdit.nom_subarea}
        }
    }

    handleChange = (e) =>{
        if (e.target.name === 'codigo'){
            this.setState({codSubArea: e.target.value})
        }

        if (e.target.name === 'nombre'){
            this.setState({nomSubArea: e.target.value})
        }

        if (e.target.name === 'selectArea'){
            this.setState({codArea: e.target.value})
        }

    }


    render() {
        const {eventOnclick, nameOpe, listAreas} = this.props

        return(
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-3 col-form-label">Area : </div>
                            <div className="col-sm-6 col-form-label">
                                <select name="selectArea"  className="form-control form-control-sm"  onChange={this.handleChange}>
                                    <option value='0'>Seleccionar Area</option>
                                    {
                                        listAreas.map((v_item) =>
                                            this.state.codArea===v_item.cod_area ?
                                                <option selected value={v_item.cod_area}>{`${v_item.nom_area}`}</option>
                                            :
                                                <option value={v_item.cod_area}>{`${v_item.nom_area}`}</option>
                                        )
                                    }
                                </select>
                            </div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-3 col-form-label">
                                <input type="text" className="form-control form-control-sm" name="codigo" onChange={this.handleChange} value={this.state.codSubArea}/>
                            </div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-3 col-form-label">Nombre : </div>
                            <div className="col-sm-8 col-form-label">
                            <input type="text" className="form-control form-control-sm" name="nombre" onChange={this.handleChange} value={this.state.nomSubArea}/>
                            </div>
                        
                    </div>
                </div>
            </div>
            <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({"cod_subarea": this.state.codSubArea, "nom_subarea": this.state.nomSubArea, "cod_area": this.state.codArea})} texto={nameOpe} />
            </div>

        )
    }
}

export default SubAreaEdicion;