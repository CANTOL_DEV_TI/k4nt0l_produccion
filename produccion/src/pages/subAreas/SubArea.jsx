import React from "react";

import {getFilter_SubArea, save_SubArea, update_SubArea, delete_SubArea} from "../../services/subAreaServices";
import {getFilter_Area} from "../../services/areaServices.jsx";

import Busqueda from "../subAreas/components/Busqueda";
import ResultadoTabla from "../subAreas/components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal";
import SubAreaEdicion from "../subAreas/components/SubAreaEdicion";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

import "../../assets/css/styleStandar.css"
class Area extends React.Component{
    constructor(props) {
        super(props);

        this.state ={
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            listAreas: null
        }
    }

    handleBusqueda = async(txtFind) => {
        const responseJson = await getFilter_SubArea(txtFind)
        console.log(responseJson)
        this.setState({resultados: responseJson, isFetch: false})
        console.log(this.state)
    }

    handleSubmit = async (e) =>{
        console.log(e)


        if (this.state.tipoOpe === 'Grabar'){
            const responseJson = await save_SubArea(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar'){
            const responseJson = await update_SubArea(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar'){
            const responseJson = await delete_SubArea(e)
            console.log(responseJson)
        }

        this.setState({showModal: false, tipoOpe: 'Find'})
    }

    render() {
        const {isFetch, resultados, showModal, VentanaSeguridad} = this.state

        return(
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({showModal: true, dataRegistro:null, tipoOpe: 'Grabar'})}/>


                { isFetch && 'Cargando'}
                { (!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>                        
                            <tr className="cabecera">
                                <th className="meCabFin">Nombre SubArea</th>
                                <th className="meCab2 meCabCentro">Editar</th>
                                <th className="meCabIni">Eliminar</th>                                                                
                            </tr>
                        </thead>
                        {resultados.map((registro)=>
                            <ResultadoTabla
                                key={registro.cod_subarea}
                                nombreSubArea={registro.nom_subarea}
                                codArea={registro.cod_area}
                                eventoEditar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar'})}
                                eventoEliminar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar'})}
                            />
                        )}
                    </table>
                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({showModal: false})}
                    titulo="Edicion Sub Areas">
                        <SubAreaEdicion
                            dataToEdit={this.state.dataRegistro}
                            eventOnclick={this.handleSubmit}
                            nameOpe={this.state.tipoOpe}
                            listAreas={this.state.listAreas}
                        />
                </VentanaModal>
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}

                />

            </React.Fragment>
        )

    }


    async componentDidMount(){        
        const ValSeguridad = await Validar("PROSUA")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })

        const ListaA = await getFilter_Area(" ")
        this.setState({listAreas : ListaA})

    }

}

export default Area;
