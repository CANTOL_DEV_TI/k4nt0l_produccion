import React from "react";

class AsientosExporta extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find'
        }
    }

    handleBusqueda = async (txtFind) => {
        const responseJson = await getFilter_Familia(txtFind)
        console.log(responseJson)
        this.setState({ resultados: responseJson, isFetch: false })
        console.log(this.state)
    }

    handleSubmit = async (e) => {
        console.log(e)

        if (this.state.tipoOpe === 'Grabar') {
            const responseJson = await save_Familia(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar') {
            const responseJson = await update_Familia(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar') {
            const responseJson = await delete_Familia(e)
            console.log(responseJson)
        }


        this.setState({ showModal: false, tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal } = this.state

        return (
            <React.Fragment>

                <div className="card cardalign w-50rd">
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="form-group">
                            <div className="form-group row">
                                <div className="col-sm-4 col-form-label">Empresa : </div>
                                <div className="col-sm-10 col-form-label">
                                    <select name="empresas">
                                        <option value={"01"}>TECNOPRESS</option>
                                        <option value={"02"}>DISTRIMAX</option>
                                        <option value={"03"}>CANTOL</option>
                                    </select>
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-3 col-form-label">Ejercicio : </div>
                                <div className="col-sm-4 col-form-label">

                                </div>
                                <div className="col-sm-3 col-form-label">Periodo : </div>
                                <div className="col-sm-4 col-form-label">

                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-4 col-form-label">Tipo : </div>
                                <div className="col-sm-10 col-form-label">

                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-14 col-form-label">
                                    <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "id_familia": this.state.id_familia, "nom_familia": this.state.nom_familia })} texto={nameOpe} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </React.Fragment>
        )

    }

}

export default AsientosExporta;