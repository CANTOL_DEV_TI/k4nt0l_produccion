import {Url} from '../../../constants/global.jsx'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url


const url = `${meServidorBackend}/produccion/oee_motivo_causa`;


export async function getFilter_MotivoCausa(cod_subarea, cod_causa, sw_estado) {
    // if (txtFind.trim()===''){
    //     txtFind='%20'
    // }

    cod_subarea=0
    // cod_causa=0
    sw_estado=0

    const vLink = `${url}/get/?cod_causa=${cod_causa}&sw_estado=${sw_estado}`
    console.log(vLink)
    const response = await fetch(vLink)
    const responseJson = await response.json()
    return responseJson
}

export async function save_MotivoCausa(meJson){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const vLink = `${url}/`
    const response = await fetch(vLink, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function update_MotivoCausa(meJson){
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const vLink = `${url}/`
    const response = await fetch(vLink, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function delete_MotivoCausa(meJson){
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const vLink = `${url}/`
    const response = await fetch(vLink, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


