import React from "react";
import { BotonGuardar, BotonConsultar } from "../../components/Botones";
import {getFilter_TipoRegistro} from "../oee_TipoRegistro/services/TipoRegistroServices.jsx";
import {getFilter_Area} from "../../services/areaServices.jsx";
import {get_subarea} from "../oee/services/oeeSubAreaServices.jsx";



class MotivoCausa_Edicion extends React.Component{

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null){
            this.state = {cod_motivo_causa: 0,cod_subarea: 0, motivo_causa: '', cod_causa: this.props.cod_causa}

        }else {
            this.state = {cod_motivo_causa: this.props.dataToEdit.cod_motivo_causa, cod_subarea: this.props.dataToEdit.cod_subarea, motivo_causa: this.props.dataToEdit.motivo_causa, cod_causa: this.props.cod_causa}
        }
    }




    handleChange = (e) =>{
        if (e.target.name === 'cod_motivo_causa'){
            this.setState({cod_motivo_causa: e.target.value})
        }

        if (e.target.name === 'motivo_causa'){
            this.setState({motivo_causa: e.target.value})
        }

        if (e.target.name === 'selectArea'){
            this.setState({cod_subarea: e.target.value})
        }

    }


    render() {
        const {eventOnclick, nameOpe, listAreas} = this.props

        return(
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-3 col-form-label">SubArea : </div>
                            <div className="col-sm-6 col-form-label">
                                <select name="selectArea"  className="form-control form-control-sm"  onChange={this.handleChange}>
                                    <option value='0'>Seleccionar SubArea</option>
                                    {
                                        listAreas.map((v_item) =>
                                            this.state.cod_subarea===v_item.codigo ?
                                                <option selected value={v_item.codigo}>{`${v_item.nombre}`}</option>
                                            :
                                                <option value={v_item.codigo}>{`${v_item.nombre}`}</option>
                                        )
                                    }
                                </select>
                            </div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-3 col-form-label">
                                <input type="text" className="form-control form-control-sm" name="cod_motivo_causa" onChange={this.handleChange} value={this.state.cod_motivo_causa}/>
                            </div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-3 col-form-label">Nombre : </div>
                            <div className="col-sm-8 col-form-label">
                            <input type="text" className="form-control form-control-sm" name="motivo_causa" onChange={this.handleChange} value={this.state.motivo_causa}/>
                            </div>

                    </div>
                </div>
            </div>
            <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({"cod_motivo_causa": this.state.cod_motivo_causa, "cod_subarea": parseInt(this.state.cod_subarea), "cod_causa": this.state.cod_causa, "motivo_causa": this.state.motivo_causa, "sw_estado": "1"})} texto={nameOpe} />
            </div>

        )
    }





}

export default MotivoCausa_Edicion;

