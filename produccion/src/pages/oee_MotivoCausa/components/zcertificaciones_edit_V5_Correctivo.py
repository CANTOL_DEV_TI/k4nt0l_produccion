# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
# from form.certificaciones_edit_V5 import Ui_certificaciones_edit_V5
from plugins_Certificados.verificacionCubicacion.form.certificaciones_edit_V5 import Ui_certificaciones_edit_V5

import main
from conexion import conexion


import os


class certificaciones_edit_V5_Correctivo(QtWidgets.QDialog):
    def __init__(self):
        super(certificaciones_edit_V5_Correctivo, self).__init__()
        self.ui = Ui_certificaciones_edit_V5()
        self.ui.setupUi(self)

        # *Decidir comportamiento hoja de calculo xxx
        # *Decidir comportamiento de certificado original xxx
        # *Decidir publicacion en Hosting xxx
        # *edicion total ? xxx



        self.ui.pushButton_6.clicked.connect(self.add_img_frente)
        self.ui.pushButton_7.clicked.connect(self.add_img_trasera)
        self.ui.comboBox_5.currentIndexChanged.connect(self.hallar_rubrica)  # responsable
        self.ui.comboBox_4.currentIndexChanged.connect(self.hallar_rubrica_GerenteTecnico)  # Gerente Tecnico



        self.ui.pushButton_3.clicked.connect(self.actualizar)

        set_swFirma_Lambda = lambda: self.generar_certificado(False)
        self.ui.pushButton_4.clicked.connect(set_swFirma_Lambda)
        self.ui.pushButton_5.clicked.connect(self.generar_tarjeta)


        self.ui.comboBox_8.currentIndexChanged.connect(self.llenar_vehiculos)
        self.ui.comboBox_8.currentIndexChanged.connect(self.hallar_data_cisterna)
        self.ui.comboBox_7.currentIndexChanged.connect(self.hallar_data_vehiculo)



        # self.ui.tableWidget_7.cellActivated.connect(self.Veri_TipoObject_Gri7)  # MET SC
        # self.ui.tableWidget_8.cellActivated.connect(self.Veri_TipoObject_Gri8)  # MET CC
        # self.ui.tableWidget_6.cellActivated.connect(self.Veri_TipoObject_Gri6)  # ensayos veri inicial y posteriro
        # self.ui.tableWidget_9.cellActivated.connect(self.Veri_TipoObject_Gri9)  # ensayos exclusivos veri inicial
        # self.ui.tableWidget_5.cellActivated.connect(self.Veri_TipoObject_Gri5)  # error maximo permisible
        # self.ui.tableWidget_10.cellActivated.connect(self.Veri_TipoObject_Gri10)  # ensayos exclusivos veri inicial



        self.ui.pushButton_14.clicked.connect(self.new_cisterna)
        self.ui.pushButton_13.clicked.connect(self.new_tracto)

        self.ui.pushButton_9.clicked.connect(self.new_personal)

        # self.ui.pushButton_12.clicked.connect(self.publicar_files)

        self.inicializar()


    def crear_CertificadoCorrectivo(self, cod_certificado):

        sql = "select count(cod_certificado) from certificados_correctivo where cod_certificado=%s" %(cod_certificado)
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)
        if int(cur.fetchall()[0][0]) > 0:
            return


        sql = " insert into certificados_correctivo "
        sql += " select * from certificados "
        sql += " where cod_certificado=%s; " %(cod_certificado)

        sql += " update certificados_correctivo "
        sql += " set registro_cubicacion_nro=(registro_cubicacion_nro||'C') "
        sql += " where cod_certificado=%s; " %(cod_certificado)

        sql += " insert into trazabilidad_correctivo "
        sql += " select * from trazabilidad "
        sql += " where cod_certificado=%s; " %(cod_certificado)

        sql += " insert into medidas_externas_tanque_correctivo "
        sql += " select * from medidas_externas_tanque "
        sql += " where cod_certificado=%s; " %(cod_certificado)

        sql += " insert into ensayos_exclusivos_verif_ini_correctivo "
        sql += " select * from ensayos_exclusivos_verif_ini "
        sql += " where cod_certificado=%s; " %(cod_certificado)

        sql += " insert into ensayos_veri_ini_posterior_correctivo "
        sql += " select * from ensayos_veri_ini_posterior "
        sql += " where cod_certificado=%s; " %(cod_certificado)

        sql += " insert into error_max_permisible_correctivo "
        sql += " select * from error_max_permisible "
        sql += " where cod_certificado=%s; " %(cod_certificado)

        sql += " insert into cubicacion_rpt_inacal_correctivo "
        sql += " select * from cubicacion_rpt_inacal "
        sql += " where cod_certificado=%s; " %(cod_certificado)

        # cur = main.con.cursor()
        cur = meCon.meConexion.cursor()
        cur.execute(sql, ('',))
        # main.con.commit()
        meCon.meConexion.commit()
        cur.close()
        meCon.cerrarConexion()



    def Veri_TipoObject_Gri7(self):

        fila = self.ui.tableWidget_7.currentRow()
        col = self.ui.tableWidget_7.currentColumn()

        vv_tipo_unidad = str(self.ui.comboBox_6.itemText(self.ui.comboBox_6.currentIndex()))

        if col == 5:
            self.created_Object_texto()
            return

        if vv_tipo_unidad != 'TANQUE SEMIREMOLQUE':
            if col in (7,):
                return

        self.created_Object_doubleSpinBox(self.ui.tableWidget_7, 1, 1000, 1000000)

    def Veri_TipoObject_Gri8(self):
        fila = self.ui.tableWidget_8.currentRow()
        col = self.ui.tableWidget_8.currentColumn()

        vv_tipo_unidad = str(self.ui.comboBox_6.itemText(self.ui.comboBox_6.currentIndex()))


        if vv_tipo_unidad != 'TANQUE SEMIREMOLQUE':
            if col in (2, 2):
                return

        self.created_Object_doubleSpinBox(self.ui.tableWidget_8, 1, 1000, 1000000)

    def Veri_TipoObject_Gri5(self):
        fila = self.ui.tableWidget_5.currentRow()


        if fila == 1:
            self.created_Object()


        if fila == 4:
            return

        if fila == 5:
            return

        if fila == 6:
            return

    def Veri_TipoObject_Gri6(self):
        fila = self.ui.tableWidget_6.currentRow()

        col = self.ui.tableWidget_6.currentColumn()

        if col > int(self.ui.lineEdit_10.text()):
            return

        if fila in (0, 1, 3, 6, 7, 9, 12, 14):
            return

        if fila == 4 and col == 1:
            lista_items = ('', 'SI', 'NO')
            self.created_Object_comboBox(lista_items, self.ui.tableWidget_6)


        if fila == 5 :
            if str(self.ui.tableWidget_6.item(4, col).text()) == 'SI':
                lista_items = ('', 'C', 'NC')
                self.created_Object_comboBox(lista_items, self.ui.tableWidget_6)

        if fila == 2:
            # return
            self.created_Object_doubleSpinBox(self.ui.tableWidget_6, 1, 0, 3)

        if fila == 8:
            valor_C = float(self.ui.tableWidget_7.item(0, 2).text()) if self.ui.tableWidget_7.item(0,2).text() != '' else 0

            self.created_Object_doubleSpinBox(self.ui.tableWidget_6, 1, 0, valor_C)

        if fila == 10:
            self.created_Object_doubleSpinBox(self.ui.tableWidget_6, 0, 0, 1000000)


        if fila == 11:
            return


        if fila == 13:
            self.created_Object_texto(self.ui.tableWidget_6)


        if fila == 15:
            self.created_Object_texto(self.ui.tableWidget_6)

    def Veri_TipoObject_Gri9(self):
        fila = self.ui.tableWidget_9.currentRow()

        if fila in (0, 1, 2, 3):
            return

        lista_items = ('', '-', 'C', 'NC')
        if self.ui.radioButton.isChecked():  # INI
            self.created_Object_comboBox(lista_items, self.ui.tableWidget_9)

    def Veri_TipoObject_Gri10(self):
        fila = self.ui.tableWidget_10.currentRow()

        col = self.ui.tableWidget_10.currentColumn()

        if col > int(self.ui.lineEdit_10.text()):
            return

        if fila in (0, 1):
            self.created_Object_doubleSpinBox(self.ui.tableWidget_10, 1, 0, 1000000)


    def restriccion_tableSeven(self):
        vv_tipo_unidad = str(self.ui.comboBox_6.itemText(self.ui.comboBox_6.currentIndex()))

        if vv_tipo_unidad == 'TANQUE SEMIREMOLQUE':
            item = QtWidgets.QTableWidgetItem()
            item.setText("")
            self.ui.tableWidget_7.setItem(0, 3, item)

            item1 = QtWidgets.QTableWidgetItem()
            item1.setText("")
            self.ui.tableWidget_7.setItem(0, 4, item1)

            item2 = QtWidgets.QTableWidgetItem()
            item2.setText("")
            self.ui.tableWidget_7.setItem(0, 7, item2)

            item3 = QtWidgets.QTableWidgetItem()
            item3.setText("")
            self.ui.tableWidget_8.setItem(0, 0, item3)

            item4 = QtWidgets.QTableWidgetItem()
            item4.setText("")
            self.ui.tableWidget_8.setItem(0, 1, item4)

            item5 = QtWidgets.QTableWidgetItem()
            item5.setText("")
            self.ui.tableWidget_8.setItem(0, 2, item5)

        else:

            item2 = QtWidgets.QTableWidgetItem()
            item2.setText("0")
            item2.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_7.setItem(0, 7, item2)


            item5 = QtWidgets.QTableWidgetItem()
            item5.setText("0")
            item5.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_8.setItem(0, 2, item5)



    def publicar_files(self):

        # ==========================================================================================
        sql = "select count(*) from BLOQUEO_SYSTEM where sw_bloqueo=true  and cod_user<>" + str(main.VGuser_cod)
        cur = main.con.cursor()
        cur.execute(sql)
        if int(cur.fetchall()[0][0]) > 0:
            QtWidgets.QMessageBox.information(self, 'VENTANA INFORMATIVA',
                                                       'OPERACION NO VALIDA... SISTEMA BLOQUEADO')
            exit()
        # ==========================================================================================

        if self.ui.pushButton_2.isEnabled() or self.ui.pushButton_3.isEnabled():
            msgbox = QtWidgets.QMessageBox.information(self, "Ventana Informativa", "OPERACION NO VALIDA")
            return

        if str(self.ui.pushButton_12.statusTip()).strip() != 'True':

            text, okPressed = QtWidgets.QInputDialog.getText(self, "Verificacion", "Ingrese su Contraseña",
                                                             QtWidgets.QLineEdit.Password, "")
            if not okPressed or self.ui.comboBox_5.statusTip() != text:
                msgbox = QtWidgets.QMessageBox.information(self, "Ventana Informativa", "OPERACION NO VALIDA")
                return

        # self.generar_certificado(True)
        self.ui.pushButton_4.setStatusTip('True')
        self.ui.pushButton_5.setStatusTip('True')
        self.ui.pushButton_12.setStatusTip('True')

        # -------------------------------------------------------------------------------------------
        v_ceros = '0' * (4 - len(str(self.ui.lineEdit_16.text())))
        v_name_path = str(self.ui.lineEdit_15.text()) + "-" + v_ceros + str(self.ui.lineEdit_16.text())

        vPublic_cod_clie = str(self.ui.lineEdit.statusTip())

        from zview_files import view_files
        p = view_files()
        p.setWindowTitle("Publicacion de Archivos")
        p.ui.label.setStatusTip(str(self.ui.lineEdit_15.statusTip()))  # cod certificado
        p.ui.tableWidget.setStatusTip(v_name_path)  # nro registro = PATH
        p.ui.pushButton_12.setStatusTip(vPublic_cod_clie)

        # p.setFixedSize(1001, 720)
        p.inicializar()
        p.setModal(1)
        p.show()
        p.exec()

    def view_files(self):
        v_name_path = str(self.ui.lineEdit_15.text()) + "-" + str(self.ui.lineEdit_16.text())
        if os.path.isdir(os.getcwd() + "/files/" + v_name_path):
            print("Si existe")
        else:
            os.mkdir(os.getcwd() + "/files/" + v_name_path)

        sql = "select * from certificados_files where cod_certificado=" + str(self.ui.lineEdit_15.statusTip())
        cur = main.con.cursor()
        cur.execute(sql)
        for row in cur.fetchall():
            tmp_name_file = "files/" + v_name_path + str(row[5])
            open(tmp_name_file, 'wb').write(row[3])  # frente

    def new_personal(self):
        from zpersonal import personal
        p = personal()
        p.exec()
        self.llenar_cubicadores_autorizadores()

    def new_cisterna(self):
        msgBox = QtWidgets.QMessageBox()
        msgBox.setText('QUE DESEA EXPORTAR ?')

        btnHabilitar = QtWidgets.QPushButton('Habilitar o Deshabilitar Listas')
        msgBox.addButton(btnHabilitar, QtWidgets.QMessageBox.NoRole)

        btnCrear = QtWidgets.QPushButton('Crear Cisterna')
        msgBox.addButton(btnCrear, QtWidgets.QMessageBox.YesRole)

        ret = msgBox.exec_()


        if msgBox.clickedButton() == btnCrear:
            from zcertificaciones_mant_cisternas import certificaciones_mant_cisternas
            p = certificaciones_mant_cisternas()
            p.ui.lineEdit.setStatusTip(str(self.ui.lineEdit.statusTip()))
            p.exec()
            self.llenar_cisternas()

    def new_tracto(self):
        vv_cod_cisterna = str(self.ui.comboBox_8.itemData(self.ui.comboBox_8.currentIndex()))
        if vv_cod_cisterna == 'None':
            return

        msgBox = QtWidgets.QMessageBox()
        msgBox.setText('QUE DESEA EXPORTAR ?')

        btnHabilitar = QtWidgets.QPushButton('Habilitar o Deshabilitar Listas')
        msgBox.addButton(btnHabilitar, QtWidgets.QMessageBox.NoRole)

        btnCrear = QtWidgets.QPushButton('Crear Tracto')
        msgBox.addButton(btnCrear, QtWidgets.QMessageBox.YesRole)

        ret = msgBox.exec_()


        if msgBox.clickedButton() == btnCrear:
            from zcertificaciones_mant_cisternas_veh import certificaciones_mant_cisternas_veh
            p = certificaciones_mant_cisternas_veh()
            p.ui.lineEdit.setStatusTip(str(self.ui.lineEdit.statusTip()))
            p.ui.pushButton.setStatusTip(vv_cod_cisterna)
            p.exec()
            self.llenar_vehiculos()



    def format_ReportInacal(self):
        self.ui.tableWidget_10.clear()
        self.ui.tableWidget_10.setColumnCount(12)
        self.ui.tableWidget_10.setRowCount(0)

        COL0 = QtWidgets.QTableWidgetItem()
        COL0.setText("Compartimentos")
        self.ui.tableWidget_10.setHorizontalHeaderItem(0, COL0)
        self.ui.tableWidget_10.setColumnWidth(0, 250)

        for i in range(11):
            COL = QtWidgets.QTableWidgetItem()
            COL.setText(str(i + 1))
            self.ui.tableWidget_10.setHorizontalHeaderItem(i + 1, COL)
            self.ui.tableWidget_10.setColumnWidth(i + 1, 50)

        # --------------------------------------------------------------------
        lista_MedIntTanq = {1: 'Volumen residual (L)',
                            2: '8.5.11 Altura de referencia HR cuando el tanque está lleno hasta su capacidad nominal (cm)'
                            }

        n = 0
        for llave in lista_MedIntTanq:
            self.ui.tableWidget_10.setRowCount(n + 1)

            item0 = QtWidgets.QTableWidgetItem()
            item0.setText(lista_MedIntTanq[llave])
            item0.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_10.setItem(n, 0, item0)

            for vcol in range(11):
                item = QtWidgets.QTableWidgetItem()
                item.setText("")
                # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.ui.tableWidget_10.setItem(n, vcol + 1, item)

            n += 1

    # def format_errorVerif_grid(self):
    def format_errorVerif_grid(self):
        self.ui.tableWidget_5.clear()
        self.ui.tableWidget_5.setColumnCount(12)
        self.ui.tableWidget_5.setRowCount(0)

        COL0 = QtWidgets.QTableWidgetItem()
        COL0.setText("Compartimentos")
        self.ui.tableWidget_5.setHorizontalHeaderItem(0, COL0)
        self.ui.tableWidget_5.setColumnWidth(0, 180)

        for i in range(11):
            COL = QtWidgets.QTableWidgetItem()
            COL.setText(str(i + 1))
            self.ui.tableWidget_5.setHorizontalHeaderItem(i + 1, COL)
            self.ui.tableWidget_5.setColumnWidth(i + 1, 50)

        # --------------------------------------------------------------------
        lista_MedIntTanq = {1: 'Capacidad nominal (L)',
                            2: 'Capacidad nominal (galones)',
                            3: '8.5.13 y 9.1 Capacidad real (L)',
                            4: 'Capacidad real (galones)',
                            5: 'Error (%)',
                            7: '5.1.2 Error ≤ 0,3%'
                            }

        n = 0
        for llave in lista_MedIntTanq:
            self.ui.tableWidget_5.setRowCount(n + 1)

            item0 = QtWidgets.QTableWidgetItem()
            item0.setText(lista_MedIntTanq[llave])
            item0.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_5.setItem(n, 0, item0)

            for vcol in range(11):
                item = QtWidgets.QTableWidgetItem()
                item.setText("")
                # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.ui.tableWidget_5.setItem(n, vcol + 1, item)

            n += 1

    def format_MedExtTanq_grid(self):
        self.ui.tableWidget_7.clear()
        self.ui.tableWidget_7.setColumnCount(8)
        self.ui.tableWidget_7.setRowCount(1)

        lista_MedExtTanq_sc = {1: 'a',
                               2: 'b',
                               3: 'c',
                               4: 'd',
                               5: 'e',
                               6: 'f',
                               7: 'g',
                               8: 'x'
                               }
        n = 0
        for llave in sorted(lista_MedExtTanq_sc.keys()):
            COL = QtWidgets.QTableWidgetItem()
            COL.setText(str(lista_MedExtTanq_sc[llave][0]))
            self.ui.tableWidget_7.setHorizontalHeaderItem(n, COL)
            self.ui.tableWidget_7.setColumnWidth(n, 50)

            for vcol in range(8):
                item = QtWidgets.QTableWidgetItem()
                if vcol == 5:
                    item.setText('52')
                else:
                    item.setText("")
                # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.ui.tableWidget_7.setItem(n, vcol, item)

            n += 1



        # --------------------------------------------------------------------

        self.ui.tableWidget_8.clear()
        self.ui.tableWidget_8.setColumnCount(3)
        self.ui.tableWidget_8.setRowCount(1)

        lista_MedExtTanq_sc = {1: 'd',
                               2: 'e',
                               3: 'x'
                               }
        n = 0
        for llave in sorted(lista_MedExtTanq_sc.keys()):
            COL = QtWidgets.QTableWidgetItem()
            COL.setText(str(lista_MedExtTanq_sc[llave][0]))
            self.ui.tableWidget_8.setHorizontalHeaderItem(n, COL)
            self.ui.tableWidget_8.setColumnWidth(n, 50)

            for vcol in range(3):
                item = QtWidgets.QTableWidgetItem()
                item.setText("")
                # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.ui.tableWidget_8.setItem(n, vcol, item)

            n += 1





    def format_ensayosExclusivos_grid(self):
        self.ui.tableWidget_9.clear()
        self.ui.tableWidget_9.setColumnCount(12)
        self.ui.tableWidget_9.setRowCount(0)

        COL0 = QtWidgets.QTableWidgetItem()
        COL0.setText("Compartimentos")
        self.ui.tableWidget_9.setHorizontalHeaderItem(0, COL0)
        self.ui.tableWidget_9.setColumnWidth(0, 330)

        for i in range(11):
            COL = QtWidgets.QTableWidgetItem()
            COL.setText(str(i + 1))
            self.ui.tableWidget_9.setHorizontalHeaderItem(i + 1, COL)
            self.ui.tableWidget_9.setColumnWidth(i + 1, 50)

        # --------------------------------------------------------------------
        # txt_5='8.4.4 Dilatacion del material del tanque\nCoef. Dilatacion Lineal ≤ 33 x 10⁻⁶ ºC⁻¹ o\nCoef. Dilatacion Cubica ≤ 99 x 10⁻⁶ ºC⁻¹'

        lista_MedIntTanq = {1: '8.4.1 Inspeccion interna, requisitos cap. 5',
                            2: '5.4.2.2 Espesor de la mesa de medición entre 4mm y 6mm',
                            3: '8.4.2 Volumen de expansion ≥ 1%',
                            4: '8.4.3 Dilatación del material del tanque\nCoef. Dilatación Lineal ≤ 33 x 10⁻⁶ ºC⁻¹ o\nCoef. Dilatación Cúbica ≤ 99 x 10⁻⁶ ºC⁻¹'
                            }

        n = 0
        for llave in sorted(lista_MedIntTanq.keys()):
            self.ui.tableWidget_9.setRowCount(n + 1)

            item = QtWidgets.QTableWidgetItem()
            item.setText(lista_MedIntTanq[llave])
            # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_9.setItem(n, 0, item)

            for vcol in range(11):
                item = QtWidgets.QTableWidgetItem()
                item.setText("")
                # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.ui.tableWidget_9.setItem(n, vcol + 1, item)

            n += 1

        self.ui.tableWidget_9.setRowHeight(4, 60)

    def format_ensayosVerificacion_grid(self):
        self.ui.tableWidget_6.clear()
        self.ui.tableWidget_6.setColumnCount(12)
        self.ui.tableWidget_6.setRowCount(0)

        COL0 = QtWidgets.QTableWidgetItem()
        COL0.setText("Compartimentos")
        self.ui.tableWidget_6.setHorizontalHeaderItem(0, COL0)
        self.ui.tableWidget_6.setColumnWidth(0, 330)

        for i in range(11):
            COL = QtWidgets.QTableWidgetItem()
            COL.setText(str(i + 1))
            self.ui.tableWidget_6.setHorizontalHeaderItem(i + 1, COL)
            self.ui.tableWidget_6.setColumnWidth(i + 1, 50)

        # --------------------------------------------------------------------
        # txt_5='8.4.4 Dilatacion del material del tanque\nCoef. Dilatacion Lineal ≤ 33 x 10⁻⁶ ºC⁻¹ o\nCoef. Dilatacion Cubica ≤ 99 x 10⁻⁶ ºC⁻¹'

        lista_EnsayoVeri = {1 : '8.5.1 Volumen residual, VRMP = 0,00029 x Vn + 0,7',
                            2 : '8.5.3.2 y 8.5.3.3 Revisión de fugas entre mamparos',
                            3 : '8.5.4.2 Sensibilidad: para 0,1% cuántos mm varía',
                            4 : 'Δh ≥ 1,0 mm',
                            5 : '¿El disco estaba fijado por un precinto anterior?',
                            6 : '8.5.5.1 |Error inicial| ≤ 0,3% (≤3Δh)',
                            7 : '8.5.7.1 Medidas contiguas.\nVariación de altura de líquido ≤ Δh.\nLa capacidad no varía más de 0,1% independientemente de si los compartimientos vecinos están llenos o vacíos',
                            8 : '8.5.9.1 Repetibilidad\nVariación de altura de líquido ≤ 1 mm\nNo presenta deformaciones que varíen la capacidad más de 1 mm entre 2 ensayos consecutivos',
                            9 : '8.5.9.1 Altura de líquido final (cm)',
                            10: '8.5.11 Variación de altura de referencia\n≤ (el mayor de 2 mm y H/1000)',
                            11: '8.5.12.1 Altura de Espacio vacío (mm)',
                            12: 'Espacio vacío ≤ 200 mm',
                            13: '8.5.13 ¿Se realizó el ajuste del disco?',
                            14: 'Numero del precinto colocado en el disco',
                            15: '8.5.16 ¿Se fijo la placa de verificacion?',
                            16: '8.5.18 Número del precinto en el perno de la tapa'
                            }

        n = 0
        for llave in sorted(lista_EnsayoVeri.keys()):
            self.ui.tableWidget_6.setRowCount(n + 1)

            item = QtWidgets.QTableWidgetItem()
            item.setText(lista_EnsayoVeri[llave])
            # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_6.setItem(n, 0, item)

            for vcol in range(11):
                item = QtWidgets.QTableWidgetItem()
                item.setText("")
                # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.ui.tableWidget_6.setItem(n, vcol + 1, item)

            n += 1

        self.ui.tableWidget_6.setRowHeight(3, 40)
        self.ui.tableWidget_6.setRowHeight(4, 40)
        self.ui.tableWidget_6.setRowHeight(6, 80)
        self.ui.tableWidget_6.setRowHeight(7, 60)

        self.ui.tableWidget_6.setRowHeight(8, 40)

    def new_trazabilidad_grid(self):
        self.format_trazabilidad_grid()

        sql = "select"
        sql = sql + " cod_equipo,"  # 0
        sql = sql + "id_equipo,"  # 1
        sql = sql + "nom_equipo,"  # 2
        sql = sql + "certificado_equipo,"  # 3
        sql = sql + "fecha_calibracion "  # 4
        sql = sql + " from equipos "
        sql = sql + " where estado=true and cod_tipo_equipo=1 "
        sql = sql + " order by cod_equipo,id_equipo,nom_equipo,certificado_equipo "
        print(sql)

        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)
        resultado = cur.fetchall()
        cur.close()
        meCon.cerrarConexion()

        n = 0
        for row in resultado:
            self.ui.tableWidget_2.setRowCount(n + 1)

            item0 = QtWidgets.QTableWidgetItem()
            item0.setText(str(row[0]))
            self.ui.tableWidget_2.setItem(n, 0, item0)

            item1 = QtWidgets.QTableWidgetItem()
            item1.setText(str(row[1]))
            self.ui.tableWidget_2.setItem(n, 1, item1)

            item2 = QtWidgets.QTableWidgetItem()
            item2.setText(str(row[2]))
            self.ui.tableWidget_2.setItem(n, 2, item2)

            item3 = QtWidgets.QTableWidgetItem()
            item3.setText(str(row[3]))
            self.ui.tableWidget_2.setItem(n, 3, item3)

            item4 = QtWidgets.QTableWidgetItem()
            item4.setText(str(row[4]))
            self.ui.tableWidget_2.setItem(n, 4, item4)

            n = n + 1

    def format_trazabilidad_grid(self):
        self.ui.tableWidget_2.clear()
        self.ui.tableWidget_2.setColumnCount(5)
        self.ui.tableWidget_2.setRowCount(0)

        COL0 = QtWidgets.QTableWidgetItem()
        COL0.setText("Cod Calibracion")
        self.ui.tableWidget_2.setHorizontalHeaderItem(0, COL0)

        COL1 = QtWidgets.QTableWidgetItem()
        COL1.setText("Ident. equipos")
        self.ui.tableWidget_2.setHorizontalHeaderItem(1, COL1)

        COL2 = QtWidgets.QTableWidgetItem()
        COL2.setText("Nombre equipo / instrumento")
        self.ui.tableWidget_2.setHorizontalHeaderItem(2, COL2)

        COL3 = QtWidgets.QTableWidgetItem()
        COL3.setText("Certificacion calibracion")
        self.ui.tableWidget_2.setHorizontalHeaderItem(3, COL3)

        COL4 = QtWidgets.QTableWidgetItem()
        COL4.setText("Fecha calibracion")
        self.ui.tableWidget_2.setHorizontalHeaderItem(4, COL4)

        self.ui.tableWidget_2.setColumnWidth(0, 90)  # CODIGO trazabilidad
        self.ui.tableWidget_2.setColumnWidth(1, 90)  ##ID TRAZABILIDAD
        self.ui.tableWidget_2.setColumnWidth(2, 150)  # NOMBRE EQUIPO
        self.ui.tableWidget_2.setColumnWidth(3, 150)  # CERTIFICADO CALIBRACION
        self.ui.tableWidget_2.setColumnWidth(4, 150)  # FECHA CALIBRACION

        # ----------------

        # --------------------------------------------------------------------
        # lista_trazabilidad={1:['HD-001','CINTA DE SONDAJE CON DIVISION  1 mm','METROIL L-0410-2018'],
        #                     2:['HD-002','CINTA MÉTRICA (0 M A 15 M)','METROIL L-0412-2018'],
        #                     3:['HD-003','MANOMETRO DIVISION 0.2 BAR','UNIMETRO CPU-956-2018'],
        #                     4:['HD-004','MEDIDOR VOLUMÉTRICO 10LT. CLASE 0.1',''],
        #                     5:['HD-005','MEDIDOR VOLUMETRICO 1LT. CLASE 0.1',''],
        #                     6:['HD-006','MEDIDOR VOLUMETRICO 5LT. CLASE 0.1',''],
        #                     7:['HD-007','MEDIDOR VOLUMETRICO DE 100 GLN CLASE 0.1','METROIL VC-0331-2018'],
        #                     8:['HD-008','MEDIDOR VOLUMETRICO DE 1000 GLN CLASE 0.1','METROIL VC-0330-2018'],
        #                     9:['HD-009','TERMÓMETRO CON RESOLUCION 0.1 °C','METROIL T-1416-2018']}
        # n=0
        # for llave in sorted(lista_trazabilidad.keys()):
        #     self.ui.tableWidget_2.setRowCount(n + 1)
        #
        #     item0 = QtWidgets.QTableWidgetItem()
        #     item0.setText(lista_trazabilidad[llave][0])
        #     self.ui.tableWidget_2.setItem(n, 0, item0)
        #
        #     item1 = QtWidgets.QTableWidgetItem()
        #     item1.setText(lista_trazabilidad[llave][1])
        #     self.ui.tableWidget_2.setItem(n, 1, item1)
        #
        #     item2 = QtWidgets.QTableWidgetItem()
        #     item2.setText(lista_trazabilidad[llave][2])
        #     self.ui.tableWidget_2.setItem(n, 2, item2)
        #
        #     n+=1

    # def llenar_a_la_fecha(self):
    #     self.ui.comboBox_3.clear()
    #     self.ui.comboBox_3.addItem("A LA FLECHA",1)
    #     self.ui.comboBox_3.addItem("AL DOMO",1)



    def inicializar(self):


        self.llenar_empresa_resolucion()
        self.llenar_cubicadores_autorizadores()

        self.ui.dateEdit.setDate(QtCore.QDate.currentDate())
        self.ui.dateEdit_3.setDate(QtCore.QDate.currentDate())

        # self.llenar_a_la_fecha()


        self.ui.lineEdit_17.setMaxLength(109)
        self.ui.lineEdit.setMaxLength(104)
        self.ui.lineEdit_6.setMaxLength(127)


        ##################### dando formato
        for child in self.findChildren(QtWidgets.QLineEdit):
            child.setReadOnly(False)

        self.ui.checkBox.setVisible(False)
        self.ui.pushButton_17.setVisible(False)
        self.ui.pushButton_10.setVisible(False)
        self.ui.pushButton_11.setVisible(False)
        self.ui.pushButton_2.setVisible(False)
        #####################


        v_doc_normativo = """Los ensayos se realizaron de acuerdo a lo establecido en la Norma Metrológica Peruana NMP 023:2021 "VEHÍCULOS Y VAGONES TANQUE, """
        v_doc_normativo += """Requisitos y Método de Ensayo" y en el Procedimiento para la Verificación de Vehículos Tanque "GT-PR-01 PROCEDIMIENTO DE INSPECCIÓN VEHÍCULO TANQUE, VERSION 07". """
        self.ui.plainTextEdit.setPlainText(v_doc_normativo)

        self.hallar_data_empresa()





        self.ui.pushButton.setVisible(False)
        self.ui.doubleSpinBox_2.setVisible(False)
        self.ui.doubleSpinBox_3.setVisible(False)
        self.ui.label_7.setVisible(False)





    def hallar_data_empresa(self):
        global emp_logo
        global emp_dir
        global emp_tlf
        global emp_correo

        vv_cod_empresa = "1"  # str(self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex()))
        sql = "select logo,direccion,tlf,correo from empresa where cod_empresa=" + vv_cod_empresa

        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)
        for row in cur.fetchall():
            emp_logo = str(row[0])
            emp_dir = str(row[1])
            emp_tlf = str(row[2])
            emp_correo = str(row[3])


        cur.close()
        meCon.cerrarConexion()


    def hallar_rubrica(self):
        vv_cod_personal = str(self.ui.comboBox_5.itemData(self.ui.comboBox_5.currentIndex()))
        if vv_cod_personal == 'None':
            return

        sql = "select firma_autorizado, pass, sello_autorizado from personal where cod_personal=" + vv_cod_personal
        print(sql)
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)
        for row in cur.fetchall():
            if str(row[0]) == 'None':
                return

            open('img/tmp_firma.png', 'wb').write(row[0])  # frente
            self.ui.label_rubrica.setStatusTip(str(os.getcwd()) + "/img/tmp_firma.png")
            self.ui.label_rubrica.setPixmap(QtGui.QPixmap(str(os.getcwd()) + "/img/tmp_firma.png"))

            open('img/tmp_sello.png', 'wb').write(row[2])  # frente
            self.ui.label_sello.setStatusTip(str(os.getcwd()) + "/img/tmp_sello.png")
            self.ui.label_sello.setPixmap(QtGui.QPixmap(str(os.getcwd()) + "/img/tmp_sello.png"))

            self.ui.comboBox_5.setStatusTip(str(row[1]))


        cur.close()
        meCon.cerrarConexion()


    def hallar_rubrica_GerenteTecnico(self):
        vv_cod_personal = str(self.ui.comboBox_4.itemData(self.ui.comboBox_4.currentIndex()))
        if vv_cod_personal == 'None':
            return

        sql = "select firma_autorizado, pass, sello_autorizado from personal where cod_personal=" + vv_cod_personal
        print(sql)
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)
        for row in cur.fetchall():
            if str(row[0]) == 'None':
                return

            open('img/tmp_firma_Gerente.png', 'wb').write(row[0])  # frente
            self.ui.label_rubrica_2.setStatusTip(str(os.getcwd()) + "/img/tmp_firma_Gerente.png")
            self.ui.label_rubrica_2.setPixmap(QtGui.QPixmap(str(os.getcwd()) + "/img/tmp_firma_Gerente.png"))

            open('img/tmp_sello_Gerente.png', 'wb').write(row[2])  # frente
            self.ui.label_sello_2.setStatusTip(str(os.getcwd()) + "/img/tmp_sello_Gerente.png")
            self.ui.label_sello_2.setPixmap(QtGui.QPixmap(str(os.getcwd()) + "/img/tmp_sello_Gerente.png"))

            self.ui.comboBox_4.setStatusTip(str(row[1]))


        cur.close()
        meCon.cerrarConexion()


    #####################################################################################################################
    ################################ Generando datos de Identificadion para generar Certificado ##########################
    #####################################################################################################################

    def hallar_nro_certificado(self):
        sql = "select serie, case when max(nro) is null then 1 else max(cast(nro as integer))+1 end from series where tipo_doc='CERTIFICACIONES' group by serie"
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        rows = cur.fetchall()[0]
        cur.close()
        meCon.cerrarConexion()

        self.ui.lineEdit_15.setText(str(rows[0]))
        self.ui.lineEdit_16.setText(str(rows[1]))

    # ---------------------------------------------------------------------------------------------------

    def hallar_nro_cisterna(self):
        sql = "select serie||'-'||case when max(nro) is null then 1 else max(cast(nro as integer))+1 end from series where tipo_doc='CISTERNA' group by serie"
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        rows = cur.fetchall()[0]
        cur.close()
        meCon.cerrarConexion()

        self.ui.lineEdit_5.setText(str(rows[0]))

    # ---------------------------------------------------------------------------------------------------

    def hallar_cod_random(self):
        # if str(self.ui.lineEdit_12.text()) != "" and str(self.ui.lineEdit_12.text()) != "None":
        #     return

        total_cod_exist = 1
        while int(total_cod_exist) > 0:
            self.ui.lineEdit_12.setText(self.generar_codigo_random(6))
            sql = "select count(cod_capicua) from tarjetas where cod_capicua='" + str(self.ui.lineEdit_12.text()) + "'"
            # cur = main.con.cursor()
            meCon = conexion()
            cur = meCon.meConexion.cursor()
            cur.execute(sql)
            total_cod_exist = cur.fetchall()[0][0]
            cur.close()
            meCon.cerrarConexion()


    def generar_codigo_random(self, limit_digit):
        import random

        number = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
        letter_min = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                      't', 'v', 'w', 'x', 'y', 'z']
        letter_may = [x.upper() for x in letter_min]

        list_character = number + letter_min + letter_may

        n = 1
        cod_random = ''
        while n <= limit_digit:
            cod_random = cod_random + str(random.choice(list_character))
            n += 1

        return cod_random

    #######################################################################################################################
    ################################################# UPDATE DATA ###########################################################
    #######################################################################################################################

    def buscar_certificado(self, vcod_certificado):
        sql = "select "
        sql = sql + " cod_empresa, registro_cubicacion_serie, registro_cubicacion_nro, cod_tanque,fecha_emision,"
        sql = sql + " fecha_expiracion,estado,cod_vehiculo,cod_cisterna,cod_clie,"
        sql = sql + " cod_venta,cod_capicua,cod_usuario,r_social,ruc,"
        sql = sql + " direccion,'' as clasificacion_tanque,marca_vehiculo,modelo_vehiculo,fab_vehiculo,"
        sql = sql + " serie_vehiculo,placa_vehiculo,ejes_vehiculo,vin_vehiculo,marca_tanque,"
        sql = sql + " modelo_tanque,fab_tanque,serie_tanque,placa_tanque,ejes_tanque,"
        sql = sql + " vin_tanque,capacidad_nominal_litros,total_compartimentos,img_frente,img_perfil,"
        sql = sql + " lugar_verif,Doc_normativo_proc_verif,conclusion,inspector,responsable,"
        sql = sql + " capacidad_nominal_galones"
        sql = sql + " from certificados"
        sql = sql + " where cod_certificado=" + str(vcod_certificado)

        print(sql)

        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)

        for row in cur.fetchall():
            open('img/img_frente.jpg', 'wb').write(row[33])
            self.ui.label_img_frente.setPixmap(QtGui.QPixmap('img/img_frente.jpg'))
            self.ui.label_img_frente.setStatusTip('img/img_frente.jpg')

            open('img/img_perfil.jpg', 'wb').write(row[34])
            self.ui.label_img_trasera.setPixmap(QtGui.QPixmap('img/img_perfil.jpg'))
            self.ui.label_img_trasera.setStatusTip('img/img_perfil.jpg')


        cur.close()
        meCon.cerrarConexion()


    #######################################################################################################################
    ################################################# GUARDAR DATA ###########################################################
    #######################################################################################################################
    def veri_ExistHojaCalculo(self):
        sql = "select COUNT(cod_hojacalculo) from hojacalculo_cab where cod_certificado=%s" %(self.ui.lineEdit_15.statusTip())
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)
        resultado = cur.fetchall()
        cur.close()
        meCon.cerrarConexion()

        for row in resultado:
            if int(row[0]) > 0:
                return True

        return False



    def generar_certificado(self, sw_firma):
        validador_fecha_emi = int("%s%s%s" % (str(self.ui.dateEdit.date().year()), str(self.ui.dateEdit.date().month()).zfill(2),str(self.ui.dateEdit.date().day()).zfill(2)))


        # ==========================================================================================
        sql = "select count(*) from BLOQUEO_SYSTEM where sw_bloqueo=true  and cod_user<>" + str(main.VGuser_cod)
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)
        resultado = cur.fetchall()[0][0]
        cur.close()
        meCon.cerrarConexion()

        if int(resultado) > 0:
            QtWidgets.QMessageBox.information(self, 'VENTANA INFORMATIVA', 'OPERACION NO VALIDA... SISTEMA BLOQUEADO')
            exit()
        # ==========================================================================================

        if self.ui.pushButton_3.isEnabled():
            QtWidgets.QMessageBox.information(self, "Ventana Informativa", "OPERACION NO VALIDA")
            return

        # ===========================================================================
        if str(self.ui.pushButton_4.statusTip()).strip() != 'True':

            text, okPressed = QtWidgets.QInputDialog.getText(self, "Verificacion", "Ingrese su Contraseña",
                                                             QtWidgets.QLineEdit.Password, "")
            if not okPressed or self.ui.comboBox_5.statusTip() != text:
                QtWidgets.QMessageBox.information(self, "Ventana Informativa", "OPERACION NO VALIDA")
                return

        self.ui.pushButton_4.setStatusTip('True')
        self.ui.pushButton_5.setStatusTip('True')
        self.ui.pushButton_12.setStatusTip('True')

        # ===========================================================================

        # self.crear_pdf_certificado()
        dic_trazabilidad = {}
        for fila in range(self.ui.tableWidget_2.rowCount()):
            vide = self.ui.tableWidget_2.item(fila, 1).text()
            vnom = self.ui.tableWidget_2.item(fila, 2).text()
            vcert = self.ui.tableWidget_2.item(fila, 3).text()
            vfecha_calibracion = str(self.ui.tableWidget_2.item(fila, 4).text())
            dic_trazabilidad[fila] = (vide, vnom, vcert, vfecha_calibracion)

        # print(dic_trazabilidad)

        # dic_condi_generales = {}  # Condiciones Generales
        dic_ensayos_exclusivos = {}  # grabar_ensayo_exclusivos_ini
        for fila in range(self.ui.tableWidget_9.rowCount()):
            vcomp = self.ui.tableWidget_9.item(fila, 0).text()

            vcomp_1 = "" if str(self.ui.tableWidget_9.item(fila, 1)) == 'None' else self.ui.tableWidget_9.item(fila,
                                                                                                               1).text()
            vcomp_2 = "" if str(self.ui.tableWidget_9.item(fila, 2)) == 'None' else self.ui.tableWidget_9.item(fila,
                                                                                                               2).text()
            vcomp_3 = "" if str(self.ui.tableWidget_9.item(fila, 3)) == 'None' else self.ui.tableWidget_9.item(fila,
                                                                                                               3).text()
            vcomp_4 = "" if str(self.ui.tableWidget_9.item(fila, 4)) == 'None' else self.ui.tableWidget_9.item(fila,
                                                                                                               4).text()
            vcomp_5 = "" if str(self.ui.tableWidget_9.item(fila, 5)) == 'None' else self.ui.tableWidget_9.item(fila,
                                                                                                               5).text()
            vcomp_6 = "" if str(self.ui.tableWidget_9.item(fila, 6)) == 'None' else self.ui.tableWidget_9.item(fila,
                                                                                                               6).text()
            vcomp_7 = "" if str(self.ui.tableWidget_9.item(fila, 7)) == 'None' else self.ui.tableWidget_9.item(fila,
                                                                                                               7).text()
            vcomp_8 = "" if str(self.ui.tableWidget_9.item(fila, 8)) == 'None' else self.ui.tableWidget_9.item(fila,
                                                                                                               8).text()
            vcomp_9 = "" if str(self.ui.tableWidget_9.item(fila, 9)) == 'None' else self.ui.tableWidget_9.item(fila,
                                                                                                               9).text()
            vcomp_10 = "" if str(self.ui.tableWidget_9.item(fila, 10)) == 'None' else self.ui.tableWidget_9.item(fila,
                                                                                                                 10).text()

            vcomp_11 = "" if str(self.ui.tableWidget_9.item(fila, 11)) == 'None' else self.ui.tableWidget_9.item(fila,
                                                                                                                 11).text()

            # vcomp_1 = "X" if self.ui.tableWidget_9.item(fila, 1).checkState() == QtCore.Qt.Checked else ""
            # vcomp_2 = "X" if self.ui.tableWidget_9.item(fila, 2).checkState() == QtCore.Qt.Checked else ""
            # vcomp_3 = "X" if self.ui.tableWidget_9.item(fila, 3).checkState() == QtCore.Qt.Checked else ""
            # vcomp_4 = "X" if self.ui.tableWidget_9.item(fila, 4).checkState() == QtCore.Qt.Checked else ""
            # vcomp_5 = "X" if self.ui.tableWidget_9.item(fila, 5).checkState() == QtCore.Qt.Checked else ""
            # vcomp_6 = "X" if self.ui.tableWidget_9.item(fila, 6).checkState() == QtCore.Qt.Checked else ""
            # vcomp_7 = "X" if self.ui.tableWidget_9.item(fila, 7).checkState() == QtCore.Qt.Checked else ""
            # vcomp_8 = "X" if self.ui.tableWidget_9.item(fila, 8).checkState() == QtCore.Qt.Checked else ""
            # vcomp_9 = "X" if self.ui.tableWidget_9.item(fila, 9).checkState() == QtCore.Qt.Checked else ""
            # vcomp_10 = "X" if self.ui.tableWidget_9.item(fila, 10).checkState() == QtCore.Qt.Checked else ""

            dic_ensayos_exclusivos[fila] = (
            u'{0}'.format(vcomp), u'{0}'.format(vcomp_1), u'{0}'.format(vcomp_2), u'{0}'.format(vcomp_3),
            u'{0}'.format(vcomp_4), u'{0}'.format(vcomp_5), u'{0}'.format(vcomp_6), u'{0}'.format(vcomp_7),
            u'{0}'.format(vcomp_8), u'{0}'.format(vcomp_9), u'{0}'.format(vcomp_10), u'{0}'.format(vcomp_11))

        # print(dic_ensayos_exclusivos)

        # dic_mit={}#Caracteristicas Tecnicas
        dic_veri_ini_post = {}
        for fila in range(self.ui.tableWidget_6.rowCount()):
            vcomp = self.ui.tableWidget_6.item(fila, 0).text()
            vcomp_1 = "" if str(self.ui.tableWidget_6.item(fila, 1)) == 'None' else self.ui.tableWidget_6.item(fila,
                                                                                                               1).text()
            vcomp_2 = "" if str(self.ui.tableWidget_6.item(fila, 2)) == 'None' else self.ui.tableWidget_6.item(fila,
                                                                                                               2).text()
            vcomp_3 = "" if str(self.ui.tableWidget_6.item(fila, 3)) == 'None' else self.ui.tableWidget_6.item(fila,
                                                                                                               3).text()
            vcomp_4 = "" if str(self.ui.tableWidget_6.item(fila, 4)) == 'None' else self.ui.tableWidget_6.item(fila,
                                                                                                               4).text()
            vcomp_5 = "" if str(self.ui.tableWidget_6.item(fila, 5)) == 'None' else self.ui.tableWidget_6.item(fila,
                                                                                                               5).text()
            vcomp_6 = "" if str(self.ui.tableWidget_6.item(fila, 6)) == 'None' else self.ui.tableWidget_6.item(fila,
                                                                                                               6).text()
            vcomp_7 = "" if str(self.ui.tableWidget_6.item(fila, 7)) == 'None' else self.ui.tableWidget_6.item(fila,
                                                                                                               7).text()
            vcomp_8 = "" if str(self.ui.tableWidget_6.item(fila, 8)) == 'None' else self.ui.tableWidget_6.item(fila,
                                                                                                               8).text()
            vcomp_9 = "" if str(self.ui.tableWidget_6.item(fila, 9)) == 'None' else self.ui.tableWidget_6.item(fila,
                                                                                                               9).text()
            vcomp_10 = "" if str(self.ui.tableWidget_6.item(fila, 10)) == 'None' else self.ui.tableWidget_6.item(fila,
                                                                                                                 10).text()

            vcomp_11 = "" if str(self.ui.tableWidget_6.item(fila, 11)) == 'None' else self.ui.tableWidget_6.item(fila,
                                                                                                                 11).text()

            dic_veri_ini_post[fila] = (
            vcomp, vcomp_1, vcomp_2, vcomp_3, vcomp_4, vcomp_5, vcomp_6, vcomp_7, vcomp_8, vcomp_9, vcomp_10, vcomp_11)

        # print(dic_veri_ini_post)

        dic_signos = {0: 'a:', 1: 'b:', 2: 'c:', 3: 'd:', 4: 'e:', 5: 'f:', 6: 'g:', 7: 'x:'}
        dic_met = {}
        for col in range(self.ui.tableWidget_7.columnCount()):
            vvalor = str(self.ui.tableWidget_7.item(0, col).text())

            dic_met[col] = (dic_signos[col], vvalor)

        dic_met[8] = (dic_signos[3], str(self.ui.tableWidget_8.item(0, 0).text()))
        dic_met[9] = (dic_signos[4], str(self.ui.tableWidget_8.item(0, 1).text()))
        dic_met[10] = (dic_signos[7], str(self.ui.tableWidget_8.item(0, 2).text()))

        # print(dic_met)

        # dic_resultados={}#Error Maximo Permisible
        dic_error_max = {}
        for fila in range(self.ui.tableWidget_5.rowCount()):
            vcomp = "" if str(self.ui.tableWidget_5.item(fila, 0)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                             0).text()
            vcomp_1 = "" if str(self.ui.tableWidget_5.item(fila, 1)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                               1).text()
            vcomp_2 = "" if str(self.ui.tableWidget_5.item(fila, 2)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                               2).text()
            vcomp_3 = "" if str(self.ui.tableWidget_5.item(fila, 3)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                               3).text()
            vcomp_4 = "" if str(self.ui.tableWidget_5.item(fila, 4)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                               4).text()
            vcomp_5 = "" if str(self.ui.tableWidget_5.item(fila, 5)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                               5).text()
            vcomp_6 = "" if str(self.ui.tableWidget_5.item(fila, 6)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                               6).text()
            vcomp_7 = "" if str(self.ui.tableWidget_5.item(fila, 7)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                               7).text()
            vcomp_8 = "" if str(self.ui.tableWidget_5.item(fila, 8)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                               8).text()
            vcomp_9 = "" if str(self.ui.tableWidget_5.item(fila, 9)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                               9).text()
            vcomp_10 = "" if str(self.ui.tableWidget_5.item(fila, 10)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                                 10).text()

            vcomp_11 = "" if str(self.ui.tableWidget_5.item(fila, 11)) == 'None' else self.ui.tableWidget_5.item(fila,
                                                                                                                 11).text()

            dic_error_max[fila] = (
            vcomp, vcomp_1, vcomp_2, vcomp_3, vcomp_4, vcomp_5, vcomp_6, vcomp_7, vcomp_8, vcomp_9, vcomp_10, vcomp_11)

        # print(dic_error_max)

        vv_fecha_emi_day = "00"[:2 - len(str(self.ui.dateEdit.date().day()))] + str(self.ui.dateEdit.date().day())
        vv_fecha_emi_mes = "00"[:2 - len(str(self.ui.dateEdit.date().month()))] + str(self.ui.dateEdit.date().month())
        vv_fecha_emi = str(self.ui.dateEdit.date().year()) + "-" + vv_fecha_emi_mes + "-" + vv_fecha_emi_day
        # vv_fecha_emi = str("%s-%s-%s" % (self.ui.dateEdit.date().year(), self.ui.dateEdit.date().month(), self.ui.dateEdit.date().day()))

        vv_fecha_exp_day = "00"[:2 - len(str(self.ui.dateEdit_2.date().day()))] + str(self.ui.dateEdit_2.date().day())
        vv_fecha_exp_mes = "00"[:2 - len(str(self.ui.dateEdit_2.date().month()))] + str(self.ui.dateEdit_2.date().month())
        vv_fecha_exp = str(self.ui.dateEdit_2.date().year()) + "-" + vv_fecha_exp_mes + "-" + vv_fecha_exp_day
        # vv_fecha_exp = str("%s-%s-%s" % (self.ui.dateEdit_2.date().year(), self.ui.dateEdit_2.date().month(), self.ui.dateEdit_2.date().day()))

        vv_fecha_verificacion_day = "00"[:2 - len(str(self.ui.dateEdit_3.date().day()))] + str(self.ui.dateEdit_3.date().day())
        vv_fecha_verificacion_mes = "00"[:2 - len(str(self.ui.dateEdit_3.date().month()))] + str(self.ui.dateEdit_3.date().month())
        vv_fecha_verificacion = str(self.ui.dateEdit_3.date().year()) + "-" + vv_fecha_verificacion_mes + "-" + vv_fecha_verificacion_day
        # vv_fecha_verificacion = str("%s-%s-%s" % (self.ui.dateEdit_3.date().year(), self.ui.dateEdit_3.date().month(), self.ui.dateEdit_3.date().day()))

        vv_tipo_unidad = str(self.ui.comboBox_6.itemText(self.ui.comboBox_6.currentIndex()))
        vv_placa_vehiculo = str(self.ui.comboBox_7.itemText(self.ui.comboBox_7.currentIndex()))
        vv_placa_cisterna = str(self.ui.comboBox_8.itemText(self.ui.comboBox_8.currentIndex())) if vv_tipo_unidad!='CAMION TANQUE' else '-'

        vv_nom_inspector = str(self.ui.comboBox_4.itemText(self.ui.comboBox_4.currentIndex()))  # Gerente tecnico
        vv_nom_responsable = str(self.ui.comboBox_5.itemText(self.ui.comboBox_5.currentIndex()))  # Responsable tecnico

        vv_img_frente = self.ui.label_img_frente.statusTip()
        vv_img_perfil = self.ui.label_img_trasera.statusTip()

        vv_tipo_verificacion = "INICIAL" if self.ui.radioButton.isChecked() else "POSTERIOR"

        v_hora_ini = str(self.ui.timeEdit.time().hour()) + ":" + str(self.ui.timeEdit.time().minute()) + ":00"
        v_hora_fin = str(self.ui.timeEdit_2.time().hour()) + ":" + str(self.ui.timeEdit_2.time().minute()) + ":00"
        v_Version = "05"

        # print("&&&&&&")
        # print((4-len(str(self.ui.lineEdit_16.text())))
        v_ceros = '0' * (4 - len(str(self.ui.lineEdit_16.text())))

        estado_conclusion = 'CONFORME (*)' if self.ui.checkBox_2.isChecked() else 'NO CONFORME (*)'

        textQR=("{}: {}".format("Numero de Certificado", str(self.ui.lineEdit_15.text()) + '-' + str(v_ceros) + str(self.ui.lineEdit_16.text())),
                "{}: {}".format("Cliente", str(self.ui.lineEdit.text()).replace("&", " & ")),
                "{}: {}".format("Producto o Servicio", "Tanques de carga montados sobre vehículos automotrices y semirremolques destinados al transporte de productos derivados de petróleo. Verificación de tanques de carga (Verificación inicial y posterior)"),
                "{}: {}".format("Código Nace", "28.21 Fabricación de cisternas, grandes depósitos y contenedores de metal"),
                "{}: {}".format("Fecha de Emisión", str(vv_fecha_emi)),
                "{}: {}".format("Tipo de Verificación", vv_tipo_verificacion),
                "{}: {}".format("Resultado de la Inspección", estado_conclusion.replace(" (*)", "")),
                "{}: {}".format("Altura de Liquido Final", ' - '.join(self.hallar_CompartimientosQR(dic_veri_ini_post)))
                )

        data_certificado = {
                'nro_certificado'                    : str(self.ui.lineEdit_15.text()) + '-' + str(v_ceros) + str(self.ui.lineEdit_16.text()),
                'razon_social'                       : str(self.ui.lineEdit.text()),
                'direccion'                          : str(self.ui.lineEdit_6.text()),
                'Fecha_emi'                          : vv_fecha_emi,
                'Fecha_venc'                         : vv_fecha_exp,
                'tipo_unidad'                        : vv_tipo_unidad,
                'nro_tanque'                         : str(self.ui.lineEdit_5.text()),

                # 'Clasific_tanque': str(self.ui.lineEdit_20.text()),

                'vehiculo_marca'                     : str(self.ui.lineEdit_25.text()),
                'vehiculo_modelo'                    : str(self.ui.lineEdit_30.text()),
                'vehiculo_ano_fab'                   : str(self.ui.lineEdit_31.text()),
                'vehiculo_serie'                     : str(self.ui.lineEdit_32.text()),
                'vehiculo_placa'                     : vv_placa_vehiculo,
                'vehiculo_ejes'                      : str(self.ui.lineEdit_33.text()),
                'vehiculo_vin'                       : str(self.ui.lineEdit_34.text()),

                'tanque_marca'                       : str(self.ui.lineEdit_21.text()),
                'tanque_modelo'                      : str(self.ui.lineEdit_22.text()),
                'tanque_ano_fab'                     : str(self.ui.lineEdit_23.text()),
                'tanque_serie'                       : str(self.ui.lineEdit_24.text()),
                'tanque_placa'                       : vv_placa_cisterna,
                'tanque_ejes'                        : str(self.ui.lineEdit_26.text()) if vv_tipo_unidad!='CAMION TANQUE' else '-',
                'tanque_vin'                         : str(self.ui.lineEdit_27.text()) if vv_tipo_unidad!='CAMION TANQUE' else '-',

                'capacidad_nominal_lt'               : str(self.ui.lineEdit_11.text()),
                'capacidad_nominal_gl'               : str(self.ui.lineEdit_29.text()),
                'total_compartimentos'               : str(self.ui.lineEdit_10.text()),

                'img_frente'                         : vv_img_frente,
                'img_perfil'                         : vv_img_perfil,
                'fecha_Veri'                         : str(vv_fecha_verificacion),
                'lugar_Veri'                         : str(self.ui.lineEdit_17.text()),
                'Doc_normativo'                      : str(self.ui.plainTextEdit.toPlainText()),

                'trazabilidad'                       : dic_trazabilidad,
                'condi_generales'                    : dic_ensayos_exclusivos,
                'mit'                                : dic_veri_ini_post,
                'met'                                : dic_met,
                'resultados'                         : dic_error_max,

                'tipo_verificacion'                  : vv_tipo_verificacion,
                'det_NoConformidad_ExclusivosVeriIni': str(self.ui.plainTextEdit_8.toPlainText()),
                'det_NoConformidad_VeriIniPost'      : str(self.ui.plainTextEdit_6.toPlainText()),
                'det_NoConformidad_ErrorMaxPermis'   : str(self.ui.plainTextEdit_7.toPlainText()),
                'obs'                                : str(self.ui.plainTextEdit_3.toPlainText()),

                'temperatura'                        : str(self.ui.doubleSpinBox.value()),
                # 'obs_condi_generales':str(self.ui.plainTextEdit_3.toPlainText()),
                # 'obs_condi_tecnicas':str(self.ui.plainTextEdit_4.toPlainText()),
                'conclusion'                         : str(self.ui.plainTextEdit_2.toPlainText()),
                'inspector'                          : vv_nom_inspector,
                'responsable'                        : vv_nom_responsable,

                'MET_op_one'                         : str(self.ui.doubleSpinBox_2.value()),
                'MET_op_two'                         : str(self.ui.doubleSpinBox_3.value()),
                'OT'                                 : str(self.ui.groupBox_8.statusTip()),

                'hora inicio'                        : v_hora_ini,
                'hora fin'                           : v_hora_fin,
                'estado conclusion'                  : estado_conclusion,
                'textQR': textQR
        }

        # try:

        # from Export_pdf_V5 import Export_pdf_V5
        from plugins_Certificados.verificacionCubicacion.Export_pdf_V5 import Export_pdf_V5
        # Export_pdf.tarjeta_Cab_Pdf(vv_name_file_pdf,data_tarjeta_cab,data_tarjeta_det)
        v_ceros = '0' * (4 - len(str(self.ui.lineEdit_16.text())))
        v_name_path = str(self.ui.lineEdit_15.text()) + "-" + str(v_ceros) + str(self.ui.lineEdit_16.text())
        v_msg = Export_pdf_V5.hoja_certificacion(v_name_path, data_certificado, sw_firma, True)
        msgbox = QtWidgets.QMessageBox.information(self, 'Ventana Informativa', v_msg)

        # except NameError as error:
        #     msgbox = QtWidgets.QMessageBox.information(self, 'Ventana Informativa', error)

        #self.publicar_files()


    def hallar_CompartimientosQR(self, dic_veri_ini_post):
        Lista_AlturaLiquidoFinal = []

        for llave in sorted(dic_veri_ini_post.keys()):
            if llave in (8,):  # Colocar (,) decimal
                for i in range(11):
                    Lista_AlturaLiquidoFinal.append("C{}: {}".format(i + 1, str(dic_veri_ini_post[llave][i + 1]).replace('.', ',')))


        return Lista_AlturaLiquidoFinal
    

    def generar_tarjeta(self):

        # ==========================================================================================
        sql = "select count(*) from BLOQUEO_SYSTEM where sw_bloqueo=true  and cod_user<>" + str(main.VGuser_cod)
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        resultado = cur.fetchall()[0][0]
        cur.close()
        meCon.cerrarConexion()
        if int(resultado) > 0:
            msgbox = QtWidgets.QMessageBox.information(self, 'VENTANA INFORMATIVA','OPERACION NO VALIDA... SISTEMA BLOQUEADO')
            exit()

        # ==========================================================================================

        if self.ui.pushButton_3.isEnabled():
            msgbox = QtWidgets.QMessageBox.information(self, "Ventana Informativa", "OPERACION NO VALIDA")
            return

        # ===========================================================================
        if str(self.ui.pushButton_4.statusTip()).strip() != 'True':

            text, okPressed = QtWidgets.QInputDialog.getText(self, "Verificacion", "Ingrese su Contraseña",
                                                             QtWidgets.QLineEdit.Password, "")
            if not okPressed or self.ui.comboBox_5.statusTip() != text:
                msgbox = QtWidgets.QMessageBox.information(self, "Ventana Informativa", "OPERACION NO VALIDA")
                return

        self.ui.pushButton_4.setStatusTip('True')
        self.ui.pushButton_5.setStatusTip('True')
        self.ui.pushButton_12.setStatusTip('True')

        # ===========================================================================

        if not self.ui.checkBox_2.checkState():
            msgbox = QtWidgets.QMessageBox.question(self, 'VENTANA CONFIRMACION', 'SEGURO GENERAR TARJETA ?',
                                                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                    QtWidgets.QMessageBox.No)
            if msgbox != QtWidgets.QMessageBox.Yes:
                # msgbox = QtWidgets.QMessageBox.information(self, "Ventana Informativa", "OPERACION NO PERMITIDO...\nEL DOCUMENTO ESTA DESAPROBADO")
                return

        # ==================================================================================================
        dic_veri_ini_post = {}
        for fila in range(self.ui.tableWidget_6.rowCount()):
            vcomp = self.ui.tableWidget_6.item(fila, 0).text()
            vcomp_1 = "" if str(self.ui.tableWidget_6.item(fila, 1)) == 'None' else self.ui.tableWidget_6.item(fila, 1).text()
            vcomp_2 = "" if str(self.ui.tableWidget_6.item(fila, 2)) == 'None' else self.ui.tableWidget_6.item(fila, 2).text()
            vcomp_3 = "" if str(self.ui.tableWidget_6.item(fila, 3)) == 'None' else self.ui.tableWidget_6.item(fila, 3).text()
            vcomp_4 = "" if str(self.ui.tableWidget_6.item(fila, 4)) == 'None' else self.ui.tableWidget_6.item(fila, 4).text()
            vcomp_5 = "" if str(self.ui.tableWidget_6.item(fila, 5)) == 'None' else self.ui.tableWidget_6.item(fila, 5).text()
            vcomp_6 = "" if str(self.ui.tableWidget_6.item(fila, 6)) == 'None' else self.ui.tableWidget_6.item(fila, 6).text()
            vcomp_7 = "" if str(self.ui.tableWidget_6.item(fila, 7)) == 'None' else self.ui.tableWidget_6.item(fila, 7).text()
            vcomp_8 = "" if str(self.ui.tableWidget_6.item(fila, 8)) == 'None' else self.ui.tableWidget_6.item(fila, 8).text()
            vcomp_9 = "" if str(self.ui.tableWidget_6.item(fila, 9)) == 'None' else self.ui.tableWidget_6.item(fila, 9).text()
            vcomp_10 = "" if str(
                self.ui.tableWidget_6.item(fila, 10)) == 'None' else self.ui.tableWidget_6.item(fila, 10).text()
            vcomp_11 = "" if str(
                self.ui.tableWidget_6.item(fila, 11)) == 'None' else self.ui.tableWidget_6.item(fila, 11).text()

            dic_veri_ini_post[fila] = (vcomp, vcomp_1, vcomp_2, vcomp_3, vcomp_4, vcomp_5, vcomp_6, vcomp_7, vcomp_8, vcomp_9, vcomp_10,vcomp_11)

        # ==================================================================================================

        emi_day = "00"[:2 - len(str(self.ui.dateEdit.date().day()))] + str(self.ui.dateEdit.date().day())
        emi_month = "00"[:2 - len(str(self.ui.dateEdit.date().month()))] + str(self.ui.dateEdit.date().month())
        emi_year = str(self.ui.dateEdit.date().year())
        emi_fecha = emi_year + "-" + emi_month + "-" + emi_day

        exp_day = "00"[:2 - len(str(self.ui.dateEdit_2.date().day()))] + str(self.ui.dateEdit_2.date().day())
        exp_month = "00"[:2 - len(str(self.ui.dateEdit_2.date().month()))] + str(self.ui.dateEdit_2.date().month())
        exp_year = str(self.ui.dateEdit_2.date().year())
        exp_fecha = exp_year + "-" + exp_month + "-" + exp_day

        vv_cod_random = ""  # str(self.ui.lineEdit_12.text())

        emp_logo = "HD_logotipo.jpg"
        print(emp_logo)

        vv_img_frente = self.ui.label_img_frente.statusTip()
        vv_img_perfil = self.ui.label_img_trasera.statusTip()

        # if str(self.ui.comboBox_6.currentText()).strip()=='CAMION TANQUE':
        #     vplaca_remolcador = ""
        #     vplaca_semiremolqu = ""
        #     vplaca_camion_cisterna = str(self.ui.comboBox_7.currentText())
        # else:

        if str(self.ui.comboBox_6.currentText()).strip() == 'CAMION TANQUE':
            # vplaca_remolcador = ""  # Tracto
            vplaca_remolcador = str(self.ui.comboBox_7.currentText())  # Tracto
            vplaca_semiremolqu = str(self.ui.comboBox_8.currentText())  # Vehiculo tanque
        else:
            vplaca_remolcador = str(self.ui.comboBox_7.currentText())  # Tracto
            vplaca_semiremolqu = str(self.ui.comboBox_8.currentText())  # Vehiculo tanque
        vplaca_camion_cisterna = ""

        v_ceros = '0' * (4 - len(str(self.ui.lineEdit_16.text())))


        estado_conclusion = 'CONFORME' if self.ui.checkBox_2.isChecked() else 'NO CONFORME'
        vv_tipo_verificacion = "INICIAL" if self.ui.radioButton.isChecked() else "POSTERIOR"

        textQR = (
            "{}: {}".format("Numero de Certificado", str(self.ui.lineEdit_15.text()) + '-' + str(v_ceros) + str(self.ui.lineEdit_16.text())),
            "{}: {}".format("Cliente", str(self.ui.lineEdit.text()).replace("&", " & ")),
            "{}: {}".format("Producto o Servicio", "Tanques de carga montados sobre vehículos automotrices y semirremolques destinados al transporte de productos derivados de petróleo. Verificación de tanques de carga (Verificación inicial y posterior)"),
            "{}: {}".format("Código Nace", "28.21 Fabricación de cisternas, grandes depósitos y contenedores de metal"),
            "{}: {}".format("Fecha de Emisión", str(emi_fecha)),
            "{}: {}".format("Tipo de Verificación", vv_tipo_verificacion),
            "{}: {}".format("Resultado de la Inspección", estado_conclusion),
            "{}: {}".format("Altura de Liquido Final", ' - '.join(self.hallar_CompartimientosQR(dic_veri_ini_post)))
            )
        

        data_tarjeta_cab = {'logo_emp'             : emp_logo, 'direccion': emp_dir, 'tlf': emp_tlf,
                            'correo'               : emp_correo,
                            'serie'                : str(self.ui.lineEdit_15.text()),
                            'nro'                  : v_ceros + str(self.ui.lineEdit_16.text()),
                            'codigo_random'        : vv_cod_random,
                            'oficio'               : "ORGANISMO DE INSPECCION",
                            'nro_cisterna'         : str(self.ui.lineEdit_5.text()), 'fecha_emi': emi_fecha,
                            'fecha_expiracion'     : exp_fecha,
                            'clie_r_social'        : str(self.ui.lineEdit.text()),
                            'clie_dir'             : str(self.ui.lineEdit_6.text()),
                            'clie_ruc'             : str(self.ui.lineEdit_18.text()),
                            'placa_remolcador'     : vplaca_remolcador, 'placa_semiremolque': vplaca_semiremolqu,
                            'placa_camion_cisterna': vplaca_camion_cisterna,
                            'altura'               : str(self.ui.tableWidget_7.item(0, 2).text()).strip(),
                            'ancho'                : str(self.ui.tableWidget_7.item(0, 6).text()).strip(),
                            'largo'                : str(self.ui.tableWidget_7.item(0, 0).text()).strip(),
                            'img_frente'           : vv_img_frente,

                            'textQR': textQR
                            }

        # diccionario_number={1:'1ero',2:'2 do',3:'3ero',4:'4 to',5:'5 to',6:'6 to',7:'7 mo',8:'8 vo'}
        diccionario_number = {1 : '1ero', 2: '2 do', 3: '3ero', 4: '4 to', 5: '5 to', 6: '6 to', 7: '7 mo', 8: '8 vo',
                              9 : '9 no',
                              10: '10 mo', 11: '11 vo'
                              }
        grid_list_compartimientos = []
        v_total_compartimentos = 0
        v_total_galones = 0
        v_total_litros = 0
        col = 1
        while col <= 11:
            if str(self.ui.tableWidget_5.item(0, col).text()).strip() != "":
                vtemp_capacidad_lt = str(format(float(self.ui.tableWidget_5.item(0, col).text()), '.2f')) + " L"
                vtemp_capacidad_gl = str(format(float(self.ui.tableWidget_5.item(1, col).text()), '.2f')) + " gal"
                # print("zzz")
                # print(str(self.ui.tableWidget_6.item(1,col).text()))
                valt_liquido = str(
                    format(float(str(self.ui.tableWidget_6.item(8, col).text()).replace(',', '.')), '.2f')).replace('.',
                                                                                                                    ',') + " cm"
                grid_list_compartimientos.append(
                        (diccionario_number[col], vtemp_capacidad_gl, vtemp_capacidad_lt, valt_liquido))
                v_total_compartimentos += 1
                v_total_litros = v_total_litros + float(self.ui.tableWidget_5.item(0, col).text())
                v_total_galones = v_total_galones + float(self.ui.tableWidget_5.item(1, col).text())

            col += 1

        data_tarjeta_det = {
                'list_compartimientos' : grid_list_compartimientos,
                'total_capacidad_gal'  : format(float(v_total_galones), '.2f') + ' gal',
                'total_capacidad_lit'  : format(float(v_total_litros), '.2f') + ' L',
                'total_compartimientos': str(v_total_compartimentos),
                'cubicado_por'         : str(self.ui.comboBox_4.currentText()),  # Gerente tecnico
                'autorizado_por'       : str(self.ui.comboBox_5.currentText()),
                'firma'                : str(self.ui.label_rubrica.statusTip()),  # Responsable tecnico
                'observaciones'        : str(self.ui.plainTextEdit_9.toPlainText()),
                'img_detras'           : vv_img_perfil,
                'a_la_fecha'           : "A LA FLECHA"  # str(self.ui.comboBox_3.currentText())
        }

        from plugins_Certificados.verificacionCubicacion.Export_pdf_V5 import Export_pdf_V5
        v_ceros = '0' * (4 - len(str(self.ui.lineEdit_16.text())))
        v_name_path = str(self.ui.lineEdit_15.text()) + "-" + str(v_ceros) + str(self.ui.lineEdit_16.text())
        Export_pdf_V5.tarjeta_Cab_Pdf(v_name_path, data_tarjeta_cab, data_tarjeta_det, True)

        #self.publicar_files()

    ##############################################################################################################################
    ##############################################################################################################################
    def actualizar(self):

        # ==========================================================================================
        sql = "select count(*) from BLOQUEO_SYSTEM where sw_bloqueo=true  and cod_user<>" + str(main.VGuser_cod)
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        resultado = cur.fetchall()[0][0]
        cur.close()
        meCon.cerrarConexion()
        if int(resultado) > 0:
            msgbox = QtWidgets.QMessageBox.information(self, 'VENTANA INFORMATIVA', 'OPERACION NO VALIDA... SISTEMA BLOQUEADO')
            exit()



        # ==========================================================================================
        # ----------------  VALIDANDO -----------
        # ---------------------------------------

        self.update_certificado_cab()

        sql = "delete from trazabilidad_correctivo where cod_certificado=%s;"  %(str(self.ui.lineEdit_15.statusTip()))
        # cur = main.con.cursor()
        # cur.execute(sql)
        # main.con.commit()

        sql += "delete from medidas_externas_tanque_correctivo where cod_certificado=%s;" %(str(self.ui.lineEdit_15.statusTip()))
        # cur = main.con.cursor()
        # cur.execute(sql)
        # main.con.commit()

        sql += "delete from ensayos_exclusivos_verif_ini_correctivo where cod_certificado=%s;" %(str(self.ui.lineEdit_15.statusTip()))
        # cur = main.con.cursor()
        # cur.execute(sql)
        # main.con.commit()

        sql += "delete from ensayos_veri_ini_posterior_correctivo where cod_certificado=%s;" %(str(self.ui.lineEdit_15.statusTip()))
        # cur = main.con.cursor()
        # cur.execute(sql)
        # main.con.commit()

        sql += "delete from error_max_permisible_correctivo where cod_certificado=%s;" %(str(self.ui.lineEdit_15.statusTip()))
        # cur = main.con.cursor()
        # cur.execute(sql)
        # main.con.commit()

        sql += "delete from cubicacion_rpt_inacal_correctivo where cod_certificado=%s;" %(str(self.ui.lineEdit_15.statusTip()))
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)
        # main.con.commit()
        meCon.meConexion.commit()

        cur.close()
        meCon.cerrarConexion()

        self.grabar_trazabilidad()
        self.grabar_MET()
        self.grabar_ensayo_exclusivos_ini()
        self.grabar_ensayos_veri_ini()  # caracteristicas tecnicas
        self.grabar_error_max()  # Error Maximo Permisible

        self.grabar_RptInacal() #datos Rpte INACAL


        self.ui.pushButton_3.setEnabled(False)
        QtWidgets.QMessageBox.information(self, "Ventana Informativa", "OPERACION CORRECTA")





    def update_certificado_cab(self):
        import psycopg2

        vv_cod_empresa = "1"  # str(self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex()))
        vv_fecha_emi = str("%s-%s-%s" % (self.ui.dateEdit.date().year(), self.ui.dateEdit.date().month(), self.ui.dateEdit.date().day()))
        vv_fecha_exp = str("%s-%s-%s" % (self.ui.dateEdit_2.date().year(), self.ui.dateEdit_2.date().month(), self.ui.dateEdit_2.date().day()))
        vv_cod_vehiculo = str(self.ui.comboBox_7.itemData(self.ui.comboBox_7.currentIndex()))
        vv_placa_vehiculo = str(self.ui.comboBox_7.itemText(self.ui.comboBox_7.currentIndex()))
        vv_cod_cisterna = str(self.ui.comboBox_8.itemData(self.ui.comboBox_8.currentIndex()))
        vv_placa_cisterna = str(self.ui.comboBox_8.itemText(self.ui.comboBox_8.currentIndex()))

        vv_cod_inspector = str(self.ui.comboBox_4.itemData(self.ui.comboBox_4.currentIndex()))  # gerente Tecnico
        vv_cod_responsable = str(self.ui.comboBox_5.itemData(self.ui.comboBox_5.currentIndex()))  # Responsable supervisor

        vv_img_frente = open(self.ui.label_img_frente.statusTip(), 'rb').read()
        vv_img_perfil = open(self.ui.label_img_trasera.statusTip(), 'rb').read()

        vv_tipo_verificacion = "INICIAL" if self.ui.radioButton.isChecked() else "POSTERIOR"

        vv_estado = "APROBADO" if self.ui.checkBox_2.isChecked() else "DESAPROBADO"

        vv_fecha_verificacion = str("%s-%s-%s" % (self.ui.dateEdit_3.date().year(), self.ui.dateEdit_3.date().month(), self.ui.dateEdit_3.date().day()))

        vv_tipo_certificado = self.obtener_tipo_certificado()

        v_hora_ini = str(self.ui.timeEdit.time().hour()) + ":" + str(self.ui.timeEdit.time().minute()) + ":00"
        v_hora_fin = str(self.ui.timeEdit_2.time().hour()) + ":" + str(self.ui.timeEdit_2.time().minute()) + ":00"
        v_Version = "05"

        sql = "update certificados_correctivo set  "
        sql = sql + " cod_vehiculo=%s,"  # 0
        sql = sql + " cod_cisterna=%s,"  # 1
        sql = sql + " cod_clie=%s,"  # 2
        sql = sql + " cod_usuario=%s,"  # 3
        sql = sql + " cod_venta=%s,"  # 4
        sql = sql + " registro_cubicacion_serie=%s,"  # 5
        sql = sql + " registro_cubicacion_nro=%s,"  # 6
        sql = sql + " fecha_emision=%s,"  # 7
        sql = sql + " fecha_expiracion=%s,"  # 8
        sql = sql + " estado=%s,"  # 9
        sql = sql + " tipo_verificacion=%s,"  # 10
        sql = sql + " r_social=%s,"  # 11
        sql = sql + " ruc=%s,"  # 12
        sql = sql + " direccion=%s,"  # 13
        sql = sql + " tipo_vehiculo=%s,"  # 14
        sql = sql + " marca_vehiculo=%s,"  # 15
        sql = sql + " modelo_vehiculo=%s,"  # 16
        sql = sql + " fab_vehiculo=%s,"  # 17
        sql = sql + " serie_vehiculo=%s,"  # 18
        sql = sql + " placa_vehiculo=%s,"  # 19
        sql = sql + " ejes_vehiculo=%s,"  # 20
        sql = sql + " vin_vehiculo=%s,"  # 21
        sql = sql + " cod_tanque=%s,"  # 22
        sql = sql + " marca_tanque=%s,"  # 23
        sql = sql + " modelo_tanque=%s,"  # 24
        sql = sql + " fab_tanque=%s,"  # 25
        sql = sql + " serie_tanque=%s,"  # 26
        sql = sql + " placa_tanque=%s,"  # 27
        sql = sql + " ejes_tanque=%s,"  # 28
        sql = sql + " vin_tanque=%s,"  # 29     nro_cisterna = cod tanque
        sql = sql + " capacidad_nominal_litros=%s, "  # 30
        sql = sql + " capacidad_nominal_galones=%s,"  # 31
        sql = sql + " total_compartimentos=%s,"  # 32
        sql = sql + " img_frente=%s,"  # 33
        sql = sql + " img_perfil=%s,"  # 34
        sql = sql + " lugar_verif=%s,"  # 35
        sql = sql + " fecha_verif=%s,"  # 36
        sql = sql + " Doc_normativo_proc_verif=%s,"  # 37
        sql = sql + " det_no_conformidad_ensayos_exclusivos=%s,"  # 38
        sql = sql + " det_no_conformidad_ensayos_verif_ini=%s,"  # 39
        sql = sql + " det_no_conformidad_error_max_permisible=%s,"  # 40
        sql = sql + " conclusion=%s,"  # 41
        sql = sql + " observaciones_certificado=%s,"  # 42
        sql = sql + " responsable=%s,"  # 43

        sql = sql + " inspector=%s,"  # 44

        sql = sql + " temperatura=%s,"  # 45
        sql = sql + " cod_capicua=%s,"  # 46    2 campos
        sql = sql + " observaciones_tarjeta=%s,"  # 47     1 campos
        sql = sql + " estado_registro='ACTUALIZACION', "  # 47     1 campos

        sql = sql + " met_op_one=%s, "  # 48     1 met_op_one
        sql = sql + " met_op_two=%s, "  # 49     1 met_op_two

        sql = sql + " tipo_certificado=%s, "  # 50

        sql = sql + " hora_inicio=%s, "  # 51
        sql = sql + " hora_fin=%s, "  # 52
        sql = sql + " version_certificado=%s, "  # 53

        sql = sql + " obs_checklist=%s, "  # 54
        sql = sql + " obs_inspeccion=%s "  # 55

        sql = sql + " where cod_certificado=%s "  # 48

        lista_valores = (str(vv_cod_vehiculo),  # cod_vehiculo * 0
                         str(vv_cod_cisterna),  # cod_cisterna * 1
                         str(self.ui.lineEdit.statusTip()),  # cod_clie * 2
                         str(main.VGuser_cod),  # cod_usuario * 3
                         str(self.ui.groupBox_2.statusTip()),  # cod_venta * 4

                         str(self.ui.lineEdit_15.text()),  # registro_cubicacion_serie * 5
                         str(self.ui.lineEdit_16.text()),  # registro_cubicacion_nro * 6
                         str(vv_fecha_emi),  # fecha_emision * 7
                         str(vv_fecha_exp),  # fecha_expiracion * 8
                         str(vv_estado),  # estado * 9
                         vv_tipo_verificacion,  # * 10
                         str(self.ui.lineEdit.text()),  # r_social * 11
                         str(self.ui.lineEdit_18.text()),  # ruc * 12
                         str(self.ui.lineEdit_6.text()),  # direccion * 13

                         str(self.ui.comboBox_6.itemText(self.ui.comboBox_6.currentIndex())),  # tipo_vehiculo * 14
                         str(self.ui.lineEdit_25.text()),  # marca_vehiculo * 15
                         str(self.ui.lineEdit_30.text()),  # modelo_vehiculo * 16
                         str(self.ui.lineEdit_31.text()),  # fab_vehiculo * 17
                         str(self.ui.lineEdit_32.text()),  # serie_vehiculo * 18
                         str(vv_placa_vehiculo),  # placa_vehiculo * 19
                         str(self.ui.lineEdit_33.text()),  # ejes_vehiculo * 20
                         str(self.ui.lineEdit_34.text()),  # vin_vehiculo * 21

                         str(self.ui.lineEdit_5.text()),  # nro_cisterna o codigo de tanque * 22
                         str(self.ui.lineEdit_21.text()),  # marca_tanque * 23
                         str(self.ui.lineEdit_22.text()),  # modelo_tanque * 24
                         str(self.ui.lineEdit_23.text()),  # fab_tanque * 25
                         str(self.ui.lineEdit_24.text()),  # serie_tanque * 26
                         str(vv_placa_cisterna),  # placa_tanque * 27
                         str(self.ui.lineEdit_26.text()),  # ejes_tanque * 28
                         str(self.ui.lineEdit_27.text()),  # vin_tanque * 29

                         str(self.ui.lineEdit_11.text()),  # capacidad_nominal_litros * 30
                         str(self.ui.lineEdit_29.text()),  # capacidad_nominal_galones * 31
                         str(self.ui.lineEdit_10.text()),  # total_compartimentos * 32

                         psycopg2.Binary(vv_img_frente),  # img_frente * 33
                         psycopg2.Binary(vv_img_perfil),  # img_perfil * 34

                         str(self.ui.lineEdit_17.text()),  # lugar_verif * 35
                         str(vv_fecha_verificacion),  # fecha_verif * 36

                         str(self.ui.plainTextEdit.toPlainText()),  # doc_normativo_proc_verif * 37

                         str(self.ui.plainTextEdit_8.toPlainText()),
                         # det_no_conformidad_ensayos exclusivos_verifi * 38
                         str(self.ui.plainTextEdit_6.toPlainText()),  # det_no_conformidad_ensayos_veri_ini, * 39
                         str(self.ui.plainTextEdit_7.toPlainText()),  # det_no_conformidad_error_max_permisible, * 40

                         str(self.ui.plainTextEdit_2.toPlainText()),  # conclusion * 41
                         str(self.ui.plainTextEdit_3.toPlainText()),  # observaciones_certificado * 42
                         str(vv_cod_responsable),  # responsable * 43
                         str(vv_cod_inspector),  # inspector * 44

                         str(self.ui.doubleSpinBox.value()),  # temperatura * 45
                         "",  # cod_capicua * 46
                         str(self.ui.plainTextEdit_9.toPlainText()),  # observaciones_tarjeta * 47

                         str(self.ui.doubleSpinBox_2.value()),  # met_op_one * 47
                         str(self.ui.doubleSpinBox_3.value()),  # met_op_two * 47

                         vv_tipo_certificado,  # tipo_certificado * 48

                         v_hora_ini,
                         v_hora_fin,
                         v_Version,

                         str(self.ui.plainTextEdit_4.toPlainText()),  # OBS CHECLIST
                         str(self.ui.plainTextEdit_5.toPlainText()),  # OBS RPT INSPECCION

                         str(self.ui.lineEdit_15.statusTip())  # cod certificado 48

                         )
        print(len(lista_valores))

        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql, lista_valores)
        # main.con.commit()
        meCon.meConexion.commit()

        cur.close()
        meCon.cerrarConexion()


    def obtener_tipo_certificado(self):
        if self.ui.radioButton_3.isChecked():
            return "RENOVACION"

        if self.ui.radioButton_4.isChecked():
            return "PRESCENCIAL"

    def grabar_certificado_cab(self):
        import psycopg2

        vv_cod_empresa = "1"  # str(self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex()))
        vv_fecha_emi = str("%s-%s-%s" % (self.ui.dateEdit.date().year(), self.ui.dateEdit.date().month(), self.ui.dateEdit.date().day()))
        vv_fecha_exp = str("%s-%s-%s" % (self.ui.dateEdit_2.date().year(), self.ui.dateEdit_2.date().month(), self.ui.dateEdit_2.date().day()))
        vv_cod_vehiculo = str(self.ui.comboBox_7.itemData(self.ui.comboBox_7.currentIndex()))
        vv_placa_vehiculo = str(self.ui.comboBox_7.itemText(self.ui.comboBox_7.currentIndex()))
        vv_cod_cisterna = str(self.ui.comboBox_8.itemData(self.ui.comboBox_8.currentIndex()))
        vv_placa_cisterna = str(self.ui.comboBox_8.itemText(self.ui.comboBox_8.currentIndex()))

        vv_cod_inspector = str(self.ui.comboBox_4.itemData(self.ui.comboBox_4.currentIndex()))  # Gerente Tecnico
        vv_cod_responsable = str(self.ui.comboBox_5.itemData(self.ui.comboBox_5.currentIndex()))  # Responsable supervisor tecnico

        vv_img_frente = open(self.ui.label_img_frente.statusTip(), 'rb').read()
        vv_img_perfil = open(self.ui.label_img_trasera.statusTip(), 'rb').read()

        vv_tipo_verificacion = "INICIAL" if self.ui.radioButton.isChecked() else "POSTERIOR"

        vv_estado = "APROBADO" if self.ui.checkBox_2.isChecked() else "DESAPROBADO"

        vv_fecha_verificacion = str("%s-%s-%s" % (self.ui.dateEdit_3.date().year(), self.ui.dateEdit_3.date().month(), self.ui.dateEdit_3.date().day()))

        vv_MET_op_one = str(self.ui.doubleSpinBox_2.value())
        vv_MET_op_two = str(self.ui.doubleSpinBox_3.value())

        vv_tipo_certificado = self.obtener_tipo_certificado()

        v_hora_ini = str(self.ui.timeEdit.time().hour()) + ":" + str(self.ui.timeEdit.time().minute()) + ":00"
        v_hora_fin = str(self.ui.timeEdit_2.time().hour()) + ":" + str(self.ui.timeEdit_2.time().minute()) + ":00"
        v_Version = "05"

        sql = "insert into certificados("
        sql = sql + " cod_empresa,cod_vehiculo,cod_cisterna,cod_clie,cod_usuario,cod_venta,"  # 6campos
        sql = sql + " registro_cubicacion_serie,registro_cubicacion_nro,fecha_emision,fecha_expiracion,estado,tipo_verificacion,"  # 6 campos
        sql = sql + " r_social,ruc,direccion,tipo_vehiculo,"  # 4 campos
        sql = sql + " marca_vehiculo,modelo_vehiculo, fab_vehiculo,serie_vehiculo,placa_vehiculo,ejes_vehiculo,vin_vehiculo,"  # 7 campos
        sql = sql + " cod_tanque,marca_tanque,modelo_tanque,fab_tanque,serie_tanque,placa_tanque,ejes_tanque,vin_tanque,"  # nro_cisterna = cod tanque - 8 campos
        sql = sql + " capacidad_nominal_litros, capacidad_nominal_galones, total_compartimentos,"  # 3 campos
        sql = sql + " img_frente,img_perfil,"  # 2 campos
        sql = sql + " lugar_verif,fecha_verif,Doc_normativo_proc_verif,"  # 3 campos
        sql = sql + " det_no_conformidad_ensayos_exclusivos, det_no_conformidad_ensayos_verif_ini, det_no_conformidad_error_max_permisible,"  # 3 campos
        sql = sql + " conclusion,observaciones_certificado,"  # 2 campos
        # sql = sql + " responsable,inspector," # 2 campos
        sql = sql + " responsable,"  # 2 campos
        sql = sql + " inspector,"  # 2 campos
        sql = sql + " temperatura, cod_capicua, "  # 2 campos
        sql = sql + " observaciones_tarjeta, "  # 1 campos
        # sql = sql + " condiciones_generales,condiciones_tecnicas,"
        sql = sql + " estado_registro, "  # 1 campos

        sql = sql + " met_op_one, "  # vv_MET_op_one
        sql = sql + " met_op_two, "  # vv_MET_op_two

        sql = sql + " tipo_certificado, "  # vv_MET_op_two

        sql = sql + " hora_inicio, "  #
        sql = sql + " hora_fin, "  #
        sql = sql + " version_certificado "  #

        sql = sql + " ) "

        print(sql)

        sql = sql + "values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
        sql = sql + "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
        sql = sql + "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
        sql = sql + "%s, %s, %s,"
        sql = sql + "%s, %s, %s);"

        print(vv_cod_responsable)

        lista_valores = (str(vv_cod_empresa),  # *
                         str(vv_cod_vehiculo),  # cod_vehiculo *
                         str(vv_cod_cisterna),  # cod_cisterna *
                         str(self.ui.lineEdit.statusTip()),  # cod_clie *
                         str(main.VGuser_cod),  # cod_usuario *
                         str(self.ui.groupBox_2.statusTip()),  # cod_venta *

                         str(self.ui.lineEdit_15.text()),  # registro_cubicacion_serie *
                         str(self.ui.lineEdit_16.text()),  # registro_cubicacion_nro *
                         str(vv_fecha_emi),  # fecha_emision *
                         str(vv_fecha_exp),  # fecha_expiracion *
                         str(vv_estado),  # estado *
                         vv_tipo_verificacion,  # *
                         str(self.ui.lineEdit.text()),  # r_social *
                         str(self.ui.lineEdit_18.text()),  # ruc *
                         str(self.ui.lineEdit_6.text()),  # direccion *

                         str(self.ui.comboBox_6.itemText(self.ui.comboBox_6.currentIndex())),  # tipo_vehiculo *
                         str(self.ui.lineEdit_25.text()),  # marca_vehiculo *
                         str(self.ui.lineEdit_30.text()),  # modelo_vehiculo *
                         str(self.ui.lineEdit_31.text()),  # fab_vehiculo *
                         str(self.ui.lineEdit_32.text()),  # serie_vehiculo *
                         str(vv_placa_vehiculo),  # placa_vehiculo *
                         str(self.ui.lineEdit_33.text()),  # ejes_vehiculo *
                         str(self.ui.lineEdit_34.text()),  # vin_vehiculo *

                         str(self.ui.lineEdit_5.text()),  # nro_cisterna o codigo de tanque *
                         str(self.ui.lineEdit_21.text()),  # marca_tanque *
                         str(self.ui.lineEdit_22.text()),  # modelo_tanque *
                         str(self.ui.lineEdit_23.text()),  # fab_tanque *
                         str(self.ui.lineEdit_24.text()),  # serie_tanque *
                         str(vv_placa_cisterna),  # placa_tanque *
                         str(self.ui.lineEdit_26.text()),  # ejes_tanque *
                         str(self.ui.lineEdit_27.text()),  # vin_tanque *

                         str(self.ui.lineEdit_11.text()),  # capacidad_nominal_litros *
                         str(self.ui.lineEdit_29.text()),  # capacidad_nominal_galones *
                         str(self.ui.lineEdit_10.text()),  # total_compartimentos *

                         psycopg2.Binary(vv_img_frente),  # img_frente *
                         psycopg2.Binary(vv_img_perfil),  # img_perfil *

                         str(self.ui.lineEdit_17.text()),  # lugar_verif *
                         str(vv_fecha_verificacion),  # fecha_verif *

                         str(self.ui.plainTextEdit.toPlainText()),  # doc_normativo_proc_verif *

                         str(self.ui.plainTextEdit_8.toPlainText()),  # det_no_conformidad_ensayos exclusivos_verifi *
                         str(self.ui.plainTextEdit_6.toPlainText()),  # det_no_conformidad_ensayos_veri_ini, *
                         str(self.ui.plainTextEdit_7.toPlainText()),  # det_no_conformidad_error_max_permisible, *

                         str(self.ui.plainTextEdit_2.toPlainText()),  # conclusion *
                         str(self.ui.plainTextEdit_3.toPlainText()),  # observaciones_certificado *
                         str(vv_cod_responsable),  # responsable *
                         str(vv_cod_inspector),  # inspector *

                         str(self.ui.doubleSpinBox.value()),  # temperatura *
                         '',  # cod_capicua *
                         str(self.ui.plainTextEdit_9.toPlainText()),  # observaciones_tarjeta *
                         "CREACION",  # estado de registro *
                         vv_MET_op_one,
                         vv_MET_op_two,

                         vv_tipo_certificado,

                         v_hora_ini,
                         v_hora_fin,
                         v_Version

                         # str(self.ui.lineEdit_20.text()),#clasificacion_tanque
                         # '',  #str(self.ui.plainTextEdit_3.toPlainText()),# condiciones_generales
                         # '',  #str(self.ui.plainTextEdit_4.toPlainText()),# condiciones_tecnicas
                         )

        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql, lista_valores)
        # main.con.commit()
        meCon.meConexion.commit()

        # HALLANDO CODIGO COMPRA ASIGNADO AUTO
        # cur = main.con.cursor()
        cur = meCon.meConexion.cursor()
        cur.execute("select currval(pg_get_serial_sequence('certificados', 'cod_certificado'));")
        for row in cur.fetchall():
            vv_cod_certificado = str(row[0])

        cur.close()
        meCon.cerrarConexion()


        return vv_cod_certificado


    def grabar_trazabilidad(self):

        vv_cod_empresa = "1"  # str(self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex()))

        meCon = conexion()


        for fila in range(self.ui.tableWidget_2.rowCount()):
            vcod_equipo = str(self.ui.tableWidget_2.item(fila, 0).text())
            vide_equipo = str(self.ui.tableWidget_2.item(fila, 1).text())
            vnom_equipo = str(self.ui.tableWidget_2.item(fila, 2).text())
            vcer_equipo = str(self.ui.tableWidget_2.item(fila, 3).text())
            vfecha_calibracion = str(self.ui.tableWidget_2.item(fila, 4).text())

            sql = "insert into trazabilidad_correctivo(cod_certificado,cod_empresa,item,cod_equipo,id_equipo,nombre_equipo,certificado_calibracion,fecha_calibracion) "
            sql = sql + "values(%s,%s,%s,%s,%s,%s,%s,%s)"

            lista_valores = (self.ui.lineEdit_15.statusTip(), vv_cod_empresa, fila, vcod_equipo, vide_equipo, vnom_equipo, vcer_equipo,vfecha_calibracion)

            # cur = main.con.cursor()
            cur = meCon.meConexion.cursor()
            cur.execute(sql, lista_valores)
            # main.con.commit()
            meCon.meConexion.commit()

        cur.close()
        meCon.cerrarConexion()


    # def grabar_certificado_CondiGenerales(self):#Condiciones Generales
    def grabar_ensayo_exclusivos_ini(self):  # Condiciones Generales

        vv_cod_empresa = "1"  # str(self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex()))

        meCon = conexion()


        for fila in range(self.ui.tableWidget_9.rowCount()):

            for col in range(11):
                if str(self.ui.tableWidget_9.item(fila, col + 1)) != 'None' and str(
                        self.ui.tableWidget_9.item(fila, col + 1).text()) != '':
                    # if self.ui.tableWidget_9.item(fila, col+1).checkState() == QtCore.Qt.Checked:

                    vitem = fila
                    vnro_compart = col + 1
                    vvalor = str(self.ui.tableWidget_9.item(fila, col + 1).text())
                    # vvalor = True #if self.ui.tableWidget.item(fila, col+1).checkState() == QtCore.Qt.Checked else False
                    # print(self.ui.tableWidget.item(fila, col + 1).checkState())

                    sql = "insert into ensayos_exclusivos_verif_ini_correctivo(cod_certificado,cod_empresa,item,nro_compartimento,valor) "
                    sql = sql + "values(%s,%s,%s,%s,%s)"

                    lista_valores = (self.ui.lineEdit_15.statusTip(), vv_cod_empresa, vitem, vnro_compart, vvalor)

                    # cur = main.con.cursor()
                    cur = meCon.meConexion.cursor()
                    cur.execute(sql, lista_valores)
                    # main.con.commit()
                    meCon.meConexion.commit()


        cur.close()
        meCon.cerrarConexion()

    # def grabar_certificado_MIT(self):  # Caracteristicas Tecnicas
    def grabar_ensayos_veri_ini(self):  # Caracteristicas Tecnicas

        vv_cod_empresa = "1"  # str(self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex()))

        meCon = conexion()

        for fila in range(self.ui.tableWidget_6.rowCount()):

            for col in range(11):

                if str(self.ui.tableWidget_6.item(fila, col + 1)) != 'None' and str(
                        self.ui.tableWidget_6.item(fila, col + 1).text()) != '':
                    vitem = fila
                    vnro_compart = col + 1
                    vvalor = str(self.ui.tableWidget_6.item(fila, col + 1).text())

                    sql = "insert into ensayos_veri_ini_posterior_correctivo(cod_certificado,cod_empresa,item,nro_compartimento,valor) "
                    sql = sql + "values(%s,%s,%s,%s,%s)"

                    lista_valores = (self.ui.lineEdit_15.statusTip(), vv_cod_empresa, vitem, vnro_compart, vvalor)

                    # cur = main.con.cursor()
                    cur = meCon.meConexion.cursor()
                    cur.execute(sql, lista_valores)
                    # main.con.commit()
                    meCon.meConexion.commit()

        cur.close()
        meCon.cerrarConexion()



    # def grabar_certificado_MET(self):
    def grabar_MET(self):

        vv_cod_empresa = "1"  # str(self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex()))

        dic_tipo_scarga = {0: "a",
                           1: "b",
                           2: "c",
                           3: "d",
                           4: "e",
                           5: "f",
                           6: "g",
                           7: "x"
                           }

        dic_tipo_ccarga = {0: "d",
                           1: "e",
                           2: "x"
                           }

        meCon = conexion()


        for col in range(8):
            vtipo = "Sin carga"
            vid_medida = dic_tipo_scarga[col]
            vvalor = str(self.ui.tableWidget_7.item(0, col).text()).strip() if str(
                self.ui.tableWidget_7.item(0, col).text()).strip() != '' else 0

            sql = "insert into medidas_externas_tanque_correctivo(cod_certificado,cod_empresa,tipo,id_medida,valor) "
            sql = sql + "values(%s,%s,%s,%s,%s)"

            lista_valores = (self.ui.lineEdit_15.statusTip(), vv_cod_empresa, vtipo, vid_medida, vvalor)

            # cur = main.con.cursor()
            cur = meCon.meConexion.cursor()
            cur.execute(sql, lista_valores)
            # main.con.commit()
            meCon.meConexion.commit()

        for col in range(3):
            vtipo = "Con carga"
            vid_medida = dic_tipo_ccarga[col]
            vvalor = str(self.ui.tableWidget_8.item(0, col).text()).strip() if str(
                self.ui.tableWidget_8.item(0, col).text()).strip() != '' else 0

            sql = "insert into medidas_externas_tanque_correctivo(cod_certificado,cod_empresa,tipo,id_medida,valor) "
            sql = sql + "values(%s,%s,%s,%s,%s)"

            lista_valores = (self.ui.lineEdit_15.statusTip(), vv_cod_empresa, vtipo, vid_medida, vvalor)

            # cur = main.con.cursor()
            cur = meCon.meConexion.cursor()
            cur.execute(sql, lista_valores)
            # main.con.commit()
            meCon.meConexion.commit()

        cur.close()
        meCon.cerrarConexion()


    def grabar_error_max(self):  # Error Maximo Permisible

        vv_cod_empresa = "1"  # str(self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex()))

        meCon = conexion()

        for fila in range(self.ui.tableWidget_5.rowCount()):

            for col in range(11):

                if str(self.ui.tableWidget_5.item(fila, col + 1)) != 'None' and str(
                        self.ui.tableWidget_5.item(fila, col + 1).text()) != '':
                    vitem = fila
                    vnro_compart = col + 1
                    vvalor = str(self.ui.tableWidget_5.item(fila, col + 1).text())

                    sql = "insert into error_max_permisible_correctivo(cod_certificado,cod_empresa,item,nro_compartimento,valor) "
                    sql = sql + "values(%s,%s,%s,%s,%s)"

                    lista_valores = (self.ui.lineEdit_15.statusTip(), vv_cod_empresa, vitem, vnro_compart, vvalor)

                    # cur = main.con.cursor()
                    cur = meCon.meConexion.cursor()

                    cur.execute(sql, lista_valores)
                    # main.con.commit()
                    meCon.meConexion.commit()

        cur.close()
        meCon.cerrarConexion()



    def grabar_RptInacal(self):  # Error Maximo Permisible

        vv_cod_empresa = "1"  # str(self.ui.comboBox_2.itemData(self.ui.comboBox_2.currentIndex()))
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        for fila in range(self.ui.tableWidget_10.rowCount()):

            for col in range(11):

                if str(self.ui.tableWidget_10.item(fila, col + 1)) != 'None' and str(self.ui.tableWidget_10.item(fila, col + 1).text()) != '':
                    vitem = fila
                    vnro_compart = col + 1
                    vvalor = str(self.ui.tableWidget_10.item(fila, col + 1).text())

                    sql = "insert into cubicacion_rpt_inacal_correctivo(cod_certificado,cod_empresa,item,nro_compartimento,valor) "
                    sql = sql + "values(%s,%s,%s,%s,%s)"

                    lista_valores = (self.ui.lineEdit_15.statusTip(), vv_cod_empresa, vitem, vnro_compart, vvalor)


                    cur.execute(sql, lista_valores)
                    # main.con.commit()
                    meCon.meConexion.commit()

        cur.close()
        meCon.cerrarConexion()


    #######################################################################################################################
    #######################################################################################################################

    #######################################################################################################################
    ################################################# ADD ###########################################################
    #######################################################################################################################

    def add_img_frente(self):
        path_open = QtWidgets.QFileDialog.getOpenFileName(self, 'Seleccionar la Imagen de Frente', 'c:/', '(*.png)')
        print(path_open)
        self.ui.label_img_frente.setStatusTip(path_open[0])
        self.ui.label_img_frente.setPixmap(QtGui.QPixmap(path_open[0]))

    def add_img_trasera(self):
        path_open = QtWidgets.QFileDialog.getOpenFileName(self, 'Seleccionar la Imagen de Frente', 'c:/', '(*.png)')
        self.ui.label_img_trasera.setStatusTip(path_open[0])
        self.ui.label_img_trasera.setPixmap(QtGui.QPixmap(path_open[0]))

    ########################################## CREAR OBJETO #########################################

    # --------------------------------- para todas las Tablas -------------------------------
    def created_Object_doubleSpinBox(self, me_tableWidget, nro_decimals, sw_ColBloquear, nro_max):
        fila = me_tableWidget.currentRow()
        col = me_tableWidget.currentColumn()

        # ================================================ Bloqueo de compartimientos desde CAJA Nro. Comaprtimientos
        # if self.ui.lineEdit_10.text().strip() == '' or col > int(self.ui.lineEdit_10.text()):
        #     return

        # ================================================

        if col == sw_ColBloquear:
            return

        if str(me_tableWidget.item(fila, col)) == 'None':
            return

        v_txt = 0.00 if str(me_tableWidget.item(fila, col).text()).strip() == '' else float(
            me_tableWidget.item(fila, col).text())

        self.destroy_Object_all(me_tableWidget)

        self.tmp_txt_Double = QtWidgets.QDoubleSpinBox()
        self.tmp_txt_Double.setDecimals(nro_decimals)
        self.tmp_txt_Double.setMaximum(nro_max)
        self.tmp_txt_Double.setValue(v_txt)
        me_tableWidget.setCellWidget(fila, col, self.tmp_txt_Double)

        setValue_grid_Lambda = lambda: self.setValue_Object(me_tableWidget, fila, col, self.tmp_txt_Double.value())
        self.tmp_txt_Double.valueChanged.connect(setValue_grid_Lambda)

        destroy_Object_Lambda = lambda: self.destroy_Object_all(me_tableWidget)
        self.tmp_txt_Double.editingFinished.connect(destroy_Object_Lambda)

    def created_Object_texto(self, me_tableWidget):
        fila = me_tableWidget.currentRow()
        col = me_tableWidget.currentColumn()

        # ================================================ Bloqueo de compartimientos desde CAJA Nro. Comaprtimientos
        if self.ui.lineEdit_10.text().strip() == '' or col > int(self.ui.lineEdit_10.text()):
            return
        # ================================================

        if col == 0:
            return

        if str(me_tableWidget.item(fila, col)) == 'None':
            return

        v_txt = str(me_tableWidget.item(fila, col).text()).strip()

        self.destroy_Object_all(me_tableWidget)

        self.tmp_txt_texto = QtWidgets.QLineEdit()
        self.tmp_txt_texto.setText(v_txt)
        me_tableWidget.setCellWidget(fila, col, self.tmp_txt_texto)

        setValue_grid_Lambda = lambda: self.setValue_Object(me_tableWidget, fila, col, self.tmp_txt_texto.text())
        self.tmp_txt_texto.textChanged.connect(setValue_grid_Lambda)

        destroy_Object_Lambda = lambda: self.destroy_Object_all(me_tableWidget)
        self.tmp_txt_texto.editingFinished.connect(destroy_Object_Lambda)

    def created_Object_comboBox(self, lista_items, me_tableWidget):
        fila = me_tableWidget.currentRow()
        col = me_tableWidget.currentColumn()

        # ================================================ Bloqueo de compartimientos desde CAJA Nro. Comaprtimientos
        if self.ui.lineEdit_10.text().strip() == '' or col > int(self.ui.lineEdit_10.text()):
            return
        # ================================================

        if col == 0:
            return

        if str(me_tableWidget.item(fila, col)) == 'None':
            return

        v_txt = str(me_tableWidget.item(fila, col).text()).strip()

        self.destroy_Object_all(me_tableWidget)

        self.tmp_txt_comboBox = QtWidgets.QComboBox()
        for item in lista_items:
            self.tmp_txt_comboBox.addItem(item)

        n = 0
        while n <= self.tmp_txt_comboBox.count() - 1:  # lista
            if str(self.tmp_txt_comboBox.itemText(n)) == str(v_txt):
                self.tmp_txt_comboBox.setCurrentIndex(n)
            n = n + 1

        me_tableWidget.setCellWidget(fila, col, self.tmp_txt_comboBox)

        setValue_grid_Lambda = lambda: self.setValue_Object(me_tableWidget, fila, col, self.tmp_txt_comboBox.itemText(
            self.tmp_txt_comboBox.currentIndex()))
        self.tmp_txt_comboBox.currentIndexChanged.connect(setValue_grid_Lambda)

        # destroy_Object_Lambda = lambda: self.destroy_Object_all(me_tableWidget)
        # self.tmp_txt_texto.editingFinished.connect(destroy_Object_Lambda)

    def destroy_Object_all(self, me_tableWidget):
        n = 0
        while n <= int(me_tableWidget.rowCount()):
            for vcol in range(12):
                me_tableWidget.removeCellWidget(n, vcol)

            n += 1

    def setValue_Object(self, me_tableWidget, fila, col, value_txt):
        item1 = QtWidgets.QTableWidgetItem()
        item1.setText(str(value_txt))
        item1.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        me_tableWidget.setItem(fila, col, item1)

        if str(me_tableWidget.objectName()) == 'tableWidget_6':
            # ========================================= Condicional 1
            if fila == 4:
                for meCol in range(int(self.ui.lineEdit_10.text())+1):
                    if meCol == 0:
                        continue

                    if meCol > 1:
                        itemRelleno = QtWidgets.QTableWidgetItem()
                        itemRelleno.setText(value_txt)
                        itemRelleno.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                        me_tableWidget.setItem(4, meCol, itemRelleno)


                    itemEval = QtWidgets.QTableWidgetItem()
                    if value_txt == 'NO':
                        itemEval.setText("NA")
                    else:
                        itemEval.setText("")
                    itemEval.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                    me_tableWidget.setItem(5, meCol, itemEval)

            # ========================================= Condicional 2
            if fila == 10:
                itemEval = QtWidgets.QTableWidgetItem()
                if float(value_txt) <= 200:
                    itemEval.setText("C")
                else:
                    itemEval.setText("NC")

                itemEval.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                me_tableWidget.setItem(11, col, itemEval)

            # ========================================= Condicional 3
            # if fila in (1, 9):
            #     self.hallarCondicion_Tabla6_Item10(col)
            # =======================================================

            self.validarMessage_certificado()

    # -------------------------- para table 5 = Error Max Permitido -------------------
    def destroy_Object(self):
        n = 0
        while n <= int(self.ui.tableWidget_5.rowCount()):
            for vcol in range(11):
                self.ui.tableWidget_5.removeCellWidget(n, vcol + 1)

            n += 1

    def created_Object(self):
        fila = self.ui.tableWidget_5.currentRow()
        col = self.ui.tableWidget_5.currentColumn()

        # ================================================ Bloqueo de compartimientos desde CAJA Nro. Comaprtimientos
        if self.ui.lineEdit_10.text().strip() == '' or col > int(self.ui.lineEdit_10.text()):
            return
        # ================================================

        if col == 0:
            return

        if str(self.ui.tableWidget_5.item(fila, col)) == 'None':
            return

        v_txt = str(self.ui.tableWidget_5.item(fila, col).text())

        self.destroy_Object()

        self.tmp_txt_cant = QtWidgets.QLineEdit()
        self.tmp_txt_cant.setText(v_txt)
        self.ui.tableWidget_5.setCellWidget(fila, col, self.tmp_txt_cant)

        validar_grid_Lambda = lambda: self.validar_grid(fila, col)
        self.tmp_txt_cant.editingFinished.connect(self.destroy_Object)
        self.tmp_txt_cant.textChanged.connect(validar_grid_Lambda)

    def validar_grid(self, fila, col):

        if str(self.tmp_txt_cant.text()) == '':
            vv_cant = 0
        else:
            if fila > 4:
                item1 = QtWidgets.QTableWidgetItem()
                item1.setText(str(self.tmp_txt_cant.text()))
                item1.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.ui.tableWidget_5.setItem(fila, col, item1)
                return

            vv_cant = float(self.tmp_txt_cant.text()) if fila < 5 else self.tmp_txt_cant.text()

            item1 = QtWidgets.QTableWidgetItem()
            item1.setText(format(float(vv_cant), '.0f'))
            item1.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_5.setItem(fila, col, item1)

        # if fila == 1 or fila == 3:
        #     itemLitro = QtWidgets.QTableWidgetItem()
        #     itemLitro.setText(format(float(vv_cant * 3.785412), '.2f'))
        #     itemLitro.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        #     self.ui.tableWidget_5.setItem(fila - 1, col, itemLitro)
        #
        # if fila == 0 or fila == 2:
        #     itemGalon = QtWidgets.QTableWidgetItem()
        #     itemGalon.setText(format(float(vv_cant / 3.785412), '.2f'))
        #     itemGalon.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        #     self.ui.tableWidget_5.setItem(fila+1, col, itemGalon)

        if fila == 1:
            itemLitroParent = QtWidgets.QTableWidgetItem()
            itemLitroParent.setText(format(float(vv_cant * 3.785412), '.1f'))
            itemLitroParent.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_5.setItem(fila - 1, col, itemLitroParent)
            # =========================================================================================================
            itemLitro = QtWidgets.QTableWidgetItem()
            itemLitro.setText(format(float(vv_cant * 3.785412), '.1f'))
            itemLitro.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_5.setItem(2, col, itemLitro)

            itemGalon = QtWidgets.QTableWidgetItem()
            itemGalon.setText(format(float(vv_cant), '.0f'))
            itemGalon.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_5.setItem(3, col, itemGalon)

        if fila == 0:
            itemGalonParent = QtWidgets.QTableWidgetItem()
            itemGalonParent.setText(format(float(vv_cant / 3.785412), '.0f'))
            itemGalonParent.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_5.setItem(fila + 1, col, itemGalonParent)
            # =========================================================================================================
            itemLitro = QtWidgets.QTableWidgetItem()
            itemLitro.setText(format(float(vv_cant / 3.785412), '.0f'))
            itemLitro.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_5.setItem(3, col, itemLitro)

            itemGalon = QtWidgets.QTableWidgetItem()
            itemGalon.setText(format(float(vv_cant), '.1f'))
            itemGalon.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_5.setItem(2, col, itemGalon)

        self.hallar_total_capacidad_nominal()

    #######################################################################################################################
    #######################################################################################################################

    #######################################################################################################################
    ################################################# OBTENER DATOS ###########################################################
    #######################################################################################################################


    def hallar_total_capacidad_nominal(self):
        vtotal_capacidad_nominal_litros = 0
        vtotal_capacidad_nominal_galones = 0

        for vcol in range(11):
            vdata_exist = 0

            if str(self.ui.tableWidget_5.item(0, vcol + 1)) != 'None' and str(
                    self.ui.tableWidget_5.item(0, vcol + 1).text()).strip() != '':
                vtotal_capacidad_nominal_litros = vtotal_capacidad_nominal_litros + float(
                    str(self.ui.tableWidget_5.item(0, vcol + 1).text()).strip())

            if str(self.ui.tableWidget_5.item(1, vcol + 1)) != 'None' and str(
                    self.ui.tableWidget_5.item(1, vcol + 1).text()).strip() != '':
                vtotal_capacidad_nominal_galones = vtotal_capacidad_nominal_galones + float(
                    str(self.ui.tableWidget_5.item(1, vcol + 1).text()).strip())

        self.ui.lineEdit_11.setText(format(float(vtotal_capacidad_nominal_litros), '.2f'))
        self.ui.lineEdit_29.setText(format(float(vtotal_capacidad_nominal_galones), '.2f'))

    #######################################################################################################################
    ################################################# CARGAR DEFAULT ###########################################################
    #######################################################################################################################

    def llenar_cisternas(self):
        sql = "select cod_cisterna,placa_tanque from cisternas where cod_clie=" + str(self.ui.lineEdit.statusTip())
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        self.ui.comboBox_8.clear()
        self.ui.comboBox_8.addItem("Seleccionar", "0")
        for row in cur.fetchall():
            self.ui.comboBox_8.addItem(str(row[1]), str(row[0]))

        cur.close()
        meCon.cerrarConexion()


    def hallar_data_cisterna(self):
        vv_cod_cisterna = str(self.ui.comboBox_8.itemData(self.ui.comboBox_8.currentIndex()))
        if vv_cod_cisterna == 'None':
            return

        sql = "select "
        sql = sql + " tipo,"  # 0
        sql = sql + " cod_tanque,"  # 1
        sql = sql + " marca_tanque,"  # 2
        sql = sql + " modelo_tanque,"  # 3
        sql = sql + " fab_tanque,"  # 4
        sql = sql + " serie_tanque,"  # 5
        sql = sql + " ejes_tanque,"  # 6
        sql = sql + " vin_tanque "  # 7
        sql = sql + " from cisternas "
        sql = sql + " where cod_cisterna=" + vv_cod_cisterna
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)
        resultado = cur.fetchall()
        cur.close()
        meCon.cerrarConexion

        for row in resultado:
            n = 0
            while n <= self.ui.comboBox_6.count() - 1:  # tipo
                if str(self.ui.comboBox_6.itemText(n)) == str(row[0]):
                    self.ui.comboBox_6.setCurrentIndex(n)
                n = n + 1

            self.ui.lineEdit_5.setText(row[1])
            self.ui.lineEdit_21.setText(row[2])
            self.ui.lineEdit_22.setText(row[3])
            self.ui.lineEdit_23.setText(str(row[4]))
            self.ui.lineEdit_24.setText(row[5])
            self.ui.lineEdit_26.setText(str(row[6]))
            self.ui.lineEdit_27.setText(row[7])


    def llenar_vehiculos(self):
        self.ui.comboBox_7.clear()
        self.ui.comboBox_7.addItem("Seleccionar", "0")
        vv_cod_cisterna = str(self.ui.comboBox_8.itemData(self.ui.comboBox_8.currentIndex()))
        if vv_cod_cisterna == 'None':
            return

        sql = "select cod_vehiculo,placa_vehiculo from vehiculos_clie where cod_cisterna=" + vv_cod_cisterna + " and cod_clie=" + str(self.ui.lineEdit.statusTip())
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        for row in cur.fetchall():
            self.ui.comboBox_7.addItem(str(row[1]), str(row[0]))

        cur.close()
        meCon.cerrarConexion()


    def hallar_data_vehiculo(self):
        vv_cod_vehiculo = str(self.ui.comboBox_7.itemData(self.ui.comboBox_7.currentIndex()))
        if vv_cod_vehiculo == 'None':
            return

        sql = "select "
        sql = sql + " marca_vehiculo,"
        sql = sql + " modelo_vehiculo,"
        sql = sql + " fab_vehiculo,"
        sql = sql + " serie_vehiculo,"
        sql = sql + " ejes_vehiculo,"
        sql = sql + " vin_vehiculo "
        sql = sql + " from vehiculos_clie "
        sql = sql + " where cod_vehiculo=" + vv_cod_vehiculo
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        for row in cur.fetchall():
            self.ui.lineEdit_25.setText(row[0])
            self.ui.lineEdit_30.setText(row[1])
            self.ui.lineEdit_31.setText(str(row[2]))
            self.ui.lineEdit_32.setText(row[3])
            self.ui.lineEdit_33.setText(str(row[4]))
            self.ui.lineEdit_34.setText(row[5])


        cur.close()
        meCon.cerrarConexion()


    # --------------------------------------------------------------------------------------------
    def llenar_cubicadores_autorizadores(self):
        sql = "select cod_personal, nombres, cargo from personal order by nombres"
        print(sql)
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        meCon.cerrarConexion()

        self.ui.comboBox_4.clear()
        self.ui.comboBox_5.clear()
        for row in rows:
            if str(row[2]) == 'GERENTE TECNICO':
                self.ui.comboBox_4.addItem(str(row[1]), str(row[0]))
            if str(row[2]) == 'RESPONSABLE TECNICO DE LA INSPECCION':
                self.ui.comboBox_5.addItem(str(row[1]), str(row[0]))

    def llenar_empresa_resolucion(self):
        sql = "select cod_empresa,resolucion from empresa order by razon_social"
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        meCon.cerrarConexion()

        # self.ui.comboBox_2.clear()
        # for row in rows:
        #     self.ui.comboBox_2.addItem(str(row[1]),str(row[0]))




    #######################################################################################################################
    ################################################# CARGAR EXISTENTE ###########################################################
    #######################################################################################################################

    def buscar_trazabilidad(self, v_cod_certificado):
        self.format_trazabilidad_grid()

        sql = "select"
        sql = sql + " cod_equipo,"  # 0
        sql = sql + "id_equipo,"  # 1
        sql = sql + "nombre_equipo,"  # 2
        sql = sql + "certificado_calibracion,"  # 3
        sql = sql + "fecha_calibracion "  # 4
        sql = sql + " from trazabilidad_correctivo "
        sql = sql + " where cod_certificado=" + str(v_cod_certificado)
        sql = sql + " order by item "
        print(sql)

        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)
        resultado = cur.fetchall()
        cur.close()
        meCon.cerrarConexion()

        n = 0
        for row in resultado:
            self.ui.tableWidget_2.setRowCount(n + 1)

            item0 = QtWidgets.QTableWidgetItem()
            item0.setText(str(row[0]))
            self.ui.tableWidget_2.setItem(n, 0, item0)

            item1 = QtWidgets.QTableWidgetItem()
            item1.setText(str(row[1]))
            self.ui.tableWidget_2.setItem(n, 1, item1)

            item2 = QtWidgets.QTableWidgetItem()
            item2.setText(str(row[2]))
            self.ui.tableWidget_2.setItem(n, 2, item2)

            item3 = QtWidgets.QTableWidgetItem()
            item3.setText(str(row[3]))
            self.ui.tableWidget_2.setItem(n, 3, item3)

            item4 = QtWidgets.QTableWidgetItem()
            item4.setText(str(row[4]))
            self.ui.tableWidget_2.setItem(n, 4, item4)

            n = n + 1

    def buscar_MET(self, v_cod_certificado):
        self.format_MedExtTanq_grid()

        vv_tipo_unidad = str(self.ui.comboBox_6.itemText(self.ui.comboBox_6.currentIndex()))

        sql = "SELECT valor FROM medidas_externas_tanque_correctivo"
        sql = sql + " where cod_empresa=1 and cod_certificado=" + v_cod_certificado + " and tipo='Sin carga'"
        sql = sql + " ORDER BY id_medida ASC"

        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)

        col = 0
        for row in cur.fetchall():
            item = QtWidgets.QTableWidgetItem()
            item.setText(format(float(row[0]), '.1f'))
            # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_7.setItem(0, col, item)
            col += 1

        sql = "SELECT valor FROM medidas_externas_tanque"
        sql = sql + " where cod_empresa=1 and cod_certificado=" + v_cod_certificado + " and tipo='Con carga'"
        sql = sql + " ORDER BY id_medida ASC"
        # cur = main.con.cursor()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)

        col = 0
        for row in cur.fetchall():
            item = QtWidgets.QTableWidgetItem()
            item.setText(str(row[0]))
            # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_8.setItem(0, col, item)
            col = col + 1


        cur.close()
        meCon.cerrarConexion()


    def buscar_ensayosExclusivos(self, v_cod_certificado):
        self.format_ensayosExclusivos_grid()
        sql = "SELECT"
        sql = sql + " item,"  # 0
        sql = sql + " nro_compartimento,"  # 1
        sql = sql + " valor "  # 2
        sql = sql + " FROM ensayos_exclusivos_verif_ini_correctivo "
        sql = sql + " where cod_empresa=1 and cod_certificado=" + str(v_cod_certificado)
        sql = sql + " order by item"
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        resultados = cur.fetchall()
        cur.close()
        meCon.cerrarConexion()

        for row in resultados:
            item = QtWidgets.QTableWidgetItem()
            item.setText(str(row[2]))
            # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_9.setItem(row[0], row[1], item)



    def buscar_ensayosVeriIni(self, v_cod_certificado):
        self.format_ensayosVerificacion_grid()
        sql = " select "
        sql = sql + " item,"  # 0
        sql = sql + " nro_compartimento,"  # 1
        sql = sql + " valor "  # 2
        sql = sql + " FROM ensayos_veri_ini_posterior_correctivo "
        sql = sql + " where cod_empresa=1 and cod_certificado=" + str(v_cod_certificado)
        sql = sql + " order by item"
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)
        resultado = cur.fetchall()
        cur.close()
        meCon.cerrarConexion()

        for row in resultado:
            item = QtWidgets.QTableWidgetItem()
            item.setText(str(row[2]))
            # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_6.setItem(row[0], row[1], item)

    def buscar_errorMax(self, v_cod_certificado):
        self.format_errorVerif_grid()
        sql = " select "
        sql = sql + " item,"  # 0
        sql = sql + " nro_compartimento,"  # 1
        sql = sql + " valor "  # 2
        sql = sql + " FROM error_max_permisible_correctivo "
        sql = sql + " where cod_empresa=1 and cod_certificado=" + str(v_cod_certificado)
        sql = sql + " order by item"
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        resultado = cur.fetchall()
        cur.close()
        meCon.cerrarConexion()

        for row in resultado:
            item = QtWidgets.QTableWidgetItem()
            item.setText(str(row[2]))
            # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_5.setItem(row[0], row[1], item)


    def buscar_DatosRptInacal(self, v_cod_certificado):
        self.format_ReportInacal()
        sql = " select "
        sql = sql + " item,"  # 0
        sql = sql + " nro_compartimento,"  # 1
        sql = sql + " valor "  # 2
        sql = sql + " FROM cubicacion_rpt_inacal_correctivo "
        sql = sql + " where cod_empresa=1 and cod_certificado=" + str(v_cod_certificado)
        sql = sql + " order by item"
        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()
        cur.execute(sql)
        resultado = cur.fetchall()
        cur.close()
        meCon.cerrarConexion()

        for row in resultado:
            item = QtWidgets.QTableWidgetItem()
            item.setText(str(row[2]))
            # item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_10.setItem(row[0], row[1], item)



    def carga_data_edit(self):
        import os

        self.llenar_cisternas()

        sql = "select "
        sql = sql + " c.cod_certificado ,"  # 0
        sql = sql + " c.cod_empresa ,"  # 1
        sql = sql + " c.cod_venta ,"  # 2

        sql = sql + " c.tipo_verificacion,"  # 3
        sql = sql + " c.registro_cubicacion_serie ,"  # 4
        sql = sql + " c.registro_cubicacion_nro ,"  # 5
        sql = sql + " c.cod_capicua,"  # 6

        sql = sql + " c.cod_clie,"  # 7
        sql = sql + " c.r_social,"  # 8
        sql = sql + " c.direccion,"  # 9
        sql = sql + " c.ruc,"  # 10
        sql = sql + " c.fecha_emision,"  # 11
        sql = sql + " c.fecha_expiracion,"  # 12

        sql = sql + " c.tipo_vehiculo,"  # 13
        sql = sql + " c.cod_tanque ,"  # 14
        sql = sql + " '' as clasificacion_tanque,"  # 15

        sql = sql + " c.marca_vehiculo,"  # 16
        sql = sql + " c.modelo_vehiculo,"  # 17
        sql = sql + " c.fab_vehiculo,"  # 18
        sql = sql + " c.serie_vehiculo,"  # 19
        sql = sql + " c.placa_vehiculo,"  # 20
        sql = sql + " c.ejes_vehiculo,"  # 21
        sql = sql + " c.vin_vehiculo,"  # 22

        sql = sql + " c.marca_tanque ,"  # 23
        sql = sql + " c.modelo_tanque ,"  # 24
        sql = sql + " c.fab_tanque ,"  # 25
        sql = sql + " c.serie_tanque ,"  # 26
        sql = sql + " c.placa_tanque ,"  # 27
        sql = sql + " c.ejes_tanque ,"  # 28
        sql = sql + " c.vin_tanque,"  # 29

        sql = sql + " c.capacidad_nominal_litros,"  # 30
        sql = sql + " c.total_compartimentos,"  # 31

        sql = sql + " c.img_frente,"  # 32
        sql = sql + " c.img_perfil,"  # 33

        sql = sql + " c.lugar_verif,"  # 34
        sql = sql + " c.doc_normativo_proc_verif,"  # 35

        sql = sql + " c.det_no_conformidad_ensayos_exclusivos,"  # 36

        sql = sql + " c.det_no_conformidad_ensayos_verif_ini,"  # 37

        sql = sql + " c.det_no_conformidad_error_max_permisible,"  # 38
        sql = sql + " c.temperatura,"  # 39

        sql = sql + " c.conclusion,"  # 40
        sql = sql + " c.estado,"  # 41

        sql = sql + " c.observaciones_certificado,"  # 42

        sql = sql + " c.inspector ,"  # 43
        sql = sql + " c.responsable,"  # 44

        sql = sql + " c.cod_cisterna,"  # 45
        sql = sql + " c.cod_vehiculo,"  # 46
        sql = sql + " c.observaciones_tarjeta, "  # 47

        sql = sql + " c.fecha_verif, "  # 48

        sql = sql + " c.estado_registro, "  # 49

        sql = sql + " c.met_op_one, "  # 50
        sql = sql + " c.met_op_two, "  # 51

        sql = sql + " c.capacidad_nominal_galones, "  # 52

        sql = sql + " c.tipo_certificado, "  # 53

        sql = sql + " c.hora_inicio, "  # 54
        sql = sql + " c.hora_fin, "  # 55

        sql = sql + " obs_checklist, "  #56
        sql = sql + " obs_inspeccion "  #57


        sql = sql + " from certificados_correctivo c "
        sql = sql + " where c.cod_certificado=" + str(self.ui.lineEdit_15.statusTip())

        print(sql)

        # cur = main.con.cursor()
        meCon = conexion()
        cur = meCon.meConexion.cursor()

        cur.execute(sql)

        resultado = cur.fetchall()
        cur.close()
        meCon.cerrarConexion()
        n = 0
        for row in resultado:

            tmp_cod_certificado = str(self.ui.lineEdit_15.statusTip())

            self.ui.lineEdit_10.setText(str(row[31]) if str(row[31]) != 'None' else '')  # nro compartimentos

            if str(row[3]) == 'INICIAL':
                self.ui.radioButton.setChecked(True)
            else:
                self.ui.radioButton_2.setChecked(True)


            # ========================================================================
            self.ui.lineEdit_15.setText(str(row[4]) if str(row[4]) != 'None' else '')  # registro cub seri
            self.ui.lineEdit_16.setText(str(row[5]) if str(row[5]) != 'None' else '')  # registro cub nro

            self.ui.groupBox_2.setStatusTip(str(row[2]))


            self.ui.lineEdit.setStatusTip(str(row[7]) if str(row[7]) != 'None' else '')  # cod clie
            self.ui.lineEdit.setText(str(row[8]) if str(row[8]) != 'None' else '')  # razon social
            self.ui.lineEdit_6.setText(str(row[9]) if str(row[9]) != 'None' else '')  # dir clie
            self.ui.lineEdit_18.setText(str(row[10]) if str(row[10]) != 'None' else '')  # ruc clie

            emi_ano = str(row[11])[:4];
            emi_mes = str(row[11])[5:-3];
            emi_dia = str(row[11])[8:]
            self.ui.dateEdit.setDate(QtCore.QDate(int(emi_ano), int(emi_mes), int(emi_dia)))  # fecha emiison

            exp_ano = str(row[12])[:4];
            exp_mes = str(row[12])[5:-3];
            exp_dia = str(row[12])[8:]
            self.ui.dateEdit_2.setDate(QtCore.QDate(int(exp_ano), int(exp_mes), int(exp_dia)))  # fecha experici

            # ================================== TANQUE
            n = 0
            while n <= self.ui.comboBox_8.count() - 1:  # placa
                if str(self.ui.comboBox_8.itemData(n)) == str(row[45]):
                    self.ui.comboBox_8.setCurrentIndex(n)
                    break
                n = n + 1

            self.ui.lineEdit_21.setText(str(row[23]) if str(row[23]) != 'None' else '')  # marca
            self.ui.lineEdit_22.setText(str(row[24]) if str(row[24]) != 'None' else '')  # modelo
            self.ui.lineEdit_23.setText(str(row[25]) if str(row[25]) != 'None' else '')  # año fab
            self.ui.lineEdit_24.setText(str(row[26]) if str(row[26]) != 'None' else '')  # serie

            self.ui.lineEdit_26.setText(str(row[28]) if str(row[28]) != 'None' else '')  # nro ejes
            self.ui.lineEdit_27.setText(str(row[29]) if str(row[29]) != 'None' else '')  # vin

            self.ui.lineEdit_11.setText(format(float(row[30]), '.2f') if str(row[30]) != 'None' else '')  # capacidad nominal Litros
            self.ui.lineEdit_29.setText(format(float(row[52]), '.2f') if str(row[52]) != 'None' else '')  # capacidad nominal galones

            print(">>>>>>>")
            print(str(row[52]))
            print(">>>>>>>")

            # ========================================================================
            n = 0
            while n <= self.ui.comboBox_6.count() - 1:  # tipo vehiculo
                if str(self.ui.comboBox_6.itemData(n)) == str(row[13]):
                    self.ui.comboBox_6.setCurrentIndex(n)
                n = n + 1

            self.ui.lineEdit_5.setText(str(row[14]) if str(row[14]) != 'None' else '')  # nro cisterna
            # self.ui.lineEdit_20.setText(str(row[15]) if str(row[15]) != 'None' else '')  # clasificacion tanque

            # ================================== VEHICULO
            n = 0
            while n <= self.ui.comboBox_7.count() - 1:  # placa
                if str(self.ui.comboBox_7.itemData(n)) == str(row[46]):
                    self.ui.comboBox_7.setCurrentIndex(n)
                    break
                n = n + 1

            self.ui.lineEdit_25.setText(str(row[16]) if str(row[16]) != 'None' else '')  # marca
            self.ui.lineEdit_30.setText(str(row[17]) if str(row[17]) != 'None' else '')  # modelo
            self.ui.lineEdit_31.setText(str(row[18]) if str(row[18]) != 'None' else '')  # año fab
            self.ui.lineEdit_32.setText(str(row[19]) if str(row[19]) != 'None' else '')  # serie

            self.ui.lineEdit_33.setText(str(row[21]) if str(row[21]) != 'None' else '')  # nro ejes
            self.ui.lineEdit_34.setText(str(row[22]) if str(row[22]) != 'None' else '')  # vin

            # ================================== REGISTRO FOTOGRAFICO
            tmp_name_file_frente = "img/" + tmp_cod_certificado + "_frente.png"
            tmp_name_file_perfil = "img/" + tmp_cod_certificado + "_perfil.png"

            open(tmp_name_file_frente, 'wb').write(row[32])  # frente
            self.ui.label_img_frente.setStatusTip(str(os.getcwd()) + "/" + tmp_name_file_frente)
            self.ui.label_img_frente.setPixmap(QtGui.QPixmap(str(os.getcwd()) + "/" + tmp_name_file_frente))

            # print(str(os.getcwd()) + tmp_name_file_frente)

            open(tmp_name_file_perfil, 'wb').write(row[33])  # perfil
            self.ui.label_img_trasera.setStatusTip(str(os.getcwd()) + "/" + tmp_name_file_perfil)
            self.ui.label_img_trasera.setPixmap(QtGui.QPixmap(str(os.getcwd()) + "/" + tmp_name_file_perfil))

            from calidad_Imagen import calidad_Imagen
            img_frente = calidad_Imagen(tmp_name_file_frente)
            img_frente.reducir()

            img_perfil = calidad_Imagen(tmp_name_file_perfil)
            img_perfil.reducir()

            # ================================== FECHA LUGAR VERIFICACION
            emi_ano = str(row[48])[:4];
            emi_mes = str(row[48])[5:-3];
            emi_dia = str(row[48])[8:]
            self.ui.dateEdit_3.setDate(QtCore.QDate(int(emi_ano), int(emi_mes), int(emi_dia)))  # fecha emiison
            self.ui.lineEdit_17.setText(str(row[34]) if str(row[34]) != 'None' else '')

            # ================================== DOC NORMATIVO
            self.ui.plainTextEdit.setPlainText(str(row[35]) if str(row[35]) != 'None' else '')

            # ================================== EXSAYOS ECLUSIVOS
            self.ui.plainTextEdit_8.setPlainText(str(row[36]) if str(row[36]) != 'None' else '')

            # ================================== VERIF INI Y POST
            self.ui.plainTextEdit_6.setPlainText(str(row[37]) if str(row[37]) != 'None' else '')

            # ================================== ERROR MAXIMO PERMISIBLE
            self.ui.plainTextEdit_7.setPlainText(str(row[38]) if str(row[38]) != 'None' else '')
            self.ui.doubleSpinBox.setValue(row[39])

            # ================================== CONCLUSION VERIFICACION
            if str(row[41]) == "APROBADO":
                self.ui.checkBox_2.setCheckState(True)
            else:
                self.ui.checkBox_2.setCheckState(False)

            self.ui.plainTextEdit_2.setPlainText(str(row[40]) if str(row[40]) != 'None' else '')  # conclusion

            self.ui.plainTextEdit_4.setPlainText(str(row[56]) if str(row[56]) != 'None' else '')  # OBS CHECKLIST
            self.ui.plainTextEdit_5.setPlainText(str(row[57]) if str(row[57]) != 'None' else '')  # OBS RPT INSPECCION


            # ================================== OBSERVACIONES

            self.ui.plainTextEdit_3.setPlainText(str(row[42]) if str(row[42]) != 'None' else '')  # obs certificado

            self.ui.plainTextEdit_9.setPlainText(str(row[47]) if str(row[47]) != 'None' else '')  # obs tarjeta


            # ==================================
            n = 0
            while n <= self.ui.comboBox_5.count() - 1:  # firma
                if str(self.ui.comboBox_5.itemData(n)) == str(row[44]):
                    self.ui.comboBox_5.setCurrentIndex(n)
                    break
                n = n + 1

            # ==================================
            n = 0
            while n <= self.ui.comboBox_4.count() - 1:  # firma
                if str(self.ui.comboBox_4.itemData(n)) == str(row[43]):
                    self.ui.comboBox_4.setCurrentIndex(n)
                    break
                n = n + 1

            print("@@@@@@")
            print(str(row[50]))

            self.ui.doubleSpinBox_2.setValue(float(row[50]) if str(row[50]) != 'None' else 0)  # MET_op_one
            self.ui.doubleSpinBox_3.setValue(float(row[51]) if str(row[51]) != 'None' else 0)  # MET_op_two

            if str(row[53]) == "RENOVACION":
                self.ui.radioButton_3.setChecked(True)
            else:
                self.ui.radioButton_4.setChecked(True)

            me_Hora_Ini = str(row[54]).split(':')
            self.ui.timeEdit.setTime(QtCore.QTime(int(me_Hora_Ini[0]), int(me_Hora_Ini[1]), int(me_Hora_Ini[2])))
            me_Hora_Fin = str(row[55]).split(':')
            self.ui.timeEdit_2.setTime(QtCore.QTime(int(me_Hora_Fin[0]), int(me_Hora_Fin[1]), int(me_Hora_Fin[2])))

        self.buscar_trazabilidad(str(self.ui.lineEdit_15.statusTip()))
        self.buscar_MET(str(self.ui.lineEdit_15.statusTip()))
        self.buscar_ensayosExclusivos(str(self.ui.lineEdit_15.statusTip()))
        self.buscar_ensayosVeriIni(str(self.ui.lineEdit_15.statusTip()))
        self.buscar_errorMax(str(self.ui.lineEdit_15.statusTip()))

        self.buscar_DatosRptInacal(str(self.ui.lineEdit_15.statusTip()))

        validador_fecha_emi = int("%s%s%s" % (str(self.ui.dateEdit.date().year()), str(self.ui.dateEdit.date().month()).zfill(2),str(self.ui.dateEdit.date().day()).zfill(2)))



    def autoRelleno_Grid(self):
        self.new_trazabilidad_grid()
        self.format_ensayosExclusivos_grid()
        self.format_ensayosVerificacion_grid()
        self.format_errorVerif_grid()

        me_TotalCompartimientos = int(self.ui.lineEdit_10.text()) if str(self.ui.lineEdit_10.text()) != '' else 0

        # ========================================================================
        # Grid 9 = 8. Ensayos exclusivos para verificacion inicial
        for row in range(self.ui.tableWidget_9.rowCount()):
            me_txt = ""
            for col in range(me_TotalCompartimientos + 1):
                if col == 0:
                    continue

                if self.ui.radioButton.isChecked():
                    me_txt = "C" if row in (0, 1, 2, 3) and self.ui.radioButton.isChecked() else ""
                else:
                    me_txt = ""

                itemTxt = QtWidgets.QTableWidgetItem()
                itemTxt.setText(me_txt)
                itemTxt.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.ui.tableWidget_9.setItem(row, col, itemTxt)

        # ========================================================================
        # Grid 6 = 9. Ensayos para verificacion inicial y Posterior

        for row in range(self.ui.tableWidget_6.rowCount()):
            me_txt = ""
            for col in range(me_TotalCompartimientos + 1):
                if col == 0:
                    continue

                if row in (0, 3, 7, 9):
                    me_txt = "C"

                if row in (5,) and self.ui.radioButton.isChecked():
                    me_txt = "NA"

                if row in (1, 6):
                    me_txt = "NA" if int(self.ui.lineEdit_10.text()) == 1 else 'C'

                # if row in (4,):
                #     me_txt = "-" if self.ui.radioButton.isChecked() else 'C'

                if row in (12,):
                    me_txt = "SI"

                if row == 14:
                    me_txt = "SI" if col==1 else ''



                itemTxt = QtWidgets.QTableWidgetItem()
                itemTxt.setText(me_txt)
                itemTxt.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.ui.tableWidget_6.setItem(row, col, itemTxt)

        # ========================================================================
        # Grid 5 = 10. Error Maximo Permitido
        for row in range(self.ui.tableWidget_5.rowCount()):
            me_txt = ""
            for col in range(me_TotalCompartimientos + 1):
                if col == 0:
                    continue

                if row == 4:
                    me_txt = "0.0"

                if row == 5:
                    me_txt = "C"

                # if row == 5 and self.ui.radioButton.isChecked():
                #     me_txt = "-"
                # if row == 5 and self.ui.radioButton_2.isChecked():
                #     me_txt = "C"

                itemTxt = QtWidgets.QTableWidgetItem()
                itemTxt.setText(me_txt)
                itemTxt.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
                self.ui.tableWidget_5.setItem(row, col, itemTxt)

    def enabled_Precintos(self):
        me_CountCompartimientos = int(self.ui.lineEdit_10.text()) if str(
            self.ui.lineEdit_10.text()).strip() != '' else 0

        for row in range(self.ui.tableWidget_9.rowCount() + 1):
            for col in range(me_CountCompartimientos + 1):
                if str(self.ui.tableWidget_9.item(row, col)) == 'None':
                    continue

                print(str(self.ui.tableWidget_9.item(row, col)))
                if self.ui.tableWidget_9.item(row, col).text().strip() == 'NC':
                    self.limpiar_Precintos()
                    return False

        for row in range(self.ui.tableWidget_6.rowCount() + 1):
            for col in range(me_CountCompartimientos + 1):
                if str(self.ui.tableWidget_6.item(row, col)) == 'None':
                    continue
                if self.ui.tableWidget_6.item(row, col).text().strip() == 'NC':
                    self.limpiar_Precintos()
                    return False

        return True

    def limpiar_Precintos(self):
        me_CountCompartimientos = int(self.ui.lineEdit_10.text()) if str(
            self.ui.lineEdit_10.text()).strip() != '' else 0

        for col in range(me_CountCompartimientos + 1):
            itemTxt = QtWidgets.QTableWidgetItem()
            itemTxt.setText("")
            itemTxt.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget_5.setItem(12, col, itemTxt)

    def detectar_CompartimientosNC(self):
        conclusionVerificacion_NC = {}

        for fila in range(self.ui.tableWidget_9.rowCount()):
            lista_compartimientos = []
            for col in range(int(self.ui.lineEdit_10.text()) + 1):
                if str(self.ui.tableWidget_9.item(fila, col)) == 'None':
                    continue

                if str(self.ui.tableWidget_9.item(fila, col).text()).strip() == 'NC':
                    lista_compartimientos.append(str(col))

            if len(lista_compartimientos) > 0:
                conclusionVerificacion_NC[
                    str(self.ui.tableWidget_9.item(fila, 0).text()).strip()] = lista_compartimientos

        for fila in range(self.ui.tableWidget_6.rowCount()):
            lista_compartimientos = []
            for col in range(int(self.ui.lineEdit_10.text()) + 1):
                if str(self.ui.tableWidget_6.item(fila, col)) == 'None':
                    continue

                if str(self.ui.tableWidget_6.item(fila, col).text()).strip() == 'NC':
                    lista_compartimientos.append(str(col))

            if len(lista_compartimientos) > 0:
                if fila == 10:
                    conclusionVerificacion_NC[
                        '8.5.4 ' + str(self.ui.tableWidget_6.item(fila, 0).text()).strip()] = lista_compartimientos
                else:
                    conclusionVerificacion_NC[
                        str(self.ui.tableWidget_6.item(fila, 0).text()).strip()] = lista_compartimientos

        for fila in range(self.ui.tableWidget_5.rowCount()):
            lista_compartimientos = []
            for col in range(int(self.ui.lineEdit_10.text()) + 1):
                if str(self.ui.tableWidget_5.item(fila, col)) == 'None':
                    continue

                if str(self.ui.tableWidget_5.item(fila, col).text()).strip() == 'NC':
                    lista_compartimientos.append(str(col))

            if len(lista_compartimientos) > 0:
                conclusionVerificacion_NC[
                    str(self.ui.tableWidget_5.item(fila, 0).text()).strip()] = lista_compartimientos

        return conclusionVerificacion_NC

    def hallarCondicion_Tabla6_Item10(self, col):
        if str(self.ui.tableWidget_6.item(1, col)) == "None" or str(self.ui.tableWidget_6.item(9, col)) == "None":
            return

        if str(self.ui.tableWidget_6.item(1, col).text()) == "" or str(self.ui.tableWidget_6.item(9, col).text()) == "":
            return

        alturaLiquido_cm = float(self.ui.tableWidget_6.item(1, col).text())
        alturaLiquido_mm = float(self.ui.tableWidget_6.item(1, col).text()) * 10
        alturaEspacio_mm = float(self.ui.tableWidget_6.item(9, col).text())

        vH_mm = alturaLiquido_mm + alturaEspacio_mm
        vPorcent_mm = vH_mm * 0.1

        v_resultado = 'NC'
        vX_mm = 155

        if vH_mm <= 1550:
            vX_mm = vPorcent_mm

        if alturaEspacio_mm <= vX_mm:
            v_resultado = 'C'

        me_Item = QtWidgets.QTableWidgetItem()
        me_Item.setText(v_resultado)
        # me_Item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
        self.ui.tableWidget_6.setItem(10, col, me_Item)

    def eval_Certificado(self):
        v_aprobado = True

        if self.ui.radioButton.isChecked():
            for fila in range(self.ui.tableWidget_9.rowCount()):
                for col in range(int(self.ui.lineEdit_10.text()) + 1):
                    if self.ui.tableWidget_9.item(fila, col).text() == 'NC':
                        v_aprobado = False

        for fila in range(self.ui.tableWidget_6.rowCount()):
            for col in range(int(self.ui.lineEdit_10.text()) + 1):
                if self.ui.tableWidget_6.item(fila, col).text() == 'NC':
                    v_aprobado = False

        for fila in range(self.ui.tableWidget_5.rowCount()):
            for col in range(int(self.ui.lineEdit_10.text()) + 1):
                if self.ui.tableWidget_5.item(fila, col).text() == 'NC':
                    v_aprobado = False

        return v_aprobado


    def IngresoPrecinto_EnAprobado(self):
        if self.ui.checkBox_2.isChecked():
            for col in range(int(self.ui.lineEdit_10.text()) + 1):
                if str(self.ui.tableWidget_6.item(12, col)) == 'None' or str(
                        self.ui.tableWidget_6.item(12, col).text()).strip() == '':
                    return False

        return True
