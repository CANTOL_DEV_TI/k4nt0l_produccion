

import React from "react";
import {GrEdit} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";

const ResultadosTablaMotivos = ({key,
                        nom_subarea,
                        causa,
                        eventoEditar,
                        eventoEliminar}) =>
    (
        <tbody>
        <tr className="data">
            <td className="colString" id={key}>{nom_subarea}</td>
            <td className="colString" id={key}>{causa}</td>
            <td className="colBtn">
                <button onClick={() => eventoEditar(true)}><GrEdit color="#011826"/></button>
            </td>
            <td className="colBtn">
                <button onClick={() => eventoEliminar(true)}><AiFillDelete color="#b04129"/></button>
            </td>
        </tr>
        </tbody>
    )


export default ResultadosTablaMotivos;


