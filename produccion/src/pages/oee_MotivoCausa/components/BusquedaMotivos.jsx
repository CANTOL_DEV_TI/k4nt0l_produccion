import React, {useState} from "react";
import Title from "../../../components/Titulo";
import {GrAdd, GrFormSearch} from "react-icons/gr";
import {BotonExcel, BotonNuevo, BotonBuscar, BotonCerrar} from "../../../components/Botones";

class BusquedaMotivos extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            txtInput: '',
            cod_subarea: ''
        }
    }

    handleChange = (e) =>{
        if (e.target.name === 'selectArea' ){
            this.setState({cod_subarea: e.target.value })
        }

        if (e.target.name === 'txtInput' ) {
            this.setState({txtInput: e.target.value})
        }
    }


    render() {

        const {handleBusqueda, handleModal} = this.props

        return(
            <div className="col-lg-12">

                <div className="card">

                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-xl-4 col-sm-4 p-1" style={{minWidth: "40%"}}>
                                <Title>Lista de motivos: {this.props.TMP_NomCausa}</Title>
                            </div>

                            <div className="col-xl-4 col-sm-4 p-1" style={{minWidth: "60%"}}>
                                <div className="input-group">


                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda(true)}/>

                                    <BotonNuevo textoBoton={"Crear Nuevo"} sw_habilitado={true} eventoClick={() => handleModal(true)}/>

                                    <BotonCerrar textoBoton={"Atras"} sw_habilitado={true} eventoClick={()=>this.props.eventoClose(true)}/>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default BusquedaMotivos;