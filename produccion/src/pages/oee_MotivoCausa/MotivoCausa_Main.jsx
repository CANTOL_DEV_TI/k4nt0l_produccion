import React from "react";
import {get_subarea} from "../oee/services/oeeSubAreaServices.jsx";
import {
    delete_MotivoCausa,
    getFilter_MotivoCausa,
    save_MotivoCausa,
    update_MotivoCausa
} from "./services/MotivoCausaServices.jsx";
import {
    delete_Empleado,
    getFilter_Empleado,
    save_Empleado,
    update_Empleado
} from "../empleado/services/EmpleadoServices.jsx";

import BusquedaMotivos from "./components/BusquedaMotivos"
import EmpleadoEdicion from "../empleado/EmpleadoEdicion.jsx";
import VentanaModal from "../../components/VentanaModal.jsx";
import MotivoCausa_Edicion from "./MotivoCausa_Edicion.jsx";
import ResultadosTablaMotivos from "./components/ResultadosTablaMotivos.jsx";



class MotivoCausa_Main extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            listAreas: [],
            listMotivoCausa: [],

            resultados: [],
            txtInput: '',
            isFetch: true,

            showModal: false,
            dataRegistro: [],


        }

    }


    handleFindSubArea = async () =>{
        const responseJson = await get_subarea("00001")
        this.setState({listAreas: responseJson})
    }

    handleBusquedaMotivos = async () =>{
        const responseJson = await getFilter_MotivoCausa(0, this.props.TMP_CodCausa,0)
        // const responseJson = await getFilter_Empleado(3, txtFind)
        console.log(this.props.TMP_CodCausa)
        console.log(responseJson)
        this.setState({resultados: responseJson, isFetch: false})
    }


    handleSubmit = async (e) =>{
        if (this.state.tipoOpe === 'Grabar'){
            const responseJson = await save_MotivoCausa(e)
            console.log("################################")
            console.log(e)
            console.log(responseJson)
            console.log("################################")
        }

        if (this.state.tipoOpe === 'Actualizar'){
            const responseJson = await update_MotivoCausa(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar'){
            const responseJson = await delete_MotivoCausa(e)
            console.log(e)
            console.log(responseJson)
        }

        this.setState({showModal: false, tipoOpe: 'Find'})


    }





    render() {
        return(

            <React.Fragment>

                <BusquedaMotivos handleBusqueda={this.handleBusquedaMotivos} handleModal={() => this.setState({showModal: true, dataRegistro:null, tipoOpe: 'Grabar'})} listAreas={this.state.listMotivoCausa} TMP_NomCausa={this.props.TMP_NomCausa} eventoClose={this.props.eventoClose}/>

                { this.state.isFetch && 'Cargando'}
                { (!this.state.isFetch && !this.state.resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{height: '600px', display: "-ms-flexbox"}}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position: "sticky", top: 0}}>
                        <tr>
                            <th className="meCabIni">Sub Area</th>
                            <th className="meCab1">Motivo</th>
                            <th className="meCab2 meCabCentro">Edicion</th>
                            <th className="meCabFin">Eliminacion</th>
                        </tr>
                        </thead>
                        {this.state.resultados.map((registro)=>
                            <ResultadosTablaMotivos
                                key={registro.cod_motivo_causa}
                                nom_subarea={registro.nom_subarea}
                                causa={registro.motivo_causa}
                                eventoEditar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar'})}
                                eventoEliminar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar'})}
                            />
                        )}

                    </table>


                </div>

                <VentanaModal
                    show={this.state.showModal}
                    size="lg"
                    handleClose={() => this.setState({showModal: false})}
                    titulo="Edicion Motivos">

                    <MotivoCausa_Edicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={this.state.tipoOpe}
                        listAreas={this.state.listAreas}
                        cod_causa={this.props.TMP_CodCausa}
                    />

                </VentanaModal>
            </React.Fragment>



        )
    }


    async componentDidMount() {
        this.handleFindSubArea()
    }

}

export default MotivoCausa_Main;

