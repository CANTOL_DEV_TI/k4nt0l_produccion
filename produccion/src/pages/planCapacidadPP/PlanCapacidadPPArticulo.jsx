import React, { useState, useEffect } from 'react'
import { get_plan_capacidad_pp_articulo, get_plan_capacidad_pp_articulo_nivel, update_plan_capacidad_pp_det, update_plan_capacidad_pp_det_nivel } from '../../services/planCapacidadPPServices';
import { PlanCapacidadPPArticulo_tabla } from "./components/PlanCapacidadPPArticulo_tabla";
import { PlanCapacidadPPArticuloNivel1_tabla } from "./components/PlanCapacidadPPArticuloNivel1_tabla";
import { PlanCapacidadPPArticuloNivel1_afectado_tabla } from './components/PlanCapacidadPPArticuloNivel1_afectado_tabla';

import { useVentanaModal } from "../../hooks/useVentanaModal";
import VentanaModal from "../../components/VentanaModal";

export function PlanCapacidadPPArticulo({ idPlan, dic_articulo, eventClick }) {

    // VARIABLES
    const [idPlanCapacidad, setIdPlanCapacidad] = useState(idPlan);
    const [cantidadP, setCantidadP] = useState(0);

    // OBTENER DATOS
    const [listDatos, setListDatos] = useState([]);
    const [listArticuloNivel, setListArticuloNivel] = useState([]);
    const [listAfectado, setListAfectado] = useState([]);
    useEffect(() => {
        setListAfectado([]);
        getData();
        getArticuloNivel();
    }, [dic_articulo])

    //
    const getData = () => {
        setCantidadP(dic_articulo.cant_programado || 0);
        get_plan_capacidad_pp_articulo(idPlanCapacidad, dic_articulo.cod_articulo)
            .then(res => {
                setListDatos(res)
            })
    };

    // artículos de primer nivel
    const getArticuloNivel = () => {
        console.log("Entro a get_plan_capacidad_pp_articulo_nivel")
        get_plan_capacidad_pp_articulo_nivel(idPlanCapacidad, dic_articulo.cod_articulo)
            .then(res => {
                setListArticuloNivel(res)
            })
    };

    // BUSQUEDA EN DOM

    // EVENTOS  

    const handleChange = (evento) => {
        if (evento.target.name === 'cant_programado') {
            setCantidadP(evento.target.value);
        }
    };

    const updateAplicar = () => {
        const nuevoDato = listDatos.map(obj => {
            // variables
            const v_cant_programado = obj.cant_programado > 0 ? cantidadP : 0;
            const v_hora_programado = (v_cant_programado / obj.std_und_hora).toFixed(2);
            const v_t_hora_programado_mod = (Number(obj.t_hora_programado) - Number(obj.hora_programado) + Number(v_hora_programado)).toFixed(2);

            return {
                ...obj,
                cant_programado_mod: v_cant_programado,
                hora_programado_mod: v_hora_programado,
                t_hora_programado_mod: v_t_hora_programado_mod
            };
        });
        setListDatos(nuevoDato);
    };

    // CRUD
    // INSERTAR DATOS
    const updateData = () => {
        const nuevoDetalle = listDatos.map(obj => {
            return {
                ...obj,
                id_plan_capacidad_pp: obj.id_plan_capacidad_pp,
                id_item_pp: obj.id_item_pp,
                id_equipo: obj.id_equipo,
                num_paso: obj.num_paso,
                cant_programado: obj.cant_programado_mod,
                hora_programado: obj.hora_programado_mod,
            };
        });


        const new_data = {
            "detalle": nuevoDetalle
        };
        update_plan_capacidad_pp_det(new_data);
        eventClick(idPlan, "U");
    };

    // EVENTO GRABAR DESDE PRIMER NIVEL
    const eventOnclick = (datos, v_modo = "") => {
        let c_cantidad = 0;
        let c_modo = "";
        if (datos.cant_programado > datos.cant_programado_mod) {
            c_cantidad = datos.cant_programado - datos.cant_programado_mod
            c_modo = "S";
        } else {
            c_cantidad = datos.cant_programado_mod - datos.cant_programado
            c_modo = "I";
        }

        const new_data = {
            "id_plan_capacidad_pp": datos.id_plan_capacidad_pp,
            "cod_articulo": datos.cod_articulo,
            "cantidad": c_cantidad,
            "sw_modo": c_modo,
        };
        console.log(new_data)
        // modificar datos
        update_plan_capacidad_pp_det_nivel(new_data).then(res => {
            setListAfectado(res)
        })
        console.log(idPlan)
        eventClick(idPlan, "U");


        //handleClose(true);

    };

    // MOSTRAR MODAL
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = (opc = "N", datos) => {
        console.log("entro a Artículos de primer nivel")
        handleShow(true)
    };

    // RETORNAR INFORMACIÓN 
    return (
        <div className="col-lg-12">

            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header">
                    <div className="row gy-2">
                        <div className="col-lg-12">
                            <table className="table table-sm table-bordered caption-top mb-0">
                                <caption><b>Artículo: </b>{dic_articulo.cod_articulo} - {dic_articulo.articulo}</caption>
                                <tbody>
                                    <tr>
                                        <th scope="col" style={{ width: "165px" }}>Programar Cantidad:</th>
                                        <td className="d-flex flex-wrap gap-1">
                                            <input type="number" className="form-control form-control-sm" style={{ maxWidth: "120px", textAlign: "right" }} name="cant_programado" onChange={handleChange} value={cantidadP} />
                                            <button className="btn btn-sm btn-success" onClick={() => updateAplicar()}>Aplicar</button>
                                            <button className="btn btn-sm btn-primary" onClick={() => updateData()}>Grabar Paso</button>
                                            <button className="btn btn-sm btn-warning" onClick={() => mostrarVentana()}>Artículos de primer nivel</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <PlanCapacidadPPArticulo_tabla datos_fila={listDatos} setDatos={setListDatos} get_id_equipo={dic_articulo.id_equipo} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                {/* FIN PAGINACION */}
            </div>

            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="lg"
                handleClose={handleClose}
                titulo={listAfectado.length === 0 ? "Lista de artículos de primer nivel" : "Artículos Afectados"}
            >
                {
                    listAfectado.length === 0 ?
                        <PlanCapacidadPPArticuloNivel1_tabla datos_fila={listArticuloNivel} setDatos={setListArticuloNivel} eventoClick={eventOnclick} />
                        : <PlanCapacidadPPArticuloNivel1_afectado_tabla datos_fila={listAfectado} />
                }

            </VentanaModal>
            {/* FIN MODAL */}
        </div>
    );
}