import React, { useState, useEffect } from 'react'
import { PlanCapacidadPPMaquina } from "./PlanCapacidadPPMaquina";
import { PlanCapacidadPPMaquinaArticulo } from "./PlanCapacidadPPMaquinaArticulo";
import { PlanCapacidadPPArticulo } from "./PlanCapacidadPPArticulo";

export function PlanCapacidadPPDetalle({ idPlan, eventOnclick }) {

    // VARIABLES Y DATOS
    const [idPlanCapacidad, setIdPlanCapacidad] = useState(idPlan);
    const [refrescar, setRefrescar] = useState(0);
    const [idEquipo, setIdEquipo] = useState(0);
    const [maquina, setMaquina] = useState([]);
    const [articulo, setArticulo] = useState([]);    


    // EVENTOS
    const eventoOnclick = (datos, v_modo) => {
        if (v_modo == "ATRAS") {
            eventOnclick(v_modo)
        } else if (v_modo == "M") {
            setIdEquipo(datos.id_equipo)
            setMaquina(datos)
            setArticulo([])
        } else if (v_modo == "A") {
            setArticulo(datos)
        } else if (v_modo == "U") {
            let aaa = refrescar + 1
            console.log(aaa)
            setRefrescar(aaa);
            setMaquina([])
            setArticulo([])
        }
        console.log(v_modo)
        console.log(datos)
    };

    // PAGINACIÓN

    // MOSTRAR MODAL

    // RETORNAR INFORMACIÓN 
    return (
        <div className="row">
            <div className="col-lg-6">
                <PlanCapacidadPPMaquina idPlan={idPlanCapacidad} eventClick={eventoOnclick} refrescarDato={refrescar} get_id_equipo = {idEquipo} />
            </div>
            <div className="col-lg-6">
                <PlanCapacidadPPMaquinaArticulo idPlan={idPlanCapacidad} dic_equipo={maquina} eventClick={eventoOnclick} />
                <div className="p-1" />
                <PlanCapacidadPPArticulo idPlan={idPlanCapacidad} dic_articulo={articulo} eventClick={eventoOnclick} />
            </div>
        </div>
    );
}