import React, { useState, useEffect } from 'react'
import { get_subarea_operacion } from '../../services/subAreaServices';
import { get_plan_capacidad_pp_resumen } from '../../services/planCapacidadPPServices';
import { BotonExcel } from "../../components/Botones";
import { InputTextBuscar } from "../../components/InputTextBuscar";
import { PlanCapacidadPPPanel_tabla } from "./components/PlanCapacidadPPPanel_tabla";
import ComboBox from "../../components/ComboBox";
import Title from "../../components/Titulo";

// CARGAR MES 
const listAbc = [
    { "codigo": "A", "nombre": "A" },
    { "codigo": "B", "nombre": "B" },
    { "codigo": "C", "nombre": "C" }
]

export function PlanCapacidadPPPanel({ idPlan, eventOnclick }) {

    // CONSUMIR DATOS
    const [dataToEdit, setDataToEdit] = useState(null);
    const [idPlanCapacidad, setIdPlanCapacidad] = useState(idPlan);
    const [cod_subarea, setCod_subarea] = useState("0");
    const [abc, setAbc] = useState("0");

    const [listDatos, setListDatos] = useState([]);
    useEffect(() => {
        getData();
    }, [idPlanCapacidad, abc, cod_subarea])

    //
    const getData = () => {
        get_plan_capacidad_pp_resumen(idPlanCapacidad, cod_subarea, abc)
            .then(res => {
                setListDatos(res)
                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM 
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_articulo", "articulo", "abc", "nom_subarea"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA 
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // cargar combo familia     
    const [subArea, setSubArea] = useState([])
    useEffect(() => {
        get_subarea_operacion("1")
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.cod_subarea, "nombre": data.nom_subarea
                    }
                })
                setSubArea(cbo_data)
            })
    }, []);

    // Captura el evento y los valores de los inputs 
    const handleChange = (evento) => {
        if (evento.target.name === 'abc') {
            setAbc((evento.target.value).toString());
        }
        if (evento.target.name === 'cod_subarea') {
            setCod_subarea((evento.target.value).toString());
        }
    };

    // PAGINACIÓN  
    let LimitPag = 50;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // MOSTRAR MODAL

    // RETORNAR INFORMACIÓN 
    return (
        <div className="col-lg-12">

            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-sm-auto">
                            <Title>Capacidad de Producto en Proceso (General)</Title>
                        </div>
                        <div className="col-sm">
                            <div className="d-flex justify-content-end flex-wrap gap-1">
                                <InputTextBuscar onBuscarChange={onSearchChange} />
                                <ComboBox
                                    datosRow={listAbc}
                                    nombre_cbo="abc"
                                    manejaEvento={handleChange}
                                    valor_ini="ABC"
                                    valor={abc}
                                />
                                <ComboBox
                                    datosRow={subArea}
                                    nombre_cbo="cod_subarea"
                                    manejaEvento={handleChange}
                                    valor_ini="-- SUB AREA --"
                                    valor={cod_subarea}
                                />
                                <BotonExcel textoBoton={"Exportar"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatos} nombreArchivo={"Plan Capacidad PP"} />
                                <button className="btn btn-sm btn-dark" onClick={() => eventOnclick('aa')}>Regresar</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <PlanCapacidadPPPanel_tabla datos_fila={search(listDatos)} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-end m-2">
                    <div className="pagination-wrap hstack gap-1">
                        Total Reg. {listDatos.length}
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                        <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>

            {/* INICIO MODAL */}
            {/* FIN MODAL */}
        </div>
    );
}