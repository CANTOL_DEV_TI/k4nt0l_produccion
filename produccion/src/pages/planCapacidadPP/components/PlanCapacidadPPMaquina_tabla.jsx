import { BsSearch } from "react-icons/bs";

export function PlanCapacidadPPMaquina_tabla({ datos_fila, eventoClick, get_id_equipoDet }) {
    // RETORNAR INFORMACIÓN 
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered" style={{ overflowY: "auto", maxHeight: "735px", display: "flow-root" }}>
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" title="Opciones">Opc</th>
                            <th className="align-middle" title="Código Máquina">Código Máquina</th>
                            <th className="align-middle" title="Descripción Máquina">Descripción Máquina</th>
                            <th className="align-middle" title="Cantidad Programado (Und)">Cantidad Programado</th>
                            <th className="align-middle" title="Horas Oferta">Horas Oferta</th>
                            <th className="align-middle" title="Horas Programado">Horas Programado</th>
                            <th className="align-middle" title="Cumplimiento (%)">(%)</th>
                            <th className="align-middle" title="Sub Area">Sub Area</th>
                            <th className="align-middle" title="Tipo Equipo">Tipo Equipo</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1} className={`${datos.id_equipo === get_id_equipoDet ? 'table-warning' : ''}`}>
                                        <td>
                                            <button data-toggle="tooltip" title="Seleccionar" className="btn btn-outline-primary btn-sm py-0 px-1"
                                                onClick={() => eventoClick(datos, "M")}>
                                                <BsSearch />
                                            </button>
                                        </td>
                                        <td className="td-cadena">{datos.cod_equipo}</td>
                                        <td className="td-cadena" style={{ minWidth: "120px" }}>{datos.equipo}</td>
                                        <td className="td-num" style={{ minWidth: "60px" }}>
                                            {datos.cant_programado.toLocaleString("en")}
                                        </td>
                                        <td className="td-num" style={{ minWidth: "50px"}}>{datos.hora_oferta.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ minWidth: "50px" }}>{datos.hora_programado.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ minWidth: "60px" }}>
                                            <span className="badge" style={{ float: "left", backgroundColor: `${datos.porc_capacidad > 100 ? '#EC7063' : '#58D68D'}` }}>&nbsp;</span>
                                            {datos.porc_capacidad.toLocaleString("en")}
                                        </td>
                                        <td className="td-cadena">{datos.nom_subarea}</td>
                                        <td className="td-cadena">{datos.tipo_equipo}</td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="9">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}