import { BsPencilFill } from "react-icons/bs";

export function PlanCapacidadPPMaquinaArticulo_tabla({ datos_fila, eventoClick, get_cod_articulo }) {

    // RETORNAR INFORMACIÓN 
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered" style={{ overflowY: "auto", maxHeight: "250px", display: "flow-root" }}>
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" title="Opciones">Opc</th>
                            <th className="align-middle" title="Código artículo">Código Artículo</th>
                            <th className="align-middle" title="Número Paso">Num Paso</th>
                            <th className="align-middle" title="Cantidad Programado (Und)">Cantidad Programado</th>
                            <th className="align-middle" title="Total Cantidad Programado (Und)">Tot.Cant Programado</th>
                            <th className="align-middle" title="Hora Programado">Hora Programado</th>
                            <th className="align-middle" title="Descripción Artículo">Artículo</th>
                            <th className="align-middle" title="Stock Inicial">Stock Inicial</th>
                            <th className="align-middle" title="Stock Inicial">Stock Final</th>
                            <th className="align-middle" title="Cantidad Boom (Und)">Cantidad Boom</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1} className={`${datos.cod_articulo === get_cod_articulo ? 'table-warning' : ''}`}>
                                        <td>
                                            <button data-toggle="tooltip" title="Seleccionar" className="btn btn-warning btn-sm"
                                                onClick={() => eventoClick(datos, "A")}>
                                                <BsPencilFill />
                                            </button>
                                        </td>
                                        <td className="td-cadena">{datos.cod_articulo}</td>
                                        <td className="td-cadena">{datos.num_paso}</td>
                                        <td className="td-num fw-bold " style={{ minWidth: "50px", backgroundColor: "#FBF8EF" }}>{datos.cant_programado.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ minWidth: "50px" }}>{datos.cant_programado_tot.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ minWidth: "50px" }}>{datos.hora_programado.toLocaleString("en")}</td>
                                        <td className="td-cadena" style={{ minWidth: "120px" }}>{datos.articulo}</td>
                                        <td className="td-num" style={{ minWidth: "50px" }}>{datos.stock_inicial.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#F2F3F4", minWidth: "60px" }}>
                                            <span className="badge" style={{ float: "left", backgroundColor: `${datos.semaforo_final}` }}>&nbsp;</span>
                                            {datos.stock_final_almacen > 0 ? datos.stock_final_almacen.toLocaleString("en") : 0}
                                        </td>
                                        <td className="td-num" style={{ minWidth: "50px" }}>{datos.cant_boom.toLocaleString("en")}</td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="10">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}