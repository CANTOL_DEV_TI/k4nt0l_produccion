import React, { useState, useEffect } from 'react'

export function PlanCapacidadPPArticulo_tabla({ datos_fila, setDatos, get_id_equipo }) {

    // VARIABLES
    const [cantidadP, setCantidadP] = useState(0);

    // CAPTURAR EL EVENTO DE LOS IMPUTS
    const handleChange = (evento) => {
        // 1. Hacer una copia superficial de los elementos
        //let items = [...formularioDet];
        // 2. Haz una copia superficial del elemento que deseas mutar
        //let item = { ...items[evento.target.name - 1] }
        // 3. Reemplace la propiedad en la que está interesado
        //item.cant_hora_lab = evento.target.value;
        // 4. Vuelva a ponerlo en nuestra matriz. NÓTESE BIEN. *estamos* mutando la matriz aquí, pero por eso primero hicimos una copia
        //items[evento.target.name - 1] = item;
        // 5. Establecer el estado en nuestra nueva copia

        let id_equipo = evento.target.name;
        let c_cantidad = evento.target.value;

        const nuevoDato = datos_fila.map(obj => {
            if (obj.id_equipo.toString() === id_equipo.toString()) {
                // variables                
                const v_cant_programado = Number(c_cantidad);
                const v_hora_programado = (v_cant_programado / obj.std_und_hora).toFixed(2);
                const v_t_hora_programado_mod = (Number(obj.t_hora_programado) - Number(obj.hora_programado) + Number(v_hora_programado)).toFixed(2);

                return {
                    ...obj,
                    cant_programado_mod: v_cant_programado,
                    hora_programado_mod: v_hora_programado,
                    t_hora_programado_mod: v_t_hora_programado_mod
                };
            }

            return obj;

        });
        setDatos(nuevoDato);
    };

    // RETORNAR INFORMACIÓN 
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered" style={{ overflowY: "auto", maxHeight: "300px", display: "flow-root" }}>
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" rowSpan="2" title="Número Paso">Num Paso</th>
                            <th className="align-middle" rowSpan="2" title="Prioridad">Prioridad</th>
                            <th className="align-middle" style={{ minWidth: "120px" }} rowSpan="2" title="Descripción Máquina">Máquina</th>
                            <th className="align-middle" colSpan="2" title="Programado (Und)">Programado</th>
                            <th className="align-middle" rowSpan="2" title="Estandar Unidad por Hora">Estandar Unidad</th>
                            <th className="align-middle" colSpan="2" title="Total Horas Máquina">Total Horas</th>
                            <th className="align-middle" rowSpan="2" title="Código Máquina">Código Máquina</th>
                        </tr>
                        <tr>
                            <th className="align-middle" style={{ minWidth: "70px" }} title="Cantidad (Und)">Cantidad</th>
                            <th className="align-middle" title="Hora">Horas</th>
                            <th className="align-middle" title="Oferta">Oferta</th>
                            <th className="align-middle" title="Asignado">Programado</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1} className={`${datos.id_equipo === get_id_equipo ? 'table-warning' : ''}`}>
                                        <td className="td-cadena">{datos.num_paso}</td>
                                        <td className="td-cadena">{datos.num_prioridad}</td>
                                        <td className="td-cadena">{datos.equipo}</td>
                                        <td className="td-num">
                                            <input type="number" className="form-control form-control-sm" style={{ minWidth: "85px", textAlign: "right" }} name={datos.id_equipo} onChange={handleChange} value={datos.cant_programado_mod} />
                                        </td>
                                        <td className="td-num" style={{ minWidth: "50px" }}>{datos.hora_programado_mod}</td>
                                        <td className="td-cadena" >{datos.std_und_hora.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ minWidth: "50px", backgroundColor: "#F2F3F4" }}>{datos.t_hora_oferta}</td>
                                        <td className="td-num" style={{ minWidth: "50px" }}>
                                            <span className="badge" style={{ float: "left", backgroundColor: `${((datos.t_hora_programado_mod || 0) <= 0 ? datos.t_hora_programado : datos.t_hora_programado_mod || 0) > datos.t_hora_oferta ? '#EC7063' : '#58D68D'}` }}>&nbsp;</span>
                                            {(datos.t_hora_programado_mod || 0) <= 0 ? datos.t_hora_programado : datos.t_hora_programado_mod || 0}</td>
                                        <td className="td-num" style={{ minWidth: "50px" }}>{datos.cod_equipo}</td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="9">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}