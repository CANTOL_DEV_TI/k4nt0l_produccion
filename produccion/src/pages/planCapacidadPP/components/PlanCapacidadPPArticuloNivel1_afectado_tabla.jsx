import React, { useState, useEffect } from 'react'
import { BsCheckCircleFill } from "react-icons/bs";

export function PlanCapacidadPPArticuloNivel1_afectado_tabla({ datos_fila }) {

    // VARIABLES

    // RETORNAR INFORMACIÓN 
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-success table-hover table-sm table-bordered" style={{ overflowY: "auto", maxHeight: "300px", display: "flow-root" }}>
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" title="Código Artículo">Cod.Artículo</th>
                            <th className="align-middle" title="Descripción de Artículo">Artículo</th>
                            <th className="align-middle" title="Paso">N.Paso</th>
                            <th className="align-middle" title="Prioridad">Prioridad</th>
                            <th className="align-middle" title="Id Equipo">Id.Equipo</th>
                            <th className="align-middle" title="Código Equipo">Cod.Equipo</th>
                            <th className="align-middle" title="Descripción de Equipo">Equipo</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td className="td-cadena">{datos.cod_articulo}</td>
                                        <td className="td-cadena">{datos.articulo}</td>
                                        <td className="td-cadena">{datos.num_paso}</td>
                                        <td className="td-cadena">{datos.num_prioridad}</td>
                                        <td className="td-cadena">{datos.id_equipo}</td>
                                        <td className="td-cadena">{datos.cod_equipo}</td>
                                        <td className="td-cadena">{datos.equipo}</td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="7">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}