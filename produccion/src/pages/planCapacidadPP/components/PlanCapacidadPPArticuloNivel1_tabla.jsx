import React, { useState, useEffect } from 'react'
import { BsCheckCircleFill } from "react-icons/bs";

export function PlanCapacidadPPArticuloNivel1_tabla({ datos_fila, setDatos, eventoClick }) {

    // VARIABLES

    // CAPTURAR EL EVENTO DE LOS IMPUTS
    const handleChange = (evento) => {
        let c_cod_articulo = evento.target.name;
        let c_cantidad = evento.target.value;
        const nuevoDato = datos_fila.map(obj => {
            if (obj.cod_articulo.toString() === c_cod_articulo.toString()) {
                // variables                
                const v_cant_programado = Number(c_cantidad);

                return {
                    ...obj,
                    cant_programado_mod: v_cant_programado,
                };
            }

            return obj;

        });
        setDatos(nuevoDato);
    };

    // RETORNAR INFORMACIÓN 
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered" style={{ overflowY: "auto", maxHeight: "300px", display: "flow-root" }}>
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" title="Opciones">Opc</th>
                            <th className="align-middle" title="Código Artículo">Cod.Artículo</th>
                            <th className="align-middle" title="Descripción de Artículo">Artículo</th>
                            <th className="align-middle" title="Cantidad programado (UND)">Cant.Programado</th>
                            <th className="align-middle" title="Cantidad Objetivo (UND)">Cant.Objetivo</th>
                            <th className="align-middle" title="Cantidad Boom de PT">Cant.Boom</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td>
                                            <button data-toggle="tooltip" title="Grabar Cambio" className="btn btn-success btn-sm"
                                                onClick={() => eventoClick(datos, "M")}>
                                                <BsCheckCircleFill />
                                            </button>
                                        </td>
                                        <td className="td-cadena">{datos.cod_articulo}</td>
                                        <td className="td-cadena">{datos.articulo}</td>
                                        <td className="td-num">
                                            <input type="number" className="form-control form-control-sm" style={{ minWidth: "85px", textAlign: "right" }} name={datos.cod_articulo} onChange={handleChange} value={datos.cant_programado_mod} />
                                        </td>
                                        <td className="td-num">{datos.cant_objetivo.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.cant_boom.toLocaleString("en")}</td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="6">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}