import React, { useState, useEffect } from 'react'
import { get_plan_capacidad_pp_equipo_articulo } from '../../services/planCapacidadPPServices';
import { PlanCapacidadPPMaquinaArticulo_tabla } from "./components/PlanCapacidadPPMaquinaArticulo_tabla";
import Title from "../../components/Titulo";


export function PlanCapacidadPPMaquinaArticulo({ idPlan, dic_equipo, eventClick }) {

    // VARIABLES Y DATOS
    const [idPlanCapacidad, setIdPlanCapacidad] = useState(idPlan);
    const [codArticulo, setCodArticulo] = useState(0);

    const [listDatos, setListDatos] = useState([]);
    useEffect(() => {
        getData();
    }, [dic_equipo])

    //
    const getData = () => {
        get_plan_capacidad_pp_equipo_articulo(idPlanCapacidad, dic_equipo.id_equipo)
            .then(res => {
                setListDatos(res)
            })
    };

    // BUSQUEDA EN DOM

    // EVENTOS
    const eventOnclick = (datos, v_modo = "") => {
        setCodArticulo(datos.cod_articulo);
        eventClick(datos, v_modo);
    };

    // MOSTRAR MODAL

    // RETORNAR INFORMACIÓN 
    return (
        <div className="col-lg-12">

            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header">
                    <div className="row gy-2">
                        <div className="col-lg-12">
                            <table className="table table-sm table-bordered caption-top mb-0">
                                <caption><b>Máquina: </b>{dic_equipo.cod_equipo} - {dic_equipo.equipo}</caption>
                                <thead className="table-dark text-center">
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">&sum; Oferta</th>
                                        <th scope="col">&sum; Programado</th>
                                        <th scope="col">Mant.Preventivo</th>
                                        <th scope="col">N° Turno</th>
                                        <th scope="col">Disponibilidad (%)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="col" style={{ backgroundColor: "#FCF3CF" }}>Horas</th>
                                        <td className="td-num" style={{ backgroundColor: "#FCF3CF" }}>{dic_equipo.hora_oferta}</td>
                                        <td className="td-num" style={{ backgroundColor: "#FCF3CF" }}>{dic_equipo.hora_programado}</td>
                                        <td className="td-num" style={{ backgroundColor: "#FCF3CF" }}>{dic_equipo.preventivo}</td>
                                        <td className="td-num">{dic_equipo.total_turnos || 0}</td>
                                        <td className="td-num">{dic_equipo.disponibilidad_historico}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <PlanCapacidadPPMaquinaArticulo_tabla datos_fila={listDatos} eventoClick={eventOnclick} get_cod_articulo={codArticulo} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                {/* FIN PAGINACION */}
            </div>

            {/* INICIO MODAL */}
            {/* FIN MODAL */}
        </div>
    );
}