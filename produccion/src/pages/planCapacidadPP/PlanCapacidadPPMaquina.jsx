import React, { useState, useEffect } from 'react'
import { get_subarea_operacion } from '../../services/subAreaServices';
import { get_plan_capacidad_pp_equipo } from '../../services/planCapacidadPPServices';
import { BotonExcel } from "../../components/Botones";
import { InputTextBuscar } from "../../components/InputTextBuscar";
import { PlanCapacidadPPMaquina_tabla } from "./components/PlanCapacidadPPMaquina_tabla";
import ComboBox from "../../components/ComboBox";
import Title from "../../components/Titulo";


export function PlanCapacidadPPMaquina({ idPlan, eventClick, refrescarDato, get_id_equipo }) {

    // VARIABLES Y DATOS
    const [idPlanCapacidad, setIdPlanCapacidad] = useState(idPlan);
    const [cod_subarea, setCod_subarea] = useState("0");


    const [listDatos, setListDatos] = useState([]);
    useEffect(() => {
        getData();
    }, [idPlanCapacidad, cod_subarea, refrescarDato])

    //
    const getData = () => {
        get_plan_capacidad_pp_equipo(idPlanCapacidad, cod_subarea)
            .then(res => {
                setListDatos(res)
            })
    };

    // BUSQUEDA EN DOM 
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_equipo", "equipo", "nom_subarea"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos;

        // SI HAY VALOR EN CAJA DE BUSQUEDA 
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered;
    };

    // cargar combo familia     
    const [subArea, setSubArea] = useState([])
    useEffect(() => {
        get_subarea_operacion("1")
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.cod_subarea, "nombre": data.nom_subarea
                    }
                })
                setSubArea(cbo_data)
            })
    }, []);

    // Captura el evento y los valores de los inputs 
    const handleChange = (evento) => {
        if (evento.target.name === 'cod_subarea') {
            setCod_subarea((evento.target.value).toString());
        }
    };

    // PAGINACIÓN   

    const onSearchChange = (e) => {
        setBuscarDato(e.target.value.toLowerCase().trim());
    }
    
    // EVENTOS
    const eventOnclick = (datos, v_modo = "") => {
        eventClick(datos, v_modo);
    };

    // MOSTRAR MODAL

    // RETORNAR INFORMACIÓN 
    return (
        <div className="col-lg-12">
            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-12 d-flex">
                            <div className="p-2 flex-grow-1">
                                <Title>Capacidad por Máquina</Title>
                            </div>
                            <div className="p-2">
                                <BotonExcel textoBoton={"Exportar"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatos} nombreArchivo={"Capacidad de maquina PP"} />&nbsp;
                                <button className="btn btn-sm btn-dark" onClick={() => eventOnclick("", "ATRAS")}>Regresar</button>
                            </div>
                        </div>
                        <div className="col-sm">
                            <div className="d-flex justify gap-1">
                                <ComboBox
                                    datosRow={subArea}
                                    nombre_cbo="cod_subarea"
                                    manejaEvento={handleChange}
                                    valor_ini="-- SUB AREA --"
                                    valor={cod_subarea}
                                />
                                <InputTextBuscar onBuscarChange={onSearchChange} />
                            </div>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <PlanCapacidadPPMaquina_tabla datos_fila={search(listDatos)} eventoClick={eventOnclick} get_id_equipoDet={get_id_equipo} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                {/* FIN PAGINACION */}
            </div>

            {/* INICIO MODAL */}
            {/* FIN MODAL */}
        </div>
    );
}