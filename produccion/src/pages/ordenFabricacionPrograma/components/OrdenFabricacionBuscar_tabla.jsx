import { BotonAdd } from "../../../components/Botones"; 
 
export function OrdenFabricacionBuscar_tabla({ datos_fila, eventoClick }) { 
    // RETORNAR INFORMACIÓN 
    return ( 
        <> 
            {/* INICIO TABLA */} 
            <div className="table-responsive"> 
                <table className="table table-hover table-sm table-bordered"> 
                    <thead className="table-secondary text-center table-sm">
                    <tr>
                        <th className="align-middle" title="Opciones">Opc</th>
                        <th className="align-middle">Fecha</th>
                        <th className="align-middle">N° Orden Fab.</th>
                        <th className="align-middle">Cod. Artículo</th>
                        <th className="align-middle">Artículo</th>
                        <th className="align-middle">Estado</th>
                    </tr>
                    </thead>
                    <tbody className="list"> 
                        {datos_fila.length > 0 ? 
                            ( 
                                datos_fila.map((datos, index) => { 
                                    return <tr key={index + 1}>
                                        <td title="Seleccionar">
                                            <BotonAdd eventoClick={() => eventoClick(datos)} textoBoton={"Add"}
                                                      title={"Seleccionar"}/>
                                        </td>
                                        <td className="td-cadena">{datos.fecha_doc}</td>
                                        <td className="td-cadena">{datos.num_of}</td>
                                        <td className="td-cadena">{datos.cod_articulo}</td>
                                        <td className="td-cadena">{datos.articulo}</td>
                                        <td className="td-cadena">{datos.estado_desc}</td>
                                    </tr>
                                })
                            ) : 
                            ( 
                                <tr> 
                                    <td className="text-center" colSpan="3">Sin datos</td> 
                                </tr> 
                            ) 
                        } 
                    </tbody> 
                </table> 
            </div> 
            {/* FIN TABLA */} 
        </> 
    ); 
}