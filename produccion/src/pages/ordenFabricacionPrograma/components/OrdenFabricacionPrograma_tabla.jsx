import { BsSearch } from "react-icons/bs";

export function OrdenFabricacionPrograma_tabla({ datos_fila, eventoClick }) {

    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" title="Opciones" style={{ width: '45px' }}>Opc</th>
                            <th className="align-middle" title="Código Artículo">Cod.Artículo</th>
                            <th className="align-middle" title="Artículo">Artículo</th>
                            <th className="align-middle" title="Unidad de medida de uso" >UND</th>
                            <th className="align-middle" title="Unidad de medida de uso" style={{ width: '70px' }}>Avance (%)</th>
                            <th className="align-middle" title="Cantidad Planeado" >Cantidad Planeado</th>
                            <th className="align-middle" title="Cantidad Producido" >Cantidad Producido</th>
                            <th className="align-middle" title="Ordenes de fabricación en Proceso" >Ordenes Proceso</th>
                            <th className="align-middle" title="Cantidad Pendiente" >Cantidad Pendiente</th>
                            <th className="align-middle" title="Lote Minimo" >Lote Minimo</th>
                            <th className="align-middle" title="ABC" >ABC</th>
                            <th className="align-middle" title="Stock Disponible" >Stock Disponible</th>
                            <th className="align-middle" title="Unidades por Hora" >Unidad Hora</th>
                            <th className="align-middle" title="Horas Proceso" >Horas Proceso</th>
                            <th className="align-middle" title="Horas procesos requerido" >H.Proceso Requerido</th>
                            <th className="align-middle" title="Consumo promedio diario historico" >CPD Historico</th>
                            <th className="align-middle" title="Sub Area" >Sub Area</th>                            
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td>
                                            <button data-toggle="tooltip" title="Ver Historial" className="btn btn-outline-info btn-sm ms-1">
                                                <BsSearch />
                                            </button>
                                        </td>
                                        <td className="td-cadena">{datos.cod_articulo}</td>
                                        <td className="td-cadena">{datos.articulo}</td>
                                        <td className="td-cadena">{datos.umu}</td>
                                        <td className="td-num" style={{ backgroundColor: "#EBEDEF" }}>
                                            <span className="badge rounded-pill" style={{ float: "left", backgroundColor: `${datos.semaforo}` }}>&nbsp;</span>
                                            {datos.cant_avance.toLocaleString("en")}
                                        </td>
                                        <td className="td-num" style={{ backgroundColor: "#F4F6F6" }}>{datos.cant_plan.toLocaleString("en")}</td>
                                        <td className="td-num" style={{ backgroundColor: "#E9F7EF" }}>{datos.cant_real.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.cant_proceso.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.cant_pendiente.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.lote_min.toLocaleString("en")}</td>
                                        <td className="td-cadena">{datos.abc}</td>
                                        <td className="td-num">{datos.cant_stock.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.und_x_hora.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.hora_proceso.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.hora_plan.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.cpdh.toLocaleString("en")}</td>
                                        <td className="td-cadena">{datos.area}</td>                                        
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="17">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}