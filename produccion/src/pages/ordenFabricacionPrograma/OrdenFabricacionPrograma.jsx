import React, { useState, useEffect } from 'react'
import {
    BsCheckLg,
    BsExclamationLg,
    BsQuestionLg,
} from "react-icons/bs";
import { get_subarea_operacion } from '../../services/subAreaServices.jsx';
import { get_subarea_propietario } from '../../services/subAreaServices';
import { get_ordenFabricacionPrograma, get_ordenFabricacionCMO } from '../../services/orderFabricacionServices';
import { get_grupo_pp_pt } from '../../services/articuloGrupoService';
import { BotonExcel } from "../../components/Botones";
import { InputTextBuscarV2 } from "../../components/InputTextBuscar";
import { useVentanaModal } from "../../hooks/useVentanaModal";
import { OrdenFabricacionPrograma_tabla } from "./components/OrdenFabricacionPrograma_tabla";
import { ComboBoxForm } from "../../components/ComboBox";
import VentanaModal from "../../components/VentanaModal";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

const v_fecha = new Date();
const iniFormulario = {
    fecha: v_fecha.toLocaleDateString('en-GB', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
    }).split('/').reverse().join('-'),
    id_grupo: "102",
    cod_subarea: "0",
    cod_propietario: "0",
    sw_todo: "0",
};

export function OrdenFabricacionPrograma() {
    // seguridad
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
    const Seguridad = async () => {
        const ValSeguridad = await Validar("PROOFP")
        setVentanaSeguridad(ValSeguridad.Validar)
    }

    const CostoMO = async (data) => {
        const CMO = await get_ordenFabricacionCMO(data)
        setCosto(CMO)
    }

    // CRUD ID
    const [formulario, setFormulario] = useState(iniFormulario);
    const [checked, setChecked] = useState(false);
    const [dataToEdit, setDataToEdit] = useState(null);
    // BUSQUEDA GENERALES    
    function datosBusqueda(evento) {
        if (evento.target.name === 'sw_todo') {
            setChecked(!checked);
            setFormulario({
                ...formulario,
                [evento.target.name]: evento.target.checked === true ? "1" : "0",
            });
        } else {
            setFormulario({
                ...formulario,
                [evento.target.name]: (evento.target.value).toUpperCase(),
            });
        }
    };

    // cargar combos area y grupo
    const [listPropietario, setListPropietario] = useState([]);
    const [listArea, setListArea] = useState([]);
    const [listGrupo, setListGrupo] = useState([]);
    useEffect(() => {
        Seguridad();

        get_subarea_operacion("1")
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.cod_subarea_sap, "nombre": data.nom_subarea
                    }
                })
                setListArea(cbo_data)
            });
        get_grupo_pp_pt()
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.id_grupo, "nombre": data.grupo
                    }
                })
                setListGrupo(cbo_data)
            });
        get_subarea_propietario()
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.cod_propietario, "nombre": data.propietario
                    }
                })
                setListPropietario(cbo_data)
            });

    }, [])

    // CONSUMIR DATOS
    const [horaPLan, setHoraPLan] = useState(0);
    const [horaReal, setHoraReal] = useState(0);
    const [horaPorc, setHoraPorc] = useState(0);
    const [HoraPerdida, setHoraPerdida] = useState(0);
    const [undPLan, setUndPLan] = useState(0);
    const [undReal, setUndReal] = useState(0);
    const [undTReal, setUndTReal] = useState(0);
    const [undPorc, setUndPorc] = useState(0);
    const [Costo, setCosto] = useState(0);
    const [TotalHPMO, setTotalHPMO] = useState(0);
    const [refrescar, setRefrescar] = useState(true)
    const [listDatos, setListDatos] = useState([])
    const [listDatosMO, setListDatosMO] = useState([])



    useEffect(() => {

        setRefrescar(false);
        getData();
        getDataMO();


    }, [refrescar, formulario])

    const getDataMO = () => {
        get_ordenFabricacionCMO(formulario)
            .then(res => {
                try {
                    setCosto(0);
                    setListDatosMO(res);

                    if (res.length === 0)
                        return;

                    let c_UMO = res.reduce((total, currItem) => total + currItem.COSTOMO)
                    setCosto(c_UMO.COSTOMO)

                    console.log(Costo)
                }
                catch {
                    console.log(Costo)
                }
            }
            )
    }
    //    
    const getData = () => {
        get_ordenFabricacionPrograma(formulario)
            .then(res => {
                // totales
                setHoraPLan(0);
                setHoraReal(0);
                setHoraPorc(0);
                setHoraPerdida(0);
                setUndPLan(0);
                setUndReal(0);
                setUndTReal(0);
                setUndPorc(0);
                setTotalHPMO(0);
                setListDatos(res);

                if (res.length === 0)
                    return;
                getDataMO();
                // calcular horas
                let c_horaPlan = res.reduce((total, currItem) => total + currItem.hora_plan, 0);
                let c_horaReal = res.reduce((total, currItem) => total + currItem.hora_real, 0);
                let c_horaporc = 0;
                let c_horaPerdida = c_horaReal - c_horaPlan;
                if (c_horaPlan > 0) { c_horaporc = ((c_horaReal / c_horaPlan) * 100) }

                setHoraPLan(Math.round(c_horaPlan * 100) / 100)
                setHoraReal(Math.round(c_horaReal * 100) / 100)
                setHoraPorc(c_horaporc.toFixed(2))
                setHoraPerdida(c_horaPerdida.toFixed(2));


                // calculando de unidades
                let c_undPlan = res.reduce((total, currItem) => total + currItem.cant_plan, 0);
                let c_undReal = res.reduce((total, currItem) => total + currItem.cant_real_plan, 0);
                let c_undRealT = res.reduce((total, currItem) => total + currItem.cant_real, 0);
                let c_undporc = 0;
                if (c_undPlan > 0) { c_undporc = ((c_undReal / c_undPlan) * 100) };
                let c_total_hpmo = (c_horaPerdida * Costo) * -1;

                setUndPLan(Math.round(c_undPlan * 100) / 100);
                setUndReal(Math.round(c_undReal * 100) / 100);
                setUndTReal(Math.round(c_undRealT * 100) / 100);
                setUndPorc(c_undporc.toFixed(2));
                setTotalHPMO(c_total_hpmo.toFixed(2));

                //console.log(HoraPerdida)
                //console.log(Costo)
                //console.log(TotalHPMO.toLocaleString("en"))
                setCurrentPage(0);
            })
            //.catch(error => console.error(error));
    };

    // BUSQUEDA EN DOM
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_articulo", "articulo"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // PAGINACIÓN 
    let LimitPag = 20;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // MOSTRAR MODAL
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = (opc = "N", datos) => {
        if (opc === "E") {
            setDataToEdit(datos);
        } else {
            setDataToEdit(null);
        }
        handleShow(true)
    };

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">

            {/* INICIO TARJETAS */}
            <div className="row align-items-center mb-2 g-3">
                {/* Horas proceso */}
                <div className="col-xl-4 col-sm-4">
                    <div className="card">
                        <div className="card-body">
                            <div className="d-flex align-items-center">
                                <div className="flex-grow-1 overflow-hidden">
                                    <p className="text-uppercase fw-semibold text-truncate mb-0" title="Cumplimiento Horas Proceso">Horas Proceso</p>
                                </div>
                                <div className="flex-shrink-0">
                                    <h5 className="text-danger fw-semibold fs-14 mb-0">
                                        <i className="ri-arrow-right-down-line fs-13 align-middle" /> {horaPorc} %
                                    </h5>
                                </div>
                            </div>
                            <div className="px-2 py-2 mt-1">
                                <div className="progress mt-2" style={{ height: "8px" }}>
                                    <div className="progress-bar bg-success opacity-75" role="progressbar" style={{ width: `${horaPorc + '%'}` }}
                                        aria-valuenow={horaPorc} aria-valuemin={0} aria-valuemax={100} />
                                </div>
                            </div>
                            <ul className="list-inline main-chart text-center mb-0">
                                <li className="list-inline-item text-black-50 m-0 px-4">
                                    <p className="fw-medium fw-semibold m-0">Plan: <span className="d-inline-block align-middle ms-1">{horaPLan.toLocaleString("en")}</span></p>
                                </li>
                                <li className="list-inline-item text-success opacity-75 m-0 px-4">
                                    <p className="fw-medium fw-semibold m-0">Real:<span className="d-inline-block align-middle ms-2">{horaReal.toLocaleString("en")}</span></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                {/* Comparacion Perdidas */}
                <div className="col-xl-4 col-sm-4">
                    <div className="card">
                        <div className="card-body">
                            <div className="d-flex align-items-center">
                                <div className="flex-grow-1 overflow-hidden">
                                    <p className="text-uppercase fw-semibold text-truncate mb-0" title="Cumplimiento Horas Proceso">Costo Perdidas</p>
                                </div>
                                <div className="flex-shrink-0">
                                    <h5 className="text-danger fw-semibold fs-14 mb-0">

                                    </h5>
                                </div>
                            </div>


                            <ul className="list-inline main-chart text-center mb-0">
                                <li className="list-inline-item text-primary opacity-75 m-0 px-4">
                                    {formulario.cod_subarea != 0 ?
                                        <p className="fw-medium fw-semibold m-0">Cant. Horas : <span className="d-inline-block align-middle ms-1">{Number(HoraPerdida).toLocaleString("en")}</span></p>
                                        :
                                        <p className="fw-medium fw-semibold m-0">Cant. Horas : <span className="d-inline-block align-middle ms-1">{"00.00"}</span></p>
                                    }
                                </li>
                                <li className="list-inline-item text-success opacity-75 m-0 px-4">
                                    {formulario.cod_subarea != 0 ?
                                        <p className="fw-medium fw-semibold m-0">Costo Area S/. :<span className="d-inline-block align-middle ms-2">{Costo.toLocaleString("en")}</span></p>
                                        :
                                        <p className="fw-medium fw-semibold m-0">Costo Area S/. :<span className="d-inline-block align-middle ms-2">{"00.00"}</span></p>
                                    }
                                </li>
                                <li className="text-danger fw-semibold fs-14 mb-0">
                                    {formulario.cod_subarea != 0 ?
                                        <p className="fw-medium fw-semibold m-0">Total S/. :<span className="d-inline-block align-middle ms-2">{Number(TotalHPMO).toLocaleString("en")}</span></p>
                                        :
                                        <p className="fw-medium fw-semibold m-0">Total S/. :<span className="d-inline-block align-middle ms-2">{"00.00"}</span></p>
                                    }
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                {/* Horas proceso */}
                <div className="col-xl-4 col-sm-4">
                    <div className="card">
                        <div className="card-body">
                            <div className="d-flex align-items-center">
                                <div className="flex-grow-1 overflow-hidden">
                                    <p className="text-uppercase fw-semibold text-truncate mb-0" title="Cumplimiento Unidades">Unidades</p>
                                </div>
                                <div className="flex-shrink-0">
                                    <h5 className="text-danger fw-semibold fs-14 mb-0">
                                        <i className="ri-arrow-right-down-line fs-13 align-middle" /> {undPorc} %
                                    </h5>
                                </div>
                            </div>
                            <div className="px-2 py-2 mt-1">
                                <div className="progress mt-2" style={{ height: "8px" }}>
                                    <div className="progress-bar bg-success opacity-75" role="progressbar" style={{ width: `${undPorc + '%'}` }}
                                        aria-valuenow={undPorc} aria-valuemin={0} aria-valuemax={100} />
                                </div>
                            </div>
                            <ul className="list-inline main-chart text-center mb-0">
                                <li className="list-inline-item text-black-50 m-0 px-3">
                                    <p className="fw-medium fw-semibold m-0">Plan: <span className="d-inline-block align-middle ms-1">{undPLan.toLocaleString("en")}</span></p>
                                </li>
                                <li className="list-inline-item text-success opacity-75 m-0 px-3">
                                    <p className="fw-medium fw-semibold m-0" title="Producción real ajustado al plan">Real:<span className="d-inline-block align-middle ms-2">{undReal.toLocaleString("en")}</span></p>
                                </li>
                                <li className="list-inline-item text-dark m-0 px-3">
                                    <p className="fw-medium fw-semibold m-0" title="Producción total, considerando las unidades que superan al plan">Real General:<span className="d-inline-block align-middle ms-2">{undTReal.toLocaleString("en")}</span></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            {/* FIN TARJETAS */}

            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border-0 card-header">
                    <div className="align-items-center gy-3 row">
                        <div className="col-sm">
                            <h4 className="card-title mb-0">Programa de Fabricación</h4>
                        </div>
                        <div className="col-sm-auto">
                            <div className="input-group input-group-sm">
                                <span className="input-group-text fw-bold" id="inputGroup-sizing-sm">Período</span>
                                <input type="date" min="2020-01-01" name="fecha" className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" data-date-format="mm/YYYY"
                                    onChange={datosBusqueda} value={formulario.fecha} />
                                <BotonExcel textoBoton={"Excel"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatos} nombreArchivo={"Programa Mensual Produccion"} />
                            </div>

                        </div>
                    </div>
                </div>

                <div className="p-0 card-body">
                    <div className="mx-1 my-2 row">
                        <div className="col-xl-4 col-sm-6 p-1">
                            <InputTextBuscarV2 onBuscarChange={onSearchChange} />
                        </div>
                        <div className="col-xl-2 col-sm-6 p-1">
                            <ComboBoxForm
                                datosRow={listArea}
                                nombre_cbo="cod_subarea"
                                manejaEvento={datosBusqueda}
                                valor_ini="Area Responsable"
                                valor={formulario.cod_subarea}
                            />
                        </div>
                        <div className="col-xl-2 col-sm-4 p-1">
                            <ComboBoxForm
                                datosRow={listGrupo}
                                nombre_cbo="id_grupo"
                                manejaEvento={datosBusqueda}
                                valor_ini="Grupo Artículo"
                                valor={formulario.id_grupo}
                            />
                        </div>
                        <div className="col-xl-2 col-sm-4 p-1">
                            <ComboBoxForm
                                datosRow={listPropietario}
                                nombre_cbo="cod_propietario"
                                manejaEvento={datosBusqueda}
                                valor_ini="Supervisor Responsable"
                                valor={formulario.cod_propietario}
                            />
                        </div>
                        <div className="col-xl-2 col-sm-4 p-1">
                            <div className="form-check">
                                <input className="form-check-input" type="checkbox" id="flexCheckDefault" name="sw_todo" checked={checked} onChange={datosBusqueda} />
                                <label className="form-check-label" htmlFor="flexCheckDefault">
                                    Incluir Programado [0]
                                </label>
                            </div>

                        </div>
                    </div>

                    {/* FIN BARRA DE NAVEGACION */}

                    { /* INICIO TABLA */}
                    <OrdenFabricacionPrograma_tabla datos_fila={search(listDatos)} eventoClick={mostrarVentana} />

                    {/* FIN TABLA */}

                    {/* INICIO PAGINACION */}
                    <div className="d-flex justify-content-end m-2">
                        <div className="pagination-wrap hstack gap-1">
                            Total Reg. {listDatos.length}
                            <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                            <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                        </div>
                    </div>
                    {/* FIN PAGINACION */}
                </div>
            </div>

            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="lg"
                handleClose={handleClose}
                titulo="OrdenFabricacionProgramas histórico de Artículo"
            >

            </VentanaModal>
            {/* FIN MODAL */}

            {/* seguridad */}
            <VentanaBloqueo show={VentanaSeguridad} />

        </div >
    );
}


/*

<i className="ri-arrow-right-down-line fs-13 align-middle" /> {horaPorc} %

                            <div className="px-2 py-2 mt-1">
                                <div className="progress mt-2" style={{ height: "8px" }}>
                                    <div className="progress-bar bg-success opacity-75" role="progressbar" style={{ width: `${horaPorc + '%'}` }}
                                        aria-valuenow={horaPorc} aria-valuemin={0} aria-valuemax={100} />
                                </div>
                            </div>


*/