import { validaToken } from "../../../services/usuarioServices";
export async function ValidarSesion_Repro () {
    try{
        let Val = undefined

        const token = sessionStorage.getItem("CDTToken","XXXXX");
                
        if (token === "") {        
            Val = undefined
            console.log("No entra Vacio")
        } else {
            if(token === null) {
                Val = undefined
                console.log("No entra Null")
            }else{
                const valor = await validaToken(token);
                const usuario_login = valor.sub.substr(3);
                return {Usuario_Codigo: usuario_login}
            }
        }
    }
    catch{
        return undefined
    }
};