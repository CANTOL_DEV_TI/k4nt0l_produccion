import axios from "axios";
import { Url_Reproceso } from "../../../constants/global";
import { useEffect } from "react";

async function getSimulations(usuarioid, dynamic_offset){
    const response1 = await axios(`${Url_Reproceso}/produccion/reproceso/simulacion?usuarioid=${usuarioid}&dynamic_offset=${dynamic_offset}`);
    const response2 = await axios(`${Url_Reproceso}/produccion/reproceso/area/usuario?iduser=${usuarioid}`);
    const response3 = await axios(`${Url_Reproceso}/produccion/reproceso/simulacion/total_by_userid?usuarioid=${usuarioid}`);
    return {simulaciones: response1.data, subareas_user: response2.data.data, total_sim: response3.data.data}
}

async function useGetSimulations (setSimulationsList, dynamic_offset, setUserSa, adminName ) {
    const fetchData = async () => {
        try{
            let {simulaciones: simulations, subareas_user, total_sim} = await getSimulations(adminName, dynamic_offset);
            setSimulationsList({data: simulations, total: total_sim});
            setUserSa(subareas_user);
          }
          catch{
              setSimulationsList([]);
          }
    }
    
    useEffect(()=>{
        if(adminName !== "XXX"){
            fetchData()
        }
    },[adminName, dynamic_offset])

};

export {useGetSimulations}