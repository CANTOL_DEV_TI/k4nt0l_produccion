import { useEffect } from 'react'
import axios from 'axios'

function useFetchRepro(setList, dir_, initList = []) {
  
  const fetchData = async (dir_dir) => {
    try{
        const response = await axios.get(dir_dir);
        let data = response.data
          !initList.length && setList([...data.data])
          !!initList.length && setList(initList.concat([...data.data]))
          setList([data.data][0])
      }
      catch{
        setList([]);
      }
  }
  
  // Fetch the product list on component mount
  useEffect(()=>{
      try {  
          if(!(dir_ === '')){
            fetchData(dir_)
          }else{
            setList([])
          }
        } catch (error) {
          console.error('Error fetching product list:', error);
        }
  },[]);
}



export { useFetchRepro }
