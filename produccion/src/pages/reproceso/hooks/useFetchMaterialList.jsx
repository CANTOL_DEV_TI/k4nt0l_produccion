
import { useEffect } from 'react'
import axios from 'axios'
import { Url_Reproceso } from '../../../constants/global';

function useFetchMaterialList(setList, idItem, currentSubArea) {
    let dir_ = `${Url_Reproceso}/produccion/reproceso/articulo/material_repro?odParameter=${idItem}&subareaName=${currentSubArea.toUpperCase()}&isAll=${false?'1':'0'}`
    let url_ = `${Url_Reproceso}/produccion/reproceso/articulo/failarticles?odArticle=${idItem}&subarea=${currentSubArea}`
    
    const fetchData = async () => {
      try{
          if(!(dir_ === '')){
            const response1 = await axios.get(dir_);
            let status_ = response1.status
            let data = [response1.data.data]
            if(status_ === 200){
              try{
                const response2 = await axios.get(url_)
                if (response2.status === 200){
                  let data2 = response2.data.data
                  let Stock = data2.map((x)=>(x.stock))
                  let WareName = data2.map((x)=>(x.WareCod))
                  let materialListTmp = [...data]
                  materialListTmp[0].qtyRejected = Stock
                  materialListTmp[0].whName = WareName
                  setList(materialListTmp)
                }
              }catch(error){
                console.log(`An error ocurred ${error}`)
              }
            }
          }else{
            console.log(`An error ocurred`)
          }
        }
        catch(error){
          console.log(`An error ocurred ${error}`);
        }
    }
    
    // Fetch the product list on component mount
    useEffect(() => {
        try {
          fetchData();  
          } catch (error) {
            console.error('Error fetching product list:', error);
          }
    },[]);
  }

  export { useFetchMaterialList }