import axios from "axios";
import { Url_Reproceso } from "../../../constants/global";
import { validaToken } from "../../../services/usuarioServices";

async function validaRol(usuarioid, rol_id){
    const response = await axios(`${Url_Reproceso}/produccion/reproceso/usuario/validarol?usuarioid=${usuarioid}`);
    return response.data.data
}

async function useValidaRol (setAdminName, rol_id, userName) {
    try{
        const token = sessionStorage.getItem("CDTToken","XXXXX");
        if (token === "") {        
            setAdminName('')
        } else {
            if(token === null) {
                setAdminName('')
            }else{
                const valor = await validaToken(token);
                //obtiene el dato de usuario
                const usuario_login = valor.sub.substr(3);
                let isvalidated = await validaRol(usuario_login, rol_id);
                if (isvalidated){
                    setAdminName(usuario_login);
                }else{
                    setAdminName('');
                }
            }
        }
    }
    catch{
        setAdminName('')
    }
};

async function ValidaRol_by_UserId (userid, rol_id) {
    try{
        let isvalidated = await validaRol(userid, rol_id);
        if (isvalidated){
            return true;
        }else{
            return false;
        }
    }
    catch{
        return false;
    }
};

export {useValidaRol, ValidaRol_by_UserId}