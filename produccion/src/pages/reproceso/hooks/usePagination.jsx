import React from 'react'

function usePagination(setDinamic_Offset, dynamic_offset, Step_Offset) {
    const nextPage = () => {
        setDinamic_Offset(dynamic_offset + Step_Offset)
    }
    
    const prevPage = () => {
        if(dynamic_offset >= Step_Offset){
            setDinamic_Offset(dynamic_offset - Step_Offset)
        }
    }

    return {
        prevPage,
        nextPage,
    }
}

export default usePagination
