import React from 'react'
import Modal from 'react-bootstrap/Modal';
import "../styles/modal.css" 

const VentanaModal = ({ children, show, handleClose, titulo, size, backdrop}) => {
    return (
        <Modal className="reprocesoModal" show={show} onHide={handleClose} size={size} backdrop="static" keyboard={false}> { /*backdrop="static" keyboard={false} */}
            { /*size="sm,lg,xl" */}
            <Modal.Header closeButton>
                <Modal.Title>{titulo}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {children}
            </Modal.Body>
        </Modal>
    );
};
export default VentanaModal