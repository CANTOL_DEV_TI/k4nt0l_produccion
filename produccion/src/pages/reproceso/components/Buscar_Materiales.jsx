import { Url_Reproceso } from "../../../constants/global";
import { useEffect, useRef, useState } from "react";
import { BotonAdd_} from "../components/Botones"
import { useFetchRepro } from "../hooks/useFetchRepro";
import { Materiales_Tabla } from './Reproceso_tabla';
import { ComboBoxBuscar_repro, ComboBoxForm_repro } from "../components/Combo_Reproceso";
import { useDebounce } from "use-debounce";

import axios from "axios";

export function Buscar_Materiales({currentSubArea, idItem, setMatlAdded, matlAdded, mostrarModal, warehouses, setWareHouses}) {
    
    //ESTADO SUBAREA SELECCIONADO EN VENTANA BUSCAR MATERIAL
    const [subAreaSelected, setSubAreaSelected] = useState('')

    //MANEJO COMBO ALMACEN    
    const [cmbAlmacen, setCmbAlmacen] = useState('0')
    
    //SETEA CMBO ALMACEN CUANDO SOLO EXISTE UN ALMACEN
    useEffect(()=>{
        let condition = warehouses.length > 1 ? '0' : !!warehouses[0] ? warehouses[0].WareCode : '0';
        setCmbAlmacen(condition)
    },[warehouses])

    let copiedWareHouses = [...warehouses]
    if(copiedWareHouses.length > 1){copiedWareHouses.unshift({WareCode: '0', WareName: 'Almacén'})}

    // //
    const [itemMaterials, setItemMaterials] = useState([])
    
    //ESTADO CAMBIO DE AREAS EN CUADRO BUSCAR MATERIAL  
    const [subAreas_, setSubAreas] = useState([])

    useEffect(()=>{
        setMatlAdded([])
        setSubAreaSelected(idItem)
    },[])

    //FETCH SUBAREA MATERIALS
    const fetchDataMaterialBySubArea = async ({subArea_, prcCode}) => {
        if (subArea_ !== idItem && subArea_ !== 'ALMACEN 3'){
            let response = await axios({
                url: `${Url_Reproceso}/produccion/reproceso/articulo/materials`,
                method: 'post',
                data: {
                    members: prcCode,
                },
            });
            setItemMaterials(response.data.data)
            setSubAreaSelected(subArea_)
            setCurrentPage(10)
        }
        else if (subArea_ === idItem){
            let url = `${Url_Reproceso}/produccion/reproceso/articulo/material_repro?odParameter=${idItem}&subareaName=${currentSubArea.toUpperCase()}&isAll=${(true?'1':'0')}`
            const response = await axios(url, {
                headers: {
                    "ngrok-skip-browser-warning": "69420",
                }
            })
            setItemMaterials(response.data.data)
            setSubAreaSelected(subArea_)
            setCurrentPage(10)
        }
        else if (subArea_ === 'ALMACEN 3'){
            offsetPage.current = 0
            let url = `${Url_Reproceso}/produccion/reproceso/articulo/materialsbyWare?searchPattern=${inputBuscar}&offset=${offsetPage.current}`
            const response = await axios(url, {
                headers: {
                    "ngrok-skip-browser-warning": "69420",
                }
            })
            setItemMaterials(response.data.data)
            setSubAreaSelected(subArea_)
            //regresa a 0 en paginacion almacen 3
        }
    }

    // FETCH AL ARTICLE MATERIALS
    useFetchRepro(setItemMaterials, `${Url_Reproceso}/produccion/reproceso/articulo/material_repro?odParameter=${idItem}&subareaName=${currentSubArea.toUpperCase()}&isAll=${(true?'1':'0')}`);

    //tabla derivada
    useFetchRepro(setSubAreas, `${Url_Reproceso}/produccion/reproceso/area/`)

    //estado derivado de subareas, agrega codio de idItem como primera opcion y tambien almacen 3 como ultima
    let subAreas = [...subAreas_]
    subAreas.unshift({id: -1, subareaName: idItem})
    subAreas.length > 1 && subAreas.push({id: subAreas[subAreas.length-1].id + 1, subareaName: "ALMACEN 3"})

    // CAMBIAR ESTADO SELECCION DE LISTA MATERIALES
    const changeSelectedState = (code, newState) => {
        let tmpAllMaterials = [...itemMaterials]
        const index = tmpAllMaterials.findIndex(x=>(x.materialCode === code))
        tmpAllMaterials[index].isSelected = newState
        //EN ESTA PARTE AGREGA EL MATERIAL SELECCIONADO A LA LISTA MATERIAL ADDED
        let tmpMaterial = {...tmpAllMaterials[index]}
        if (Array.isArray(tmpMaterial.baseQty)){tmpMaterial.baseQty = tmpMaterial.baseQty[0]}
        if (Array.isArray(tmpMaterial.baseCost)){tmpMaterial.baseCost = tmpMaterial.baseCost[0]}
        delete tmpMaterial.isSelected
        //COPIA ACUMULADOR DE MATERIALES
        let matlAdded_tmp = [...matlAdded]
        //SETEO DE ALMACEN DE SUBAREA CON VALOR DE CMBSUBAREA
        tmpMaterial.whName = (cmbAlmacen !== '0' && subAreaSelected !== 'ALMACEN 3') ? cmbAlmacen : (subAreaSelected === 'ALMACEN 3') ? 'ALM03' : '';
        //PRIMERO BUSCA SI EL MATERIAL SE ENCUENTRA EN EL ACUMULADOR
        let inputMaterial = JSON.stringify({area: (subAreaSelected === idItem ? currentSubArea : subAreaSelected), data: tmpMaterial})
        const foundedItem = matlAdded_tmp.find((x)=>(x===inputMaterial))

        //SI EXISTE EL MATERIAL EN EL ACUMULADOR Y HACE CHECK, LO ELIMINA
        if (!!foundedItem && !newState){
            const index = matlAdded_tmp.findIndex((x)=>(x === inputMaterial))
            matlAdded_tmp.splice(index, 1)
            setMatlAdded(matlAdded_tmp)
        //SI NO EXISTE EL MATERIAL EN EL ACUMULADOR Y NO HACE CHECK, LO AGREGA
        }else if(!foundedItem && newState){
            matlAdded_tmp.push(inputMaterial)
            setMatlAdded(matlAdded_tmp)
        };
        setItemMaterials(tmpAllMaterials)
    }

    // PAGINACIÓN 
    let LimitPag = 10;
    const offsetPage = useRef(0)
    const [currentPage, setCurrentPage] = useState(10);
    const nextPage = (data) => {
        if (data.length > currentPage)
            setCurrentPage(currentPage + LimitPag);
    }
    const prevPage = () => {
        if ((currentPage - LimitPag) > 0)
            setCurrentPage(currentPage - LimitPag);
    }
    
    const nextPageOffset = async () => {
        offsetPage.current = offsetPage.current + LimitPag
        let url = `${Url_Reproceso}/produccion/reproceso/articulo/materialsbyWare?searchPattern=${inputBuscar || ''}&offset=${offsetPage.current}`
        const response = await axios(url, {
            headers: {
                "ngrok-skip-browser-warning": "69420",
            }
        })
        setItemMaterials(response.data.data);
    }

    const prevPageOffset = async () => {
        offsetPage.current = offsetPage.current - LimitPag
        let url = `${Url_Reproceso}/produccion/reproceso/articulo/materialsbyWare?searchPattern=${inputBuscar || ''}&offset=${offsetPage.current}`
        const response = await axios(url, {
            headers: {
                "ngrok-skip-browser-warning": "69420",
            }
        })
        setItemMaterials(response.data.data);
    }


    //ON COMBOAREAS OPTIONS ARE CHANGED
    function datosBusqueda(evento) {
        if (evento.target.name === 'cbo_area') {
            let target = evento.target.value
            let subarea_filtered = subAreas_.filter((x=>(x.subareaName === target)))[0]
            fetchDataMaterialBySubArea({subArea_: target, prcCode: subarea_filtered?.members })
        }
        else if (evento.target.name === 'cbo_warehouse') {
            let matlAdded_tmp = [...matlAdded].map((x)=>({area: JSON.parse(x).area, data: {...JSON.parse(x).data, 
                whName: JSON.parse(x).area !== 'ALMACEN 3' ? evento.target.value : "ALM03"}}))
            matlAdded_tmp = matlAdded_tmp.map((x)=>(JSON.stringify(x)))
            setMatlAdded(matlAdded_tmp)
            setCmbAlmacen(evento.target.value)
        }
    };

    //CAMBIAR ORDEN DE CANTIDADES BASE
    function onChangeQtyCell(evento, materialCode){
        if (evento.target.name === 'qtyComboTable') {
            let tmpItemMaterials = [...itemMaterials]
            const foundIdx = tmpItemMaterials.findIndex(x => x.materialCode === materialCode)
            let tmpQtyArray = tmpItemMaterials[foundIdx].baseQty
            let baseQtySelected = Number(evento.target.value)
            const foundIdxBaseQty = tmpQtyArray.findIndex(x => x === baseQtySelected)
            tmpQtyArray.splice(foundIdxBaseQty, 1)
            tmpQtyArray.unshift(baseQtySelected)
            setItemMaterials(tmpItemMaterials)
        }
    }

    // ON INPUTFIELD CHANGE
    const on_search_input_changed = (searchPattern) => {
        if (subAreaSelected !== 'ALMACEN 3'){
            setCurrentPage(10);
        }
        setInputBuscar(searchPattern);
    }
    
    // ON SEARCH INPUT CHANGED
    const [inputBuscar, setInputBuscar] = useState('');
    const [debounceInputBuscar] = useDebounce(inputBuscar, 200);

    useEffect(()=>{
        const fetchData = async () => {
            //aca falta captar offset, searchPattern
            try{
                // let url = `${Url_Reproceso}/produccion/articulo/materialsbyWare?offset=${offsetPage.current}&searchPattern=${inputBuscar}`
                let url = `${Url_Reproceso}/produccion/reproceso/articulo/materialsbyWare?offset=0&searchPattern=${inputBuscar}`
                const response = await axios(url, {
                    headers: {
                        "ngrok-skip-browser-warning": "69420",
                    }
                })
                if (response.status === 200){
                    offsetPage.current = 0; 
                    setItemMaterials(response.data.data);
                }
            } catch(error){
                console.error(error)
                }
            }
            if (subAreaSelected === 'ALMACEN 3'){
                fetchData()
            }
        }
    , [debounceInputBuscar])

    const search = (dataList) => {
        if(!!dataList.length && !inputBuscar.length && subAreaSelected !== 'ALMACEN 3'){
            let sliceData = dataList.slice(currentPage - LimitPag, currentPage)
            if(sliceData.length < 10){
                //COMPLETA CON FILAS CERO PARA NO LA ALTURA DEL MODAL
                let filledArray = [...Array(10 - sliceData.length)].map(()=>0)
                let filtered = sliceData.concat(filledArray)
                return {itemsPage: filtered, searchedItems: dataList};
            }
            return {itemsPage: dataList.slice(currentPage - LimitPag, currentPage), searchedItems: dataList};
        }
        else if(!!dataList.length && !!inputBuscar.length && subAreaSelected !== 'ALMACEN 3'){
            let filtered = dataList.filter((item) => {if ((item.materialCode.toString() + " " + item.materialName.toString()).toLowerCase().includes(inputBuscar.toLowerCase())) return true});
            if(filtered.length < 10){
                let filledArray = [...Array(10 - filtered.length)].map(()=>0)
                filtered = filtered.concat(filledArray)
            }
            return {itemsPage: filtered.slice(currentPage - LimitPag, currentPage), searchedItems: filtered};
        }
        else if (!!dataList.length && subAreaSelected === 'ALMACEN 3'){
            let filtered = [...dataList]
            if(dataList.length < 10){
                let filledArray = [...Array(10 - dataList.length)].map(()=>0)
                filtered = filtered.concat(filledArray)
            }
            return {itemsPage: filtered, searchedItems: filtered};
        }else {
            return {itemsPage: [], searchedItems: []};
        }      
    };
    
    // EJECUTA FILTER ALL DATA
    const {itemsPage, searchedItems} = search(itemMaterials)

    
    const onSearchMaterialClose = () => {
        //reducer false cuando estan mezclado los almacenes
        let reducer = matlAdded.reduce((acc, cur)=>(acc && JSON.parse(cur).area !== 'ALMACEN 3'), true)
        if(!!matlAdded.length && cmbAlmacen === '0' && !reducer){
            let matlAdded_tmp = matlAdded.filter((x, index)=>(JSON.parse(x).area === 'ALMACEN 3'))
            if(matlAdded_tmp.length === matlAdded.length){
                mostrarModal(true)
            }else{
                alert('No hay almacenes seleccionados')
            }
        }
        else if(!!matlAdded.length && cmbAlmacen !== '0' && !reducer){
            mostrarModal(true)
        }else if(!matlAdded.length && cmbAlmacen !== '0' && !!reducer){
            alert('No hay materiales seleccionados')
        }else if(!matlAdded.length && cmbAlmacen === '0' && !!reducer){
            alert('No hay materiales seleccionados')}
        else if(!!matlAdded.length && cmbAlmacen === '0' && !!reducer){
            alert('No hay almacenes seleccionados')
        }
        else if(!!matlAdded.length && cmbAlmacen !== '0' && !!reducer){
            mostrarModal(true)
        }
    }

    // CAMBIA COLOR DE AGREGAR MATERIAL CUANDO HAY MATERIAL SELECCIONADO
    const sw_outline = !!matlAdded.length && cmbAlmacen !== '0'


    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">
            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-2">
                        <div className="col-sm-3 p-1">
                            <ComboBoxForm_repro
                                datosRow={subAreas}
                                nombre_cbo="cbo_area"
                                manejaEvento={datosBusqueda}
                                valor={(subAreaSelected)}
                            />
                        </div>
                        <div className="col-sm-1 col-sm-3 p-1">
                            <input type="search" 
                            className="form-control" 
                            style={{ maxWidth: '380px', minWidth: '100px' }} 
                            placeholder="Buscar por código o detalle" 
                            value={inputBuscar} 
                            // onChange={(x)=>{setInputBuscar(x.target.value);
                            onChange={(x)=>{
                                on_search_input_changed(x.target.value)
                            }}
                            />
                        </div>
                    </div>
                </div>
                {/* FIN BARRA DE NAVEGACION */}

                {/* INICIO TABLA */}
                <Materiales_Tabla 
                    datos={itemsPage} 
                    changeSelectedState={(x,y) => {changeSelectedState(x,y)}} 
                    subAreaSelected={subAreaSelected} 
                    idItem={idItem}
                    onChangeQtyCell_={(x, y)=>onChangeQtyCell(x, y)}
                    />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-between flex-wrap m-2" style={{gap:10}}>
                    <div className="pagination-wrap hstack gap-1">
                        {/* lo de abajo es para paginacion desde el backend */}
                        {(subAreaSelected === 'ALMACEN 3') && (
                            <>
                            <button className={offsetPage.current < 10 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"}
                                onClick={()=>{prevPageOffset()}}>Anterior
                            </button>
                            <button className={"btn btn-primary btn-sm"} onClick={()=>{nextPageOffset()}}>Siguiente</button>
                            </>
                        )}
                        
                        {/* lo de abajo es para paginacion desde el frondend */}
                        {(subAreaSelected !== 'ALMACEN 3') &&(
                            <>
                                <button className={currentPage <= 10 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={(prevPage)}>Anterior</button>
                                <button className={searchedItems.length > currentPage ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={()=>{nextPage(searchedItems)
                                }}>Siguiente</button>
                                Total Reg. {(currentPage)}/{searchedItems.length}
                            </>
                        )}
                    </div>
                    <div className="d-flex justify-content-end border border-success rounded p-1" style={{maxWidth: '320px', gap: 5}}>
                        {/* <div className="col-sm-2 p-1"> */}
                        <div className="w-50">
                                {/* <ComboBoxForm_repro */}
                                <ComboBoxBuscar_repro
                                    datosRow={copiedWareHouses}
                                    nombre_cbo="cbo_warehouse"
                                    manejaEvento={datosBusqueda}
                                    valor_ini={"Almacén"}
                                    valor={cmbAlmacen}
                                />
                        </div>
                        <div>
                            <BotonAdd_ textoBoton={"Añadir Materiales"} sw_outline={sw_outline} eventoClick={onSearchMaterialClose}/>
                        </div>
                        {/* <div className="col-xxl-4 col-sm-2 p-1">
                            <BotonAdd_ textoBoton={"Añadir Materiales"} sw_outline={sw_outline} eventoClick={onSearchMaterialClose}/>
                        </div> */}
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>
        </div>
    );
}