import '../styles/numberInput.css'

export const InputTextBuscarV3 = ({placeholder, maxWidth, inputMaterial, setInputMaterial, onPatternChange, onBuscarChange, disabled = false}) => {
    return (
      <>
      {!!onPatternChange && (
        <input 
        type="search" 
        autoComplete='off'
        className="form-control w-100" 
        style={{ maxWidth: maxWidth || '210px', minWidth: '100px' }} 
        placeholder={placeholder}
        value = {inputMaterial}
        onChange={(x)=>{setInputMaterial(x.target.value)}} 
        name = "txt_buscar"
        disabled = {disabled} 
        />
      )}
      {!!onBuscarChange && (
        <input 
        type="search" 
        autoComplete='off'
        className="form-control w-100" 
        style={{ maxWidth: maxWidth || '210px', minWidth: '100px' }} 
        placeholder={placeholder}
        value = {inputMaterial}
        onChange={onBuscarChange} 
        name = "txt_buscar"
        disabled = {disabled}  
        />
      )}
      </>
    );
};

export const InputJustNumber = ({placeholder, maxWidth, rjctdQty, onRjctdQtyChanged}) => {
  return (
      <input 
      type="Number" 
      autoComplete='off'
      className="form-control w-100" 
      onInput={(x)=>{(x.target.value.length > 7) && (x.target.value = x.target.value.slice(0, 7))}} 
      style={{ maxWidth: maxWidth || '210px', minWidth: '100px' }} 
      placeholder={placeholder}
      value = {rjctdQty>0?rjctdQty:''}
      onChange={onRjctdQtyChanged} 
      name = "txt_buscar" 
      />
  );
}

export const InputTextforTable = ({qtyRejected, updateRejectedQty, disabled = false}) => {
    return (
        <input 
        type="Number" 
        className="form-control" 
        disabled = {disabled?"disabled":""}
        onInput={(x)=>{(x.target.value.length > 7) && (x.target.value = x.target.value.slice(0, 7))}} 
        style={{maxHeight: '32px', paddingLeft: '8px', fontSize: '11px', textAlign: 'center'}}
        autoComplete='off'
        value={qtyRejected}
        onChange={(x)=>{updateRejectedQty(x.target.value)}}
        />
    );
};