import React from 'react'

export const componente_temporal = ()  => {
  return (
    <div>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore, itaque.
    </div>
  )
}

export const Combo_Buscar_Material_Tabla = ({datosRow, nombre_cbo, onChangeQtyCell, valor})  => {
    return (
        <div >
            <select id={nombre_cbo} name={nombre_cbo} onChange={onChangeQtyCell} value={valor} style={{minWidth: "90px", marginTop: "0px", borderRadius: "4px"}}>
                {
                    !!datosRow.length && datosRow.map((x, index)=>{
                        return (<option key={index + 1} value={x.toString()}>{x}</option>)
                    })
                }
            </select>
        </div>
    )
  }

export const ComboBoxForm_repro = ({ datosRow, nombre_cbo, manejaEvento, valor, valor_ini, disabled=false, motivo_causa = false}) => {
    return (
        <>
        {!motivo_causa ? (
            <select className="form-select" id={nombre_cbo} name={nombre_cbo} value={valor} onChange={(x)=>{manejaEvento(x)}} disabled={disabled}>
                {!!valor_ini ? 
                (
                <>
                    {datosRow.map((data, index) => {
                    return (
                        // <option key={index + 1}  value={data.WareCode} disabled={!index ? true: false} selected={!index ? true: false}>
                        <option key={index + 1}  value={data.WareCode} disabled={!index ? true: false}>
                            {!index && datosRow.length>1 ? `${data.WareName}` : `${data.WareCode} - ${data.WareName}`}
                        </option>
                        );
                    })}
                </>
                ) 
                : 
                (<>
                    {datosRow.map((data, index) => {
                    return <option key={index + 1}  value={data.subareaName} >{data.subareaName}</option>;
                    })}
                </>)
                }
            </select>
        ):(
            <select className="form-select" id={nombre_cbo} name={nombre_cbo} value={valor} onChange={(x)=>{manejaEvento(x)}} disabled={disabled}>
                {datosRow.map((data, index) => {
                    return (
                        // <option key={index + 1}  value={data.WareCode} disabled={!index ? true: false} selected={!index ? true: false}>
                        <option key={index + 1}  value={data.reason} disabled={data.reason === 'MOTIVO CAUSA' ? true: false}>
                            {data.reason}
                        </option>
                        );
                    })
                }
            </select>
            )
        }
        </>
    );
};

export const ComboBox_Repro = ({ datosRow, nombre_cbo, manejaEvento, valor, valor_ini, disabled=false}) => {
    return (
        <>
            <select className="form-select" id={nombre_cbo} name={nombre_cbo} value={valor} onChange={(x)=>{manejaEvento(x)}} disabled={disabled}>
                <>
                    <option value={'-1'} disabled={true}>
                        {valor_ini}
                    </option>
                    {datosRow?.map((data, index) => {
                    return (
                        <option key={index + 1}  value={data.id}>
                            {data.subareaName}
                        </option>
                        );
                    })}
                </>
            </select>
        </>
    );
};

export const ComboBoxBuscar_repro = ({ datosRow, nombre_cbo, manejaEvento, valor, valor_ini, disabled=false, motivo_causa = false}) => {
    return (
        <>
        {!motivo_causa ? (
            <select className="form-select form-select-sm" id={nombre_cbo} name={nombre_cbo} value={valor} onChange={(x)=>{manejaEvento(x)}} disabled={disabled}>
                {!!valor_ini ? 
                (
                <>
                    {datosRow.map((data, index) => {
                    return (
                        // <option key={index + 1}  value={data.WareCode} disabled={!index ? true: false} selected={!index ? true: false}>
                        <option key={index + 1}  value={data.WareCode} disabled={!index ? true: false}>
                            {!index && datosRow.length>1 ? `${data.WareName}` : `${data.WareCode} - ${data.WareName}`}
                        </option>
                        );
                    })}
                </>
                ) 
                : 
                (<>
                    {datosRow.map((data, index) => {
                    return <option key={index + 1}  value={data.subareaName} >{data.subareaName}</option>;
                    })}
                </>)
                }
            </select>
        ):(
            <select className="form-select" id={nombre_cbo} name={nombre_cbo} value={valor} onChange={(x)=>{manejaEvento(x)}} disabled={disabled}>
                {datosRow.map((data, index) => {
                    return (
                        // <option key={index + 1}  value={data.WareCode} disabled={!index ? true: false} selected={!index ? true: false}>
                        <option key={index + 1}  value={data.reason} disabled={data.reason === 'MOTIVO CAUSA' ? true: false}>
                            {data.reason}
                        </option>
                        );
                    })
                }
            </select>
            )
        }
        </>
    );
};
  
