import React, { useRef, useState } from 'react'
import {GoFoldDown} from "react-icons/go";
import InputSpinner from 'react-bootstrap-input-spinner';
import { InputTextforTable } from './InputTextBuscar_Reproceso';
import { BsXLg } from 'react-icons/bs';
import { useNavigate } from 'react-router-dom';
import { Combo_Buscar_Material_Tabla } from './Combo_Reproceso';
import { BtnSAPDetail } from './Botones';
import { formatDateReverse, truncate2Decimal} from '../utils/functions';





function Reporte_Tabla({ datos_fila, objSubArea }) {
    const navigate = useNavigate()
    
    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle">Id</th>
                            <th className="align-middle">Cod. Artículo</th>
                            <th className="align-middle">Descripción Artículo</th>
                            <th className="align-middle" style={{width: '60px'}}>Simular</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                            {datos_fila.length > 0 ?
                            (   
                                datos_fila.map((datos, index) => {
                                    return(
                                        <tr key={index + 1}>
                                        <td className="td-cadena">{datos.id}</td>
                                        <td className="td-cadena">{datos.itemId}</td>
                                        <td className="td-cadena">{datos.itemName}</td>
                                        <td className='d-flex justify-content-center'>
                                            <button data-toggle="tooltip" 
                                            title="Simular Reproceso" 
                                            className="btn btn-outline-dark btn-sm ms-1"
                                            onClick={()=>navigate(`/reproceso/simular`, { state: {idItem: datos.itemId, objSubArea: objSubArea}})}>
                                                <GoFoldDown/>
                                            </button>
                                        </td>
                                        </tr>
                                        )
                                }
                            )
                        ) :
                        (
                            <tr>
                                <td className="text-center" colSpan="10">Sin datos</td>
                            </tr>
                        )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}

function  Reproceso_Tabla({materialList, deleteMaterial, updateRejectedQty, disableBaseQty, rjctdQty, onRjctdQtyChanged, onChangeQtyCell_}) {
    const [spinner, setSpinner] = useState(false);
    // CONDICION CUANDO MODIFICA CANTIDAD DE REPROCESO DESACTIVA TOOLTIP
    const initialValue = useRef({value: 0, state: false});
    if (!!materialList[0] && !initialValue.current.state){
        initialValue.current = {value: materialList[0].qtyRejected, state: true}
    }
    // RETORNAR INFORMACIÓN
    return (
        <div className="table-responsive">
            {/* INICIO TABLA */}
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                            <tr>
                            <th className="align-middle" style={{width: "170px"}}>Orden</th>
                            <th className="align-middle" style={{width: "170px"}}>Tipo</th>
                            <th className="align-middle" style={{width: "170px"}}>Código detalle</th>
                            <th className="align-middle">Detalle</th>
                            <th className="align-middle" style={{width: "100px"}}>Und</th>
                            <th className="align-middle" style={{width: "100px"}}>Cant. Base</th>
                            <th className="align-middle" style={{width: "100px"}}>Costo base</th>
                            <th className="align-middle" style={{width: "120px"}}>
                                <div className='position-relative' style={{height: '36px', width: '120px', padding: '0px'}} 
                                    onMouseOver={()=>{
                                    !spinner && setSpinner(true)}
                                    }
                                    onMouseLeave={()=>{
                                    !!spinner && setSpinner(false)}
                                    }
                                    onMouseOut={()=>{
                                    !!spinner && setSpinner(false)}
                                    }
                                    >
                                    {/* {(!spinner || (window.innerWidth < 901)) && (<div className='position-absolute top-0 start-0 w-100 h-100' style={{lineHeight:'37px'}}>Cant. Reproceso</div>)} */}
                                    {(!spinner || (window.innerWidth < 901)) && (<div className='' style={{lineHeight:'37px'}}>Cant. Requerida</div>)}
                                    {!!spinner && (window.innerWidth > 900) && (
                                    <div className='position-absolute top-0'>
                                        <InputSpinner
                                        type={'real'}
                                        precision={0}
                                        max={1000}
                                        min={0}
                                        step={1}
                                        value={rjctdQty}
                                        onChange={num=>{onRjctdQtyChanged(num)}}
                                        onMouseLeave={()=>{
                                            !!spinner && setSpinner(false)}
                                        }
                                        variant={'secondary'}
                                        size="sm"
                                        />
                                    </div>
                                    )}
                                </div>
                            </th>
                            <th className="align-middle" style={{width: "80px"}}>Almacén</th>
                            <th className="align-middle" style={{width: "80px"}}>Eliminar</th>
                        </tr>
                    </thead>    
                    <tbody className="list">
                                {materialList.length > 0 ?
                            (   
                                materialList.map((datos, index) => {
                                    return(
                                        <tr key={index + 1}>
                                        <td className="td-cadena">{datos.order}</td>
                                        <td className="td-cadena">{datos.type}</td>
                                        <td className="td-cadena">{datos.materialCode}</td>
                                        <td className="td-cadena">{datos.materialName}</td>
                                        <td className="td-cadena">{datos.und}</td>
                                        <td className="td-cadena">
                                            {disableBaseQty ? (<span>{datos.baseQty}</span>) : (<InputTextforTable qtyRejected={datos.baseQty} updateRejectedQty={(x)=>{updateBaseQty(x,index)}} disabled={false}/>)}
                                        </td>
                                        <td className="td-cadena">{datos.baseCost}</td>
                                        <td className="td-cadena" >
                                            <InputTextforTable className=".my-anchor-element" 
                                            qtyRejected={Array.isArray(datos.qtyRejected) ? datos.qtyRejected[0] : datos.qtyRejected} 
                                            updateRejectedQty={(x)=>{updateRejectedQty(x,index)}}
                                            />
                                        </td>
                                        <td className="td-cadena">
                                            {
                                            Array.isArray(datos.whName) && datos.whName.length>1 ?(
                                                    <Combo_Buscar_Material_Tabla
                                                    datosRow={datos.whName} 
                                                    nombre_cbo={'qtyRejected'} 
                                                    onChangeQtyCell={(x)=>{onChangeQtyCell_(x, index)}}
                                                    valor={datos.whName[0]}
                                                    />
                                                )
                                                :
                                                (Array.isArray(datos.whName) && datos.whName.length===1)?
                                                datos.whName[0]
                                                :
                                                datos.whName
                                            }
                                        </td>
                                        <td className='d-flex justify-content-center'>
                                            <button data-toggle="tooltip" 
                                            title="Simular Reproceso" 
                                            className={`btn btn-outline-danger btn-sm ms-1 ${(index === 0) && 'disabled'}`}
                                            onClick={()=>deleteMaterial(datos.materialCode)}>
                                                <BsXLg className=''/>
                                            </button>
                                        </td>
                                    </tr>
                                    )
                                }
                            )
                        ) :
                        (
                            <tr>
                                    <td className="text-center" colSpan="10">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            {/* FIN TABLA */}
        </div>
    );
}

function Materiales_Tabla({datos, changeSelectedState, subAreaSelected, idItem, onChangeQtyCell_}) {
    // RETORNAR INFORMACIÓN
    return (
        <div className="table-responsive">
            {/* INICIO TABLA */}
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" style={{width:'55px'}}>Orden</th>
                            <th className="align-middle">Tipo</th>
                            <th className="align-middle">Código detalle</th>
                            <th className="align-middle">Detalle</th>
                            <th className="align-middle">Und</th>
                            <th className="align-middle">Cantidad base</th>
                            <th className="align-middle">Costo base</th>
                            <th className="align-middle" style={{width: '35px'}}>✔</th>
                        </tr>
                    </thead>    
                    <tbody className="list">
                            {datos.length > 0 ?
                            (   
                                datos.map((dato, index) => {
                                    return(
                                    <tr key={index + 1}>
                                        {!!dato.materialCode && (
                                        <>
                                        <td className="td-cadena">{dato.order}</td>
                                        <td className="td-cadena">{dato.type}</td>
                                        <td className="td-cadena">{dato.materialCode}</td>
                                        <td className="td-cadena">{dato.materialName}</td>
                                        <td className="td-cadena">{dato.und}</td>
                                        <td className="td-cadena">
                                            {Array.isArray(dato.baseQty) && dato.baseQty.length>1 ?(
                                                <Combo_Buscar_Material_Tabla 
                                                    datosRow={dato.baseQty} nombre_cbo={'qtyComboTable'}
                                                    onChangeQtyCell={(x)=>{onChangeQtyCell_(x, dato.materialCode)}}
                                                    valor={dato.baseQty[0]}
                                                    />
                                                ):(subAreaSelected===idItem)?dato.baseQty:(dato.baseQty[0])}
                                        </td>
                                        <td className="td-cadena">{Array.isArray(dato.baseCost)?dato.baseCost[0]:dato.baseCost}</td>
                                        <td className="td-cadena">
                                            <input type="checkbox" checked={dato.isSelected} onChange={(x)=>{changeSelectedState(dato.materialCode, x.target.checked)}}/>
                                        </td>
                                        </>)    
                                        }
                                        {!dato.code && (
                                           <>
                                           &#160;
                                           </> 
                                        )}
                                    </tr>)
                                }
                            )
                        ) :
                            (
                            <tr>
                                <td className="text-center" colSpan="10">Sin datos</td>
                            </tr>
                            )
                        }
                    </tbody>
                </table>
            {/* FIN TABLA */}
        </div>
);
}

function Aprobaciones_Tabla({datos, onDetailClick}) {
    const change_date_order = (date_) => {
        let myDate = new Date(date_);
        let dateString = myDate.toLocaleDateString('es-PE', {day: '2-digit', month: '2-digit', year: 'numeric', timeZone: 'UTC'});
        return dateString;
    }

    let datos_ = datos.filter((item)=>!item.isOk) //borrar ! para que liste todos, cerrados como abiertos

    return (
        <div className="table-responsive">
            {/* INICIO TABLA */}
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" style={{width:'55px'}}>N°</th>
                            <th className="align-middle" style={{width:'145px'}}>Cod. Artículo</th>
                            <th className="align-middle" style={{width:'470px'}}>Descripción Artículo</th>
                            <th className="align-middle" style={{width:'140px'}}>Cant. Planificada</th>
                            <th className="align-middle">Motivo reproceso</th>
                            <th className="align-middle">Usuario</th>
                            <th className="align-middle" style={{width:'120px'}}>Fecha de simulación</th>
                            <th className="align-middle">Estado</th>
                            <th className="align-middle" style={{width:'80px'}}>Detalle</th>
                        </tr>
                    </thead>    
                    <tbody className="list">
                            {datos_.length > 0 ?
                            (   
                                datos_.map((dato, index) => {
                                    return(
                                        <tr key={index + 1}>
                                        <>
                                        <td className="td-cadena">{dato.SimId}</td>
                                        <td className="td-cadena">{dato.ItemCode}</td>
                                        <td className="td-cadena">{dato.ProdName}</td>
                                        <td className="td-cadena">{dato.PlannedQty}</td>
                                        <td className="td-cadena">{dato.Motivo_causa}</td>
                                        <td className="td-cadena">{dato.Usuario_Nombre}</td>
                                        <td className="td-cadena">{change_date_order(dato.SimDate)}</td>
                                        <td className="td-cadena" style={{fontSize: 14}}>
                                            <span className={`${dato.isOk ? 'badge bg-success' : 'badge bg-warning text-dark'}`}>{dato.isOk ? 'CERRADO' : 'ABIERTO'}</span>
                                        </td>
                                        <td className="td-cadena">
                                            <BtnSAPDetail iconType='BsFileText' onDetailClick={(rowData)=>{onDetailClick(dato.DocumentNumber, {isOk: dato.isOk, initialCost: dato.initialCost, additionalCost: dato.additionalCost, finalCost: dato.finalCost, percentage: dato.percentage, SimId: dato.SimId})}}/>
                                        </td>
                                        </>
                                    </tr>)
                                }
                            )
                        ) :
                            (
                            <tr>
                                <td className="text-center" colSpan="10">Sin datos</td>
                            </tr>
                            )
                        }
                    </tbody>
                </table>
            {/* FIN TABLA */}
        </div>
);
}

function Aprobaciones_Tabla_Aprobados({datos}) {
    // const change_date_order = (date_) => {
    //     let myDate = new Date(date_);
    //     let dateString = myDate.toLocaleDateString('es-PE', {day: '2-digit', month: '2-digit', year: 'numeric', timeZone: 'UTC'});
    //     return dateString;
    // }

    return (
        <div className="table-responsive">
            {/* INICIO TABLA */}
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" style={{width:'145px'}}>Fecha de simulación</th>
                            <th className="align-middle" style={{width:'175px'}}>Cod. Artículo</th>
                            <th className="align-middle" style={{width:'470px'}}>Descripción Artículo</th>
                            <th className="align-middle" style={{width:'140px'}}>Cant. Planificada</th>
                            <th className="align-middle" style={{width:'190px'}}>C. Inicial (S/.)</th>
                            <th className="align-middle" style={{width:'190px'}}>C. Adicional (S/.)</th>
                            <th className="align-middle" style={{width:'190px'}}>C. Total (S/.)</th>
                            <th className="align-middle">Doc. SAP</th>
                            <th className="align-middle" style={{width:'155px'}}>Usuario</th>
                        </tr>
                    </thead>    
                    <tbody className="list">
                            {datos.length > 0 ?
                            (   
                                datos.map((dato, index) => {
                                    return(
                                        <tr key={dato.id_cab}>
                                        <>
                                        <td className="td-cadena">{formatDateReverse(dato?.sim_date)}</td>
                                        <td className="td-cadena">{dato?.itemCode}</td>
                                        <td className="td-cadena">{dato?.itemName}</td>
                                        <td className="td-cadena">{dato?.plannedQty}</td>
                                        <td className="td-cadena">{truncate2Decimal(dato?.ini_cost)}</td>
                                        <td className="td-cadena">{truncate2Decimal(dato?.add_cost)}</td>
                                        <td className="td-cadena">{truncate2Decimal(dato?.total_cost)}</td>
                                        <td className="td-cadena">{dato?.docNumSAP}</td>
                                        <td className="td-cadena">{dato?.sim_user.toUpperCase()}</td>
                                        </>
                                    </tr>)
                                }
                            )
                        ) :
                            (
                            <tr>
                                <td className="text-center" colSpan="10">Sin datos</td>
                            </tr>
                            )
                        }
                    </tbody>
                </table>
            {/* FIN TABLA */}
        </div>
);
}


export {Reporte_Tabla, Reproceso_Tabla, Materiales_Tabla, Aprobaciones_Tabla, Aprobaciones_Tabla_Aprobados}