import React from "react";
import styled from "styled-components";
import { motion, AnimatePresence } from "framer-motion";

const Overlay = styled(motion.div)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: #ffffff;
  `;

const ModalContainer = styled(motion.div)`
  width: 100%;
  height: 100%;
  background-color: white;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  border-radius: 12px;
`;

const OverlayCards = styled(motion.div)`
  position: absolute;
  top: 20px;
  left: 0;
  width: 100%;
  height: 100%;
  background: #ffffff;
  `;

const ModalContainerCards = styled(motion.div)`
  width: 100%;
  height: 100%;
  background-color: white;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  border-radius: 12px;
`;

const modalVariant = {
  initial: { opacity: 0 },
  isOpen: { opacity: 1 },
  exit: { opacity: 0 }
};
const containerVariant = {
  initial: { top: "-50%", transition: { type: "spring" }},
  isOpen: { top: "50%"},
  exit: { top: "-50%"}
};

const Modal = ({children, isOpen }) => {
  return (
    <AnimatePresence>
      {isOpen && (
        <Overlay
          initial={"initial"}
          animate={"isOpen"}
          exit={"exit"}
          variants={modalVariant}
        >
          <ModalContainer variants={containerVariant} transition={{duration: 0.6, delay: 0}}>
            {children}
          </ModalContainer>
        </Overlay>
            )}
    </AnimatePresence>
        );
      };



const ModalCards = ({children, isOpen }) => {
  return (
    <AnimatePresence>
      {isOpen && (
        <OverlayCards
          initial={"initial"}
          animate={"isOpen"}
          exit={"exit"}
          variants={modalVariant}
        >
          <ModalContainerCards variants={containerVariant} transition={{duration: 0.6, delay: 0}}>
            {children}
          </ModalContainerCards>
        </OverlayCards>
            )}
    </AnimatePresence>
        );
      };
      
export {Modal, ModalCards};
