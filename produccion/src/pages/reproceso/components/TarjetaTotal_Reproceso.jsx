import '../styles/TarjetaTotal.css'
import { addZeroes } from '../utils/functions';

export const CustomTarjetaTotal_Reproceso = ({str_concepto , obj_imagen, str_monto, isColorValidation = false, color = ""}) => {
    return (
        // <div className="col-lg-3 col-md-6" >
        <div className="" style={{width: 'fit-content'}} >
            <div className="card" style={{maxHeight: '88px', minWidth: '400px', maxWidth: '100%'}}>
                <div className="card-body d-flex justify-content-between" style={{paddingRight:30}}>
                    <div className="d-flex align-items-center">
                        <div className="avatar-sm flex-shrink-0">
                            <span className="avatar-title bg-warning text-dark rounded-circle fs-3">
                                {obj_imagen}
                            </span>
                        </div>
                        <div className="flex-grow-1 ms-3">
                            <p className="text-uppercase fw-semibold fs-12 text-muted mb-1">{str_concepto}</p>
                            <h4 className=" mb-0"><span className="counter-value">{addZeroes(str_monto.toString())}</span></h4>
                        </div>
                    </div>
                    {isColorValidation && (
                        <div className='d-flex align-items-center'>
                             <div className="trafficLight">
                                <span className={`red ${color==='green' && 'bg-success'}`}></span>
                                <span className={`green ${color==='red' && 'bg-danger'}`}></span>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
};