import React, { useEffect, useRef, useState } from 'react'
import { Tabs } from 'react-bootstrap'
import { Tab } from 'react-bootstrap'
import "../styles/orden_fabricacion.css"
import { ComboBoxForm_repro } from './Combo_Reproceso'

function Orden_Fabricacion({idItem, itemName = "", rjctdItem, ofData, setOfData, handleClose, warehouses, user}) {
    //MANEJO COMBO ALMACEN    
    const [cmbAlmacen, setCmbAlmacen] = useState(ofData.WareHouse)
    //ACTUALIZA CMBALMACEN LUEGO DE TERMINAS DE CARGAR WARESHOUSES
    useEffect(()=>{
        warehouses.length===1 && setCmbAlmacen(warehouses[0].WareCode)
    },[warehouses])
    //SI EXISTE MAS DE UN ALMACEN, AGREGA UN ALMACEN CERO DEFAULT
    let copiedWareHouses = [...warehouses]
    if(copiedWareHouses.length > 1){copiedWareHouses.unshift({WareCode: '0', WareName: 'Almacén'})}

    // MANEJA CAMBIO DE ESTADO EN INPUT DATES
    const onDateChange = (x) => {
        if(x.target.value !== ""){
            if(x.target.name === 'startDate'){
                let updatedValue =  {...ofData, startDate: x.target.value}
                if (updatedValue.endDate < updatedValue.startDate){
                    updatedValue = {...updatedValue, endDate: updatedValue.startDate}
                }
                setOfData(updatedValue)
            }
            else if(x.target.name === 'endDate'){
                let updatedValue =  {...ofData, endDate: x.target.value}
                setOfData(updatedValue)
            }
        }
    }
    //ON COMBOAREAS OPTIONS ARE CHANGED
       function datosBusqueda(evento) {
        if (evento.target.name === 'cbo_warehouse') {
            let tmpOfData = {...ofData, WareHouse: evento.target.value}
            setOfData(tmpOfData)
            setCmbAlmacen(evento.target.value)
        }
    };

  return (
    <div className='d-flex flex-column'>
        <Tabs
            defaultActiveKey="secondary_data"
            id="uncontrolled-tab-example"
            className="mb-3"
        >
            <Tab eventKey="secondary_data" title="Fechas">
                <div className='d-flex flex-column' style={{gap:'5px'}}>
                        <div className="form-floating-md" autoComplete="off">
                            <label><b>Fecha de OF:</b></label>
                            <input type="date" name="docDate" className="form-control" id="floatingInput"
                                style={{width: '170px'}}
                                placeholder="Almacén" 
                                autoComplete="off" 
                                autoFocus 
                                value={ofData.docDate}
                                disabled={true}
                            />
                        </div>
                        <hr style={{margin: "10px 0px"}}/>
                        <label><b>Fechas de fabricación:</b></label>
                        <div className='d-flex' style={{gap:10}}>
                            <div className="form-floating-md" autoComplete="off">
                                <label>Inicio:</label>
                                <input type="date" name="startDate" className="form-control" id="floatingInput" 
                                    style={{width: '170px'}}
                                    placeholder="Fecha de inicio" 
                                    autoComplete="off" 
                                    autoFocus 
                                    value={ofData.startDate} 
                                    min={ofData.docDate}
                                    onChange={onDateChange}
                                />
                            </div>
                    
                            <div className="form-floating-md" autoComplete="off">
                                <label>Fin:</label>
                                <input type="date" name="endDate" className="form-control" id="floatingInput" 
                                    style={{width: '170px'}}
                                    placeholder="Fecha de fin" 
                                    autoComplete="off" 
                                    autoFocus 
                                    value={ofData.endDate}
                                    min={ofData.startDate}
                                    onChange={onDateChange}
                                />
                            </div>
                        </div>

                </div>
            </Tab>
            <Tab eventKey="primary_data" title="General">
                <div className='d-flex flex-column' style={{gap: "5px"}}>
                    <div className='d-flex' style={{gap: '7px'}}>
                        <div className="form-floating-md" autoComplete="off">
                            <label>Tipo:</label>
                            <input type="text" name="txtType" className="form-control" id="floatingInput" 
                                style={{width: '170px'}}
                                placeholder="Tipo" 
                                autoComplete="off" 
                                autoFocus 
                                value={"ESPECIAL"} 
                                disabled={true}
                                />
                        </div>
                        <div className="form-floating-md" autoComplete="off">
                            <label>Usuario:</label>
                            <input type="text" name="txtType" className="form-control" id="floatingInput" 
                                style={{width: '150px'}}
                                placeholder="Usuario" 
                                autoComplete="off" 
                                autoFocus 
                                value={user} 
                                disabled={true}
                                />
                        </div>
                    </div>
                    <div className='d-flex' style={{gap: '7px'}}>
                        <div className="form-floating-md" autoComplete="off">
                            <label>Artículo:</label>
                            <input type="text" name="txtArticle" className="form-control" id="floatingInput" 
                                style={{width: '170px'}}
                                placeholder="Artículo" 
                                autoComplete="off" 
                                autoFocus 
                                value={idItem} 
                                disabled={true}
                            />
                        </div>
                        <div className="form-floating-md" 
                            style={{width: '290px'}}
                            autoComplete="off">
                            <label>Descripción:</label>
                            <input type="text" name="txtDescription" className="form-control" id="floatingInput" 
                                placeholder="Descripción" 
                                autoComplete="off" 
                                autoFocus 
                                value={itemName} 
                                disabled={true}
                            />
                        </div>
                    </div>
                    <div className='d-flex' style={{gap: '7px'}}>
                        <div className="form-floating-md w-75" autoComplete="off">
                            <label>Almacén:</label>
                            <ComboBoxForm_repro
                                datosRow={copiedWareHouses}
                                nombre_cbo="cbo_warehouse"
                                manejaEvento={(x)=>{datosBusqueda(x)}}
                                valor_ini={"Almacén"}
                                valor={cmbAlmacen}
                                disabled={!(copiedWareHouses.length>1)}
                            />
                        </div>

                        <div className="form-floating-md" autoComplete="off">
                            <label>Cantidad:</label>
                            <input type="number" name="txtPlanQty" className="form-control" id="floatingInput" 
                                style={{width: '100px'}}
                                placeholder="Cantidad planificada" 
                                autoComplete="off" 
                                autoFocus 
                                value={rjctdItem} 
                                disabled={true}
                            />
                        </div>
                    </div>
                </div>
            </Tab>
        </Tabs>
        <div className='mt-3'>
            <button className={"btn btn-success"} onClick={()=>{cmbAlmacen==='0'?alert("No hay almacén seleccionado"):handleClose()}}>Guardar 💼</button>
        </div>
    </div>
  )
}

export {Orden_Fabricacion} 
