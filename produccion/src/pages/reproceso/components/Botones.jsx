import React from 'react'
import {
    BsPlusLg,
    BsXLg,
    BsFileText
} from "react-icons/bs";

const icons_buttons = {
    "BsXLg": <BsXLg className=''/>,
    "BsPlusLg": <BsPlusLg className=''/>,
    "BsFileText": <BsFileText className=''/>
}

const BotonAdd_ = ({ eventoClick, textoBoton = "", sw_outline = true, title = "" }) => {
    return (
        <>
            <button title={title} className={sw_outline ? 'btn btn-success btn-sm' : 'btn btn-outline-success btn-sm'} onClick={eventoClick}>
                <i className="align-bottom me-1"><BsPlusLg /></i>
                {textoBoton}
            </button>
        </>
    );
};

const BtnSAPDetail = ({title, iconType = "BsXLg", onDetailClick}) => {
    return (
            <button data-toggle="tooltip" 
                title={title} 
                className={`btn btn-outline-secondary btn-sm ms-1`}
                onClick={(rowData)=>{onDetailClick(rowData)}}>
                {icons_buttons[iconType]}
            </button>
    );
};

export {BotonAdd_, BtnSAPDetail}