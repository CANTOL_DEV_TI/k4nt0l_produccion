import { Navigate, Outlet, useLocation, useNavigate } from "react-router-dom";

const ProtectedRoute_Reproceso = ({Redirige }) =>
{
    const { state } = useLocation();
    const Login1 = sessionStorage.getItem("CDTToken")
    if(Login1==""||Login1==null)
    {
        return <Navigate to={Redirige} replace />
    }
    else if(state === null || state === undefined){
        return <Navigate to={"/reproceso"} replace />
    }
    return <Outlet />;
}

export {ProtectedRoute_Reproceso};