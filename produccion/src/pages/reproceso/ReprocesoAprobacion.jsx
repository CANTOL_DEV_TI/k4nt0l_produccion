import React, { useEffect, useRef, useState } from 'react'
import { ValidaRol_by_UserId, useValidaRol } from './hooks/useValidaRol'
import ReprocesoAprobacion_UI from './UI/ReprocesoAprobacion_UI';
import { useGetSimulations } from './hooks/useGetSimulations';
import { useVentanaModalReproceso } from './hooks/useVentanaModalReproceso';
import axios from 'axios';
import { Url_Reproceso } from '../../constants/global';
import usePagination from './hooks/usePagination';
import { Validar } from '../../services/ValidaSesion';
import { validaToken } from '../../services/usuarioServices';
// import { responsivePropType } from 'react-bootstrap/esm/createUtilityClasses';
import { get_User_Company } from './utils/functions';


function ReprocesoAprobacion() {
  // ventana seguridad
  const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
  const Seguridad = async () => {
      let meToken = sessionStorage.getItem("CDTToken");
      if (!!meToken){
        const ValSeguridad = await Validar("PROSIM");
        const nmd = await validaToken(meToken);
        const ValRol = await ValidaRol_by_UserId(nmd.sub.substring(3),4);
        setVentanaSeguridad(!ValRol || ValSeguridad.Validar);
      }else{
        setVentanaSeguridad(true);
      }
  }
  //
  const [adminName, setAdminName] = useState('XXX');
  const [isLoading, setIsLoading] = useState(false);
  const [userSa, setUserSa] = useState([]);
  const [dynamic_offset, setDinamic_Offset] = useState(0);
  //
  const [checkedPage, setCheckedPage] = useState(1);
  //
  const [simulationsList, setSimulationsList] = useState({data: [], total: 0});
  const [checkedSimulations, setCheckedSimulations] = useState([]);
  const simulationsListRef = useRef([])
  const simulationsTotal = useRef(0)
  //
  const rowdata_SAP = useRef({"DocumentNumber": "", "isOK": false, "initialCost": "", "additionalCost": ""})
  const subareas = useRef([]);

  //hooks
  const [isOpenDetailModal, openDetailModal, closeDetailModal] = useVentanaModalReproceso(false) //hook para control de modal
  const {prevPage, nextPage} = usePagination(setDinamic_Offset, dynamic_offset, 20) //hook para control de paginacion
  useGetSimulations(setSimulationsList, dynamic_offset, setUserSa, adminName); //obtiene simulaciones
  
  // EFFECTS
  //Validacion para rol de usuario: 4 para supervisor
  useEffect(()=>{
    useValidaRol(setAdminName,4);
    Seguridad();
    const obtenerSimAprobados = async () => {
      let {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
      try {
        const response = await axios.get(`${Url_Reproceso}/produccion/reproceso/simulacion/listaraprobados`, {
          params: {
            usuarioid: user
          }
        })
        if (response.status === 200){setCheckedSimulations([...response?.data])}
        else (setCheckedSimulations([]))
      } catch (error) {
        console.log('Error response:', error.response.data);
      }
    }
    obtenerSimAprobados()
  },[])
  
  
const catch_List = (simulationlist) => {
  if(simulationlist?.data?.length > 0){
    simulationsListRef.current = simulationlist.data
    simulationsTotal.current = simulationlist.total
  }
  if(userSa.length > 0){
    subareas.current = userSa
  }
}

const onDetailClick = (DocumentNumber, objResume) => {
  rowdata_SAP.current = {
    "DocumentNumber": DocumentNumber, 
    "isOK": objResume.isOk,
    "initialCost": objResume.initialCost,
    "additionalCost": objResume.additionalCost,
    "finalCost": objResume.finalCost,
    "percentage": objResume.percentage,
    "SimId": objResume.SimId,
  }
  openDetailModal()
}

const validateSim = async (SimId, checkDate) => {
  setIsLoading(true);
  let response = await axios({
    method: 'put',
    url: `${Url_Reproceso}/produccion/reproceso/simulacion/validar`,
    data: {
      Usuario_Codigo_VAL: adminName,
      id_cab: SimId,
      checkDate: checkDate
    }
  });
  setIsLoading(false);
  if(response.status === 201){
    try{
      let simulationList_temp = [...simulationsListRef.current];
      let index = simulationList_temp.findIndex((x)=>(x.SimId===SimId));
      //actualiza el dato de la lista
      simulationList_temp[index].DocumentNumber = Number(response.data.detail.DocumentNumber);
      simulationList_temp[index].isOk = true;
      //actualiza el dato unicamente de la fila activa
      rowdata_SAP.current = {...rowdata_SAP.current, "isOK": true, "DocumentNumber":Number(response.data.detail.DocumentNumber)}
      simulationsListRef.current = simulationList_temp;
      setSimulationsList({...simulationsList, data: simulationList_temp});
    }catch{
      console.log("An error ocurred")}
  }
}

//Estados derivados en cada render
catch_List(simulationsList)

  return (
    <ReprocesoAprobacion_UI
    userSa={subareas.current}
    VentanaSeguridad={VentanaSeguridad}
    simulationsList={simulationsListRef.current}
    checkedSimulations={checkedSimulations}
    simulationsTotal={simulationsTotal.current}
    isOpenDetailModal={isOpenDetailModal}
    closeDetailModal={closeDetailModal}
    onDetailClick={onDetailClick}
    setCheckedPage={setCheckedPage}
    rowdata_SAP={rowdata_SAP.current}
    validateSim={validateSim}
    isLoading={isLoading}
    dynamic_offset={dynamic_offset}
    prevPage={prevPage}
    nextPage={nextPage}
    checkedPage={checkedPage}
    />
  )
}

export { ReprocesoAprobacion }
