import React, { useEffect, useRef, useState } from 'react'
import { Url_Reproceso } from '../../constants/global'
import { InputTextBuscarV3 } from './components/InputTextBuscar_Reproceso'
import { Reporte_Tabla} from './components/Reproceso_tabla'
import { useFetchRepro } from './hooks/useFetchRepro'
import { capitalize } from './utils/functions'
import { ComboBox_Repro } from './components/Combo_Reproceso'
import axios from 'axios'
import VentanaBloqueo from '../../components/VentanaBloqueo'
import { Validar } from '../../services/ValidaSesion'

export const Reproceso = () => {
    // ventana seguridad
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
    const Seguridad = async () => {        
        const ValSeguridad = await Validar("PROSIM")
		setVentanaSeguridad(ValSeguridad.Validar)
    }
    //
    // const currentSubArea = sessionStorage.getItem("CDTToken");   
    const [buscarDato, setBuscarDato] = useState("");
    //PAGINATION
    const [currentPage, setCurrentPage] = useState(20);
    //CARD DATA
    const [articleList, setArticleList] = useState([])

    //current cmbsubarea
    const [currentSubArea, setCurrentSubArea] = useState('-1')

    //subareas estado
    const [subAreas, setSubAreas] = useState([]);

    // REF UI CURRENT SUBAREA
    const UISubAre = useRef(' ');

    // FETCH SUBAREAS
    useFetchRepro(setSubAreas, `${Url_Reproceso}/produccion/reproceso/area/`)

    useEffect(()=>{
        Seguridad()
    }, [])

    // FETCH ALL ARTICLES SUBAREA
    const fetchArticuloArea = async (evento) => {
        if (evento.target.name === 'cbo_subareas'){
            let cmbIndex = evento.target.value;
            try{
                let subarea_ = subAreas.filter((x)=>x.id.toString() === cmbIndex)[0];
                UISubAre.current = {prcCode: subarea_?.prcCode, subareaName: subarea_?.subareaName, members: subarea_?.members}
                let data_ = {members: UISubAre.current.members}
                let response = await axios({
                    url: `${Url_Reproceso}/produccion/reproceso/articulo`,
                    method: 'post',
                    data: data_,
                });
                if (response.status === 200){
                    setArticleList(response.data.data)
                }
                setCurrentSubArea(cmbIndex);
            }catch(error){
                console.log(`Error ocurred: ${error}`);
            }
        }
    }

    // PAGINATIONS
    let LimitPag = 20;
    const onSearchChange = (e) => {
        setCurrentPage(20);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    const prevPage = () => {
        if ((currentPage-LimitPag) > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    // curData: currentData
    const nextPage = (curData) => {
        if (curData.length > currentPage)
            setCurrentPage(currentPage + LimitPag);
    }

    // BUSQUEDA EN DOM
    // WHEN OF TEXTFIELD OR CMBAREA IS NOT EMPTY
    const search = (dataList) => {
        if (!!buscarDato.length){
            const filtered = dataList.filter((item) => `${item.itemId.toString().toLowerCase()} ${item.itemName.toString().toLowerCase()}`.includes(buscarDato));
            return {filtered: filtered.slice(currentPage - LimitPag, currentPage), dataAftSearch: filtered}}
        return {filtered: dataList.slice(currentPage - LimitPag, currentPage), dataAftSearch: dataList};
    };

    // OBTIENE UNICA TABLA DE REPORTS Y MATERIALES
    const {filtered, dataAftSearch} = search(articleList)

    return (
    <div>
        <div className="col-lg-12">
        <div className="card">
            {/* INICIO BARRA DE NAVEGACION */}
            <div className="card-header border-0 card-header">
                <div className="align-items-center gy-3 row">
                    <div className="col-sm">
                        <h4 className="card-title mb-0">{`Lista de artículos ${currentSubArea !== '-1'? "- " + (capitalize(UISubAre.current.subareaName.toLowerCase())) : ""}`}</h4>
                    </div>
                </div>
            </div>
                <div className="p-0 card-body">
                        <div className='d-flex flex-wrap'>
                    {/* INICIO CMB SUBAREAS */}
                            {/* <div className='p-2' style={{minWidth: '320px'}}> */}
                            <div className='p-2'>
                                    <ComboBox_Repro
                                        datosRow={subAreas}
                                        nombre_cbo="cbo_subareas"
                                        manejaEvento={(x)=>{fetchArticuloArea(x)}}
                                        valor_ini={"Subarea de producción"}
                                        valor={currentSubArea}
                                        />
                    {/* FIN CMB SUBAREAS */}
                            </div>
                    {/* INICIO BARRA DE BUSQUEDA */}
                            <div className="col-md-3 p-2">
                            {/* <div> */}
                                <InputTextBuscarV3 onBuscarChange={onSearchChange} placeholder={`Buscar por descripción o código de artículo`} 
                                maxWidth={'510px'} disabled={UISubAre.current!==' '?false:true}/>
                            </div>
                    {/* FIN BARRA DE BUSQUEDA */}
                        </div>

                    {/* FIN BARRA DE NAVEGACION */}
                {
                    currentSubArea !== '-1' && ( <>
                            { /* INICIO TABLA */}
                            <div className='w-100 p-0 position-relative overflow-hidden' style={{height:'fit-content'}}>
                                <Reporte_Tabla datos_fila={filtered} objSubArea={UISubAre.current}/>
                            </div>
                            {/* FIN TABLA */}

                            {/* INICIO PAGINACION */}
                            <div className="d-flex justify-content-end m-2">
                                <div className="pagination-wrap hstack gap-1">
                                    Total Reg. {(currentPage)}/{dataAftSearch.length}
                                    <button className={currentPage <= 20 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                                    <button className={dataAftSearch.length > currentPage? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={()=>{nextPage(dataAftSearch)}}>Siguiente</button>
                                </div>
                            </div>
                            {/* FIN PAGINACION */}
                            </>
                        )}
                        </div>
        </div>
        </div >
        {/* seguridad */}
        <VentanaBloqueo show={VentanaSeguridad} />
    </div>
    )
}

export default Reproceso




