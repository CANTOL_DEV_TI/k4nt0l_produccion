import React from 'react'
import { formatDate } from './utils/functions'

//Aqui viene la parte de los tabs
function ReprocesoAprobacion_Modal({rowdata_SAP, validateSim, isLoading}) {
  return (
    <>
      {!!isLoading && (
        <>
          <div className="loader"></div>
          <p style={{width: "100%", textAlign: "center", margin: "12px 0px"}}><b>Aprobando....</b></p>
        </>
      )}
      {!isLoading && (
        <div>
            <h6>Costo inicial: <b>{`S/.${rowdata_SAP?.initialCost}`}</b></h6>
            <h6>Costo adicional: <b>{`S/.${rowdata_SAP?.additionalCost}`} ({`${rowdata_SAP?.percentage}%`}CI)</b></h6>
            <h6>Costo final: <b>{`S/.${rowdata_SAP?.finalCost}`}</b></h6>
            <h6>Numero de documento SAP: <b>{rowdata_SAP.isOK ? rowdata_SAP.DocumentNumber: "(Pendiente)"}</b></h6>
            <button type="button" onClick={()=>{validateSim(rowdata_SAP.SimId, formatDate())}} style={{display: "block", margin: "auto", width: 140}} disabled={rowdata_SAP.isOK?true:false}>{rowdata_SAP.isOK?"APROBADO":"APROBAR"}</button>
        </div>
        )}
    </>
  )
}

export default ReprocesoAprobacion_Modal

