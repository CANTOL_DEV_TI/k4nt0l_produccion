import React from 'react'
import "../styles/loader.css"

function Loader({isloading}) {
  return (
    <div>
        {isloading === 'waiting' && (<div className="loader"></div>)}
        {isloading !== 'waiting' && (<div style={{whiteSpace:"break-spaces"}}>{isloading}</div>)}
    </div>
  )
}

export default Loader
