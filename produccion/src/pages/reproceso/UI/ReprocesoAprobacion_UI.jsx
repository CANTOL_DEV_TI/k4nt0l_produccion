import React, { useMemo } from 'react'
import { Aprobaciones_Tabla, Aprobaciones_Tabla_Aprobados } from '../components/Reproceso_tabla'
import VentanaBloqueo from '../../../components/VentanaBloqueo'
import VentanaModal from '../components/VentanaModal_Reproceso'
import ReprocesoAprobacion_Modal from '../ReprocesoAprobacion_Modal'
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

function ReprocesoAprobacion_UI(
    {
    userSa,
    VentanaSeguridad,
    simulationsList,
    checkedSimulations,
    simulationsTotal,
    isOpenDetailModal,
    closeDetailModal,
    onDetailClick,
    setCheckedPage,
    rowdata_SAP,
    validateSim,
    isLoading,
    dynamic_offset,
    prevPage,
    nextPage,
    checkedPage
    }
) {
    const cabecera = useMemo(()=>(
        <div className="card-header border-0 card-header">
            <div className="align-items-center gy-3 row">
                <div className="col-sm">
                    <h4 className="card-title mb-0">Lista de simulaciones - <span style={{fontSize:"1.15rem"}}>{`${userSa.map(x=>x.subarea).toString().replaceAll(',', ' - ')}`}</span></h4>
                </div>
            </div>
        </div>
    ),[userSa])

  return (
    <>
    <div className="col-lg-12">
    <Tabs
      defaultActiveKey="Aprobados"
      id="uncontrolled-tab-example"
      className="mb-3"
    >
      <Tab eventKey="Aprobados" title="Aprobados">
        <div className="card">
        {cabecera}
        { /* INICIO TABLA */}
        <div className='w-100 pt-3 position-relative overflow-hidden' style={{height:'fit-content'}}>
            <Aprobaciones_Tabla_Aprobados datos={checkedSimulations.slice((checkedPage-1)*20,checkedPage*20)}/>
        </div>
        {/* FIN TABLA */}
        {/* INICIO PAGINACION */}
            <div className="d-flex justify-content-end m-2">
                <div className="pagination-wrap hstack gap-1">
                    Total Reg. {checkedPage * 20 }/{checkedSimulations?.length}
                    <button className={ checkedPage == 1 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={()=>setCheckedPage(checkedPage - 1)}>Anterior</button>
                    <button className={ checkedPage * 20 < checkedSimulations?.length ? "btn btn-primary btn-sm": "btn btn-primary btn-sm disabled"} onClick={()=>setCheckedPage(checkedPage + 1)}>Siguiente</button>
                </div>
            </div>
        {/* FIN PAGINACION*/}
        </div>
      </Tab>
      <Tab eventKey="Pendientes" title="Pendientes">
        <div className="card">
                {cabecera}
                { /* INICIO TABLA */}
                <div className='w-100 pt-3 position-relative overflow-hidden' style={{height:'fit-content'}}>
                    <Aprobaciones_Tabla datos={simulationsList} onDetailClick={(DocumentNumber,isOk)=>{onDetailClick(DocumentNumber, isOk)}}/>
                </div>
                {/* FIN TABLA */}
                {/* INICIO PAGINACION */}
                    <div className="d-flex justify-content-end m-2">
                        <div className="pagination-wrap hstack gap-1">
                            Total Reg. {dynamic_offset + 20 }/{simulationsTotal}
                            <button className={dynamic_offset == 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                            <button className={(dynamic_offset + 20) < simulationsTotal? "btn btn-primary btn-sm": "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                            {/* <button className={"btn btn-primary btn-sm"} onClick={nextPage}>Siguiente</button> */}
                        </div>
                    </div>
                {/* FIN PAGINACION*/}
        </div>
      </Tab>
    </Tabs>
      

    </div >
    {/* INICIO MODAL BUSCA MATERIALES*/}
    <VentanaModal
        show={isOpenDetailModal}
        size="md"
        handleClose={()=>{closeDetailModal()}}
        titulo={`Resumen`}>
        <ReprocesoAprobacion_Modal 
        rowdata_SAP={rowdata_SAP}
        validateSim={(SimId, checkDate)=>{validateSim(SimId, checkDate)}}
        isLoading={isLoading}/>
    </VentanaModal>
    {/* FIN MODAL BUSCAR MATERIALES */}
    
    <VentanaBloqueo show={VentanaSeguridad} handleClose={()=>{}}/>
  </>
  )
}

export default ReprocesoAprobacion_UI
