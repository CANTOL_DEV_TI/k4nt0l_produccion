import React, { useEffect, useMemo, useRef, useState } from 'react'
import { Url_Reproceso } from '../../constants/global'
import { Reproceso_Tabla } from './components/Reproceso_tabla'
import { Modal } from './components/Modal'
import { useLocation, useNavigate } from 'react-router-dom'
import { useFetchRepro } from './hooks/useFetchRepro'
import { useFetchMaterialList } from './hooks/useFetchMaterialList'
import { useVentanaModalReproceso } from './hooks/useVentanaModalReproceso'
import { CustomTarjetaTotal_Reproceso} from './components/TarjetaTotal_Reproceso'
import { Buscar_Materiales } from './components/Buscar_Materiales'
import VentanaModal from './components/VentanaModal_Reproceso'
import { capitalize, formatDate, format_materialList, sleep } from './utils/functions'
import { BsAlignCenter, BsAlignEnd, BsAlignStart, BsXLg, BsFileEarmarkText } from 'react-icons/bs'
import {Orden_Fabricacion} from './components/Orden_Fabricacion'
import axios from 'axios'
import { ComboBoxForm_repro } from './components/Combo_Reproceso'
import { ValidarSesion_Repro } from './services_http/ValidaSesion_Repro'
import { ValidaRol_by_UserId } from './hooks/useValidaRol'
import Loader from './UI/Loader'



const ReprocesoSimulacion = ({route}) => {
    const { state } = useLocation();
    const navigate = useNavigate();
    const idItem = state?.idItem;
    const currentSubArea = state?.objSubArea?.subareaName;
    const [materialList, setMaterialList] = useState([]);
    const [motivocausa, setMotivoCausa] = useState([]);
    const [warehouses, setWareHouses] = useState([]);
    const [isloading, setIsLoading] = useState("");
    const isAddeded = useRef(false);
    const currentUser = useRef("");
    const isSim50 = useRef(false);
    
    // FETCH ALMACEN DE SUBAREA ACTUAL
    useFetchRepro(setWareHouses, `${Url_Reproceso}/produccion/reproceso/almacen/subarea?odSubArea=${currentSubArea}`)

    // FETCH ARTICLE MATERIALS
    // useFetch(setMaterialList, `${Url_Reproceso}/produccion/reproceso/articulo/material_repro?odParameter=${idItem}&subareaName=${currentSubArea.toUpperCase()}&isAll=${false?'1':'0'}`)
    useFetchMaterialList(setMaterialList, idItem, currentSubArea)
    
    // FETCH MOTIVO CAUSA
    useFetchRepro(setMotivoCausa, `${Url_Reproceso}/produccion/reproceso/articulo/motivo_causa?saPattern=${currentSubArea}`, [{cod_reason: -1, reason: 'MOTIVO CAUSA'}])

    //USE EFFECT PARA OBTENER EL USUARIO DENTRO DE VARIABLE DE REFERENCIA
    useEffect(()=>{
        const getuser = async () => {
            let userCode = await ValidarSesion_Repro()
            currentUser.current = userCode.Usuario_Codigo
        }
        getuser()
    }, [])
    
    //ON COMBOAREAS OPTIONS ARE CHANGED
    function datosBusqueda(evento) {
        if (evento.target.name === 'cbo_causa_motivo') {
            let target = evento.target.value
            let index = motivocausa.findIndex((x)=>(x.reason === target))
            let copiedMotivoCausa = [...motivocausa]
            copiedMotivoCausa.unshift(copiedMotivoCausa.splice(index, 1)[0]);
            let indexMotivo = copiedMotivoCausa.findIndex((x)=>(x.reason === 'MOTIVO CAUSA'))
            indexMotivo>0 && copiedMotivoCausa.splice(indexMotivo, 1)
            setMotivoCausa(copiedMotivoCausa)
        }
    };

    //MATERIALS ADDED
    //matl: material
    const [matlAdded, setMatlAdded] = useState([])

    //PAGINATION
    const [currentPage, setCurrentPage] = useState(20);

    //SHOW ADD MATERIALS MODAL
    const [show, handleShow, handleClose] = useVentanaModalReproceso(false);
    //EFFECT QUE SE ACTIVA CUANDO HAY UN CAMBIO DE ESTADO DE SHOW
    useEffect(()=>{
        let selectedMaterials = [...matlAdded].map((x)=>(JSON.parse(x)))
        if(!show && !!(selectedMaterials.length) && !!isAddeded.current){
            //buscar si existe material en tabla reproceso para no agregar nuevamente
                for (let i of materialList){
                    let index = selectedMaterials.findIndex((x)=>(x.data.materialCode === i.materialCode && x.data.baseQty === i.baseQty))
                    index>=0 && selectedMaterials.splice(index, 1)
                }
                let tmpMaterials = selectedMaterials.map((x)=>{
                    return  {
                    order: (x.area === currentSubArea ? x.data.order: x.area),
                    type: x.data.type,
                    materialCode: x.data.materialCode,
                    materialName: x.data.materialName,
                    und: x.data.und,
                    baseQty:
                    x.data.baseQty,
                    baseCost: x.data.baseCost,
                    qtyRejected: 0,
                    costbyOne: 0.0,
                    whName: x.data.whName,
                }
                })
                setMaterialList(materialList.concat(tmpMaterials))
        }
    },[show])
    //

    //SHOW OF DATA INPUTS
    const [showOfData, handleShowOfData, handleCloseOfData] = useVentanaModalReproceso(false);

    //ESTADO PARA QTYGENERAL DE RECHAZADOS
    const [rjctdQty, setRjctdQty] = useState(0)

    //REFERENCIA PARA MANEJAR EL SUBAREA SELECTED
    const curFatherId = useRef(idItem)
    
    //ESTADO DATOS DE OF DOC
    const [ofData, setOfData] = useState({qtyRejected: 0,user: 1, WareHouse: '0',docDate: formatDate(), startDate: formatDate(), endDate: formatDate()})

    const mostrarModal = (isOpen = false, isAdded = false) => {
        isAddeded.current = isAdded
        isOpen && handleShow()
        !isOpen && handleClose()
    };

    // PAGINATIONS
    let LimitPag = 20;
    const prevPage = () => {
        if ((currentPage-LimitPag) > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const nextPage = (curData) => {
        if (curData.length > currentPage)
            setCurrentPage(currentPage + LimitPag);
    }

    // FILTER DATA TABLE
    const search = (dataList) => {
        // if(typeof(dataList) === 'object'){return {filtered: [dataList], dataAftSearch: [dataList]}}
        return (!!dataList.length) ?  {filtered: dataList.slice(currentPage - LimitPag, currentPage), dataAftSearch: dataList, itemName: dataList[0].materialName, rjctdItem: dataList[0].qtyRejected} : {filtered: [], dataAftSearch: [], itemName: undefined, rjctdItem: 0};
    };

    // OBTIENE UNICA TABLA DE DE MATERIALES EN TRABLA Y TOTAL DE MATERIALES
    const {filtered, dataAftSearch, itemName, rjctdItem} = search(materialList)

    // MANEJO DE TABLA PRINCIPA DE MATERIALES
    function deleteMaterial(materialCode){
        let tmpMaterialList = [...materialList]
        const index = tmpMaterialList.findIndex((material)=>(material.materialCode === materialCode))
        tmpMaterialList.splice(index, 1)
        setMaterialList(tmpMaterialList)
    }

    function updateRejectedQty(newValue, index){
        let tmpMaterialList = [...materialList]
        //verifica si primera fila es array para grabar cambio en primera posicion
        if (Array.isArray(tmpMaterialList[index].qtyRejected)){
            tmpMaterialList[index].qtyRejected[0] = newValue
        }else{
            tmpMaterialList[index].qtyRejected = newValue
        }
        setMaterialList(tmpMaterialList)
    }

    // CAMBIAR CANTIDAD DE RECHAZADOS DESDE INPUT GENERAL
    const changeAllRjctQty = (x) => {
        // let qtyInputValue = x.target.value
        let qtyInputValue = x
        let tmpMaterialList = materialList.map(x=>({...x, qtyRejected: qtyInputValue}))
        // setRjctdQty(x.target.value)
        setRjctdQty(x)
        setMaterialList(tmpMaterialList)
    }

    const cabecera = useMemo(()=>(
        <div className="card-header border-0 card-header">
            <div className="align-items-center gy-3 row">
                <div className="col-sm">
                    <h4 className="card-title mb-0">{`Simular costos de reproceso: ${idItem} - ${currentSubArea}`}</h4>
                </div>
            </div>
        </div>
    ))

    //CAMBIA POSICION DE COMBO RECHAZADOS DE CELDA
    const onChangeQtyCell_ = (x, y) => {
        if (!y){
            let tmpMaterialList = [...materialList]
            let index = tmpMaterialList[y].whName.findIndex((item)=>(item === x.target.value))
            tmpMaterialList[y].whName.unshift(tmpMaterialList[y].whName.splice(index, 1)[0])
            tmpMaterialList[y].qtyRejected.unshift(tmpMaterialList[y].qtyRejected.splice(index, 1)[0])
            setMaterialList(tmpMaterialList)
        }
    }
    // ACTUALIZAR VALOR DE TARJETAS
    function valoresTarjetas(){
        let CI = 0.0;
        let CA = 0.0;
        let CT = 0.0;
        let isOk = false;
        if (!!materialList.length && ('baseCost' in materialList[0]) && !!materialList[0].baseCost){
            CI =  Array.isArray(materialList[0].qtyRejected) ? (materialList[0].baseCost * materialList[0].qtyRejected[0]) : (materialList[0].baseCost * materialList[0].qtyRejected);
            let Total = materialList.reduce((accumulator, currentValue) => accumulator + (currentValue.baseCost * (Array.isArray(currentValue.qtyRejected)?currentValue.qtyRejected[0]:currentValue.qtyRejected)), 0)
            CA = Total - CI
            CT =  Total
            isOk = true;
        }
        return {CI: Math.trunc(CI*1000)/1000, CA: Math.trunc(CA*1000)/1000, CT: Math.trunc(CT*1000)/1000, isOk: isOk}
    }
    const {CI, CA, CT, isOk} = valoresTarjetas()
    isSim50.current = (CT > (CI*1.5))

    // GUARDAR SIMULACION
    const guardar_simulacion = async () => {
        let reducer_qtyRejected = materialList.reduce((acc, cur)=> (Number(cur.qtyRejected) === undefined || Number(cur.qtyRejected) === 0) || acc, false)
        let reducer_baseCost = materialList.reduce((acc, cur)=> (Number(cur.baseCost) === undefined || Number(cur.baseCost) === 0) || acc, false)
        if (reducer_qtyRejected || reducer_baseCost){
            alert("Ingrese cantidades requeridas o costo base mayor a cero");
        }else if(materialList.length === 1){
            alert("Debe ingresar materiales");
        }else{
            try{
                let userCode = await ValidarSesion_Repro();
                let isRolValidated = await ValidaRol_by_UserId(userCode.Usuario_Codigo, 4)
                let response = { status: 500 };
                let curWareHouse = "0";

                if(warehouses.length === 1){
                    curWareHouse = warehouses[0].WareCode;
                }else if(warehouses.length > 0){
                    curWareHouse = ofData.WareHouse;
                }

                if (!!userCode && curWareHouse !== '0' && isOk){
                    let data_resume = {
                        ItemCode: idItem,
                        ProdName: itemName,
                        Type: 'P',
                        PlannedQty: materialList[0].qtyRejected[0],
                        SimDate: ofData.docDate,
                        PostDate: ofData.docDate,
                        DueDate: ofData.endDate,
                        StartDate: ofData.startDate,
                        Warehouse: curWareHouse,
                        OriginType: 'M',
                        PrcCode: state?.objSubArea?.prcCode,
                        Usuario_Codigo_SIM: userCode.Usuario_Codigo,
                        cod_motivo_causa: motivocausa[0].cod_reason,
                        isChecked: !isSim50.current || isRolValidated,
                        ProductionOrderLines: format_materialList(materialList, ofData.startDate, ofData.endDate, state?.objSubArea?.prcCode),
                    };
                        try {
                            setIsLoading("waiting")
                            response = await axios({
                                method: 'post',
                                url: `${Url_Reproceso}/produccion/reproceso/simulacion`,
                                data: data_resume
                            });
                            await sleep(100).then(()=>{console.log('World');});
                        } catch (error) {
                            setIsLoading(`¡Error durante operacion!\Contactar personal de TI`)
                        }
                        if(response.status === 200){
                            // if(!!isSim50.current){setIsLoading("¡Simulación enviada a bandeja de aprobaciones!")};
                            setIsLoading("¡Simulación enviada a bandeja de aprobaciones!");
                        }else if(response.status === 201){
                            let objResponse = response.data.detail
                            setIsLoading(`¡Simulación grabada!\nN°. Documento SAP: ${objResponse.DocumentNumber}`)
                        }else{
                            setIsLoading(`¡Error durante operacion!\nContactar personal de TI`)
                        }
                }else{
                    alert("¡Error!\nPosibles causas:\n>Falta agregar datos de cabecera OF");    
                }
            }catch(error){
                console.log(`An Error Ocurred ${error}`)
            }
        }
    }

    return (
    <div>
        <div className="col-lg-12">
            {/* INICIO TARJETAS */}
            <div className={`d-flex flex-wrap g-10 justify-content-start ${true && 'pb-2 mt-0 sticky-top w-100'}`} style={true ? {background: '#f1f1ef', zIndex: 1, gap: 10} : {}}>
                <CustomTarjetaTotal_Reproceso
                    str_concepto={"COSTO INICIAL (s/.):"}
                    obj_imagen={<BsAlignEnd />}
                    str_monto={CI}
                    isColorValidation={false}
                    />
                <CustomTarjetaTotal_Reproceso
                    str_concepto={"COSTO ADICIONAL (s/.):"}
                    obj_imagen={<BsAlignCenter />}
                    str_monto={CA}
                    isColorValidation={false}
                    />
                <CustomTarjetaTotal_Reproceso
                    str_concepto={"COSTO FINAL (s/.):"}
                    obj_imagen={<BsAlignStart />}
                    str_monto={CT}
                    isColorValidation={true}
                    color={isSim50.current ? 'red': 'green'}
                />
            </div>
            {/* FIN TARJETAS */}

        <div className="card" style={{minWidth: "400px"}}>
            {/* INICIO BARRA DE NAVEGACION */}
            {cabecera}
            <div className="p-0 card-body">
                {/* INICIO BARRA DE BUSQUEDA */}
                <div className="mx-1 my-2 row d-flex justify-content-start">
                    <div className="col-sm-1 p-1" style={{width: "fit-content"}}>
                        <button className={"btn btn-secondary"} onClick={()=>mostrarModal(true, false)}>Agregar Material</button>
                    </div>
                    <div className="col-sm-3 p-1">
                        <ComboBoxForm_repro
                            datosRow={motivocausa}
                            nombre_cbo="cbo_causa_motivo"
                            manejaEvento={(x)=>{datosBusqueda(x)}}
                            valor={motivocausa[0]?.reason}
                            motivo_causa={true}
                        />
                    </div>
                </div>
                {/* FIN BARRA DE BUSQUEDA */}

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <div className='w-100 p-0 position-static' style={{height:'fit-content'}}>
                    <div className=' w-100 mh-100 position-relative overflow-hidden' style={{height: "100vh"}}>
                        <Modal isOpen={true}>
                            <Reproceso_Tabla 
                                materialList={filtered} 
                                deleteMaterial={(x)=>{deleteMaterial(x)}} 
                                updateRejectedQty={(x,y)=>{updateRejectedQty(x,y)}}
                                valoresTarjetas={valoresTarjetas} 
                                disableBaseQty={true} 
                                rjctdQty={rjctdQty} 
                                onRjctdQtyChanged={changeAllRjctQty} 
                                onChangeQtyCell_={onChangeQtyCell_}
                            />
                            {/* INICIO PAGINACION */}
                            <div className="d-flex justify-content-end m-2">
                                <div className="pagination-wrap hstack gap-1">
                                    Total Reg. {(currentPage)}/{dataAftSearch.length}
                                    <button className={currentPage <= 20 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                                    <button className={dataAftSearch.length > currentPage? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={()=>{nextPage(dataAftSearch)}}>Siguiente</button>
                                    <button className={"btn btn-secondary btn-sm"} onClick={()=>navigate('/reproceso')}>Retornar</button>
                                </div>
                            </div>
                            {/* FIN PAGINACION*/}
                            <div className="w-100 d-flex justify-content-end" style={{paddingRight: '8px'}}>
                                <div className='d-flex border border-success rounded' style={{gap: '5px', padding: '5px'}}>
                                    <div>
                                        <a title="Datos OF" 
                                            className={`btn btn-warning btn-sm-1 text-decoration-none`}
                                            onClick={()=>handleShowOfData()}>
                                            <BsFileEarmarkText/>
                                        </a>
                                    </div>
                                    <div>
                                        <button className={"btn btn-success"} onClick={()=>(guardar_simulacion())}>Grabar ▶</button>
                                    </div>
                                </div>
                            </div>
                        </Modal>
                    </div>
                </div>

                {/* FIN TABLA */}
                {/* INICIO MODAL BUSCA MATERIALES*/}
                <VentanaModal
                    show={show}
                    size="xl"
                    handleClose={()=>{mostrarModal(false, false)}}
                    // titulo={`Lista de materiales - ${(curFatherId.current === idItem)?curFatherId.current.toUpperCase():capitalize(curFatherId.current.toLowerCase())}`}
                    titulo={`Lista de materiales`}
                >
                    <Buscar_Materiales idItem={idItem}
                    currentSubArea={currentSubArea}
                    setMatlAdded={setMatlAdded}
                    matlAdded={matlAdded}
                    mostrarModal={(y)=>{mostrarModal(false, y)}}
                    warehouses={warehouses}
                    setWareHouses={setWareHouses}
                    />
                </VentanaModal>
                {/* FIN MODAL BUSCAR MATERIALES */}

                {/* INICIO MODAL DATOS OF*/}
                <VentanaModal
                    show={showOfData}
                    size="md"
                    handleClose={handleCloseOfData}
                    titulo={`Orden de Fabricación`}
                >
                <Orden_Fabricacion 
                    idItem={idItem}
                    itemName={itemName || ""}
                    rjctdItem={rjctdItem || 0}
                    ofData={ofData}
                    setOfData={setOfData}
                    handleClose={handleCloseOfData}
                    warehouses={warehouses}
                    user={currentUser.current}
                />
                </VentanaModal>
                {/* FIN MODAL DATOS OF */}

                {/* INICIO MODAL DATOS UPLOAD SAP*/}
                <VentanaModal
                    show={!!isloading}
                    size="sm"
                    titulo={"******"}
                    // handleClose={()=>{setIsLoading(""); navigate('/reproceso');}}
                    handleClose={()=>{setIsLoading("");}}
                >
                    <Loader isloading = {isloading}/>
                </VentanaModal>
                {/* FIN MODAL DATOS UPLOAD IF */}
                    </div>
                </div>
            </div >
    </div>
    )
}

export {ReprocesoSimulacion}