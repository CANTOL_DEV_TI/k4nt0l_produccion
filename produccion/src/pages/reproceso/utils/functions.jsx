import { jwtDecode } from "jwt-decode";

// FUNCION QUE CAPITALIZA
export const capitalize = (s) => {
    return s[0].toUpperCase() + s.slice(1);
} 

export const format_materialList = (materialList, startDate, endDate, prcCode) => {
  let ProductionOrderLines = materialList.filter((x)=>(x.order !== 'ALMACEN 3'));
  ProductionOrderLines = ProductionOrderLines.map((x)=>(
    {
    ItemNo: x.materialCode,
    PlannedQuantity: Array.isArray(x.qtyRejected)?x.qtyRejected[0]:x.qtyRejected,
    ProductionOrderIssueType: x.type==='ARTICULO'?'im_Manual':x.type==='RECURSO'?'im_Backflush':'im_Manual',
    ItemType: x.type==='ARTICULO'?'pit_Item':x.type==='RECURSO'?'pit_Resource':'pit_Item',
    Warehouse: Array.isArray(x.whName)?x.whName[0]:x.whName,
    DistributionRule2: prcCode,
    StartDate: startDate,
    EndDate: endDate,
    ItemName: x.materialName,
    baseCost: x.baseCost,
    }
  ));
  return ProductionOrderLines;
}

export function addZeroes(num) {
  const dec = num.split('.')[1];
  const len = dec && dec.length > 3 ? dec.length : 3;
  return Number(num).toFixed(len);
}

export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function formatDate() {
  var d = new Date(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;
  return [year, month, day].join('-');
}


/**
 * Convierte el orden de una fecha date, primero va el dia ...
 * @param {string} dateString - date en formato 'YYYY-MM-DD'
 * @returns {string} - date en formato 'DD-MM-YYYY'
 */
export function formatDateReverse(dateString) {
      // Convertir el string a un objeto Date
      const date = new Date(dateString + 'T00:00:00');

      // Obtener el día, mes y año
      const day = (date.getDate()).toString().padStart(2, '0'); // Asegura que el día tenga 2 dígitos
      const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Asegura que el mes tenga 2 dígitos
      const year = date.getFullYear().toString().slice(-2); // Año, solo los dos últimos dígitos
  
      // Formatear la fecha como DD-M-YY
      return `${day}-${month}-${year}`;
}

/**
 * Trunca un numero a dos decimales
 * @param {number} num - numero a truncar
 * @returns {string} - numero truncado
 */
export function truncate2Decimal(num){
    let wt = Math.trunc(num*100)/100
    wt = wt.toLocaleString()
    let [integer, decimal] = wt.split('.');
    if (!decimal) {
        decimal = '00';
    } else if (decimal.length === 1) {
        decimal = decimal + '0';
    }
    return `${integer}.${decimal}`
}



/**
 * @param {string} token - token de autenticacion
 * @returns {object} - objeto con la informacion del usuario y empresa
 */
export function get_User_Company(token){
  const { sub, exp } = jwtDecode(token);
  return {user: sub.substring(3) , company: sub.substring(0,3)}
}