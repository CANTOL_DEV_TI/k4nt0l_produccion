import React, { useState, useEffect } from 'react'

export function Plan_venta_crud_tabla({ datos_fila, deleteData, eventoClick }) {

    // RETORNAR INFORMACIÓN
    return (
        < div className="table-responsive">
            {/* INICIO TABLA */}
            < table className="table table-hover table-sm table-bordered" >
                <thead className="table-secondary text-center table-sm">
                    <tr>
                        <td className="align-middle">Año</td>
                        <td className="align-middle">Mes</td>
                        <td className="align-middle">Cod.Artículo</td>
                        <td className="align-middle">Artículo</td>
                        <td className="align-middle">Cantidad</td>
                    </tr>
                </thead>
                <tbody className="list">
                    {datos_fila.length > 0 ?
                        (
                            datos_fila.map((datos, index) => {
                                return <tr key={index + 1}>
                                    <td className="td-cadena">{datos.ejercicio}</td>
                                    <td className="td-cadena">{datos.mes}</td>
                                    <td className="td-cadena">{datos.cod_articulo}</td>
                                    <td className="td-cadena">{datos.articulo}</td>
                                    <td className="td-cadena">{datos.cantidad}</td>
                                </tr>
                            })
                        ) :
                        (
                            <tr>
                                <td className="text-center" colSpan="5">Sin datos</td>
                            </tr>
                        )
                    }
                </tbody>
            </table >
            {/* FIN TABLA */}
        </div >
    );
}
