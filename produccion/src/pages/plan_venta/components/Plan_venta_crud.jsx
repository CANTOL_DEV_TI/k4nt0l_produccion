import React, { useState } from 'react'
import { BsFileExcel } from "react-icons/bs";
import { BotonGrabar, BotonExcel } from "../../../components/Botones"
import { Plan_venta_crud_tabla } from "./Plan_venta_crud_tabla"
import * as XLSX from 'xlsx';

export function Plan_venta_crud({ createData }) {
    // VARIABLES
    const format_excel = [{
        ejercicio: "2023",
        mes: "1",
        cod_articulo: "PT0101030007",
        articulo: "CERRADURA MAXIMA 1000",
        cantidad: "0",
    }];

    // importar excel desde archivo
    const [totalPlan, setTotalPlan] = useState(0);
    const [listDatos, setListDatos] = useState([])
    const [typeError, setTypeError] = useState(null);
    const handleFile = (e) => {
        let fileTypes = ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'text/csv'];
        let selectedFile = e.target.files[0];
        if (selectedFile) {
            if (selectedFile && fileTypes.includes(selectedFile.type)) {
                setTypeError(null);
                let reader = new FileReader();
                reader.readAsArrayBuffer(selectedFile);
                reader.onload = (e) => {
                    // leer archivo
                    const workbook = XLSX.read(e.target.result, { type: 'buffer' });
                    const worksheetName = workbook.SheetNames[0];
                    const worksheet = workbook.Sheets[worksheetName];
                    const data = XLSX.utils.sheet_to_json(worksheet);
                    //setExcelData(data.slice(0, 10));
                    setListDatos(data);
                    setTotalPlan(data.reduce((total, currItem) => total + currItem.cantidad, 0))
                }
            }
            else {
                setTypeError('Seleccione solo tipos de archivos de Excel.');
            }
        }
        else {
            setTypeError('Por favor seleccione su archivo.');
        }
    }

    // BUSQUEDA EN DOM
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_articulo", "articulo"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => String(item[key]).toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // GUARDAR INFORMACION
    const handleSubmit = (e) => {
        if (listDatos.length === 0) {
            alert("No hay datos cargados.");
            return;
        }

        // generar datos
        let c_ejercicio = listDatos[0].ejercicio;
        const new_data = {
            "id_plan": 0,
            "ejercicio": c_ejercicio,
            "sw_aprobado": "C", //CREACION
            "glosa": "",
            "cod_usuario": "",
            "detalle": listDatos
        };
        createData(new_data);
    };

    // PAGINACIÓN 
    let LimitPag = 20;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">

            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        {/*<div className="col-sm">
                            <Title>Total Gasto Real: S/. {totalReal.toLocaleString("en")}</Title>
                        </div>*/}
                        <div className="col-sm">
                            <div className="d-flex justify-content-end gap-1">
                                <input type="search" name="txt_buscar" className="form-control" style={{ maxWidth: '210px', minWidth: '100px' }} placeholder="Buscar" onChange={onSearchChange} />
                                <BotonExcel textoBoton={"Formato"} sw_habilitado={format_excel.length > 0 ? true : false} listDatos={format_excel} nombreArchivo={"Formato_plan_venta"} />
                                <label htmlFor="file_excel" className="btn btn-success btn-sm" ><i className="align-bottom me-1"><BsFileExcel /></i> Cargar Excel</label>
                                <input type="file" id="file_excel" name="file_excel" accept=".xlsx, .xls, .csv" style={{ visibility: 'hidden', maxWidth: '1px' }} onChange={handleFile} />
                                <BotonGrabar textoBoton={"Grabar"} sw_habilitado={true} eventoClick={handleSubmit} />
                            </div>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                {/* INICIO TABLA */}
                <Plan_venta_crud_tabla datos_fila={search(listDatos)} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-end mb-3">
                    <div className="me-auto p-2 bd-highlight">
                        Total General Plan: <span className="fw-bold">{totalPlan.toLocaleString("en")}</span> Unidades.
                    </div>
                    <div className="pagination-wrap hstack gap-1">
                        Total Reg. {listDatos.length}
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                        <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>

        </div>
    );
}