import React, { useState, useEffect } from 'react'
import { BsInfoCircle } from "react-icons/bs";

export function Plan_venta_tabla({ datos_fila, setDataToEdit, deleteData, eventoClick }) {

    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle">Año</th>
                            <th className="align-middle">Cod.Artículo</th>
                            <th className="align-middle">Artículo</th>
                            <th className="align-middle">Enero</th>
                            <th className="align-middle">Febrero</th>
                            <th className="align-middle">Marzo</th>
                            <th className="align-middle">Abril</th>
                            <th className="align-middle">Mayo</th>
                            <th className="align-middle">Junio</th>
                            <th className="align-middle">Julio</th>
                            <th className="align-middle">Agosto</th>
                            <th className="align-middle">Setiembre</th>
                            <th className="align-middle">Octubre</th>
                            <th className="align-middle">Noviembre</th>
                            <th className="align-middle">Diciembre</th>
                            <th className="align-middle">Total</th>
                            <th className="align-middle" title="Opciones" style={{ width: '50px' }}>Opc</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td className="td-cadena">{datos.ejercicio}</td>
                                        <td className="td-cadena">{datos.cod_articulo}</td>
                                        <td className="td-cadena">{datos.articulo}</td>
                                        <td className="td-num">{datos.ene.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.feb.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.mar.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.abr.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.may.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.jun.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.jul.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.ago.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.sep.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.oct.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.nov.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.dic.toLocaleString("en")}</td>
                                        <td className="td-num">{datos.total.toLocaleString("en")}</td>
                                        <td>
                                            <button data-toggle="tooltip" title="Información" className="btn btn-outline-info btn-sm ms-1">
                                                <BsInfoCircle />
                                            </button>
                                        </td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="17">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}