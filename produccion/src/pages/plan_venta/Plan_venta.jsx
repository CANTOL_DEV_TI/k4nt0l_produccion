import React, { useState, useEffect } from 'react'
import {
    get_plan_venta_version,
    get_plan_venta_ejercicio,
    get_plan_venta_lista,
    insert_plan_venta,
    delete_plan_venta,
} from '../../services/plan_venta';
import { BotonEliminar, BotonExcel, BotonNuevo } from "../../components/Botones";
import { InputTextBuscar } from "../../components/InputTextBuscar";
import { useVentanaModal } from "../../hooks/useVentanaModal";
import { Plan_venta_tabla } from "./components/Plan_venta_tabla";
import { Plan_venta_crud } from "./components/Plan_venta_crud";
import ComboBox from "../../components/ComboBox";
import VentanaModal from "../../components/VentanaModal";
import Title from "../../components/Titulo";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

export function Plan_venta() {
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
    const Seguridad = async () => {
        const ValSeguridad = await Validar("PROPLV")
        setVentanaSeguridad(ValSeguridad.Validar)
    }
    // VARIABLES
    const [sw_aprobado, setSw_aprobado] = useState("");
    const [dataToEdit, setDataToEdit] = useState(null);
    const [refrescar, setRefrescar] = useState(false);

    // BUSQUEDA GENERALES
    const [ejercicio, setEjercicio] = useState(0);
    const [id_plan, setId_plan] = useState(0);
    function datosBusqueda(evento) {
        if (evento.target.name === 'cbo_ejercicio') {
            setEjercicio(evento.target.value)
            setId_plan(0)
        }
        if (evento.target.name === 'cbo_version') {
            setId_plan(evento.target.value)
        }
    };

    // CARGAR COMBO EJERCICIO
    const [listEjercicio, setListEjercicio] = useState([])
    useEffect(() => {
        Seguridad();
        get_plan_venta_ejercicio()
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.ejercicio, "nombre": data.ejercicio
                    }
                })
                setListEjercicio(cbo_data)
            });
        setRefrescar(false);
    }, [refrescar])

    // CARGAR COMBO VERSION
    const [listVersion, setListVersion] = useState([])
    useEffect(() => {
        get_plan_venta_version(ejercicio)
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.id_plan, "nombre": data.desc_version
                    }
                })
                setListVersion(cbo_data)
            })
    }, [ejercicio])

    // CONSUMIR DATOS
    const [listDatos, setListDatos] = useState([])
    useEffect(() => {
        setRefrescar(false);
        getData();
    }, [id_plan])

    // CARGAR DATOS
    const getData = () => {
        get_plan_venta_lista(id_plan)
            .then(res => {
                setListDatos(res)

                // obtener estado de plan de ventas
                if (res.length > 0)
                    setSw_aprobado(res[0].sw_aprobado);

                // primera pagina
                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_articulo", "articulo"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // INSERTAR DATOS
    const createData = (data) => {
        insert_plan_venta(data);
        handleClose(true);
        setRefrescar(true);
    };

    // ELIMINAR DATOS
    const deleteData = () => {
        delete_plan_venta(id_plan)
        setRefrescar(true);
        setEjercicio(0);
        setId_plan(0);
    };

    // PAGINACIÓN 
    let LimitPag = 15;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // MOSTRAR MODAL
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = (opc = "N", datos) => {
        console.log("Verificar")
        console.log(opc)
        console.log(datos)

        if (opc === "E") {
            setDataToEdit(datos);
        } else {
            setDataToEdit(null);
        }
        handleShow(true)
    };

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">
            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-3">
                            <Title>Plan de Venta</Title>
                        </div>
                        <div className="col-sm">
                            <div className="d-flex justify-content-end gap-1">
                                <InputTextBuscar onBuscarChange={onSearchChange} />
                                <ComboBox
                                    datosRow={listEjercicio}
                                    nombre_cbo="cbo_ejercicio"
                                    manejaEvento={datosBusqueda}
                                    valor_ini="Año"
                                    valor={ejercicio}
                                />
                                <ComboBox
                                    datosRow={listVersion}
                                    nombre_cbo="cbo_version"
                                    manejaEvento={datosBusqueda}
                                    valor_ini="Versión"
                                    valor={id_plan}
                                />
                                <BotonExcel textoBoton={"Exportar Excel"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatos} nombreArchivo={"Plan venta"} />
                                <BotonNuevo textoBoton={"Añadir Plan"} sw_habilitado={true} eventoClick={() => mostrarVentana("E", listDatos)} />
                                <BotonEliminar textoBoton={"Eliminar Plan"} sw_habilitado={sw_aprobado === "C" ? true : false} eventoClick={() => deleteData()} />
                            </div>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <Plan_venta_tabla datos_fila={search(listDatos)} setDataToEdit={setDataToEdit} deleteData={deleteData} eventoClick={mostrarVentana} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-end m-2">
                    <div className="pagination-wrap hstack gap-1">
                        Total Reg. {listDatos.length}
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                        <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>

            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="lg"
                handleClose={handleClose}
                titulo="Añadir Plan de Venta"
            >
                <Plan_venta_crud
                    datos={dataToEdit}
                    createData={createData}
                />
            </VentanaModal>
            <VentanaBloqueo
                show={VentanaSeguridad}

            />

            {/* FIN MODAL */}
        </div>
    );
}