import React from "react";
import { BsFillTrashFill } from "react-icons/bs"

export function ConfigProcesoDetalle({ datosRow, borrarFilas, listaEquipos, listaTipoOperaciones }) {
    // RETORNAR INFORMACIÓN
    //handleBusqueda = async (txtFind) => {
    //    const responseJson = await getFilter_Equipo()
    //    console.log(responseJson)
    //    this.setState({ resultados: responseJson, isFetch: false })
    //    console.log(this.state)
    //}

    return (
        < div className="table-responsive">
            {/* INICIO TABLA */}
            < table className="table table-hover table-sm table-bordered" >
                <thead className="table-secondary text-center table-sm">
                    <tr>
                    <td className="align-middle">Paso</td>
                        <td className="align-middle" style={{ minWidth: '50px' }}>Equipo</td>
                        <td className="align-middle" style={{ minWidth: '100px' }}>Tipo Operación</td>                        
                        <td className="align-middle">Prioridad</td>
                        <td className="align-middle" style={{ minWidth: '200px' }}>Descripcion</td>
                        <td className="align-middle">S.U.H.</td>
                        <td className="align-middle">Total Turnos</td>                        
                    </tr>
                </thead>
                <tbody className="list">

                    {
                        datosRow.map((datos, index) =>
                            <tr key={index + 1}>
                                <td className="td-cadena">{datos.num_paso}</td>
                                <td className="td-cadena">
                                    <select className='form-control form-control-sm-8' name="sEquipo" value={datos.id_equipo} >
                                        <option value='0'>Seleccionar Equipo</option>
                                        {
                                            listaEquipos.map((v_item) =>
                                                datos.id_equipo === v_item.id_equipo ?
                                                    <option selected value={v_item.id_equipo}>{`${v_item.equipo}`}</option>
                                                    :
                                                    <option value={v_item.id_equipo}>{`${v_item.equipo}`}</option>
                                            )
                                        }
                                    </select>
                                </td>
                                <td className="td-cadena">
                                    <select className='form-control form-control-sm-8' name="sTipoOperacion" value={datos.id_tpoperacion} >
                                        <option value='0'>Seleccionar Tipo Operacion</option>
                                        {
                                            listaTipoOperaciones.map((v_item2) =>
                                                datos.id_tpoperacion === v_item2.id_tpoperacion ?
                                                    <option selected value={v_item2.id_tpoperacion}>{`${v_item2.tipo_operacion}`}</option>
                                                    :
                                                    <option value={v_item2.id_tpoperacion}>{`${v_item2.tipo_operacion}`}</option>
                                            )
                                        }
                                    </select>
                                </td>                                
                                <td className="td-cadena">{datos.num_prioridad}</td>
                                <td className="td-cadena">{datos.desc_proceso}</td>
                                <td className="td-num">{datos.std_und_hora}</td>                                
                                <td>
                                    <span className="actions">
                                        <button><BsFillTrashFill className="baja-btn" data-bs-t onClick={() => borrarFilas(index)} /></button>
                                    </span>
                                </td>
                            </tr>
                        )
                    }
                </tbody>
            </table >

            {/* FIN TABLA */}
        </div >
    );
}