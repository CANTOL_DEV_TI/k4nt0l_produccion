import React from "react";
import {GrEdit} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({                        
                        id_fmproceso,
                        cod_articulo,
                        articulo,
                        cant_lote_min,
                        eventoEditar,
                        eventoEliminar}) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena " id={id_fmproceso}>{id_fmproceso}</td>                
                <td className="td-cadena " >{cod_articulo}</td>
                <td className="td-cadena " >{articulo}</td>
                <td className="td-cadena " >{cant_lote_min}</td>
                <td><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                
            </tr>
        </tbody>        
    )

export default ResultadoTabla;