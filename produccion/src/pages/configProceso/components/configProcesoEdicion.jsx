import React from "react";
import { BotonGuardar, BotonConsultar, BotonAdd, BotonAtras } from "../../../components/Botones";
import { getNombre_Item } from "../../../services/configProcesoServices";
import { ConfigProcesoDetalle } from "./configProcesoDetalle";
import ConfigProcesoPasoEdicion from "./configProcesoAñadirPaso";
import VentanaModal from "../../../components/VentanaModal";
import { getLista_Equipos } from "../../../services/equipoServices";
import { getLista_TipoOperacion } from "../../../services/tipoOperacionServices";
import { getDetalleProceso } from "../../../services/configProcesoServices";

class ConfigProcesoEdicion extends React.Component {

    constructor(props) {
        super(props);
        
        if (this.props.dataToEdit == null) {
            this.state = { id_fmproceso: 0, cod_articulo: '', articulo: '', cant_lote_min: 0, detalles: [], showModal : true , listEquipos : [], listTipoOperacion : []}
        } else {               
            this.state = { id_fmproceso: this.props.dataToEdit.id_fmproceso, cod_articulo: this.props.dataToEdit.cod_articulo, articulo: this.props.dataToEdit.articulo, cant_lote_min: this.props.dataToEdit.cant_lote_min, detalles : [] }                        
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'id_fmproceso') {
            this.setState({ id_fmproceso: e.target.value })
        }

        if (e.target.name === 'cod_articulo') {
            this.setState({ cod_articulo: e.target.value })
        }

        if (e.target.name === 'articulo') {
            this.setState({ articulo: e.target.value })
        }

        if (e.target.name === 'cant_lote_min') {
            this.setState({ cant_lote_min: e.target.value })
        }

    }

    handleFind = async (txtFind) => {
        const responseJson = await getNombre_Item(txtFind)        
        this.setState({ articulo: responseJson[0].nombre })
    }

    handleDetalle = async (txtID) => {
        const ListaDetalle = await fetch (getDetalleProceso(txtID))
        const rptaDet = await ListaDetalle.json()        
        return rptaDet        
    }


    handleSubmit = (newRow) => {
        this.setState({ detalles: [...this.state.detalles, newRow] })
        //[...Filas, newRow]
        //console.log('Nueva Fila')
        //console.log(newRow)
        //console.log('Todo')
        //console.log(this.state.detalles)
    }

    handleShowNuevaLinea = (e) => {
        console.log(this.state.listTipoOperacion)
        this.setState({ showModal: !this.state.show })        
    }

    HandleBorrarFila = (targetIndex) =>{
        //console.log(this.state.detalles)
        //console.log(targetIndex)
        const items = this.state.detalles
        //delete items[targetIndex]
        items.splice(targetIndex,1)
        //console.log(items)
        this.setState({detalles : items})
    }

    render() {
        const { dataToEdit, eventOnclick, nameOpe, EventoVolver } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label"></div>
                            <div className="col-sm-2 col-form-label">Codigo : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="id_fmproceso" autoFocus onChange={this.handleChange} value={this.state.id_fmproceso} /></div>
                            <div className="col-sm-3 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label"></div>
                            <div className="col-sm-2 col-form-label">Codigo Articulo : </div>
                            <div className="col-sm-2 col-form-label">
                                <input type="text" name="cod_articulo" onChange={this.handleChange} value={this.state.cod_articulo} />
                                <BotonConsultar sw_habilitado={true} eventoClick={() => this.handleFind(this.state.cod_articulo)} texto="SAP" />
                            </div>
                            <div className="col-sm-3 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label"></div>
                            <div className="col-sm-2 col-form-label">Articulo : </div>
                            <div className="col-sm-3 col-form-label">
                                <input type="text" className="form-control form-control-sm" name="articulo" onChange={this.handleChange} value={this.state.articulo} />
                            </div>
                            <div className="col-sm-1 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label"></div>
                            <div className="col-sm-2 col-form-label">Cant. Minimo Lote : </div>
                            <div className="col-sm-2 col-form-label"><input type="number" name="cant_lote_min" min={1} max={99999} onChange={this.handleChange} value={this.state.cant_lote_min} /></div>
                            <div className="col-sm-3 col-form-label"></div>
                        </div>

                    </div>
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">
                            </div>
                            <div className="col-sm-2 col-form-label">
                                <BotonAdd sw_habilitado={true} textoBoton={"Agregar Detalle"} eventoClick={this.handleShowNuevaLinea} />
                            </div>
                            <div className="col-sm-4 col-form-label">
                            </div>
                        </div>
                    </div>
                </div>
                <ConfigProcesoDetalle datosRow={this.state.detalles} listaEquipos={this.state.listEquipos} listaTipoOperaciones={this.state.listTipoOperacion} borrarFilas={this.HandleBorrarFila} />
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "id_fmproceso": this.state.id_fmproceso, "cod_articulo": this.state.cod_articulo, "articulo": this.state.articulo, "cant_lote_min": this.state.cant_lote_min, "detalles" : this.state.detalles })} texto={"Guardar"} />
                <BotonAtras sw_habilitado={true} textoBoton={"Atras"} eventoClick={EventoVolver} />
                <VentanaModal show={this.state.showModal} handleClose={() => this.setState({ showModal: false })} size={"lg"} titulo={"Agregar Nuevo paso en Proceso"}>
                    <ConfigProcesoPasoEdicion onSubmit={this.handleSubmit} nameOpe={"agregar"} ListaEquipos={this.state.listEquipos} ListaTipoO={this.state.listTipoOperacion} />
                </VentanaModal>

            </div>

        )
    }

    async componentDidMount(){
        const ListaEq = await getLista_Equipos();        
        this.setState({listEquipos: ListaEq})

        const ListaTOp = await getLista_TipoOperacion();           
        this.setState({listTipoOperacion : ListaTOp})
        
        const ListaDetalle = await getDetalleProceso(this.state.id_fmproceso)
        this.setState({detalles : ListaDetalle})
        console.log(this.state)

    }

}

export default ConfigProcesoEdicion;