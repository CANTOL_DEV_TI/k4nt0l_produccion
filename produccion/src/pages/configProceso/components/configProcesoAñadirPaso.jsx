import React from 'react'
import { BotonAdd } from '../../../components/Botones';

class ConfigProcesoPasoEdicion extends React.Component {
    constructor(props) {
        super(props);

        this.state = { num_paso: "", num_prioridad: 0, desc_proceso: "", id_tpoperacion : 0, id_equipo: 0, std_und_hora: 0 }

    }

    validateForm = () => {
        if (this.state.num_paso && this.state.num_prioridad && this.state.desc_proceso && this.state.id_tpoperacion && this.state.id_equipo && this.state.std_und_hora ) {
            return true;
        } else {
            return false;
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        
        if (!this.validateForm) return;

        this.props.onSubmit(this.state);

        //setShow(false)

    };

    handleChange = (e) => {

        if (e.target.name === "tPaso") {
            this.setState({ num_paso: e.target.value })
        }

        if (e.target.name === "tDescripcion") {
            this.setState({ desc_proceso: e.target.value })
        }

        if (e.target.name === "tPrioridad") {
            this.setState({ num_prioridad: e.target.value })
        }

        if (e.target.name === "tUnidHora") {
            this.setState({ std_und_hora: e.target.value })
        }

        if (e.target.name === "sEquipo"){
            this.setState({id_equipo : e.target.value})
        }

        if (e.target.name === "sTipoOpe"){
            this.setState({id_tpoperacion : e.target.value})
        }

    }

    handleClose = (e) => {
        this.props.closeDetalle = false
    }

    render() {

        const { dataToEdit, onSubmit, nameOpe, ListaEquipos, ListaTipoO } = this.props

        return (
            <div className="card cardalign w-75rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Paso : </div>
                            <div className="col-sm-2 col-form-label"><input className='form-control form-control-sm' id='tPaso' name='tPaso' type='text' placeholder='Paso' autoFocus onChange={this.handleChange} value={this.state.num_paso} ></input></div>
                            <div className="col-sm-4 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Nro Prioridad : </div>
                            <div className="col-sm-2 col-form-label"><input className='form-control form-control-sm' id='tPrioridad' name='tPrioridad' type='text' placeholder='Prioridad' onChange={this.handleChange} value={this.state.num_prioridad}></input></div>
                            <div className="col-sm-4 col-form-label"></div>
                        </div>

                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Descripcion : </div>
                            <div className="col-sm-6 col-form-label"><input className='form-control form-control-sm-8' id='tDescripcion' name='tDescripcion' placeholder='Descripcion del Paso' value={this.state.desc_proceso} type='text' onChange={this.handleChange}></input></div>

                        </div>
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Equipo : </div>
                            <div className="col-sm-6 col-form-label">
                                <select className='form-control form-control-sm-8' name="sEquipo" onChange={this.handleChange}>
                                    <option value='0'>Seleccionar Equipo</option>
                                    {
                                        ListaEquipos.map((v_item) =>
                                            this.state.id_equipo === v_item.id_equipo ?
                                                <option selected value={v_item.id_equipo}>{`${v_item.equipo}`}</option>
                                                :
                                                <option value={v_item.id_equipo}>{`${v_item.equipo}`}</option>
                                        )
                                    }
                                </select>
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Tipo Operacion : </div>
                            <div className="col-sm-6 col-form-label">
                                <select className='form-control form-control-sm-8' name="sTipoOpe" onChange={this.handleChange}>
                                    <option value='0'>Seleccionar Tipo</option>
                                    {
                                        ListaTipoO.map((v_tipo) =>
                                            this.state.id_tpoperacion === v_tipo.tipo_operacion ?
                                                <option selected value={v_tipo.id_tpoperacion}>{`${v_tipo.tipo_operacion}`}</option>
                                                :
                                                <option value={v_tipo.id_tpoperacion}>{`${v_tipo.tipo_operacion}`}</option>
                                        )
                                    }
                                </select>
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-sm-4 col-form-label">Uni. Hora: </div>
                            <div className="col-sm-2 col-form-label">
                                <input className='form-control form-control-sm' id='tUnidHora' name='tUnidHora' placeholder='Standar Unidad x Hora' value={this.state.std_und_hora} type='text' onChange={this.handleChange}></input></div>
                            <div className="col-sm-4 col-form-label"></div>
                        </div>
                       
                        <div className="form-group row">
                            <div className="col-sm-8 col-form-label"></div>
                            <div className="col-sm-2 col-form-label">
                                <BotonAdd textoBoton={nameOpe} sw_habilitado={true} eventoClick={this.handleSubmit}></BotonAdd>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )

    }
}

export default ConfigProcesoPasoEdicion;