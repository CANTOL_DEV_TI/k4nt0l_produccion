import React from "react";
import { getFilter_ConfigProceso, save_ConfigProceso, update_ConfigProceso, delete_ConfigProceso } from "../../services/configProcesoServices.jsx";
import Busqueda from "../configProceso/components/Busqueda";
import ResultadoTabla from "../configProceso/components/ResultadoTabla";
import ConfigProcesoEdicion from "../configProceso/components/configProcesoEdicion.jsx";
import { BotonNuevo } from "../../components/Botones.jsx";
import {  Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";


class ConfigProceso extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            tipoOpe: 'Find',
            showComponente: 'Find',
        }
    }

    handleBusqueda = async (txtFind) => {
        const responseJson = await getFilter_ConfigProceso(txtFind)

        this.setState({ resultados: responseJson, isFetch: false })
    }

    handleSubmit = async (e) => {
        console.log(e)
        const valido = true

        this.valido = true

        if (e.cod_articulo === '') {
            this.valido = false
            alert("El codigo de articulo no puede ir vacio")
        }

        if (e.articulo === '') {
            this.valido = false
            alert("El nombre del articulo no puede ir vacio")
        }

        if (e.detalles.length === 0) {
            this.valido = false
            alert("No puede ir sin detalles")
        }

        if (e.cant_lote_min === '' || e.cant_lote_min <= 0) {
            this.valido = false
            alert("Valor invalido para la cantidad minima")
        }

        if (this.valido === true) {
            console.log("Entro : " + this.state.tipoOpe)

            if (this.state.tipoOpe === 'nuevo') {
                const responseJson = await save_ConfigProceso(e)
                console.log(responseJson)
            }

            if (this.state.tipoOpe === 'Actualizar') {
                const responseJson = await update_ConfigProceso(e)
                console.log(responseJson)
            }

            if (this.state.tipoOpe === 'Eliminar') {
                const responseJson = await delete_ConfigProceso(e)
                console.log(responseJson)
            }


            
        }
    }

    handleAtras = async (e) => {
        this.setState({ dataRegistro: null })
        this.setState({ tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad } = this.state

        return (
            <React.Fragment>
                {this.state.tipoOpe == 'nuevo' || this.state.tipoOpe == 'Actualizar' || this.state.tipoOpe == 'Eliminar' ?
                    <ConfigProcesoEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={'Grabar'}
                        showDetalle={false}
                        EventoVolver={this.handleAtras}
                    />
                    :
                    <>
                        <Busqueda handleBusqueda={this.handleBusqueda} showComponente={'nuevo'} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Find' })} />

                        {isFetch && 'Cargando'}
                        {(!isFetch && !resultados.length) && 'Sin Informacion'}

                        <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                            <table className="table table-hover table-sm table-bordered table-striped" >
                                <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                                    <tr >
                                        <th colSpan={6} ><BotonNuevo textoBoton={"Crear Nuevo"} sw_habilitado={true} eventoClick={() => this.setState({ tipoOpe: 'nuevo' })} /></th>
                                    </tr>
                                    <tr >
                                        <th className="align-middle" >Codigo C.P.</th>
                                        <th className="align-middle" >Codigo Articulo</th>
                                        <th className="align-middle" >Nombre Articulo</th>
                                        <th className="align-middle" >Cant. Lote Min.</th>
                                        <th className="align-middle" >Editar</th>
                                        <th className="align-middle" >Eliminar</th>
                                    </tr>
                                </thead>

                                {resultados.map((registro) =>
                                    <ResultadoTabla
                                        key={registro.id_fmproceso}
                                        id_fmproceso={registro.id_fmproceso}
                                        cod_articulo={registro.cod_articulo}
                                        articulo={registro.articulo}
                                        cant_lote_min={registro.cant_lote_min}
                                        eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                        eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}
                                    />
                                )}
                                <tfoot>

                                </tfoot>
                            </table>
                            <VentanaBloqueo
                                show={this.state.VentanaSeguridad}

                            />
                        </div>
                    </>
                }

            </React.Fragment>
        )

    }
    async componentDidMount() {      
        const ValSeguridad = await Validar("PROCPR")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
    }
}

export default ConfigProceso;