import React, { useState, useEffect } from 'react'

export function Turno_crud_item({ formularioDet, setFormularioDet }) {

    // CAPTURAR EL EVENTO DE LOS IMPUTS
    const handleChange = (evento) => {
        // 1. Hacer una copia superficial de los elementos
        let items = [...formularioDet];
        // 2. Haz una copia superficial del elemento que deseas mutar
        let item = { ...items[evento.target.name - 1] }
        // 3. Reemplace la propiedad en la que está interesado
        item.cant_hora_lab = evento.target.value;
        // 4. Vuelva a ponerlo en nuestra matriz. NÓTESE BIEN. *estamos* mutando la matriz aquí, pero por eso primero hicimos una copia
        items[evento.target.name - 1] = item;
        // 5. Establecer el estado en nuestra nueva copia
        setFormularioDet(items)      
    };

    // RETORNAR INFORMACIÓN
    return (
        <>
            <table className="table table-hover table-sm table-bordered">
                <thead className="table-secondary text-center table-sm">
                    <tr>
                        <th className="align-middle" scope="col">Num.Día</th>
                        <th className="align-middle" style={{ width: '200px' }}>Día</th>
                        <th className="align-middle">Horas Laborables</th>
                    </tr>
                </thead>
                <tbody className="list">
                    {formularioDet.length > 0 ?
                        (
                            formularioDet.map((datos, index) => {
                                return (
                                    <tr key={datos.num_dia}>
                                        <th scope="row">{datos.num_dia}</th>
                                        <td>{datos.nom_dia}</td>
                                        <td><input type="number" className="form-control form-control-sm" name={datos.num_dia} onChange={handleChange}
                                            value={datos.cant_hora_lab} /></td>
                                    </tr>
                                )
                            })
                        ) :
                        (
                            <tr>
                                <td className="text-center" colSpan="3">Sin datos</td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
        </>
    );
}