import React, { useState, useEffect } from "react";
import { Turno_crud_item } from "./Turno_crud_item";
import { get_turno_det } from '../../../services/turnoServices';

const iniFormulario = {
    cod_turno: "0",
    nom_turno: "",
    glosa: "",
    sw_estado: "1",
    cod_usuario: "1",
};

const iniFormularioDet = [
    { num_dia: 1, nom_dia: "LUNES", cant_hora_lab: 0 },
    { num_dia: 2, nom_dia: "MARTES", cant_hora_lab: 0 },
    { num_dia: 3, nom_dia: "MIERCOLES", cant_hora_lab: 0 },
    { num_dia: 4, nom_dia: "JUEVES", cant_hora_lab: 0 },
    { num_dia: 5, nom_dia: "VIERNES", cant_hora_lab: 0 },
    { num_dia: 6, nom_dia: "SABADO", cant_hora_lab: 0 },
    { num_dia: 7, nom_dia: "DOMINGO", cant_hora_lab: 0 },
];

export function Turno_crud({ createData, updateData, dataToEdit, setDataToEdit, sw_mode = "C" }) {
    // C: CREAR , M: MODIFICAR

    // Asignar a variables de estado    
    const [formulario, setFormulario] = useState(iniFormulario);
    const [formularioDet, setFormularioDet] = useState(iniFormularioDet);
    const [checked, setChecked] = useState(false);
    useEffect(() => {
        if (dataToEdit) {
            if (dataToEdit.sw_estado === "1") {
                setChecked(true);
            } else {
                setChecked(false);
            }
            setFormulario(dataToEdit);
            getData(dataToEdit.cod_turno);
        } else {
            setChecked(true);
            setFormulario(iniFormulario);
        }
    }, [dataToEdit]);

    // CARGAR DATOS DE DETALLE DE TURNO
    const getData = (cod_turno) => {
        get_turno_det(cod_turno)
            .then(res => {
                setFormularioDet(res)
            })
    };

    // CAPTURAR EL EVENTO DE LOS IMPUTS
    const handleChange = (evento) => {
        if (evento.target.name === 'sw_estado') {
            setChecked(!checked);
            setFormulario({
                ...formulario,
                [evento.target.name]: evento.target.checked === true ? "1" : "0",
            });
        } else {
            setFormulario({
                ...formulario,
                [evento.target.name]: (evento.target.value).toUpperCase(),
            });
        }
    };

    // GUARDAR INFORMACION
    const handleSubmit = (e) => {
        if (!formulario.nom_turno) {
            alert("Ingrese nombre de turno.");
            return;
        }

        // generar datos
        const new_data = {
            "cod_turno": formulario.cod_turno,
            "nom_turno": formulario.nom_turno,
            "glosa": formulario.glosa,
            "sw_estado": formulario.sw_estado,
            "cod_usuario": "1",
            "detalle": formularioDet
        };

        if (sw_mode === "C") 
        {
            createData(new_data);

        } else {            
            updateData(new_data);
        }

        handleReset();
    };

    const handleReset = (e) => {
        setFormulario(iniFormulario);
        setDataToEdit(null);
    };

    // RETORNAR INFORMACIÓN
    return (
        <div className="row g-2">
            {/* INICIO  FORM*/}
            <div className="col-md-3">
                <label className="form-label">Cod. Turno</label>
                <input type="text" readOnly className="form-control form-control-sm" name="cod_turno" value={formulario.cod_turno} />
            </div>
            <div className="col-md-9">
                <label className="form-label">Nombre Turno</label>
                <input type="text" className="form-control form-control-sm" name="nom_turno" onChange={handleChange} value={formulario.nom_turno} />
            </div>
            <div className="col-md-3">
                <label className="form-label">Observación:</label>
            </div>
            <div className="col-md-9">
                <input type="text" className="form-control form-control-sm" name="glosa" onChange={handleChange} value={formulario.glosa} />
            </div>
            <div className="col-12">
                <Turno_crud_item formularioDet={formularioDet} setFormularioDet={setFormularioDet} />
            </div>
            <div className="col-12">
                <input className="form-check-input" type="checkbox" name="sw_estado" checked={checked} onChange={handleChange} />
                <label className="form-check-label ms-2">
                    Habilitado
                </label>
            </div>

            <div className="modal-footer">
                <button type="submit" className="btn btn-primary" onClick={handleSubmit}>{`${sw_mode === "C" ? 'Crear' : 'Actualizar'}`}</button>{/* Actualizar*/}
            </div>
            {/* FIN FORM */}
        </div>
    );
}