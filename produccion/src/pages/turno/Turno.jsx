import React, { useState, useEffect } from 'react'
import { get_turno, insert_turno, update_turno, delete_turno } from '../../services/turnoServices';
import { BotonExcel, BotonNuevo } from "../../components/Botones";
import { InputTextBuscar } from "../../components/InputTextBuscar";
import { useVentanaModal } from "../../hooks/useVentanaModal";
import { Turno_tabla } from "./components/Turno_tabla";
import { Turno_crud } from "./components/Turno_crud";
import VentanaModal from "../../components/VentanaModal";
import Title from "../../components/Titulo";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";
//import { Await } from 'react-router-dom';

export function Turno() {
    
    const Seguridad = async() =>{        
        const ValSeguridad = await Validar("PROTUL")
		setVentanaSeguridad(ValSeguridad.Validar)

    }

    const [dataToEdit, setDataToEdit] = useState(null);

    // CONSUMIR DATOS
    const [VentanaSeguridad, setVentanaSeguridad] = useState(true);
    const [sw_modo, setSw_modo] = useState("C");
    const [refrescar, setRefrescar] = useState(true);
    const [listDatos, setListDatos] = useState([]);
    useEffect(() => {
        Seguridad();
        setRefrescar(false);
        getData();
        console.log(sessionStorage.getItem("userID"));
    }, [refrescar])

    //
    const getData = () => {
        get_turno()
            .then(res => {
                setListDatos(res)
                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM
    //const VentanaSeguridad
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_turno", "nom_turno", "sw_estado"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // INSERTAR DATOS
    const createData = (data) => {
        const res = insert_turno(data);
        setRefrescar(true);
        getData();
        handleClose(true);
    };

    // ACTUALIZAR DATOS
    const updateData = (data) => {
        const res = update_turno(data)
        setRefrescar(true);
        getData();
        handleClose(true)
    };

    // ACTUALIZAR DATOS
    const deleteData = (id) => {
        delete_turno(id)
        setRefrescar(true);
        getData();
    };

    // PAGINACIÓN 
    let LimitPag = 15;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // MOSTRAR MODAL
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = (opc = "N", datos) => {
        if (opc === "E") {
            setDataToEdit(datos);
            setSw_modo("M")
        } else {
            setDataToEdit(null);
            setSw_modo("C")
        }
        handleShow(true)
    };

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">

            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-sm-auto">
                            <Title>Lista de Turno Laboral</Title>
                        </div>
                        <div className="col-sm">
                            <div className="d-flex justify-content-end gap-1">
                                <InputTextBuscar onBuscarChange={onSearchChange} />
                                <BotonExcel textoBoton={"Exportar Excel"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatos} nombreArchivo={"Presupesto mensual"} />
                                <BotonNuevo textoBoton={"Crear"} sw_habilitado={true} eventoClick={mostrarVentana} />
                            </div>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <Turno_tabla datos_fila={search(listDatos)} setDataToEdit={setDataToEdit} deleteData={deleteData} eventoClick={mostrarVentana} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-end m-2">
                    <div className="pagination-wrap hstack gap-1">
                        Total Reg. {listDatos.length}
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                        <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>

            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="xsm"
                handleClose={handleClose}
                titulo={dataToEdit != null ? "Editar Turno Laboral" : "Crear Turno Laboral"}
            >
                <Turno_crud
                    createData={createData}
                    updateData={updateData}
                    dataToEdit={dataToEdit}
                    setDataToEdit={setDataToEdit}
                    sw_mode={sw_modo}
                />
            </VentanaModal>
            {/* FIN MODAL */}
            <VentanaBloqueo
                show={VentanaSeguridad}

            />
        </div>
    );
}