import React from "react";

import { getFilter_TipoEquipo, save_TipoEquipo, update_TipoEquipo, delete_TipoEquipo } from "../../services/tipoEquipoServices.jsx";

import Busqueda from "../tipoEquipo/components/Busqueda";
import ResultadoTabla from "../tipoEquipo/components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal";
import TipoEquipoEdicion from "../tipoEquipo/components/TipoEquipoEdicion.jsx";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

class TipoEquipo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find'
        }
    }

    handleBusqueda = async (txtFind) => {
        const responseJson = await getFilter_TipoEquipo(txtFind)
        console.log(responseJson)
        this.setState({ resultados: responseJson, isFetch: false })
        console.log(this.state)
    }

    handleSubmit = async (e) => {
        console.log(e)
            
        if (this.state.tipoOpe === 'Grabar') {
            const responseJson = await save_TipoEquipo(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar') {
            const responseJson = await update_TipoEquipo(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar') {
            const responseJson = await delete_TipoEquipo(e)
            console.log(responseJson)
        }


        this.setState({ showModal: false, tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad } = this.state

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} />


                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>
                                <th className="align-middle">ID</th>
                                <th className="align-middle">Tipo Equipo</th>
                                <th className="align-middle">Editar</th>
                                <th className="align-middle">Eliminar</th>
                            </tr>
                        </thead>
                        
                        {resultados.map((registro) =>
                            <ResultadoTabla
                                key={registro.id_tpequipo}
                                id_tpequipo={registro.id_tpequipo}
                                tipo_equipo={registro.tipo_equipo}                                
                                eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}
                            />
                        )}
                    </table>
                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({ showModal: false })}
                    titulo="Edición Tipo Equipo">
                    <TipoEquipoEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={this.state.tipoOpe}
                    />
                </VentanaModal>
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}

                />
            </React.Fragment>
        )

    }
    async componentDidMount(){        
        const ValSeguridad = await Validar("PROTEQ")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
    }
}

export default TipoEquipo;
