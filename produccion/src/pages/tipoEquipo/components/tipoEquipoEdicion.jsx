import React from "react";
import { BotonGuardar } from "../../../components/Botones";

class TipoEquipoEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { id_tpequipo: 0, tipo_equipo: '' }
        } else {
            this.state = { id_tpequipo: this.props.dataToEdit.id_tpequipo, tipo_equipo: this.props.dataToEdit.tipo_equipo }
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'codigo') {
            this.setState({ id_tpequipo: e.target.value })
        }

        if (e.target.name === 'nombre') {
            this.setState({ tipo_equipo: e.target.value })
        }
    }


    render() {
        const { eventOnclick, nameOpe } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-4 col-form-label"><input type="text" className="form-control form-control-sm"  name="codigo" onChange={this.handleChange} value={this.state.id_tpequipo} /></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Nombre : </div>
                            <div className="col-sm-6 col-form-label"><input type="text" className="form-control form-control-sm"  name="nombre" autoFocus onChange={this.handleChange} value={this.state.tipo_equipo} /></div>
                        </div>
                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "id_tpequipo": this.state.id_tpequipo, "tipo_equipo": this.state.tipo_equipo })} texto={nameOpe} />
            </div>
        )
    }
}

export default TipoEquipoEdicion;
