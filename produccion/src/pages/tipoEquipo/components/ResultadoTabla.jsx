import React from "react";
import {GrEdit} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({id_tpequipo,
                        tipo_equipo,                        
                        eventoEditar,
                        eventoEliminar}) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena" id={id_tpequipo}>{id_tpequipo}</td>
                <td className="td-cadena" >{tipo_equipo}</td>
                <td><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                
            </tr>
        </tbody>
    )

export default ResultadoTabla;