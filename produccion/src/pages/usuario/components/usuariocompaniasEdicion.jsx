import React from "react";
import { BotonGuardar, BotonAdd, BotonEliminar } from "../../../components/Botones";
import { getEmpresasAsignadas,add_Cia_Usuario,del_Cia_Usuario } from "../../../services/usuarioServices";

class UsuarioCiasEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { usuario_id: 0, usuario_codigo: '', empresa_id: 0 ,ListAsignadas:[]}
        } else {
            this.state = { usuario_id: this.props.dataToEdit.usuario_id, usuario_codigo: this.props.dataToEdit.usuario_codigo, empresa_id: this.props.dataToEdit.empresa_id ,ListAsignadas:[]}
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'usuario_id') {
            this.setState({ usuario_id: e.target.value })
        }

        if (e.target.name === 'Empresas') {
            this.setState({ empresa_id: e.target.value })
        }

        if(e.target.name === 'EmpresasAsignadas') {
            this.setState({ empresa_id : e.target.value})
        }

    }

    handleRefresca = async(pUsuario) => {
        const ListaEmpresasAsig = await getEmpresasAsignadas(pUsuario);
        this.setState({ListAsignadas : ListaEmpresasAsig})  
    }

    handleAddEmpresa = async(e) =>{
        const data = this.state
        console.log(data)
        const Agrega = await add_Cia_Usuario(data);
        console.log(Agrega)
        this.handleRefresca(data.usuario_id)
    }

    handleDelEmpresa = async (e) =>{
        const data = this.state
        const QuitarAcceso = await  del_Cia_Usuario(data);
        console.log(QuitarAcceso)
        this.handleRefresca(data.usuario_id)
    }

    render() {
        const { eventOnclick, nameOpe, ListEmpresas } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">ID : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="usuario_id" autoFocus onChange={this.handleChange} value={this.state.usuario_id} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>

                        <div>
                            <div className="form-group row">
                                <div className="col-sm-3 col-form-label">Codigo : </div>
                                <div className="col-sm-2 col-form-label"><input type="text" name="usuario_codigo" onChange={this.handleChange} value={this.state.usuario_codigo} /></div>
                                <div className="col-sm-2 col-form-label"></div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Empresas : </div>
                            <div className="col-sm-2 col-form-label">
                                <select name="Empresas" onChange={this.handleChange}>
                                    <option value='0'>Seleccionar Empresa</option>
                                    {
                                        ListEmpresas.map((v_empresas) =>                                            
                                            <option key={v_empresas.empresa_id} value={v_empresas.empresa_id}>{`${v_empresas.empresa_nombre}`}</option>
                                        )
                                    }

                                </select>
                                <div className="col-sm-2 col-form-label">
                                    <BotonAdd textoBoton="Agregar Acceso" eventoClick={this.handleAddEmpresa} />
                                </div>
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Acceso a : </div>
                            <div className="col-sm-2 col-form-label">
                                <select name="EmpresasAsignadas" size={6} onChange={this.handleChange}>                                    
                                    {
                                        this.state.ListAsignadas.map((v_asignadas) =>                                            
                                            <option key={v_asignadas.empresa_id} value={v_asignadas.empresa_id}>{`${v_asignadas.empresa_nombre}`}</option>
                                        )
                                    }

                                </select>
                                <div className="col-sm-2 col-form-label"></div>
                            </div>
                        </div>
                        <div className="form-group row">
                        <div className="col-sm-3 col-form-label">
                            </div>
                            <div className="col-sm-2 col-form-label">
                                <BotonEliminar textoBoton="Quitar Acceso" sw_habilitado={true} eventoClick={this.handleDelEmpresa} />
                            </div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "usuario_id": this.state.usuario_id, "usuario_codigo": this.state.usuario_codigo, "empresa_id": this.state.empresa_id})} texto={nameOpe} />
            </div>
        )
    }
    async componentDidMount(){
        const ListaEmpresasAsig = await getEmpresasAsignadas(this.state.usuario_id);
        this.setState({ListAsignadas : ListaEmpresasAsig})   
        console.log(this.state.ListAsignadas)     
    }
}

export default UsuarioCiasEdicion;