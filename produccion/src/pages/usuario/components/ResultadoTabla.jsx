import React from "react";
import {GrEdit,GrAmazon,GrCodeSandbox,GrTask} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({usuario_id,                        
                        usuario_codigo,
                        usuario_nombre,                        
                        usuario_estado,
                        eventoEditar,
                        eventoEliminar,
                        eventoAcceso,
                        eventoModulos}) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena" id={usuario_id}>{usuario_codigo}</td>                
                <td className="td-cadena" >{usuario_nombre}</td>
                <td className="td-cadena" >{usuario_estado}</td>                
                <td style={{textAlign:"center"}}><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td style={{textAlign:"center"}}><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>
                <td style={{textAlign:"center"}}><button onClick={() => eventoAcceso(true)}><GrCodeSandbox/></button></td>                                
                <td style={{textAlign:"center"}}><button onClick={() => eventoModulos(true)}><GrTask/></button></td>
            </tr>
        </tbody>
    )

export default ResultadoTabla;