import { BotonAdd } from "../../../components/Botones";

export function EquipoBuscar_tabla({ datos_fila, eventoClick }) {
    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle">Cod. Equipo</th>
                            <th className="align-middle">Nombre Equipo</th>
                            <th className="align-middle" title="Opciones" style={{ width: '88px' }}>Acciones</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td className="td-cadena">{datos.cod_equipo}</td>
                                        <td className="td-cadena">{datos.equipo}</td>
                                        <td>
                                            <BotonAdd eventoClick={() => eventoClick(datos)} textoBoton={"Añadir"} title={"Añadir"} />
                                        </td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="3">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}