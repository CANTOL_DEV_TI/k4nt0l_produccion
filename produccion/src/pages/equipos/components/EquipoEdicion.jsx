import React from "react";
import { BotonGuardar } from "../../../components/Botones";

class EquipoEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { id_equipo: 0, cod_subarea: 0, id_tpequipo: 0, cod_equipo: '', equipo: '', factor_desviacion: 0, oferta_horas_x_dia: 0, sw_operativo: 0, disponibilidad_historico: 100, total_turnos: 0 }
        } else {
            this.state = {  id_equipo: this.props.dataToEdit.id_equipo, cod_subarea: parseInt(this.props.dataToEdit.cod_subarea), id_tpequipo: parseInt(this.props.dataToEdit.id_tpequipo), 
                            cod_equipo: this.props.dataToEdit.cod_equipo, equipo: this.props.dataToEdit.equipo, factor_desviacion: parseFloat(this.props.dataToEdit.factor_desviacion), 
                            oferta_horas_x_dia: parseFloat(this.props.dataToEdit.oferta_horas_x_dia), sw_operativo: this.props.dataToEdit.sw_operativo, 
                            disponibilidad_historico: parseFloat(this.props.dataToEdit.disponibilidad_historico), total_turnos: parseInt(this.props.dataToEdit.total_turnos) }
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'id_equipo') {
            this.setState({ id_equipo: e.target.value })
        }

        if (e.target.name === 'cod_subarea') {
            this.setState({ cod_subarea: e.target.value })
        }

        if (e.target.name === 'id_tpequipo') {
            this.setState({ id_tpequipo: e.target.value })
        }

        if (e.target.name === 'cod_equipo') {
            this.setState({ cod_equipo: e.target.value })
        }

        if (e.target.name === 'equipo') {
            this.setState({ equipo: e.target.value })
        }

        if (e.target.name === 'factor_desviacion') {
            this.setState({ factor_desviacion: e.target.value })
        }

        if (e.target.name === 'total_turnos') {
            this.setState({ total_turnos: e.target.value })
        }

        if (e.target.name === 'oferta_horas_x_dia') {
            this.setState({ oferta_horas_x_dia: e.target.value })
        }

        if (e.target.name === 'sw_operativo') {
            if(e.target.value==="true"){
                this.setState({ sw_operativo: true })
            }else{
                this.setState({ sw_operativo: false })
            }

        }

        if (e.target.name === 'disponibilidad_historico') {
            this.setState({ disponibilidad_historico: e.target.value })
        }
    }


    render() {
        const { eventOnclick, nameOpe, listSubAreas, listTiposEquipo } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">ID : </div>
                            <div className="col-sm-3 col-form-label">
                                <input className="form-control form-control-sm Disabled readonly input" type="text" name="id_equipo" autoFocus onChange={this.handleChange} value={this.state.id_equipo} />

                            </div>
                        </div>

                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">SubArea : </div>
                            <div className="col-sm-6 col-form-label">
                                <select className="form-control form-control-sm" name="cod_subarea" onChange={this.handleChange}>
                                    <option value='0'>Seleccionar SubArea</option>
                                    {
                                        listSubAreas.map((v_subarea) =>
                                            this.state.cod_subarea === v_subarea.cod_subarea ?
                                                <option key={v_subarea.cod_subarea} selected value={v_subarea.cod_subarea}>{`${v_subarea.nom_subarea}`}</option>
                                                :
                                                <option key={v_subarea.cod_subarea} value={v_subarea.cod_subarea}>{`${v_subarea.nom_subarea}`}</option>
                                        )
                                    }
                                </select>
                            </div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label"> Tipo de Equipo : </div>
                            <div className="col-sm-6 col-form-label">
                                <select name="id_tpequipo" onChange={this.handleChange}>
                                    <option value='0'>Seleccionar Tipo Equipo</option>
                                    {
                                        listTiposEquipo.map((v_tipoequipo) =>
                                            this.state.id_tpequipo === v_tipoequipo.id_tpequipo ?
                                                <option key={v_tipoequipo.id_tpequipo} selected value={v_tipoequipo.id_tpequipo}>{`${v_tipoequipo.tipo_equipo}`}</option>
                                                :
                                                <option key={v_tipoequipo.id_tpequipo} value={v_tipoequipo.id_tpequipo}>{`${v_tipoequipo.tipo_equipo}`}</option>
                                        )
                                    }
                                </select>
                            </div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label"> Codigo : </div>
                            <div className="col-sm-3 col-form-label">
                                <input type="text" className="form-control form-control-sm" name="cod_equipo" onChange={this.handleChange} value={this.state.cod_equipo} />
                            </div>
                        </div>

                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label"> Nombre : </div>
                            <div className="col-sm-6 col-form-label">
                                <input type="text" className="form-control form-control-ss" name="equipo" onChange={this.handleChange} value={this.state.equipo} /></div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Factor Desviacion : </div>
                            <div className="col-sm-3 col-form-label">
                                <input type="number" className="form-control form-control-sm" name="factor_desviacion" min={0} max={9999} onChange={this.handleChange} value={this.state.factor_desviacion} />
                            </div>                            
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Oferta horas por día : </div>
                            <div className="col-sm-3 col-form-label">
                                <input type="number" className="form-control form-control-sm" name="oferta_horas_x_dia" min={0} max={9999} onChange={this.handleChange} value={this.state.oferta_horas_x_dia} />
                            </div>                            
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Operativo : </div>
                            <div className="col-sm-3 col-form-label">
                                <select aria-label=".form-select-lg example" defaultValue={this.state.sw_operativo} name="sw_operativo" onChange={this.handleChange}>
                                    <option value={false}>No</option>
                                    <option value={true}>Si</option>
                                </select>                                
                            </div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Disponibilidad Historico (%) : </div>
                            <div className="col-sm-3 col-form-label">
                                <input type="number" className="form-control form-control-sm" name="disponibilidad_historico" min={0.00} max={9999.99} onChange={this.handleChange} value={this.state.disponibilidad_historico} />
                            </div>                                
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Total Turnos : </div>
                            <div className="col-sm-3 col-form-label">
                                <input type="number" className="form-control form-control-sm" name="total_turnos" min={1} max={5} onChange={this.handleChange} value={this.state.total_turnos} /></div>                            
                        </div>                        
                    </div>
                </div>
        
            <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({    "id_equipo": this.state.id_equipo, "cod_subarea": parseInt(this.state.cod_subarea), "id_tpequipo": parseInt(this.state.id_tpequipo), 
                                                                                    "cod_equipo": this.state.cod_equipo, "equipo": this.state.equipo, "factor_desviacion": parseFloat(this.state.factor_desviacion), 
                                                                                    "oferta_horas_x_dia": parseFloat(this.state.oferta_horas_x_dia), "sw_operativo": this.state.sw_operativo, 
                                                                                    "disponibilidad_historico": parseFloat(this.state.disponibilidad_historico), "total_turnos": parseInt(this.state.total_turnos) })} texto={nameOpe} />
            </div >
        )
    }

}

export default EquipoEdicion;