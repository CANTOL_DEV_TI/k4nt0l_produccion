import React from "react";
import {GrEdit} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({id_equipo,
                        cod_subarea,                        
                        id_tpequipo,
                        cod_equipo,
                        equipo,
                        factor_desviacion,
                        oferta_horas_x_dia,
                        sw_operativo,
                        disponibilidad_historico,
                        total_turnos,
                        eventoEditar,
                        eventoEliminar}) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena" id={id_equipo}>{cod_equipo}</td>                
                <td className="td-cadena" >{equipo}</td>
                <td className="td-cadena" >{factor_desviacion}</td>
                <td className="td-cadena" >{total_turnos}</td>
                <td><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                
            </tr>
        </tbody>
    )

export default ResultadoTabla;