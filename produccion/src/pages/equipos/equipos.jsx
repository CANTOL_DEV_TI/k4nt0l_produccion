import React from "react";

import { getFilter_Equipo, save_Equipo, update_Equipo, delete_Equipo } from "../../services/equipoServices.jsx";

import Busqueda from "../equipos/components/Busqueda";
import ResultadoTabla from "../equipos/components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal";
import { ListaTipoEquipo } from "../../services/tipoEquipoServices.jsx";
import {get_subarea_operacion} from "../../services/subAreaServices.jsx";
import EquipoEdicion from "../equipos/components/EquipoEdicion.jsx";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

class Equipos extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            listTipoEquipo : null,
            listSubArea : null
        }
    }

    handleBusqueda = async (txtFind) => {
        const responseJson = await getFilter_Equipo(txtFind)
        console.log(responseJson)
        this.setState({ resultados: responseJson, isFetch: false })
        console.log(this.state)
    }

    handleSubmit = async (e) => {
        console.log(e)


        if (this.state.tipoOpe === 'Grabar') {
            const responseJson = await save_Equipo(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar') {
            const responseJson = await update_Equipo(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar') {
            const responseJson = await delete_Equipo(e)
            console.log(responseJson)
        }


        this.setState({ showModal: false, tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad } = this.state

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} style={{position:"sticky", top:0}}/>


                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>                                
                                <th className="align-middle">Codigo</th>
                                <th className="align-middle">Nombre</th>
                                <th className="align-middle">Factor Desviación</th>
                                <th className="align-middle">Total Turnos</th>
                                <th className="align-middle">Editar</th>
                                <th className="align-middle">Eliminar</th>
                            </tr>
                        </thead>
                        
                        {resultados.map((registro) =>
                            <ResultadoTabla
                                key={registro.id_equipo}
                                cod_subarea={registro.cod_subarea}
                                id_tpequipo={registro.id_tpequipo}
                                cod_equipo={registro.cod_equipo}
                                equipo={registro.equipo}
                                oferta_horas_x_dia={registro.oferta_horas_x_dia}
                                sw_operativo={registro.sw_operativo}
                                factor_desviacion={registro.factor_desviacion}
                                disponibilidad_historico={registro.disponibilidad_historico}
                                total_turnos={registro.total_turnos}
                                eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}
                            />
                        )}
                    </table>
                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({ showModal: false })}
                    titulo="Edición Equipos">
                    <EquipoEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={this.state.tipoOpe}
                        listTiposEquipo={this.state.listTipoEquipo}
                        listSubAreas = {this.state.listSubArea}
                    />
                </VentanaModal>
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}

                />

            </React.Fragment>
        )

    }

    async componentDidMount() {

        const ValSeguridad = await Validar("PROEQU")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
        console.log(ValSeguridad)        

        if(!this.state.VentanaSeguridad)
            {
            const responseJson = await ListaTipoEquipo()
            this.setState({ listTipoEquipo: responseJson })

            const responseJson2 = await get_subarea_operacion(1)
            console.log(responseJson2)
            this.setState({listSubArea : responseJson2})
        }
    }

}

export default Equipos;
