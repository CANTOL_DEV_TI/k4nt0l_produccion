import React, { useState, useEffect } from 'react'
import { get_equipo_buscar } from '../../services/equipoServices';
import { InputTextBuscar } from "../../components/InputTextBuscar";
import { EquipoBuscar_tabla } from "./components/EquipoBuscar_tabla";

export function EquipoBuscar({ eventoClick }) {

    // CONSUMIR DATOS
    const [sw_modo, setSw_modo] = useState("C");
    const [listDatos, setListDatos] = useState([]);
    useEffect(() => {
        getData();
    }, [])

    // Cargar datos a listado
    const getData = () => {
        get_equipo_buscar()
            .then(res => {
                setListDatos(res)
                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_equipo", "equipo"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // Seleccionar datos de tabla
    const eventoSeleccionar = (datos) => {
        eventoClick(datos);
    };

    // PAGINACIÓN 
    let LimitPag = 10;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">
            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-md-6">
                            <h5>Lista Equipos</h5>
                        </div>
                        <div className="col-sm-6">
                            <div className="d-flex gap-1">
                                <InputTextBuscar onBuscarChange={onSearchChange} />
                            </div>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <EquipoBuscar_tabla datos_fila={search(listDatos)} eventoClick={eventoSeleccionar} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-end m-2">
                    <div className="pagination-wrap hstack gap-1">
                        Total Reg. {listDatos.length}
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                        <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>
        </div>
    );
}