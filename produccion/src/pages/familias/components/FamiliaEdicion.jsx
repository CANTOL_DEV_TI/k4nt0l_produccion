import React from "react";
import { BotonGuardar } from "../../../components/Botones";

class FamiliaEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { id_familia: 0, nom_familia: '' }
        } else {
            this.state = { id_familia: this.props.dataToEdit.id_familia, nom_familia: this.props.dataToEdit.nom_familia }
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'codigo') {
            this.setState({ id_familia: e.target.value })
        }

        if (e.target.name === 'nombre') {
            this.setState({ nom_familia: e.target.value })
        }
    }


    render() {
        const { eventOnclick, nameOpe } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-4 col-form-label">
                                <input type="text" name="codigo"  className="form-control form-control-sm" onChange={this.handleChange} value={this.state.id_familia} />
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Nombre : </div>
                            <div className="col-sm-6 col-form-label"><input type="text"  className="form-control form-control-sm"  name="nombre" autoFocus onChange={this.handleChange} value={this.state.nom_familia} /></div>
                        </div>
                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "id_familia": this.state.id_familia, "nom_familia": this.state.nom_familia })} texto={nameOpe} />
            </div>
        )
    }
}

export default FamiliaEdicion;
