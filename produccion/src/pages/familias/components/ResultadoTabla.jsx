import React from "react";
import {GrEdit} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({id_familia,
                        nom_familia,
                        sw_estado,
                        eventoEditar,
                        eventoEliminar}) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena" id={id_familia}>{nom_familia}</td><td><span className={"label label-"  + sw_estado}>{sw_estado=='1'?'Activo':sw_estado=='0'?' InActivo ':' Eliminado '}</span></td>
                <td><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                
            </tr>
        </tbody>
    )

export default ResultadoTabla;