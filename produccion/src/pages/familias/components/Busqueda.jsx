import React, { useState } from "react";
import Title from "../../../components/Titulo";
import { BotonExcel, BotonNuevo, BotonBuscar } from "../../../components/Botones";

class Busqueda extends React.Component {

    constructor(props) {
        super(props);

        this.state = { txtInput: '' }
    }

    handleChange = (e) => {
        this.setState({ txtInput: e.target.value })
    }


    render() {

        const { handleBusqueda, handleModal } = this.props

        return (
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Lista de Familias</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    <input
                                        placeholder="Ingrese Busqueda"
                                        onChange={this.handleChange}
                                        value={this.state.txtInput}
                                    />

                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda(this.state.txtInput)} />
                                                                                                        
                                    <BotonNuevo textoBoton={"Crear Nuevo"} sw_habilitado={true} eventoClick={() => handleModal(true)} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Busqueda;