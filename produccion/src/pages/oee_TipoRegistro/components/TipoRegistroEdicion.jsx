import React from "react";
import { BotonGuardar, BotonConsultar } from "../../../components/Botones";

class TipoRegistroEdicion extends React.Component{

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null){
            this.state = {
                cod_tipo_registro: 0,
                tipo_registro: '',
                umu: '',
                sw_estado: '0',
                label_name: '',
                style_Principal: '',
                style_secundario: '',
                sw_default: '0'
            }

        }else {
            this.state = {
                cod_tipo_registro: this.props.dataToEdit.cod_tipo_registro,
                tipo_registro: this.props.dataToEdit.tipo_registro,

                umu: this.props.dataToEdit.umu,
                sw_estado: this.props.dataToEdit.sw_estado,
                label_name: this.props.dataToEdit.label_name,
                style_Principal: '',
                style_secundario: '',
                sw_default: this.props.dataToEdit.sw_default

            }
        }
    }

    handleChange = (e) =>{
        if (e.target.name === 'cod_tipo_registro'){
            this.setState({cod_tipo_registro: e.target.value})
        }

        if (e.target.name === 'tipo_registro'){
            this.setState({tipo_registro: e.target.value})
        }

        if (e.target.name === 'umu'){
            this.setState({umu: e.target.value})
        }

        if (e.target.name === 'sw_estado'){
            this.setState({sw_estado: e.target.checked === true ? "1" : "0"})
        }

        if (e.target.name === 'label_name'){
            this.setState({label_name: e.target.value})
        }

        if (e.target.name === 'sw_default'){
            this.setState({sw_default: e.target.checked === true ? "1" : "0"})
        }





    }


    render() {
        const {eventOnclick, nameOpe} = this.props

        return(
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Codigo :</div>
                            <div className="col-sm-4 col-form-label"><input type="text"
                                                                            className="form-control form-control-sm"
                                                                            name="cod_tipo_registro"
                                                                            onChange={this.handleChange}
                                                                            value={this.state.cod_tipo_registro}/></div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Tipo Registro :</div>
                            <div className="col-sm-8 col-form-label"><input type="text"
                                                                            className="form-control form-control-sm"
                                                                            name="tipo_registro"
                                                                            onChange={this.handleChange}
                                                                            value={this.state.tipo_registro}/></div>
                        </div>

                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Und. Medida :</div>
                            <div className="col-sm-8 col-form-label"><input type="text"
                                                                            className="form-control form-control-sm"
                                                                            name="umu"
                                                                            onChange={this.handleChange}
                                                                            value={this.state.umu}/></div>
                        </div>

                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Nombre Boton :</div>
                            <div className="col-sm-8 col-form-label"><input type="text"
                                                                            className="form-control form-control-sm"
                                                                            name="label_name"
                                                                            onChange={this.handleChange}
                                                                            value={this.state.label_name}/></div>
                        </div>

                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Habilitado :</div>
                            <div className="col-sm-8 col-form-label"><input type="checkbox" className="form-check-input"
                                                                            name="sw_estado"
                                                                            onChange={this.handleChange}
                                                                            checked={parseInt(this.state.sw_estado)}/></div>
                        </div>

                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Visible por defecto:</div>
                            <div className="col-sm-8 col-form-label"><input type="checkbox" className="form-check-input" name="sw_default" onChange={this.handleChange} checked={parseInt(this.state.sw_default)}/></div>
                        </div>

                    </div>
                </div>

                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick(this.state)} texto={nameOpe}/>
            </div>
        )
    }
}

export default TipoRegistroEdicion;
