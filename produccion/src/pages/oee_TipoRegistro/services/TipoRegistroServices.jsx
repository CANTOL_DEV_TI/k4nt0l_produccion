import { Url } from '../../../constants/global.jsx'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

const url = `${meServidorBackend}/produccion/oee_tipo_registro`;

export async function getFilter_TipoRegistro(txtFind) {
    if (txtFind.trim()===''){
        txtFind='%20'
    }
    const meUrl = `${url}/mostrar/${txtFind}`
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    console.log(meUrl)
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    const response = await fetch(meUrl)
    const responseJson = await response.json()
    return responseJson
}

export async function save_TipoRegistro(meJson){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${url}/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function update_TipoRegistro(meJson){
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${url}/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function delete_TipoRegistro(meJson){
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${url}/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

