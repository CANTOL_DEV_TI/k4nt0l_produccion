import React from "react";

import Busqueda from "./components/Busqueda";
import ResultadoTabla from "./components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal.jsx";
import TipoRegistroEdicion from "./components/TipoRegistroEdicion";

import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

// import {getFilter_TipoRegistro} from "./services/TipoRegistroServices";
import {getFilter_TipoRegistro, save_TipoRegistro, update_TipoRegistro, delete_TipoRegistro} from "./services/TipoRegistroServices.jsx";



class TipoRegistro extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find'
        }
    }

    handleBusqueda = async(txtFind) => {
        const responseJson = await getFilter_TipoRegistro(txtFind)
        console.log(responseJson)
        this.setState({resultados: responseJson, isFetch: false})
        console.log(this.state)
    }


    handleSubmit = async (e) =>{
        console.log(e)

        if (this.state.tipoOpe === 'Grabar'){
            const responseJson = await save_TipoRegistro(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar'){
            const responseJson = await update_TipoRegistro(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar'){
            const responseJson = await delete_TipoRegistro(e)
            console.log(responseJson)
        }


        this.setState({showModal: false, tipoOpe: 'Find'})
    }


    render() {

        const {isFetch, resultados, showModal, VentanaSeguridad} = this.state

        return(
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({showModal: true, dataRegistro:null, tipoOpe: 'Grabar'})}/>


                { isFetch && 'Cargando'}
                { (!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>
                                <th className="meCabFin">Tipo Registro</th>
                                <th className="meCab2 meCabCentro">Edicion</th>
                                <th className="meCabIni">Eliminacion</th>
                            </tr>
                        </thead>
                        {resultados.map((registro)=>
                            <ResultadoTabla
                                key={registro.cod_tipo_registro}
                                tipo_registro={registro.tipo_registro}
                                eventoEditar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar'})}
                                eventoEliminar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar'})}
                            />
                        )}
                    </table>
                </div>


                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({showModal: false})}
                    titulo="Edicion Tipo Registro">
                        <TipoRegistroEdicion
                            dataToEdit={this.state.dataRegistro}
                            eventOnclick={this.handleSubmit}
                            nameOpe={this.state.tipoOpe}
                        />
                </VentanaModal>
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}
                />


            </React.Fragment>
        )
    }


}



export default TipoRegistro;
