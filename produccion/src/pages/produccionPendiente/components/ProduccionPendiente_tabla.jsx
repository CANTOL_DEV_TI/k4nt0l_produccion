import React, { useState, useEffect } from 'react'
import { BsInfoCircle } from "react-icons/bs";

export function ProduccionPendiente_tabla({ datos_fila, eventoClick }) {

    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle">Cod.Artículo</th>
                            <th className="align-middle">Artículo</th>
                            <th className="align-middle">Cantidad</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td className="td-cadena">{datos.cod_articulo}</td>
                                        <td className="td-cadena">{datos.articulo}</td>
                                        <td className="td-num">{datos.cantidad.toLocaleString("en")}</td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="3">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}