import React, { useState, useEffect } from 'react'
import { BsFileExcel } from "react-icons/bs";
import {
    get_produccion_pendiente_all,
    insert_produccion_pendiente,
} from '../../services/produccionPendienteServices';
import { BotonExcel, BotonNuevo } from "../../components/Botones";
import { InputTextBuscar } from "../../components/InputTextBuscar";
import { ProduccionPendiente_tabla } from "./components/ProduccionPendiente_tabla";
import Title from "../../components/Titulo";
import * as XLSX from 'xlsx';
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

export function ProduccionPendiente() {

    const Seguridad = async() =>{        
        const ValSeguridad = await Validar("PROPRP")
		setVentanaSeguridad(ValSeguridad.Validar)
    }
    // VARIABLES
    const format_excel = [{
        cod_articulo: "PT0101030007",
        cantidad: "0",
    }];

    const [refrescar, setRefrescar] = useState(false);
    const [VentanaSeguridad, setVentanaSeguridad] = useState(true);

    // CONSUMIR DATOS
    const [totalPlan, setTotalPlan] = useState(0);
    const [listDatos, setListDatos] = useState([])
    useEffect(() => {
        Seguridad();
        setRefrescar(false);
        getData();
    }, [refrescar])

    // CARGAR DATOS
    const getData = () => {
        get_produccion_pendiente_all()
            .then(res => {
                setListDatos(res)

                // obtener total
                setTotalPlan(res.reduce((total, currItem) => total + currItem.cantidad, 0))

                // primera pagina
                setCurrentPage(0);
            })
    };

    // importar excel desde archivo
    const [typeError, setTypeError] = useState(null);
    const handleFile = (e) => {
        let fileTypes = ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'text/csv'];
        let selectedFile = e.target.files[0];
        if (selectedFile) {
            if (selectedFile && fileTypes.includes(selectedFile.type)) {
                setTypeError(null);
                let reader = new FileReader();
                reader.readAsArrayBuffer(selectedFile);
                reader.onload = (e) => {
                    // leer archivo
                    const workbook = XLSX.read(e.target.result, { type: 'buffer' });
                    const worksheetName = workbook.SheetNames[0];
                    const worksheet = workbook.Sheets[worksheetName];
                    const data = XLSX.utils.sheet_to_json(worksheet);
                    createData(data);
                }
            }
            else {
                setTypeError('Seleccione solo tipos de archivos de Excel.');
            }
        }
        else {
            setTypeError('Por favor seleccione su archivo.');
        }
    }

    // BUSQUEDA EN DOM
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_articulo", "articulo"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // INSERTAR DATOS
    const createData = (data) => {
        const new_data = {
            "cod_articulo": "TODO",
            "detalle": data
        };
        insert_produccion_pendiente(new_data);
        setRefrescar(true);
    };

    // PAGINACIÓN 
    let LimitPag = 15;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">
            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-sm">
                            <Title>Producción Pendiente</Title>
                        </div>
                        <div className="col-sm">
                            <div className="d-flex justify-content-end gap-1">
                                <InputTextBuscar onBuscarChange={onSearchChange} />
                                <BotonExcel textoBoton={"Exportar Excel"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatos} nombreArchivo={"Plan venta"} />
                                <label htmlFor="file_excel" className="btn btn-success btn-sm" >
                                    <i className="align-bottom me-1"><BsFileExcel /></i> Cargar Excel</label>
                                <input type="file" id="file_excel" name="file_excel" accept=".xlsx, .xls, .csv" style={{ visibility: 'hidden', maxWidth: '1px' }} onChange={handleFile} />
                                <BotonExcel textoBoton={"Formato"} sw_habilitado={format_excel.length > 0 ? true : false} listDatos={format_excel} nombreArchivo={"Formato_carga"} />
                            </div>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <ProduccionPendiente_tabla datos_fila={search(listDatos)} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-end mb-3">
                    <div className="me-auto p-2 bd-highlight">
                        Suma Total: <span className="fw-bold">{totalPlan.toLocaleString("en")}</span> Unidades.
                    </div>
                    <div className="pagination-wrap hstack gap-1">
                        Total Reg. {listDatos.length}
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                        <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>

            {/* INICIO MODAL */}

            {/* FIN MODAL */}
            <VentanaBloqueo
                show={VentanaSeguridad}

            />
        </div>
    );
}