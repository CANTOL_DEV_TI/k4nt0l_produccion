import React from "react";
import { BotonGuardar, BotonConsultar } from "../../../components/Botones";
import {getFilter_Causa} from "../services/CausaServicios.jsx";
import {getFilter_TipoRegistro} from "../../oee_TipoRegistro/services/TipoRegistroServices.jsx";

class CausaEdicion extends React.Component{

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null){
            this.state = {
                cod_causa: 0,
                cod_tipo_registro: '0',
                causa: '',
                sw_estado: '0',
                tiempo_default: '0'


            }

        }else {
            this.state = {
                cod_causa: this.props.dataToEdit.cod_causa,
                cod_tipo_registro: this.props.dataToEdit.cod_tipo_registro,
                causa: this.props.dataToEdit.causa,
                sw_estado: this.props.dataToEdit.sw_estado,
                tiempo_default: this.props.dataToEdit.tiempo_default

            }
        }
    }

    handleChange = (e) =>{
        if (e.target.name === 'cod_causa'){
            this.setState({cod_causa: e.target.value})
        }

        if (e.target.name === 'cod_tipo_registro'){
            this.setState({cod_tipo_registro: e.target.value})
        }

        if (e.target.name === 'causa'){
            this.setState({causa: e.target.value})
        }

        if (e.target.name === 'sw_estado'){
            this.setState({sw_estado: e.target.checked === true ? "1" : "0"})
        }

        if (e.target.name === 'tiempo_default'){
            this.setState({tiempo_default: e.target.value})
        }

    }


    render() {
        const {eventOnclick, nameOpe, listTipoRegistro} = this.props

        return(
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Codigo :</div>
                            <div className="col-sm-4 col-form-label"><input type="text"
                                                                            className="form-control form-control-sm"
                                                                            name="cod_causa"
                                                                            onChange={this.handleChange}
                                                                            value={this.state.cod_causa}/></div>
                        </div>
                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Tipo Registro :</div>
                            <div className="col-sm-8 col-form-label">
                                <select name="selectArea" className="form-control form-control-sm" onChange={this.handleChange} disabled={true}>
                                    <option value='0'>Seleccionar Tipo Registro</option>
                                    {
                                        listTipoRegistro.map((v_item) =>
                                            this.state.cod_tipo_registro === v_item.cod_tipo_registro ?
                                                <option selected value={v_item.cod_tipo_registro}>{`${v_item.tipo_registro}`}</option>
                                                :
                                                <option value={v_item.cod_tipo_registro}>{`${v_item.tipo_registro}`}</option>
                                        )
                                    }
                                </select>
                            </div>
                        </div>

                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Causa :</div>
                            <div className="col-sm-8 col-form-label"><input type="text"
                                                                            className="form-control form-control-sm"
                                                                            name="umu"
                                                                            onChange={this.handleChange}
                                                                            value={this.state.causa}/></div>
                        </div>


                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Habilitado :</div>
                            <div className="col-sm-8 col-form-label"><input type="checkbox" className="form-check-input"
                                                                            name="sw_estado"
                                                                            onChange={this.handleChange}
                                                                            checked={parseInt(this.state.sw_estado)}/></div>
                        </div>

                        <div className="form-group row form-control-sm">
                            <div className="col-sm-4 col-form-label">Tiempo default:</div>
                            <div className="col-sm-8 col-form-label"><input type="text" className="form-control form-control-sm" name="tiempo_default" onChange={this.handleChange} value={this.state.tiempo_default}/></div>
                        </div>

                    </div>
                </div>

                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick(this.state)} texto={nameOpe}/>
            </div>
        )
    }



}

export default CausaEdicion;
