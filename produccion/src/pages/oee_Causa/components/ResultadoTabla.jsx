import React from "react";
import {GrEdit} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";

const ResultadoTabla = ({key,
                        causa,
                        eventoMotivos,
                        eventoEditar,
                        eventoEliminar}) =>
    (
        <tbody>
        <tr className="data">
            <td className="colString" id={key}>{causa}</td>
            <td className="colBtn"><button onClick={() => eventoMotivos(true)}><GrEdit color="#011826"/></button></td>
            <td className="colBtn"><button onClick={() => eventoEditar(true)}><GrEdit color="#011826"/></button></td>
            <td className="colBtn"><button onClick={() => eventoEliminar(true)}><AiFillDelete color="#b04129"/></button></td>
        </tr>
        </tbody>
    )


export default ResultadoTabla;


