import React from "react";

import Busqueda from "./components/Busqueda";
import ResultadoTabla from "./components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal.jsx";
import CausaEdicion from "./components/CausaEdicion.jsx";

import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

import {getFilter_Causa, save_Causa, update_Causa, delete_Causa} from "./services/CausaServicios.jsx"
import TipoRegistroEdicion from "../oee_TipoRegistro/components/TipoRegistroEdicion.jsx";
import {getFilter_TipoRegistro} from "../oee_TipoRegistro/services/TipoRegistroServices.jsx";
import MotivoCausa_Main from "../oee_MotivoCausa/MotivoCausa_Main.jsx";



class Causa extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',

            TMP_CodCausa: 0,
            TMP_NomCausa: '',


        }

    }


    handleBusqueda = async(txtFind) => {
        const responseJson = await getFilter_Causa(txtFind)
        console.log(responseJson)
        this.setState({resultados: responseJson, isFetch: false})
        console.log(this.state)
    }


    handleSubmit = async (e) =>{
        console.log("888888888888888888888888")
        console.log(e)
        console.log("888888888888888888888888")

        if (this.state.tipoOpe === 'Grabar'){
            const responseJson = await save_Causa(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar'){
            const responseJson = await update_Causa(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar'){
            const responseJson = await delete_Causa(e)
            console.log(responseJson)
        }


        this.setState({showModal: false, tipoOpe: 'Find'})
    }




    render() {

        const {isFetch, resultados, showModal, VentanaSeguridad} = this.state

        return(
            <React.Fragment>


                {this.state.tipoOpe === 'Motivos' ?
                    <MotivoCausa_Main TMP_CodCausa={this.state.TMP_CodCausa} TMP_NomCausa={this.state.TMP_NomCausa} eventoClose={()=> this.setState({tipoOpe: 'Find',})}/>
                :
                <>
                    <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({showModal: true, dataRegistro:null, tipoOpe: 'Grabar'})}/>


                    { isFetch && 'Cargando'}
                    { (!isFetch && !resultados.length) && 'Sin Informacion'}


                    <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                        <table className="table table-hover table-sm table-bordered">
                            <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>
                                <th className="meCabFin">Causa</th>
                                <th className="meCab2 meCabCentro">Motivos</th>
                                <th className="meCab2 meCabCentro">Edicion</th>
                                <th className="meCabIni">Eliminacion</th>
                            </tr>
                            </thead>
                            {resultados.map((registro)=>
                                <ResultadoTabla
                                    key={registro.cod_causa}
                                    causa={registro.causa}
                                    eventoMotivos={() => this.setState({showModal: false, dataRegistro: registro, tipoOpe: 'Motivos', TMP_CodCausa: registro.cod_causa, TMP_NomCausa: registro.causa})}
                                    eventoEditar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar'})}
                                    eventoEliminar={() => this.setState({showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar'})}
                                />
                            )}
                        </table>
                    </div>


                    <VentanaModal
                        show={showModal}
                        size="lg"
                        handleClose={() => this.setState({showModal: false})}
                        titulo="Edicion Causa">
                            <CausaEdicion
                                dataToEdit={this.state.dataRegistro}
                                eventOnclick={this.handleSubmit}
                                nameOpe={this.state.tipoOpe}
                                listTipoRegistro={this.state.listTipoRegistro}
                            />
                    </VentanaModal>
                    <VentanaBloqueo
                        show={this.state.VentanaSeguridad}
                    />


                </>
                }


            </React.Fragment>
        )
    }

    async componentDidMount() {
        const responseJson = await getFilter_TipoRegistro("")
        console.log("/////////////////////////////////")
        console.log(responseJson)
        this.setState({listTipoRegistro: responseJson})
        console.log(this.state)
    }


}


export default Causa;

