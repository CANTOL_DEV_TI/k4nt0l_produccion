import { Link } from 'react-router-dom'
import { BsPencilFill, BsXCircleFill } from "react-icons/bs";

export function ArticuloOmitido_tabla({ datos_fila, deleteData, eventoClick }) {
    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle">ID</th>
                            <th className="align-middle">Fecha Documento</th>
                            <th className="align-middle">Observación</th>
                            <th className="align-middle" style={{ width: '100px' }}>Estado</th>
                            <th className="align-middle" title="Opciones" style={{ width: '100px' }}>Opc</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td className="td-cadena">{datos.id_plan_omitido}</td>
                                        <td className="td-cadena">{datos.fecha_emision}</td>
                                        <td className="td-cadena">{datos.glosa}</td>
                                        <td className="td-cadena">
                                            <span className={`text-uppercase${datos.sw_estado === '1' ? ' badge bg-success' : ' badge bg-danger'}`}>{`${datos.sw_estado === '1' ? 'Habilitado' : 'Deshabilitado'}`}</span>
                                        </td>
                                        <td>
                                            <button data-toggle="tooltip" title="Modificar" className="btn btn-outline-primary btn-sm ms-2"
                                                onClick={() => eventoClick(datos)}>
                                                <BsPencilFill />
                                            </button>
                                            <button data-toggle="tooltip" title="Eliminar" className="btn btn-outline-danger btn-sm ms-2"
                                                onClick={() => deleteData(datos.id_plan_omitido)}>
                                                <BsXCircleFill />
                                            </button>
                                        </td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="5">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}