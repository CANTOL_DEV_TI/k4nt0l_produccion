import React, { useState, useEffect } from "react";
import { BotonNuevo, BotonAtras, BotonGrabar } from "../../../components/Botones";
import { useVentanaModal } from "../../../hooks/useVentanaModal";
import { ArticuloOmitido_crud_tabla } from "./ArticuloOmitido_crud_tabla";
import {
    insert_articuloOmitido,
    update_articuloOmitido,
    get_articuloOmitido_det,
} from '../../../services/articuloOmitidoServices';
import { ArticuloFamiliaBuscar } from "../../articulofamilia/ArticuloFamiliaBuscar";
import VentanaModal from "../../../components/VentanaModal";
import Title from "../../../components/Titulo";

const iniFormCab = {
    id_plan_omitido: "0",
    fecha_emision: "",
    glosa: "",
    sw_estado: "1",
    cod_usuario: "1",
};

export function ArticuloOmitido_crud({ modoVista, formData }) {
    // C: CREAR , M: MODIFICAR
    const [sw_modo, setSw_modo] = useState("C");

    // CONSUMIR DATOS
    const [formCab, setFormCab] = useState(iniFormCab);
    const [formDet, setFormDet] = useState([]);
    const [checked, setChecked] = useState(false);
    useEffect(() => {
        if (Object.keys(formData).length > 0) {
            if (formData.sw_estado === "1") {
                setChecked(true);
            } else {
                setChecked(false);
            }
            setFormCab(formData);
            getDataDet();
            setSw_modo("M")
        } else {
            setChecked(true);
        }
    }, []);

    // Datos detalle
    const getDataDet = () => {
        get_articuloOmitido_det(formData.id_plan_omitido)
            .then(res => {
                setFormDet(res)
                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM
    const [buscarDato, setBuscarDato] = useState("");
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);
    };

    // CAPTURAR EL EVENTO DE LOS IMPUTS
    const handleChange = (evento) => {
        if (evento.target.name === 'sw_estado') {
            setChecked(!checked);
            setFormCab({
                ...formCab,
                [evento.target.name]: evento.target.checked === true ? "1" : "0",
            });
        } else {
            setFormCab({
                ...formCab,
                [evento.target.name]: (evento.target.value).toUpperCase(),
            });
        }
    };
    
    // Seleccionar datos de tabla
    const eventoSeleccionar = (datos) => {
        if (Object.keys(datos).length) {
            // buscar si existe en lista
            const c_res = formDet.filter(item => {
                return item.cod_articulo.toLowerCase().includes(datos.cod_articulo.toLowerCase())
            })

            if (c_res.length === 0) {
                const c_data = {
                    "cod_articulo": datos.cod_articulo,
                    "nom_articulo": datos.articulo,
                    "cod_grupo": datos.cod_articulo.substr(0, 2)
                }
                const newList = formDet.concat(c_data);
                setFormDet(newList);
            }
        }
    };

    // Eliminar items
    const deleteItem = (cod_articulo) => {
        const c_res = formDet.filter((item) => item.cod_articulo !== cod_articulo);
        setFormDet(c_res);
    };

    // GUARDAR INFORMACION
    const handleSubmit = (e) => {
        if (!formCab.fecha_emision) {
            alert("Seleccionar fecha. !");
            return;
        }
        if (formDet.length === 0) {
            alert("Docmuento no tiene Detalle");
            return;
        }

        // generar datos
        const new_data = {
            "id_plan_omitido": formCab.id_plan_omitido,
            "fecha_emision": Date.parse(formCab.fecha_emision),
            "glosa": formCab.glosa,
            "sw_estado": formCab.sw_estado,
            "cod_usuario": "1",
            "detalle": formDet
        };

        // realizar operación segun condición
        if (sw_modo === "C") { // CREAR
            const res = insert_articuloOmitido(new_data);
            handleAtras();
        } else { // MODIFICAR
            const res = update_articuloOmitido(new_data)
            handleAtras();
        }
    };

    // Ir a consulta principal
    const handleAtras = (e) => {
        //navigate('/articulo_omitido');
        modoVista("L") // lista
    };

    // PAGINACIÓN 
    let LimitPag = 10;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (formDet.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    // MOSTRAR MODAL
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = () => {
        handleShow(true)
    };

    // RETORNAR INFORMACIÓN
    return (
        <>
            <div className="row justify-content-center">
                <div className="col-lg-8">
                    <div className="card">
                        <div className="card-header">
                            <div className="row g-2">                                
                                <div className="col-md-8">
                                    <Title>Lista de Articulos Omitidos</Title>
                                </div>
                                <div className="col-md-2">
                                    <BotonAtras textoBoton={"Regresar"} eventoClick={handleAtras} />
                                </div>
                                <div className="col-md-2">
                                    <BotonGrabar textoBoton={`${sw_modo === "C" ? 'Crear' : 'Actualizar'}`} eventoClick={handleSubmit} />
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="row g-2">
                                {/* INICIO  FORM*/}
                                <div className="col-md-1">
                                    <label className="form-label">Id</label>
                                    <input type="text" readOnly className="form-control form-control-sm" name="id_plan_omitido" value={formCab.id_plan_omitido || '0'} />
                                </div>
                                <div className="col-md-2">
                                    <label className="form-label">Fecha</label>
                                    <input type="date" min="2023-01-01" className="form-control form-control-sm" name="fecha_emision" onChange={handleChange} value={formCab.fecha_emision || ''} />
                                </div>
                                <div className="col-md-7">
                                    <label className="form-label">Observación</label>
                                    <input type="text" className="form-control form-control-sm" name="glosa"
                                        onChange={handleChange} value={formCab.glosa} />
                                </div>
                                <div className="col-md-2">
                                    <label className="form-label">Opciones&emsp;&emsp;</label>
                                    <BotonNuevo textoBoton={"Añadir"} sw_habilitado={true} eventoClick={mostrarVentana} />
                                </div>
                                <div className="col-12">
                                    <ArticuloOmitido_crud_tabla formDet={search(formDet)} eventoClick={deleteItem} />
                                </div>
                                <div className="col-md-4">
                                    <input className="form-check-input" type="checkbox" name="sw_estado" checked={checked} onChange={handleChange} />
                                    <label className="form-check-label ms-2">
                                        Habilitado
                                    </label>
                                </div>
                                <div className="col-md-8">
                                    {/* INICIO PAGINACION */}
                                    <div className="d-flex justify-content-end">
                                        <div className="pagination-wrap hstack gap-1">
                                            Total Reg. {formDet.length}
                                            <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                                            <button className={formDet.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                                        </div>
                                    </div>
                                    {/* FIN PAGINACION */}
                                </div>
                                {/* FIN FORM */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="xsm"
                handleClose={handleClose}
                titulo={"Buscar Artículo"}
            >
                <ArticuloFamiliaBuscar eventoClick={eventoSeleccionar} />
            </VentanaModal>
            {/* FIN MODAL */}
        </>
    );
}