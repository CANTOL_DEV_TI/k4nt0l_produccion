import {BsXCircleFill } from "react-icons/bs";

export function ArticuloOmitido_crud_tabla({ formDet, eventoClick }) {


    // RETORNAR INFORMACIÓN
    return (
        <>
            <table className="table table-hover table-sm table-bordered">
                <thead className="table-secondary text-center table-sm">
                    <tr>
                        <th className="align-middle" style={{ maxWidth: "110px" }}>Cod. Artículo</th>
                        <th className="align-middle">Nombre Artículo</th>
                        <th className="align-middle">Grupo</th>
                        <th className="align-middle" style={{ width: "40px" }}>Opciones</th>
                    </tr>
                </thead>
                <tbody className="list">
                    {formDet.length > 0 ?
                        (
                            formDet.map((datos, index) => {
                                return (
                                    <tr key={index + 1}>
                                        <td>{datos.cod_articulo}</td>
                                        <td>{datos.nom_articulo}</td>
                                        <td>{datos.cod_grupo}</td>
                                        <td>
                                            <button data-toggle="tooltip" title="Eliminar" className="btn btn-outline-danger btn-sm"
                                                onClick={() => eventoClick(datos.cod_articulo)}>
                                                <BsXCircleFill />
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })
                        ) :
                        (
                            <tr>
                                <td className="text-center" colSpan="4">Sin datos</td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
        </>
    );
}