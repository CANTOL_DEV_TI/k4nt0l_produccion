import React, { useState, useEffect } from 'react'
import { get_articuloOmitido, delete_articuloOmitido } from '../../../services/articuloOmitidoServices';
import { BotonExcel, BotonNuevo } from "../../../components/Botones";
import { InputTextBuscar } from "../../../components/InputTextBuscar";
import { ArticuloOmitido_tabla } from "./ArticuloOmitido_tabla";
import Title from "../../../components/Titulo";
import { Validar } from '../../../services/ValidaSesion';
import VentanaBloqueo from '../../../components/VentanaBloqueo';

export function ArticuloOmitido({ modoVista, setFormData }) {

    // seguridad
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
    const Seguridad = async () => {
        const ValSeguridad = await Validar("PROARO")
        setVentanaSeguridad(ValSeguridad.Validar)
    }
    // CONSUMIR DATOS
    const [refrescar, setRefrescar] = useState(true);
    const [listDatos, setListDatos] = useState([]);
    useEffect(() => {
        Seguridad();
        setFormData([]);
        setRefrescar(false);
        getData();
    }, [refrescar])

    //
    const getData = () => {
        get_articuloOmitido()
            .then(res => {
                setListDatos(res)
                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["id_plan_omitido", "glosa", "sw_estado"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // Eliminar datos
    const deleteData = (id) => {
        setRefrescar(true);
        delete_articuloOmitido(id)
    };

    // PAGINACIÓN 
    let LimitPag = 15;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // Mostrar componente crud
    const mostrarVentana = (datos) => {
        modoVista("C");
        setFormData(datos);
    };

    // Ir a crud
    const handleCrud = (e) => {
        modoVista("C"); // crud
    };

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">
            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-sm">
                            <Title>Lista de Articulos Omitidos</Title>
                        </div>
                        <div className="col-sm">
                            <div className="d-flex justify-content-end gap-1">
                                <InputTextBuscar onBuscarChange={onSearchChange} />
                                <BotonExcel textoBoton={"Exportar Excel"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatos} nombreArchivo={"Presupesto mensual"} />
                                <BotonNuevo textoBoton={"Nuevo"} eventoClick={handleCrud} />
                            </div>
                        </div>
                    </div>
                </div>
                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <ArticuloOmitido_tabla datos_fila={search(listDatos)} deleteData={deleteData} eventoClick={mostrarVentana} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-end m-2">
                    <div className="pagination-wrap hstack gap-1">
                        Total Reg. {listDatos.length}
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                        <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>
            <VentanaBloqueo show={VentanaSeguridad} />
        </div>
    );
}