import React, { useState, useEffect } from 'react'
import { ArticuloOmitido } from "./components/ArticuloOmitido";
import { ArticuloOmitido_crud } from "./components/ArticuloOmitido_crud";

export function Articulo_omitido() {
    // L: LISTA , C: CRUD
    const [modoVista, setModoVista] = useState("L");
    const [formCab, setFormCab] = useState([]);

    // RETORNAR INFORMACIÓN
    if (modoVista === "L") {
        //setFormCab([]); // limpiar
        return (
            <ArticuloOmitido modoVista={setModoVista} setFormData={setFormCab} />
        );
    } else {
        return (
            <ArticuloOmitido_crud modoVista={setModoVista} formData={formCab} />
        );
    }

}