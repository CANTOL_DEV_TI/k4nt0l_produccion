import React from "react";

import { getFilter_TipoOperacion, save_TipoOperacion, update_TipoOperacion, delete_TipoOperacion } from "../../services/tipoOperacionServices.jsx";

import Busqueda from "../tipoOperacion/components/Busqueda";
import ResultadoTabla from "../tipoOperacion/components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal";
import TipoOperacionEdicion from "../tipoOperacion/components/TipoOperacionEdicion.jsx";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

class TipoOperacion extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find'
        }
    }

    handleBusqueda = async (txtFind) => {
        const responseJson = await getFilter_TipoOperacion(txtFind)
        console.log(responseJson)
        this.setState({ resultados: responseJson, isFetch: false })
        console.log(this.state)
    }

    handleSubmit = async (e) => {
        console.log(e)
            
        if (this.state.tipoOpe === 'Grabar') {
            const responseJson = await save_TipoOperacion(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar') {
            const responseJson = await update_TipoOperacion(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar') {
            const responseJson = await delete_TipoOperacion(e)
            console.log(responseJson)
        }


        this.setState({ showModal: false, tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad } = this.state

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} />


                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm"  style={{position:"sticky", top:0}}>
                            <tr>
                                <th className="align-middle">ID</th>
                                <th className="align-middle">Tipo Operacion</th>
                                <th className="align-middle">Editar</th>
                                <th className="align-middle">Eliminar</th>
                            </tr>
                        </thead>
                        
                        {resultados.map((registro) =>
                            <ResultadoTabla
                                key={registro.id_tpoperacion}
                                id_tpoperacion={registro.id_tpoperacion}
                                tipo_operacion={registro.tipo_operacion}                                
                                eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}
                            />
                        )}
                    </table>
                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({ showModal: false })}
                    titulo="Edición Tipo Operación">
                    <TipoOperacionEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={this.state.tipoOpe}
                    />
                </VentanaModal>
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}

                />
            </React.Fragment>
        )

    }
    async componentDidMount(){        
        const ValSeguridad = await Validar("PROTIO")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
            
    }
}

export default TipoOperacion;
