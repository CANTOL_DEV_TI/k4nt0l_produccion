import React from "react";
import { BotonGuardar } from "../../../components/Botones";

class TipoOperacionEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { id_tpoperacion: 0, tipo_operacion: '' }
        } else {
            this.state = { id_tpoperacion: this.props.dataToEdit.id_tpoperacion, nom_familia: this.props.dataToEdit.tipo_operacion }
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'codigo') {
            this.setState({ id_tpoperacion: e.target.value })
        }

        if (e.target.name === 'nombre') {
            this.setState({ tipo_operacion: e.target.value })
        }
    }


    render() {
        const { eventOnclick, nameOpe } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-4 col-form-label">
                                <input type="text" className="form-control form-control-sm" name="codigo" onChange={this.handleChange} value={this.state.id_tpoperacion} />
                            </div>                            
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">nombre: </div>
                            <div className="col-sm-6 col-form-label">
                                <input type="text" className="form-control form-control-sm" name="nombre" autoFocus onChange={this.handleChange} value={this.state.tipo_operacion} />
                            </div>
                        </div>
                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "id_tpoperacion": this.state.id_tpoperacion, "tipo_operacion": this.state.tipo_operacion })} texto={nameOpe} />
            </div>
        )
    }
}

export default TipoOperacionEdicion;