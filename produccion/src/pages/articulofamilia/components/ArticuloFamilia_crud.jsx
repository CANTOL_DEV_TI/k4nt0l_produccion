import React, { useState, useEffect } from "react";
import { get_familia_activo } from '../../../services/familiaServices';
import { get_grupo_sap } from '../../../services/articuloGrupoService';
import { get_subarea_operacion } from '../../../services/subAreaServices';
import { ComboBoxForm } from "../../../components/ComboBox";
import { BsSearch } from "react-icons/bs";
import { ArticuloSapBuscar } from "../ArticuloSapBuscar";
import { useVentanaModal } from "../../../hooks/useVentanaModal";
import VentanaModal from "../../../components/VentanaModal";
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

const iniFormulario = {
    cod_articulo: "",
    tipo_formulado: "0",
    id_grupo: "0",
    id_familia: "0",
    cod_subarea: "0",
    articulo: "",
    abc: "B",
    cant_lote_min: 0,
    cant_und_hora: 0,
    cob_minimo_dia: 0,
    cob_ideal_dia: 0,
    cob_maximo_dia: 0,
    id_maquina: "",
    estacion: "",
    frecuencia_uso: 0,
    cpdp: 0,
    sw_estado: "1",
    sw_maneja_plano: "1",
};

// ABC
const listAbc = [
    { "codigo": "A", "nombre": "A" },
    { "codigo": "B", "nombre": "B" },
    { "codigo": "C", "nombre": "C" },
];

// tipo formulado
const listFormulado = [
    { "codigo": "1", "nombre": "FORMULADO" },
    { "codigo": "2", "nombre": "NO FORMULADO" },
]

export function ArticuloFamilia_crud({ createData, updateData, dataToEdit, setDataToEdit, sw_mode = "C" }) {
    // C: CREAR , M: MODIFICAR    

    // Asignar a variables de estado     
    const [formulario, setFormulario] = useState(iniFormulario);
    const [checked, setChecked] = useState(false);
    useEffect(() => {
        if (dataToEdit) {
            setFormulario(dataToEdit);
        } else {
            setChecked(true);
            setFormulario(iniFormulario);
        }
    }, [dataToEdit]);

    // cargar combo sub area, grupo, familia
    const [subArea, setSubArea] = useState([]);
    const [listGrupo, setListGrupo] = useState([]);
    const [familia, setFamilia] = useState([])
    useEffect(() => {
        get_subarea_operacion('1')
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.cod_subarea, "nombre": data.nom_subarea
                    }
                })
                setSubArea(cbo_data)
            });
        get_grupo_sap()
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.id_grupo, "nombre": data.grupo
                    }
                })
                setListGrupo(cbo_data)
            });
        get_familia_activo()
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.id_familia, "nombre": data.nom_familia
                    }
                })
                setFamilia(cbo_data)
            });
    }, []);

    // CAPTURAR EL EVENTO DE LOS IMPUTS 
    const handleChange = (evento) => {
        if (evento.target.name === "sw_estado" || evento.target.name === "sw_maneja_plano") {
            setChecked(!checked);
            setFormulario({
                ...formulario,
                [evento.target.name]: evento.target.checked === true ? "1" : "0",
            });
        } else {
            setFormulario({
                ...formulario,
                [evento.target.name]: (evento.target.value).toUpperCase(),
            });
        }
    };

    // GUARDAR INFORMACION 
    const handleSubmit = (e) => {
        if (formulario.tipo_formulado === "0") {
            alert("Seleccionar Tipo Formulado.");
            return;
        }
        if (formulario.id_grupo === "0") {
            alert("Seleccionar Grupo.");
            return;
        }
        if (formulario.id_familia === "0") {
            alert("Seleccionar Familia.");
            return;
        }
        if (formulario.cod_subarea === "0") {
            alert("Seleccionar Sub Area.");
            return;
        }
        if (formulario.cod_articulo.length == 0) {
            alert("Ingrese código de artículo.");
            return;
        }
        if (formulario.articulo.length == 0) {
            alert("Ingrese descipción de artículo.");
            return;
        }

        if (formulario.abc === "0") {
            alert("Seleccione ABC");
            return;
        }

        // generar datos 
        const new_data = {
            "cod_articulo": formulario.cod_articulo,
            "tipo_formulado": formulario.tipo_formulado,
            "id_grupo": formulario.id_grupo,
            "id_familia": formulario.id_familia,
            "cod_subarea": formulario.cod_subarea,
            "articulo": formulario.articulo,
            "abc": formulario.abc,
            "cant_lote_min": formulario.cant_lote_min,
            "cant_und_hora": formulario.cant_und_hora,
            "cob_minimo_dia": formulario.cob_minimo_dia,
            "cob_ideal_dia": formulario.cob_ideal_dia,
            "cob_maximo_dia": formulario.cob_maximo_dia,
            "cpdp": formulario.cpdp,
            "sw_estado": formulario.sw_estado,
            "sw_maneja_plano": formulario.sw_maneja_plano,
            "id_maquina": formulario.id_maquina,
            "estacion": formulario.estacion,
            "frecuencia_uso": formulario.frecuencia_uso,
        };

        if (sw_mode === "C") {
            createData(new_data);

        } else {
            updateData(new_data);
        }

        handleReset();
    };

    const handleReset = (e) => {
        setFormulario(iniFormulario);
        setDataToEdit(null);
    };

    // MOSTRAR MODAL
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = () => {
        handleShow(true)
    };
    // Seleccionar datos de tabla
    const eventoSeleccionar = (datos) => {
        if (Object.keys(datos).length) {
            // buscar si existe en lista
            setFormulario({
                ...formulario,
                cod_articulo: datos.cod_articulo,
                articulo: datos.articulo,
                tipo_formulado: datos.tipo_formulado,
                id_grupo: datos.cod_grupo,
                cob_minimo_dia: datos.cob_min,
                cob_ideal_dia: datos.cob_ide,
                cob_maximo_dia: datos.cob_max,
                cant_und_hora: datos.und_x_hora,
                abc: datos.abc,
            });
            handleClose(true);

        }
    };

    // RETORNAR INFORMACIÓN 
    return (
        <div className="row g-2">
            {/* INICIO  FORM*/}
            <div className="col-md-8">
                <div className="input-group">
                    <div className="input-group-text" id="btnGroupAddon">Cod.Artículo</div>
                    <input type="text" className="form-control" readOnly name="cod_articulo" onChange={handleChange} value={formulario.cod_articulo} />
                    <button className={`${sw_mode === "C" ? 'btn btn-warning px-2' : 'btn btn-warning px-2 disabled'}`} onClick={mostrarVentana}><BsSearch /> Buscar en SAP</button>
                </div>
            </div>
            <div className="col-md-2">
                <input className="form-check-input" type="checkbox" name="sw_estado" checked={formulario.sw_estado === "1" ? true : false} onChange={handleChange} />
                <label className="form-check-label ms-2">
                    Habilitado
                </label>
            </div>
            <div className="col-md-2">
                <input className="form-check-input" type="checkbox" name="sw_maneja_plano" checked={formulario.sw_maneja_plano === "1" ? true : false} onChange={handleChange} />
                <label className="form-check-label ms-2">
                    Utiliza plano
                </label>
            </div>
            <div className="col-md-12">
                <div className="input-group">
                    <div className="input-group-text" id="btnGroupAddon">Descripción Artículo</div>
                    <input type="text" className="form-control" name="articulo" onChange={handleChange} value={formulario.articulo} />
                </div>
            </div>
            <div className="col-md-4">
                <label className="form-label">Tipo Formulado</label>
                <ComboBoxForm
                    datosRow={listFormulado}
                    nombre_cbo="tipo_formulado"
                    manejaEvento={handleChange}
                    valor_ini="Tipo Formulado"
                    valor={formulario.tipo_formulado}
                />

            </div>
            <div className="col-md-4">
                <label className="form-label">Grupo</label>
                <ComboBoxForm
                    datosRow={listGrupo}
                    nombre_cbo="id_grupo"
                    manejaEvento={handleChange}
                    valor_ini="Grupo Artículo"
                    valor={formulario.id_grupo}
                />
            </div>
            <div className="col-md-4">
                <label className="form-label">Sub Area</label>
                <ComboBoxForm
                    datosRow={subArea}
                    nombre_cbo="cod_subarea"
                    manejaEvento={handleChange}
                    valor_ini="Seleccionar..."
                    valor={formulario.cod_subarea}
                />
            </div>
            <div className="col-md-12">
                <Tabs defaultActiveKey="general" id="uncontrolled-tab-example" className="mb-2">
                    <Tab eventKey="general" title="Conf. general" className="">
                        <div className="row g-2 p-1 border bg-light">
                            <div className="col-md-4">
                                <label className="form-label">Familia</label>
                                <ComboBoxForm
                                    datosRow={familia}
                                    nombre_cbo="id_familia"
                                    manejaEvento={handleChange}
                                    valor_ini="Seleccionar..."
                                    valor={formulario.id_familia}
                                />
                            </div>
                            <div className="col-md-3">
                                <label className="form-label">abc</label>
                                <ComboBoxForm
                                    datosRow={listAbc}
                                    nombre_cbo="abc"
                                    manejaEvento={handleChange}
                                    valor_ini="Seleccionar..."
                                    valor={formulario.abc}
                                />
                            </div>
                            <div className="col-md-5">
                                <label className="form-label">Frecuencia de uso (días)</label>
                                <input type="number" title="Total días Laborables del Mes" className="form-control" name="frecuencia_uso" onChange={handleChange} value={formulario.frecuencia_uso} />
                            </div>
                            <div className="col-md-8">
                                <label className="form-label">Código de Máquinas donde se Usa (separado por comas (,))</label>
                                <input type="text" title="Máquinas donde se Utiliza" className="form-control" name="id_maquina" onChange={handleChange} value={formulario.id_maquina} />
                            </div>
                            <div className="col-md-4">
                                <label className="form-label">Estación</label>
                                <input type="text" title="Total días Laborables del Mes" className="form-control" name="estacion" onChange={handleChange} value={formulario.estacion} />
                            </div>
                        </div>
                    </Tab>
                    <Tab eventKey="produccion" title="Conf. producción">
                        <div className="row g-2 p-1 border bg-light">
                            <div className="col-md-4">
                                <label className="form-label">Consumo Diario Plan</label>
                                <input type="number" title="Total días Laborables del Mes" className="form-control" name="cpdp" onChange={handleChange} value={formulario.cpdp} />
                            </div>
                            <div className="col-md-4">
                                <label className="form-label">Lote ninimo</label>
                                <input type="number" title="Total días Laborables del Mes" className="form-control" name="cant_lote_min" onChange={handleChange} value={formulario.cant_lote_min} />
                            </div>
                            <div className="col-md-4">
                                <label className="form-label">Unidades por Hora</label>
                                <input type="number" title="Total días Laborables del Mes" className="form-control" name="cant_und_hora" onChange={handleChange} value={formulario.cant_und_hora} />
                            </div>
                            <div className="col-md-4">
                                <label className="form-label">Cobertura ninimo (día)</label>
                                <input type="number" title="Total días Laborables del Mes" className="form-control" name="cob_minimo_dia" onChange={handleChange} value={formulario.cob_minimo_dia} />
                            </div>
                            <div className="col-md-4">
                                <label className="form-label">Cobertura Ideal (día)</label>
                                <input type="number" title="Total días Laborables del Mes" className="form-control" name="cob_ideal_dia" onChange={handleChange} value={formulario.cob_ideal_dia} />
                            </div>
                            <div className="col-md-4">
                                <label className="form-label">Cobertura Maxima (día)</label>
                                <input type="number" title="Total días Laborables del Mes" className="form-control" name="cob_maximo_dia" onChange={handleChange} value={formulario.cob_maximo_dia} />
                            </div>
                        </div>
                    </Tab>
                </Tabs>
            </div>
            <div className="modal-footer">
                <button type="submit" className="btn btn-primary" onClick={handleSubmit}>{`${sw_mode === "C" ? 'Crear' : 'Actualizar'}`}</button>
            </div>
            {/* FIN FORM */}
            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="xsm"
                handleClose={handleClose}
                titulo={"Buscar Articulo desde SAP"}
            >
                <ArticuloSapBuscar eventoClick={eventoSeleccionar} />
            </VentanaModal>
            {/* FIN MODAL */}
        </div>

    );
}