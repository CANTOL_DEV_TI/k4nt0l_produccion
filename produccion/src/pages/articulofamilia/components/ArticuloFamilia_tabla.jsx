import { BsPencilFill, BsXCircleFill } from "react-icons/bs";

export function ArticuloFamilia_tabla({ datos_fila, setDataToEdit, deleteData, eventoClick }) {
    // RETORNAR INFORMACIÓN 
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" title="Opciones" style={{ width: '85px' }}>Opc</th>
                            <th className="align-middle" title="Código Artículo">Cod.Artículo</th>
                            <th className="align-middle" title="Descripción Artículo">Descripción Artículo</th>
                            <th className="align-middle" title="ABC">ABC</th>
                            <th className="align-middle" title="Cantidad de Lote Mínimo">Lote Minimo</th>
                            <th className="align-middle" title="Standar Unidad Hora">Unidad Hora</th>
                            <th className="align-middle" title="Cobertura Mínimo">Cob. Min</th>
                            <th className="align-middle" title="Cobertura Ideal">Cob. Ide</th>
                            <th className="align-middle" title="Cobertura Máxima">Cob. Max</th>
                            <th className="align-middle" title="Familia">Familia</th>
                            <th className="align-middle" title="Grupo">Grupo</th>
                            <th className="align-middle" title="Maquina">Maquina</th>
                            <th className="align-middle" title="Frecuencia de Uso">F.Uso</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td>
                                            <button data-toggle="tooltip" title="Modificar" className="btn btn-outline-primary btn-sm"
                                                onClick={() => eventoClick("E", datos)}>
                                                <BsPencilFill />
                                            </button>
                                            <button data-toggle="tooltip" title="Eliminar" className="btn btn-outline-danger btn-sm ms-1"
                                                onClick={() => deleteData(datos)}>
                                                <BsXCircleFill />
                                            </button>
                                        </td>
                                        <td className="td-cadena">{datos.cod_articulo}</td>
                                        <td className="td-cadena">{datos.articulo}</td>
                                        <td className="td-cadena">{datos.abc}</td>
                                        <td className="td-cadena">{datos.cant_lote_min}</td>
                                        <td className="td-cadena">{datos.cant_und_hora}</td>
                                        <td className="td-cadena">{datos.cob_minimo_dia}</td>
                                        <td className="td-cadena">{datos.cob_ideal_dia}</td>
                                        <td className="td-cadena">{datos.cob_maximo_dia}</td>
                                        <td className="td-cadena">{datos.familia}</td>
                                        <td className="td-cadena">{datos.grupo}</td>
                                        <td className="td-cadena">{datos.id_maquina}</td>
                                        <td className="td-cadena">{datos.frecuencia_uso}</td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="13">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}