import React, { useState, useEffect } from 'react'
import { get_familia_activo } from '../../services/familiaServices';
import {
    get_articulo_familia,
    insert_articulo_familia,
    update_articulo_familia,
    delete_articulo_familia,
} from '../../services/articulofamiliaServices';
import { get_area_responsable_sap } from '../../services/produccion';
import { get_grupo_sap } from '../../services/articuloGrupoService';
import { BotonExcel, BotonNuevo } from "../../components/Botones";
import { InputTextBuscarV2 } from "../../components/InputTextBuscar";
import { useVentanaModal } from "../../hooks/useVentanaModal";
import { ArticuloFamilia_tabla } from "./components/ArticuloFamilia_tabla";
import { ArticuloFamilia_crud } from "./components/ArticuloFamilia_crud";
import { ComboBoxForm } from "../../components/ComboBox";
import VentanaModal from "../../components/VentanaModal";

// seguridad
import {  Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

// tipo formulado
const listFormulado = [
    { "codigo": "1", "nombre": "FORMULADO" },
    { "codigo": "2", "nombre": "NO FORMULADO" },
]

export function ArticuloFamilia() {
    // seguridad    
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
	const Seguridad = async () => {
        const ValSeguridad = await Validar("PROARF")
		setVentanaSeguridad(ValSeguridad.Validar)
	}

    const [dataToEdit, setDataToEdit] = useState(null);

    // Captura el evento y los valores de los inputs
    const [formulado, setFormulado] = useState("0");
    const [grupo, setGrupo] = useState("0");
    const [area, setArea] = useState("0");
    function datosBusqueda(evento) {
        if (evento.target.name === 'cbo_area') {
            setArea(evento.target.value)
        }
        if (evento.target.name === 'cbo_formulado') {
            setFormulado(evento.target.value)
        }
        if (evento.target.name === 'cbo_grupo') {
            setGrupo(evento.target.value)
        }
    };

    // CONSUMIR DATOS
    const [sw_modo, setSw_modo] = useState("C");
    const [refrescar, setRefrescar] = useState(true);
    const [listDatos, setListDatos] = useState([]);
    useEffect(() => {
        Seguridad();
        setRefrescar(false);
        getData();
    }, [refrescar, formulado, grupo, area])

    // 
    const getData = () => {
        get_articulo_familia(formulado, grupo, area)
            .then(res => {
                setListDatos(res)
                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM 
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_articulo", "articulo"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA 
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // cargar combos area, grupo
    const [listArea, setListArea] = useState([]);
    const [listGrupo, setListGrupo] = useState([]);
    useEffect(() => {
        Seguridad();
        get_area_responsable_sap()
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.COD_AREA, "nombre": data.AREA
                    }
                })
                setListArea(cbo_data)
            });
        get_grupo_sap()
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.id_grupo, "nombre": data.grupo
                    }
                })
                setListGrupo(cbo_data)
            });
    }, []);

    // INSERTAR DATOS 
    const createData = async (data) => {
        const res = await insert_articulo_familia(data);
        setRefrescar(true);
        handleClose(true);
    };

    // ACTUALIZAR DATOS 
    const updateData = (data) => {
        const res = update_articulo_familia(data)
        setRefrescar(true);
        handleClose(true)
    };

    // ACTUALIZAR DATOS 
    const deleteData = (data) => {
        delete_articulo_familia(data)
        setRefrescar(true);
    };

    // PAGINACIÓN  
    let LimitPag = 15;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // MOSTRAR MODAL 
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = (opc = "N", datos) => {
        if (opc === "E") {
            setDataToEdit(datos);
            setSw_modo("M")
        } else {
            setDataToEdit(null);
            setSw_modo("C")
        }
        handleShow(true)
    };

    // RETORNAR INFORMACIÓN 
    return (
        <div className="col-lg-12">

            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border-0 card-header">
                    <div className="align-items-center gy-3 row">
                        <div className="col-sm">
                            <h4 className="card-title mb-0">Lista de Artículos</h4>
                        </div>
                        <div className="col-sm-auto">
                            <div className="d-flex gap-1 flex-wrap">
                                <BotonExcel textoBoton={"Exportar Excel"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatos} nombreArchivo={"Maestro Articulo"} />
                                <BotonNuevo textoBoton={"Crear Articulo"} sw_habilitado={true} eventoClick={mostrarVentana} />
                            </div>
                        </div>
                    </div>
                </div>

                {/* INICIO BARRA DE NAVEGACION */}
                <div className="p-0 card-body">
                    <div className="mx-1 my-2 row">
                        <div className="col-sm-6 p-1">
                            <InputTextBuscarV2 onBuscarChange={onSearchChange} />
                        </div>
                        <div className="col-xxl-2 col-sm-6 p-1">
                            <ComboBoxForm
                                datosRow={listArea}
                                nombre_cbo="cbo_area"
                                manejaEvento={datosBusqueda}
                                valor_ini="Area Responsable"
                                valor={area}
                            />
                        </div>
                        <div className="col-xxl-2 col-sm-6 p-1">
                            <ComboBoxForm
                                datosRow={listGrupo}
                                nombre_cbo="cbo_grupo"
                                manejaEvento={datosBusqueda}
                                valor_ini="Grupo Artículo"
                                valor={grupo}
                            />
                        </div>
                        <div className="col-xxl-2 col-sm-6 p-1">
                            <ComboBoxForm
                                datosRow={listFormulado}
                                nombre_cbo="cbo_formulado"
                                manejaEvento={datosBusqueda}
                                valor_ini="Tipo Formulado"
                                valor={formulado}
                            />
                        </div>
                    </div>

                    {/* FIN BARRA DE NAVEGACION */}

                    { /* INICIO TABLA */}
                    <ArticuloFamilia_tabla datos_fila={search(listDatos)} setDataToEdit={setDataToEdit} deleteData={deleteData} eventoClick={mostrarVentana} />
                    {/* FIN TABLA */}

                    {/* INICIO PAGINACION */}
                    <div className="d-flex justify-content-end m-2">
                        <div className="pagination-wrap hstack gap-1">
                            Total Reg. {listDatos.length}
                            <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                            <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                        </div>
                    </div>
                    {/* FIN PAGINACION */}
                </div>
            </div>

            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="lg"
                handleClose={handleClose}
                titulo={dataToEdit != null ? "Editar Artículo" : "Crear Artículo"}
            >
                <ArticuloFamilia_crud
                    createData={createData}
                    updateData={updateData}
                    dataToEdit={dataToEdit}
                    setDataToEdit={setDataToEdit}
                    sw_mode={sw_modo}
                />
            </VentanaModal>
            {/* FIN MODAL */}

            {/* seguridad */}
            <VentanaBloqueo show={VentanaSeguridad} />

        </div>
    );
}