import React, { useState, useEffect } from 'react'
import { get_articulo_sap,get_articulo_sap_filtro } from '../../services/articulofamiliaServices';
import { InputTextBuscar, InputTextBuscarV2 } from "../../components/InputTextBuscar";
import { ArticuloSapBuscar_tabla } from "./components/ArticuloSapBuscar_tabla";
import { BotonBuscar } from '../../components/Botones';

export function ArticuloSapBuscar({ eventoClick }) {

    // CONSUMIR DATOS
    const [sw_modo, setSw_modo] = useState("C");
    const [listDatos, setListDatos] = useState([]);
    useEffect(() => {
        //getData();
    }, [])

    // Cargar datos a listado
    //const getData = (pFiltro) => {
    //    get_articulo_sap()
    //        .then(res => {
    //            setListDatos(res)
    //            setCurrentPage(0);
    //        })
    //};
    const getData = (pFiltro) => {
        get_articulo_sap_filtro(pFiltro)
            .then(res => {
                setListDatos(res)
                setCurrentPage(0);
            })
    };

    const HandleSearch = () =>{
        getData(buscarDato)
    }

    const handleChange = (e) =>{
        setBuscarDato(e.target.value.toUpperCase())
    }

    // BUSQUEDA EN DOM
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["cod_articulo", "articulo"];
    const search = (listDatos) => {

        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);
        console.log(listDatos)
        // SI HAY VALOR EN CAJA DE BUSQUEDA
        //const filtered = listDatos.filter((item) =>
        //    idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        //);
        //return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // Seleccionar datos de tabla
    const eventoSeleccionar = (datos) => {
        eventoClick(datos);
    };

    // PAGINACIÓN 
    let LimitPag = 10;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        //setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">
            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-sm-10">
                            <input type="search" className="form-control sm-8" placeholder="Buscar" name="txt_buscar" onChange={handleChange} />

                        </div>
                        <div className="col-sm-2">
                            <BotonBuscar sw_habilitado={true} eventoClick={HandleSearch}/>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <ArticuloSapBuscar_tabla datos_fila={listDatos} eventoClick={eventoSeleccionar} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-end m-2">
                    <div className="pagination-wrap hstack gap-1">
                        Total Reg. {listDatos.length}
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                        <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>
        </div>
    );
}

/*<InputTextBuscarV2 onBuscarChange={onSearchChange} /> */