import React from "react";
import {ComboBoxForm2} from "../../components/ComboBox.jsx";
import {BsSearch} from "react-icons/bs";
import {Get_StopMachine_SubArea} from "./services/oeeEficienciaServices.jsx";
import {get_subarea} from "./services/oeeSubAreaServices.jsx";
import {get_RegistroOee_Excel} from "./services/oee_ExportExcel.jsx"
import {BotonExcel} from "../../components/Botones.jsx";

import XLSXDownload from "./components/EficienciaOperativa_DownloadExcel.jsx";



class EficienciaOperativa_Excel extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            listArea: [],
            fecha_doc_ini: null,
            fecha_doc_fin: null,
            cod_subarea: 0,
            resultado: [],
            
            multiDataSet:  [
                {columns: [
                    { value: "Name", widthPx: 50 }, // width in pixels
                    { value: "Salary", widthCh: 20 }, // width in charachters
                    { value: "Sex", widthPx: 60, widthCh: 20 }, // will check for width in pixels first
                  ],
                  data: [
                    ["Johnson", 30000, "Male"],
                    ["Monika", 355000, "Female"],
                    ["Konstantina", 20000, "Female"],
                    ["John", 250000, "Male"],
                    ["Josef", 450500, "Male"],
                  ],
                },

                {
                    xSteps: 1, // Will start putting cell with 1 empty cell on left most
                    ySteps: 5, //will put space of 5 rows,
                    columns: ["Name", "Department"],
                    data: [
                      ["Johnson", "Finance"],
                      ["Monika", "IT"],
                      ["Konstantina", "IT Billing"],
                      ["John", "HR"],
                      ["Josef", "Testing"],
                    ],
                  },
              ]

        }
    }


    handleFindSubArea = async () =>{
        const responseJson = await get_subarea("00001")
        this.setState({listArea: responseJson})
    }


    handleChange =(e)=>{
        if (e.target.name === 'fecha_doc_ini') {
            this.setState({fecha_doc_ini: e.target.value})
        }

        if (e.target.name === 'fecha_doc_fin') {
            this.setState({fecha_doc_fin: e.target.value})
        }


        if (e.target.name === 'cod_subarea') {
            this.setState({cod_subarea: e.target.value})
        }
    }

    handleBusqueda = async (cod_subarea, fecha_ini, fecha_fin) => {
        const responseJson = await get_RegistroOee_Excel(cod_subarea, fecha_ini, fecha_fin)
        console.log(responseJson)

        this.setState({resultado: responseJson})

    }




    render() {
        return(
            <React.Fragment>
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header border-0 p-0">
                            <div className="mx-1 my-2 row">
                                <div className="col-xl-3 col-sm-3 p-1" style={{minWidth: "20%"}}>
                                    <h4 className="card-title mb-0" style={{marginTop: "5px", fontSize: "22px"}}>Registro Maquinas Detenidas</h4>
                                </div>

                                <div className="col-xl-7 col-sm-7 p-1" style={{minWidth: "80%"}}>
                                    <div className="input-group">

                                        <span className="input-group-text fw-bold" style={{minWidth: "135px"}}>Fecha Desde:</span>
                                        <input type="date" value={this.state.fecha_doc_ini} name="fecha_doc_ini"
                                               onChange={this.handleChange} className="form-control"
                                               style={{width: "130px", padding: "0 1px", marginRight: "15px"}} disabled={false}
                                        />


                                        <span className="input-group-text fw-bold" style={{minWidth: "135px"}}>Fecha Hasta:</span>
                                        <input type="date" value={this.state.fecha_doc_fin} name="fecha_doc_fin"
                                               onChange={this.handleChange} className="form-control"
                                               style={{width: "130px", padding: "0 1px", marginRight: "15px"}} disabled={false}
                                        />


                                        <ComboBoxForm2
                                            nombre_cbo="cod_subarea"
                                            valor_ini="*** Area ***"
                                            datosRow={this.state.listArea}
                                            manejaEvento={this.handleChange}
                                            valor={this.state.cod_subarea}
                                            view={false}
                                        />


                                        <button
                                            className="btn btn-primary"
                                            onClick={() => this.handleBusqueda(this.state.cod_subarea, this.state.fecha_doc_ini, this.state.fecha_doc_fin)}
                                            style={{marginRight: "10px"}}
                                        >
                                            <BsSearch/>
                                        </button>

                                        {/* <BotonExcel textoBoton={"Listo, Puedes Exportar"} sw_habilitado={this.state.resultado.length > 0 ? true : false} listDatos={this.state.resultado} nombreArchivo={"export_resultados"} /> */}


                                        <XLSXDownload sw_habilitado={this.state.resultado.length > 0 ? true : false} listDatos={this.state.resultado}  />

                                        


                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


            </React.Fragment>
        )




    }

    async componentDidMount(){
        this.handleFindSubArea()
    }

}

export default EficienciaOperativa_Excel;