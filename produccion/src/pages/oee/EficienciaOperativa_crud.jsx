import React, { useState, useEffect } from 'react'
import {
    BsSearch,
    BsPlusLg,
} from "react-icons/bs";
import { get_subarea_operacion_usuario } from '../../services/subAreaServices';
import { get_periodo_laboral_turno } from '../../services/periodoLaboralServices';
import { get_config_proceso_det_articulo } from '../../services/configProcesoServices';
import { get_turno_det } from '../../services/turnoServices';
import {insert_oeeEficiencia, update_oeeEficiencia} from './services/oeeEficienciaServices.jsx';

import { useVentanaModal } from "../../hooks/useVentanaModal";
import { EficienciaOperativa_crud_tabla } from "./components/EficienciaOperativa_crud_tabla";
import { EficienciaOperativa_crud_detalle } from "./components/EficienciaOperativa_crud_detalle";
import { EficienciaOperativa_crud_oee_detalle } from "./components/EficienciaOperativa_crud_oee_detalle";
import { EficienciaOperativa_crud_oee } from "./components/EficienciaOperativa_crud_oee";
import { OrdenFabricacionBuscar } from "../ordenFabricacionPrograma/OrdenFabricacionBuscar";
import { ArticuloFamiliaBuscar } from "../articulofamilia/ArticuloFamiliaBuscar";
import { EmpleadoBuscar } from "../empleado/EmpleadoBuscar";
import { ComboBoxForm } from "../../components/ComboBox";
import { BotonAtras, BotonGrabar } from "../../components/Botones";
import VentanaModal from "../../components/VentanaModal";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

const v_fecha = new Date();
const iniFormulario = {
    id_oee: 0,
    fecha_doc: v_fecha.toLocaleDateString('en-GB', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
    }).split('/').reverse().join('-'),
    cod_subarea: "0",
    cod_turno: "0",
    num_of_sap: "",
    cod_articulo: "",
    articulo: "",
    cod_empleado: "",
    empleado: "",
    id_paso_equipo: "0",
    num_paso: "0",
    id_equipo: "0",
    id_tpoperacion: "0",
    std_und_hora: 0,
    hora_ini: "",
    hora_fin: "",
    hora_plan: 0,
    hora_operativo: 0,
    produccion_total: 0,
    sw_estado: "1",
};

const iniFormulario_det = {
    item_oee: 0,
    cod_tipo_registro: "0",
    tipo_registro: "",
    umu: "",
    cod_causa: "0",
    causa: "",
    cod_motivo_causa: "0",
    motivo_causa: "",
    cantidad: 0,
    detalle: "",
    num_vale: "",
};

export function EficienciaOperativa_crud(modoVista = "C") {
    // seguridad
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
    const Seguridad = async () => {
        const ValSeguridad = await Validar("PROOFP")
        setVentanaSeguridad(false)
    }

    // CRUD ID
    const [formulario, setFormulario] = useState(iniFormulario);
    const [formDet, setFormDet] = useState([]);
    const [numOEEItem, setNumOEEItem] = useState(0);
    const [tipoModal, setTipoModal] = useState("");
    const [checked, setChecked] = useState(false);

    // BUSQUEDA GENERALES
    function datosBusqueda(evento) {
        if (evento.target.name === 'sw_todo') {
            setChecked(!checked);
            setFormulario({
                ...formulario,
                [evento.target.name]: evento.target.checked === true ? "1" : "0",
            });
        } else {
            setFormulario({
                ...formulario,
                [evento.target.name]: (evento.target.value).toUpperCase(),
            });
        }

        // cargar los turnos relacionados al area
        if (evento.target.name === 'cod_subarea') {
            let c_cod_subarea = (evento.target.value).toUpperCase();
            cbo_turno_subarea(c_cod_subarea === "0" ? "999" : c_cod_subarea);
        }

        // cargar los dias lab del turno
        if (evento.target.name === 'cod_turno') {
            let c_cod_turno = (evento.target.value).toUpperCase();
            hora_ini_fin_plan(c_cod_turno);
        }

        // calcular horas plan segun hora inicio y fin
        if (evento.target.name === 'hora_ini') {
            let c_hora_ini = (evento.target.value).toUpperCase();
            hora_ini_fin_modificar("I", c_hora_ini);
        }
        if (evento.target.name === 'hora_fin') {
            let c_hora_fin = (evento.target.value).toUpperCase();
            hora_ini_fin_modificar("F", c_hora_fin);
        }

        // obtener datos del paso seleccionado
        if (evento.target.name === 'id_paso_equipo') {
            let c_id_paso_equipo = (evento.target.value).toUpperCase();
            let filtrarDatos = listPasoDato.filter(paso => paso.num_paso.toString() + paso.id_equipo.toString() === c_id_paso_equipo);

            if (filtrarDatos.length === 0) {
                return;
            }

            setFormulario({
                ...formulario,
                id_paso_equipo: c_id_paso_equipo,
                num_paso: filtrarDatos[0].num_paso,
                id_equipo: filtrarDatos[0].id_equipo,
                id_tpoperacion: filtrarDatos[0].id_tpoperacion,
                std_und_hora: filtrarDatos[0].std_und_hora,
            });
        }

        // reiniciar detalle
        IniDetalleOee(iniFormulario_det);
    };

    // añadir detalle de documento
    const eventoAddDetalle = (datos) => {
        if (Object.keys(datos).length) {
            const newList = formDet.concat(datos);
            console.log(newList)
            const listaCalculado = eventoUpdateDetalleOeeCalcular(newList);
            setFormDet(listaCalculado);

        };
        console.log("@@@@@@@@@@@@@@@@@@")
        console.log(formDet)
    };

    // añadir motivo de OEE
    const IniDetalleOee = (datos) => {

        datos["cod_tipo_registro"] = "5";
        datos["tipo_registro"] = "OEE";
        datos["umu"] = "PORCENTAJE";

        let aaa = { ...datos }
        aaa["item_oee"] = 1;
        aaa["cod_causa"] = "8";
        aaa["causa"] = "DISPONIBILIDAD";
        let newList = [aaa];

        let bbb = { ...datos }
        bbb["item_oee"] = 2;
        bbb["cod_causa"] = "9";
        bbb["causa"] = "RENDIMIENTO";
        let newList2 = [...newList, bbb];

        let ccc = { ...datos }
        ccc["item_oee"] = 3;
        ccc["cod_causa"] = "10";
        ccc["causa"] = "CALIDAD";
        let newList3 = [...newList2, ccc];

        let ddd = { ...datos }
        ddd["item_oee"] = 4;
        ddd["cod_causa"] = "11";
        ddd["causa"] = "OEE";
        let newList4 = [...newList3, ddd];

        setFormDet(newList4);
    };

    const eventoUpdateDetalleOee = (datos) => {
        if (Object.keys(datos).length) {
            const newList = formDet.map(item =>
                item.item_oee === datos.item_oee ? { ...datos } : item
            );
            setFormDet(newList);
            handleClose(true);
        };
    };

    const eventoUpdateDetalleOeeCalcular = (c_detalle) => {
        console.log("??????")
        console.log(c_detalle)
        console.log("??????")
        if (c_detalle.length > 0) {
            const minParada = c_detalle.reduce((acc, det) => det.cod_tipo_registro === "1" || det.cod_tipo_registro === "2" ? acc + Number(det.cantidad) : acc, 0);
            const prodBuena = c_detalle.reduce((acc, det) => det.cod_tipo_registro === "3" ? acc + Number(det.cantidad) : acc, 0);
            const prodMala = c_detalle.reduce((acc, det) => det.cod_tipo_registro === "4" ? acc + Number(det.cantidad) : acc, 0);
            const prodTotal = prodBuena + prodMala;
            const minOperativo = Number(formulario.hora_plan) > minParada ? (Number(formulario.hora_plan) - minParada) || 0 : 0;
            const horaLogrado = Number(formulario.std_und_hora) > 0 ? Math.round((prodTotal / Number(formulario.std_und_hora)) * 60) : 0;

            // calcular OEE
            const oeeDispo = Number(formulario.hora_plan) > 0 ? minOperativo / Number(formulario.hora_plan) : 0;
            const oeeRendi = minOperativo > 0 ? horaLogrado / minOperativo : 0;
            const oeeCalid = prodTotal > 0 ? prodBuena / prodTotal : 0;
            const oeeOee = (oeeDispo * oeeRendi * oeeCalid) || 0;

            // actualizar cabecera
            setFormulario({
                ...formulario,
                hora_operativo: minOperativo,
                produccion_total: prodTotal,
            });

            // actualizar detalle
            const newList = c_detalle.map(item =>
                item.item_oee === 1 ? { ...item, cantidad: oeeDispo } :
                    item.item_oee === 2 ? { ...item, cantidad: oeeRendi } :
                        item.item_oee === 3 ? { ...item, cantidad: oeeCalid } :
                            item.item_oee === 4 ? { ...item, cantidad: oeeOee } : item
            );

            return newList;

        }
    };

    // Eliminar items
    const deleteItem = (c_item) => {
        const c_res = formDet.filter((item) => item.item_oee !== c_item);
        const listaCalculado = eventoUpdateDetalleOeeCalcular(c_res);
        setFormDet(listaCalculado);
    };

    // Seleccionar articulo
    const eventoSeleccionarArticulo = (datos) => {
        setFormulario({
            ...formulario,
            cod_articulo: datos.cod_articulo,
            articulo: datos.articulo,
        });
        handleClose(true);

        // cargar los pasos del articulo
        cbo_paso_maquina(datos.cod_articulo);
    };

    // Seleccionar empleado
    const eventoSeleccionarEmpleado = (datos) => {
        setFormulario({
            ...formulario,
            cod_empleado: datos.cod_empleado,
            empleado: datos.nombre,
        });
        handleClose(true);
    };

    // seleccionar orden de fabricación
    const eventoSeleccionarOf = (datos) => {
        setFormulario({
            ...formulario,
            num_of_sap: datos.num_of,
            cod_articulo: datos.cod_articulo,
            articulo: datos.articulo,
        });
        handleClose(true);
        // cargar los pasos del articulo
        cbo_paso_maquina(datos.cod_articulo);
    };

    // cargar combos area y grupo
    const [listArea, setListArea] = useState([]);
    const [listTurno, setListTurno] = useState([]);
    const [listPaso, setListPaso] = useState([]);
    const [listPasoDato, setListPasoDato] = useState([]);
    useEffect(() => {
        Seguridad();
        IniDetalleOee(iniFormulario_det);
        get_subarea_operacion_usuario("00001")
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.cod_subarea, "nombre": data.nom_subarea
                    }
                })
                setListArea(cbo_data)
            });
    }, [])

    const cbo_turno_subarea = (cod_subarea) => {
        get_periodo_laboral_turno(cod_subarea)
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.cod_turno, "nombre": data.nom_turno
                    }
                })
                setListTurno(cbo_data)
            });
    };

    const hora_ini_fin_plan = (cod_turno) => {
        get_turno_det(cod_turno)
            .then(res => {
                let c_dia = new Date(formulario.fecha_doc).getDay() + 1;
                let filtrarDia = res.filter(turno => turno.num_dia === c_dia);
                let c_minuto = calular_minutos(filtrarDia);

                setFormulario({
                    ...formulario,
                    cod_turno: cod_turno,
                    hora_ini: filtrarDia[0].hora_ini,
                    hora_fin: filtrarDia[0].hora_fin,
                    hora_plan: c_minuto,
                });
            });
    };

    const hora_ini_fin_modificar = (c_tipo = "", c_hora) => {
        let filtrarDia = [
            {
                hora_ini: c_tipo === "I" ? c_hora : formulario.hora_ini,
                hora_fin: c_tipo === "F" ? c_hora : formulario.hora_fin,
            }
        ];

        let c_minuto = calular_minutos(filtrarDia);

        setFormulario({
            ...formulario,
            hora_ini: filtrarDia[0].hora_ini,
            hora_fin: filtrarDia[0].hora_fin,
            hora_plan: c_minuto,
            hora_operativo: 0,
        });
    };

    const calular_minutos = (c_datos_dia) => {
        let c_fecha_ini = new Date(formulario.fecha_doc + " " + c_datos_dia[0].hora_ini);
        let c_fecha_fin = new Date(formulario.fecha_doc + " " + c_datos_dia[0].hora_fin);
        let c_minuto = 0;
        if (c_fecha_fin > c_fecha_ini) {
            c_minuto = Math.round((c_fecha_fin - c_fecha_ini) / 60000);
        }
        return c_minuto;
    };

    const cbo_paso_maquina = (cod_articulo) => {
        get_config_proceso_det_articulo(cod_articulo)
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.num_paso.toString() + data.id_equipo.toString(), "nombre": data.num_paso + ": " + data.equipo + " - " + data.tipo_operacion
                    }
                })
                setListPasoDato(res);
                setListPaso(cbo_data);
            });
    };

    // CONSUMIR DATOS
    const [refrescar, setRefrescar] = useState(true)
    const [listDatos, setListDatos] = useState([])
    useEffect(() => {
        setRefrescar(false);
        //getData();
    }, [refrescar, formulario])

    // GUARDAR INFORMACION
    const handleSubmit = (e) => {
        if (!formulario.fecha_doc) {
            alert("Seleccionar fecha");
            return;
        }
        if (formulario.cod_subarea === "0") {
            alert("Seleccionar area");
            return;
        }
        if (formulario.cod_articulo === "") {
            alert("Seleccionar artículo");
            return;
        }
        if (formulario.cod_empleado === "") {
            alert("Seleccionar colaborador");
            return;
        }
        if (formulario.cod_turno === "0") {
            alert("Seleccionar turno");
            return;
        }
        if (formulario.id_equipo === "0") {
            alert("Seleccionar paso/estación/máquina");
            return;
        }
        if (formDet.length === 4) {
            alert("Documento no tiene Detalle.");
            return;
        }

        // generar datos
        const new_data = {
            "id_oee": formulario.id_oee,
            "fecha_doc": Date.parse(formulario.fecha_doc),
            "cod_subarea": formulario.cod_subarea,
            "num_of_sap": formulario.num_of_sap,
            "cod_empleado": formulario.cod_empleado,
            "cod_articulo": formulario.cod_articulo,
            "articulo": formulario.articulo,
            "num_paso": formulario.num_paso,
            "id_equipo": formulario.id_equipo,
            "id_tpoperacion": formulario.id_tpoperacion,
            "cod_turno": formulario.cod_turno,
            "hora_ini": formulario.hora_ini,
            "hora_fin": formulario.hora_fin,
            "std_und_hora": formulario.std_und_hora,
            "hora_planificado": formulario.hora_plan,
            "sw_estado": formulario.sw_estado,
            "detalle": formDet
        };

        // realizar operación segun condición
        modoVista = "C";
        if (modoVista === "C") { // CREAR
            const res = insert_oeeEficiencia(new_data);
            handleAtras();
        } else { // MODIFICAR
            const res = update_oeeEficiencia(new_data)
            handleAtras();
        }
    };

    // Ir a consulta principal
    const handleAtras = (e) => {
        //modoVista("L") // lista
    };

    // MOSTRAR MODAL
    const [tituloModal, setTituloModal] = useState("");
    const [datosModal, setDatosModal] = useState([]);
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = (opc = "", datos) => {
        /* opc:
        prd: orden de fabricación
        det: detalle de crud
        emp: empleado
        art: articulo
        oee: motivo oee
        */
        if (formulario.cod_subarea === "0") {
            alert("Primero seleccionar Area");
            return;
        }

        if (opc === "det") {
            if (formulario.id_paso_equipo === "0") {
                alert("Primero seleccionar Paso/estación/Máquina");
                return;
            }
            setNumOEEItem(formDet.length + 1)
            setTituloModal("Registrar Detalle");
        }
        if (opc === "prd") {
            setTituloModal("Buscar Orden Fabricación");
        }
        if (opc === "art") {
            setTituloModal("Buscar Artículo");
        }
        if (opc === "emp") {
            setTituloModal("Buscar Colaborador");
        }
        if (opc === "oee") {
            setTituloModal("Asignar " + datos.causa.toLowerCase());
            setDatosModal(datos);
        }
        setTipoModal(opc)
        handleShow(true)
    };

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">
            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border-0 p-0">
                    <div className="mx-1 my-2 row">
                        <div className="col-xl-6 col-sm-6 p-1">
                            <h4 className="card-title mb-0">Eficiencia Productiva OEE</h4>
                        </div>
                        <div className="col-xl-6 col-sm-6 p-1">
                            <div className="input-group">
                                <ComboBoxForm
                                    datosRow={listArea}
                                    nombre_cbo="cod_subarea"
                                    manejaEvento={datosBusqueda}
                                    valor_ini="*** Area ***"
                                    valor={formulario.cod_subarea}
                                />
                                <span className="input-group-text fw-bold">Fecha Doc:</span>
                                <input type="date" min="2020-01-01" name="fecha_doc" className="form-control" onChange={datosBusqueda} value={formulario.fecha_doc} />
                            </div>

                        </div>
                    </div>
                </div>
                {/* FIN BARRA DE NAVEGACION */}

                <div className="p-0 card-body">
                    <div className="mx-1 my-2 row">
                        <div className="col-xl-4 col-sm-4 p-1">
                            <div className="input-group">
                                <input type="text" className="form-control" placeholder="N° Fabricación SAP" onChange={datosBusqueda} value={formulario.num_of_sap} name="num_of_sap" />
                                <button className="btn btn-primary" onClick={() => mostrarVentana("prd")}><BsSearch /></button>
                            </div>
                        </div>
                        <div className="col-xl-4 col-sm-4 p-1">
                            <div className="input-group">
                                <input type="text" className="form-control" placeholder="Artículo" onChange={datosBusqueda} value={formulario.articulo} name="articulo" />
                                <button className="btn btn-primary" onClick={() => mostrarVentana("art")}><BsSearch /></button>
                            </div>
                        </div>
                        <div className="col-xl-4 col-sm-4 p-1">
                            <div className="input-group">
                                <input type="text" className="form-control" placeholder="Colaborador" onChange={datosBusqueda} value={formulario.empleado} name="empleado" />
                                <button className="btn btn-primary" onClick={() => mostrarVentana("emp")}><BsSearch /></button>
                            </div>
                        </div>
                        <div className="col-xl-4 col-sm-4 p-1">
                            <ComboBoxForm
                                datosRow={listTurno}
                                nombre_cbo="cod_turno"
                                manejaEvento={datosBusqueda}
                                valor_ini="*** Turno ***"
                                valor={formulario.cod_turno}
                            />
                        </div>
                        <div className="col-xl-4 col-sm-4 p-1">
                            <div className="input-group">
                                <span className="input-group-text">Hora Ini:</span>
                                <input type="time" style={{ minWidth: '75px' }} name="hora_ini" className="form-control" onChange={datosBusqueda} value={formulario.hora_ini} />
                            </div>
                        </div>
                        <div className="col-xl-4 col-sm-4 p-1">
                            <div className="input-group">
                                <span className="input-group-text">Hora Fin:</span>
                                <input type="time" style={{ minWidth: '75px' }} name="hora_fin" className="form-control" onChange={datosBusqueda} value={formulario.hora_fin} />
                            </div>
                        </div>
                    </div>
                    <div className="mx-0 my-2 py-2 px-1 row border border-dashed border-end-0 border-start-0 bg-light">
                        <div className="col-sm p-1">
                            <ComboBoxForm
                                datosRow={listPaso}
                                nombre_cbo="id_paso_equipo"
                                manejaEvento={datosBusqueda}
                                valor_ini="*** Paso/Estación/Maquina ***"
                                valor={formulario.id_paso_equipo}
                            />
                        </div>
                        <div className="col-sm-auto p-1">
                            <div className="d-flex flex-row-revers">
                                <button className="btn btn-warning mx-1" onClick={() => mostrarVentana("det")}><BsPlusLg /> Agregar</button>
                                <BotonGrabar textoBoton={"Guardar"} eventoClick={handleSubmit} />
                                {
                                    /*
                                    <BotonGrabar textoBoton={`${sw_modo === "C" ? 'Crear' : 'Actualizar'}`} eventoClick={handleSubmit} />
                                    */
                                }
                            </div>
                        </div>
                    </div>

                    { /* INICIO TABLA */}
                    <EficienciaOperativa_crud_tabla datos_fila={formDet.filter(det => det.cod_tipo_registro.toString() != "5")} eventoClick={deleteItem} />

                    {/* FIN TABLA */}

                    <div className="mx-1 my-2 row">
                        <div className="col-xl-4 col-sm-4 p-1">
                            <div className="table-responsive">
                                <table className="table table-hover table-bordered">
                                    <tbody>
                                        <tr>
                                            <td className="align-middle" title="Estandar unidades por hora" >Estandar Und.X.Hora:</td>
                                            <td className="td-num" title="Unidad de medida de uso" style={{ width: '70px' }}>{formulario.std_und_hora}</td>
                                        </tr>
                                        <tr>
                                            <td className="align-middle" title="Producción total (Unidades)" >Producción Total (Und)</td>
                                            <td className="td-num" title="Unidad de medida de uso" style={{ width: '70px' }}>{formulario.produccion_total}</td>
                                        </tr>
                                        <tr>
                                            <td className="align-middle" title="Horas Planificadas (Minutos)" >Horas Planificadas (Min):</td>
                                            <td className="td-num" title="Valor en Minutos" style={{ width: '70px' }}>{formulario.hora_plan}</td>
                                        </tr>
                                        <tr>
                                            <td className="align-middle" title="Horas Operativos (Minutos)" >Horas Operativos (Min):</td>
                                            <td className="align-middle td-num" title="Valor en Minutos" style={{ width: '70px' }}>{formulario.hora_operativo}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="col-xl-8 col-sm-8 p-1">
                            <EficienciaOperativa_crud_oee datos_fila={formDet.filter(det => det.cod_tipo_registro.toString() === "5")} eventoClick={mostrarVentana} />
                        </div>
                    </div>

                </div>
            </div>

            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="lg"
                handleClose={handleClose}
                titulo={tituloModal}
            >
                {
                    tipoModal === 'det' ? <EficienciaOperativa_crud_detalle eventoClick={eventoAddDetalle} iniFormDet={iniFormulario_det} cod_subarea={formulario.cod_subarea} item_oee={numOEEItem} /> :
                        tipoModal === 'prd' ? <OrdenFabricacionBuscar eventoClick={eventoSeleccionarOf} cod_subarea={formulario.cod_subarea} /> :
                            tipoModal === 'art' ? <ArticuloFamiliaBuscar eventoClick={eventoSeleccionarArticulo} cod_subarea={formulario.cod_subarea} /> :
                                tipoModal === 'emp' ? <EmpleadoBuscar eventoClick={eventoSeleccionarEmpleado} cod_subarea={formulario.cod_subarea} /> :
                                    tipoModal === 'oee' ? <EficienciaOperativa_crud_oee_detalle eventoClick={eventoUpdateDetalleOee} cod_subarea={formulario.cod_subarea} datosModal={datosModal} /> : <></>
                }
            </VentanaModal>
            {/* FIN MODAL */}

            {/* seguridad */}
            <VentanaBloqueo show={VentanaSeguridad} />

        </div >
    );
}