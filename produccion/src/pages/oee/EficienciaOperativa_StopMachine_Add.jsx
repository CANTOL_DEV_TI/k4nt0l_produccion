import React from "react";
import {get_oee_causa} from "./services/oeeCausaServices.jsx";
import {ComboBoxForm2} from "../../components/ComboBox.jsx";
import {get_oee_motivo_causa} from "./services/oeeMotivoCausaServices.jsx";
import {BsPlusLg} from "react-icons/bs";

class EficienciaOperativa_StopMachine_Add extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            listCausa: [],
            listMotivosCausa: [],

            cod_causa: '0',
            causa: '',

            cod_motivo_causa: 0,
            motivo: '',
            tiempo_min: '',

            cod_subarea: 0,
            itemTable: 0
        }

    }



    async FindCausa (cod_tipo_registro) {
        const responseJson = await get_oee_causa(cod_tipo_registro, "0", "1")

        let responseStandarizado = []
        responseJson.map(data => {
                responseStandarizado.push({"codigo": data.cod_causa, "nombre": data.causa})
            })

        this.setState({listCausa: responseStandarizado})
    }

    async FindMotivoCausa (cod_causa) {
        const responseJson = await get_oee_motivo_causa(this.props.cod_subarea, cod_causa, "1")

        let responseStandarizado = []
        responseJson.map(data => {
                responseStandarizado.push({"codigo": data.cod_motivo_causa, "nombre": data.motivo_causa})
            })

        this.setState({listMotivosCausa: responseStandarizado})

        // console.log(responseStandarizado)

    }

    HandleChange = (e) =>{
        if (e.target.name === 'cod_causa'){
            const c_res = this.state.listCausa.filter((item) => item.codigo == e.target.value);
            this.setState({cod_causa: c_res.length>0 ? e.target.value : '0', causa: c_res.length>0 ? c_res[0].nombre : ''})
            this.FindMotivoCausa(e.target.value)

        }

        if (e.target.name === 'cod_motivo_causa'){
            const c_res = this.state.listMotivosCausa.filter((item) => item.codigo == e.target.value);
            console.log(c_res)

            this.setState({cod_motivo_causa: c_res.length>0 ? e.target.value : '0', motivo: c_res.length>0 ? c_res[0].nombre : ''})


        }

        if (e.target.name === 'tiempo_min'){
            const newValor = (e.target.validity.valid) ? e.target.value : this.state.tiempo_min
            this.setState({tiempo_min: newValor})
        }

    }


    HandleSubmit  = () =>{
        let dataEdit = {
            cod_causa: this.state.cod_causa,
            causa: this.state.causa,
            cod_motivo_causa: this.state.cod_motivo_causa,
            motivo: this.state.motivo,
            tiempo_min: this.state.tiempo_min
        }

        this.props.eventoClick(dataEdit)
    }


    render() {
        return(

            <div style={{alignItems: "center", justifyContent: "center", display: "flex"}}>
                <div className="mx-1 my-2 row text-center" style={{padding: "0"}}>
                    <div className="col-xl-12 col-sm-12 p-1">
                        <div className="input-group">
                            <span className="input-group-text">Causa:</span>
                            <ComboBoxForm2
                                datosRow={this.state.listCausa}
                                nombre_cbo="cod_causa"
                                manejaEvento={this.HandleChange}
                                valor_ini="*** Causa (7m) ***"
                                valor={this.state.cod_causa}
                            />
                        </div>
                    </div>
                    <div className="col-xl-12 col-sm-12 p-1">
                        <div className="input-group">
                            <span className="input-group-text">Motivo:</span>
                            <ComboBoxForm2
                                datosRow={this.state.listMotivosCausa}
                                nombre_cbo="cod_motivo_causa"
                                manejaEvento={this.HandleChange}
                                valor_ini="*** Motivo Causa ***"
                                valor={this.state.cod_motivo_causa}
                            />
                        </div>

                    </div>

                    <div
                        className={this.state.cod_tipo_registro === 3 ? "col-xl-4 col-sm-4 p-1" : "col-xl-12 col-sm-12 p-1"}>
                        <div className="input-group">
                            <input
                                type="number"
                                name="tiempo_min"
                                placeholder="Tiempo"
                                onChange={this.HandleChange}
                                className="form-control"
                                value={this.state.tiempo_min}
                                pattern="[0-9]*"
                            />
                            <span className="input-group-text">Min</span>
                        </div>
                    </div>

                    <div className="col-xl-12 col-sm-12 p-1">
                        <button
                            className="form-control btn btn-primary"
                            onClick={this.HandleSubmit}
                        >
                            <BsPlusLg/>
                            Agregar
                        </button>
                    </div>

                </div>
            </div>

        )
    }


    async componentDidMount() {
        this.FindCausa("1")
    }

}

export default EficienciaOperativa_StopMachine_Add;

