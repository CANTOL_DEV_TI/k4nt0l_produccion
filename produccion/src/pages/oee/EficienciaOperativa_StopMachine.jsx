import React from "react";
import {BsPlusLg, BsSearch} from "react-icons/bs";

import {ComboBoxForm2} from "../../components/ComboBox";
import {NavLink} from "react-router-dom";
import {get_subarea} from "./services/oeeSubAreaServices.jsx";
import {
    find_oeeEficiencia,
    get_StopMachine,
    insert_oeeEficiencia,
    insert_oeeStopMachine,
    update_oeeStopMachine,
    Get_StopMachine_SubArea
} from "./services/oeeEficienciaServices.jsx";
import {EficienciaOperativa_crud_tabla} from "./components/EficienciaOperativa_crud_tabla.jsx";
import EficienciaOperativa_Tabla from "./components/EficienciaOperativa_Tabla.jsx";
import {EficienciaOperativo_Tabla} from "./components/EficienciaOperativo_Tabla.jsx";
import LectorQR from "./components/lectorQR.jsx";
import EficienciaOperativa_Edit_Add from "./EficienciaOperativa_Edit_Add.jsx";
import {EmpleadoBuscar} from "../empleado/EmpleadoBuscar.jsx";
import {OrdenFabricacionBuscar} from "../ordenFabricacionPrograma/OrdenFabricacionBuscar.jsx";
import EficienciaOperativa_Indicadores_Edit from "./components/EficienciaOperativa_Indicadores_Edit.jsx";
import VentanaModal from "../../components/VentanaModal.jsx";
import EficienciaOperativa_StopMachine_Add from "./EficienciaOperativa_StopMachine_Add.jsx";
import {BotonGrabar, BotonGrabar2} from "../../components/Botones.jsx";
import {get_periodo_laboral_turno} from "./services/oeeTurnoServices.jsx";


const v_fecha = (new Date()).toLocaleDateString('en-GB', {year: 'numeric', month: '2-digit', day: '2-digit',}).split('/').reverse().join('-');

class EficienciaOperativa_StopMachine extends React.Component{

    constructor() {
        super();

        this.state ={
            showModal: {estado: false, tipo: ""},
            fecha: v_fecha,
            txt_empleado: '',
            listArea: [],
            listTurno: [],

            isFetch: true,
            resultados: [],

            id_oee: 0,

            cod_subarea: null,
            detalle_formulario: [],

            tipoOperacion: 'Grabar',

            cod_turno: 0,

            itemTableEdit: 0,
            Maquina: '',

            op_OK: true
        }
    }
    // =====================================================================
    handleFindSubArea = async (cod_usuario) =>{
        console.log(cod_usuario)
        const responseJson = await Get_StopMachine_SubArea(cod_usuario)
        console.log(responseJson)
        this.setState({listArea: responseJson, cod_subarea: responseJson[0].codigo})
        this.handleFinTurno(responseJson[0].codigo)
    }

    handleFinTurno = async (cod_subarea) =>{
        const responseJson = await get_periodo_laboral_turno(cod_subarea)
        this.setState({listTurno: responseJson})
    }

    // =====================================================================

    EditItem = (item, maquina) =>{
        this.setState({
            showModal: {estado: true, tipo: "det"},
            itemTableEdit: item,
            Maquina: maquina
        })

        console.log(item)
    }


    DeleteItem = (item) =>{
        console.log(item)
        this.state.resultados[item]["cod_causa"]= 0
        this.state.resultados[item]["causa"]= ""
        this.state.resultados[item]["cod_motivo_causa"]= 0
        this.state.resultados[item]["motivo_causa"]= ""
        this.state.resultados[item]["tiempo_min"]= 0
        this.state.resultados[item]["item"]= null
        this.state.resultados[item]["fecha_doc"]= null
        this.state.resultados[item]["id_oee"]= null
        this.state.resultados[item]["cod_empleado"]= ""
        this.state.resultados[item]["cod_turno"]= 0


        this.setState({itemTableEdit: item})

    }

    // =====================================================================
    handleChange =(e)=>{
        if (e.target.name === 'txt_empleado'){
            this.setState({txt_empleado: e.target.value})


            if (e.target.value.trim().length==8){
                console.log("tutuu")
                console.log(e.target.value.trim())
                console.log(e.target.value.trim().length)
                this.handleFindSubArea(e.target.value)

            }

        }

        if (e.target.name === 'cod_turno'){
            this.setState({cod_turno: e.target.value})
        }


        if (e.target.name === 'fecha_doc') {
            this.setState({fecha: e.target.value})
        }


    }

    handleBusqueda = async() =>{
        if (this.state.txt_empleado===''){
            return
        }
        const responseJson = await get_StopMachine(this.state.txt_empleado, this.state.fecha, this.state.cod_subarea, this.state.cod_turno)



        this.setState({
            resultados: responseJson.resultados,
            // cod_subarea: responseJson.cod_subarea,
            id_oee: responseJson.id_oee,
            isFetch: false
        })




        console.log("%%%%%%")
        console.log(responseJson)
        console.log("%%%%%%")
    }

    handleEditItem = (dataEdit) =>{

        let ListaErrores = {
            cod_causa: 'Causa',
            causa: 'Causa',
            cod_motivo_causa: 'Motivo',
            motivo: 'Motivo',
            tiempo_min: 'Tiempo'
        }

        for(const [key, value] of Object.entries(dataEdit)){
            if (value.toString().trim()==='0' || value.toString().trim()===''){
                alert("Error en " + ListaErrores[key])
                return
            }
        }

        console.log("=====================")
        console.log(this.state.itemTableEdit)
        console.log(dataEdit)
        this.state.resultados[this.state.itemTableEdit]["cod_causa"]= dataEdit.cod_causa
        this.state.resultados[this.state.itemTableEdit]["causa"]= dataEdit.causa
        this.state.resultados[this.state.itemTableEdit]["cod_motivo_causa"]= dataEdit.cod_motivo_causa
        this.state.resultados[this.state.itemTableEdit]["motivo_causa"]= dataEdit.motivo
        this.state.resultados[this.state.itemTableEdit]["tiempo_min"]= dataEdit.tiempo_min
        this.state.resultados[this.state.itemTableEdit]["item"]= this.state.itemTableEdit

        console.log(this.state.resultados[this.state.itemTableEdit])
        console.log(this.state.resultados)
        console.log("=====================")

        this.setState({showModal: {estado: false, tipo: ""}})

    }

    // ####################################
    handleSave = async ()=>{
        const c_res = this.state.resultados.filter((item) => item.cod_causa !== 0);

        let det = []
        c_res.map((data, index) => {
            det.push({
                item: data.item,
                id_oee: 0,
                tiempo_min: data.tiempo_min,
                id_equipo: data.id_equipo,
                cod_causa: data.cod_causa,
                cod_motivo_causa: data.cod_motivo_causa
            })
        })

        let cab = {
            id_oee: 0,
            fecha_doc: this.state.fecha + " 00:00:00",
            cod_empleado: this.state.txt_empleado,
            cod_turno: this.state.cod_turno,
            cod_subarea: this.state.cod_subarea,
            detalle: det
        }


        let ListaErrores = {
            "fecha_doc": "Fecha Emision",
            "cod_empleado": "DNi",
            "cod_turno": "Turno",
            "cod_subarea": "Sub Area"
        }

        for(const [key, value] of Object.entries(cab)){

            if (key==='detalle' && value.length<=0){
                alert("Error en " + ListaErrores[key])
                return
            }

            if (value.toString().trim()==='0' && key!='id_oee'){
                alert("Error en " + ListaErrores[key])
                return
            }

        }


        this.setState({op_OK: false})
        const res = await insert_oeeStopMachine(cab)
        alert("Grabado Satisfactoriamente")

        console.log(cab)

    }

    handleUpdate = async ()=>{
        const c_res = this.state.resultados.filter((item) => item.cod_causa !== 0);

        let det = []
        c_res.map((data, index) => {
            det.push({
                item: data.item,
                id_oee: 0,
                tiempo_min: data.tiempo_min,
                id_equipo: data.id_equipo,
                cod_causa: data.cod_causa,
                cod_motivo_causa: data.cod_motivo_causa
            })
        })

        let cab = {
            id_oee: this.state.id_oee,
            fecha_doc: this.state.fecha + " 00:00:00",
            cod_empleado: this.state.txt_empleado,
            cod_turno: this.state.cod_turno,
            cod_subarea: this.state.cod_subarea,
            detalle: det
        }



        let ListaErrores = {
            "id_oee": "no existe",
            "fecha_doc": "Fecha Emision",
            "cod_empleado": "DNi",
            "cod_turno": "Turno",
            "cod_subarea": "Sub Area"
        }

        for(const [key, value] of Object.entries(cab)){

            if (key==='detalle' && value.length<=0){
                alert("Error en " + ListaErrores[key])
                return
            }

            if (value.toString().trim()==='0'){
                alert("Error en " + ListaErrores[key])
                return
            }

        }

        this.setState({op_OK: false})
        const res = await update_oeeStopMachine(cab)
        alert("Actualizado Satisfactoriamente")

        console.log(cab)
    }

    // ####################################



    render() {
        return(
            <React.Fragment>
                <div className="col-lg-12">
                    <div className="card">
                        <div className="card-header border-0 p-0">
                            <div className="mx-1 my-2 row">
                                <div className="col-xl-6 col-sm-6 p-1" style={{minWidth: "50%"}}>
                                    <h4 className="card-title mb-0">Registro Maquinas Detenidas</h4>
                                </div>

                                <div className="col-xl-6 col-sm-6 p-1" style={{minWidth: "50%"}}>
                                    <div className="input-group">

                                        <span className="input-group-text fw-bold">Fecha:</span>
                                        <input type="date" value={this.state.fecha} name="fecha_doc"
                                               onChange={this.handleChange} className="form-control"
                                               style={{width: "100px", padding: "0 1px"}} disabled={false}
                                        />

                                        <input placeholder="Ingrese Dni" onChange={this.handleChange}
                                               value={this.state.txt_empleado} name="txt_empleado"
                                               style={{marginLeft: "10px", marginRight: "10px", width: "100px"}}
                                        />

                                        <ComboBoxForm2
                                            nombre_cbo="cod_subarea"
                                            valor_ini="*** Area ***"
                                            datosRow={this.state.listArea}
                                            manejaEvento={this.handleChange}
                                            valor={this.state.cod_subarea}
                                            view={true}
                                        />

                                        <ComboBoxForm2
                                            datosRow={this.state.listTurno}
                                            nombre_cbo="cod_turno"
                                            manejaEvento={this.handleChange}
                                            valor_ini="*** Turno ***"
                                            valor={this.state.cod_turno}
                                        />


                                        <button
                                            className="btn btn-primary"
                                            onClick={this.handleBusqueda}
                                            style={{marginRight: "10px"}}
                                        >
                                            <BsSearch/>
                                        </button>


                                    </div>

                                </div>

                            </div>

                            <EficienciaOperativo_Tabla
                                datos_fila={this.state.resultados}
                                eventoClick={this.EditItem}
                                eventoDelete={this.DeleteItem}
                            />

                            <div className="col-xl-12 col-sm-12 p-1">
                                <div className="col-sm-auto p-1">
                                    <div className="d-flex flex-row-revers" style={{
                                        alignItems: "center",
                                        justifyContent: "center",
                                        display: "flex",
                                        margin: "5px 0"
                                    }}>
                                        {this.state.id_oee == '0' ?
                                            <BotonGrabar2
                                                textoBoton={"Grabar"}
                                                eventoClick={this.handleSave}
                                                sw_habilitado={this.state.op_OK}
                                                style={{width: "45%", height: "70px"}}
                                            />
                                            :
                                            <BotonGrabar2
                                                textoBoton={"Guardar\nCambios"}
                                                eventoClick={this.handleUpdate}
                                                sw_habilitado={this.state.op_OK}
                                                style={{width: "40%", height: "70px"}}
                                            />
                                        }


                                        <NavLink
                                            className='btn btn-secondary  mx-1'
                                            to="/oee"
                                            style={{width: "45%", height: "70px"}}
                                        >
                                            Terminar
                                        </NavLink>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <VentanaModal
                    show={this.state.showModal.estado}
                    size="lg"
                    handleClose={() => this.setState({showModal: {estado: false, tipo: ""}})}
                    titulo={"Edicion " + this.state.Maquina}>
                    {
                        this.state.showModal.tipo === 'det' ? <EficienciaOperativa_StopMachine_Add eventoClick={this.handleEditItem} cod_subarea={this.state.cod_subarea}/> : <></>
                    }

                </VentanaModal>
            </React.Fragment>
        )
    }

    async componentDidMount() {
        // this.handleFindSubArea()
    }

}

export default EficienciaOperativa_StopMachine;






