import { Url } from '../../../constants/global.jsx'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

const url = `${meServidorBackend}/produccion/oee`;


// =================================================================================================
export async function Get_OrdenFabricacion(cod_subarea, nroOrdenFab ){
    // if (nroOrdenFab.trim().length <= 4){
    //     return []
    // }

    if (nroOrdenFab.trim().length <= 0){
        nroOrdenFab = "%20"
    }

    let url_id = `${url}/finOrdenFabricacion/${cod_subarea}/${nroOrdenFab}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()

    let responseStandarizado = []
        responseJson.map(data => {
            responseStandarizado.push({
                "codigo": data.num_of.toString(),
                "nombre": data.num_of + " = (" + data.articulo.toString() + " - " + data.estado_desc + ")",
                "num_of": data.num_of,
                "cod_articulo": data.cod_articulo,
                "articulo": data.articulo
            })
        })

    return responseStandarizado


    // return responseJson
}
