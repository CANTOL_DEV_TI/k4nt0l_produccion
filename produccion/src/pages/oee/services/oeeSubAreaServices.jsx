import { Url } from '../../../constants/global.jsx'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url


const url = `${meServidorBackend}/produccion/subarea`;


export async function get_subarea(cod_usuario) {
    let url_id = `${url}/subarea_usuario/?cod_usuario=${cod_usuario}`;
    console.log("XXXXXXXXXXXXXXXX")
    console.log(url_id)

    const response = await fetch(url_id);
    const responseJson = await response.json()

    let responseStandarizado = []
    responseJson.map(data => {
            responseStandarizado.push({"codigo": data.cod_subarea, "nombre": data.nom_subarea})
        })

    return responseStandarizado
}




