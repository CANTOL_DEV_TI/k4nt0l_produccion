import { Url } from '../../../constants/global.jsx'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url


const url = `${meServidorBackend}/produccion/oee_indicadores_motivo`;


export async function get_indicadores_motivos(id_config_oee, cod_subarea) {
    let url_id = `${url}/${id_config_oee}/${cod_subarea}`;

    const response = await fetch(url_id);
    const responseJson = await response.json()


    let responseStandarizado = []
    responseJson.map(data => {
            responseStandarizado.push({"codigo": data.cod_motivo_indicador, "nombre": data.indicador_motivo})
        })


    return responseStandarizado
}