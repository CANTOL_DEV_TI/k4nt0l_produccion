import { Url } from '../../../constants/global'

const meServidorBackend = Url
const url = `${meServidorBackend}/produccion/configproceso`;
//const meServidorBackend = 'http://192.168.2.148:8000'
//const meServidorBackend = 'http://192.168.5.21:8090'

export async function get_config_proceso_det_articulo(cod_articulo = "") {
    let url_id = `${url}/det_articulo/?cod_articulo=${cod_articulo}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()

    let responseStandarizado = []
        responseJson.map(data => {
            responseStandarizado.push(
                {"codigo": data.id_equipo.toString()+data.num_paso.toString(),
                    "nombre": data.num_paso + ": " + data.equipo + " - " + data.tipo_operacion,
                    "num_paso": data.num_paso,
                    "id_equipo": data.id_equipo,
                    "id_tpoperacion": data.id_tpoperacion,
                    "std_und_hora": data.std_und_hora
                })

            // responseStandarizado.push(
            //     {"id_paso_equipo": data.id_equipo.toString()+data.num_paso.toString(),
            //         "nom_paso_equipo": data.num_paso + ": " + data.equipo + " - " + data.tipo_operacion,
            //         "num_paso": data.num_paso,
            //         "id_equipo": data.id_equipo,
            //         "id_tpoperacion": data.id_tpoperacion,
            //         "std_und_hora": data.std_und_hora
            //     })
        })

    return responseStandarizado
}



