import { Url } from '../../../constants/global.jsx'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

const url = `${meServidorBackend}/produccion/periodo_laboral`;
const url2 = `${meServidorBackend}/produccion/turno`;

export async function get_periodo_laboral_turno(cod_subarea) {
    let url_id = `${url}/turno?cod_subarea=${cod_subarea}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()

    let responseStandarizado = []
    responseJson.map(data => {
            responseStandarizado.push({"codigo": data.cod_turno, "nombre": data.nom_turno})
        })

    return responseStandarizado
}

export async function get_turno_det(cod_turno) {
    let url_id = `${url2}/${cod_turno}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}




