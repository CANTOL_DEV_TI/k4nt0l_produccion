import { Url } from '../../../constants/global.jsx'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

const url = `${meServidorBackend}/produccion/oee`;

export async function get_RegistroOee_Excel(cod_subarea = "0", fecha_ini = "0", fecha_fin = "0") {
    let url_id = `${url}/excel/${cod_subarea}/${fecha_ini}/${fecha_fin}`;
    console.log(url_id)
    const response = await fetch(url_id)
    const responseJson = await response.json()
    console.log(responseJson)
    return responseJson
}
 

