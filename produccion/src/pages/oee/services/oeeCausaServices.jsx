import { Url } from '../../../constants/global.jsx'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

const url = `${meServidorBackend}/produccion/oee_causa`;

export async function get_oee_causa(cod_tipo_registro = "0", cod_subarea = "0", sw_estado = "0") {
    let url_id = `${url}?cod_tipo_registro=${cod_tipo_registro}&cod_subarea=${cod_subarea}&sw_estado=${sw_estado}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function insert_oee_causa(data) {
    try {
        const controller = new AbortController();
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url, options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function update_oee_causa(data) {
    try {
        const controller = new AbortController();
        //let url_id = `${url}/${data.id_per_lab}`; 
        //delete data.id_per_lab; 
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "PUT",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url, options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        //console.error("Error:", error); 
        return { "Error": error };
    }
}

export async function delete_oee_causa(id_per_lab) {
    try {
        let isDelete = window.confirm(
            `¿Estás seguro de eliminar el registro con el id '${id_per_lab}'?`
        );
        if (!isDelete) {
            return;
        }

        const controller = new AbortController();
        let url_id = `${url}/${id_per_lab}`;
        let options = {
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "DELETE",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
} 