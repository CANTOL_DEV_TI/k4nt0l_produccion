import { Url } from '../../../constants/global.jsx'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

const url = `${meServidorBackend}/produccion/oee`;


// =================================================================================================
export async function Get_StopMachine_SubArea(cod_usuario){
    if (cod_usuario.trim()===''){
        cod_usuario='%20'
    }

    let url_id = `${url}/stop_machine/subarea/${cod_usuario}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()

    let responseStandarizado = []
        responseJson.map(data => {
            responseStandarizado.push({
                "codigo": data.cod_subarea.toString(),
                "nombre": data.nom_subarea.toString()
            })
        })

    return responseStandarizado


    // return responseJson
}


export async function update_oeeStopMachine(data) {
    try {
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTTTTTT")
        console.log(data)
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTTTTTT")
        const controller = new AbortController();
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "PUT",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 5000);

        const response = await fetch(url + "/stop_machine/", options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function insert_oeeStopMachine(data) {
    try {
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTTTTTT")
        console.log(data)
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTTTTTT")
        const controller = new AbortController();
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 5000);

        const response = await fetch(url + "/stop_machine/", options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}


export async function get_StopMachine(dni_empleado, fecha, cod_subarea, cod_turno){
    if (dni_empleado.trim()===''){
        dni_empleado='%20'
    }

    let url_id = `${url}/lista_stop_machine/${dni_empleado}/${fecha}/${cod_subarea}/${cod_turno}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    console.log(url_id)
    return responseJson

}


export async function get_CausasDefault(){
    let url_id = `${url}/fin_causasdefault`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function get_OnlyMaquinas(cod_subarea){
    let url_id = `${url}/find_onlymaquinas/${cod_subarea}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()

    let responseStandarizado = []
        responseJson.map(data => {
            responseStandarizado.push({
                "codigo": data.id_equipo.toString(),
                "nombre": data.equipo.toString()
            })
        })

    return responseStandarizado
}

export async function get_Maquinas_x_Paso(cod_articulo = "", nropaso=0, cod_tipo_proceso=0) {
    let url_id = `${url}/find_maquinas/${cod_articulo}/${nropaso}/${cod_tipo_proceso}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()

    let responseStandarizado = []
        responseJson.map(data => {
            responseStandarizado.push({
                "codigo": data.id_equipo.toString(),
                "nombre": data.num_prioridad.toString()+" - "+data.equipo.toString(),
                "id_tpoperacion": data.id_tpoperacion.toString(),
                "std_und_hora": data.std_und_hora.toString()
            })
        })

    return responseStandarizado
}


export async function get_NumPaso(cod_articulo = "", id_equipo="") {
    let url_id = `${url}/find_pasos/${cod_articulo}/${id_equipo}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()

    let responseStandarizado = []
        responseJson.map(data => {
            responseStandarizado.push({
                "codigo": data.num_paso.toString()+" - "+data.id_tpoperacion.toString(),
                "nombre": data.num_paso.toString()+" - "+data.tipo_operacion.toString(),
                "num_paso": data.num_paso.toString(),
                "operacion": data.tipo_operacion.toString(),
                "id_tpoperacion": data.id_tpoperacion.toString(),
                "std_und_hora": data.std_und_hora.toString(),

            })
        })

    return responseStandarizado
}

export async function find_oeeEficiencia(fecha, tipo_filtro, cod_empleado){
    let url_id = `${url}/findcab/${fecha}/${tipo_filtro}/${cod_empleado}`;
    console.log("&&&&&&&")
    console.log(url_id)
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function find_oee_EfienciaDet(id_oee){
    let url_id = `${url}/finddet/${id_oee}`;
    console.log("&&&&&&&")
    console.log(url_id)
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

// =================================================================================================

export async function get_oeeEficiencia() {
    const response = await fetch(url)
    const responseJson = await response.json()
    return responseJson
}

export async function get_oeeEficiencia_det(cod_oeeEficiencia) {
    let url_id = `${url}/det/${cod_oeeEficiencia}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function insert_oeeEficiencia(data) {
    try {
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTTTTTT")
        console.log(data)
        console.log("TTTTTTTTTTTTTTTTTTTTTTTTTTTTT")
        const controller = new AbortController();
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 5000);

        const response = await fetch(url, options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function update_oeeEficiencia(data) {
    try {
        const controller = new AbortController();
        //let url_id = `${url}/${data.cod_oeeEficiencia}`;
        //delete data.cod_oeeEficiencia;
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "PUT",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url, options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function delete_oeeEficiencia(cod_oeeEficiencia) {
    try {
        let isDelete = window.confirm(
            `¿Estás seguro de eliminar el registro con el id '${cod_oeeEficiencia}'?`
        );
        if (!isDelete) {
            return;
        }

        const controller = new AbortController();
        let url_id = `${url}/${cod_oeeEficiencia}`;
        let options = {
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "DELETE",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
}

