import React from "react";
import EficienciaOperativa_Edit from "./EficienciaOperativa_Edit.jsx";

import {get_oee_tipo_registro} from "./services/oeeTipoRegistroServices"
import {get_oee_causa} from "./services/oeeCausaServices"
import {get_oee_motivo_causa} from "./services/oeeMotivoCausaServices"
import {get_RegistrosEmpleado} from "./services/oeeEmpleadosServices"
import {Get_OrdenFabricacion} from "./services/oeeOrdenFabricacionServices"

import {ComboBoxForm, ComboBoxForm2} from "../../components/ComboBox.jsx";
import {BsPlusLg, BsSearch} from "react-icons/bs";
import {get_NumPaso} from "./services/oeeEficienciaServices.jsx";


class EficienciaOperativa_Edit_Add extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            listOrdenFab: [],
            listPasosProcesos: [],

            listTipo: this.props.listTipo,
            listCausa: [],
            listMotivosCausa: [],

            item_oee: 0,
            cod_tipo_registro: 0,
            tipo_registro: '',
            umu: '',
            cod_causa: 0,
            causa: '',
            cod_motivo_causa: 0,
            motivo_causa: '',
            cantidad_prod_buena: 0,
            detalle: '',
            num_vale: '',

            cantidad_prod_mala: 0,
            cantidad_total: 0,
            tiempo_min: 0,

            sw_horario: "0",
            hora_ini: '00:00',
            hora_fin: '00:00',


            cod_subarea: this.props.cod_subarea,

            cod_empleado: typeof this.props.DataTemp === 'undefined' ? '' : this.props.DataTemp.TempAdd_cod_empleado,
            nom_empleado: typeof this.props.DataTemp === 'undefined' ? '' : this.props.DataTemp.TempAdd_nom_empleado,


            INPUT_nroOrdenFab: typeof this.props.DataTemp === 'undefined' ? '' : this.props.DataTemp.TempAdd_num_of_sap,

            num_of_sap: typeof this.props.DataTemp === 'undefined' ? '' : this.props.DataTemp.TempAdd_num_of_sap,
            articulo: typeof this.props.DataTemp === 'undefined' ? '' : this.props.DataTemp.TempAdd_articulo,
            cod_articulo: typeof this.props.DataTemp === 'undefined' ? null : this.props.DataTemp.TempAdd_cod_articulo,



            std_und_hora: typeof this.props.DataTemp === 'undefined' ? '' : this.props.DataTemp.TempAdd_std_und_hora,
            id_tpoperacion: typeof this.props.DataTemp === 'undefined' ? '' : this.props.DataTemp.TempAdd_id_tpoperacion,
            num_paso: typeof this.props.DataTemp === 'undefined' ? '' : this.props.DataTemp.TempAdd_num_paso,


            cod_PasoTemp: typeof this.props.DataTemp === 'undefined' ? 0 : this.props.DataTemp.TempAdd_num_paso+" - "+this.props.DataTemp.TempAdd_id_tpoperacion,


            rendimiento_porcentaje: 0,
            rendimiento_porcentaje_color: "red",


        }


    }


    get_ColorRendimiento = (P_Rendimiento) => {
        switch (true) {
            case P_Rendimiento < 75:
                return "red"

            case (75 < P_Rendimiento &&  P_Rendimiento <= 85):
                return "Yellow"

            case (85 < P_Rendimiento && P_Rendimiento <= 100):
                return "green"

            case (P_Rendimiento > 100):
                return "black"
        }
    }


    calcularTiempo = (p_HoraIni, p_HoraFin) =>{
        let p_FechaIni = new Date(`2024-02-26 ${p_HoraIni}:00`)
        let p_FechaFin = new Date(`2024-02-26 ${p_HoraFin}:00`)

        let c_minuto = 0;
        if (p_FechaFin > p_FechaIni) {
            c_minuto = Math.round((p_FechaFin - p_FechaIni) / 60000);
        }

        const P_Rendimiento = Number(this.state.std_und_hora) > 0 ? Math.round(this.state.cantidad_total / ((Number(this.state.std_und_hora) / 60) * Number(c_minuto))*100, 2) : 0;


        this.setState({
            tiempo_min: c_minuto,
            rendimiento_porcentaje: P_Rendimiento,
            rendimiento_porcentaje_color: this.get_ColorRendimiento(P_Rendimiento),
        })

    }

    async FindCausa (cod_tipo_registro) {
        const responseJson = await get_oee_causa(cod_tipo_registro, this.props.cod_subarea, "1")

        let responseStandarizado = []
        responseJson.map(data => {
                responseStandarizado.push({"codigo": data.cod_causa, "nombre": data.causa})
            })

        this.setState({listCausa: responseStandarizado})
    }

    async FindMotivoCausa (cod_causa) {
        // this.setState({listMotivosCausa: []})
        const responseJson = await get_oee_motivo_causa(this.props.cod_subarea, cod_causa, "1")

        let responseStandarizado = []
        responseJson.map(data => {
                responseStandarizado.push({"codigo": data.cod_motivo_causa, "nombre": data.motivo_causa})
            })

        this.setState({listMotivosCausa: responseStandarizado})
    }

    async FindEmpleado (cod_empleado) {
        const responseJson = await get_RegistrosEmpleado(0, cod_empleado, this.state.cod_subarea)
        this.setState({nom_empleado: responseJson[0].nombre})
        console.log(responseJson)
    }

    async FindOrdenFabricacion (nroOrdenFab) {
        console.log("????????????????????????????????????????????")
        console.log(this.state.cod_subarea)
        console.log(nroOrdenFab)
        console.log("????????????????????????????????????????????")
        const responseJson = await Get_OrdenFabricacion(this.state.cod_subarea, nroOrdenFab)
        console.log(responseJson)
        this.setState({listOrdenFab: responseJson})
        // 3925065
    }

    handleFindNumPaso = async (cod_articulo) =>{
        const responseJson = await get_NumPaso(cod_articulo, this.props.id_equipo)
        this.setState({listPasosProcesos: responseJson})
        console.log("PEPITOOOO")
        console.log(responseJson)
    }

    // ========================================================================================

    HandleChange = (e) =>{

        if (e.target.name === 'num_paso'){
            console.log(this.state.listPasosProcesos)
            let P_NroPaso = this.state.listPasosProcesos.filter(data => data.codigo.toString() === e.target.value)

            console.log("/777777777777")
            console.log(P_NroPaso)
            console.log(this.props.id_equipo)
            console.log("/777777777777")

            const P_Rendimiento = Number(P_NroPaso[0].std_und_hora) > 0 && Number(this.state.tiempo_min) > 0 ? Math.round(this.state.cantidad_total / ((Number(P_NroPaso[0].std_und_hora) / 60) * Number(this.state.tiempo_min))*100, 2) : 0;



            this.setState({
                num_paso: P_NroPaso[0].num_paso,
                std_und_hora: P_NroPaso[0].std_und_hora,
                id_tpoperacion: P_NroPaso[0].id_tpoperacion,
                cod_PasoTemp: e.target.value,

                rendimiento_porcentaje: P_Rendimiento,
                rendimiento_porcentaje_color: this.get_ColorRendimiento(P_Rendimiento),

            })

            console.log(this.state)
        }


        if (e.target.name === 'num_of') {
            let P_CodArticulo = this.state.listOrdenFab.filter(data => data.codigo.toString() === e.target.value)

            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            console.log(P_CodArticulo[0])
            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            this.setState({
                num_of_sap: P_CodArticulo[0].num_of,
                cod_articulo: P_CodArticulo[0].cod_articulo,
                articulo: P_CodArticulo[0].articulo
            })

            this.handleFindNumPaso(P_CodArticulo[0].cod_articulo)

        }

        if (e.target.name === 'nroOrdenFab'){

            this.setState({INPUT_nroOrdenFab: e.target.value})
        }

        if (e.target.name === 'cod_empleado'){
            this.setState({cod_empleado: e.target.value})
        }

        if (e.target.name === 'cod_causa'){
            let getCausa = this.state.listCausa.filter(data => data.codigo.toString() === e.target.value)

            var v_CodCausa = "0"
            var v_NomCausa = "*** Causa (7m) ***"
            if (getCausa.length > 0){
                v_CodCausa = e.target.value
                v_NomCausa = getCausa[0].nombre
                this.FindMotivoCausa(v_CodCausa)
            }
            this.setState({
                cod_causa: v_CodCausa,
                causa: v_NomCausa,
                listMotivosCausa: [],
                cod_motivo_causa: "0",
                motivo_causa: "*** Motivo Causa ***"
            })

        }


        if (e.target.name === 'cod_motivo_causa') {
            let getMotivoCausa = this.state.listMotivosCausa.filter(data => data.codigo.toString() === e.target.value)

            var v_CodMotivoCausa = "0"
            var v_NomMotivoCausa = "*** Motivo Causa ***"
            if (getMotivoCausa.length > 0){
                v_CodMotivoCausa = e.target.value
                v_NomMotivoCausa = getMotivoCausa[0].nombre
            }
            this.setState({
                cod_motivo_causa: v_CodMotivoCausa,
                motivo_causa: v_NomMotivoCausa
            })

        }


        if (e.target.name === 'cantidad_prod_buena'){
            const newValor = (e.target.validity.valid) ? e.target.value : this.state.cantidad_prod_buena
            console.log(newValor)
            const TMP_cantidad_prod_buena = newValor==='' ? 0 : newValor
            const TMP_cantidad_prod_mala = this.state.cantidad_prod_mala==='' ? 0 : this.state.cantidad_prod_mala

            const P_Rendimiento = Number(this.state.std_und_hora) > 0 && Number(this.state.tiempo_min) ? Math.round((parseFloat(TMP_cantidad_prod_buena) + parseFloat(TMP_cantidad_prod_mala)) / ((Number(this.state.std_und_hora) / 60) * Number(this.state.tiempo_min))*100, 2) : 0;

            this.setState({
                cantidad_prod_buena: newValor,
                cantidad_total: parseFloat(TMP_cantidad_prod_buena) + parseFloat(TMP_cantidad_prod_mala),
                rendimiento_porcentaje: P_Rendimiento,
                rendimiento_porcentaje_color: this.get_ColorRendimiento(P_Rendimiento),
            })
        }

        if (e.target.name === 'cantidad_prod_mala'){
            const newValor = (e.target.validity.valid) ? e.target.value : this.state.cantidad_prod_mala

            const TMP_cantidad_prod_mala = newValor==='' ? 0 : newValor
            const TMP_cantidad_prod_buena = this.state.cantidad_prod_buena==='' ? 0 : this.state.cantidad_prod_buena

            const P_Rendimiento = Number(this.state.std_und_hora) > 0 && Number(this.state.tiempo_min) > 0 ? Math.round((parseFloat(TMP_cantidad_prod_buena) + parseFloat(TMP_cantidad_prod_mala)) / ((Number(this.state.std_und_hora) / 60) * Number(this.state.tiempo_min))*100, 2) : 0;

            this.setState({
                cantidad_prod_mala: newValor,
                cantidad_total: parseFloat(TMP_cantidad_prod_mala) + parseFloat(TMP_cantidad_prod_buena),
                rendimiento_porcentaje: P_Rendimiento,
                rendimiento_porcentaje_color: this.get_ColorRendimiento(P_Rendimiento),
            })
        }

        if (e.target.name === 'tiempo_min'){
            let newValor = (e.target.validity.valid) ? e.target.value : this.state.tiempo_min

            // newValor = newValor == '' ? 0 :   newValor

            const P_Rendimiento = Number(this.state.std_und_hora) > 0 && Number(newValor) > 0 ? Math.round(this.state.cantidad_total / ((Number(this.state.std_und_hora) / 60) * Number(newValor))*100, 2) : 0;

            this.setState({
                tiempo_min: newValor,
                rendimiento_porcentaje: P_Rendimiento,
                rendimiento_porcentaje_color: this.get_ColorRendimiento(P_Rendimiento),
            })
        }


        if (e.target.name === 'detalle') {
            this.setState({detalle: e.target.value})
        }

        if (e.target.name === 'num_vale') {
            this.setState({num_vale: e.target.value})
        }

        if (e.target.name === 'hora_ini') {
            this.setState({hora_ini: e.target.value})
            this.calcularTiempo(e.target.value, this.state.hora_fin)
        }

        if (e.target.name === 'hora_fin') {
            this.setState({hora_fin: e.target.value})
            this.calcularTiempo(this.state.hora_ini, e.target.value)
        }



    }

    SelectTipoRegistro = (valor, um, sw_horario) => {
        let getTipoRegistro = this.state.listTipo.filter(data => data.cod_tipo_registro === valor)

        this.setState({
            cod_causa: 0,
            causa: '',
            cod_motivo_causa: 0,
            motivo_causa: '',

            detalle: '',
            num_vale: '',

            cod_tipo_registro: valor,
            umu: um,
            tipo_registro: getTipoRegistro[0].tipo_registro,
            listCausa:[],
            listMotivosCausa: [],

            cantidad_prod_buena: '',
            cantidad_prod_mala: '',
            cantidad_total: 0,
            tiempo_min: '',

            sw_horario: sw_horario,
            hora_ini: '00:00',
            hora_fin: '00:00'
        })

        this.FindCausa(valor)

        console.log("77777777777777777777777777")
        console.log(valor)
        console.log(um)
        console.log(sw_horario)

    }



    HandleSubmit = () =>{
        console.log("pepito")

        this.props.DataTemp['TempAdd_nom_empleado'] = this.state.nom_empleado
        this.props.DataTemp['TempAdd_cod_empleado'] = this.state.cod_empleado
        this.props.DataTemp['TempAdd_nroOrdenFab'] = this.state.INPUT_nroOrdenFab

        this.props.DataTemp['TempAdd_num_of_sap'] = this.state.num_of_sap
        this.props.DataTemp['TempAdd_cod_articulo'] = this.state.cod_articulo
        this.props.DataTemp['TempAdd_articulo'] = this.state.articulo

        this.props.DataTemp['TempAdd_num_paso'] = this.state.num_paso
        this.props.DataTemp['TempAdd_std_und_hora'] = this.state.std_und_hora
        this.props.DataTemp['TempAdd_id_tpoperacion'] = this.state.id_tpoperacion


        this.props.eventMantenerInfoTemp(this.props.DataTemp)


        // const P_Rendimiento = Number(this.state.std_und_hora) > 0 ? Math.round(this.state.cantidad_total / ((Number(this.state.std_und_hora) / 60) * Number(this.state.tiempo_min))*100, 2) : 0;
        const P_Rendimiento = this.state.rendimiento_porcentaje
        

        let temp_Detalle = {
            id_oee: 0,
            item_oee: this.state.item_oee,
            cod_tipo_registro: this.state.cod_tipo_registro,
            tipo_registro: this.state.tipo_registro,
            umu: this.state.umu,
            cod_causa: this.state.cod_causa,
            causa: this.state.causa,
            cod_motivo_causa: this.state.cod_motivo_causa,
            motivo_causa: this.state.motivo_causa,
            cantidad_prod_buena: this.state.cantidad_prod_buena==='' ? 0 : this.state.cantidad_prod_buena,
            detalle: this.state.detalle,
            num_vale: this.state.num_vale,

            cantidad_prod_mala: this.state.cantidad_prod_mala==='' ? 0 : this.state.cantidad_prod_mala,
            cantidad_total: this.state.cantidad_total,
            tiempo_min: this.state.tiempo_min ==='' ? 0 : this.state.tiempo_min,

            hora_ini: this.state.hora_ini,
            hora_fin: this.state.hora_fin,

            cod_empleado: this.state.cod_empleado,
            empleado: this.state.nom_empleado,

            num_of_sap: this.state.num_of_sap,
            cod_articulo: this.state.cod_articulo,
            articulo: this.state.articulo,
            num_paso: this.state.num_paso,

            std_und_hora: this.state.std_und_hora,
            id_tpoperacion: this.state.id_tpoperacion,

            rendimiento_porcentaje: P_Rendimiento,


        }


        let ListaError = {
            "cod_tipo_registro": "Tipo de Registro",
            // "tiempo_min": "tiempo_min",
            "empleado": "empleado",
        }

        for(const [key, value] of Object.entries(ListaError)){
            if (temp_Detalle[key]==='' || parseInt(temp_Detalle[key])<=0){
                alert("Error en " + value)
                return
            }
        }

        this.props.eventoClick(temp_Detalle)

        this.props.eventoClose()


    }


    render() {
        return(
            <div style={{alignItems: "center", justifyContent: "center", display: "flex"}}>
                <div className="mx-1 my-2 row text-center" style={{padding: "0"}}>
                    {/*================================================================*/}
                    <div className="col-xl-4 col-sm-4 p-1">
                        <div className="input-group">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="DNI CLiente"
                                onChange={this.HandleChange}
                                value={this.state.cod_empleado}
                                name="cod_empleado"/>
                            <button className="btn btn-primary"
                                    onClick={() => this.FindEmpleado(this.state.cod_empleado)}
                            >
                                <BsSearch/></button>

                        </div>
                    </div>
                    <div className="col-xl-8 col-sm-8 p-1">
                        <span className="input-group-text" style={{height: "100%"}} >{this.state.nom_empleado}</span>
                    </div>
                    {/*================================================================*/}
                    <div className="col-xl-4 col-sm-4 p-1">
                        <div className="input-group">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Orden Fabricacion"
                                onChange={this.HandleChange}
                                value={this.state.INPUT_nroOrdenFab}
                                name="nroOrdenFab"/>
                            <button className="btn btn-primary"
                                    onClick={() => this.FindOrdenFabricacion(this.state.INPUT_nroOrdenFab)}
                            >
                                <BsSearch/></button>
                        </div>
                    </div>

                    <div className="col-xl-8 col-sm-8 p-1">
                        <ComboBoxForm2
                            datosRow={this.state.listOrdenFab}
                            nombre_cbo="num_of"
                            manejaEvento={this.HandleChange}
                            valor_ini="*** Orden Fabricacion ***"
                            valor={this.state.num_of_sap}
                        />
                    </div>

                    {/*================================================================*/}
                    <div className="col-xl-12 col-sm-12 p-1">
                        <div className="input-group">
                            <span className="input-group-text">Nro Paso:</span>
                            <ComboBoxForm2
                                datosRow={this.state.listPasosProcesos}
                                nombre_cbo="num_paso"
                                manejaEvento={this.HandleChange}
                                valor_ini="*** Num Paso/Operacion ***"
                                valor={this.state.cod_PasoTemp}
                            />
                            <span className="input-group-text">{'Unid. X Hora STD: ' + this.state.std_und_hora}</span>
                        </div>
                    </div>

                    {/*================================================================*/}
                    {
                        this.state.listTipo.map((data) =>
                            <div
                                className={data.label_name.length > 7 ? "col-xl-4 col-sm-4 p-1 " : "col-xl-4 col-sm-4 p-1"}> {/*col-xl-2 col-sm-2 p-1*/}
                                <div className="input-group">
                                    <button
                                        className={`form-control btn ${this.state.cod_tipo_registro != data.cod_tipo_registro ? data.style_principal : data.style_secundario}`}
                                        name={data.tipo_registro.replace(" ", "_")}
                                        onClick={() => this.SelectTipoRegistro(data.cod_tipo_registro, data.umu, data.sw_horario)}
                                    >
                                        {/*{data.label_name==='PRODUCCION' ?*/}
                                        {/*    this.SelectTipoRegistro(data.cod_tipo_registro, data.umu, data.sw_horario) : ''*/}
                                        {/*}*/}
                                        {data.label_name}
                                    </button>
                                </div>
                            </div>
                        )
                    }


                    {/*================================================================*/}
                    <div className="col-xl-6 col-sm-6 p-1"
                         style={this.state.sw_horario === '1' ? {display: "block"} : {display: "none"}}>
                        <div className="input-group">
                            <span className="input-group-text">Hora Ini:</span>
                            <input type="time" style={{minWidth: '75px'}} name="hora_ini"
                                   className="form-control"
                                   value={this.state.hora_ini}
                                   onChange={this.HandleChange}
                            />
                        </div>
                    </div>

                    <div className="col-xl-6 col-sm-6 p-1"
                         style={this.state.sw_horario === '1' ? {display: "block"} : {display: "none"}}>
                        <div className="input-group">
                            <span className="input-group-text">Hora Fin:</span>
                            <input type="time" style={{minWidth: '75px'}} name="hora_fin"
                                   className="form-control"
                                   value={this.state.hora_fin}
                                   onChange={this.HandleChange}
                            />
                        </div>
                    </div>

                    {/*================================================================*/}
                    <div className="col-xl-4 col-sm-4 p-1"
                         style={this.state.cod_tipo_registro === 3 ? {display: "block"} : {display: "none"}}>
                        <div className="input-group">
                            <input
                                type="number" style={{minWidth: '80px', color: "green"}}
                                name="cantidad_prod_buena"
                                placeholder="Produccion Buena"
                                onChange={this.HandleChange}
                                className="form-control"
                                value={this.state.cantidad_prod_buena}
                                pattern="[0-9]*"
                            />
                            <span className="input-group-text">Und</span>
                            {/*<span className="input-group-text">{this.state.umu}</span>*/}
                        </div>
                    </div>

                    <div className="col-xl-4 col-sm-4 p-1"
                         style={this.state.cod_tipo_registro === 3 ? {display: "block"} : {display: "none"}}>
                        <div className="input-group">
                            <input
                                type="number" style={{minWidth: '75px', color: "red"}}
                                name="cantidad_prod_mala"
                                placeholder="Produccion Mala"
                                onChange={this.HandleChange}
                                className="form-control"
                                value={this.state.cantidad_prod_mala}
                                pattern="[0-9]*"
                            />
                            <span className="input-group-text">Und</span>
                        </div>
                    </div>

                    <div
                        className={this.state.cod_tipo_registro === 3 ? "col-xl-4 col-sm-4 p-1" : "col-xl-12 col-sm-12 p-1"}>
                        <div className="input-group">
                            <input
                                type="number" style={{minWidth: '80px'}}
                                name="tiempo_min"
                                placeholder="Tiempo"
                                onChange={this.HandleChange}
                                className="form-control"
                                value={this.state.tiempo_min}
                                pattern="[0-9]*"
                                disabled={this.state.sw_horario === '1' ? true : false}
                            />
                            <span className="input-group-text">Min</span>
                        </div>
                    </div>
                    {/*================================================================*/}
                    <div className="col-xl-12 col-sm-12 p-1">
                        <ComboBoxForm2
                            datosRow={this.state.listCausa}
                            nombre_cbo="cod_causa"
                            manejaEvento={this.HandleChange}
                            valor_ini="*** Causa (7m) ***"
                            valor={this.state.cod_causa}
                        />
                    </div>

                    <div className="col-xl-12 col-sm-12 p-1">
                        <ComboBoxForm2
                            datosRow={this.state.listMotivosCausa}
                            nombre_cbo="cod_motivo_causa"
                            manejaEvento={this.HandleChange}
                            valor_ini="*** Motivo Causa ***"
                            valor={this.state.cod_motivo_causa}
                        />
                    </div>

                    {/*================================================================*/}
                    <div>
                        <span
                            className="input-group-text"
                            style={{color: this.state.rendimiento_porcentaje_color, fontWeight: "bold"}}
                        >
                            Redimiento: {this.state.rendimiento_porcentaje} %
                        </span>
                    </div>

                    {/*================================================================*/}
                    <div className="col-xl-12 col-sm-12 p-1">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Detalle"
                            onChange={this.HandleChange}
                            value={this.state.detalle}
                            name="detalle"/>
                    </div>

                    <div className="col-xl-12 col-sm-12 p-1">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="N° Vale"
                            onChange={this.HandleChange}
                            value={this.state.num_vale}
                            name="num_vale"/>
                    </div>


                    {/*================================================================*/}
                    <div className="col-xl-12 col-sm-12 p-1">
                        <button
                            className="form-control btn btn-primary"
                            onClick={this.HandleSubmit}
                        >
                            <BsPlusLg/>
                            Agregar
                        </button>
                    </div>

                </div>
            </div>
        )
    }

    async componentDidMount() {
        // const responseJson = await get_oee_tipo_registro("0", "0", this.props.cod_subarea)
        // this.setState({listTipo: responseJson})
        // console.log("AAAAAA")
        // console.log(this.props.cod_subarea)
        // console.log(responseJson)

        // let me_SwHorario = responseJson.filter((item) => item.cod_tipo_registro === 3);

        let me_SwHorario = this.state.listTipo.filter((item) => item.cod_tipo_registro === 3);
        console.log(me_SwHorario[0].sw_horario)
        console.log(me_SwHorario)
        console.log("AAAAAA")
        this.SelectTipoRegistro(3, 'UNIDAD', me_SwHorario[0].sw_horario)




        if (typeof this.props.DataTemp != 'undefined'){

            this.FindOrdenFabricacion(this.state.INPUT_nroOrdenFab.toString())
            this.handleFindNumPaso(this.state.cod_articulo)


        //     this.setState({
        //         cod_empleado: this.props.TempAdd_cod_empleado,
        //         nom_empleado: this.props.TempAdd_nom_empleado,
        //
        //
        //         num_of_sap: this.props.TempAdd_num_of_sap,
        //         cod_articulo: this.props.TempAdd_cod_articulo,
        //         articulo: this.props.TempAdd_articulo,
        //
        //         num_paso: this.props.TempAdd_num_paso,
        //         std_und_hora: this.props.TempAdd_std_und_hora,
        //         id_tpoperacion: this.props.TempAdd_id_tpoperacion,
        //
        //
        //         INPUT_nroOrdenFab: this.props.TempAdd_num_of_sap,
        //     })
        }

    }

}

export default EficienciaOperativa_Edit_Add;
