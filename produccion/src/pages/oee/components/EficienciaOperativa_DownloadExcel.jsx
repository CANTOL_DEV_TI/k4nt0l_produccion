import React from "react";

import * as XLSX from 'xlsx'
import {saveAs} from 'file-saver'

import {BsFileEarmarkExcelFill} from "react-icons/bs";


function XLSXDownload(props) {

    const data = [
        { name: "John", email: "john@example.com", age: 28 },
        { name: "Jane", email: "jane@example.com", age: 32 },
        // ... more data
    ];

    const exportToExcel = () => {
        //const worksheet = XLSX.utils.json_to_sheet(data);
        const worksheet = XLSX.utils.json_to_sheet(props.listDatos);
        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, "Sheet1");

        // Buffer to store the generated Excel file
        const excelBuffer = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        const blob = new Blob([excelBuffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });

        saveAs(blob, "exportedData.xlsx");
    };

    return (
        <div className="App">
            <button className={props.sw_habilitado ? 'btn btn-warning btn-sm' : 'btn btn-warning btn-sm disabled'}  onClick={exportToExcel}><BsFileEarmarkExcelFill/>Listo,Puedes Exportar</button>
        </div>
    );

}

export default XLSXDownload;



