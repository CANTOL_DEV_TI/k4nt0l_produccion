import React from "react";

import { BsPencilFill } from "react-icons/bs";



class EficienciaOperativa_Indicadores extends React.Component{
    constructor(props) {
        super(props);
    }


    render() {
        return(
            <>
                {/* INICIO TABLA */}
                <div className="table-responsive">
                    <table className="table table-hover table-sm table-bordered m-0 p-0">
                        <thead className="table-secondary text-center table-sm">
                            <tr>
                                <th className="align-middle" title="Descripción del OEE">Descripción</th>
                                <th className="align-middle" title="Porcentaje" >(%)</th>
                                <th className="align-middle" title="Motivo de la causa">Motivo Causa/Detalle</th>
                            </tr>
                        </thead>
                        <tbody className="list">


                        {Object.keys(this.props.dataIndicadores).length > 0 ?

                            Object.keys(this.props.dataIndicadores).map((e, index) =>
                                <tr key={index}>
                                    <td className="td-cadena">{this.props.dataIndicadores[e].causa}</td>
                                    <td className="td-num" style={{ backgroundColor: "#EBF5FB" }}>
                                         {(Math.round(this.props.dataIndicadores[e].cantidad, 2)).toLocaleString("en")}
                                    </td>
                                    <td className="">
                                        <button title="Editar detalle" className="btn btn-outline-primary btn-sm"
                                                onClick={() => this.props.eventEditDetalle(this.props.dataIndicadores[e].id_config_oee)}>
                                            <BsPencilFill /> {this.props.dataIndicadores[e].motivo_causa}
                                         </button>
                                    </td>
                                </tr>

                            )


                            :

                        <tr><td className="text-center" colSpan="3">Sin datos</td></tr>

                        }
                        </tbody>
                    </table>
                </div>
                {/* FIN TABLA */}
            </>
        )
    }


}

export default EficienciaOperativa_Indicadores;

