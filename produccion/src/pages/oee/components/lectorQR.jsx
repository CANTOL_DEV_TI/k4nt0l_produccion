
import React from "react";

import QrReader from 'react-qr-scanner';

class LectorQR extends React.Component{
    constructor(props) {
        super(props);

        this.state ={
            delay: 100,
            resultado: 'No Resultado'
        }


        this.handleScan = this.handleScan.bind(this)
        this.handleError = this.handleError.bind(this)

    }

    handleScan(data){
        this.setState({resultado: data})
        console.log(data)
    }

    handleError(err){
        console.error(err)
    }

    render() {
        const previewStyle={
            height: 240,
            width: 320
        }

        return (
            <div style={{height: "auto", margin: "0 auto", maxWidth: 64, width: "100%", alignItems: "center", justifyContent: "center", display: "flex"}}>
                <QrReader
                    delay={this.state.delay}
                    style={previewStyle}
                    onError={this.handleError}
                    onScan={this.handleScan}
                />

                {/*<span>{this.state.resultado}</span>*/}
                <span>PEPEPE</span>
            </div>
        )
    }
}

export default LectorQR;
