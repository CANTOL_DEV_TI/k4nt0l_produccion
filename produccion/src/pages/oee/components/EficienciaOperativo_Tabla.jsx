import { BsXCircleFill, BsPencilFill } from "react-icons/bs";

export function EficienciaOperativo_Tabla({ datos_fila, eventoClick, eventoDelete }) {

    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered m-0 p-0">
                    <thead className="table-secondary text-center table-sm">
                    <tr>
                        <th className="align-middle" title="Opciones" style={{width: '70px'}}>Edit</th>
                        <th className="align-middle" title="Opciones" style={{width: '70px'}}>Eli</th>
                        <th className="align-middle" title="Equipo">Equipo</th>
                        {/*<th className="align-middle" title="Unidad de medida de uso" >UND</th>*/}
                        <th className="align-middle" title="Causa" style={{width: '200px'}}>Causa</th>
                        <th className="align-middle" title="Motivo" style={{width: '200px'}}>Motivo</th>
                        <th className="align-middle" title="Tiempo en Minutos" style={{width: '70px'}}>Tiempo (Min)</th>
                    </tr>
                    </thead>
                    <tbody className="list">
                    {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index}>
                                        <td className="">
                                            <button data-toggle="tooltip" title="Editar"
                                                    className="btn btn-outline-primary btn-sm"
                                                    onClick={() => eventoClick(index, datos.equipo)}
                                            >
                                                <BsPencilFill/>
                                            </button>
                                        </td>
                                        <td className="">
                                            <button data-toggle="tooltip" title="Eliminar"
                                                    className="btn btn-outline-danger btn-sm"
                                                    onClick={() => eventoDelete(index)}>
                                                <BsXCircleFill/>
                                            </button>
                                        </td>
                                        <td className="td-cadena">{datos.equipo}</td>
                                        {/*<td className="td-cadena">{datos.umu}</td>*/}
                                        <td className="td-num" style={{backgroundColor: "#EBF5FB"}}>{datos.causa}</td>
                                        <td className="td-num"
                                            style={{backgroundColor: "#EBF5FB"}}>{datos.motivo_causa}</td>
                                        <td className="td-num"
                                            style={{backgroundColor: "#EBF5FB"}}>{datos.tiempo_min.toLocaleString("en")}</td>
                                    </tr>
                                })
                            ) :
                        (
                            <tr>
                                <td className="text-center" colSpan="8">Sin datos</td>
                            </tr>
                        )
                    }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}
