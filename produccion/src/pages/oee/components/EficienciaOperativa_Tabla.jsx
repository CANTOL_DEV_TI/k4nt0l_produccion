import React from "react";
import {BsPencilFill, BsXCircleFill} from "react-icons/bs";




class EficienciaOperativa_Tabla extends React.Component{
    constructor(props) {
        super(props);

    }


    render() {
        return(
            <>
                {/* INICIO TABLA */}
                <div className="table-responsive">
                    <table className="table table-hover table-sm table-bordered m-0 p-0">
                        <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" title="Opciones" style={{width: '42px'}}>EDIT</th>
                            {this.props.swPermisos == "" || this.props.swPermisos == null ?
                                ''
                                :
                                <th className="align-middle" title="Opciones" style={{width: '42px'}}>ELI</th>
                            }

                            <th className="align-middle" title="Código Registro">Cod. Reg.</th>
                            <th className="align-middle" title="Fecha">Fecha</th>
                            <th className="align-middle" title="Turno">Turno</th>
                            <th className="align-middle" title="Equipo">Equipo</th>
                            <th className="align-middle" title="Total" style={{width: '70px'}}>Total Produccion</th>

                        </tr>
                        </thead>
                        <tbody className="list">
                        {this.props.dataRegistros.length > 0 ?

                            this.props.dataRegistros.map((datos, index) =>
                                <tr key={index + 1}>
                                    <td className="">
                                        <button data-toggle="tooltip" title="Editar"
                                                className="btn btn-outline-primary btn-sm"
                                                onClick={() => this.props.eventoClick(datos)}>
                                            <BsPencilFill/>
                                        </button>
                                    </td>
                                    {this.props.swPermisos == "" || this.props.swPermisos == null ?
                                        ''
                                        :
                                        <td className="">
                                            <button data-toggle="tooltip" title="Eliminar"
                                                    className="btn btn-outline-danger btn-sm"
                                                    onClick={() => this.props.eventoDelete(datos.id_oee)}>
                                                <BsXCircleFill/>
                                            </button>
                                        </td>
                                    }

                                    <td className="td-cadena">{datos.id_oee}</td>
                                    <td className="td-cadena">{datos.fecha_doc}</td>
                                    <td className="td-cadena"
                                        style={{backgroundColor: "#EBF5FB"}}>{datos.nom_turno}</td>
                                    <td className="td-cadena">{datos.equipo}</td>
                                    <td className="td-num">{datos.produccion_total}</td>

                                </tr>
                            )
                            :

                            <tr>
                                <td className="text-center" colSpan="8">Sin datos</td>
                            </tr>

                        }
                        </tbody>
                    </table>
                </div>
                {/* FIN TABLA */}
            </>
        )
    }

}

export default EficienciaOperativa_Tabla;

