import { BsPencilFill } from "react-icons/bs";

export function EficienciaOperativa_crud_oee({ datos_fila, eventoClick }) {

    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered m-0 p-0">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle" title="Descripción del OEE">Descripción</th>
                            <th className="align-middle" title="Porcentaje" >(%)</th>
                            <th className="align-middle" title="Motivo de la causa">Motivo Causa/Detalle</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                        {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td className="td-cadena">{datos.causa}</td>
                                        <td className="td-num" style={{ backgroundColor: "#EBF5FB" }}>
                                            {(Math.round((datos.cantidad * 100) * 100) / 100).toLocaleString("en")}
                                        </td>
                                        <td className="">
                                            <button title="Editar detalle" className="btn btn-outline-primary btn-sm"
                                                onClick={() => eventoClick("oee", datos)}>
                                                <BsPencilFill /> {datos.motivo_causa}
                                            </button>
                                        </td>
                                    </tr>
                                })
                            ) :
                            (
                                <tr>
                                    <td className="text-center" colSpan="3">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}