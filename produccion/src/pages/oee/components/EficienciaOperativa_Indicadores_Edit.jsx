import React from "react";
import {ComboBoxForm, ComboBoxForm2} from "../../../components/ComboBox.jsx";
import {BsPlusLg} from "react-icons/bs";

import {get_indicadores_motivos} from "../services/oeeIndicadoresMotivosServices"


class EficienciaOperativa_Indicadores_Edit extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            listIndicadorMotivos: [],
            cod_motivo_indicador: 0,
            detalle_motivo: ''
        }

    }

    handleChange = (e) =>{
        if (e.target.name === 'cod_motivo_indicador' ){
            this.setState({cod_motivo_indicador: e.target.value})
        }

        if (e.target.name === 'detalle'){
            this.setState({detalle_motivo: e.target.value})
        }

    }

    render() {
        return(
            <>
                {/* INICIO */}
            <div className="mx-1 my-2 row text-center">
                <div className="col-xl-4 col-sm-4 p-1">
                    <ComboBoxForm2
                        datosRow={this.state.listIndicadorMotivos}
                        nombre_cbo="cod_motivo_indicador"
                        manejaEvento={this.handleChange}
                        valor_ini="*** Motivo Indicador ***"
                        valor={this.state.cod_motivo_indicador}
                    />
                </div>
                <div className="col-xl-5 col-sm-5 p-1">
                    <input type="text" className="form-control" placeholder="Detalle" name="detalle" onChange={this.handleChange} value={this.state.detalle_motivo}/>
                </div>
                <div className="col-xl-3 col-sm-3 p-1">
                    <button className="form-control btn btn-primary" onClick={() => this.props.eventoClick(this.state.cod_motivo_indicador, this.state.detalle_motivo, this.props.id_config_oee)}><BsPlusLg /> Agregar</button>
                </div>
            </div>
            {/* FIN */}
            </>
        )
    }

    async componentDidMount() {
        const responseJson = await get_indicadores_motivos(this.props.id_config_oee, this.props.cod_subarea)
        this.setState({listIndicadorMotivos: responseJson})
    }

}

export default EficienciaOperativa_Indicadores_Edit;
