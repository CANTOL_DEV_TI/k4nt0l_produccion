import { BsXCircleFill } from "react-icons/bs";

export function EficienciaOperativa_crud_tabla({ datos_fila, eventoClick }) {

    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered m-0 p-0">
                    <thead className="table-secondary text-center table-sm">
                    <tr>
                        <th className="align-middle" title="Opciones" style={{width: '42px'}}>Opc</th>
                        <th className="align-middle" title="Código Artículo">Tipo Reg.</th>
                        {/*<th className="align-middle" title="Unidad de medida de uso" >UND</th>*/}
                        <th className="align-middle" title="Unidad de medida de uso" style={{width: '70px'}}>Prod.
                            Buena
                        </th>
                        <th className="align-middle" title="Unidad de medida de uso" style={{width: '70px'}}>Prod.
                            Mala
                        </th>
                        <th className="align-middle" title="Unidad de medida de uso" style={{width: '70px'}}>Tiempo
                            (Min)
                        </th>
                        <th className="align-middle" title="Artículo">Causa (7M)</th>

                        <th className="align-middle" title="Artículo">Hora Ini</th>
                        <th className="align-middle" title="Artículo">Hora Final</th>

                        <th className="align-middle" title="Artículo">Producto</th>
                        <th className="align-middle" title="Artículo">Nro Paso</th>
                        <th className="align-middle" title="Artículo">Colaborador</th>

                        <th className="align-middle" title="Rendimiento">Rendi. %</th>

                        <th className="align-middle" title="Artículo">Motivo Causa</th>
                        <th className="align-middle" title="Cantidad Planeado">Detalle</th>
                        <th className="align-middle" title="Cantidad Producido">N° Vale</th>
                    </tr>
                    </thead>
                    <tbody className="list">
                    {datos_fila.length > 0 ?
                            (
                                datos_fila.map((datos, index) => {
                                    return <tr key={index + 1}>
                                        <td className="">
                                            <button data-toggle="tooltip" title="Ver Historial"
                                                    className="btn btn-outline-danger btn-sm"
                                                    onClick={() => eventoClick(datos.item_oee)}>
                                                <BsXCircleFill/>
                                            </button>
                                        </td>
                                        <td className="td-cadena">{datos.tipo_registro}</td>
                                        {/*<td className="td-cadena">{datos.umu}</td>*/}
                                        {/*<td className="td-num" style={{backgroundColor: "#EBF5FB"}}>{Number(datos.cantidad_prod_buena).toLocaleString('en-US', {style: 'currency', currency: 'PEN'})}</td>*/}
                                        <td className="td-num" style={{backgroundColor: "#EBF5FB"}}>{Number(datos.cantidad_prod_buena).toLocaleString('en')}</td>
                                        <td className="td-num" style={{backgroundColor: "#EBF5FB"}}>{Number(datos.cantidad_prod_mala).toLocaleString("en")}</td>
                                        <td className="td-num" style={{backgroundColor: "#EBF5FB"}}>{Number(datos.tiempo_min).toLocaleString("en")}</td>
                                        <td className="td-cadena">{datos.causa}</td>

                                        <td className="td-cadena">{datos.hora_ini}</td>
                                        <td className="td-cadena">{datos.hora_fin}</td>

                                        <td className="td-cadena">{datos.articulo}</td>
                                        <td className="td-cadena">{datos.num_paso}</td>
                                        <td className="td-cadena">{datos.empleado}</td>

                                        <td className="td-num">{ datos.rendimiento_porcentaje == null ? "" : datos.rendimiento_porcentaje + ' %'}</td>

                                        <td className="td-cadena">{datos.motivo_causa}</td>
                                        <td className="td-cadena">{datos.detalle}</td>
                                        <td className="td-cadena">{datos.num_vale}</td>
                                    </tr>
                                })
                            ) :
                        (
                            <tr>
                                <td className="text-center" colSpan="8">Sin datos</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}