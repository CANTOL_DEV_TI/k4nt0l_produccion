import React, { useState, useEffect } from 'react'
import { BsPlusLg } from "react-icons/bs";
import { get_oee_motivo_causa } from '../services/oeeMotivoCausaServices.jsx';
import { ComboBoxForm } from "../../../components/ComboBox";

export function EficienciaOperativa_crud_oee_detalle({ eventoClick, cod_subarea = "0", datosModal = {} }) {

    // llenar combos
    const [formDet, setFormDet] = useState(datosModal);
    const [listMotivo, setListMotivo] = useState([]);
    const [listMotivoCausa, setListMotivoCausa] = useState([]);
    useEffect(() => {
        get_oee_motivo_causa(cod_subarea, datosModal.cod_causa, "1")
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.cod_motivo_causa, "nombre": data.motivo_causa
                    }
                })
                setListMotivoCausa(cbo_data)
                setListMotivo(res)
            });
    }, []);

    // BUSQUEDA GENERALES
    function datosBusqueda(evento, opc = "0") {
        // seleccionar motivo
        if (evento.target.name === 'cod_motivo_causa') {
            let c_motivo = get_motivo((evento.target.value).toUpperCase());
            setFormDet({
                ...formDet,
                cod_motivo_causa: c_motivo[0],
                motivo_causa: c_motivo[1],
            });
        };

        // ingresar detalle
        if (evento.target.name === 'detalle') {
            setFormDet({
                ...formDet,
                detalle: (evento.target.value).toUpperCase(),
            });
        };
    };

    // añadir detalle
    const handleSubmit = (e) => {
        // devolver datos
        eventoClick(formDet);
    };

    // Seleccionar orden de fabricación
    const get_motivo = (cod_motivo_causa) => {
        let c_motivo = ["0", ""];
        let getMotivo = listMotivo.filter(motivo => motivo.cod_motivo_causa.toString() === cod_motivo_causa);
        if (getMotivo.length > 0) {
            c_motivo = [getMotivo[0].cod_motivo_causa.toString(), getMotivo[0].motivo_causa];
        }
        return c_motivo;
    };

    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO */}
            <div className="mx-1 my-2 row text-center">
                <div className="col-xl-5 col-sm-5 p-1">
                    <ComboBoxForm
                        datosRow={listMotivoCausa}
                        nombre_cbo="cod_motivo_causa"
                        manejaEvento={datosBusqueda}
                        valor_ini="*** Motivo Causa ***"
                        valor={formDet.cod_motivo_causa}
                    />
                </div>
                <div className="col-xl-5 col-sm-5 p-1">
                    <input type="text" className="form-control" placeholder="Detalle" onChange={datosBusqueda} value={formDet.detalle} name="detalle" />
                </div>
                <div className="col-xl-2 col-sm-2 p-1">
                    <button className="form-control btn btn-primary" onClick={handleSubmit}><BsPlusLg /> Agregar</button>
                </div>
            </div>
            {/* FIN */}
        </>
    );
}