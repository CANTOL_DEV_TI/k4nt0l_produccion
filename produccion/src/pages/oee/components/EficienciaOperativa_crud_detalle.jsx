import React, { useState, useEffect } from 'react'
import { BsPlusLg } from "react-icons/bs";
import { get_oee_tipo_registro } from '../services/oeeTipoRegistroServices.jsx';
import { get_oee_causa } from '../services/oeeCausaServices.jsx';
import { get_oee_motivo_causa } from '../services/oeeMotivoCausaServices.jsx';
import { ComboBoxForm } from "../../../components/ComboBox";

export function EficienciaOperativa_crud_detalle({ eventoClick, iniFormDet={}, cod_subarea = "0", item_oee = 0 }) {

    // llenar combos
    const [formDet, setFormDet] = useState(iniFormDet);
    const [numOEEItem, setNumOEEItem] = useState(item_oee);
    const [listTipo, setListTipo] = useState([]);
    const [listCausa, setListCausa] = useState([]);
    const [listMotivo, setListMotivo] = useState([]);
    useEffect(() => {
        get_oee_tipo_registro("0", "1")
            .then(res => {
                setListTipo(res)
            });

        get_oee_causa("0", cod_subarea, "1")
            .then(res => {
                setListCausa(res)
            });

        get_oee_motivo_causa(cod_subarea, "0", "1")
            .then(res => {
                setListMotivo(res)
            });
    }, []);

    // BUSQUEDA GENERALES
    function datosBusqueda(evento, opc = "0") {
        // seleccionar tipos de operación
        if (opc != "0") {
            let c_tipo
            let c_causa;
            if (opc === "REF") {
                c_tipo = get_tipo("2");
                c_causa = get_causa("1"); // refrigerio
            } else {
                c_tipo = get_tipo(opc);
                c_causa = get_causa("0"); // vacio
            }

            setFormDet({
                ...formDet,
                item_oee: numOEEItem,
                cod_tipo_registro: c_tipo[0],
                tipo_registro: c_tipo[1],
                umu: c_tipo[2],
                cantidad: c_causa[2],
                cod_causa: c_causa[0],
                causa: c_causa[1],
                cod_motivo_causa: "0",
            });

            cbo_causa(c_tipo[0].toString());
        }

        // registrar cantidad
        if (evento.target.name === 'cantidad') {
            setFormDet({
                ...formDet,
                cantidad: (evento.target.value).toUpperCase(),
            });
        };

        // seleccionar causa
        if (evento.target.name === 'cod_causa') {
            let c_causa = get_causa((evento.target.value).toUpperCase());
            setFormDet({
                ...formDet,
                cod_causa: c_causa[0],
                causa: c_causa[1],
                cantidad: c_causa[2],
                cod_motivo_causa: "0",
            });
            cbo_motivo((evento.target.value).toUpperCase());
        };

        // seleccionar motivo
        if (evento.target.name === 'cod_motivo_causa') {
            let c_motivo = get_motivo((evento.target.value).toUpperCase());
            setFormDet({
                ...formDet,
                cod_motivo_causa: c_motivo[0],
                motivo_causa: c_motivo[1],
            });
        };

        // ingresar detalle
        if (evento.target.name === 'detalle') {
            setFormDet({
                ...formDet,
                detalle: (evento.target.value).toUpperCase(),
            });
        };

        // ingresar detalle
        if (evento.target.name === 'num_vale') {
            setFormDet({
                ...formDet,
                num_vale: (evento.target.value).toUpperCase(),
            });
        };

    };

    // añadir detalle
    const handleSubmit = (e) => {
        if (formDet.cod_tipo_registro === "0") {
            alert("Seleccionar tipo de registro");
            return;
        }
        if (formDet.cod_tipo_registro != "3" && formDet.cod_causa === "0") {
            alert("Seleccionar Causa");
            return;
        }
        if ((formDet.cod_tipo_registro != "2" && formDet.cod_tipo_registro != "3") && formDet.cod_motivo_causa === "0") {
            alert("Seleccionar Motivo");
            return;
        }
        if (formDet.cantidad === 0) {
            alert("Ingresar Cantidad");
            return;
        }

        // devolver datos
        setNumOEEItem(numOEEItem + 1)
        eventoClick(formDet);
        setFormDet(iniFormDet);
    };

    // Seleccionar orden de fabricación
    const [listCausaTipo, setListCausaTipo] = useState([]);
    const [listMotivoCausa, setListMotivoCausa] = useState([]);
    const cbo_causa = (cod_tipo_registro) => {
        let getCausa = listCausa.filter(causa => causa.cod_tipo_registro.toString() === cod_tipo_registro);
        const cbo_data = getCausa.map(data => {
            return {
                "codigo": data.cod_causa, "nombre": data.causa
            }
        })
        setListCausaTipo(cbo_data);
    };

    const cbo_motivo = (cod_causa) => {
        let filtrarMotivo = listMotivo.filter(mot => mot.cod_causa.toString() === cod_causa);
        const cbo_data = filtrarMotivo.map(data => {
            return {
                "codigo": data.cod_motivo_causa, "nombre": data.motivo_causa
            }
        })
        setListMotivoCausa(cbo_data);
    };

    const get_tipo = (cod_tipo_registro) => {
        let c_tipo = ["0", "", ""];
        let getTipo = listTipo.filter(tipo => tipo.cod_tipo_registro.toString() === cod_tipo_registro);
        if (getTipo.length > 0) {
            c_tipo = [getTipo[0].cod_tipo_registro.toString(), getTipo[0].tipo_registro, getTipo[0].umu];
        }
        return c_tipo;
    };

    const get_causa = (cod_causa) => {
        let c_causa = ["0", "", 0];
        let getCausa = listCausa.filter(causa => causa.cod_causa.toString() === cod_causa);
        if (getCausa.length > 0) {
            c_causa = [getCausa[0].cod_causa.toString(), getCausa[0].causa, getCausa[0].cantidad || 0];
        }
        return c_causa;
    };

    const get_motivo = (cod_motivo_causa) => {
        let c_motivo = ["0", ""];
        let getMotivo = listMotivo.filter(motivo => motivo.cod_motivo_causa.toString() === cod_motivo_causa);
        if (getMotivo.length > 0) {
            c_motivo = [getMotivo[0].cod_motivo_causa.toString(), getMotivo[0].motivo_causa];
        }
        return c_motivo;
    };


    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO */}
            <div className="mx-1 my-2 row text-center">
                <div className="col-xl-4 col-sm-4 p-1">
                    <div className="input-group">
                        <button className={`form-control btn ${formDet.cod_tipo_registro === "2" && formDet.cod_causa === "1" ? 'btn-primary' : 'btn-outline-primary'}`}
                            name="btn_refrigerio" onClick={event => datosBusqueda(event, "REF")}  >Refrigerio</button>
                        <button className={`form-control btn ${formDet.cod_tipo_registro === "2" && formDet.cod_causa != "1" ? 'btn-secondary' : 'btn-outline-secondary'}`}
                            name="btn_pausa" onClick={event => datosBusqueda(event, "2")}>Pausa</button>
                    </div>
                </div>
                <div className="col-xl-4 col-sm-4 p-1">
                    <div className="input-group">
                        <button className={`form-control btn ${formDet.cod_tipo_registro === "1" ? 'btn-warning' : 'btn-outline-warning'}`}
                            name="btn_parada" onClick={event => datosBusqueda(event, "1")}>Parada</button>
                    </div>
                </div>
                <div className="col-xl-4 col-sm-4 p-1">
                    <div className="input-group">
                        <button className={`form-control btn ${formDet.cod_tipo_registro === "3" ? 'btn-success' : 'btn-outline-success'}`}
                            name="btn_buena" onClick={event => datosBusqueda(event, "3")}>Prod.Buena</button>
                        <button className={`form-control btn ${formDet.cod_tipo_registro === "4" ? 'btn-danger' : 'btn-outline-danger'}`}
                            name="btn_mala" onClick={event => datosBusqueda(event, "4")}>Prod.Mala</button>
                    </div>
                </div>
                <div className="col-xl-4 col-sm-4 p-1">
                    <ComboBoxForm
                        datosRow={listCausaTipo}
                        nombre_cbo="cod_causa"
                        manejaEvento={datosBusqueda}
                        valor_ini="*** Causa (7m) ***"
                        valor={formDet.cod_causa}
                    />
                </div>
                <div className="col-xl-4 col-sm-4 p-1">
                    <div className="input-group">
                        <input type="number" style={{ minWidth: '75px' }} name="cantidad" placeholder="Cantidad" className="form-control" value={formDet.cantidad} onChange={datosBusqueda} />
                        <span className="input-group-text">{formDet.umu}</span>
                    </div>
                </div>
                <div className="col-xl-4 col-sm-4 p-1">
                    <ComboBoxForm
                        datosRow={listMotivoCausa}
                        nombre_cbo="cod_motivo_causa"
                        manejaEvento={datosBusqueda}
                        valor_ini="*** Motivo Causa ***"
                        valor={formDet.cod_motivo_causa}
                    />
                </div>
                <div className="col-xl-7 col-sm-7 p-1">
                    <input type="text" className="form-control" placeholder="Detalle" onChange={datosBusqueda} value={formDet.detalle} name="detalle" />
                </div>
                <div className="col-xl-3 col-sm-3 p-1">
                    <input type="text" className="form-control" placeholder="N° Vale" onChange={datosBusqueda} value={formDet.num_vale} name="num_vale" />
                </div>
                <div className="col-xl-2 col-sm-2 p-1">
                    <button className="form-control btn btn-primary" onClick={handleSubmit}><BsPlusLg /> Agregar</button>
                </div>
            </div>
            {/* FIN */}
        </>
    );
}