import React from "react";
import {ComboBoxForm2} from "../../components/ComboBox";

import {BsPlusLg, BsSearch} from "react-icons/bs";

import {get_subarea} from "./services/oeeSubAreaServices"
import {get_periodo_laboral_turno, get_turno_det} from "./services/oeeTurnoServices"
import {get_config_proceso_det_articulo} from "./services/oeeMaquinaServices"
import {get_indicadores} from "./services/oeeIndicadoresServices.jsx"

import VentanaModal from "../../components/VentanaModal";
import {OrdenFabricacionBuscar} from "../ordenFabricacionPrograma/OrdenFabricacionBuscar";
import {EmpleadoBuscar} from "../empleado/EmpleadoBuscar";


import EficienciaOperativa_Edit_Add from "./EficienciaOperativa_Edit_Add.jsx"
import {EficienciaOperativa_crud_tabla} from "./components/EficienciaOperativa_crud_tabla";

import EficienciaOperativa_Indicadores from "./components/EficienciaOperativa_Indicadores";
import {EficienciaOperativa_crud_oee_detalle} from "./components/EficienciaOperativa_crud_oee_detalle.jsx";
import EficienciaOperativa_Indicadores_Edit from "./components/EficienciaOperativa_Indicadores_Edit";
import {BotonGrabar, BotonGrabar2, BotonGuardar} from "../../components/Botones.jsx";
import {
    insert_oeeEficiencia,
    update_oeeEficiencia,
    get_NumPaso,
    get_Maquinas_x_Paso,
    get_CausasDefault,
    get_OnlyMaquinas
} from "./services/oeeEficienciaServices.jsx";
import {NavLink, useNavigate} from "react-router-dom";

import LectorQR from "./components/lectorQR.jsx"
import {get_oee_tipo_registro} from "./services/oeeTipoRegistroServices.jsx";

const v_fecha = (new Date()).toLocaleDateString('en-GB', {year: 'numeric', month: '2-digit', day: '2-digit',}).split('/').reverse().join('-');

class EficienciaOperativa_Edit extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            showModal: {estado: false, tipo: ""},
            listTipo: [],

            listOnlyMaquinas: [],

            listArea: [],
            listTurno: [],
            listPasosProcesos: [],

            listIndicadores: typeof this.props.regEdit === 'undefined' ? [] : this.props.regEdit.listIndicadores,    //para llenar los indicadores

            id_config_oee_SELECTED: 0,

            tipoOperacion: typeof this.props.tipoOperacion === 'undefined' ? "Grabar" : this.props.tipoOperacion,

            op_OK: true,




            // CABECERA ---------------------------------
            id_oee: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.id_oee,

            cod_subarea: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.cod_subarea,
            fecha_doc: typeof this.props.regEdit === 'undefined' ? v_fecha : this.props.regEdit.fecha_doc,

            cod_turno: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.cod_turno,
            id_equipo: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.id_equipo,

            // cod_PasoTemp: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.num_paso+" - "+this.props.regEdit.id_tpoperacion.toString(),
            // id_paso_equipo: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.id_equipo.toString()+this.props.regEdit.num_paso,
            id_tpoperacion: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.id_tpoperacion,



            // DETALLE --------------------------------------
            autoNumericItem: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.autoNumericItem,
            detalle_formulario: typeof this.props.regEdit === 'undefined' ? [] : this.props.regEdit.detalle,


            // TOTALES ----------------------------------
            std_und_hora: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.std_und_hora,
            hora_ini: typeof this.props.regEdit === 'undefined' ? '00:00' : this.props.regEdit.hora_ini,
            hora_fin: typeof this.props.regEdit === 'undefined' ? '00:00' : this.props.regEdit.hora_fin,
            produccion_total: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.produccion_total,
            hora_plan: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.hora_planificado,
            hora_operativo: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.horas_operativo,

            hora_perdida: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.horas_perdidas,
            hora_pausas: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.hora_pausas,
            hora_paradas: typeof this.props.regEdit === 'undefined' ? 0 : this.props.regEdit.hora_paradas,


            // INDICADORES ----------------------------------
            tmp_oeeDisponibilidad: 0,
            tmp_oeeRendimiento: 0,
            tmp_oeeCalidad: 0,
            tmp_oeeOee: 0,
            indicadores_formulario: [],      //Save DB


            // TEMPORAL VENTANA ADD ----------------------------------
            TempAdd: {
                TempAdd_nom_empleado: '',
                TempAdd_cod_empleado: '',
                TempAdd_nroOrdenFab: '',

                TempAdd_num_of_sap: '',
                TempAdd_cod_articulo: null,
                TempAdd_articulo: '',

                TempAdd_num_paso: '0',
                TempAdd_std_und_hora: '0',
                TempAdd_id_tpoperacion: '',
            }






            // this.setState({nom_empleado: responseJson[0].nombre})
            // cod_empleado
            // FindOrdenFabricacion (nroOrdenFab)

            // this.setState({
            //     num_of_sap: P_CodArticulo[0].num_of,
            //     cod_articulo: P_CodArticulo[0].cod_articulo,
            //     articulo: P_CodArticulo[0].articulo
            // })
            //
            // this.handleFindNumPaso(P_CodArticulo[0].cod_articulo)


            // this.setState({
            //     num_paso: P_NroPaso[0].num_paso,
            //     std_und_hora: P_NroPaso[0].std_und_hora,
            //     id_tpoperacion: P_NroPaso[0].id_tpoperacion,
            // })



        }



    }

    // ===========================================================================
    TempData_AddItem = (item) =>{

        this.state.TempAdd['TempAdd_nom_empleado']=item['TempAdd_nom_empleado']
        this.state.TempAdd['TempAdd_cod_empleado']=item['TempAdd_cod_empleado']
        this.state.TempAdd['TempAdd_nroOrdenFab']=item['TempAdd_nroOrdenFab']

        this.state.TempAdd['TempAdd_num_of_sap']=item['TempAdd_num_of_sap']
        this.state.TempAdd['TempAdd_cod_articulo']=item['TempAdd_cod_articulo']
        this.state.TempAdd['TempAdd_articulo']=item['TempAdd_articulo']

        this.state.TempAdd['TempAdd_num_paso']=item['TempAdd_num_paso']
        this.state.TempAdd['TempAdd_std_und_hora']=item['TempAdd_std_und_hora']
        this.state.TempAdd['TempAdd_id_tpoperacion']=item['TempAdd_id_tpoperacion']

    }



    // ===========================================================================
    AddItem = (item) => {
        item['item_oee']= this.state.autoNumericItem++
        this.state.detalle_formulario.push(item)
        this.calcularTotales(this.state.detalle_formulario)
        //console.log(item)
        //console.log("turururur")
    }

    DeleteItem = (c_item) =>{
        const c_res = this.state.detalle_formulario.filter((item) => item.item_oee !== c_item);
        this.calcularTotales(c_res)
        this.setState({detalle_formulario: c_res})

    }

    // ===========================================================================
    eventoSeleccionarEmpleado = (datos) => {
        this.setState({
            showModal: {estado: false, tipo: ""},
            cod_empleado: datos.cod_empleado,
            empleado: datos.nombre
        })
    }

    eventoSeleccionarOf = (datos) =>{
        this.setState({
            showModal: {estado: false, tipo: ""},
            num_of_sap: datos.num_of,
            cod_articulo: datos.cod_articulo,
            articulo: datos.articulo,
            std_und_hora: 0,
            cod_PasoTemp: 0,
            id_equipo: 0,
        })

        // this.handleFindNumPaso(datos.cod_articulo)
        // this.handleFindMaquinas(datos.cod_articulo, 0, 0)

        this.state.listIndicadores[2]['cantidad'] = 0



    }

    // ===========================================================================

    handleFindTipoRegistro = async (cod_subarea) =>{
        const responseJson = await get_oee_tipo_registro("0", "0", cod_subarea)
        this.setState({listTipo: responseJson})
    }


    handleFinCausasDefault = async () =>{
        const responseJson = await get_CausasDefault()
        console.log("66666666666666666666666666666666")
        console.log(responseJson.length)
        console.log("66666666666666666666666666666666")
        this.setState({detalle_formulario: responseJson, autoNumericItem: responseJson.length+1})

        this.calcularTotales(this.state.detalle_formulario)
        // responseJson.map((data, index) => {
        //     this.AddItem(data)
        // })
    }

    handleFind_OnlyMaquinas = async (cod_subarea) =>{
        const responseJson = await get_OnlyMaquinas(cod_subarea)
        this.setState({listOnlyMaquinas: responseJson})
    }

    // handleFindMaquinas = async  (cod_articulo, nro_paso, cod_tipo_proceso) =>{
    //     // const responseJson = await get_config_proceso_det_articulo(cod_articulo)
    //     // this.setState({listPaso: responseJson})
    //     const responseJson = await get_Maquinas_x_Paso(cod_articulo, nro_paso, cod_tipo_proceso)
    //     this.setState({listPaso: responseJson})
    // }

    handleFindTurnos = async (cod_subarea) =>{
        const responseJson = await get_periodo_laboral_turno(cod_subarea)
        this.setState({listTurno: responseJson})
    }

    handleFindHoraIniFin = async (cod_turno) =>{
        const responseJson = await get_turno_det(cod_turno)

        let c_dia = new Date(this.state.fecha_doc).getDay() + 1;
        let filtrarDia = responseJson.filter(turno => turno.num_dia === c_dia);
        let c_minuto = this.calcular_minutos(filtrarDia, this.state.fecha_doc);

        this.setState({
            hora_ini: filtrarDia[0].hora_ini,
            hora_fin: filtrarDia[0].hora_fin,
            // hora_plan: c_minuto
            hora_plan: (filtrarDia[0].cant_hora_lab*60)
        })

    }

    handleFindSubArea = async () =>{
        const responseJson = await get_subarea("00001")
        this.setState({listArea: responseJson})
    }

    handleFindIndicadores = async () =>{
        const responseJson = await get_indicadores()

        let responseStandarizado = {}
        responseJson.map((data, index) => {
            // const id = data.id_config_oee
            responseStandarizado[data.id_config_oee] = {
                id_oee: 0,
                item_indicador_oeee: 0,
                id_config_oee: data.id_config_oee,
                causa: data.descripcion,
                cantidad: 0,
                motivo_causa: "",
                tipo_registro: "OEE",
                umu: data.umu,
                cod_tipo_registro: "5",
                cod_motivo_indicador: 0
            }
        })

        this.setState({listIndicadores: responseStandarizado})
        // this.setState({indicadores_formulario: []})

    }

    // handleFindNumPaso = async (cod_maquina) =>{
    //     const responseJson = await get_NumPaso(cod_maquina)
    //     this.setState({listPasosProcesos: responseJson})
    //     console.log("PEPITOOOO")
    //     console.log(responseJson)
    // }

    // ===========================================================================


    // ===========================================================================
    handleUpdate = async () =>{
        const dataUpdate = {
            "id_oee": this.state.id_oee,
            "fecha_doc": Date.parse(this.state.fecha_doc),
            "cod_subarea": this.state.cod_subarea,
            // "num_of_sap": this.state.num_of_sap,
            // "cod_empleado": this.state.cod_empleado,
            // "cod_articulo": this.state.cod_articulo,
            // "articulo": this.state.articulo,
            // "num_paso": this.state.num_paso,
            "id_equipo": this.state.id_equipo,
            // "id_tpoperacion": this.state.id_tpoperacion,
            "cod_turno": this.state.cod_turno,
            "hora_ini": this.state.hora_ini,
            "hora_fin": this.state.hora_fin,
            // "std_und_hora": this.state.std_und_hora,
            "hora_planificado": this.state.hora_plan,
            "horas_operativo": this.state.hora_operativo,
            "produccion_total": this.state.produccion_total,
            "horas_perdidas": this.state.hora_perdida ,

            "hora_pausas": this.state.hora_pausas,
            "hora_paradas": this.state.hora_paradas,

            "sw_estado": 1,
            "detalle": this.state.detalle_formulario,
            "indicadores": Object.values(this.state.listIndicadores)
        }




        if (Number(this.state.hora_plan) != (Number(this.state.hora_operativo)+Number(this.state.hora_perdida))){
            alert("no coincide horas")
            // return
        }



        this.setState({op_OK: false})

        console.log("$$$$$$$")
        // console.log(this.state.listIndicadores)
        // console.log(Object.values(this.state.listIndicadores))
        // console.log(this.state.detalle_formulario)
        console.log(dataUpdate)
        const res = await update_oeeEficiencia(dataUpdate)
        //console.log(res)
        //console.log("$$$$$$$")
        alert("Grabado Satisfactoriamente")
    }


    handleSave = async () => {
        const dataSave = {
            "id_oee": null,
            "fecha_doc": Date.parse(this.state.fecha_doc),
            "cod_subarea": this.state.cod_subarea,
            // "num_of_sap": this.state.num_of_sap,
            // "cod_empleado": this.state.cod_empleado,
            // "cod_articulo": this.state.cod_articulo,
            // "articulo": this.state.articulo,
            // "num_paso": this.state.num_paso,
            "id_equipo": this.state.id_equipo,
            // "id_tpoperacion": this.state.id_tpoperacion,
            "cod_turno": this.state.cod_turno,
            "hora_ini": this.state.hora_ini,
            "hora_fin": this.state.hora_fin,
            // "std_und_hora": this.state.std_und_hora,
            "hora_planificado": this.state.hora_plan,
            "horas_operativo": this.state.hora_operativo,
            "produccion_total": this.state.produccion_total,
            "horas_perdidas": this.state.hora_perdida ,

            "hora_pausas": this.state.hora_pausas,
            "hora_paradas": this.state.hora_paradas,

            "sw_estado": 1,
            "detalle": this.state.detalle_formulario,
            "indicadores": Object.values(this.state.listIndicadores)
        }

        let ListaErrores = {
            "id_oee": null,
            "fecha_doc": "Fecha Emision",
            "cod_subarea": "Sub Area",
            // "num_of_sap": "Nro. Fabricacion",
            // "cod_empleado": "Empleado",
            // "cod_articulo": "Articulo",
            // "articulo": "Articulo",
            // "num_paso": "Equipo",
            "id_equipo": "Equipo",
            // "id_tpoperacion": "Equipo",
            "cod_turno": "Turno",
            "hora_ini": "Hora inicial",
            "hora_fin": "Hora Final",
            // "std_und_hora": "Detalle",
            "hora_planificado": "Detalle",
            "horas_operativo": "Horas Operativo",
            "produccion_total": "produccion total",
            "sw_estado": 1,
            "detalle": "Detalle",
            "indicadores": "Detalle"
        }

        if (Number(this.state.hora_plan) != (Number(this.state.hora_operativo)+Number(this.state.hora_perdida))){
            alert("no coincide horas")
            // return
        }

        for(const [key, value] of Object.entries(dataSave)){

            if (key==='detalle' && value.length<=0){
                alert(key)
                alert("Error en " + ListaErrores[key])
                return
            }

            if (value===0 && key!='horas_perdidas' && key!='hora_paradas' && key!='hora_pausas' && key!='horas_operativo' && key!='produccion_total'){
                // alert(key)
                alert("Error en " + ListaErrores[key])
                return
            }

        }


        this.setState({op_OK: false})

        // console.log("$$$$$$$")
        // console.log(dataSave)
        const res = await insert_oeeEficiencia(dataSave)
        alert(res)
        alert("Grabado Satisfactoriamente")

        // console.log("$$$$$$$")

    }
    // ===========================================================================
    calcular_minutos = (c_datos_dia, fecha_doc) =>{
        let c_fecha_ini = new Date(fecha_doc + " " + c_datos_dia[0].hora_ini);
        let c_fecha_fin = new Date(fecha_doc + " " + c_datos_dia[0].hora_fin);
        let c_minuto = 0;
        if (c_fecha_fin > c_fecha_ini) {
            c_minuto = Math.round((c_fecha_fin - c_fecha_ini) / 60000);
        }
        return c_minuto;
    }

    // ===========================================================================
    calcularTotales = (DetalleReg, V_std_und_hora=this.state.std_und_hora) => {
        // INICIALIZAR
        var minutosPerdidos = 0
        var minutosPausas = 0
        var minutosParadas = 0

        var minutosOperativos = 0
        var produccionBuena = 0
        var produccionMala = 0

        // Calculas Totales
        minutosPausas = DetalleReg.reduce((acumulador, item) => item.cod_tipo_registro === 2 ? acumulador + Number(item.tiempo_min) : acumulador, 0);
        minutosParadas = DetalleReg.reduce((acumulador, item) => item.cod_tipo_registro === 1 ? acumulador + Number(item.tiempo_min) : acumulador, 0);
        minutosPerdidos = DetalleReg.reduce((acumulador, item) => item.cod_tipo_registro === 1 || item.cod_tipo_registro === 2 ? acumulador + Number(item.tiempo_min) : acumulador, 0);
        // minutosOperativos = Number(this.state.hora_plan) > minutosPerdidos ? (Number(this.state.hora_plan) - minutosPerdidos) || 0 : 0;
        minutosOperativos = DetalleReg.reduce((acumulador, item) => [3].includes(item.cod_tipo_registro) ? acumulador + Number(item.tiempo_min) : acumulador, 0);

        produccionBuena = DetalleReg.reduce((acumulador, item) => item.cod_tipo_registro === 3 ? acumulador + Number(item.cantidad_total) : acumulador, 0);
        produccionMala = DetalleReg.reduce((acumulador, item) => item.cod_tipo_registro === 4 ? acumulador + Number(item.cantidad_total) : acumulador, 0);
        // minutosPerdidos = this.state.detalle_formulario.reduce((acumulador, item) => item.cod_tipo_registro === 1 || item.cod_tipo_registro === 2 ? acumulador + Number(item.cantidad) : acumulador, 0);
        // minutosOperativos = Number(this.state.hora_plan) > minutosPerdidos ? (Number(this.state.hora_plan) - minutosPerdidos) || 0 : 0;
        //
        // produccionBuena = this.state.detalle_formulario.reduce((acumulador, item) => item.cod_tipo_registro === 3 ? acumulador + Number(item.cantidad) : acumulador, 0);
        // produccionMala = this.state.detalle_formulario.reduce((acumulador, item) => item.cod_tipo_registro === 4 ? acumulador + Number(item.cantidad) : acumulador, 0);

        // console.log("¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿")
        // console.log(DetalleReg)
        // console.log(produccionBuena)
        // console.log(produccionMala)
        // console.log(minutosPerdidos)
        // console.log("¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿")

        const produccionTotal = produccionBuena + produccionMala

        // Calculas Indicadores
        // const horaLogrado = Number(this.state.std_und_hora) > 0 ? Math.round((produccionTotal / Number(this.state.std_und_hora)) * 60) : 0;
        // const horaLogrado = Number(this.state.std_und_hora) > 0 ? Math.round(produccionTotal / ((Number(this.state.std_und_hora) / 60) * Number(minutosOperativos))*100, 2) : 0;
        const horaLogrado = Number(V_std_und_hora) > 0 ? Math.round(produccionTotal / ((Number(V_std_und_hora) / 60) * Number(minutosOperativos))*100, 2) : 0;

        const oeeDisponibilidad = Number(this.state.hora_plan) > 0 ? minutosOperativos/Number(this.state.hora_plan) : 0
        const oeeRendimiento = minutosOperativos > 0 ? horaLogrado: 0
        const oeeCalidad = produccionTotal > 0 ? produccionBuena/produccionTotal : 0
        const oeeOee = (oeeDisponibilidad*oeeRendimiento*oeeCalidad) || 0

        //------------------------

        // this.state.listIndicadores[1]['cantidad'] = oeeDisponibilidad
        this.state.listIndicadores[2]['cantidad'] = oeeRendimiento
        // this.state.listIndicadores[3]['cantidad'] = oeeCalidad
        // this.state.listIndicadores[4]['cantidad'] = oeeOee

        this.setState({hora_operativo: minutosOperativos, produccion_total: produccionTotal, hora_perdida: minutosPerdidos, hora_pausas:minutosPausas, hora_paradas: minutosParadas})

        // console.log(this.state.produccion_total)

    }



    // ===========================================================================
    handleRefreshIndicadores = (codigoEdit, motivo, id_config_oee) => {
        this.state.listIndicadores[id_config_oee]['motivo_causa'] = motivo
        this.state.listIndicadores[id_config_oee]['cod_motivo_indicador'] = codigoEdit


        // console.log(this.state.listIndicadores[id_config_oee])

    }

    handleChange = (e) =>{

        if (e.target.name === 'fecha_doc'){
            console.log(e.target.value)
            this.setState({fecha_doc: e.target.value})
        }


        if (e.target.name === 'cod_subarea'){

            const c_res = this.state.detalle_formulario.filter((item) => item.cod_tipo_registro == 2);
            // console.log(this.state.detalle_formulario)

            this.setState({
                cod_subarea: e.target.value,
                num_of_sap: 0,
                cod_articulo: 0,
                articulo: "",
                std_und_hora: 0,
                cod_PasoTemp: 0,
                id_equipo: 0,
                cod_turno: 0,
                cod_empleado: 0,
                empleado: "",
                detalle_formulario: c_res,
            })
            this.handleFindTurnos(e.target.value)
            this.calcularTotales(c_res, 0)

            this.handleFindTipoRegistro(e.target.value)

            this.handleFind_OnlyMaquinas(e.target.value)

        }

        if (e.target.name === 'cod_turno'){
            this.setState({cod_turno: e.target.value})
            this.handleFindHoraIniFin(e.target.value)
        }

        // if (e.target.name === 'num_paso'){
        //     let filtrarDatos = this.state.listPasosProcesos.filter(machine => machine.codigo === e.target.value)
        //
        //     if (filtrarDatos.length === 0) {
        //         return
        //     }
        //
        //     this.setState({num_paso: filtrarDatos[0].num_paso, cod_PasoTemp: e.target.value, id_tpoperacion: filtrarDatos[0].id_tpoperacion})
        //
        //     this.handleFindMaquinas(this.state.cod_articulo, filtrarDatos[0].num_paso, filtrarDatos[0].id_tpoperacion)
        // }


        if (e.target.name === 'id_paso_equipo'){

            this.setState({
                id_equipo: e.target.value,
            })

            // this.calcularTotales(this.state.detalle_formulario, filtrarDatos[0].std_und_hora)

        }

        if (e.target.name === 'hora_plan'){
            this.setState({hora_plan: e.target.value})
            this.calcularTotales(this.state.detalle_formulario)
        }

    }

    selected_Indicador = (id_config_oee) =>{
        this.setState({
            showModal: {estado: true, tipo: "oee"},
            id_config_oee_SELECTED: id_config_oee
        })
    }

    tutu = () =>{
        //console.log("#####")
        //console.log(this.state)

        //console.log(this.state.fecha_doc)
        //console.log(this.props.regEdit.fecha_doc.split("T")[0])
        // 2024-02-02T00:00:00

        //console.log("#####")
    }

    render() {
        return(

            <div className="col-lg-12">
                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border-0 p-0">
                        <div className="mx-1 my-2 row">
                            <div className="col-xl-6 col-sm-6 p-1">
                                <h4 className="card-title mb-0">Eficiencia Productiva OEE</h4>
                            </div>
                            <div className="col-xl-6 col-sm-6 p-1">
                                <div className="input-group">
                                    <ComboBoxForm2
                                        nombre_cbo="cod_subarea"
                                        valor_ini="*** Area ***"
                                        datosRow={this.state.listArea}
                                        manejaEvento={this.handleChange}
                                        valor={this.state.cod_subarea}
                                        view={this.state.tipoOperacion==="Grabar" ? false : true}
                                    />
                                    <span className="input-group-text fw-bold">Fecha Doc:</span>
                                    <input
                                        type="date"
                                        min="2020-01-01"
                                        name="fecha_doc"
                                        className="form-control"
                                        onChange={this.handleChange}
                                        value={this.state.fecha_doc}
                                        disabled={this.state.tipoOperacion==="Grabar" ? false : true}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* FIN BARRA DE NAVEGACION */}

                    <div className="p-0 card-body">
                        <div className="mx-1 my-2 row">
                            {/*<div className="col-xl-4 col-sm-4 p-1">*/}
                            {/*    <div className="input-group">*/}
                            {/*        <input*/}
                            {/*            type="text"*/}
                            {/*            className="form-control"*/}
                            {/*            placeholder="N° Fabricación SAP"*/}
                            {/*            onChange={this.handleChange}*/}
                            {/*            value={this.state.num_of_sap}*/}
                            {/*            name="num_of_sap"*/}
                            {/*            // disabled={this.state.tipoOperacion === "Grabar" ? false : true}*/}
                            {/*            disabled={true}*/}
                            {/*        />*/}
                            {/*        <button*/}
                            {/*            className="btn btn-primary"*/}
                            {/*            onClick={() => this.setState({showModal: {estado: true, tipo: "prd"}})}*/}
                            {/*            // disabled={this.state.tipoOperacion === "Grabar" ? false : true}*/}
                            {/*        >*/}
                            {/*            <BsSearch/>*/}
                            {/*        </button>*/}
                            {/*    </div>*/}
                            {/*</div>*/}


                            {/*<div className="col-xl-4 col-sm-4 p-1">*/}
                            {/*    <div className="input-group">*/}
                            {/*        <input type="text" className="form-control" placeholder="Artículo"*/}
                            {/*               onChange={this.handleChange} value={this.state.articulo}*/}
                            {/*               name="articulo"*/}
                            {/*               disabled={true}*/}
                            {/*        />*/}
                            {/*        <button className="btn btn-primary"*/}
                            {/*                onClick={() => this.setState({showModal: {estado: true, tipo: "art"}})}*/}
                            {/*                disabled={true}*/}
                            {/*        >*/}
                            {/*            <BsSearch/></button>*/}
                            {/*    </div>*/}
                            {/*</div>*/}

                            {/*<div className="col-xl-4 col-sm-4 p-1">*/}
                            {/*    <div className="input-group">*/}
                            {/*        <input type="text" className="form-control" placeholder="Colaborador"*/}
                            {/*               onChange={this.handleChange} value={this.state.empleado}*/}
                            {/*               name="empleado"*/}
                            {/*               disabled={true}*/}
                            {/*        />*/}
                            {/*        <button className="btn btn-primary"*/}
                            {/*                onClick={() => this.setState({showModal: {estado: true, tipo: "emp"}})}*/}
                            {/*                // disabled={this.state.tipoOperacion === "Grabar" ? false : true}*/}
                            {/*        >*/}
                            {/*            <BsSearch/></button>*/}
                            {/*    </div>*/}
                            {/*</div>*/}

                            <div className="col-xl-6 col-sm-6 p-1">
                                <ComboBoxForm2
                                    datosRow={this.state.listTurno}
                                    nombre_cbo="cod_turno"
                                    manejaEvento={this.handleChange}
                                    valor_ini="*** Turno ***"
                                    valor={this.state.cod_turno}
                                    // view={this.state.tipoOperacion === "Grabar" ? false : true}
                                />
                            </div>

                            {/*<div className="col-xl-4 col-sm-4 p-1">*/}
                            {/*    <div className="input-group">*/}
                            {/*        <span className="input-group-text">Nro Paso:</span>*/}
                            {/*        <ComboBoxForm2*/}
                            {/*            datosRow={this.state.listPasosProcesos}*/}
                            {/*            nombre_cbo="num_paso"*/}
                            {/*            manejaEvento={this.handleChange}*/}
                            {/*            valor_ini="*** Num Paso/Operacion ***"*/}
                            {/*            valor={this.state.cod_PasoTemp}*/}
                            {/*            // view={this.state.tipoOperacion === "Grabar" ? false : true}*/}
                            {/*        />*/}
                            {/*    </div>*/}
                            {/*</div>*/}

                            <div className="col-xl-6 col-sm-6 p-1">
                                <div className="input-group">
                                    <span className="input-group-text">Maquina:</span>
                                    <ComboBoxForm2
                                        datosRow={this.state.listOnlyMaquinas}
                                        nombre_cbo="id_paso_equipo"
                                        manejaEvento={this.handleChange}
                                        valor_ini="*** Prioridad/Maquina ***"
                                        valor={this.state.id_equipo}
                                        // view={this.state.tipoOperacion === "Grabar" ? false : true}
                                    />
                                </div>
                            </div>

                            {/*<div className="col-xl-4 col-sm-4 p-1">*/}
                            {/*    <div className="input-group">*/}
                            {/*        <span className="input-group-text">Hora Ini:</span>*/}
                            {/*        <input type="time" style={{minWidth: '75px'}} name="hora_ini"*/}
                            {/*               className="form-control"*/}
                            {/*               value={this.state.hora_ini}*/}
                            {/*               disabled={this.state.tipoOperacion==="Grabar" ? false : true}*/}
                            {/*        />*/}
                            {/*    </div>*/}
                            {/*</div>*/}

                            {/*<div className="col-xl-4 col-sm-4 p-1">*/}
                            {/*    <div className="input-group">*/}
                            {/*        <span className="input-group-text">Hora Fin:</span>*/}
                            {/*        <input type="time" style={{minWidth: '75px'}} name="hora_fin"*/}
                            {/*               className="form-control"*/}
                            {/*               value={this.state.hora_fin}*/}
                            {/*               disabled={this.state.tipoOperacion === "Grabar" ? false : true}*/}
                            {/*        />*/}
                            {/*    </div>*/}
                            {/*</div>*/}

                            <div className="col-xl-12 col-sm-12 p-1">
                            {/*<div className="mx-0 my-2 py-2 px-1 row border border-dashed border-end-0 border-start-0 bg-light">*/}

                                {/*<div className="col-sm p-1">*/}
                                {/*    <ComboBoxForm2*/}
                                {/*        datosRow={this.state.listPaso}*/}
                                {/*        nombre_cbo="id_paso_equipo"*/}
                                {/*        manejaEvento={this.handleChange}*/}
                                {/*        valor_ini="*** Paso/Estación/Maquina ***"*/}
                                {/*        valor={this.state.id_paso_equipo}*/}
                                {/*        view={this.state.tipoOperacion === "Grabar" ? false : true}*/}
                                {/*    />*/}
                                {/*</div>*/}
                                <div className="col-sm-auto p-1">
                                    <div className="d-flex flex-row-revers" style={{alignItems: "center", justifyContent: "center", display: "flex", margin: "5px 0"}}>
                                        <button
                                            className="btn btn-warning mx-1"
                                            style={{width: "90%", height: "40px"}}
                                            onClick={() => this.setState({showModal: {estado: true, tipo: "det"}})}
                                            disabled={!this.state.op_OK}>
                                            <BsPlusLg/> Agregar
                                        </button>
                                        {/*<button*/}
                                        {/*    className="btn btn-warning mx-1"*/}
                                        {/*    style={{width: "10%", height: "40px"}}*/}
                                        {/*    onClick={() => this.setState({showModal: {estado: true, tipo: "qr"}})}*/}
                                        {/*>*/}
                                        {/*    QR*/}
                                        {/*</button>*/}
                                    </div>
                                </div>
                            </div>

                            {/*<div>*/}
                            {/*    <button onClick={this.tutu}>mensaje</button>*/}
                            {/*</div>*/}

                            { /* INICIO TABLA */}
                            <EficienciaOperativa_crud_tabla
                                datos_fila={this.state.detalle_formulario.filter(det => det.cod_tipo_registro.toString() != "5")}
                                eventoClick={this.DeleteItem}/>
                            {/* FIN TABLA */}

                            <div className="mx-1 my-2 row">
                                <div className="col-xl-4 col-sm-4 p-1">
                                    <div className="table-responsive">
                                        <table className="table table-hover table-bordered">
                                            <tbody>
                                            {/*<tr>*/}
                                            {/*    <td className="align-middle" title="Estandar unidades por hora">Estandar Und.X.Hora</td>*/}
                                            {/*    <td className="td-num" title="Unidad de medida de uso" style={{width: '100px'}}>{this.state.std_und_hora}</td>*/}
                                            {/*</tr>*/}
                                            <tr>
                                                <td className="align-middle" title="Producción total (Unidades)" style={{color: "blue"}}>Producción Total (Und)</td>
                                                <td className="td-num" title="Unidad de medida de uso" style={{width: '100px', color: "blue"}}>{this.state.produccion_total}</td>
                                            </tr>
                                            <tr>
                                                <td className="align-middle" title="Horas Planificadas (Minutos)" style={{color: "red"}}>Tiempo Planificado (Min)</td>
                                                <td className="td-num" title="Valor en Minutos"
                                                    style={{width: '100px'}}>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        placeholder="Horas Planificadas"
                                                        style={{margin: "0", padding: "0", width: "100%", color: "red"}}
                                                        value={this.state.hora_plan}
                                                        onChange={this.handleChange}
                                                        name="hora_plan"
                                                    />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td className="align-middle" title="Tiempo Operativo (Minutos)" style={{color: "blue"}}>Tiempo Operativo (Min)</td>
                                                <td className="align-middle td-num" title="Valor en Minutos" style={{width: '100px', color: "blue"}}>{this.state.hora_operativo}</td>
                                            </tr>
                                            <tr>
                                                <td className="align-middle" title="Tiempo Perdido (Minutos)">Tiempo Pausa (Min)</td>
                                                <td className="align-middle td-num" title="Valor en Minutos" style={{width: '100px'}}>{this.state.hora_pausas}</td>
                                            </tr>
                                            <tr>
                                                <td className="align-middle" title="Tiempo Perdido (Minutos)">Tiempo Parada (Min)</td>
                                                <td className="align-middle td-num" title="Valor en Minutos" style={{width: '100px'}}>{this.state.hora_paradas}</td>
                                            </tr>
                                            {/*<tr>*/}
                                            {/*    <td className="align-middle" title="Tiempo Perdido (Minutos)">Tiempo Perdido (Min)</td>*/}
                                            {/*    <td className="align-middle td-num" title="Valor en Minutos" style={{width: '100px'}}>{this.state.hora_perdida}</td>*/}
                                            {/*</tr>*/}
                                            <tr>
                                                <td className="align-middle" title="Tiempo total (Minutos)" style={{color: "red"}}>Tiempo Total (Min)</td>
                                                <td className="align-middle td-num" title="Valor en Minutos" style={{width: '100px', color: "red"}}>{this.state.hora_perdida + this.state.hora_operativo}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                {/*<div className="col-xl-8 col-sm-8 p-1">*/}
                                {/*    <EficienciaOperativa_Indicadores dataIndicadores={this.state.listIndicadores}*/}
                                {/*                                     eventEditDetalle={this.selected_Indicador}/>*/}
                                {/*</div>*/}
                            </div>

                            <div className="col-xl-12 col-sm-12 p-1">
                                <div className="col-sm-auto p-1">
                                    <div className="d-flex flex-row-revers" style={{alignItems: "center", justifyContent: "center", display: "flex", margin: "5px 0"}}>
                                        {this.state.tipoOperacion === 'Grabar' ?
                                            <BotonGrabar2
                                                textoBoton={"Grabar"}
                                                eventoClick={this.handleSave}
                                                sw_habilitado={this.state.op_OK}
                                                style={{width: "45%", height: "70px"}}
                                            />
                                            :
                                            <BotonGrabar2
                                                textoBoton={"Guardar\nCambios"}
                                                eventoClick={this.handleUpdate}
                                                sw_habilitado={this.state.op_OK}
                                                style={{width: "40%", height: "70px"}}
                                            />
                                        }

                                        {typeof this.props.eventClose != 'undefined' ?
                                            <button
                                                className="btn btn-secondary mx-1"
                                                onClick={this.props.eventClose}
                                                style={{width: "40%", height: "70px"}}
                                            >
                                                Terminar
                                            </button>
                                            :
                                            <NavLink
                                                className='btn btn-secondary  mx-1'
                                                to="/oee"
                                                style={{width: "45%", height: "70px", paddingTop: "20px"}}
                                            >
                                                Terminar
                                            </NavLink>
                                        }
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>


                </div>


                <VentanaModal
                    show={this.state.showModal.estado}
                    size="lg"
                    handleClose={() => this.setState({showModal: {estado: false, tipo: ""}})}
                    titulo="Edicion Area">
                    {
                        this.state.showModal.tipo === 'qr' ? <LectorQR/> :
                        this.state.showModal.tipo === 'det' ? <EficienciaOperativa_Edit_Add eventoClick={this.AddItem} cod_subarea={this.state.cod_subarea} listTipo={this.state.listTipo} eventoClose={() => this.setState({showModal: {estado: false, tipo: ""}})}  id_equipo={this.state.id_equipo} eventMantenerInfoTemp={this.TempData_AddItem} DataTemp={this.state.TempAdd}/> :
                            this.state.showModal.tipo === 'emp' ? <EmpleadoBuscar eventoClick={this.eventoSeleccionarEmpleado} cod_subarea={this.state.cod_subarea}/> :
                                this.state.showModal.tipo === 'prd' ? <OrdenFabricacionBuscar eventoClick={this.eventoSeleccionarOf} cod_subarea={this.state.cod_subarea}/> :
                                    this.state.showModal.tipo === 'oee' ? <EficienciaOperativa_Indicadores_Edit eventoClick={this.handleRefreshIndicadores} id_config_oee={this.state.id_config_oee_SELECTED} cod_subarea={this.state.cod_subarea} /> : <></>
                    }

                </VentanaModal>


            </div>


        )
    }


    async componentDidMount() {
        this.handleFindSubArea()

        if (typeof this.props.regEdit === 'undefined'){
            this.handleFindIndicadores()
        }


        if (this.state.cod_subarea > 0) {
            this.handleFindTurnos(this.state.cod_subarea)
            this.handleFindTipoRegistro(this.state.cod_subarea)
            this.handleFind_OnlyMaquinas(this.state.cod_subarea)


        }

        if (this.state.cod_articulo != ''){
            // this.handleFindMaquinas(this.state.cod_articulo)
            // this.handleFindNumPaso(this.state.cod_articulo)
            // this.handleFindMaquinas(this.state.cod_articulo, this.state.num_paso, this.state.id_tpoperacion)
        }

        if (typeof this.props.tipoOperacion === 'undefined'){
            // this.setState({detalle_formulario: []})
            this.handleFinCausasDefault()
            console.log("")
        }



    }


}

export default EficienciaOperativa_Edit;
