
import React from "react";

import {ComboBoxForm, ComboBoxForm3} from "../../components/ComboBox";
import {BotonBuscar} from "../../components/Botones.jsx";
import {BsPlusLg, BsSearch} from "react-icons/bs";

import {get_RegistrosEmpleado} from "./services/oeeEmpleadosServices.jsx"
import {find_oeeEficiencia, find_oee_EfienciaDet, delete_oeeEficiencia} from "./services/oeeEficienciaServices"

import EficienciaOperativa_Edit from "./EficienciaOperativa_Edit.jsx"

import EficienciaOperativa_Tabla from "./components/EficienciaOperativa_Tabla"

import {NavLink} from "react-router-dom";



// const Login1 = sessionStorage.getItem("CDTToken")


class EficienciaOperativa extends React.Component{

    constructor(props) {
        super(props);

        const fechaSistema = new Date(Date.now())

        this.state = {
            tipo_filtro: [{"codigo": 'MAQUINA', "nombre": 'MAQUINA'}, {"codigo": 'EMPLEADO', "nombre": 'EMPLEADO'}],
            nom_tipo_filtro: '',

            resultados: [],
            // fechaSistema: new Date(Date.now()),
            txtInput: '',
            fecha: fechaSistema.toISOString().slice(0, 10),
            isFetch: true,

            regEdit: null,
            showComponente: 'principal',

            swPermisos: sessionStorage.getItem("CDTToken")

        }

    }


    handleBusqueda = async() => {
        const responseJson = await find_oeeEficiencia(this.state.fecha, this.state.nom_tipo_filtro, this.state.txtInput)
        this.setState({resultados: responseJson, isFetch: false})
        console.log("%%%%%%")
        console.log(responseJson)
        console.log(this.state.swPermisos)
        console.log("%%%%%%")
    }


    handleChange = (e) => {
        if (e.target.name === 'txt_empleado') {
            this.setState({txtInput: e.target.value})
            console.log("444444444")
            console.log(this.state.resultados)
        }

        if (e.target.name === 'fecha_doc') {
            this.setState({fecha: e.target.value})
        }

        if (e.target.name === 'tipo_filtro') {
            this.setState({nom_tipo_filtro: e.target.value})
        }
    }


    handleDelete = async(P_id_oee) =>{
        const responseJson = await delete_oeeEficiencia(P_id_oee)

        console.log(P_id_oee)
        console.log(this.state.swPermisos)
        alert(responseJson['respuesta'])
        this.handleBusqueda()

    }


    handleEdit = async(regCab) => {
        const responseJson = await find_oee_EfienciaDet(regCab.id_oee)
        regCab['detalle'] = responseJson['detalle']

        let responseStandarizado = {}
        responseJson['listIndicadores'].map((data, index) => {
            // const id = data.id_config_oee
            responseStandarizado[data.id_config_oee] = {
                id_oee: data.id_oee,
                item_indicador_oeee: data.item_indicador_oeee,
                id_config_oee: data.id_config_oee,
                causa: data.causa,
                cantidad: data.cantidad,
                motivo_causa: data.motivo_causa,
                tipo_registro: data.tipo_registro,
                umu: data.umu,
                cod_tipo_registro: data.cod_tipo_registro,
                cod_motivo_indicador: data.cod_motivo_indicador
            }
        })


        // regCab['listIndicadores'] = responseJson['listIndicadores']
        regCab['listIndicadores'] = responseStandarizado

        console.log("99999999999999999")
        const ids = regCab['detalle'].map(object => {return object.item_oee})
        const max = Math.max(...ids)
        regCab['autoNumericItem'] = max + 1
        console.log(regCab)
        this.setState({showComponente: "Editar", regEdit: regCab})
    }

    // ===========================================================================
    terminar = () =>{
        this.setState({showComponente: "principal"})
    }


    
    render() {
        return(
            <React.Fragment>
                {this.state.showComponente === 'principal' ?
                    <div className="col-lg-12">
                        <div className="card">

                            <div className="card-header border-0 p-0">
                                <div className="mx-1 my-2 row">
                                    <div className="col-xl-2 col-sm-2 p-1" style={{minWidth: "20%"}}>
                                        <h4 className="card-title mb-0" style={{marginTop: "15px", fontSize: "22px"}}>Eficiencia Productiva OEE</h4>
                                    </div>
                                    <div className="col-xl-8 col-sm-8 p-1" style={{minWidth: "80%"}}>
                                        <div className="input-group" >

                                            <span className="input-group-text fw-bold" style={{marginTop: "10px"}}>Fecha:</span>
                                            <input type="date" value={this.state.fecha} name="fecha_doc" onChange={this.handleChange} className="form-control" style={{width: "50px", paddingLeft: "1px", marginTop: "10px", marginRight: "10px"}} disabled={true} />

                                            <ComboBoxForm3
                                                datosRow={this.state.tipo_filtro}
                                                nombre_cbo="tipo_filtro"
                                                manejaEvento={this.handleChange}
                                                valor_ini="Tipo Filtro"
                                                valor={this.state.nom_tipo_filtro}
                                            />

                                            <input placeholder="Ingrese Maquina" onChange={this.handleChange} value={this.state.txtInput} name="txt_empleado" style={{marginLeft: "10px", width: "150px", marginTop: "10px"}}/>

                                            <button className="btn btn-primary" onClick={this.handleBusqueda} style={{marginTop: "10px", marginRight: "10px"}}><BsSearch/></button>

                                            <NavLink className='btn btn-primary mx-1' to="/oee/edit" style={{marginLeft: "10px", marginRight: "10px", width: "170px", marginTop: "10px"}}>Nuevo Registro</NavLink>
                                            {this.state.swPermisos == "" || this.state.swPermisos == null ?
                                                ''
                                                :
                                                <NavLink className='btn btn-success mx-1' to="/oee/export" style={{marginLeft: "10px", marginRight: "10px", width: "170px", marginTop: "10px"}}>Exportar Data</NavLink>
                                            }
                                            <NavLink className='btn btn-danger mx-2' to="/oee/stopmachine" style={{marginLeft: "5px", width: "220px", marginTop: "10px"}}>Reg. Maquinas Detenidas</NavLink>

                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div className="p-0 card-body">
                                <EficienciaOperativa_Tabla dataRegistros={this.state.resultados}
                                                           eventoClick={this.handleEdit} eventoDelete={this.handleDelete} swPermisos={this.state.swPermisos}/>
                            </div>

                        </div>


                    </div>
                    :
                    <EficienciaOperativa_Edit regEdit={this.state.regEdit} tipoOperacion="Editar"
                                              eventClose={this.terminar}/>

                }
            </React.Fragment>
        )
    }


}

export default EficienciaOperativa;



