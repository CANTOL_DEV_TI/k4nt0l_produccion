import React, { useState, useEffect } from "react"; 
import { get_turno_activo } from '../../../services/turnoServices'; 
import { get_subarea_operacion } from '../../../services/subAreaServices'; 
import { ComboBoxForm } from "../../../components/ComboBox"; 
 
const fecha = new Date(); 
const iniFormulario = { 
    id_per_lab: "0", 
    ejercicio: fecha.getFullYear(), 
    periodo: "0", 
    cod_subarea: "0", 
    cod_turno: "0", 
    total_dia_lab: "0", 
    sw_estado: "1", 
    cod_usuario: "1", 
}; 
 
// CARGAR MES 
const listPeriodo = [ 
    { "codigo": "1", "nombre": "Enero" }, 
    { "codigo": "2", "nombre": "Febrero" }, 
    { "codigo": "3", "nombre": "Marzo" }, 
    { "codigo": "4", "nombre": "Abril" }, 
    { "codigo": "5", "nombre": "Mayo" }, 
    { "codigo": "6", "nombre": "Junio" }, 
    { "codigo": "7", "nombre": "Julio" }, 
    { "codigo": "8", "nombre": "Agosto" }, 
    { "codigo": "9", "nombre": "Septiembre" }, 
    { "codigo": "10", "nombre": "Octubre" }, 
    { "codigo": "11", "nombre": "Noviembre" }, 
    { "codigo": "12", "nombre": "Diciembre" } 
] 
 
export function PeriodoLaboral_crud({ createData, updateData, dataToEdit, setDataToEdit, sw_mode = "C" }) { 
    // C: CREAR , M: MODIFICAR    
 
    // Asignar a variables de estado     
    const [formulario, setFormulario] = useState(iniFormulario); 
    const [checked, setChecked] = useState(false); 
    useEffect(() => { 
        if (dataToEdit) { 
            if (dataToEdit.sw_estado === "1") { 
                setChecked(true); 
            } else { 
                setChecked(false); 
            } 
            setFormulario(dataToEdit); 
        } else { 
            setChecked(true); 
            setFormulario(iniFormulario); 
        } 
    }, [dataToEdit]); 
 
    // cargar combo sub area 
    const [subArea, setSubArea] = useState([]) 
    useEffect(() => { 
        get_subarea_operacion('1') 
            .then(res => { 
                const cbo_data = res.map(data => { 
                    return { 
                        "codigo": data.cod_subarea, "nombre": data.nom_subarea 
                    } 
                }) 
                setSubArea(cbo_data) 
            }) 
    }, []); 
 
    // cargar combo turno 
    const [turno, setTurno] = useState([]) 
    useEffect(() => { 
        get_turno_activo() 
            .then(res => { 
                const cbo_data = res.map(data => { 
                    return { 
                        "codigo": data.cod_turno, "nombre": data.nom_turno 
                    } 
                }) 
                setTurno(cbo_data) 
            }) 
    }, []); 
 
    // CAPTURAR EL EVENTO DE LOS IMPUTS 
    const handleChange = (evento) => { 
        if (evento.target.name === 'sw_estado') { 
            setChecked(!checked); 
            setFormulario({ 
                ...formulario, 
                [evento.target.name]: evento.target.checked === true ? "1" : "0", 
            }); 
        } else { 
            setFormulario({ 
                ...formulario, 
                [evento.target.name]: (evento.target.value).toUpperCase(), 
            }); 
        } 
    }; 
 
    // GUARDAR INFORMACION 
    const handleSubmit = (e) => { 
        if (formulario.periodo === "0") { 
            alert("Seleccionar Mes."); 
            return; 
        } 
        if (formulario.cod_subarea === "0") { 
            alert("Seleccionar Sub Area."); 
            return; 
        } 
        if (formulario.cod_turno === "0") { 
            alert("Seleccionar Turno."); 
            return; 
        } 
 
        if (formulario.total_dia_lab === "0") { 
            alert("Total días > 0."); 
            return; 
        } 
 
        // generar datos 
        const new_data = { 
            "id_per_lab": formulario.id_per_lab, 
            "ejercicio": formulario.ejercicio, 
            "periodo": formulario.periodo, 
            "cod_subarea": formulario.cod_subarea, 
            "cod_turno": formulario.cod_turno, 
            "total_dia_lab": formulario.total_dia_lab, 
            "sw_estado": formulario.sw_estado, 
            "cod_usuario": "1" 
        }; 
 
        if (sw_mode === "C") { 
            if (e.target.name === 'btn_all') { 
                createData(new_data, "ALL"); 
            } else { 
                createData(new_data,""); 
            } 
 
        } else { 
            updateData(new_data); 
        } 
 
        handleReset(); 
    }; 
 
    const handleReset = (e) => { 
        setFormulario(iniFormulario); 
        setDataToEdit(null); 
    }; 
 
    // RETORNAR INFORMACIÓN 
    return ( 
        <div className="row g-2"> 
            {/* INICIO  FORM*/} 
            <div className="col-md-2"> 
                <label className="form-label">ID</label> 
                <input type="text" readOnly className="form-control" name="id_per_lab" value={formulario.id_per_lab} /> 
            </div> 
            <div className="col-md-4"> 
                <label className="form-label">Año</label> 
                <input type="number" className="form-control" name="ejercicio" onChange={handleChange} value={formulario.ejercicio} /> 
            </div> 
            <div className="col-md-6"> 
                <label className="form-label">Mes</label> 
                <ComboBoxForm 
                    datosRow={listPeriodo} 
                    nombre_cbo="periodo" 
                    manejaEvento={handleChange} 
                    valor_ini="Seleccionar..." 
                    valor={formulario.periodo} 
                /> 
            </div> 
            <div className="col-md-8"> 
                <label className="form-label">Sub Area </label> 
                <ComboBoxForm 
                    datosRow={subArea} 
                    nombre_cbo="cod_subarea" 
                    manejaEvento={handleChange} 
                    valor_ini="Seleccionar..." 
                    valor={formulario.cod_subarea} 
                /> 
            </div> 
            <div className="col-md-4"> 
                <label className="form-label">Total días</label> 
                <input type="number" title="Total días Laborables del Mes" className="form-control" name="total_dia_lab" onChange={handleChange} value={formulario.total_dia_lab} /> 
            </div> 
            <div className="col-12"> 
                <label className="form-label">Turno Laboral</label> 
                <ComboBoxForm 
                    datosRow={turno} 
                    nombre_cbo="cod_turno" 
                    manejaEvento={handleChange} 
                    valor_ini="Seleccionar..." 
                    valor={formulario.cod_turno} 
                /> 
            </div> 
            <div className="col-12"> 
                <input className="form-check-input" type="checkbox" name="sw_estado" checked={checked} onChange={handleChange} /> 
                <label className="form-check-label ms-2"> 
                    Habilitado 
                </label> 
            </div> 
            <div className="modal-footer"> 
                {sw_mode === "C" ? 
                    <button type="submit" name="btn_all" className="btn btn-primary" onClick={handleSubmit}>Crear para todas las Sub Areas</button> : ''} 
                <button type="submit" className="btn btn-primary" onClick={handleSubmit}>{`${sw_mode === "C" ? 'Crear' : 'Actualizar'}`}</button> 
            </div> 
            {/* FIN FORM */} 
        </div> 
    ); 
}