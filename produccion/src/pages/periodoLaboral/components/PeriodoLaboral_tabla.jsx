import { BsPencilFill, BsXCircleFill } from "react-icons/bs"; 
 
export function PeriodoLaboral_tabla({ datos_fila, setDataToEdit, deleteData, eventoClick }) { 
    // RETORNAR INFORMACIÓN 
    return ( 
        <> 
            {/* INICIO TABLA */} 
            <div className="table-responsive"> 
                <table className="table table-hover table-sm table-bordered"> 
                    <thead className="table-secondary text-center table-sm"> 
                        <tr> 
                            <th className="align-middle">Año</th> 
                            <th className="align-middle">Mes</th> 
                            <th className="align-middle">Sub Area</th> 
                            <th className="align-middle">Turno</th> 
                            <th className="align-middle" title="Total de días laborables del mes">Días Laborables</th> 
                            <th className="align-middle" style={{ width: '100px' }}>Estado</th> 
                            <th className="align-middle" title="Opciones" style={{ width: '85px' }}>Opc</th> 
                        </tr> 
                    </thead> 
                    <tbody className="list"> 
                        {datos_fila.length > 0 ? 
                            ( 
                                datos_fila.map((datos, index) => { 
                                    return <tr key={index + 1}> 
                                        <td className="td-cadena">{datos.ejercicio}</td> 
                                        <td className="td-cadena">{datos.periodo}</td> 
                                        <td className="td-cadena">{datos.nom_subarea}</td> 
                                        <td className="td-cadena">{datos.nom_turno}</td> 
                                        <td className="td-cadena">{datos.total_dia_lab}</td> 
                                        <td className="td-cadena"> 
                                            <span className={`text-uppercase${datos.sw_estado === '1' ? ' badge bg-success' : ' badge bg-danger'}`}>{`${datos.sw_estado === '1' ? 'Habilitado' : 'Deshabilitado'}`}</span> 
                                        </td> 
                                        <td> 
                                            <button data-toggle="tooltip" title="Modificar" className="btn btn-outline-primary btn-sm" 
                                                onClick={() => eventoClick("E", datos)}> 
                                                <BsPencilFill /> 
                                            </button> 
                                            <button data-toggle="tooltip" title="Eliminar" className="btn btn-outline-danger btn-sm ms-2" 
                                                onClick={() => deleteData(datos.id_per_lab)}> 
                                                <BsXCircleFill /> 
                                            </button> 
                                        </td> 
                                    </tr> 
                                }) 
                            ) : 
                            ( 
                                <tr> 
                                    <td className="text-center" colSpan="7">Sin datos</td> 
                                </tr> 
                            ) 
                        } 
                    </tbody> 
                </table> 
            </div> 
            {/* FIN TABLA */} 
        </> 
    ); 
}