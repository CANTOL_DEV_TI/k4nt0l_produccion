import React, { useState, useEffect } from 'react'
import { get_subarea_operacion } from '../../services/subAreaServices';
import { get_turno_activo } from '../../services/turnoServices';
import {
    get_periodo_laboral,
    insert_periodo_laboral,
    insert_periodo_laboral_sub_area,
    update_periodo_laboral,
    delete_periodo_laboral
} from '../../services/periodoLaboralServices';
import { BotonExcel, BotonNuevo } from "../../components/Botones";
import { InputTextBuscar } from "../../components/InputTextBuscar";
import { useVentanaModal } from "../../hooks/useVentanaModal";
import { PeriodoLaboral_tabla } from "./components/PeriodoLaboral_tabla";
import { PeriodoLaboral_crud } from "./components/PeriodoLaboral_crud";
import ComboBox from "../../components/ComboBox";
import VentanaModal from "../../components/VentanaModal";
import Title from "../../components/Titulo";
import { validaToken,validaAcceso } from "../../services/usuarioServices";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

export function PeriodoLaboral() {
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
	const Seguridad = async () => {
        	const ValSeguridad = await Validar("PROPEL")
		setVentanaSeguridad(ValSeguridad.Validar)
	}
    
    const [dataToEdit, setDataToEdit] = useState(null);

    // CONSUMIR DATOS 
    const [codSubArea, setCodSubArea] = useState("0");
    const [codTurno, setCodTurno] = useState("0");

    const [sw_modo, setSw_modo] = useState("C");
    const [refrescar, setRefrescar] = useState(true);
    const [listDatos, setListDatos] = useState([]);
    useEffect(() => {
        Seguridad();
        setRefrescar(false);
        getData();
    }, [refrescar, codTurno, codSubArea])

    // 
    const getData = () => {
        get_periodo_laboral(codTurno, codSubArea)
            .then(res => {
                setListDatos(res)
                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM 
    const [buscarDato, setBuscarDato] = useState("");
    const idBusqueda = ["ejercicio", "periodo", "nom_subarea", "nom_turno", "sw_estado"];
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);

        // SI HAY VALOR EN CAJA DE BUSQUEDA 
        const filtered = listDatos.filter((item) =>
            idBusqueda.some((key) => item[key].toString().toLowerCase().includes(buscarDato))
        );
        return filtered.slice(currentPage, currentPage + LimitPag);
    };

    // cargar combo sub area     
    const [subArea, setSubArea] = useState([])
    useEffect(() => {
        get_subarea_operacion('1')
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.cod_subarea, "nombre": data.nom_subarea
                    }
                })
                setSubArea(cbo_data)
            })
    }, []);

    // cargar combo turno     
    const [turno, setTurno] = useState([]);
    useEffect(() => {
        get_turno_activo()
            .then(res => {
                const cbo_data = res.map(data => {
                    return {
                        "codigo": data.cod_turno, "nombre": data.nom_turno
                    }
                })
                setTurno(cbo_data)
            })
    }, []);

    // Captura el evento y los valores de los inputs 
    const handleChange = (evento) => {
        if (evento.target.name === 'cod_turno') {
            setCodTurno((evento.target.value).toString());
        }
        if (evento.target.name === 'cod_subarea') {
            setCodSubArea((evento.target.value).toString());
        }
    };

    // INSERTAR DATOS 
    const createData = async (data, modo = "") => {
        if (modo === "ALL") {
            const res = await insert_periodo_laboral_sub_area(data)
        } else {
            const res = await insert_periodo_laboral(data);
        }
        setRefrescar(true);
        handleClose(true);
    };

    // ACTUALIZAR DATOS 
    const updateData = (data) => {
        const res = update_periodo_laboral(data)
        console.log(data)
        setRefrescar(true);
        handleClose(true)
    };

    // ACTUALIZAR DATOS 
    const deleteData = (id) => {
        delete_periodo_laboral(id)
        setRefrescar(true);
    };

    // PAGINACIÓN  
    let LimitPag = 15;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (listDatos.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    const onSearchChange = (e) => {
        setCurrentPage(0);
        setBuscarDato(e.target.value.toLowerCase().trim());
    }

    // MOSTRAR MODAL 
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = (opc = "N", datos) => {
        if (opc === "E") {
            setDataToEdit(datos);
            setSw_modo("M")
        } else {
            setDataToEdit(null);
            setSw_modo("C")
        }
        handleShow(true)
    };

    // RETORNAR INFORMACIÓN 
    return (
        <div className="col-lg-12">

            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-sm-auto">
                            <Title>Lista de Período Laboral</Title>
                        </div>
                        <div className="col-sm">
                            <div className="d-flex justify-content-end flex-wrap gap-1">
                                <InputTextBuscar onBuscarChange={onSearchChange} />
                                <ComboBox
                                    datosRow={subArea}
                                    nombre_cbo="cod_subarea"
                                    manejaEvento={handleChange}
                                    valor_ini="Sub Area"
                                    valor={codSubArea}
                                />
                                <ComboBox
                                    datosRow={turno}
                                    nombre_cbo="cod_turno"
                                    manejaEvento={handleChange}
                                    valor_ini="Turno"
                                    valor={codTurno}
                                />
                                <BotonExcel textoBoton={"Exportar Excel"} sw_habilitado={listDatos.length > 0 ? true : false} listDatos={listDatos} nombreArchivo={"Presupesto mensual"} />
                                <BotonNuevo textoBoton={"Crear"} sw_habilitado={true} eventoClick={mostrarVentana} />
                            </div>
                        </div>
                    </div>
                </div>

                {/* FIN BARRA DE NAVEGACION */}

                { /* INICIO TABLA */}
                <PeriodoLaboral_tabla datos_fila={search(listDatos)} setDataToEdit={setDataToEdit} deleteData={deleteData} eventoClick={mostrarVentana} />
                {/* FIN TABLA */}

                {/* INICIO PAGINACION */}
                <div className="d-flex justify-content-end m-2">
                    <div className="pagination-wrap hstack gap-1">
                        Total Reg. {listDatos.length}
                        <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                        <button className={listDatos.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                    </div>
                </div>
                {/* FIN PAGINACION */}
            </div>

            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="xsm"
                handleClose={handleClose}
                titulo={dataToEdit != null ? "Editar Período Laboral" : "Crear Período Laboral"}
            >
                <PeriodoLaboral_crud
                    createData={createData}
                    updateData={updateData}
                    dataToEdit={dataToEdit}
                    setDataToEdit={setDataToEdit}
                    sw_mode={sw_modo}
                />
            </VentanaModal>
            {/* FIN MODAL */}
            <VentanaBloqueo
                show={VentanaSeguridad}

            />
        </div>
    );
}