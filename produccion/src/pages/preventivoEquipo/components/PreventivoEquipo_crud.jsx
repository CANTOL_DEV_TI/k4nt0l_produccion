import React, { useState, useEffect } from "react";
import { BotonNuevo, BotonAtras, BotonGrabar } from "../../../components/Botones";
import { useVentanaModal } from "../../../hooks/useVentanaModal";
import { PreventivoEquipo_crud_tabla } from "./PreventivoEquipo_crud_tabla";
import { insert_preventivo, update_preventivo, get_preventivo_det } from '../../../services/preventivoServices';
import { EquipoBuscar } from "../../equipos/EquipoBuscar";
import VentanaModal from "../../../components/VentanaModal";
import Title from "../../../components/Titulo";

const iniFormCab = {
    id_preventivo: "0",
    fecha_plan: "",
    glosa: "",
};

export function PreventivoEquipo_crud({ modoVista, formData }) {
    // C: CREAR , M: MODIFICAR
    const [sw_modo, setSw_modo] = useState("C");

    // CONSUMIR DATOS
    const [formCab, setFormCab] = useState(iniFormCab);
    const [formDet, setFormDet] = useState([]);
    const [checked, setChecked] = useState(false);
    useEffect(() => {
        if (Object.keys(formData).length > 0) {
            if (formData.sw_preventivo === '1') {
                setChecked(true);
            } else {
                setChecked(false);
            }
            setFormCab(formData);
            getDataDet();
            setSw_modo("M")
        } else {
            setChecked(true);
        }
    }, []);

    // Datos detalle
    const getDataDet = () => {
        get_preventivo_det(formData.id_preventivo)
            .then(res => {
                setFormDet(res)
                setCurrentPage(0);
            })
    };

    // BUSQUEDA EN DOM
    const [buscarDato, setBuscarDato] = useState("");
    const search = (listDatos) => {
        if (buscarDato.length === 0)
            return listDatos.slice(currentPage, currentPage + LimitPag);
    };

    // CAPTURAR EL EVENTO DE LOS IMPUTS
    const handleChange = (evento) => {
        setFormCab({
            ...formCab,
            [evento.target.name]: (evento.target.value).toUpperCase(),
        });
    };
    
    // Seleccionar datos de tabla
    const eventoSeleccionar = (datos) => {
        if (Object.keys(datos).length) {
            // buscar si existe en lista
            const c_res = formDet.filter(item => {
                return item.cod_equipo.toLowerCase().includes(datos.cod_equipo.toLowerCase())
            })

            if (c_res.length === 0) {
                const c_data = {
                    "id_equipo": datos.id_equipo,
                    "cod_equipo": datos.cod_equipo,
                    "equipo": datos.equipo,
                    "cantidad": 0
                }
                const newList = formDet.concat(c_data);
                setFormDet(newList);
            }
        }
    };

    // Eliminar items
    const deleteItem = (id_equipo) => {
        const c_res = formDet.filter((item) => item.id_equipo !== id_equipo);
        setFormDet(c_res);
    };

    // GUARDAR INFORMACION
    const handleSubmit = (e) => {
        if (!formCab.fecha_plan) {
            alert("Seleccionar fecha. !");
            return;
        }
        if (formDet.length === 0) {
            alert("Documuento no tiene Detalle");
            return;
        }

        // generar datos
        const new_data = {
            "id_preventivo": formCab.id_preventivo,
            "fecha_plan": Date.parse(formCab.fecha_plan),
            "glosa": formCab.glosa,
            "sw_preventivo": formCab.sw_preventivo,
            "cod_usuario": "1",
            "detalle": formDet
        };

        // realizar operación segun condición
        if (sw_modo === "C") { // CREAR
            const res = insert_preventivo(new_data);
            handleAtras();
        } else { // MODIFICAR
            const res = update_preventivo(new_data)
            handleAtras();
        }
    };

    // Ir a consulta principal
    const handleAtras = (e) => {
        //navigate('/articulo_omitido');
        modoVista("L") // lista
    };

    // PAGINACIÓN 
    let LimitPag = 10;
    const [currentPage, setCurrentPage] = useState(0);
    const nextPage = () => {
        if (formDet.length > currentPage + LimitPag)
            setCurrentPage(currentPage + LimitPag);
    }

    const prevPage = () => {
        if (currentPage > 0)
            setCurrentPage(currentPage - LimitPag);
    }

    // MOSTRAR MODAL
    const [show, handleShow, handleClose] = useVentanaModal(false);
    const mostrarVentana = () => {
        handleShow(true)
    };

    // RETORNAR INFORMACIÓN
    return (
        <>
            <div className="row justify-content-center">
                <div className="col-lg-8">
                    <div className="card">
                        <div className="card-header">
                            <div className="row g-2">
                                <div className="col-sm-auto">
                                    <Title>Mantenimiento de Plan Preventivo</Title>
                                </div>
                                <div className="col-sm">
                                    <div className="d-flex justify-content-end gap-1">
                                        <BotonAtras textoBoton={"Regresar"} eventoClick={handleAtras} />
                                        <BotonGrabar textoBoton={`${sw_modo === "C" ? 'Crear' : 'Actualizar'}`} eventoClick={handleSubmit} sw_habilitado={formData.sw_preventivo === '1' ? false : true} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="row g-2">
                                {/* INICIO  FORM*/}
                                <div className="col-md-1">
                                    <label className="form-label">Id</label>
                                    <input type="text" readOnly className="form-control form-control-sm" name="id_preventivo" value={formCab.id_preventivo || '0'} />
                                </div>
                                <div className="col-md-2">
                                    <label className="form-label">Fecha Plan</label>
                                    <input type="date" min="2023-01-01" className="form-control form-control-sm" name="fecha_plan" onChange={handleChange} value={formCab.fecha_plan || ''} />
                                </div>
                                <div className="col-md-7">
                                    <label className="form-label">Observación</label>
                                    <input type="text" className="form-control form-control-sm" name="glosa"
                                        onChange={handleChange} value={formCab.glosa} />
                                </div>
                                <div className="col-md-2">
                                    <label className="form-label">Opciones&emsp;&emsp;</label>
                                    <BotonNuevo textoBoton={"Añadir"} sw_habilitado={true} eventoClick={mostrarVentana} />
                                </div>
                                <div className="col-12">
                                    <PreventivoEquipo_crud_tabla formDet={search(formDet)} setFormDet={setFormDet} eventoClick={deleteItem} />
                                </div>
                                <div className="col-md-4">
                                    Total Reg. {formDet.length}
                                </div>
                                <div className="col-md-8">
                                    {/* INICIO PAGINACION */}
                                    <div className="d-flex justify-content-end">
                                        <div className="pagination-wrap hstack gap-1">
                                            <button className={currentPage <= 0 ? "btn btn-primary btn-sm disabled" : "btn btn-primary btn-sm"} onClick={prevPage}>Anterior</button>
                                            <button className={formDet.length > currentPage + LimitPag ? "btn btn-primary btn-sm" : "btn btn-primary btn-sm disabled"} onClick={nextPage}>Siguiente</button>
                                        </div>
                                    </div>
                                    {/* FIN PAGINACION */}
                                </div>
                                {/* FIN FORM */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* INICIO MODAL */}
            <VentanaModal
                show={show}
                size="xsm"
                handleClose={handleClose}
                titulo={"Buscar Equipo"}
            >
                <EquipoBuscar eventoClick={eventoSeleccionar} />
            </VentanaModal>
            {/* FIN MODAL */}
        </>
    );
}