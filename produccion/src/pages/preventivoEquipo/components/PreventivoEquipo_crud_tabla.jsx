import { BsXCircleFill } from "react-icons/bs";

export function PreventivoEquipo_crud_tabla({ formDet, setFormDet, eventoClick }) {

    // CAPTURAR EL EVENTO DE LOS IMPUTS
    const handleChange = (evento) => {

        let id_equipo = evento.target.name;
        let c_cantidad = evento.target.value;

        const nuevoDato = formDet.map(obj => {
            if (obj.id_equipo.toString() === id_equipo.toString()) {
                return {
                    ...obj,
                    cantidad: c_cantidad
                };
            }

            return obj;

        });
        setFormDet(nuevoDato);
    };


    // RETORNAR INFORMACIÓN
    return (
        <>
            <table className="table table-hover table-sm table-bordered">
                <thead className="table-secondary text-center table-sm">
                    <tr>
                        <th className="align-middle" style={{ maxWidth: "110px" }}>Cod. Equipo</th>
                        <th className="align-middle">Nombre Equipo</th>
                        <th className="align-middle" style={{ width: "160px" }} >Cantidad de Horas</th>
                        <th className="align-middle" style={{ width: "40px" }}>Opciones</th>
                    </tr>
                </thead>
                <tbody className="list">
                    {formDet.length > 0 ?
                        (
                            formDet.map((datos, index) => {
                                return (
                                    <tr key={index + 1}>
                                        <td>{datos.cod_equipo}</td>
                                        <td>{datos.equipo}</td>
                                        <td>
                                            <input type="number" className="form-control form-control-sm" style={{ minWidth: "60px", maxWidth: "140px", textAlign: "right" }} name={datos.id_equipo} onChange={handleChange} value={datos.cantidad} />
                                        </td>
                                        <td>
                                            <button data-toggle="tooltip" title="Eliminar" className="btn btn-outline-danger btn-sm ms-2"
                                                onClick={() => eventoClick(datos.id_equipo)}>
                                                <BsXCircleFill />
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })
                        ) :
                        (
                            <tr>
                                <td className="text-center" colSpan="4">Sin datos</td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
        </>
    );
}