import React, { useState, useEffect } from 'react'
import { PreventivoEquipo } from "./components/PreventivoEquipo";
import { PreventivoEquipo_crud } from "./components/PreventivoEquipo_crud";

export function Preventivo() {
    // L: LISTA , C: CRUD
    const [modoVista, setModoVista] = useState("L");
    const [formCab, setFormCab] = useState([]);

    // RETORNAR INFORMACIÓN
    if (modoVista === "L") {
        //setFormCab([]); // limpiar
        return (
            <PreventivoEquipo modoVista={setModoVista} setFormData={setFormCab} />
        );
    } else {
        return (
            <PreventivoEquipo_crud modoVista={setModoVista} formData={formCab} />
        );
    }

}