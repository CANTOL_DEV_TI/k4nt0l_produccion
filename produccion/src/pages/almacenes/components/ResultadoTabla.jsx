import React from "react";
import {GrEdit} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({cod_almacen,                                                
                        nom_almacen,                        
                        eventoEditar,
                        eventoEliminar}) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena" id={cod_almacen}>{cod_almacen}</td>                
                <td className="td-cadena" >{nom_almacen}</td>                
                <td><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                
            </tr>
        </tbody>
    )

export default ResultadoTabla;