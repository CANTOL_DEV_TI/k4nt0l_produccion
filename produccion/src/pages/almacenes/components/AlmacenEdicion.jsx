import React from "react";
import { BotonGuardar, BotonConsultar } from "../../../components/Botones";
import { getNombre_Almacen } from "../../../services/almacenServices.jsx";

class AlmacenEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { cod_almacen: '', nom_almacen: '' }
        } else {
            this.state = { cod_almacen: this.props.dataToEdit.cod_almacen, nom_almacen: this.props.dataToEdit.nom_almacen }
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'cod_almacen') {
            this.setState({ cod_almacen: e.target.value })
        }

        if (e.target.name === 'nom_almacen') {
            this.setState({ nom_almacen: e.target.value })
        }
    }

    handleFind = async (txtFind) => {
        const responseJson = await getNombre_Almacen(txtFind)        
        this.setState({nom_almacen : responseJson[0].nombre})        
    }


    render() {
        const { eventOnclick, nameOpe } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-4 col-form-label">
                                <input type="text" className="form-control form-control-sm" name="cod_almacen" autoFocus onChange={this.handleChange} value={this.state.cod_almacen} />
                            </div>
                            <div className="col-sm-4 col-form-label">
                                <BotonConsultar sw_habilitado={true} eventoClick={() => this.handleFind(this.state.cod_almacen)} texto="SAP"/>
                            </div>                            
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Nombre : </div>
                            <div className="col-sm-8 col-form-label"><input className="form-control form-control-sm" type="text"  name="nom_almacen" onChange={this.handleChange} value={this.state.nom_almacen} /></div>                            
                        </div>
                    </div>
                </div>

                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "cod_almacen": this.state.cod_almacen, "nom_almacen": this.state.nom_almacen })} texto={nameOpe} />
            </div >
        )
    }

}

export default AlmacenEdicion;