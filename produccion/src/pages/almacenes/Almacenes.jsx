import React from "react";

import { getFilter_Almacen, save_Almacen, update_Almacen, delete_Almacen } from "../../services/almacenServices.jsx";

import Busqueda from "../almacenes/components/Busqueda.jsx";
import ResultadoTabla from "../almacenes/components/ResultadoTabla.jsx";
import VentanaModal from "../../components/VentanaModal.jsx";
import AlmacenEdicion from "../almacenes/components/AlmacenEdicion.jsx";
import {  Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";

class Almacenes extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find'            
        }
    }

    handleBusqueda = async (txtFind) => {
        const responseJson = await getFilter_Almacen(txtFind)
        console.log(responseJson)
        this.setState({ resultados: responseJson, isFetch: false })
        console.log(this.state)
    }

    handleSubmit = async (e) => {
        console.log(e)


        if (this.state.tipoOpe === 'Grabar') {
            const responseJson = await save_Almacen(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar') {
            const responseJson = await update_Almacen(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar') {
            const responseJson = await delete_Almacen(e)
            console.log(responseJson)
        }


        this.setState({ showModal: false, tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal , VentanaSeguridad} = this.state

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} />


                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>                                
                                <th className="align-middle">Codigo</th>
                                <th className="align-middle">Nombre</th>                                
                                <th className="align-middle">Editar</th>
                                <th className="align-middle">Eliminar</th>
                            </tr>
                        </thead>
                        
                        {resultados.map((registro) =>
                            <ResultadoTabla
                                key={registro.cod_almacen}                                
                                cod_almacen={registro.cod_almacen}
                                nom_almacen={registro.nom_almacen}                                
                                eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}
                            />
                        )}
                    </table>
                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({ showModal: false })}
                    titulo="Edición Almacenes">
                    <AlmacenEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={this.state.tipoOpe}                        
                    />
                </VentanaModal>
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}

                />
            </React.Fragment>
        )

    }
    async componentDidMount(){
        const ValSeguridad = await Validar("PROALM")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
    }
}

export default Almacenes;
