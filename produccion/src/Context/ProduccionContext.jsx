import React, { useEffect, useState } from 'react'
import { createContext } from 'react'
import meLogo from '../components/navegacion/assets/cantol.png'
import tecnoLogo from '../components/navegacion/assets/tecnopress_alt.png'
import distriLogo from '../components/navegacion/assets/distrimax.png'

const ProductionContext = createContext()

const logos = {
  CNT: meLogo,
  TCN: tecnoLogo,
  DTM: distriLogo
}

function ProduccionContext({children}) {
  const [logoC, setLogoC] = useState(!!sessionStorage.getItem("CDTToken") ? logos[sessionStorage.getItem("Logo")] : logos["CNT"]);
  
  const refresh_Logo = (logo_) => {
    setLogoC(logos[logo_])
  }

  return (
    <ProductionContext.Provider value={
      {
        logoC,
        refresh_Logo,
      }
    }>
        {children}
    </ProductionContext.Provider>
  )
}

export { ProduccionContext, ProductionContext} 
