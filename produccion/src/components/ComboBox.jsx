import React from 'react'
const ComboBox = ({ datosRow, nombre_cbo, manejaEvento, valor_ini, valor = "" }) => {
    return (
        <select className="form-select-sm" id={nombre_cbo} name={nombre_cbo} onChange={manejaEvento} value={valor}>  
            <option value="0">{valor_ini}</option>
            {datosRow.map((data, index) => {
                return <option key={index}  value={data.codigo}>{data.nombre}</option>;
            })}
        </select>
    );
};
export default ComboBox

////selected={data.codigo === valor ? true : false}
export const ComboBoxForm = ({ datosRow, nombre_cbo, manejaEvento, valor_ini, valor = "" }) => {

    return (



        <select className="form-select" id={nombre_cbo} name={nombre_cbo} onChange={manejaEvento} value={valor}>  
            <option value="0">{valor_ini}</option>
            {datosRow.map((data, index) => {
                return <option key={index}  value={data.codigo}>{data.nombre}</option>;
            })}
        </select>
    );
};


export const ComboBoxForm2 = ({ datosRow, nombre_cbo, manejaEvento, valor_ini, valor, view}) => {

    return (

        <select className="form-select" id={nombre_cbo} name={nombre_cbo} onChange={manejaEvento} value={valor} disabled={view} style={{minWidth: "150px"}}>
            <option value='0'>{valor_ini}</option>
            {
                datosRow.map((data) =>
                        <option value={data.codigo}>{data.nombre}</option>
                )
            }
        </select>
    );
};


export const ComboBoxForm3 = ({ datosRow, nombre_cbo, manejaEvento, valor_ini, valor, view}) => {

    return (

        <select className="form-select" id={nombre_cbo} name={nombre_cbo} onChange={manejaEvento} value={valor} disabled={view} style={{minWidth: "120px", marginTop: "10px"}}>
            <option value='0'>{valor_ini}</option>
            {
                datosRow.map((data) =>
                        <option value={data.codigo}>{data.nombre}</option>
                )
            }
        </select>
    );
};


