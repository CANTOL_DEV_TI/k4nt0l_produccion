import styled from "styled-components"



export const Header =styled.header`
  width: 100%;
`;


export const Menu_bar = styled.div`
  display: none;
  
  @media screen and (max-width: 800px) {
    display: block;
    width: 100%;
    position: fixed;
    top: 0;
    background:#E6344A;
    
    span{
      float: right;
      font-size: 40px;
    }
  }
`;

export const Bt_menu = styled.a`
  @media screen and (max-width: 800px) {
    display: block;
    padding: 20px;
    color: #fff;
    overflow: hidden;
    font-size: 25px;
    font-weight: bold;
    text-decoration: none;
  }
`;


export const Nav = styled.nav`
  background: #023859;
  z-index: 1000;
  max-width: 1000px;
  width: 95%;
  margin: 20px auto;
  
  
  @media screen and (max-width: 800px) {
    width: 80%;
    height: calc(100% - 80px);
    position: fixed;
    right: 100%;
    margin: 0;
    overflow: scroll;
  }
`;


export const Ul = styled.ul`
  list-style: none;
  
  
  
`;

export const Li = styled.li`
  display: inline-block;
  position: relative;
  
  @media screen and (max-width: 800px) {
    display: block;
    border-bottom: 1px solid rgba(255, 255, 255, .5);
    
  }
`;

export const A = styled.a`
  color: #ffffff;
  display: block;
  text-decoration: none;
  padding: 20px;
  
  &:hover{
    background:#E6344A;
  }
  
  @media screen and (max-width: 800px) {
    display: block;
  }
`;

export const Span = styled.span`
    margin-right: 10px;
`;


export const Caret = styled.span`
  position: relative;
  top: 3px;
  margin-left: 10px;
  margin-right: 0;
  
  @media screen and (max-width: 800px) {
    float: right;
  }
`;

export const Children = styled.ul`
  display: none;
  background: #011826;
  position: absolute;
  width: 150%;
  z-index: 1000;
  
  @media screen and (max-width: 800px) {
    width: 100%;
    position: relative;
  }
`;

export const Children_Li = styled.li`
  display: block;
  overflow: hidden;
  border-bottom: 1px solid rgba(255, 255, 255 5);
`;

export const Children_A = styled.a`
  display: block;
  
  color: #ffffff;
  text-decoration: none;
  padding: 20px;
  
  @media screen and (max-width: 800px) {
    margin-left:20px;
  }
`;

export const Children_Span = styled.span`
  float: right;
  position: relative;
  top: 3px;
  margin-right: 0;
  margin-left: 10px;
`;



