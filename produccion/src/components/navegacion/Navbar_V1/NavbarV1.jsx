import React, { useContext, useEffect, useState } from "react";
import { useMediaQuery } from "react-responsive";

import {
    Header,
    Menu_bar, Bt_menu,
    Nav, Ul, Li, A, Span, Caret,
    Children, Children_Li, Children_A, Children_Span
} from "./NavbarV1.elements";

// import {ReactComponent as ReactLogo} from './static/logoSVG.svg';
import meLogo from '../assets/cantol.png';


import './navbarV1_Tecnopress.css'
import './static/fonts.css'
// import {AddLibrary} from "./upload_JsScript";

import { NavLink } from "react-router-dom";
import { ProductionContext } from "../../../Context/ProduccionContext";


const NavbarV1 = () => {
    const [activeNav, setActiveNav] = useState("nav_menu");
    const isReprocesoMobil = useMediaQuery({ query: '(max-width: 800px)' })
    const [activeChildren, setActiveChildren] = useState("children")
    const {
        logoC
        } = useContext(ProductionContext);
    const navToggle = () => {
        activeNav === "nav_menu"
            ? setActiveNav("nav_menu nav_active")
            : setActiveNav("nav_menu");
    };

    const childrenToggle = () => {
        activeChildren === "children"
            ? setActiveChildren("children children_active")
            : setActiveChildren("children");

        console.log(activeChildren)
    };

    const handleClear=()=>{
        sessionStorage.clear("")
    }

    return (
        <header>
            <div className={"menu_bar"} >                
                <a href="#" className={"bt-menu"}><span className={"icon-menu"} onClick={navToggle}></span><img src={logoC} className="LogoEmpresa" /></a>
            </div>

            {/*<img src={meLogo} className="LogoEmpresa" />*/}

            <nav className={activeNav}>
                <ul>
                    <li className={"submenu"}>
                        <NavLink to="" onClick={()=>{childrenToggle;}}><span className={"icon-rocket"}></span>Producción<span className={"caret icon-chevron-thin-down"}></span></NavLink>
                        {/* <ul className={'children'}>  */}
                        <ul className={activeChildren}>
                            <li><NavLink to="/programa_fabricacion">Programa Fabricación<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/oee">OEE<span className={"icon-dot-single"}></span></NavLink></li>
                                <li className={isReprocesoMobil?"submenu":"parent"}><NavLink to="/reproceso" onClick={childrenToggle}>Reproceso<span className="expand reprocesoflecha"></span></NavLink> 
                                    <ul className={isReprocesoMobil?activeChildren:"child"}>
                                        <li><NavLink to="/reproceso/simular">Simulación</NavLink></li>
                                        <li><NavLink to="/reproceso/aprobar">Aprobaciones</NavLink></li>
                                    </ul>
                                </li>
                        </ul>
                    </li>
                    <li><NavLink to="/articulo_plano"><span className={"icon-briefcase"}></span>Planos</NavLink></li>
                    <li className={"submenu"}>
                        <NavLink to="" onClick={childrenToggle}><span className={"icon-rocket"}></span>Conf. General<span className={"caret icon-chevron-thin-down"}></span></NavLink>
                        {/*<ul className={'children'}>*/}
                        <ul className={activeChildren}>
                            <li><NavLink to="/area">Area<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/subarea">Sub Area<span className={"icon-dot-single"}></span></NavLink>
                            </li>
                            <li><NavLink to="/familia">Familia<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/almacenes">Almacen<span className={"icon-dot-single"}></span></NavLink>
                            </li>
                            <li><NavLink to="/turno">Turno Laboral<span className={"icon-dot-single"}></span></NavLink>
                            </li>
                            <li><NavLink to="/tipoOperacion">Tipo Operación<span
                                className={"icon-dot-single"}></span></NavLink></li>

                            <li><NavLink to="/oee/tiporegistro">Tipo Registro OEE<span
                                className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/oee/causa">Causas OEE<span className={"icon-dot-single"}></span></NavLink></li>


                            <li><NavLink to="/oee/empleados">Empleados OEE<span className={"icon-dot-single"}></span></NavLink></li>
                        </ul>
                    </li>
                    <li className={"submenu"}>
                    <NavLink to="" onClick={childrenToggle}><span className={"icon-rocket"}></span>Maestros<span className={"caret icon-chevron-thin-down"}></span></NavLink>
                        {/*<ul className={'children'}>*/}
                        <ul className={activeChildren}>
                            <li><NavLink to="/periodo_laboral">Periodo Laboral<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/articuloFamilia">Artículo<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/equipos">Equipos<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/configproceso">Configuración Proceso<span className={"icon-dot-single"}></span></NavLink></li>
                        </ul>
                    </li>
                    <li className={"submenu"}>
                        <NavLink to="" onClick={childrenToggle}><span className={"icon-rocket"}></span>Capacidad<span className={"caret icon-chevron-thin-down"}></span></NavLink>
                        {/*<ul className={'children'}>*/}
                        <ul className={activeChildren}>
                            <li><NavLink to="/plan_venta">Plan de Venta<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/articulo_omitido">Articulo Omitido<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/produccion_pendiente">Produccion Pendiente<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/preventivo">Plan Preventivo<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/tareaprogramadausuario">Programar Tarea<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/plan_capacidad_pt"><span className={"icon-earth"}></span>Plan Capacidad</NavLink></li>
                        </ul>
                    </li>
                    <li className={"submenu"}>
                        <NavLink to="" onClick={childrenToggle}><span className={"icon-user"}></span>Usuario<span className={"caret icon-chevron-thin-down"}></span></NavLink>
                        {/*<ul className={'children'}>*/}
                        <ul className={activeChildren}>
                            <li><NavLink to="/cambiocontrasena">Cambio de Contraseña<span className={"icon-dot-single"}></span></NavLink></li>                            
                            <li><NavLink to="/login">Inicio de Sesion<span className={"icon-dot-single"}></span></NavLink></li>
                        </ul>
                    </li>
                    <li><NavLink to="http://192.168.5.21/" onClick={handleClear}><span className={"icon-mail"}></span>Salir</NavLink></li>

                </ul>

            </nav>
        </header>
    );
};

export default NavbarV1;


