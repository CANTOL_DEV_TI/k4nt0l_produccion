import { Routes, Route, Outlet, } from "react-router-dom";
import { Plano } from "../pages/plano/Plano";
import { Turno } from "../pages/turno/Turno";
//reproceso
import { Reproceso} from "../pages/reproceso/Reproceso.jsx"
import { ReprocesoSimulacion } from "../pages/reproceso/ReprocesoSimulacion.jsx"
import { ReprocesoAprobacion } from "../pages/reproceso/ReprocesoAprobacion.jsx";
import {ProtectedRoute_Reproceso} from "../pages/reproceso/components/ProtectedRoute_Reproceso.jsx";
//

import { Articulo_omitido } from "../pages/articuloOmitido/Articulo_omitido";
import { PeriodoLaboral } from "../pages/periodoLaboral/PeriodoLaboral";
import { ProduccionPendiente } from "../pages/produccionPendiente/ProduccionPendiente";
import { Plan_venta } from "../pages/plan_venta/Plan_venta";
import { Preventivo } from "../pages/preventivoEquipo/Preventivo";
import { OrdenFabricacionPrograma } from "../pages/ordenFabricacionPrograma/OrdenFabricacionPrograma";


import Area from "../pages/areas/Area";
import SubArea from "../pages/subAreas/SubArea";

import Familia from "../pages/familias/Familias";
import { ArticuloFamilia } from "../pages/articulofamilia/ArticuloFamilia";
import TareaProgramadaUsuario from "../pages/tareaprogramadausuario/tareaprogramadaUsuario";
import PlancapacidadPT from "../pages/planCapacidadPT/plancapacidadpt";
import TipoOperacion from "../pages/tipoOperacion/TipoOperacion";
import TipoEquipo from "../pages/tipoEquipo/TipoEquipo";
import Equipos from "../pages/equipos/equipos";
import Almacenes from "../pages/almacenes/almacenes";
import ConfigProceso from "../pages/configProceso/configProceso";

import SubAreaAlmacen from "../pages/subAreasAlmacen/SubAreaAlmacen.jsx";

import LoginV from "../pages/login/login";
import ActuaPassword from "../pages/login/cambiocontraseña";
import React from "react";


import { EficienciaOperativa_crud } from "../pages/oee/EficienciaOperativa_crud";
import EficienciaOperativa from "../pages/oee/EficienciaOperativa";
import EficienciaOperativa_Edit from "../pages/oee/EficienciaOperativa_Edit.jsx"
import TipoRegistro from "../pages/oee_TipoRegistro/TipoRegistro.jsx"
import Causa from "../pages/oee_Causa/Causa.jsx"

import EficienciaOperativa_StopMachine from "../pages/oee/EficienciaOperativa_StopMachine.jsx";
import EficienciaOperativa_Excel from "../pages/oee/EficienciaOperativa_Excel"


import ProtectedRoute from "../components/utils/ProtectedRoute.jsx"
import EmpleadoMain from "../pages/empleado/EmpleadoMain.jsx";
import { Login } from "../services/loginServices.jsx";


export function MiRutas() {
    return (
        <Routes>
          
<<<<<<< HEAD
            <Route path="/" element={<LoginV/>} />
=======
            <Route path="/" element={<LoginV />} />
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/inicio" element={<div/>} />
            </Route>
>>>>>>> origin/main
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/articulo_plano" element={<Plano />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/reproceso" element={<Reproceso />} />
            </Route>            
            <Route element={<ProtectedRoute_Reproceso Redirige={"/"} />} >
                <Route path="/reproceso/simular" element={<ReprocesoSimulacion />} />
            </Route>  
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/reproceso/aprobar" element={<ReprocesoAprobacion />} />
            </Route>              
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/area" element={<Area />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/subarea" element={<SubArea />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/turno" element={<Turno />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/periodo_laboral" element={<PeriodoLaboral />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/articulo_omitido" element={<Articulo_omitido />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/familia" element={<Familia />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/articulofamilia" element={<ArticuloFamilia />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/produccion_pendiente" element={<ProduccionPendiente />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/tareaprogramadausuario" element={<TareaProgramadaUsuario />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/plan_venta" element={<Plan_venta />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/plan_capacidad_pt" element={<PlancapacidadPT />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/tipoOperacion" element={<TipoOperacion />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/tipoEquipo" element={<TipoEquipo />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/equipos" element={<Equipos />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/subareaalmacen" element={<SubAreaAlmacen />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/almacenes" element={<Almacenes />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/configproceso" element={<ConfigProceso />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/preventivo" element={<Preventivo />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/programa_fabricacion" element={<OrdenFabricacionPrograma />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/oee/marco" element={<EficienciaOperativa_crud />} />
            </Route>

            <Route path="/oee" element={<EficienciaOperativa />} />
            <Route path="/oee/edit" element={<EficienciaOperativa_Edit />} />
            <Route path="/oee/empleados" element={<EmpleadoMain />} />


            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/oee/tiporegistro" element={<TipoRegistro />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/oee/causa" element={<Causa />} />
            </Route>

            <Route path="/oee/stopmachine" element={<EficienciaOperativa_StopMachine />} />
            <Route path="/oee/export" element={<EficienciaOperativa_Excel />} />

            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/cambiocontrasena" element={<ActuaPassword />} />
            </Route>
        </Routes>
    );
}