import React, { useState, useEffect } from 'react'
import { BrowserRouter } from "react-router-dom";
import { MiRutas } from "./routers/rutas"
import MenuNav from "./components/navegacion/MenuNav"
// import NavbarV4 from "./components/navegacion/Navbar_V4/NavbarV4";
import NavbarV1 from "./components/navegacion/Navbar_V1/NavbarV1"
import { ProduccionContext } from './Context/ProduccionContext';

function App() {
  return (
    <BrowserRouter basename="produccion">
      <ProduccionContext>
        <div className="page-content">
        <NavbarV1 />
          <div className="container-fluid">
            <MiRutas />                    
          </div>
        </div>
      </ProduccionContext>
    </BrowserRouter>
  )
}


export default App