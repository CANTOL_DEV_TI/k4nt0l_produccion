import {Url} from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.2.148:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

export async function getFilter_ProgramacionPT(idPlanificacion, txtFind) {
    if (txtFind.toString().trim()===''){
        txtFind='%20'
    }
    const response = await fetch(`${meServidorBackend}/produccion/programacion_pt/${idPlanificacion}/${txtFind}`)
    const responseJson = await response.json()
    return responseJson
}

export async function update_ProgramacionPT(meJson){
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/programacion_pt/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


