import {Url} from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url


const url = `${meServidorBackend}/produccion/subarea`;

export async function getFilter_SubArea(txtFind) {
    if (txtFind.trim()===''){
        txtFind='%20'
    }
    const response = await fetch(`${meServidorBackend}/produccion/subarea/${txtFind}`)
    const responseJson = await response.json()
    return responseJson
}

export async function getLista_SubArea() {    
    const response = await fetch(`${meServidorBackend}/produccion/listasubarea/`)
    const responseJson = await response.json()
    return responseJson
}


export async function get_subarea_operacion(cod_area) {
    let url_id = `${url}/cod_area/${cod_area}`;
    console.log(url_id)
    const response = await fetch(url_id);
    const responseJson = await response.json()
    return responseJson
}

export async function get_subarea_operacion_usuario(cod_usuario) {
    let url_id = `${url}/subarea_usuario/?cod_usuario=${cod_usuario}`;
    const response = await fetch(url_id);
    const responseJson = await response.json()
    return responseJson
}

export async function get_subarea_propietario() {
    let url_id = `${url}/propietario/`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}


export async function save_SubArea(meJson){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/subarea/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function update_SubArea(meJson){
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/subarea/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function delete_SubArea(meJson){
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/subarea/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}
