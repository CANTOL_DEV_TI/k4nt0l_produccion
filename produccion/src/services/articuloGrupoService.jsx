import {Url} from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

export async function get_grupo_sap() {
    const response = await fetch(`${meServidorBackend}/produccion/grupo_sap/activo`)
    const responseJson = await response.json()
    return responseJson
}

export async function get_grupo_pp_pt() {
    const response = await fetch(`${meServidorBackend}/produccion/grupo_sap/pt_pp`)
    const responseJson = await response.json()
    return responseJson
}