//const meServidorBackend = 'http://192.168.5.21:8090'

import {Url} from '../constants/global'

const meServidorBackend = Url

//const token = await localStorage.getItem("CDTToken")    

//const cabecera = {'Content-type': 'application/json; charset=UTF-8',"Authorization": "Bearer " + token}

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function getFilter_TipoOperacion(txtFind) {
    if (txtFind.trim() === '') {
        txtFind = '%20'
    }
    
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/produccion/tipoOperacion/${txtFind}`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getLista_TipoOperacion() {        
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/produccion/tipoOperacion/ListaTipoOperacion/`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function save_TipoOperacion(meJson){   
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/tipoOperacion/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function update_TipoOperacion(meJson){    
    const requestOptions = {
        method: 'PUT',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/tipoOperacion/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function delete_TipoOperacion(meJson){  
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,//{ 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/tipoOperacion/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}