import {Url} from '../constants/global'

const meServidorBackend = Url
//const meServidorBackend = 'http://192.168.5.21:8090'

//const token = await localStorage.getItem("CDTToken")    

//const cabecera = {'Content-type': 'application/json; charset=UTF-8',"Authorization": "Bearer " + token}

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function getFilter_Equipo(txtFind) {
    if (txtFind.trim() === '') {
        txtFind = '%20'
    }
    
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/produccion/Equipo/${txtFind}`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function get_equipo_buscar() {
    let url_id = `${meServidorBackend}/produccion/Equipo/buscar/`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function getLista_Equipos() {
        
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/produccion/Equipo/ListaEquipos/`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}


export async function save_Equipo(meJson){   
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/Equipo/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function update_Equipo(meJson){    
    const requestOptions = {
        method: 'PUT',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/Equipo/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function delete_Equipo(meJson){  
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,//{ 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/Equipo/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}