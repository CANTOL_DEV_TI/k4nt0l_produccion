import { Url } from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

export async function get_area_responsable_sap() {
    const response = await fetch(`${meServidorBackend}/produccion/get_area_responsable_sap`)
    const responseJson = await response.json()
    return responseJson
}

export async function get_articulo_plano_sap(cod_formulado, cod_grupo, cod_area) {
    const response = await fetch(`${meServidorBackend}/produccion/articulo_plano/all?cod_formulado=${cod_formulado}&cod_grupo=${cod_grupo}&cod_area=${cod_area}`)
    const responseJson = await response.json()
    console.log(responseJson)
    return responseJson
}

export async function get_articulo_plano_codigo_nuevo(tipo, cod_articulo) {
    const response = await fetch(`${meServidorBackend}/produccion/articulo_plano/codigo_nuevo?tipo=${tipo}&cod_articulo=${cod_articulo}`)
    const responseJson = await response.json()    
    return responseJson
}

export async function get_articulo_plano_historial(cod_articulo,tipo) {
    const response = await fetch(`${meServidorBackend}/produccion/articulo_plano/historial/${cod_articulo}/${tipo}`)
    const responseJson = await response.json()
    console.log("HISTORIAL")
    console.log(responseJson)
    return responseJson
}

export async function get_articulo_plano_file() {
    const response = `${meServidorBackend}/produccion/articulo_plano/plano/`
    return response
}

export async function insert_articulo_plano_file(data) {
    try {
        console.log(data)
        // insertar en la base de datos
        const controller = new AbortController();
        let url_id = `${meServidorBackend}/produccion/articulo_plano/`;
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options);
        const responseJson = await response.json();

        // grabar pdf en el servidor
        const controller_f = new AbortController();
        let url_id_f = `${meServidorBackend}/produccion/articulo_plano/upload_plano/`;
        const new_name = data.file_name;

        const formData = new FormData();
        formData.append('file', data.file_plano, new_name);

        let options_f = {
            body: formData,
            //headers: { "content-type": "multipart/form-data;" },
            method: "POST",
        };
        options_f.signal = controller.signal;

        setTimeout(() => controller_f.abort(), 3000);

        const response_f = await fetch(url_id_f, options_f);
        const responseJson_f = await response_f.json();
        //return responseJson_f | responseJson
        return responseJson | responseJson_f

    } catch (error) {
        console.error("Error:", error);
    }
}

export async function update_articulo_plano_estado(data) {
    try {
        const controller = new AbortController();
        let url_id = `${meServidorBackend}/produccion/articulo_plano/estado/${data.id_art_plano}`;
        //delete data.id_art_plano; // Quitar para no pasar por el body
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "PUT",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function delete_articulo_plano(id_art_plano) {
    try {
        let isDelete = window.confirm(
            `¿Estás seguro de eliminar el registro con el id '${id_art_plano}'?`
        );
        if (!isDelete) {
            return;
        }

        const controller = new AbortController();
        let url_id = `${meServidorBackend}/produccion/articulo_plano/${id_art_plano}`;
        let options = {
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "DELETE",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
}

export async function generate_new_version(ptipo, particulo) {
    try {
        const response = await fetch(`${meServidorBackend}/produccion/articulo_plano/gen_version/${ptipo}/${particulo}`)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error)
    }
}