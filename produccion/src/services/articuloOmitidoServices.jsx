import {Url} from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url


const url = `${meServidorBackend}/produccion/articulo_omitido`;

export async function get_articuloOmitido() {
    const response = await fetch(url)
    const responseJson = await response.json()
    return responseJson
}

export async function get_articuloFamilia() {
    let url_id = `${url}/articulo_familia`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function get_articuloOmitido_det(cod_articuloOmitido) {
    let url_id = `${url}/det/${cod_articuloOmitido}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function insert_articuloOmitido(data) {
    try {
        const controller = new AbortController();
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url, options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function update_articuloOmitido(data) {
    try {
        const controller = new AbortController();
        //let url_id = `${url}/${data.cod_articuloOmitido}`;
        //delete data.cod_articuloOmitido;
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "PUT",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url, options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function delete_articuloOmitido(cod_articuloOmitido) {
    try {
        let isDelete = window.confirm(
            `¿Estás seguro de eliminar el registro con el id '${cod_articuloOmitido}'?`
        );
        if (!isDelete) {
            return;
        }

        const controller = new AbortController();
        let url_id = `${url}/${cod_articuloOmitido}`;
        let options = {
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "DELETE",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
}

