import {Url} from '../constants/global'

const meServidorBackend = Url
const url = `${meServidorBackend}/produccion/configproceso`;
//const meServidorBackend = 'http://192.168.2.148:8000'
//const meServidorBackend = 'http://192.168.5.21:8090'

export async function getFilter_ConfigProceso(txtFind) {
    if (txtFind.trim()===''){
        txtFind='%20'
    }
    const response = await fetch(`${meServidorBackend}/produccion/configproceso/${txtFind}`)
    const responseJson = await response.json()
    return responseJson
}

export async function get_config_proceso_det_articulo(cod_articulo = "") {
    let url_id = `${url}/det_articulo/?cod_articulo=${cod_articulo}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function save_ConfigProceso(meJson){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/configproceso/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function update_ConfigProceso(meJson){
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/configproceso/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function delete_ConfigProceso(meJson){
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/configproceso/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function getNombre_Item(txtFind) {
    if (txtFind.trim()===''){
        txtFind='%20'
    }
    const response = await fetch(`${meServidorBackend}/produccion/configproceso/NombreItem/${txtFind}`)
    const responseJson = await response.json()
    return responseJson
}

export async function getDetalleProceso(txtID) {
    
    const response = await fetch(`${meServidorBackend}/produccion/configproceso/DetalleConfig/${txtID}`)
    const responseJson = await response.json()
    return responseJson
}

