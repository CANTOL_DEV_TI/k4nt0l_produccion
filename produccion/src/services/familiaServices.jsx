import {Url} from '../constants/global'

//const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url


const url = `${meServidorBackend}/produccion/familia`;

//const token = await localStorage.getItem("CDTToken")    

//const cabecera = {'Content-type': 'application/json; charset=UTF-8',"Authorization": "Bearer " + token}
const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function getFilter_Familia(txtFind) {
    if (txtFind.trim() === '') {
        txtFind = '%20'
    }
    
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/produccion/familia/${txtFind}`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function get_familia_activo() {
    let url_id = `${url}/Activos/`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}


export async function save_Familia(meJson){   
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/familia/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function update_Familia(meJson){    
    const requestOptions = {
        method: 'PUT',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/familia/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function delete_Familia(meJson){  
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,//{ 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/familia/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}