import { Url } from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

const url = `${meServidorBackend}/produccion/orden_fabricacion`;

export async function get_ordenFabricacionPrograma(data) {
    try {
        const controller = new AbortController();
        let url_id = `${url}/programa`;
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        console.log(responseJson)
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function get_ordenFabricacion_pendiente(cod_subarea) {
    let url_id = `${url}/pendiente?cod_subarea=${cod_subarea}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function get_ordenFabricacionCMO(data) {
    try {
        const controller = new AbortController();
        let url_id = `${url}/costos_fabricacion`;
        console.log(`${url}/costos_fabricacion`)
        console.log(data)
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        
        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
}