import {Url} from '../constants/global'


// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url


export async function getFilter_Tareas(txtDesde) {
    if (txtDesde.trim()===''){
        txtDesde='%20'
    }

    const response = await fetch(`${meServidorBackend}/produccion/tareaprogramadausuario/${txtDesde}`)
    const responseJson = await response.json()
    return responseJson
}


export async function save_Tareas(meJson){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/tareaprogramadausuario/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function update_Tareas(meJson){
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/tareaprogramadausuario/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function delete_Tareas(meJson){
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/tareaprogramadausuario/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}
