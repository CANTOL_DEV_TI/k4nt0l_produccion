import {Url} from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

 
const url = `${meServidorBackend}/produccion/plan_capacidad_pp`; 
 
export async function get_plan_capacidad_pp_resumen(id_plan_capacidad = 0, cod_subarea= 0, abc = "0") { 
    let url_id = `${url}/consolidado?id_plan_capacidad=${id_plan_capacidad}&cod_subarea=${cod_subarea}&abc=${abc}`; 
    const response = await fetch(url_id) 
    const responseJson = await response.json() 
    return responseJson 
}

export async function get_plan_capacidad_pp_equipo(id_plan_capacidad = 0, cod_subarea= 0) { 
    let url_id = `${url}/equipo?id_plan_capacidad=${id_plan_capacidad}&cod_subarea=${cod_subarea}`; 
    const response = await fetch(url_id) 
    const responseJson = await response.json() 
    return responseJson 
}

export async function get_plan_capacidad_pp_equipo_articulo(id_plan_capacidad = 0, id_equipo= 0) { 
    let url_id = `${url}/equipo_articulo?id_plan_capacidad=${id_plan_capacidad}&id_equipo=${id_equipo}`; 
    const response = await fetch(url_id)
    const responseJson = await response.json() 
    return responseJson 
}

export async function get_plan_capacidad_pp_articulo(id_plan_capacidad = 0, cod_articulo= "") { 
    let url_id = `${url}/articulo?id_plan_capacidad=${id_plan_capacidad}&cod_articulo=${cod_articulo}`; 
    const response = await fetch(url_id) 
    const responseJson = await response.json() 
    return responseJson 
}

export async function get_plan_capacidad_pp_articulo_nivel(id_plan_capacidad = 0, cod_articulo= "") { 
    let url_id = `${url}/get_nivel_articulo?id_plan_capacidad=${id_plan_capacidad}&cod_articulo=${cod_articulo}`; 
    const response = await fetch(url_id) 
    const responseJson = await response.json() 
    return responseJson 
}

export async function update_plan_capacidad_pp_det(data) {
    try {
        // update en la base de datos
        let url_id = `${url}/update`; 
        const controller = new AbortController();
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);
        const response = await fetch(url_id, options);
        const responseJson = await response.json();

        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
}

export async function update_plan_capacidad_pp_det_nivel(data) {
    try {
        // update en la base de datos
        let url_id = `${url}/boom_nivel`; 
        const controller = new AbortController();
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);
        const response = await fetch(url_id, options);
        const responseJson = await response.json();

        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
}

export async function insert_plan_capacidad_pp(id_plan_pt) {
    try {
        const controller = new AbortController();
        let url_id = `${meServidorBackend}/produccion/programacionpi/${id_plan_pt}`;
        let options = {
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };

        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 15000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function delete_plan_capacidad_pp(id_plan) {
    try {
        let isDelete = window.confirm(
            `¿Estás seguro de eliminar el registro con el id '${id_plan}'?`
        );
        if (!isDelete) {
            return;
        }

        const controller = new AbortController();
        let url_id = `${meServidorBackend}/produccion/programacionpi/${id_plan}`;
        let options = {
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "DELETE",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
}