import { Url } from '../constants/global'
// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

const url = `${meServidorBackend}/gdh/empleados`;

export async function get_empleado_subarea(cod_subarea = "0") {
    let url_id = `${url}/subarea?cod_subarea=${cod_subarea}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}
