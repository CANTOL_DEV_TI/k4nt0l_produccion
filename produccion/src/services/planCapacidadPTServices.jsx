import {Url} from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

 
const url = `${meServidorBackend}/produccion/plan_capacidad_pt`; 
 
export async function get_plan_capacidad_pt_resumen(id_plan_capacidad = 0, id_familia= 0, abc = "0") { 
    let url_id = `${url}/consolidado?id_plan_capacidad=${id_plan_capacidad}&id_familia=${id_familia}&abc=${abc}`; 
    const response = await fetch(url_id) 
    const responseJson = await response.json() 
    return responseJson 
}

export async function delete_plan_capacidad_pt(id_plan) {
    try {
        let isDelete = window.confirm(
            `¿Estás seguro de eliminar el registro con el id '${id_plan}'?`
        );
        if (!isDelete) {
            return;
        }

        const controller = new AbortController();
        let url_id = `${meServidorBackend}/produccion/programacionpt/${id_plan}`;
        let options = {
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "DELETE",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
}