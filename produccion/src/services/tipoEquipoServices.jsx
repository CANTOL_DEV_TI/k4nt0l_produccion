import {Url} from '../constants/global'

const meServidorBackend = Url
// const meServidorBackend = 'http://192.168.5.21:8090'

//const token = await localStorage.getItem("CDTToken")    

//const cabecera = {'Content-type': 'application/json; charset=UTF-8',"Authorization": "Bearer " + token}

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function getFilter_TipoEquipo(txtFind) {
    if (txtFind.trim() === '') {
        txtFind = '%20'
    }
    
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/produccion/tipoEquipo/${txtFind}`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function ListaTipoEquipo(){
    
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/produccion/tipoEquipo/ListaTipoEquipo/`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function save_TipoEquipo(meJson){   
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/tipoEquipo/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function update_TipoEquipo(meJson){    
    const requestOptions = {
        method: 'PUT',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/tipoEquipo/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function delete_TipoEquipo(meJson){  
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,//{ 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/tipoEquipo/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}