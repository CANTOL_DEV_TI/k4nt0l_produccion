import {Url} from '../constants/global'


// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.2.148:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

export async function getFilter_Area(txtFind) {
    if (txtFind.trim()===''){
        txtFind='%20'
    }


    const response = await fetch(`${meServidorBackend}/produccion/area/${txtFind}`)
    const responseJson = await response.json()
    return responseJson
}


export async function save_Area(meJson){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/area/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function update_Area(meJson){
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/area/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function delete_Area(meJson){
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/area/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}
