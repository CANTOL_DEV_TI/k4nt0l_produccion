import {Url} from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url
 
const url = `${meServidorBackend}/produccion/periodo_laboral`; 
 
export async function get_periodo_laboral(cod_turno, cod_subarea) { 
    let url_id = `${url}?cod_turno=${cod_turno}&cod_subarea=${cod_subarea}`; 
    const response = await fetch(url_id) 
    const responseJson = await response.json() 
    return responseJson 
}

export async function get_periodo_laboral_turno(cod_subarea) { 
    let url_id = `${url}/turno?cod_subarea=${cod_subarea}`; 
    const response = await fetch(url_id) 
    const responseJson = await response.json() 
    return responseJson 
} 
 
export async function insert_periodo_laboral(data) { 
    try { 
        const controller = new AbortController(); 
        let options = { 
            body: JSON.stringify(data), 
            headers: { "content-type": "application/json; charset=UTF-8" }, 
            method: "POST", 
        }; 
        options.signal = controller.signal; 
 
        setTimeout(() => controller.abort(), 3000); 
 
        const response = await fetch(url, options) 
        const responseJson = await response.json() 
        return responseJson 
    } catch (error) { 
        console.error("Error:", error); 
    } 
} 
 
export async function insert_periodo_laboral_sub_area(data) { 
    try { 
        const controller = new AbortController(); 
        let url_id = `${url}/sub_area`; 
        let options = { 
            body: JSON.stringify(data), 
            headers: { "content-type": "application/json; charset=UTF-8" }, 
            method: "POST", 
        }; 
        options.signal = controller.signal; 
 
        setTimeout(() => controller.abort(), 3000); 
 
        const response = await fetch(url_id, options) 
        const responseJson = await response.json() 
        return responseJson 
    } catch (error) { 
        console.error("Error:", error); 
    } 
} 
 
export async function update_periodo_laboral(data) { 
    try { 
        const controller = new AbortController(); 
        //let url_id = `${url}/${data.id_per_lab}`; 
        //delete data.id_per_lab; 
        let options = { 
            body: JSON.stringify(data), 
            headers: { "content-type": "application/json; charset=UTF-8" }, 
            method: "PUT", 
        }; 
        options.signal = controller.signal; 
 
        setTimeout(() => controller.abort(), 3000); 
 
        const response = await fetch(url, options) 
        const responseJson = await response.json() 
        return responseJson 
    } catch (error) { 
        //console.error("Error:", error); 
        return { "Error": error }; 
    } 
} 
 
export async function delete_periodo_laboral(id_per_lab) { 
    try { 
        let isDelete = window.confirm( 
            `¿Estás seguro de eliminar el registro con el id '${id_per_lab}'?` 
        ); 
        if (!isDelete) { 
            return; 
        } 
 
        const controller = new AbortController(); 
        let url_id = `${url}/${id_per_lab}`; 
        let options = { 
            headers: { "content-type": "application/json; charset=UTF-8" }, 
            method: "DELETE", 
        }; 
        options.signal = controller.signal; 
 
        setTimeout(() => controller.abort(), 3000); 
 
        const response = await fetch(url_id, options) 
        const responseJson = await response.json() 
        return responseJson 
 
    } catch (error) { 
        console.error("Error:", error); 
    } 
} 