import { Url } from '../constants/global'
// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

const url = `${meServidorBackend}/produccion/articulofamilia`;

const cabecera = { 'Content-type': 'application/json; charset=UTF-8' }
export async function get_articulo_sap() {
    let url_id = `${Url}/produccion/get_articulo_sap/`;    
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson

}

export async function get_articulo_familia(cod_formulado = "0", cod_grupo = "0", cod_subarea = "0") {
    let url_id = `${url}/all?cod_formulado=${cod_formulado}&cod_grupo=${cod_grupo}&cod_subarea=${cod_subarea}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function get_articuloFamiliaActivo(cod_formulado = "0", cod_grupo = "0", cod_subarea = "0") {
    let url_id = `${url}/articulofamilia_activo?cod_formulado=${cod_formulado}&cod_grupo=${cod_grupo}&cod_subarea=${cod_subarea}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}


export async function insert_articulo_familia(meJson) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };
    const response = await fetch(`${meServidorBackend}/produccion/articulofamilia/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function update_articulo_familia(meJson) {
    const requestOptions = {
        method: 'PUT',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/articulofamilia/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}


export async function delete_articulo_familia(meJson) {
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/produccion/articulofamilia/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function lista_familias_activas() {

    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }

    const response = await fetch(`${meServidorBackend}/produccion/familia/Activos/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function get_articulo_sap_filtro(pFiltro) {
    let url_id = `${Url}/produccion/get_articulo_sap_filtro/${pFiltro}`;    
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson

}