import {Url} from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

const url = `${meServidorBackend}/produccion/produccion_pendiente`;

export async function get_produccion_pendiente_all() {
    const response = await fetch(url)
    const responseJson = await response.json()
    return responseJson
}

export async function insert_produccion_pendiente(data) {
    try {
        // insertar en la base de datos
        console.log(data)
        const controller = new AbortController();
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);
        const response = await fetch(url, options);
        const responseJson = await response.json();

        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
}

export async function delete_produccion_pendiente(id_plan) {
    try {
        let isDelete = window.confirm(
            `¿Estás seguro de eliminar el registro con el id '${id_plan}'?`
        );
        if (!isDelete) {
            return;
        }

        const controller = new AbortController();
        let url_id = `${meServidorBackend}/venta/produccion_pendiente/${id_plan}`;
        let options = {
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "DELETE",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
}
