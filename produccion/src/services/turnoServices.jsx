import {Url} from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

const url = `${meServidorBackend}/produccion/turno`;

export async function get_turno() {
    const response = await fetch(url)
    const responseJson = await response.json()
    return responseJson
}

export async function get_turno_activo(cod_turno) {
    let url_id = `${url}/activo`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function get_turno_det(cod_turno) {
    let url_id = `${url}/${cod_turno}`;
    const response = await fetch(url_id)
    const responseJson = await response.json()
    return responseJson
}

export async function insert_turno(data) {
    try {
        const controller = new AbortController();
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "POST",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url, options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function update_turno(data) {
    try {
        const controller = new AbortController();
        //let url_id = `${url}/${data.cod_turno}`;
        //delete data.cod_turno;
        let options = {
            body: JSON.stringify(data),
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "PUT",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url, options)
        const responseJson = await response.json()
        return responseJson
    } catch (error) {
        console.error("Error:", error);
    }
}

export async function delete_turno(cod_turno) {
    try {
        let isDelete = window.confirm(
            `¿Estás seguro de eliminar el registro con el id '${cod_turno}'?`
        );
        if (!isDelete) {
            return;
        }

        const controller = new AbortController();
        let url_id = `${url}/${cod_turno}`;
        let options = {
            headers: { "content-type": "application/json; charset=UTF-8" },
            method: "DELETE",
        };
        options.signal = controller.signal;

        setTimeout(() => controller.abort(), 3000);

        const response = await fetch(url_id, options)
        const responseJson = await response.json()
        return responseJson

    } catch (error) {
        console.error("Error:", error);
    }
}

