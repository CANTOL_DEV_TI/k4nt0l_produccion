import {Url} from '../constants/global'

// const meServidorBackend = 'http://127.0.0.1:8000'
// const meServidorBackend = 'http://192.168.5.21:8090'
const meServidorBackend = Url

//const token = await localStorage.getItem("CDTToken")    

//const cabecera = {'Content-type': 'application/json; charset=UTF-8',"Authorization": "Bearer " + token}
const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function getFilter_PlanCapacidadPT(txtFind) {
    if (txtFind.trim()===''){
        txtFind='%20'
    }
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }

    const response = await fetch(`${meServidorBackend}/produccion/plan_capacidad_pt/${txtFind}`)//,requestOptions)
    const responseJson = await response.json()
    return responseJson
}
