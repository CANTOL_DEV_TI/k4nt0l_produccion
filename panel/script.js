// Función para cambiar el fondo cada 7 segundos

document.addEventListener("DOMContentLoaded", function () {
    const backgroundElement = document.querySelector('.background');
    let currentImage = 1;
    function changeBackground() {
      if (currentImage === 1) {
        backgroundElement.classList.remove('image1');
        backgroundElement.classList.add('image2');
        currentImage = 2;
      } else if (currentImage === 2) {
        backgroundElement.classList.remove('image2');
        backgroundElement.classList.add('image3');
        currentImage = 3;
      }else{
        backgroundElement.classList.remove('image3');
        backgroundElement.classList.add('image1');
        currentImage = 1;
      }
    }
  
    setInterval(changeBackground, 7000); // Cambia la imagen automáticamente cada 7 segundos
  });


  // Función para redirigir al seleccionar una opción
function redirectOnSelect() {
  const selectElement = document.getElementById('enlacesSelect');
  const selectedValue = selectElement.value;

  if (selectedValue === 'puntos') {
    window.open('http://puntoscantol.cantol.com.pe/puntoscliente', '_blank')
  }else if(selectedValue === 'gdh'){
    window.open('https://linktr.ee/gdhcantol', '_blank')
  }else if(selectedValue === 'glpi'){
    window.open('http://glpi.tecnopress.pe', '_blank')
  }else if(selectedValue === 'prq'){
    window.open('http://192.168.5.10:8082/home/index', '_blank')
  }else if(selectedValue === 'adryan'){
    window.open('https://adryanweb.com/AdryanWeb/Adryan/Account/LogIn', '_blank')
  }else if(selectedValue === 'ssoma'){
    window.open('https://grupocantol.app.lausapp.com/', '_blank')
  }

  selectElement.selectedIndex = 0;
  

}


  
  