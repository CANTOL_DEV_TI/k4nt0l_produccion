from pydantic import BaseModel
from typing import Optional

class centrocostousuario_schema(BaseModel):
    cia : Optional[str]
    centrocosto_id : Optional[str]
    centrocosto_nombre : Optional[str]
    usuario : Optional[str]
    rol_requerimiento : Optional[str]
    empleado_sap : Optional[int]

    class Config: 
        orm_mode = True