from pydantic import BaseModel
from typing import Optional

class requerimientosgestion_schema(BaseModel):
    cia : Optional[str]
    codigo_rq : Optional[str]
    estado : Optional[str]
    usuario_aprueba : Optional[str]
    usuario : Optional[str]
    observaciones : Optional[str]