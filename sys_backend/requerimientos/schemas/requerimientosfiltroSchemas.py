from pydantic import BaseModel
from typing import Optional

class filtrorq(BaseModel):
    cia : Optional[str]
    desde : Optional[str]
    hasta : Optional[str]
    estado : Optional[str]
    nro_sol : Optional[str]
    usuario: Optional[str]
    moneda : Optional[str]
    