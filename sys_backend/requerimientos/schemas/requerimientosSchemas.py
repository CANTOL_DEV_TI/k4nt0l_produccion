from pydantic import BaseModel
from typing import Optional,List

class requerimiento_detalle_schema(BaseModel):
    cia : Optional[str]
    codigo_rq : Optional[int]
    linea  : Optional[int]
    itemcode  : Optional[str]
    itemname  : Optional[str]
    fecha_necesaria  : Optional[str]
    cantidad_requerida  : Optional[float]
    precio_sin_igv : Optional[float]
    unidad_medida_id  : Optional[str]
    comentario_linea  : Optional[str]
    cuenta_linea : Optional[str]
    catpre_codigo : Optional[str]
    catpre_nombre : Optional[str]
    proyecto : Optional[str]

class requerimiento_schema(BaseModel):
    cia : Optional[str]
    codigo_rq : Optional[int]
    centro_costo_id : Optional[str]
    fecha_pedido : Optional[str]
    fecha_necesaria : Optional[str]
    fecha_vencimiento : Optional[str]
    usuario : Optional[str]
    empleado_sap : Optional[str]
    estado : Optional[str]
    proveedor_id : Optional[str]
    proveedor_nombre : Optional[str]
    ruc_proveedor : Optional[str]
    forma_pago_id : Optional[int]
    forma_pago_descripcion : Optional[str]
    comentario : Optional[str]    
    moneda : Optional[str]
    lineas : Optional[List[requerimiento_detalle_schema]]

    class Config: 
        orm_mode = True
	