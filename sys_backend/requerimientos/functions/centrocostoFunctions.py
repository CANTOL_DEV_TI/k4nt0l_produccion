from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from conexiones.conexion_sap_solo import conexion_SAP_Tecno
from generales.generales import CiaSAP
from requerimientos.schemas.centrocostoSchemas import centrocostousuario_schema

def CentroCostoSAP(pCia:str):
    try:
        resultado = []

        conn = conexion_SAP_Tecno()

        cur_lista = conn.cursor()

        sCia = CiaSAP(pCia)
        
        sqlccl = f"select \"PrcCode\" AS CC_Codigo,\"PrcName\" AS CC_Nombre from {sCia}.OPRC WHERE LENGTH (\"PrcCode\") = 7 AND \"Locked\" = 'N'"
        print(sqlccl)

        cur_lista.execute(sqlccl)

        for item in cur_lista:
            fila = {"CC_Codigo":item['CC_Codigo'],"CC_Nombre":item['CC_Nombre']}
            resultado.append(fila)

        return resultado
    except Exception as err:
        return f"Error : {err}."

def CentroCostoSAPBusca(pCia:str,pFiltro:str):
    try:
        resultado = []
        print(pFiltro)
        if((pFiltro=="%20") or(pFiltro==" ")):
            pFiltro = ""

        conn = conexion_SAP_Tecno()

        cur_lista = conn.cursor()

        sCia = CiaSAP(pCia)
        
        sqlccl = f"select \"PrcCode\" AS CC_Codigo,\"PrcName\" AS CC_Nombre from {sCia}.OPRC WHERE LENGTH (\"PrcCode\") = 7 AND \"Locked\" = 'N' AND \"PrcName\" like '%{pFiltro.strip()}%'"
        print(sqlccl)
        cur_lista.execute(sqlccl)

        for item in cur_lista:
            fila = {"CC_Codigo":item['CC_Codigo'],"CC_Nombre":item['CC_Nombre']}
            resultado.append(fila)

        return resultado
    except Exception as err:
        return f"Error : {err}."

def CentroCostoxUsuario(pCia:str,pUsuario:str):
    try:
        resultado = []

        conn = ConectaSQL_Produccion()

        cur_lista = conn.cursor(as_dict = True)

        sqlccxu = f"select centro_costo,cc_desc,usuario_empleadosap from presu_cc_usuario pcu where usuario='{pUsuario}' and cia='{pCia}'"
        print(sqlccxu)
        cur_lista.execute(sqlccxu)

        resultado = cur_lista.fetchall()

        return resultado
    except Exception as err:
        return f"Error : {err}."
    
def AgregarCCUsuario(pData:centrocostousuario_schema):
    try:
        print(pData)
        sql = f"insert into presu_cc_usuario values('{pData.cia}','{pData.usuario}','{pData.centrocosto_id}','{pData.centrocosto_nombre}','{pData.rol_requerimiento}',{pData.empleado_sap})"
        print(sql)
        conn = ConectaSQL_Produccion()

        cur_add = conn.cursor()

        cur_add.execute(sql)

        conn.commit()

        conn.close()

        return "Centro de Costo Agregado."

    except Exception as err:
        return f"Error : {err}"

def QuitarCCUsuario(pData:centrocostousuario_schema):
    try:
        sql = f"delete from presu_cc_usuario where cia='{pData.cia}' and usuario='{pData.usuario}' and centro_costo='{pData.centrocosto_id}' "

        conn = ConectaSQL_Produccion()

        cur_add = conn.cursor()

        cur_add.execute(sql)

        conn.commit()

        conn.close()

        return "Centro de Costo Removido."

    except Exception as err:
        return f"Error : {err}"

def ActualizarCCUsuario(pData:centrocostousuario_schema):
    try:
        sql = f"update presu_cc_usuario set rol_requerimiento='{pData.rol_requerimiento}', usuario_empleadosap={pData.empleado_sap}  where cia='{pData.cia}' and usuario='{pData.usuario}' and centro_costo='{pData.centrocosto_id}' "
        print(sql)
        conn = ConectaSQL_Produccion()

        cur_add = conn.cursor()

        cur_add.execute(sql)

        conn.commit()

        conn.close()

        return "Centro de Costo Actualizado."

    except Exception as err:
        return f"Error : {err}"

def ListadoCCxUsuarioxCia(pData:centrocostousuario_schema):
    try:
        sql = "select cia,usuario,centro_costo,cc_desc, rol_requerimiento, usuario_empleadosap,"
        sql += "case rol_requerimiento when 'A' then 'Aprobador' when 'S' then 'Solicitador' when 'M' then 'Solicitador/Aprobador' end as rol_descripcion"
        sql += f" from presu_cc_usuario where usuario like '%{pData.usuario}%' and cia = '{pData.cia}'"
        print(sql)
        conn = ConectaSQL_Produccion()

        cur_lista = conn.cursor(as_dict = True)

        cur_lista.execute(sql)

        resultado = cur_lista.fetchall()

        return resultado
    except Exception as err:
        return f"Error : {err}."
    
def AplicarTodosCC(pData:centrocostousuario_schema):
    try:

        listaCC = CentroCostoSAP(pData.cia)
        
        for filacc in listaCC:

            cc_id = filacc['CC_Codigo']
            cc_nombre = filacc['CC_Nombre']
            
            trama = centrocostousuario_schema

            trama.cia = pData.cia
            trama.centrocosto_id = cc_id
            trama.centrocosto_nombre = cc_nombre
            trama.usuario = pData.usuario
            trama.rol_requerimiento = pData.rol_requerimiento
            trama.empleado_sap = pData.empleado_sap 
                        
            mensaje = AgregarCCUsuario(trama)
            print(mensaje)

        return "Centros de Costo asignados al usuario."
    except Exception as err:
        return f"Error : {err}."
