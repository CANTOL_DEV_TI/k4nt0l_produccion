from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from conexiones.conexion_sap_solo import conexion_SAP_Tecno
from generales.generales import CiaSAP
from typing import List

def ListaEmpleadosSAPSWC(pCia:str):
    try:
        resultado = []

        sqlswccc = f"select usuario_empleadosap from presu_cc_usuario where cia='{pCia}' and isnull(usuario_empleadosap,0)<>0"

        conn = ConectaSQL_Produccion()

        cur_lista = conn.cursor(as_dict = True)

        cur_lista.execute(sqlswccc)

        resultado = cur_lista.fetchall()

        return resultado
    except Exception as err:
        return f"Error : {err}."


def ListaEmpleadoSAPRRHH(pCia:str,pFiltro:str):

    try:
        if((pFiltro=="%20") or(pFiltro==" ")):
            pFiltro = ""

        listaswc = ListaEmpleadosSAPSWC(pCia)

        listaE = []

        for emp in listaswc:
            listaE.append(emp['usuario_empleadosap'])
        
        listaT = tuple(listaE)
        
        resultado = []

        conn = conexion_SAP_Tecno()

        cur_lista = conn.cursor()

        sCia = CiaSAP(pCia)
        
        sqlccl = f"select \"empID\" AS empID, \"lastName\" || ', ' || IFNULL(\"firstName\",'')|| ' ' || IFNULL(\"middleName\",'') AS Empleado from {sCia}.OHEM "
        
        #if(len(listaT) == 0):
        sqlccl += f"WHERE \"Active\"='Y' and \"lastName\" || ', ' || IFNULL(\"firstName\",'')|| ' ' || IFNULL(\"middleName\",'') like '%{pFiltro.strip()}%' ORDER BY \"lastName\" "            
        #else:
        #    if(len(listaT) == 1):
        #        uno =str(listaT)[:-2] + ")"
        #        sqlccl += f"WHERE \"Active\"='Y' and \"lastName\" || ', ' || IFNULL(\"firstName\",'')|| ' ' || IFNULL(\"middleName\",'') like '%{pFiltro.strip()}%' and \"empID\" not in {uno} ORDER BY \"lastName\" "
        #    else:
        #        sqlccl += f"WHERE \"Active\"='Y' and \"lastName\" || ', ' || IFNULL(\"firstName\",'')|| ' ' || IFNULL(\"middleName\",'') like '%{pFiltro.strip()}%' and \"empID\" not in {listaT} ORDER BY \"lastName\" "

        cur_lista.execute(sqlccl)

        for item in cur_lista:
            fila = {"empID":item['empID'],"Empleado":item['Empleado']}
            resultado.append(fila)

        return resultado
    except Exception as err:
        return f"Error : {err}."
 
def ListaEmpleadoSAPRRHH_All(pCia:str):

    try:    
        resultado = []

        conn = conexion_SAP_Tecno()

        cur_lista = conn.cursor()

        sCia = CiaSAP(pCia)
        
        sqlccl = f"select \"empID\" AS empID, \"lastName\" || ', ' || IFNULL(\"firstName\",'')|| ' ' || IFNULL(\"middleName\",'') AS Empleado from {sCia}.OHEM "
        
        cur_lista.execute(sqlccl)

        for item in cur_lista:
            fila = {"empID":item['empID'],"Empleado":item['Empleado']}
            resultado.append(fila)

        return resultado
    except Exception as err:
        return f"Error : {err}."