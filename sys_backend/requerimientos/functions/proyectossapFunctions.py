from conexiones import conexion_sap_solo
from generales.generales import CiaSAP

def ListaProyectos(pCia:str, pFiltro:str):
    try:
        
        if(pFiltro=="%20")or(pFiltro==" "):
            pFiltro=""

        lista = []
        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_lista = conn.cursor()

        sCia = CiaSAP(pCia)

        sql = f"select \"PrjCode\" AS Proy_Codigo , \"PrjName\" AS Proy_Nombre from {sCia}.OPRJ "
        sql += f"WHERE \"Locked\" = 'N' AND \"Active\" ='Y' AND (\"PrjCode\" like '%{pFiltro.strip()}%' OR \"PrjName\" like '%{pFiltro.strip()}%')"
        
        cur_lista.execute(sql)

        for dato in cur_lista:
            fila = {"Proy_Codigo":dato['Proy_Codigo'], "Proy_Nombre":dato['Proy_Nombre']}
            lista.append(fila)

        return lista
    except Exception as err:
        return f"Error :{err}."