from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from requerimientos.schemas.requerimientosfiltroSchemas import filtrorq
from requerimientos.schemas.requerimientosSchemas import requerimiento_schema
from requerimientos.schemas.requerimientosgestionSchemas import requerimientosgestion_schema
from generales.notificaciones import enviarnotificacionrq,ExtraerCorreoUsuario
from requerimientos.functions.PurchaseRequestSAPFunctions import GenerarSolicitudCompraSAP

def GeneraCodigoRQ(pCia:str):
    try:
        registro = {}

        conn = ConectaSQL_Produccion()
        cur_gen = conn.cursor(as_dict = True)

        sqlGI = f"select isnull(max(codigo_rq),0) + 1 as codigo from logi_requerimiento_cabecera where cia='{pCia}'"
        
        cur_gen.execute(sqlGI)

        resultado = cur_gen.fetchall()

        for i in resultado:
            registro = i['codigo']

        conn.close()
        
        return registro
    except Exception as err:
        print(err)
        return f"Error GCRQ:{err}."

def ExtraerAprobadores(pCia:str,pCCosto:str):
    try:
        resultado = []

        conn = ConectaSQL_Produccion()

        sqlU = f"select centro_costo,cc_desc,usuario from presu_cc_usuario pcu where cia = '{pCia}' and centro_costo = '{pCCosto}' and rol_requerimiento in ('A','M')"
        
        cur_aprobadores = conn.cursor(as_dict = True)

        cur_aprobadores.execute(sqlU)

        resultado = cur_aprobadores.fetchall()
        
        conn.close()

        return resultado
    except Exception as err:
        return f"Error EA : {err}."
  
def Listado(pData:filtrorq):    
    try:
        resultado = []
        conn = ConectaSQL_Produccion()
        cur_lista = conn.cursor(as_dict = True)
        
        sqlT = "select lrc.cia,lrc.codigo_rq,centro_costo_id,pcu.cc_desc as centro_costo_desc,format(fecha_pedido,'yyyyMMdd') as fecha_pedido,format(lrc.fecha_necesaria,'yyyyMMdd') as fecha_necesaria"
        sqlT += ",format(fecha_vencimiento,'yyyyMMdd') as fecha_vencimiento,lrc.usuario,format(fecha_registro,'yyyyMMdd hh:mm') as fecha_registro,"
        sqlT += "estado, case estado when 'P' then 'Pendiente de Aprobacion' when 'A' then 'Aprobada' when 'S' then 'En SAP' when 'E' then 'Eliminado' "
        sqlT += "when 'R' then 'Rechazado' when 'M' then 'Migrado' end as estado_des, isnull(lrc.SAP_DocEntry,'') as sap_docentry, isnull(lrc.SAP_DocNum,'') as sap_docnum , count(lrd.linea) as lineas"
        sqlT += ",isnull(comentario,'') as comentario,moneda from logi_requerimiento_cabecera lrc inner join logi_requerimiento_detalle lrd on lrc.cia = lrd.cia and lrc.codigo_rq = lrd.codigo_rq "
        sqlT += " inner join presu_cc_usuario pcu on pcu.cia = lrc.cia and pcu.centro_costo = lrc.centro_costo_id and pcu.usuario = lrc.usuario "
        sqlT += f"where lrc.cia = '{pData.cia}' and lrc.usuario='{pData.usuario}' and (format(fecha_registro,'yyyyMMdd') between '{pData.desde}' and '{pData.hasta}') "        

        if(len(pData.estado)!=0):
            sqlT +=  f"and estado = '{pData.estado}' " 
        
        if(len(pData.nro_sol)!=0):
            sqlT += f"and lrc.codigo_rq = {pData.nro_sol} "

        sqlT += "group by lrc.cia,lrc.codigo_rq,centro_costo_id,pcu.cc_desc,fecha_pedido,lrc.fecha_necesaria,fecha_vencimiento,lrc.usuario,estado,fecha_registro, estado, "
        sqlT += "case estado when 'P' then 'Pendiente de Aprobacion' when 'A' then 'Aprobada' when 'S' then 'En SAP' when 'R' then 'Rechazado'"
        sqlT += "when 'M' then 'Migrado' end,lrc.SAP_DocEntry, lrc.SAP_DocNum ,comentario,moneda order by fecha_registro desc"
        
        print(sqlT)

        cur_lista.execute(sqlT)

        #resultado = cur_lista.fetchall()

        for f in cur_lista:
            fila = {"cia":f['cia'],"codigo_rq":f['codigo_rq'],"centro_costo_id":f['centro_costo_id'],"centro_costo_desc":f['centro_costo_desc'],"fecha_pedido":f['fecha_pedido'],
                    "fecha_necesaria":f['fecha_necesaria'],"fecha_vencimiento":f['fecha_vencimiento'],"usuario":f['usuario'],"fecha_registro":f['fecha_registro'],
                    "estado":f['estado'],"estado_des":f['estado_des'],"sap_docentry":f['sap_docentry'],"sap_docnum":f['sap_docnum'],"moneda":f['moneda'],"lineas":f['lineas'],
                    "detalles":DetalleRQ(pData.cia,f['codigo_rq'],)}
            resultado.append(fila)
        
        conn.close()

        return resultado
    except Exception as err:
        print(err)
        return f"Error L : {err}."

def NuevaRQ(pData:requerimiento_schema):
    try:    
        ID = GeneraCodigoRQ(pData.cia)
        
        conn = ConectaSQL_Produccion()
        cur_agregar = conn.cursor()
        print("Cabecera...")
        sqlI = "insert into logi_requerimiento_cabecera(cia,codigo_rq,centro_costo_id,fecha_pedido,fecha_necesaria,fecha_vencimiento,usuario,"
        sqlI += "proveedor_id,proveedor_nombre,ruc_proveedor,forma_pago_id,forma_pago_descripcion,comentario,empleado_sap,moneda) "
        sqlI += f"values('{pData.cia}',{ID},'{pData.centro_costo_id}','{pData.fecha_pedido}','{pData.fecha_necesaria}','{pData.fecha_vencimiento}','{pData.usuario}',"
        sqlI += f"'{pData.proveedor_id}','{pData.proveedor_nombre}','{pData.ruc_proveedor}',{pData.forma_pago_id},'{pData.forma_pago_descripcion}','{pData.comentario}'"
        sqlI += f",{pData.empleado_sap},'{pData.moneda}')"
        print(sqlI)
        cur_agregar.execute(sqlI)

        for fila in pData.lineas:
            print("Detalle....")
            sqlID = "insert into logi_requerimiento_detalle(cia,codigo_rq,linea,itemcode,itemname,fecha_necesaria,cantidad_requerida,unidad_medida_id,"
            sqlID += "comentario_linea,precio_sin_igv,cuenta_linea,catpre_codigo,catpre_nombre,proyecto)"
            sqlID += f"values ('{pData.cia}',{ID},{fila.linea},'{fila.itemcode}','{fila.itemname}','{fila.fecha_necesaria}',{fila.cantidad_requerida},"
            sqlID += f"'{fila.unidad_medida_id}','{fila.comentario_linea}',{fila.precio_sin_igv},'{fila.cuenta_linea}','{fila.catpre_codigo}','{fila.catpre_nombre}'"
            sqlID += f",'{fila.proyecto}')"
        #    print(sqlID)
            cur_agregar.execute(sqlID)
                
        aprobadores = ExtraerAprobadores(pData.cia,pData.centro_costo_id)
        
        fila = 1

        for aprobador in aprobadores:
            print("Aprobadores...")
            sqlAp =  "insert into logi_requerimiento_aprobaciones(cia,codigo_rq,linea,usuario_rq,usuario_aprueba,fecha_actualizacion)"
            sqlAp += f"values('{pData.cia}',{ID},{fila},'{pData.usuario}','{aprobador['usuario']}',getdate())"        
            fila = fila + 1
            
            cur_agregar.execute(sqlAp)

            enviarnotificacionrq(pData.cia,pData.usuario,aprobador['usuario'],ID)
            
        conn.commit()

        conn.close()
        
        return f"Se registro correctamente el requerimiento nro {ID}."
    except Exception as err:
        print(err)
        return f"Error NRQ: {err}."

def ActualizarRQ(pData:requerimiento_schema):
    try:
        
        val = ValidarEstado(pData.cia, pData.codigo_rq, "Editar")
        
        if(val['codigo'] == -1):
            return val['mensaje']
        
        conn = ConectaSQL_Produccion()
        cur_actualizar = conn.cursor()

        sqlA = "update logi_requerimiento_cabecera set "
        sqlA += f"centro_costo_id = '{pData.centro_costo_id}',fecha_pedido='{pData.fecha_pedido}',fecha_necesaria ='{pData.fecha_necesaria}',"
        sqlA += f"fecha_vencimiento='{pData.fecha_vencimiento}', proveedor_id='{pData.proveedor_id}', proveedor_nombre='{pData.proveedor_nombre}', "
        sqlA += f"forma_pago_id={pData.forma_pago_id}, forma_pago_descripcion='{pData.forma_pago_descripcion}', comentario = '{pData.comentario}', ruc_proveedor='{pData.ruc_proveedor}' "
        sqlA += f", moneda = '{pData.moneda}' where cia='{pData.cia}' and codigo_rq={pData.codigo_rq}"

        cur_actualizar.execute(sqlA)

        sqlDD = f"delete from logi_requerimiento_detalle where cia='{pData.cia}' and codigo_rq='{pData.codigo_rq}'"

        cur_actualizar.execute(sqlDD)

        for fila in pData.lineas:
            sqlAD = "insert into logi_requerimiento_detalle(cia,codigo_rq,linea,itemcode,itemname,fecha_necesaria,cantidad_requerida,unidad_medida_id,"
            sqlAD += "comentario_linea,precio_sin_igv,cuenta_linea,catpre_codigo,catpre_nombre,proyecto)"
            sqlAD += f"values ('{fila.cia}',{fila.codigo_rq},{fila.linea},'{fila.itemcode}','{fila.itemname}','{fila.fecha_necesaria}',{fila.cantidad_requerida},"
            sqlAD += f"'{fila.unidad_medida_id}','{fila.comentario_linea}',{fila.precio_sin_igv},'{fila.cuenta_linea}','{fila.catpre_codigo}','{fila.catpre_nombre}'"
            sqlAD += f",'{fila.proyecto}')"

            cur_actualizar.execute(sqlAD)
        
        conn.commit()

        conn.close()

        return f"Se actualizo correctamente el requerimiento nro {pData.codigo_rq}."
    except Exception as err:
        print(f"Error UPD: {err}.")
        return f"Error UPD: {err}."

def EliminarRQ(pData:requerimiento_schema):
    try:
        
        val = ValidarEstado(pData.cia, pData.codigo_rq, "Eliminar")
        
        if(val['codigo'] == -1):
            print(val)
            return val['mensaje']
        
        print(val)

        conn = ConectaSQL_Produccion()
        cur_eliminar = conn.cursor()

        sqlR = f"update logi_requerimiento_cabecera set estado='E' where cia='{pData.cia}' and codigo_rq='{pData.codigo_rq}'"

        cur_eliminar.execute(sqlR)
        
        conn.commit()

        conn.close()

        return f"Se elimino correctamente el requerimiento nro {pData.codigo_rq}."
    except Exception as err:
        print(err)
        return f"Error : {err}."

def RechazarRQ(pData:requerimientosgestion_schema):
    try:
        conn = ConectaSQL_Produccion()
        cur_rechazar = conn.cursor()

        sqlD = f"update logi_requerimiento_cabecera set estado='R' where cia='{pData.cia}' and codigo_rq='{pData.codigo_rq}'"

        cur_rechazar.execute(sqlD)

        sqlIR = f"update logi_requerimiento_aprobaciones set estado='R',observaciones='{pData.observaciones}',fecha_actualizacion=getdate() where cia='{pData.cia}' and codigo_rq='{pData.codigo_rq}' and usuario_aprueba='{pData.usuario_aprueba}'"

        cur_rechazar.execute(sqlIR)

        conn.commit()

        conn.close()

        return f"Se elimino correctamente el requerimiento nro {pData.codigo_rq}."
    except Exception as err:
        return f"Error : {err}."
    
def VerificarAprobaciones(pCia:str, pRQ:str):
    try:
        conn = ConectaSQL_Produccion()
        
        sqlV = "select isnull(count(*),0) as aprobaciones_pendientes from logi_requerimiento_aprobaciones lra "
        sqlV += f"where lra.cia = '{pCia}' and lra.codigo_rq = {pRQ} and estado = 'P' "

        curV = conn.cursor(as_dict = True)

        curV.execute(sqlV)

        res = curV.fetchall()

        totApr = 0

        for i in res:
            totApr = i['aprobaciones_pendientes']

        return totApr
    except Exception as err:
        return f"Error : {err}."

def AprobarRQ(pData:requerimientosgestion_schema):
    try:
        conn = ConectaSQL_Produccion()
        cur_aprobar = conn.cursor()

        sqlIR = f"update logi_requerimiento_aprobaciones set estado='A',observaciones='{pData.observaciones}',fecha_actualizacion=getdate() where cia='{pData.cia}' and codigo_rq='{pData.codigo_rq}' and usuario_aprueba='{pData.usuario_aprueba}'"

        cur_aprobar.execute(sqlIR)

        conn.commit()

        ApPen = VerificarAprobaciones(pData.cia, pData.codigo_rq)
        
        if(ApPen==0):
            sqlD = f"update logi_requerimiento_cabecera set estado='A' where cia='{pData.cia}' and codigo_rq='{pData.codigo_rq}'"

            cur_aprobar.execute(sqlD)

            conn.commit()

            conn.close()

            Trama = VerRequerimiento(pData.cia,pData.codigo_rq)

            EnvioSAP = GenerarSolicitudCompraSAP(pData.cia, pData.codigo_rq, Trama, pData.usuario_aprueba)

            print(EnvioSAP)

            mensajeT = f"Se Aprobo correctamente el requerimiento nro {pData.codigo_rq}\n" + EnvioSAP

            return mensajeT
        else: 
            return f"Quedan Aprobaciones pendientes para el requerimiento nro {pData.codigo_rq}" 
    except Exception as err:
        return f"Error : {err}."

def VerRequerimiento(pCia:str, pNro:str):
    try:
        
        resultado = {}
        conn = ConectaSQL_Produccion()
        cur_requerimiento = conn.cursor(as_dict = True)

        sqlCab = "select cia,codigo_rq,centro_costo_id,format(fecha_pedido,'yyyy-MM-dd') as fecha_pedido,format(fecha_necesaria,'yyyy-MM-dd') as fecha_necesaria,"
        sqlCab += "format(fecha_vencimiento,'yyyy-MM-dd') as fecha_vencimiento,usuario,estado,fecha_registro,proveedor_id,isnull(ruc_proveedor,'') as ruc_proveedor,"
        sqlCab += "proveedor_nombre,isnull(sap_docentry,'') as sap_docentry,"
        sqlCab += "isnull(sap_docnum,'') as sap_docnum,forma_pago_id,forma_pago_descripcion,empleado_sap,isnull(comentario,'') as comentario ,"
        sqlCab += f"dbo.fu_email_usuario_solicita(usuario) as email_usuario,moneda from logi_requerimiento_cabecera where cia='{pCia}' and codigo_rq={pNro}"
        
        sqlDet = "select cia,codigo_rq,linea,itemcode,itemname,format(fecha_necesaria,'yyyy-MM-dd') as fecha_necesaria,cantidad_requerida,precio_sin_igv,"
        sqlDet += "cuenta_linea,catpre_codigo,catpre_nombre,unidad_medida_id,comentario_linea,proyecto"
        sqlDet += f" from logi_requerimiento_detalle where cia='{pCia}' and codigo_rq={pNro}"
        
        sqlApr = f"select cia,codigo_rq,linea,usuario_rq,usuario_aprueba,estado,fecha_actualizacion,observaciones ,"        
        sqlApr += f" dbo.fu_nombre_usuario_aprueba(usuario_aprueba) as usuario_aprueba_nombre from logi_requerimiento_aprobaciones where cia='{pCia}' and codigo_rq={pNro}"        

        cur_requerimiento.execute(sqlCab)

        cabecera  = cur_requerimiento.fetchall()

        cur_requerimiento.execute(sqlDet)

        detalle = cur_requerimiento.fetchall()

        cur_requerimiento.execute(sqlApr)

        aprobaciones = cur_requerimiento.fetchall()
        
        usuario_nombre = ExtraerEmail(cabecera[0]['usuario'])        

        resultado = {"cia":cabecera[0]['cia'],"codigo_rq":cabecera[0]['codigo_rq'],"centro_costo_id":cabecera[0]['centro_costo_id'],"fecha_pedido":cabecera[0]['fecha_pedido']
                     ,"fecha_necesaria":cabecera[0]['fecha_necesaria'],"fecha_vencimiento":cabecera[0]['fecha_vencimiento'],"usuario":cabecera[0]['usuario']
                     ,"estado":cabecera[0]['estado'],"fecha_registro":cabecera[0]['fecha_registro'],"proveedor_id":cabecera[0]['proveedor_id']
                     ,"proveedor_nombre":cabecera[0]['proveedor_nombre'],"forma_pago_id":cabecera[0]['forma_pago_id'],"forma_pago_descripcion":cabecera[0]['forma_pago_descripcion']
                     ,"empleado_sap" : cabecera[0]['empleado_sap'],'usuario_nombre': usuario_nombre['Usuario'], "comentario" : cabecera[0]['comentario']
                     ,"sap_docentry":cabecera[0]['sap_docentry'],"sap_docnum":cabecera[0]['sap_docnum'], "usuario_email" : cabecera[0]['email_usuario']
                     ,"ruc_proveedor":cabecera[0]['ruc_proveedor'], "moneda":cabecera[0]['moneda'], "detalle" : detalle, "aprobaciones" : aprobaciones}
        
        conn.close()

        return resultado
    except Exception as err:
        return f"Error : {err}."
    
def BandejaAprobaciones(pData:filtrorq):
    try:
        sqlBA = "select lrc.cia,lrc.codigo_rq,centro_costo_id,pcu.cc_desc as centro_costo_desc,format(fecha_pedido,'yyyyMMdd') as fecha_pedido,format(lrc.fecha_necesaria,'yyyyMMdd') as fecha_necesaria,"
        sqlBA += "format(fecha_vencimiento,'yyyyMMdd') as fecha_vencimiento,lrc.usuario,format(fecha_registro,'yyyyMMdd hh:mm') as fecha_registro,lrc.estado, "
        sqlBA += "case lrc.estado when 'P' then 'Pendiente' when 'A' then 'Aprobada' when 'S' then 'En SAP' when 'E' then 'Eliminado' when 'R' then 'Rechazado'"
        sqlBA += "when 'M' then 'Migrado' end as estado_des,isnull(lrc.SAP_DocEntry,'') as sap_docentry, isnull(lrc.SAP_DocNum,'') as sap_docnum,count(lrd.linea) as lineas "
        sqlBA += ",isnull(comentario,'') as comentario,moneda from logi_requerimiento_cabecera lrc inner join logi_requerimiento_detalle lrd on lrc.cia = lrd.cia and lrc.codigo_rq = lrd.codigo_rq "
        sqlBA += "inner join logi_requerimiento_aprobaciones lra on lra.cia = lrc.cia and lra.codigo_rq = lrc.codigo_rq "
        sqlBA += "inner join presu_cc_usuario pcu on pcu.cia = lrc.cia and pcu.centro_costo = lrc.centro_costo_id and pcu.usuario = lrc.usuario "
        sqlBA += f"where lrc.cia = '{pData.cia}' and lra.usuario_aprueba='{pData.usuario}' and (format(fecha_registro,'yyyyMMdd') between '{pData.desde}' and '{pData.hasta}') "

        if(len(pData.estado)!=0):
            sqlBA +=  f"and lra.estado = '{pData.estado}' " 
        
        if(len(pData.nro_sol)!=0):
            sqlBA += f"and lrc.codigo_rq = {pData.nro_sol} "

        sqlBA += "group by lrc.cia,lrc.codigo_rq,centro_costo_id,pcu.cc_desc,fecha_pedido,lrc.fecha_necesaria,fecha_vencimiento,lrc.usuario,fecha_registro, lrc.estado, "
        sqlBA += "case lrc.estado when 'P' then 'Pendiente' when 'A' then 'Aprobada' when 'S' then 'En SAP' when 'R' then 'Rechazado'  when 'M' then 'Migrado' end,comentario,"
        sqlBA += "lrc.SAP_DocEntry,lrc.SAP_DocNum,fecha_actualizacion,moneda order by fecha_actualizacion desc"

        conn = ConectaSQL_Produccion()

        cur_bandeja = conn.cursor(as_dict = True)

        cur_bandeja.execute(sqlBA)
        
        resultado = []

        for f in cur_bandeja:
            fila =  {"cia" : f['cia'], "codigo_rq" : f['codigo_rq'], "centro_costo_id" : f['centro_costo_id'], "centro_costo_desc" : f['centro_costo_desc'],
                     "fecha_pedido" : f['fecha_pedido'], "fecha_necesaria" : f['fecha_necesaria'], "fecha_vencimiento" : f['fecha_vencimiento'], 
                     "usuario" : f['usuario'],"fecha_registro" : f['fecha_registro'], "estado_des" : f['estado_des'], "sap_docentry" : f['sap_docentry'], 
                     "sap_docnum" : f['sap_docnum'],"lineas" : f['lineas'], "comentario" : f['comentario'], "moneda":f['moneda'], "detalles" : DetalleRQ(pData.cia,f['codigo_rq'],)}
            
            resultado.append(fila)

        return resultado
    
    except Exception as err:
        return f"Error : {err}."

def ExtraerEmail(pUser:str):    
    email = ExtraerCorreoUsuario(pUser)
    return email

def ValidarEstado(pCia:str,pRQ:str, pEvaluar:str):
    try:
        
        conn = ConectaSQL_Produccion()
        cur_valida = conn.cursor(as_dict = True)
        sqlV = ""
        mensaje = {}

        if(pEvaluar == 'Editar'):
            sqlV =  "select count(*) as ap,estado from logi_requerimiento_aprobaciones group by estado,cia,codigo_rq"
            sqlV += f" having cia = '{pCia}' and codigo_rq = {pRQ} and estado = 'A' "
        else:
            
            sqlV =  "select count(*) as ap,estado from logi_requerimiento_aprobaciones group by estado,cia,codigo_rq"
            sqlV += f" having cia = '{pCia}' and codigo_rq = {pRQ} and estado <> 'A' "
            print(sqlV)


        cur_valida.execute(sqlV)

        resultado = cur_valida.fetchall()
        
        aprobaciones = 0
        
        if(len(resultado) == 0):            
            aprobaciones = 0
        else:            
            for i in resultado:            
                aprobaciones = i['ap']
        
        if(pEvaluar == 'Editar'):            
            if(aprobaciones > 0):
                
                mensaje = {"codigo":-1,"mensaje":"El RQ tiene aprobaciones..."}
                
            else:
                mensaje = {"codigo":0,"mensaje":"El RQ tiene no aprobaciones..."}

        if(pEvaluar == 'Eliminar'):
            if(aprobaciones <= 0):
                mensaje = {"codigo":0,"mensaje":"El RQ no tiene aprobaciones... "}
            else:
                mensaje = {"codigo":-1,"mensaje":"El RQ tiene aprobaciones... "}
        
        return mensaje            
    except Exception as err:
        print(err)
        return f"Error VE: {err}."
    
def DetalleRQ(pCia:str,pRQ:str):
    try:
        resultado = []

        conn = ConectaSQL_Produccion()
        cur_valida = conn.cursor(as_dict = True)

        sqlV =  "select cia,codigo_rq,linea,itemcode,itemname,fecha_necesaria,cantidad_requerida,precio_sin_igv,unidad_medida_id,"
        sqlV += "comentario_linea,cuenta_linea,catpre_codigo,catpre_nombre,proyecto"
        sqlV += f" from logi_requerimiento_detalle where cia='{pCia}' and codigo_rq='{pRQ}'"
        
        cur_valida.execute(sqlV)

        resultado = cur_valida.fetchall()
        
        return resultado
    except Exception as err:
        return f"Error : {err}."
    
def Totales(pInfo:filtrorq):
    try:
        resultado = []

        sqlT = f"select 'Eliminados' as Tipo, count(*) as Total  from logi_requerimiento_cabecera lrc where cia = '{pInfo.cia}' "
        sqlT += f" and fecha_pedido between '{pInfo.desde}' and '{pInfo.hasta}' and estado = 'E' and usuario = '{pInfo.usuario}' union ALL "
        sqlT += f"select 'Pendientes' as Tipo, count(*) as Total  from logi_requerimiento_cabecera lrc where cia = '{pInfo.cia}' "
        sqlT += f" and fecha_pedido between '{pInfo.desde}' and '{pInfo.hasta}' and estado = 'P' and usuario = '{pInfo.usuario}' union ALL "
        sqlT += f"select 'Rechazados' as Tipo, count(*) as Total  from logi_requerimiento_cabecera lrc where cia = '{pInfo.cia}' "
        sqlT += f" and fecha_pedido between '{pInfo.desde}' and '{pInfo.hasta}' and estado = 'R' and usuario = '{pInfo.usuario}' union ALL "
        sqlT += f"select 'Aprobados' as Tipo, count(*) as Total  from logi_requerimiento_cabecera lrc where cia = '{pInfo.cia}' "
        sqlT += f" and fecha_pedido between '{pInfo.desde}' and '{pInfo.hasta}' and estado = 'A' and usuario = '{pInfo.usuario}' union all "
        sqlT += f"select 'Migrados' as Tipo, count(*) as Total  from logi_requerimiento_cabecera lrc where cia = '{pInfo.cia}' "
        sqlT += f" and fecha_pedido between '{pInfo.desde}' and '{pInfo.hasta}' and estado = 'M' and usuario = '{pInfo.usuario}' union all "
        sqlT += f"select 'Total' as Tipo, count(*) as Total  from logi_requerimiento_cabecera lrc where cia = '{pInfo.cia}' "
        sqlT += f" and fecha_pedido between '{pInfo.desde}' and '{pInfo.hasta}' and usuario = '{pInfo.usuario}' order by 1 "

        conn = ConectaSQL_Produccion()
        cur_totales = conn.cursor(as_dict = True)

        cur_totales.execute(sqlT)

        resultado = cur_totales.fetchall()

        campos = []
        valores = []

        for d in resultado:
            campos.append(d['Tipo'])
            valores.append(d['Total'])    
        
        totales = dict(zip(campos,valores)) 

        return totales
    except Exception as err:
        return f"Error : {err}."
    
def TotalesAprobadores(pInfo:filtrorq):
    try:
        resultado = []

        sqlT = "select 'Eliminados' as Tipo, count(*) as Total  from logi_requerimiento_cabecera lrc "
        sqlT += f"inner join logi_requerimiento_aprobaciones lra on lrc.cia = lra.cia and lrc.codigo_rq = lra.codigo_rq "
        sqlT += f"where lrc.cia = '{pInfo.cia}' and fecha_pedido between '{pInfo.desde}' and '{pInfo.hasta}' and lrc.estado = 'E' and usuario_aprueba = '{pInfo.usuario}'"
        sqlT += f" union ALL select 'Pendientes' as Tipo, count(*) as Total  from logi_requerimiento_cabecera lrc "
        sqlT += f"inner join logi_requerimiento_aprobaciones lra on lrc.cia = lra.cia and lrc.codigo_rq = lra.codigo_rq "
        sqlT += f"where lrc.cia = '{pInfo.cia}' and fecha_pedido between '{pInfo.desde}' and '{pInfo.hasta}' and lra.estado = 'P' and usuario_aprueba = '{pInfo.usuario}'"
        sqlT += f" union ALL select 'Rechazados' as Tipo, count(*) as Total  from logi_requerimiento_cabecera lrc "
        sqlT += f"inner join logi_requerimiento_aprobaciones lra on lrc.cia = lra.cia and lrc.codigo_rq = lra.codigo_rq "
        sqlT += f"where lrc.cia = '{pInfo.cia}' and fecha_pedido between '{pInfo.desde}' and '{pInfo.hasta}' and lra.estado = 'R' and usuario_aprueba = '{pInfo.usuario}'"
        sqlT += f" union ALL select 'Aprobados' as Tipo, count(*) as Total  from logi_requerimiento_cabecera lrc "
        sqlT += f"inner join logi_requerimiento_aprobaciones lra on lrc.cia = lra.cia and lrc.codigo_rq = lra.codigo_rq "
        sqlT += f"where lrc.cia = '{pInfo.cia}' and fecha_pedido BETWEEN '{pInfo.desde}' and '{pInfo.hasta}' and lra.estado = 'A' and usuario_aprueba = '{pInfo.usuario}'"
        sqlT += f" union all select 'Migrados' as Tipo, count(*) as Total  from logi_requerimiento_cabecera lrc "
        sqlT += f"inner join logi_requerimiento_aprobaciones lra on lrc.cia = lra.cia and lrc.codigo_rq = lra.codigo_rq "
        sqlT += f"where lrc.cia = '{pInfo.cia}' and fecha_pedido BETWEEN '{pInfo.desde}' and '{pInfo.hasta}' and lrc.estado = 'M' and usuario_aprueba = '{pInfo.usuario}'"
        sqlT += " order by 1 "

        conn = ConectaSQL_Produccion()
        cur_totales = conn.cursor(as_dict = True)

        cur_totales.execute(sqlT)

        resultado = cur_totales.fetchall()

        campos = []
        valores = []

        for d in resultado:
            campos.append(d['Tipo'])
            valores.append(d['Total'])    

        totales = dict(zip(campos,valores)) 

        return totales

    except Exception as err:
        return f"Error : {err}."