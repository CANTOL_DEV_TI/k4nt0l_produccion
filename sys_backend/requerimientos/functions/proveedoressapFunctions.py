from conexiones.conexion_sap_solo import conexion_SAP_Tecno
from generales.generales import CiaSAP


def ListadoProveedoresSAP(pCia:str,pItem:str):
    try:
        resultado = []

        sCia = CiaSAP(pCia)

        sqlLI = f"SELECT top 100 \"CardCode\" as CardCode,\"LicTradNum\" as RUC,\"CardName\" as CardName, o.\"GroupNum\" as CondPagoID , o2.\"PymntGroup\" as CondPagoDes"
        sqlLI += f", \"LicTradNum\" AS RUC,\"Currency\" AS Moneda  FROM {sCia}.OCRD o INNER JOIN {sCia}.OCTG o2 ON o.\"GroupNum\" = o2.\"GroupNum\" "
        sqlLI += f"WHERE \"CardType\" = 'S' AND \"CardCode\" LIKE 'P%' AND (\"CardName\" LIKE '%{pItem}%' OR \"LicTradNum\" LIKE '%{pItem}%' )"
        #print(sqlLI)
        conn = conexion_SAP_Tecno()

        cur_items = conn.cursor()

        cur_items.execute(sqlLI)

        for row in cur_items:
            fila = {"CardCode":row['CardCode'],"RUC":row['RUC'], "CardName":row['CardName'],"RUC":row['RUC'],"CondPagoID":row['CondPagoID'],
                    "CondPagoDes":row['CondPagoDes'],"Moneda":row['Moneda']}
            resultado.append(fila)

        return resultado
    except Exception as err:
        return f"Error : {err}."