from conexiones.conexion_sap_solo import conexion_SAP_Tecno
from generales.generales import CiaSAP

def ListaCondicionesPagoSAP(pCia:str):
    try:        
        sCia = CiaSAP(pCia)
        
        sqlCP = f"SELECT \"GroupNum\" AS CP_Codigo,\"PymntGroup\" AS CP_Nombre FROM {sCia}.octg"
        

        conn = conexion_SAP_Tecno()

        cur_cp = conn.cursor()

        cur_cp.execute(sqlCP)

        listado = []

        for fcp in cur_cp:
            fila = {'cp_codigo':fcp['CP_Codigo'], 'cp_nombre':fcp['CP_Nombre']}
            listado.append(fila)
            
        return listado
    except Exception as err:
        return f"Error : {err}."