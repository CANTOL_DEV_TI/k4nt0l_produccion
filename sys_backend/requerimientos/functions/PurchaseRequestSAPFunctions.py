from requerimientos.functions.loginSLDFunction import logintoken
from os import getenv
import requests
import json
import urllib3
from datetime import date
from typing import List
from generales.generales import CiaSAP
from generales.configuracion import GetSeries
from generales.notificaciones import enviarnotificacionaprobacion
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from conexiones.conexion_sap_solo import conexion_SAP_Tecno

def ActualizarMigracion(pCia:str, pRQ:str, pNroSAPDE : str, pNroSAPDN : str ,pUPro : str,pUApr : str):
    try:
        conn = ConectaSQL_Produccion()

        cur_act = conn.cursor()

        sqlAM = f"update logi_requerimiento_cabecera set estado='M', SAP_DocEntry='{pNroSAPDE}', SAP_DocNum='{pNroSAPDN}' , fechamigracion=getdate() where cia='{pCia}' and codigo_rq={pRQ}"

        cur_act.execute(sqlAM)

        conn.commit()

        enviarnotificacionaprobacion(pCia, pUPro, pUApr, pRQ,pNroSAPDN)

        return f"Se actualizo la migracion del RQ {pRQ}.\n"
    
    except Exception as err:
        return f"Error : {err}."
    
def extraeTC(pCia):
    sfecha = date.today().strftime('%Y%m%d')

    sqlT = f"SELECT \"Rate\" as TC FROM {pCia}.ORTT WHERE \"RateDate\" = '{sfecha}'"
    print(sqlT)
    datos = {}

    conn = conexion_SAP_Tecno()

    cur_data = conn.cursor()

    cur_data.execute(sqlT)

    for fila in cur_data:        
        datos = {"TC" : fila['TC']}
    
    conn.close()
        
    return datos

def GenerarTrama(pTrama):
    try:
        sfecha = date.today().strftime('%Y%m%d %H:%M:%S')
        Requerimiento = pTrama
        detalleTrama = []
        #print(Requerimiento)
        print(Requerimiento)

        SeriePR = GetSeries(Requerimiento['cia'])
        sCia = CiaSAP(Requerimiento['cia']) 
        dataTC = extraeTC(sCia)
        
        TC = 0

        if(Requerimiento['moneda'] == 'S/'):
            TC = 1
        else:
            TC = float(dataTC["TC"])
             
        for det in pTrama['detalle']:
            if(Requerimiento['proveedor_id']=="" or Requerimiento['proveedor_id']==None):
                lineas ={
                        "LineNum" : det['linea'], 
                        "ItemCode" : det['itemcode'], 
                        "ItemDescription" :  det['itemname'],  
                        "Quantity" : det['cantidad_requerida'], 
                        "Currency" : Requerimiento['moneda'],
                        "DocRate" : TC,
                        "Price" : det['precio_sin_igv'], 
                        "LineTotal" : 0,
                        "WareHouseCode" : "01",
                        "MeasureUnit" : det['unidad_medida_id'],
                        "AccountCode" : det['cuenta_linea'],
                        "CostingCode" : Requerimiento['centro_costo_id'][0:5] ,
                        "CostingCode2":Requerimiento['centro_costo_id'], 
                        "RequiredDate" : str(det['fecha_necesaria']),
                        "U_MSS_CAPRE" : det['catpre_codigo'],
                        "U_MSS_CADES" : det['catpre_nombre'],
                        "U_MSSL_ICBPER": "N",
                        "U_MSS_CMN" : det['comentario_linea'],
                        "ProjectCode" : det['proyecto']
                        }
            else:
                lineas ={
                        "LineNum" : det['linea'], 
                        "ItemCode" : det['itemcode'], 
                        "ItemDescription" :  det['itemname'],  
                        "Quantity" : det['cantidad_requerida'], 
                        "Currency" : Requerimiento['moneda'],
                        "DocRate" : TC,
                        "Price" : det['precio_sin_igv'],
                        "LineTotal" : 0, 
                        "WareHouseCode" : "01",
                        "MeasureUnit" : det['unidad_medida_id'],
                        "AccountCode" : det['cuenta_linea'],
                        "CostingCode" : Requerimiento['centro_costo_id'][0:5] ,
                        "CostingCode2":Requerimiento['centro_costo_id'], 
                        "RequiredDate" : str(det['fecha_necesaria']),
                        "U_MSS_CAPRE" : det['catpre_codigo'],
                        "U_MSS_CADES" : det['catpre_nombre'],
                        "U_MSSL_ICBPER": "N",
                        "U_MSS_CMN" : det['comentario_linea'],
                        "ProjectCode" : det['proyecto'], 
                        "LineVendor" : Requerimiento['proveedor_id'] 
                        }
            
            detalleTrama.append(lineas)

        Trama = {
            "ReqType" : 171,
            "DocDate" : str(Requerimiento['fecha_pedido']),
            "DocDueDate" : str(Requerimiento['fecha_vencimiento']),
            "DocCurrency" : Requerimiento['moneda'],
            "DocRate" : TC,
            "Series" : SeriePR['serie_solicitud_compra'],
            "Comments" : Requerimiento['comentario'],
            "DocObjectCode": "oPurchaseRequest",   
            "RequriedDate" : str(Requerimiento['fecha_necesaria']),
            "DocumentsOwner": Requerimiento['empleado_sap'],
            "Requester" : Requerimiento['empleado_sap'],
            #"RequesterName" : Requerimiento['usuario_nombre'],
            "DocTotalSys" : 0,
            "DocTotalFC" : 0,
            "U_MSSL_EXP": "N",
            "U_MSSL_RCI": "N",    
            "U_MSSL_PRT": "N",    
            "U_MSSM_CRM": "N",    
            "U_MSS_ESTADO": "CN",    
            "U_VLE_RENOV": "N",    
            "U_VLE_TREN": "N",    
            "U_MSS_ORPT": "R",    
            "U_SCR_TOS": "0",    
            "U_SCR_MGD": "N",    
            "U_MSSC_COM": "P",
            "U_MSSC_CRE": "P",    
            "U_MSS_ESRP": "-",    
            "U_MSS_OKBON": "N",    
            "U_MSSF_BEST": "0",    
            "U_MSSF_BVAL": "N",    
            "U_MSSM_TRM": "01",
            "U_MSSM_MOL": "N",
            "U_MSSM_RAN": "03",    
            "U_MSSM_PFL": "N",    
            "U_MSS_APAN": "N",    
            "U_MSS_PROVI": "N",
            "U_MSSL_RVI": "A3",
            "U_MSSL_RCE": "A11",    
            "U_SCR_DTS": "00",    
            "U_MSS_DOPC": "N",
            "U_SWC_COMENTARIO" : Requerimiento['comentario'],
            "U_SWC_FECHA_MIGRACION" : sfecha,            
            "DocumentLines" : detalleTrama
        }

        return Trama
    except Exception as err:
        return f"Error : f{err}."

def GenerarSolicitudCompraSAP(pCia : str, pRQ : str, pTrama : dict, pUApr:str):
    try:
        print("genera solicitud de compra...")

        usuario_prop = pTrama['usuario']
        usuario_apro = pUApr
        resultado = ""

        pSociedad = ""

        pSociedad = CiaSAP(pCia)

        urllib3.disable_warnings()

        token = logintoken(pSociedad)

        session = token['SessionId']
        
        jTrama = GenerarTrama(pTrama)
        #print(jTrama)
        payload = json.dumps(jTrama)
        print(payload)
        header =    {
                        'Content-Type': 'application/json',
                        'Cookie': f'B1SESSION={session}; ROUTERID=.node3'
                    }
        
        Servidor = getenv("SERVIDOR_SLD")
        
        url = f"{Servidor}/b1s/v1/PurchaseRequests"

        response = requests.request("POST", url, headers=header, data=payload,verify=False)

        resultado = json.loads(response.text)

        #print(resultado)

        if("error" in resultado):
            mensaje = resultado['error']['message']['value']

            mensajeT = mensaje
        else:
            mensaje = ActualizarMigracion(pCia, pRQ, resultado['DocEntry'], resultado['DocNum'], usuario_prop, usuario_apro)

            mensajeT = mensaje + f"Se genero el pedido de compra nro : {resultado['DocNum']}.\n"

        return mensajeT
            
    except Exception as err:
        print(err)
        return f"Error : {err}."
    