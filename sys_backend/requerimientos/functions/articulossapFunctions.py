from conexiones.conexion_sap_solo import conexion_SAP_Tecno
from generales.generales import CiaSAP


def ListadoItemSAP(pCia:str,pItem:str):
    try:
        resultado = []

        sCia = CiaSAP(pCia)

        sqlLI = "SELECT top 100 \"ItemCode\" as ItemCode,\"ItemName\" as ItemName, U_MSSL_CUM as Und,\"LastPurPrc\" AS UltimoPrecio"
        sqlLI += f" FROM {sCia}.OITM WHERE \"PrchseItem\" = 'Y' AND \"validFor\" = 'Y' AND ((\"ItemCode\" like '%{pItem}%') OR (\"ItemName\" like '%{pItem}%'))"
        
        conn = conexion_SAP_Tecno()

        cur_items = conn.cursor()

        cur_items.execute(sqlLI)

        for row in cur_items:
            fila = {"ItemCode":row['ItemCode'],"ItemName":row['ItemName'], "Und":row['Und'], "UltimoPrecio" :row['UltimoPrecio']}
            resultado.append(fila)

        return resultado
    except Exception as err:
        return f"Error : {err}."
    
def CuentasxItemSAP(pCia:str, pItem:str):
    try:
        resultado = []

        sCia = CiaSAP(pCia)

        sqlLI = f"SELECT distinct cuenta,ctanombre FROM {sCia}.SWC_LOGI_SOLICITUDES_CUENTAS WHERE itemcode='{pItem}'"
        
        print(sqlLI)
        conn = conexion_SAP_Tecno()

        cur_items = conn.cursor()

        cur_items.execute(sqlLI)

        for row in cur_items:
            fila = {"Cuenta":row['cuenta'],"CuentaNombre":row['ctanombre']}
            resultado.append(fila)

        return resultado
    except Exception as err:
        return f"Error : {err}."

def CategoriaxCuenta(pCia:str, pCta:str):
    try:
        resultado = []

        sCia = CiaSAP(pCia)

        sqlLI = f"SELECT \"Code\" AS Cate_Codigo,\"Name\" AS Cate_Desc FROM {sCia}.\"@MSS_CATPRE\" mc WHERE U_MSS_CUCON = '{pCta}'"
        
        conn = conexion_SAP_Tecno()

        cur_items = conn.cursor()

        cur_items.execute(sqlLI)

        for row in cur_items:
            fila = {"Cate_Codigo":row['Cate_Codigo'],"Cate_Desc":row['Cate_Desc']}
            resultado.append(fila)

        return resultado
    except Exception as err:
        return f"Error : {err}."