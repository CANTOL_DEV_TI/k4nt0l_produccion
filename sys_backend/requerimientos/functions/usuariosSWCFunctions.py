from conexiones.conexion_sql_solo import ConectaSQL_Seguridad

def ListaUsuarioxCia(pCia:str,pFiltro:str):
    try:
        resultado = []
        conn = ConectaSQL_Seguridad()

        cur_lista = conn.cursor(as_dict = True)

        sql = "select te.Empresa_Codigo,tu.Usuario_Codigo,tu.Usuario_Nombre from tUsuarios tu" 
        sql += " inner join tAccesoEmpresa tae on tu.Usuario_ID=tae.Usuario_ID"
        sql += " inner join tEmpresa te on tae.Empresa_ID = te.Empresa_ID "
        sql += f" where te.Empresa_Codigo = '{pCia}' and tu.Usuario_estado='1' and tu.Usuario_Codigo like '%{pFiltro.strip()}%'"
        print(sql)

        cur_lista.execute(sql)

        resultado = cur_lista.fetchall()

        return resultado
    except Exception as err:
        return f"Error: {err}."