from fastapi import APIRouter, Depends, status, UploadFile, File, Request  
from fastapi.responses import FileResponse  
from requerimientos.schemas.requerimientosfiltroSchemas import filtrorq
from requerimientos.schemas.requerimientosSchemas import requerimiento_schema
from requerimientos.schemas.centrocostoSchemas import centrocostousuario_schema
from requerimientos.schemas.requerimientosgestionSchemas import requerimientosgestion_schema
from requerimientos.functions.requerimientosFunctions import Listado,NuevaRQ,ActualizarRQ,EliminarRQ,VerRequerimiento,ExtraerEmail,AprobarRQ,RechazarRQ,BandejaAprobaciones,Totales,TotalesAprobadores
from requerimientos.functions.centrocostoFunctions import CentroCostoSAP,CentroCostoxUsuario,AgregarCCUsuario,QuitarCCUsuario,ListadoCCxUsuarioxCia,ActualizarCCUsuario,CentroCostoSAPBusca,AplicarTodosCC
from requerimientos.functions.articulossapFunctions import ListadoItemSAP,CuentasxItemSAP,CategoriaxCuenta
from requerimientos.functions.proveedoressapFunctions import ListadoProveedoresSAP
from requerimientos.functions.empleadosapFunctions import ListaEmpleadoSAPRRHH,ListaEmpleadoSAPRRHH_All
from requerimientos.functions.condicionespagosapFunctions import ListaCondicionesPagoSAP
from requerimientos.functions.usuariosSWCFunctions import ListaUsuarioxCia
from requerimientos.functions.proyectossapFunctions import ListaProyectos

ruta = APIRouter(  
    prefix="/requerimientos",tags=["Requerimientos"]
) 

@ruta.post("/listado/", status_code=status.HTTP_200_OK)
def listaRQs(pData:filtrorq):
    return Listado(pData)

@ruta.get("/centrocostosap/{pCia}",status_code=status.HTTP_200_OK)
def listaCCSAP(pCia:str):
    return CentroCostoSAP(pCia)

@ruta.get("/centrocostosaplista/{pCia}/{pFiltro}",status_code=status.HTTP_200_OK)
def listaCCSAP(pCia:str,pFiltro:str):
    return CentroCostoSAPBusca(pCia,pFiltro)

@ruta.get("/centrocostoxusuario/{pCia}/{pUsuario}",status_code=status.HTTP_200_OK)
def listaCCxUsuario(pCia:str,pUsuario:str):
    return CentroCostoxUsuario(pCia,pUsuario)

@ruta.post("/nuevo/",status_code=status.HTTP_201_CREATED)
def agregarReq(pData:requerimiento_schema):    
    return NuevaRQ(pData)

@ruta.put('/actualizar/',status_code=status.HTTP_202_ACCEPTED)
def actualizarReq(pData:requerimiento_schema):
    return ActualizarRQ(pData)

@ruta.delete('/eliminar/',status_code=status.HTTP_200_OK)
def eliminarReq(pData:requerimiento_schema):    
    return EliminarRQ(pData)

@ruta.post("/aprobar/",status_code=status.HTTP_202_ACCEPTED)
def aprobarrq(pData:requerimientosgestion_schema):
    return AprobarRQ(pData)

@ruta.delete("/rechazar/",status_code=status.HTTP_202_ACCEPTED)
def rechazarrq(pData:requerimientosgestion_schema):
    return RechazarRQ(pData)

@ruta.post("/centrocostorq/agregar/",status_code=status.HTTP_201_CREATED)
def agregarccusuario(pCCU:centrocostousuario_schema):
    return AgregarCCUsuario(pCCU)

@ruta.post("/centrocostorq/guardartodos/",status_code=status.HTTP_201_CREATED)
def guardartodoccusuario(pCCU:centrocostousuario_schema):
    return AplicarTodosCC(pCCU)

@ruta.patch("/centrocostorq/actualizar/",status_code=status.HTTP_202_ACCEPTED)
def actualizarccusuario(pCCU:centrocostousuario_schema):
    return ActualizarCCUsuario(pCCU)

@ruta.delete("/centrocostorq/remover/", status_code=status.HTTP_202_ACCEPTED)
def removerccusuario(pCCU:centrocostousuario_schema):
    return QuitarCCUsuario(pCCU)

@ruta.get("/itemsap/{pCia}/{pNombre}/", status_code=status.HTTP_200_OK)
def listaitemssap(pCia:str,pNombre:str):
    return ListadoItemSAP(pCia,pNombre)

@ruta.get("/proveedoressap/{pCia}/{pNombre}/",status_code=status.HTTP_200_OK)
def listaproveedoressap(pCia:str,pNombre:str):
    return ListadoProveedoresSAP(pCia,pNombre)

@ruta.get("/{pCia}/{pNro}",status_code=status.HTTP_202_ACCEPTED)
def verRQ(pCia:str,pNro:str):
    return VerRequerimiento(pCia,pNro)

@ruta.get("/email/{pUsuario}/",status_code=status.HTTP_200_OK)
def extraerCorreo(pUsuario:str):    
    return ExtraerEmail(pUsuario)

@ruta.get('/listasapempleados/{pCia}/{pFiltro}/',status_code=status.HTTP_200_OK)
def listaemp(pCia:str,pFiltro:str):
    return ListaEmpleadoSAPRRHH(pCia,pFiltro)

@ruta.get('/listasapempleadosall/{pCia}/',status_code=status.HTTP_200_OK)
def listaemp_all(pCia:str):
    return ListaEmpleadoSAPRRHH_All(pCia)

@ruta.post('/bandejaaprobacion/',status_code=status.HTTP_202_ACCEPTED)
def listaAprobaciones(pData:filtrorq):
    return BandejaAprobaciones(pData)

@ruta.get('/proveedorcondpago/{pCia}/',status_code=status.HTTP_200_OK)
def listaCPs(pCia:str):
    return ListaCondicionesPagoSAP(pCia)

@ruta.post("/totales/",status_code=status.HTTP_200_OK)
def listaTotales(pinfo:filtrorq):
    return Totales(pinfo)

@ruta.post("/listaccxusuarioxcia/",status_code=status.HTTP_200_OK)
def listaCC(pInfo:filtrorq):
    return ListadoCCxUsuarioxCia(pInfo)

@ruta.post("/totalesaprobadores/",status_code=status.HTTP_200_OK)
def listaTotalesA(pinfo:filtrorq):
    return TotalesAprobadores(pinfo)

@ruta.get('/listacuentasxitems/{pCia}/{pItem}', status_code=status.HTTP_200_OK)
def listaCtaItems(pCia:str, pItem:str):
    return CuentasxItemSAP(pCia,pItem)

@ruta.get('/listacatexcuenta/{pCia}/{pCta}', status_code=status.HTTP_200_OK)
def listaCatexCuenta(pCia:str, pCta:str):
    return CategoriaxCuenta(pCia,pCta)

@ruta.get('/listausuariosxcia/{pCia}/{pFiltro}', status_code=status.HTTP_200_OK)
def listauxc(pCia:str, pFiltro:str):
    return ListaUsuarioxCia(pCia,pFiltro)

@ruta.get('/listaproyectos/{pCia}/{pFiltro}', status_code=status.HTTP_200_OK)
def listaproy(pCia:str, pFiltro:str):
    return ListaProyectos(pCia,pFiltro)