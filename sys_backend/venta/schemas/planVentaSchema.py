from typing import List, Optional
from pydantic import BaseModel


class PlanVentaDetSchema(BaseModel):
    id_plan: Optional[int]
    ejercicio: Optional[int]
    mes: Optional[int]
    cod_articulo: Optional[str]
    articulo: Optional[str]
    cantidad: Optional[float]
    cod_usuario_apr: Optional[str]
    sw_estado: Optional[str]
    cod_usuario: Optional[str]

class PlanVentaCabSchema(BaseModel):
    id_plan: Optional[int]
    ejercicio: Optional[int]
    id_version: Optional[int]
    sw_aprobado: Optional[str]
    gloda: Optional[str]
    cod_usuario_apr: Optional[str]
    sw_estado: Optional[str]
    cod_usuario: Optional[str]
    detalle : List[PlanVentaDetSchema]


