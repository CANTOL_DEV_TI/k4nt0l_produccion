from fastapi import APIRouter, Depends, status
from venta.schemas.planVentaSchema import PlanVentaCabSchema
from venta.functions import PlanVenta
from conexiones.conexion_sql import get_db
from sqlalchemy.orm import Session
from typing import Union

ruta = APIRouter(
    prefix="/plan_venta"
)

"""
@ruta.get('/', status_code=status.HTTP_200_OK)
def articuloPlano_all(db: Session = Depends(get_db)):
    data = ArticuloPlano.articuloPlano_all(db)
    return data


@ruta.get('/historial/{cod_articulo}', status_code=status.HTTP_200_OK)
def articuloPlano_historial(cod_articulo: str, db: Session = Depends(get_db)):
    data = ArticuloPlano.articuloPlano_cod_articulo(cod_articulo, db)
    return data

@ruta.get("/plano/{nombre_plano}")
def get_file(nombre_plano: str):
    return FileResponse("../data_planos/" + nombre_plano)

"""
@ruta.get("/ejercicio")
def get_plan_venta_ejercicio():
    data = PlanVenta.planVenta_periodo()
    return data

@ruta.get("/version")
def get_plan_venta_version(ejercicio: Union[int, None] = 0, db: Session = Depends(get_db)):
    data = PlanVenta.planVenta_version(ejercicio, db)
    return data

@ruta.get("/")
def get_articulo_plano_sap(id_plan: Union[str, None] = "0", db: Session = Depends(get_db)):
    data = PlanVenta.planVenta_lista(id_plan, db)
    return data

@ruta.post('/', status_code=status.HTTP_201_CREATED)
def insertar_articulo_plano(dt_plan_venta: PlanVentaCabSchema, db: Session = Depends(get_db)):
    PlanVenta.planVenta_insert(dt_plan_venta, db)
    return {"respuesta": "Plano creado satisfactoriamente!!"}

@ruta.delete('/{id_plan}', status_code=status.HTTP_200_OK)
def eliminar_articulo_plano(id_plan:int,db:Session = Depends(get_db)):
    res = PlanVenta.planVenta_delete(id_plan, db)
    return res