from fastapi import APIRouter
from . import plan_venta

ruta = APIRouter(
    prefix="/venta",
    tags=["Venta"]
)


ruta.include_router(plan_venta.ruta)

