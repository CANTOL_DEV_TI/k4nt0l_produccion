from fastapi import HTTPException, status
from sqlalchemy import select, and_, text
from sqlalchemy.orm import Session
from conexiones.conexion_mmsql import conexion_mssql
from datetime import datetime


def planVenta_nuevo(ejercicio):
    sql = f"SELECT * FROM VENTA_PLAN_VENTA_NUEVO (%s)"
    me_conexion = conexion_mssql()
    resultados = me_conexion.consultas_sgc(sql, (ejercicio,))
    return resultados

def planVenta_periodo():
    sql = f"SELECT * FROM VENTA_PLAN_VENTA_EJERCICIO"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta

def planVenta_version(ejercicio, db: Session):
    sql = f"SELECT * FROM VENTA_PLAN_VENTA_VERSION (%s)"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, (ejercicio,))
    return resultadosConsulta

def planVenta_lista(id_plan, db: Session):
    sql = f"SELECT * FROM VENTA_PLAN_VENTA (%s)"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, (id_plan,))
    return resultadosConsulta

def planVenta_insert(dt_plan_venta, db: Session):

    # obtener id de plan de ventas
    c_id_plan = "0"
    c_id_version = "0" 
    c_json_res = planVenta_nuevo(dt_plan_venta.ejercicio)
    for row in c_json_res:
        c_id_plan = str(row['nuevo_id'])
        c_id_version = str(row['nuevo_ver_id'])

    if c_id_plan == 0:
        return HTTPException(status_code=401, detail="Error en generar ID")

    # CABECERA
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(c_id_plan)
    lista_cab.append(dt_plan_venta.ejercicio)
    lista_cab.append(c_id_version)
    lista_cab.append(dt_plan_venta.cod_usuario)

    # ejecutar cabecera
    v_sql_cab = f"INSERT INTO PLAN_VENTA_CAB (id_plan,ejercicio,id_version,cod_usuario) VALUES (%s, %s, %s, %s)"
    me_conexion = conexion_mssql()
    # resultados = me_conexion.ejecutar_funciones(v_sql, tuple(lista_cab))

    # DETALLE
    lista_det = []
    # v_sql = """INSERT INTO PLAN_VENTA_DET (id_plan,fecha_doc,cod_articulo,cantidad) VALUES (%s, %s, %s, %s)"""
    v_sql_det = f"INSERT INTO PLAN_VENTA_DET (id_plan,fecha_doc,cod_articulo,articulo,cantidad) VALUES (%s, %s, %s, %s, %s)"
    for row in dt_plan_venta.detalle:
        rowD = row.dict()
        lista_det.append((
            c_id_plan,
            str(str(dt_plan_venta.ejercicio) + '-' +
                str(rowD['mes']).zfill(2) + '-01'),
            rowD['cod_articulo'],
            rowD['articulo'],
            rowD['cantidad']
        ))
    # resultados = me_conexion.ejecutar_funciones_multi(v_sql, lista_det)}
    if (len(lista_det) > 0):
        resultados = me_conexion.ejecutar_funciones_CabDet(
            v_sql_cab, tuple(lista_cab), v_sql_det, lista_det)
    else:
        resultados = HTTPException(status_code=401, detail="No hay datos")

    return resultados

def planVenta_delete(id_plan, db: Session):
    # ejecutar procedimiento
    me_conexion = conexion_mssql()
    resultados = me_conexion.ejecutar_store_crud(
        "VENTA_PLAN_VENTA_DELETE", (id_plan,))
    return resultados