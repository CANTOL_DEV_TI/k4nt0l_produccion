from conexiones.conexion_sql_solo import ConectaSQL_GDH
from gdh.schemas.asientosSchema import asiento_Schema,asientoparamentros_Schema
from gdh.functions.loginSLDFunction import logintoken
from os import getenv
import requests
import json
import urllib3
from generales.generales import CiaSAP


def GrabarAsiento(pCia : str, pAsiento : asiento_Schema):
    
    try:    
        resultado = ""

        urllib3.disable_warnings()

        token = logintoken(pCia)

        session = token['SessionId']
        
        payload = json.dumps(pAsiento)
        
        header = {
                    'Content-Type': 'application/json',
                    'Cookie': f'B1SESSION={session}; ROUTEID=.node3'
                 }
        
        Servidor = getenv("SERVIDOR_SLD")
        #url = f"{Servidor}/b1s/v1/JournalEntries"
        url = f"{Servidor}/b1s/v1/JournalVouchersService_Add"

        response = requests.request("POST", url, headers=header, data=payload,verify=False)
        
        if(response.status_code!=204):
            print(response.json())
            resultado = response.json()            

        mensaje = ""
        
        if("error" in resultado):
            mensaje = resultado['error']['message']['value']
        else:
            print(resultado)
            mensaje = f"Se genero el asiento en preliminar."

        return mensaje
        
    except Exception as err:
        print(err)
        return f"Error en la operación : {err}."

def ExtraerAsientosADRYAN(pPara : asientoparamentros_Schema):
    try:      
        final = {"JournalVoucher": {} }

        asientopre = {"JournalEntry" : {} }
                        
        asiento = {"ReferenceDate" : pPara.Fecha , "Memo" : pPara.Comentario}
                        
        detalle_final = []
        
        sql_da = f"exec SP_genera_archivo_contable_det N'{pPara.CiaADRYAN}',N'{pPara.TipoPlanilla}',N'E',N'0001',N'{pPara.Periodo + pPara.Ejercicio}',N'{pPara.Periodo}',N'{pPara.Ejercicio}'"
        #print (sql_da)
        conn = ConectaSQL_GDH()

        detalle_cur = conn.cursor(as_dict = True)

        detalle_cur.execute(sql_da)

        for row in detalle_cur:            
            if(row["ShortName"]!="ShortName"):
                detalle = {"Line_ID" : row["Line_ID"].strip() , "ShortName" : row["ShortName"].strip(), "AccountCode" : row["AccountCode"].strip() , "Debit" : row["Debit"].strip() ,
                           "Credit" : row["Credit"].strip() ,"CostingCode" : row["CostingCode"].strip() , "CostingCode2" : row["CostingCode2"].strip()  }
                detalle_final.append(detalle)
        
        asiento["JournalEntryLines"] = detalle_final

        conn.close()
        
        asientopre = {"JournalEntry" : asiento}
        
        final = {"JournalVoucher" : asientopre}

        #print(final)

        cia = ""

        cia = CiaSAP(pPara.CiaADRYAN)

        Resultado = GrabarAsiento(cia,final)

        return Resultado
    except Exception as Err:
        print(Err)
        return Err