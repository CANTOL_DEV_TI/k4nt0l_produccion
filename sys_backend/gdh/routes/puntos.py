
from fastapi import APIRouter
from gdh.routes.Puntos import campania, productos, puntos, login, terminos


# Llamar a la función connection() para obtener la conexión
#connection = conn()


# Inicializar la aplicación FastAPI
# app = FastAPI()

ruta = APIRouter(
    prefix='/puntos',
)

ruta.include_router(campania.ruta)
ruta.include_router(productos.ruta)
ruta.include_router(puntos.ruta)
ruta.include_router(login.ruta)
ruta.include_router(terminos.ruta)
