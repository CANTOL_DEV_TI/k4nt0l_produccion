from fastapi import HTTPException, APIRouter
from fastapi.middleware.cors import CORSMiddleware
#from conexion import conn
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
import logging
from pydantic import BaseModel


logger = logging.getLogger()

ruta = APIRouter(
    prefix='/campania',
    tags=["puntos"]
)



def get_connection():
    return ConectaSQL_Produccion()



class dataCampania(BaseModel):
    codigoCampania:str
    tipo:str
    categoria:str
    nombre:str
    puntos:str

@ruta.post("/insertar")
def agregarCampania(dataCampania:dataCampania):
    print("muestrame data campaña", dataCampania)
    connection = get_connection()
    try:
        cursor = connection.cursor()
        codigo =  dataCampania.codigoCampania
        tipo = dataCampania.tipo
        categoria = dataCampania.categoria
        nombre = dataCampania.nombre
        estado = 'default'
        puntos = dataCampania.puntos
        sql_insert = "INSERT INTO puntos_campania(codigocampania,tipo,categoria,nombre,estado,puntos)VALUES('{}','{}','{}','{}',{},{})".format(codigo,tipo,categoria,nombre,estado,puntos)
        cursor.execute(sql_insert)
        connection.commit()

        connection.close()
        return "Se insertó la campaña exitosamente."
    except Exception as e:
        logger.error(f"Error al agregar campaña: {e}")
        raise HTTPException(status_code=500, detail="Ocurrió un error al insertar las camapañas")
    finally:
        #Cerrar la conexión a la base de datos
        cursor.close()
            

@ruta.get("/listarCampania")
def listarCampania(): 
    connection = get_connection()
    try:
        print("entras aqui..")
        with connection.cursor() as cursor:
            sqlNew = "select * from puntos_campania order by codigocampania asc;"
            print("pintame consulta sql:",sqlNew)
            cursor.execute(sqlNew)
            rs = cursor.fetchall()
            lista=[]
            listaDic = {}
            for row in rs:
                listaDic = {"id":row[0], "codigoCampania":row[1], "tipo":row[2], "categoria": row[3], "nombre": row[4], "estado":row[5], "puntos":row[6]}
                lista.append(listaDic)
        return lista
    except Exception as e:
        logger.error(f"Error al listar campaña: {e}")
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener las camapañas")
    finally:
        cursor.close()
            
            

    
@ruta.post("/actualizarcampania")
def actualizarCampania(dataCampania:dict):
    connection = get_connection()
    try:
        cursor = connection.cursor()
        codigo =  dataCampania['codigo']
        tipo = dataCampania['tipo']
        categoria = dataCampania['categoria']
        nombre = dataCampania['nombre']
        estado = dataCampania['estado']
        puntos = dataCampania['puntos']

        sqlUpdate = f"UPDATE puntos_campania SET tipo = '{tipo}', categoria = '{categoria}',nombre='{nombre}',estado={estado},puntos={puntos} WHERE codigoCampania = '{codigo}'"
        cursor.execute(sqlUpdate)
        connection.commit()
        #connection.close()
        return "se actualizó correctamente."
    except Exception as e:
        logger.error(f"Error al actualizar campaña: {e}")
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener las camapañas")
    finally:
        # Cerrar la conexión a la base de datos
        cursor.close()
            

@ruta.post("/buscar")
def buscarCampania(campania:dict):
    connection = get_connection()
    try:
        cursor = connection.cursor()
        datos = campania['datos']

        #sql = '''SELECT * FROM campania where (codigocampania ilike '%{}%' or nombre ilike '%{}%')  order by codigocampania asc;'''.format(datos,datos)
        sql = '''select * from puntos_campania where (upper(codigocampania) like upper('%{}%') or upper(nombre) like upper('%{}%')) order by codigocampania asc;'''.format(datos,datos)

        print("pintame consulta:",sql)
        cursor.execute(sql)
        rs = cursor.fetchall()

        lista = []

        for row in rs:
            listaDic = {"id":row[0], "codigoCampania":row[1], "tipo":row[2], "categoria": row[3], "nombre": row[4], "estado":row[5], "puntos":row[6]}
            lista.append(listaDic)
    
        return lista
    except Exception as e:
            # Manejar errores y devolver un mensaje de error adecuado
            logger.error(f"Error al buscar campaña: {e}")
            raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los productos")
    finally:
        # Cerrar la conexión a la base de datos
        connection.close()

@ruta.post("/cambiarestado")
def cambiarEstado(campania:dict):
    connection = get_connection()
    try:
        id = campania['id']
        cursor = connection.cursor()
        sqlUpdate = f"update puntos_campania set estado = 0 where id = {id};"
        cursor.execute(sqlUpdate)
        connection.commit()
        return "se desactivó la campania"
    except KeyError as e:
        return f"Error: Se esperaba el campo {str(e)}"
    except Exception as e:
        logger.error(f"Error al cambiar estado: {e}")
        return f"Error: {str(e)}"
    finally:
        connection.close()

@ruta.get("/obtenercodigo")
def obtenerCodigo():
    connection = get_connection()
    try:
        with connection.cursor() as cursor:
            sql = f'SELECT MAX(codigocampania) FROM puntos_campania;'
            cursor.execute(sql)
            rs = cursor.fetchone()
            last_code = rs[0]
        if last_code:
            last_number = int(last_code[4:])
            new_code = f'CAMP{last_number + 1:03}'
        else:
            new_code = 'CAMP001'
        
        return {"codigoCampaña": new_code}
    except Exception as e:
        logger.error(f"Error al obtener codigo: {e}")
        raise HTTPException(status_code=500, detail="Error al mostrar campaña")
    finally:
        connection.close()


@ruta.get("/obtenercampanias")
def obtenerCampañas():
    connection = get_connection()
    try:
        cursor = connection.cursor()
        sql = "select * from puntos_campania where estado = 1 order by id desc;"

        print("pintame consulta:",sql)
        cursor.execute(sql)
        rs = cursor.fetchall()

        lista = []

        for row in rs:
            listaDic = {"id":row[0], "codigocampania":row[1], "nombre": row[4], "puntos": row[6]}
            lista.append(listaDic)
    
        return lista
    except Exception as e:
        logger.error(f"Error al obtener campaña: {e}")
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener las camapañas")
    finally:
        connection.close()
    









