import jwt
from fastapi import APIRouter, HTTPException
from fastapi.middleware.cors import CORSMiddleware
import bcrypt
#from conexion import conn
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
import logging
import json

logger = logging.getLogger()

ruta = APIRouter(
    prefix = '/usuario',
    tags=["puntos"]
)

def get_connection():
    return ConectaSQL_Produccion()





@ruta.post("/login")
def login(user_login: dict):
    connection = get_connection()
    try:
        #Generar token - inicio
        SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
        ALGORITHM = "HS256"
        #Generar token - fin
        cursor = connection.cursor()
        #sqlUsuario = f"SELECT * FROM usuario WHERE dni = '{user_login['dni']}' AND idempresa = '{user_login['idEmpresa']}'"
        sqlUsuario = f"SELECT * FROM puntos_usuario WHERE dni = '{user_login['dni']}' and estado = 1;"
        cursor.execute(sqlUsuario)
        user = cursor.fetchone()
        userPerfil = json.loads(user[6])
        listaPerfil = []
   
        if not user:
            raise HTTPException(status_code=401, detail="usuario no existe")
        
        if user[6]:
            for row in userPerfil:
                listaPerfil.append(row['idperfil'])
        else:
            raise HTTPException(status_code=401, detail="usuario sin perfil asignado")
        
        if 1 in listaPerfil:
            print("entras como perfil:", listaPerfil)
            clave_ingresada = user_login['clave']
            clave_encriptada = user[5] # Clave de la BD

            if verificar_contraseña(clave_ingresada, clave_encriptada):
                # Aquí podrías devolver un token JWT u otro identificador de sesión seguro
                token_data = {"sub":user_login['dni']}
                token = jwt.encode(token_data, SECRET_KEY, algorithm=ALGORITHM)
                empresa = user[7]
                return {"token": token, "dni": user_login['dni'], "empresa": empresa }
            else:
                raise HTTPException(status_code=401, detail="Credenciales incorrectas")
        else:
            
            raise HTTPException(status_code=401, detail="Usted no es administrador")
    
    except Exception as e:
        logger.error(f"Error al loguearse: {e}")
        raise e
        #raise HTTPException(status_code=401, detail="Credenciales incorrectas")
    finally:
        # Cerrar la conexión a la base de datos
        connection.close()

@ruta.post("/cambiaContrasena")
def cambiarClave (user_login: dict):
    connection = get_connection()
    try:
        print("datos de usuario:",user_login)
        cursor = connection.cursor()
        sqlUsuario = f"SELECT * FROM puntos_usuario WHERE dni = '{user_login['dni']}' AND idempresa = '{user_login['idempresa']}'"
        print("pintame consulta:",sqlUsuario)
        cursor.execute(sqlUsuario)
        user = cursor.fetchone()


        clave_ingresada = user_login['clave']
        clave_encriptada = user[5] # Clave de la BD
        print("pintame clave encriptada:",clave_encriptada)

        if verificar_contraseña(clave_ingresada,clave_encriptada):
            print("credenciales correctas")
            claveEncriptada = encriptar_contraseña(user_login['claveNueva'])
            clave_bytes_encriptada = str(claveEncriptada)[2:-1]
            sqlupdate = f"UPDATE puntos_usuario SET clave = '{clave_bytes_encriptada}' where dni = '{user_login['dni']}'"
            print("pintame consulta update:",sqlupdate)
            cursor.execute(sqlupdate)
            connection.commit()
            msj = "contraseña actualizada"
            return msj
        else:
            raise HTTPException(status_code=401, detail="Credenciales incorrectas")

    except Exception as e:
        logger.error(f"Error al cambiar contraseña: {e}")
        raise HTTPException(status_code=500, detail="Ocurrió un error al actualizar contraseña")    
    finally:
        # Cerrar la conexión a la base de datos
        cursor.close()
        connection.close()


def encriptar_contraseña (clave):
    hashed_clave = bcrypt.hashpw(clave.encode('utf-8'), bcrypt.gensalt())
    print("clave encriptada:",hashed_clave)
    return hashed_clave

def verificar_contraseña(clave, hashed_clave):
    return bcrypt.checkpw(clave.encode('utf-8'), hashed_clave.encode('utf-8'))
    

