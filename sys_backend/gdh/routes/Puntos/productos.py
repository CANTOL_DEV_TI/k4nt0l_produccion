#from fastapi import FastAPI, HTTPException
from fastapi import HTTPException, File,Form,Request, APIRouter

import psycopg2
import logging
from fastapi.middleware.cors import CORSMiddleware
#from conexion import conn
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
logger = logging.getLogger()
import pymssql
#New comands
from fastapi.responses import JSONResponse
import base64

ruta = APIRouter(
    prefix='/producto',
    tags=["puntos"]
)


def get_connection():
    return ConectaSQL_Produccion()



@ruta.get("/listarProducto")
def listarProductos():
    connection = get_connection()
    try:
        # Establecer la conexión a la base de datos
        with connection.cursor() as cursor:
            # Ejecutar la consulta SQL
            sql = f'select p.id, p.nombre, p.precio, p.cantidadPuntos, s.fechaRegistro, s.cantidad, p.estado, p.imagen, p.descripcion from puntos_producto p inner join puntos_stock s on p.id = s.idProducto order by p.nombre asc;'
            cursor.execute(sql)
            
            # Obtener los resultados de la consulta
            rs = cursor.fetchall()
            
            # Procesar los resultados y construir la lista de productos
            lista = []
            for row in rs:
                imagen = row[7]
                if imagen:
                    imagen_base64 = base64.b64encode(imagen).decode('utf-8')
                else:
                    imagen_base64 = None

                producto = {
                            "id": row[0],
                            "nombre": row[1],
                            "precio": row[2],
                            "cantidadpuntos": row[3],
                            "fecharegistro": row[4],
                            "cantidad": row[5],
                            "estado": row[6],
                            "imagen":imagen_base64,
                            "descripcion":row[8]
                            }
                lista.append(producto)
        
        # Devolver la lista de productos
        return lista
        
    except Exception as e:
        logger.error(f"Error in listarProductos: {e}")
        #connection.rollback()
        # Manejar errores y devolver un mensaje de error adecuado
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los productos")
    finally:
        # Cerrar la conexión a la base de datos
        connection.close()


@ruta.post("/registrarProducto")
async def añadirProducto(request:Request):
    connection = get_connection()
    try:
        form = await request.form()
        print("pintame form", form)

        nombre = form.get('nombre')
        precio= form.get('precio')
        cantidad= form.get('cantidad')
        cantidadpuntos = form.get('cantidadPuntos')
        descripcion = form.get('descripcion')
        file = form.get('imagen')

        
        contents = await file.read()

         # Codificar el contenido a base64
        imagen_base64 = base64.b64encode(contents).decode("utf-8")

        cursor = connection.cursor()

        sql_sp = """
            EXEC sp_puntos_insertarproducto 
                @nombre = %s, 
                @precio = %s, 
                @cantidad = %s, 
                @cantidadpuntos = %s, 
                @imagen = %s, 
                @descripcion = %s
        """
        valores_sp = (nombre, precio, cantidad, cantidadpuntos, pymssql.Binary(contents), descripcion)
        cursor.execute(sql_sp,valores_sp)

       
        connection.commit()
        #cursor.close()
        return "Procedimiento almacenado ejecutado exitosamente."
    except KeyError as e:
        return f"Error: Se esperaba el campo {str(e)}"
    except Exception as e:
        logger.error(f"Error en insertar producto: {e}")
        return f"Error: {str(e)}"
    finally:
        connection.close()
    

@ruta.post("/actualizarproducto")
async def actualizarproducto(request: Request):
    connection = get_connection()
    try:
        form = await request.form()
        print("pintame form", form)

        idProducto = form.get('id')
        nombre = form.get('nombre')
        precio = form.get('precio')
        cantidadPuntos = form.get('cantidadpuntos')
        estado = form.get('estado')
        file = form.get('imagen')
        descripcion = form.get('descripcion')

        cursor = connection.cursor()

        if file != 'null':
            print("entras con imagen?:", file)
            contents = await file.read()
            sqlupdate = """
                UPDATE puntos_producto 
                SET nombre = %s, precio = %s, cantidadpuntos = %s, estado = %s, descripcion = %s, imagen = %s 
                WHERE id = %s
            """
            cursor.execute(sqlupdate, (nombre, precio, cantidadPuntos, estado, descripcion, pymssql.Binary(contents), idProducto))
        else:
            print("entras a actualizar solo datos")
            sqlupdate = """
                UPDATE puntos_producto 
                SET nombre = %s, precio = %s, cantidadpuntos = %s, estado = %s, descripcion = %s
                WHERE id = %s
            """
            cursor.execute(sqlupdate, (nombre, precio, cantidadPuntos, estado, descripcion, idProducto))

        connection.commit()
        return "Se actualizó los datos del producto"

    except KeyError as e:
        return f"Error: Se esperaba el campo {str(e)}"
    except Exception as e:
        logger.error(f"Error al actualizar producto: {e}")
        return f"Error: {str(e)}"
    finally:
        connection.close()

    
@ruta.post("/descontarStock")
def descontarStock(producto:dict):
    connection = get_connection()
    try:
        print("entras a descontar",producto)
        cursor = connection.cursor()
        idproducto = producto['id_producto']
        cantDescontar = producto['cantidad_descontar']
        tipo = producto['tipo']

        sql_sp = """
            EXEC sp_puntos_descontarstock 
                @id_producto = %s, 
                @cantidad_descontar = %s, 
                @tipo = %s
        """

        valores_sp = (idproducto,cantDescontar,tipo)
        cursor.execute(sql_sp,valores_sp)

        connection.commit()
        return "Procedimiento almacenado ejecutado exitosamente."
    except KeyError as e:
        return f"Error: Se esperaba el campo {str(e)}"
    except Exception as e:
        logger.error(f"Error al descontar stock: {e}")
        return f"Error: {str(e)}"
    finally:
        connection.close()

@ruta.post("/agregarStock")
def agregarStock(producto:dict):
    connection = get_connection()
    try:
        
        cursor = connection.cursor()

        idProducto = producto['id_producto']
        cantidadAgregar = producto['cantidad_agregar']
        tipo = producto['tipo']
        print("pintame cantidad agregada",cantidadAgregar)

        sql_sp = """
            EXEC sp_puntos_agregarstock 
                @id_producto = %s, 
                @cantidad_agregar = %s, 
                @tipo = %s
        """
        valores_sp = (idProducto,cantidadAgregar,tipo)
        cursor.execute(sql_sp,valores_sp)


        connection.commit()
        cursor.close()
        return "Procedimiento almacenado ejecutado exitosamente."
    except KeyError as e:
        return f"Error: Se esperaba el campo {str(e)}"
    except Exception as e:
        logger.error(f"Error al agregar stock: {e}")
        return f"Error: {str(e)}"

@ruta.post("/upload")
async def upload_file(request: Request):
    connection = get_connection()
    try:
        form = await request.form()
        descripcion = form.get('descripcion')
        file = form.get('file')
        
        if descripcion is None or file is None:
            print("pintame descripcion:", descripcion)
            return JSONResponse(content={"message": "Error: Campos 'descripcion' y 'file' requeridos"}, status_code=400)
        
        contents = await file.read()
        
        # Codificar el contenido a base64
        imagen_base64 = base64.b64encode(contents).decode("utf-8")

        # Conectar a la base de datos
        cursor = connection.cursor()

        # Insertar la descripción y la imagen codificada en base64 en la base de datos
        query = "INSERT INTO productos (descripcion, ruta) VALUES (%s, %s)"
        cursor.execute(query, (descripcion, imagen_base64))

        # Confirmar la transacción
        connection.commit()
        
        return JSONResponse(content={"message": "Descripción e imagen cargadas exitosamente"})
    except Exception as e:
        return JSONResponse(content={"message": f"Error: {str(e)}"}, status_code=500)
    

    
@ruta.get("/listarimagenes")
async def get_productos():
    connection = get_connection()
    try:
    
        cursor = connection.cursor()
        query = "select id, nombre, cantidadpuntos, imagen from puntos_producto;"
        cursor.execute(query)
        productos = cursor.fetchall()

        # Convertir los datos de imagen a base64
        productos_converted = []
        for producto in productos:
            id, nombre, puntos, imagen = producto
            if imagen:
                imagen_base64 = base64.b64encode(imagen).decode('utf-8')
            else:
                imagen_base64 = None
            #productos_converted.append({"id": id, "descripcion": descripcion, "ruta": imagen_base64})
            productos_converted.append({"id": id, "nombre": nombre, "puntos": puntos, "imagen": imagen_base64})


        return JSONResponse(content={"productos": productos_converted})
    except psycopg2.Error as e:
        raise HTTPException(status_code=500, detail=f"Error al obtener los productos de la base de datos: {str(e)}")
    except Exception as e:
        logger.error(f"Error al listar imagenes: {e}")
        raise HTTPException(status_code=500, detail=f"Error: {str(e)}")
    finally:
        # Cerrar la conexión a la base de datos
        connection.close()

@ruta.post("/buscar")
def buscarProducto(producto:dict):
    connection = get_connection()
    try:
        cursor = connection.cursor()
        nombre = producto['nombre']

        #sql = f"select p.id, p.nombre, p.precio, p.cantidadPuntos, s.fechaRegistro, s.cantidad, p.estado, p.imagen, p.descripcion from Producto p inner join stock_puntos s on p.id = s.idProducto where p.nombre ilike '%{nombre}%';"
        sql = f"select p.id, p.nombre, p.precio, p.cantidadPuntos, s.fechaRegistro, s.cantidad, p.estado, p.imagen, p.descripcion from puntos_producto p inner join puntos_stock s on p.id = s.idProducto where p.nombre like upper('%{nombre}%');"
        print("pintame consulta:",sql)
        cursor.execute(sql)
        rs = cursor.fetchall()

        lista = []

        for row in rs:
            
            print("pintame imagen:",row)
            imagen = row[7]
            if imagen:
                print("entras a la imagen")
                imagen_base64 = base64.b64encode(imagen).decode('utf-8')
            else:
                imagen_base64 = None

            producto = {"id": row[0], "nombre": row[1], "precio": row[2], "cantidadpuntos": row[3], "fecharegistro": row[4], "cantidad": row[5], "estado": row[6], "imagen":imagen_base64, "descripcion": row[8]}
            lista.append(producto)
    
        return lista
    except Exception as e:
            # Manejar errores y devolver un mensaje de error adecuado
            logger.error(f"Error al buscar producto: {e}")
            raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los productos")
    finally:
            # Cerrar la conexión a la base de datos
            connection.close()

@ruta.post("/cambiarestado")
def cambiarEstado(producto:dict):
    connection = get_connection()
    try:
        id = producto['id']
        cursor = connection.cursor()
        sqlUpdate = f"update puntos_producto set estado = 0 where id = {id};"
        cursor.execute(sqlUpdate)
        connection.commit()
        return "se desactivó el producto"
    except KeyError as e:
        return f"Error: Se esperaba el campo {str(e)}"
    except Exception as e:
        logger.error(f"Error al cambiar estado de producto: {e}")
        return f"Error: {str(e)}"
    finally:
        connection.close()






