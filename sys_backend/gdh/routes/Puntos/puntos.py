import pymssql
import pandas as pd
from fastapi import APIRouter, HTTPException, File, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from io import BytesIO
from datetime import datetime
#from conexion import conn
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
import logging



logger = logging.getLogger()

# Inicializar la aplicación FastAPI
ruta = APIRouter(
    prefix='/puntos',
    tags=["puntos"]
)

def get_connection():
    return ConectaSQL_Produccion()


@ruta.post("/agregarPuntosUsuario")
def agregarPuntosUsuario(data:"list[dict]"):
    connection = get_connection()
    try:
        print("Entras a insertar",data)
        cursor = connection.cursor()
        for row in data:
            sql = f"select idempresa from puntos_usuario where dni='{row['dniusuario']}'"
            cursor.execute(sql)
            rs = cursor.fetchone()
            print("muestrame data:",rs)
            puntosactuales = row['puntosgenerados']

            if rs is not None:
                idEmpresa = rs[0]

                sql_sp = """
                            EXEC sp_agregarpuntos
                                @dniusuario = %s,
                                @codigocampania = %s,
                                @tipomovimiento = %s,
                                @puntosgenerados = %s,
                                @puntosactuales = %s,
                                @estado = %s,
                                @idempresa = %s
                        """
                valores_sp = (row['dniusuario'],row['codigocampania'],row['tipomovimiento'],row['puntosgenerados'],puntosactuales,row['estado'],idEmpresa)
                cursor.execute(sql_sp,valores_sp)

            else:
                return {"status":500, "msg" : "dni no existe"}

        connection.commit()
        return {"status":200, "msg" : "puntos registrado correctamente"}
    except Exception as e:
        logger.error(f"Error al agregar puntos: {e}")
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener las camapañas")
    finally:
        connection.close()

def actualizarPuntosUsuario(dataUpdate):
    connection = get_connection()
    try:
        print("Entras a actualizar")
        cursor = connection.cursor()

        sql_update = '''UPDATE Puntos SET tipomovimiento = '{}',puntosgenerados = {},
                                        fechabeneficio = '{}',fechabeneficioanio = '{}',
                                        fechabeneficiomes = '{}',fechabeneficiofin = '{}',
                                        puntosactuales = {},estado = '{}'
                                        WHERE id = {}'''.format(dataUpdate['tipomovimiento'],
                                                                dataUpdate['puntosgenerados'],
                                                                dataUpdate['fechabeneficio'],
                                                                dataUpdate['fechabeneficioanio'],
                                                                dataUpdate['fechabeneficiomes'],
                                                                dataUpdate['fechabeneficiofin'],
                                                                dataUpdate['puntosactuales'],
                                                                dataUpdate['estado'],
                                                                dataUpdate['id'])

        print("pintame consulta update:", sql_update)
        cursor.execute(sql_update)
        connection.commit()
        cursor.close()
        return "Puntos actualizado."
    except KeyError as e:
        return f"Error: Se esperaba el campo {str(e)}"
    except Exception as e:
        return f"Error: {str(e)}"


@ruta.post("/agregarPuntosMasivo")
def agregarPuntosMasivo(file: UploadFile = File(...)):
    connection = get_connection()
    try:

        contents = file.file.read()
        io_contents = BytesIO(contents)
        #df = pd.read_excel(io_contents)
        df = pd.read_excel(io_contents, dtype={'dniusuario': str, 'codigocampania': str})
        listDniFalse = []
        dicDniFalse = {}
        cursor = connection.cursor()

        for index, row in df.iterrows():
            #Validar usuario
            sql = f"select idempresa from puntos_usuario where dni ='{row['dniusuario']}'"
            cursor.execute(sql)
            rs = cursor.fetchone()

            if rs is None:
                dniFalse = "Dni:" + row['dniusuario']
                dicDniFalse = {
                    "dniFalse": dniFalse
                }
                listDniFalse.append(dicDniFalse)

            #Validar campaña

            sqlCampania = f"select * from puntos_campania where codigocampania = '{row['codigocampania']}'"
            cursor.execute(sqlCampania)
            rsc = cursor.fetchone()

            if rsc:
                print("pintame estado:", rsc[5])
                if rsc[5] == 0:
                    codeFalse = "Campaña inactiva:" + row['codigocampania']
                    dicDniFalse = {
                    "dniFalse": codeFalse
                    }
                    listDniFalse.append(dicDniFalse)
            else:
                codeFalse = "Campaña:" + row['codigocampania'] + " inválida"
                dicDniFalse = {
                    "dniFalse": codeFalse
                }
                listDniFalse.append(dicDniFalse)


            '''if rsc is None:
                codeFalse = "Campaña:" + row['codigocampania']
                dicDniFalse = {
                    "dniFalse": codeFalse
                }
                listDniFalse.append(dicDniFalse)'''



        if listDniFalse:
            #return 401, listDniFalse
            return {"status":401, "data": listDniFalse}

        print("pintame el df:", df )

        for index, row in df.iterrows():
            print("pintame el row:", row )

            sql = f"select idempresa from puntos_usuario where dni ='{row['dniusuario']}'"
            cursor.execute(sql)
            rs = cursor.fetchone()

            sqlCampania = f"select * from puntos_campania where codigocampania = '{row['codigocampania']}'"
            cursor.execute(sqlCampania)
            rsc = cursor.fetchone()

            idEmpresa = rs[0]
            dniusuario = str(row['dniusuario'])
            codigocampania = row['codigocampania']
            tipomovimiento = "INGRESO"
            puntosgenerados = rsc[6]
            puntosactuales = rsc[6]
            estado = "VIGENTE"

            sql_sp = """
                            EXEC sp_agregarpuntos
                                @dniusuario = %s,
                                @codigocampania = %s,
                                @tipomovimiento = %s,
                                @puntosgenerados = %s,
                                @puntosactuales = %s,
                                @estado = %s,
                                @idempresa = %s
                        """
            valores_sp = (dniusuario,codigocampania,tipomovimiento,puntosgenerados,puntosactuales,estado,idEmpresa)
            cursor.execute(sql_sp,valores_sp)
            connection.commit()
        return {"status":200, "data": "Puntos agregado correctamente"}

    except KeyError as e:
        return f"Error: Se esperaba el campo {str(e)}"
    except Exception as e:
        logger.error(f"Error al agregar puntos: {e}")
        return f"Error: {str(e)}"
    finally:
        cursor.close()
        connection.close()




@ruta.post("/listarUsuariosDni")
def listarPuntosPorUsuario(dniUsuario:str):
    connection = get_connection()
    try:
        cursor = connection.cursor()

        sqlLista = '''select e.nombre as empresa, u.dni, u.nombre as usuario, p.tipomovimiento, c.nombre as campania, p.puntosgenerados,
                        p.fechabeneficio, p.fechabeneficiomes,p.fechabeneficioanio , p.fechabeneficiofin, p.puntosactuales, p.estado,p.id,p.puntosconsumidos
                        from puntos p inner join puntos_usuario u
                        on P.dniusuario = u.dni
                        inner join puntos_empresa e
                        on u.idempresa = e.id
                        inner join puntos_campania c
                        on p.codigocampania = c.codigocampania
                        where u.dni = '{}' order by p.id asc;'''.format(dniUsuario)
        cursor.execute(sqlLista)
        rs = cursor.fetchall()
        lista = []
        listaDic = {}
        for row in rs:
            listaDic = {
                "empresa": row[0],
                "dni": row[1],
                "usuario": row[2],
                "tipomovimiento":row[3],
                "campaña": row[4],
                "puntosgenerados": row[5],
                "fechabeneficio": row[6],
                "fechabeneficiomes": row[7],
                "fechabeneficioanio": row[8],
                "fechabeneficiofin": row[9],
                "puntosactuales": row[10],
                "estado": row[11],
                "id": row[12],
                "puntosconsumidos": row[13]
            }
            lista.append(listaDic)
        #cursor.close()
        #connection.close()
        return lista
    except Exception as e:
        logger.error(f"Error al listar puntos por usuario: {e}")
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener las camapañas")
    finally:
        # Cerrar la conexión a la base de datos
        connection.close()

@ruta.post("/listarPuntosAcumulados")
def totalPuntosAcumulados(dniUsuario:str):
    connection = get_connection()
    try:
        cursor = connection.cursor()
        sqlCount = f"select SUM(puntosactuales) as total from puntos where dniusuario = '{dniUsuario}' and estado = 'VIGENTE'"
        cursor.execute(sqlCount)
        rs = cursor.fetchone()
        total = rs[0] if rs else 0
        return total
    except Exception as e:
        logger.error(f"Error al listar total de puntos: {e}")
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los puntos")
    finally:
        # Cerrar la conexión a la base de datos
        connection.close()





@ruta.post("/listarUsuarios")
def listarPuntosAdmin(idEmpresa:str):
    connection = get_connection()
    try:
        cursor = connection.cursor()

        sqlLista = '''select e.nombre as empresa, u.dni, u.nombre as usuario, p.tipomovimiento, c.nombre as campania, p.puntosgenerados,
                        p.fechabeneficio, p.fechabeneficiomes,p.fechabeneficioanio , p.fechabeneficiofin, p.puntosactuales, p.estado,p.id
                        from puntos p inner join puntos_usuario u
                        on P.dniusuario = u.dni
                        inner join puntos_empresa e
                        on u.idempresa = e.id
                        inner join puntos_campania c
                        on p.codigocampania = c.codigocampania
                        where e.id = '{}';'''.format(idEmpresa)

        print("pintame consulta:",sqlLista)


        cursor.execute(sqlLista)
        rs = cursor.fetchall()
        lista = []
        listaDic = {}
        for row in rs:
            print("pintame row:",row)
            listaDic = {
                "empresa": row[0],
                "dni": row[1],
                "usuario": row[2],
                "tipomovimiento":row[3],
                "campaña": row[4],
                "puntosgenerados": row[5],
                "fechabeneficio": row[6],
                "fechabeneficiomes": row[7],
                "fechabeneficioanio": row[8],
                "fechabeneficiofin": row[9],
                "puntosactuales": row[10],
                "estado": row[11],
                "id": row[12],
            }
            lista.append(listaDic)
        cursor.close()
        connection.close()
    except pymssql.Error as e:
        logger.error(f"Error al listar puntos admin: {e}")
        print(f"Error: {str(e)}")
    return lista

@ruta.post("/cambiaContrasena")
def cambiarClave (user_login: dict):
    connection = get_connection()
    try:
        print("datos de usuario:",user_login['dni'])
        cursor = connection.cursor()
        query = f"SELECT * FROM puntos_usuario WHERE dni = '{user_login['dni']}' AND clave = '{user_login['clave']}'"
        cursor.execute(query)
        user = cursor.fetchone()
        if not user:
            raise HTTPException(status_code=401, detail="Credenciales incorrectas")
        else:
            sqlupdate = f"UPDATE puntos_usuario SET clave = '{user_login['claveNueva']}' where dni = '{user_login['dni']}'"
            cursor.execute(sqlupdate)
            connection.commit()
            msj = "contraseña actualizada"
            #cursor.close()
        return msj

    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al actualizar contraseña")
    finally:
        # Cerrar la conexión a la base de datos
        cursor.close()
        connection.close()

@ruta.post("/buscar")
def buscarUsuario(puntos:dict):
    connection = get_connection()
    try:
        cursor = connection.cursor()
        datos = puntos['datos']

        #sql = '''select * from campania where (upper(codigocampania) like upper('%{}%') or upper(nombre) like upper('%{}%')) order by codigocampania asc;'''.format(datos,datos)

        sqlLista = '''select SUM(p.puntosactuales) AS puntosactuales, u.nombre, u.dni, u.id, u.areas, e.nombre as empresa
                        FROM puntos p
                        INNER JOIN puntos_usuario u ON p.dniusuario = u.dni
                        INNER JOIN puntos_empresa e ON e.id = u.idempresa
                        WHERE (upper(u.dni) like upper('%{}%') or upper(u.nombre) like upper('%{}%'))
                        GROUP BY p.dniusuario, u.nombre, u.dni, e.nombre,  u.id, u.areas;'''.format(datos,datos)
        print("pintame consulta:",sqlLista)
        cursor.execute(sqlLista)
        rs = cursor.fetchall()
        lista = []
        listaDic = {}
        for row in rs:
            print("pintame row:",row)
            listaDic = {
                "id": row[3],
                "dni": row[2],
                "nombre": row[1],
                "puntos": row[0],
                "area": row[4],
                "empresa" : row[5]
            }
            lista.append(listaDic)
        return lista
    except Exception as e:
            # Manejar errores y devolver un mensaje de error adecuado
            logger.error(f"Error al buscar usuario: {e}")
            raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los puntos")
    finally:
         # Cerrar la conexión a la base de datos
        connection.close()

@ruta.get("/listarUsuarioConsolidado")
def listarConsolidado():
    connection = get_connection()
    try:
        cursor = connection.cursor()
        sqlLista = '''select SUM(p.puntosactuales) AS puntosactuales, u.nombre, u.dni, u.id, u.areas, e.nombre as empresa
                        FROM puntos p
                        INNER JOIN puntos_usuario u ON p.dniusuario = u.dni
                        INNER JOIN puntos_empresa e ON e.id = u.idempresa
                        GROUP BY p.dniusuario, u.nombre, u.dni, e.nombre,  u.id, u.areas;'''
        print("pintame consulta:",sqlLista)
        cursor.execute(sqlLista)
        rs = cursor.fetchall()
        lista = []
        listaDic = {}

        for row in rs:
            print("pintame row:", row)
            listaDic = {
                "id": row[3],
                "dni": row[2],
                "nombre": row[1],
                "puntos": row[0],
                "area": row[4],
                "empresa" : row[5]
            }
            lista.append(listaDic)
        return lista
    except Exception as e:
            # Manejar errores y devolver un mensaje de error adecuado
            logger.error(f"Error al listar consolidado: {e}")
            raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los productos")
    finally:
        # Cerrar la conexión a la base de datos
        connection.close()

@ruta.post("/listarUsuariosCardex")
def listarusuariocardex(dniUsuario:str):
    connection = get_connection()
    try:
        cursor = connection.cursor()

        sqlLista = '''select p.nombre as producto, cu.fecharegistro, cu.cantidad, cu.tipo as estado,
                        p.cantidadpuntos as cpproducto, cu.cantidad * p.cantidadpuntos as cpproductototal,
                        cu.fechaentrega, cu.estado as estadoentrega, cu.id
                        from puntos_cardex_usuario cu inner join puntos_producto p
                        on cu.idproducto = p.id
                        inner join puntos_usuario u
                        on cu.dniusuario = u.dni
                        where cu.dniusuario = '{}';'''.format(dniUsuario)
        cursor.execute(sqlLista)
        rs = cursor.fetchall()
        lista = []
        listaDic = {}
        for row in rs:
            listaDic = {
                "producto" : row[0],
                "fecharegistro" : row[1],
                "cantidad" : row[2],
                "estado": row[3],
                "puntosxproducto": row[4],
                "puntostotalproducto": row[5],
                "fechaentrega": row[6],
                "estadoentrega": row[7],
                "id": row[8]
            }
            lista.append(listaDic)
        return lista
    except Exception as e:
        logger.error(f"Error al listar usuario cardex: {e}")
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener las camapañas")
    finally:
        # Cerrar la conexión a la base de datos
        connection.close()

@ruta.post("/agregardemo")
def agregarDemoMasivo(lista:list[dict]):
    connection = get_connection()
    try:
        listDniFalse = []
        dicDniFalse = {}
        cursor = connection.cursor()
        for row in lista:
            #Validar usuario
            sql = f"select idempresa from puntos_usuario where dni ='{row['dniusuario']}'"
            cursor.execute(sql)
            rs = cursor.fetchone()

            if rs is None:
                dicDniFalse = {
                    "dniFalse": row['dniusuario']
                }
                listDniFalse.append(dicDniFalse)

            #Validar campaña

            sqlCampania = f"select * from puntos_campania where codigocampania = '{row['codigocampania']}'"
            cursor.execute(sqlCampania)
            rsc = cursor.fetchone()

            if rsc is None:
                dicDniFalse = {
                    "dniFalse": row['codigocampania']
                }
                #listCodeFalse.append(dicDniFalse)

        if listDniFalse:
            return listDniFalse
        print("ejecuta el siguiente for para el excel")

        return {"status":200, "msg": "Puntos agregado correctamente"}
    except KeyError as e:
        return f"Error: Se esperaba el campo {str(e)}"
    except Exception as e:
        return f"Error: {str(e)}"
    finally:
        # Cerrar la conexión a la base de datos
        cursor.close()
        connection.close()


@ruta.post("/actualizarcardex")
def actualizarcardex(datacardex:dict):
    connection = get_connection()
    try:
        cursor = connection.cursor()
        idcardex = datacardex['id']
        fechaentrega = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        sqlUpdate = f"update puntos_cardex_usuario set fechaentrega = '{fechaentrega}', estado = 1 where id = {idcardex}"
        cursor.execute(sqlUpdate)
        connection.commit()
        return "se entregó el producto"
    except Exception as e:
        logger.error(f"Error al actualizar cardex: {e}")
        raise HTTPException(status_code=500, detail="Error al entregar producto")
    finally:
        connection.close()


