
from fastapi import HTTPException, APIRouter
from fastapi.middleware.cors import CORSMiddleware
#from conexion import conn
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
import psycopg2
from pydantic import BaseModel
import logging


logger = logging.getLogger()

ruta = APIRouter(
    prefix='/terminos',
    tags=["puntos"]
)



def get_connection():
    return ConectaSQL_Produccion()




@ruta.get("/obtenerHtml")
def mostrarHtml():
    connection = get_connection()
    try:
        cursor = connection.cursor()
        consultaSQL = "select estructurahtml from puntos_plantillas;"
        print("pintame consulta html:",consultaSQL)
        cursor.execute(consultaSQL)
        rs = cursor.fetchall()
        estructuraHtml = rs[0]
        print("pintame estructura html:",estructuraHtml)
        return estructuraHtml
    except psycopg2.Error as e:
        logger.error(f"Error al mostrar plantilla: {e}")
        raise e
    finally:
        connection.close()


class HTMLStructureUpdate(BaseModel):
    id: int
    structure_html: str

@ruta.post("/actualizarHtml/{structure_id}")
def actualizarPlantilla(structure_id: int, html_structure: HTMLStructureUpdate):
    connection = get_connection()
    try:
        print("muestrame estructura html id:",structure_id)
        print("muestrame estructura html:",html_structure)
        cursor = connection.cursor()
        updateSQL = "UPDATE puntos_plantillas SET estructurahtml = %s WHERE id = %s;"
        cursor.execute(updateSQL, (html_structure.structure_html, structure_id))
        connection.commit()
        return {"message": "Plantilla actualizada correctamente"}
    except psycopg2.Error as e:
        logger.error(f"Error al actualizar plantillas: {e}")
        raise HTTPException(status_code=500, detail=str(e))
    finally:
        cursor.close()

