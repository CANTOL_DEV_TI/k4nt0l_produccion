##Paquetes a usar## 
import hashlib 
from seguridad.functions import auditoriaFunction 
from fastapi import HTTPException, status 
from typing import Union 
from conexiones import conexion_sql_solo 
from seguridad.functions import tokenFunction 
from ldap3 import Server,Connection, SAFE_SYNC 

##GET para Login de Usuarios 
 
def Login(usuario: Union[str, None] = "",password: Union[str, None] = "",cia : Union[str, None] = ""):       
     
    valAD = LoginAD(usuario,password)

    if(valAD == 1):

        dc_contrasena = hashlib.md5(password.encode()) 
    
        conn = conexion_sql_solo.ConectaSQL_Seguridad() 
    
        cur_val_usuario = conn.cursor(as_dict = True) 
            
        cad_sql = f"select ISNULL( count(*),0) as LoginV from tAccesoEmpresa AE inner join tUsuarios U on U.Usuario_ID = AE.Usuario_ID inner join tEmpresa E on E.Empresa_ID = AE.Empresa_ID where E.Empresa_Codigo = '{cia}' and U.Usuario_Codigo='{usuario}'" # and U.Usuario_Password='{dc_contrasena.hexdigest()}'" 
        #print(cad_sql) 
        cur_val_usuario.execute(cad_sql) 
    
        valido = 0 
    
        for row in cur_val_usuario: 
            valido = (row['LoginV'])     
                                                        
        conn.close()     
        #print(valido) 
        if valido == 1: 
    
            auditoriaFunction.auditausuarios(1 ,usuario,cia,"Acceso al Sistema")       
            
        else: 
            auditoriaFunction.auditausuarios(2,usuario,cia,"Intento Fallido de acceso") 
            raise HTTPException(status_code=status.HTTP_203_NON_AUTHORITATIVE_INFORMATION,detail="Error en el inicio de sesion, revise sus credenciales")         
    
        token = tokenFunction.creacion_token(cia,usuario)       
    
        return token
    else:
        return valAD

def LoginAD(pUsuario: Union[str, None] = "",pPass: Union[str, None] = ""):
    try:
        server = Server('192.168.1.236')
        sUsuario = pUsuario + "@tecnopress.pe"
        #print(sUsuario)
        conn = Connection(server,sUsuario,pPass,client_strategy=SAFE_SYNC, auto_bind=True)

        status = conn.search('o=test', '(objectclass=*)')  # usually you don't need the original request (4th element of the returned tuple)
        
        #print(status[1]['result'])

        return status[1]['result']
    except Exception as err:
        print(err)
        return f"Error : {err}."