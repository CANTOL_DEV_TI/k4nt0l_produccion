from typing import Optional
from pydantic import BaseModel


class login_Schema(BaseModel):
    usuario_login : Optional[str]
    usuario_contraseña : Optional[str]
    usuario_empresa : Optional[str]

    class Config:
        orm_mode = True