from fastapi import APIRouter
from login.schemas import loginSchema

from login.functions import loginFunctions


ruta = APIRouter(
    prefix="/login",tags=["Login"]
)

@ruta.post('/')
async def loginUsuarios( pLogin : loginSchema.login_Schema):
    #print(pLogin)
    pUsuario = pLogin.usuario_login
    pPassword = pLogin.usuario_contraseña
    pCia = pLogin.usuario_empresa

    token = loginFunctions.Login(pUsuario,pPassword,pCia)
    return token

@ruta.post('/AD/')
async def loginAD_Usuarios(pLogin:loginSchema.login_Schema):
    pUsuario = pLogin.usuario_login
    pPass = pLogin.usuario_contraseña

    token = loginFunctions.LoginAD(pUsuario,pPass)

    return token
