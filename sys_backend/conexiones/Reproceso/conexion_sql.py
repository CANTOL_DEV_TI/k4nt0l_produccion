import sys
import pymssql
from datetime import datetime
from produccion.utils.functions import obj


class dbConnection:
  def __init__(self):
    self.selectArray = []
    pass

  def openDB(self):
    self.cnxn = pymssql.connect(server="192.168.5.4", user="sa", password="Cantol123@abc", database="WEB_ERP_CANTOL_DESARROLLO")
    self.cursor = self.cnxn.cursor()

  def closeDB(self): 
    self.cursor.close()
    self.cnxn.close()
    

  def getData(self):
    self.openDB()
    SQL_QUERY = "select top 40000 * from (select T.SubArea, T.SAPNumero, T.CodigoArticulo, T.NombreArticulo, (select SUM(od.cantidad_prod_mala) from oee_cab oc inner join oee_det od on oc.id_oee = od.id_oee where od.num_of_sap = T.SAPNumero) as ProduccionMala, (select SUM(od.cantidad_prod_buena) from oee_cab oc inner join oee_det od on oc.id_oee = od.id_oee where od.num_of_sap = T.SAPNumero) as ProduccionBuena , (select SUM(od.cantidad_total) from oee_cab oc inner join oee_det od on oc.id_oee = od.id_oee where od.num_of_sap = T.SAPNumero) as ProduccionTotal from (select oc.id_oee, oc.fecha_doc as fe, sa.nom_subarea as SubArea, od.cod_articulo as CodigoArticulo, od.articulo as NombreArticulo, od.cantidad_prod_mala as ProduccionMala, od.cantidad_prod_buena as ProduccionBuena, od.cantidad_total as ProduccionTotal, od.causa as Causa, od.motivo_causa as Motivo, od.detalle as Detalle, od.num_of_sap as SAPNumero from oee_cab oc inner join sub_area sa on oc.cod_subarea = sa.cod_subarea inner join oee_det od on oc.id_oee  = od.id_oee where od.cantidad_prod_mala > 0 and od.tipo_registro = 'PRODUCCION') as T GROUP BY T.SAPNumero, T.SubArea, T.CodigoArticulo, T.NombreArticulo) as F"
    self.cursor.execute(SQL_QUERY)
    records = self.cursor.fetchall()
    self.closeDB()
    return records

  def get_motivo_causa_areas(self, saPattern):
    self.selectArray.clear()
    self.openDB()
    SQL_QUERY = f"select cod_motivo_causa, motivo_causa from oee_motivo_causa mc inner join oee_causa c on mc.cod_causa = c.cod_causa inner join sub_area sa on mc.cod_subarea = sa.cod_subarea where c.cod_causa = 11 and UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(nom_subarea, 'Á', 'A'), 'É', 'E'), 'Í', 'I'), 'Ó', 'O'), 'Ú', 'U')) LIKE UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('%{saPattern}%', 'Á', 'A'), 'É', 'E'), 'Í', 'I'), 'Ó', 'O'), 'Ú', 'U')) group by cod_motivo_causa, motivo_causa;"
    self.cursor.execute(SQL_QUERY)
    for row in self.cursor:
        self.selectArray.append(row)
    self.closeDB()
    return self.selectArray
      
  #obtiene id y nombre de subarea
  def getAllSubAreas(self):
    SQL_QUERY = "SELECT sa.cod_subarea , sa.nom_subarea FROM sub_area sa;"
    self.openDB()
    self.cursor.execute(SQL_QUERY)
    records = self.cursor.fetchall()
    self.closeDB()
    return records

  #insertar simulacion
  def postSimulation(self, model, DocNumSAP, isChecked = False):
    self.openDB()
    try:
        SQL_QUERY_0 = "INSERT INTO reproceso_cab (%sItemCode, ProdName, Type, PlannedQty, SimDate, PostDate, DueDate, StartDate,Warehouse, OriginType, prcCode, Usuario_Codigo_SIM, Usuario_Codigo_VAL, cod_motivo_causa, isChecked) VALUES (%s" %("DocNumSAP, " if isChecked else "", ("'"+ DocNumSAP + "', ") if isChecked else "")
        SQL_QUERY_1 = f"'{model.ItemCode}', '{model.ProdName}','{model.Type}',{model.PlannedQty}, '{model.SimDate}', '{model.PostDate}','{model.DueDate}','{model.StartDate}','{model.Warehouse}', '{model.OriginType}', '{model.PrcCode}', '{model.Usuario_Codigo_SIM}', {'NULL' if model.Usuario_Codigo_VAL is None else model.Usuario_Codigo_VAL}, {'NULL' if model.cod_motivo_causa is None else model.cod_motivo_causa}, {int(model.isChecked)});"
        SQL_QUERY = SQL_QUERY_0 + SQL_QUERY_1
        stmt = "INSERT INTO reproceso_det (id_cab, ItemCode, ItemName, IssuedType, PlannedQty, DueDate, StartDate, wareHouse, ItemType, OcrCode, OcrCode2, baseCost) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        self.cursor.execute(SQL_QUERY)
        self.cnxn.commit()
        SQL_QUERY_GET_LAST = "SELECT top 1 id_cab FROM reproceso_cab ORDER BY id_cab DESC"
        self.cursor.execute(SQL_QUERY_GET_LAST)
        simulationId = self.cursor.fetchone()[0]
        if len(model.ProductionOrderLines) > 0:
          self.cursor.executemany(stmt, self.get_reproceso_det_format(simulationId, model.ProductionOrderLines))
        self.cnxn.commit()
        self.closeDB()
        return 201, {"SWC_ID" : simulationId}
    except Exception as e:
        self.closeDB()
        print(f"An error ocurred: {e}")
        return 500, {"SWC_ID" : "XXXXXXX"}

 
  def get_reproceso_det_format(self, simulationId, ProductionOrderLines):
    ProductionOrderLines_tuple = [(str(simulationId), x.ItemNo, x.ItemName, x.ProductionOrderIssueType, str(x.PlannedQuantity), x.EndDate, x.StartDate, x.Warehouse, x.ItemType, x.DistributionRule, x.DistributionRule2, str(x.baseCost)) for x in ProductionOrderLines if bool(x)]
    return ProductionOrderLines_tuple


  def get_whole_simulations(self, usuarioid, dynamic_offset: int = 0, static_offset: int = 20):
    try:
      self.openDB()
      SQL_QUERY_GET_LAST = (f"SELECT id_cab, ItemCode, ProdName, PlannedQty, SimDate, PostDate, DueDate, StartDate, WareHouse, Usuario_Codigo_SIM as Usuario_Nombre, motivo_causa, isChecked, DocNumSAP, (select (rd.PlannedQty * rd.baseCost) from reproceso_det rd where id_cab = rc.id_cab and ItemCode = rc.ItemCode) as initialCost, (select sum(rd.PlannedQty * rd.baseCost) from reproceso_det rd where id_cab = rc.id_cab and ItemCode != rc.ItemCode) as additionalCost FROM reproceso_cab rc LEFT JOIN (select * from oee_motivo_causa omc where cod_causa = 11) as omc ON rc.cod_motivo_causa = omc.cod_motivo_causa ORDER BY rc.SimDate DESC id_cab DESC OFFSET {str(dynamic_offset)} ROWS FETCH NEXT {str(static_offset)} ROWS ONLY;" if usuarioid == 'XXXX' else f"SELECT id_cab, ItemCode, ProdName, PlannedQty, SimDate, PostDate, DueDate, StartDate, WareHouse, Usuario_Nombre, motivo_causa, isChecked, DocNumSAP, (select (rd.PlannedQty * rd.baseCost) from reproceso_det rd where id_cab = rc.id_cab and ItemCode = rc.ItemCode) as initialCost, (select sum(rd.PlannedQty * rd.baseCost) from reproceso_det rd where id_cab = rc.id_cab and ItemCode != rc.ItemCode) as additionalCost FROM reproceso_cab rc INNER JOIN sub_area sa ON rc.PrcCode = sa.centro_costo_sap INNER JOIN cali_personal cp ON sa.usuario_login = cp.usuarioid INNER JOIN Usuarios_Cantol.dbo.tUsuarios tu ON rc.Usuario_Codigo_SIM = tu.Usuario_Codigo LEFT JOIN (select * from oee_motivo_causa omc where cod_causa = 11) as omc ON rc.cod_motivo_causa = omc.cod_motivo_causa WHERE cp.usuarioid = '{usuarioid}' AND cp.rol_id = 4 ORDER BY rc.SimDate DESC, id_cab DESC OFFSET {str(dynamic_offset)} ROWS FETCH NEXT {str(static_offset)} ROWS ONLY;")
      self.cursor.execute(SQL_QUERY_GET_LAST)
      simulations = self.cursor.fetchall()
      self.closeDB()
      return simulations
    except Exception as error:
      self.closeDB()
      print(f"An error ocurred: {error}")
      return []

  def get_total_number_simulations(self, usuarioid: str = ""):
    try:
      self.openDB()
      QUERY = f"SELECT count(*) FROM reproceso_cab rc INNER JOIN sub_area sa ON rc.PrcCode = sa.centro_costo_sap INNER JOIN cali_personal cp ON sa.usuario_login = cp.usuarioid INNER JOIN Usuarios_Cantol.dbo.tUsuarios tu ON rc.Usuario_Codigo_SIM = tu.Usuario_Codigo LEFT JOIN (select * from oee_motivo_causa omc where cod_causa = 11) as omc ON rc.cod_motivo_causa = omc.cod_motivo_causa WHERE cp.usuarioid = '{usuarioid}' AND cp.rol_id = 4"
      self.cursor.execute(QUERY)
      QtySim = self.cursor.fetchone()[0]
      self.closeDB()
      return QtySim
    except Exception as error:
      print(f"An error ocurred: {error}")
      return 0


  def validateRol(self, usuarioid, rol_id):
    try:
      self.openDB()
      QUERY = f"select 1 from cali_personal cp where usuarioid = '{usuarioid}' and rol_id = {rol_id}"
      self.cursor.execute(QUERY)
      isOk = self.cursor.fetchone()
      self.closeDB()
      return isOk
    except Exception as error:
      print(f"An error ocurred: {error}")
      return []

  # aqui se debe
  def get_database_model(self, id_cab, checkDate):
    try:
      self.openDB()
      QUERY0 = f"SELECT * FROM reproceso_cab WHERE id_cab = {str(id_cab)};"
      self.cursor.execute(QUERY0)
      data_cabecera = self.cursor.fetchall()[0]
      QUERY1 = f"SELECT * FROM reproceso_det WHERE id_cab = {str(id_cab)};"
      self.cursor.execute(QUERY1)
      data_detalle = self.cursor.fetchall()
      self.closeDB()
      return self.get_of_sap_format(data_cabecera,data_detalle, checkDate)
    except Exception as error:
      print(f"An Error Ocurred on get_database_model: {error}")
      return None, False

  def get_of_sap_format(self, data_cabecera, data_detalle, checkDate):
    checkDate = datetime.strptime(checkDate,'%Y-%m-%d')
    postDate = datetime.strptime(data_cabecera[7].strftime('%Y-%m-%d'),'%Y-%m-%d')
    startDate = datetime.strptime(data_cabecera[9].strftime('%Y-%m-%d'),'%Y-%m-%d')
    dueDate = datetime.strptime(data_cabecera[8].strftime('%Y-%m-%d'),'%Y-%m-%d')

    # verifica si fecha de validacion esta por despues de la fecha de publicacion o de inicio
    if(checkDate > postDate):
      postDate = checkDate
      if(checkDate > startDate):
        startDate = checkDate 
        if(checkDate > dueDate):
          dueDate = checkDate

    ProductionOrderLines = [obj({"ItemNo": x[1], "PlannedQuantity": x[4], "ProductionOrderIssueType": x[3], "Warehouse": x[7], "DistributionRule": x[9], "DistributionRule2": x[10], "ItemType": x[8], "StartDate": startDate.strftime('%Y-%m-%d'), "EndDate": dueDate.strftime('%Y-%m-%d'), "ItemName": x[2]}) for x in data_detalle if bool(x)]
    try:
      data_dict = {
      "ItemCode": data_cabecera[2],
      "ProdName": data_cabecera[3],
      "PlannedQty": data_cabecera[5],
      "PostDate": postDate.strftime('%Y-%m-%d'),
      "DueDate": dueDate.strftime('%Y-%m-%d'),
      "StartDate": startDate.strftime('%Y-%m-%d'),
      "Warehouse": data_cabecera[10],
      "Usuario_Codigo_SIM": data_cabecera[13],
      "ProductionOrderLines": ProductionOrderLines,
      }
      OBJsap = obj(data_dict)
      return OBJsap, True
    except Exception as error:
      print(f"An error ocurred: {error}")
      return None, False

  def set_validate_sim(self, usuarioid, id_cab, sap_number, PostDate: str = "", StartDate: str = "", DueDate: str = ""):
    try:
      self.openDB()
      QUERY0 = f"UPDATE reproceso_cab SET DocNumSAP = {str(sap_number)}, Usuario_Codigo_VAL = '{usuarioid}', isChecked = 1 WHERE id_cab = {str(id_cab)};"
      self.cursor.execute(QUERY0)
      self.cnxn.commit()
      QUERY1 = f"update reproceso_cab set PostDate = '{PostDate}', StartDate = '{StartDate}', DueDate = '{DueDate}' where id_cab = {id_cab};"
      self.cursor.execute(QUERY1)
      self.cnxn.commit()
      QUERY2 = f"update reproceso_det set StartDate = '{StartDate}', DueDate = '{DueDate}' where id_cab = {id_cab};"
      self.cursor.execute(QUERY2)
      self.cnxn.commit()
      self.closeDB()
      return True
    except Exception as error:
      print(f"An Error Ocurred on set_validate_sim: {error}")
      return False


  def get_areas_by_user(self, idUser: str = ""):
    try:
      self.openDB()
      QUERY0 = f"SELECT nom_subarea FROM sub_area WHERE usuario_login = '{idUser}';"
      self.cursor.execute(QUERY0)
      data = self.cursor.fetchall()
      self.closeDB()
      return data
    except Exception as error:
      print(f"An Error Ocurred on get_areas_by_user: {error}")
      return None