import requests
from hdbcli import dbapi
from produccion.utils.functions import obj

class sapConnection:
    def __init__(self):
        self.selectArray = []
        pass
    
    def openConnection(self):
        self.con = dbapi.connect(address='192.168.5.2', port='30015', user='B1ADMIN', password='C4nt012019$%$.')
        self.cursor = self.con.cursor()
    
    def closeCursor(self):
        self.cursor.close()
        self.con.close()

    # def getDatafromTable(self, tableName : str = None, limit : int = 0):
    def getDataProductionByOF(self, of: str = None):
      if not(not(of)):
          self.selectArray.clear()
          sql = "SELECT \"DocNum\", \"ItemCode\", \"PostDate\", \"CloseDate\", \"Warehouse\" FROM SBO_TECNO_PRODUCCION.OWOR WHERE \"DocNum\" = {}".format(of)
          self.openConnection()
          self.cursor.execute(sql)
          record = self.cursor.fetchall()
          return record
      else:
        print("invalid input parameters")

    def getFailsOF(self):
      self.selectArray.clear()
      sql = "SELECT top 50 * FROM (SELECT ow.\"DocNum\" AS of, ow.\"ItemCode\" AS productId, itm.\"ItemName\" AS productName, ow.\"RjctQty\", ow.\"CmpltQty\", ow.\"PlannedQty\", ow.\"PostDate\", ow.\"CloseDate\", ow.\"Warehouse\" FROM SBO_TECNO_PRODUCCION.OWOR ow INNER JOIN SBO_TECNO_PRODUCCION.OITM itm ON ow.\"ItemCode\" = itm.\"ItemCode\" WHERE RIGHT(ow.\"ItemCode\", 1) !='R' AND ow.\"RjctQty\" > 0 ORDER BY ow.\"PostDate\" DESC)"
      self.openConnection()
      self.cursor.execute(sql)
      response = self.cursor.fetchall()
      self.closeCursor()
      return response
    
    #obtiene lista de materiales por idProduct para reproceso
    def getMaterialsByAD(self, od: str = None):
        # splited_subarea = subarea.split(" ")
        self.selectArray.clear()
        sql = "SELECT '0.0' AS NroOrden, 'ARTICULO' AS TIPO, OTM.\"ItemCode\" AS Codigo_Det, OTM.\"ItemName\" AS Nombre_Det, 'UND' AS Und, 1 AS Cantidad, 1 AS Costo, (SELECT \"LastPurPrc\" FROM SBO_TECNO_PRODUCCION.OITM OIT WHERE OIT.\"ItemCode\" = '{}') AS costo_Uno, 0 AS Stock, 'Undefined' AS WareCod FROM SBO_TECNO_PRODUCCION.OITM AS OTM WHERE \"ItemCode\" = '{}' UNION SELECT concat('1.' , TO_VARCHAR(\"ChildNum\")) as NroOrden, IFNULL(CASE WHEN \"Type\" = '290' THEN 'RECURSO' WHEN \"Type\" = '04' THEN 'ARTICULO' ELSE 'OTROS' END ,'OTROS') AS TIPO, RD.\"Code\" as Codigo_Det, RD.\"ItemName\" as Nombre_Det, It.\"InvntryUom\" AS Und, \"Quantity\" as Cantidad, CASE WHEN RD.\"Code\" LIKE '%GGRC%' THEN It.\"AvgPrice\" ELSE It.\"LastPurPrc\" END as Costo, (SELECT \"LastPurPrc\" FROM SBO_TECNO_PRODUCCION.OITM OIT WHERE OIT.\"ItemCode\" = RD.\"Father\") AS costo_Uno, 0 AS Stock, 'Undefined' AS WareCod FROM SBO_TECNO_PRODUCCION.ITT1 RD LEFT JOIN SBO_TECNO_PRODUCCION.OITM It ON It.\"ItemCode\" = RD.\"Code\" WHERE RD.\"Father\" = '{}' AND RD.\"Code\" NOT LIKE '%MOH%' AND RD.\"Code\" NOT LIKE '%MOM%' UNION SELECT '2.0' AS NROORDEN, IFNULL(CASE WHEN \"ObjType\" = '290' THEN 'RECURSO' WHEN \"ObjType\" = '04' THEN 'ARTICULO' ELSE 'OTROS' END ,'OTROS') AS TIPO, \"ResCode\" AS CODIGO_DET, \"ResName\" AS NOMBRE_DET, \"UnitOfMsr\" AS UND, NULL AS CANTIDAD, \"StdCost1\" AS COSTO, NULL AS COSTO_UNO, 0 AS STOCK, NULL AS WARECOD FROM SBO_TECNO_PRODUCCION.ORSC WHERE \"ResCode\" LIKE '%REPROCESO%';".format(od, od, od)
        self.openConnection()
        self.cursor.execute(sql)
        for row in self.cursor:
            self.selectArray.append(row)
        self.closeCursor()
        return self.selectArray

    #obtiene lista de materiales por patron del almacen que tiene ultimo precio del maestro
    #por defecto esta consulta trae solo 10 filas
    def getMaterialfromWare(self, ware: str, searchPattern: str ='', offset: int = 0):
        self.selectArray.clear()
        sql = "SELECT '-' AS NroOrden, IFNULL(CASE WHEN itt1.\"Type\" = '290' THEN 'RECURSO' WHEN \"Type\" = '04' THEN 'ARTICULO' ELSE 'OTROS' END ,'OTROS') AS TIPO, oitw.\"ItemCode\" AS Codigo_Det, oitm.\"ItemName\" AS Nombre_Det, oitm.\"InvntryUom\" AS Und, NULL AS Cantidad, oitm.\"LastPurPrc\" AS Costo FROM SBO_TECNO_PRODUCCION.OITW oitw LEFT JOIN SBO_TECNO_PRODUCCION.OITM oitm ON oitw.\"ItemCode\" = oitm.\"ItemCode\" LEFT JOIN SBO_TECNO_PRODUCCION.ITT1 itt1 ON oitw.\"ItemCode\" = itt1.\"Code\" WHERE \"WhsCode\" like '%{}%' AND oitm.\"LastPurPrc\"  > 0  AND UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONCAT(oitw.\"ItemCode\", oitm.\"ItemName\"), 'Á', 'A'), 'É', 'E'), 'Í', 'I'), 'Ó', 'O'), 'Ú', 'U')) LIKE UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('%{}%', 'Á', 'A'), 'É', 'E'), 'Í', 'I'), 'Ó', 'O'), 'Ú', 'U')) GROUP BY itt1.\"Type\", oitw.\"ItemCode\", oitm.\"ItemName\",oitm.\"LastPurPrc\",oitm.\"InvntryUom\" LIMIT 10 OFFSET {};".format(ware.upper(), searchPattern.upper(), str(offset))
        self.openConnection()
        self.cursor.execute(sql)
        for row in self.cursor:
            self.selectArray.append(row)
        self.closeCursor()
        return self.selectArray

    #obtiene materiales por codigo o por nombre de material
    def getMaterialsByPattern(self, pattern: str = None):
        self.selectArray.clear()
        sql = "SELECT CONCAT(CONCAT(ITT.\"Code\", ' - '), ITT.\"ItemName\"), MAX(ITT.\"Quantity\") AS Quantity, MAX(ITT.\"Price\") AS Price, OTT.\"InvntryUom\" AS Und, IFNULL(CASE WHEN ITT.\"Type\" = '290' THEN 'RECURSO' WHEN \"Type\" = '04' THEN 'ARTICULO' ELSE 'OTROS' END ,'OTROS') AS TIPO FROM SBO_TECNO_PRODUCCION.ITT1 ITT LEFT JOIN SBO_TECNO_PRODUCCION.OITM OTT ON ITT.\"Code\" = OTT.\"ItemCode\" WHERE concat(ITT.\"Code\", ITT.\"ItemName\") like '%{}%' GROUP BY ITT.\"Code\", ITT.\"ItemName\", OTT.\"InvntryUom\", ITT.\"Type\";".format(pattern.upper())
        self.openConnection()
        self.cursor.execute(sql)
        for row in self.cursor:
            self.selectArray.append(row)
        self.closeCursor()
        return self.selectArray

    #obtiene todos los materiales en general
    def getMaterials(self):
        self.selectArray.clear()
        sql = "SELECT top 50 ITT.\"Code\" AS Code, ITT.\"ItemName\" AS ItemName, MAX(ITT.\"Quantity\") AS Quantity, MAX(ITT.\"Price\") AS Price, OTT.\"InvntryUom\" AS Und, IFNULL(CASE WHEN ITT.\"Type\" = '290' THEN 'RECURSO' WHEN \"Type\" = '04' THEN 'ARTICULO' ELSE 'OTROS' END ,'OTROS') AS TIPO FROM SBO_TECNO_PRODUCCION.ITT1 ITT LEFT JOIN SBO_TECNO_PRODUCCION.OITM OTT ON ITT.\"Code\" = OTT.\"ItemCode\" GROUP BY ITT.\"Code\", ITT.\"ItemName\", OTT.\"InvntryUom\", ITT.\"Type\";"
        self.openConnection()
        self.cursor.execute(sql)
        for row in self.cursor:
            self.selectArray.append(row)
        self.closeCursor()
        return self.selectArray


    #obtiene todos los articulos que pertenecen a una sub area de produccion
    def get_Articulo_Area(self, members: list = []):
      init_str = "OPRC.\"PrcCode\" = "
      members_str = " OR OPRC.\"PrcCode\" = ".join(f"'{x}'" for x in members)
      last_member_str = init_str + members_str
      self.selectArray.clear()
      sql = f"SELECT itt1.\"Father\", oit.\"ItemName\", oprc.\"PrcCode\" FROM SBO_TECNO_PRODUCCION.ITT1 itt1 INNER JOIN SBO_TECNO_PRODUCCION.OPRC oprc ON itt1.\"OcrCode2\" = oprc.\"PrcCode\" INNER JOIN SBO_TECNO_PRODUCCION.OITM oit ON itt1.\"Father\" = oit.\"ItemCode\" WHERE {last_member_str} GROUP BY itt1.\"Father\", oit.\"ItemName\", oprc.\"PrcCode\";"
      self.openConnection()
      self.cursor.execute(sql)
      for row in self.cursor:
          self.selectArray.append(row)
      self.closeCursor()
      return self.selectArray

    #obtiene todas las subareas del area de produccion
    def get_areas(self):
      self.selectArray.clear()
      sql = f"SELECT \"PrcCode\", \"PrcName\" FROM SBO_TECNO_PRODUCCION.OPRC WHERE \"PrcCode\" LIKE '%20001%' AND \"DimCode\" = 2 AND \"Active\" = 'Y' AND \"PrcCode\" != '2000103' AND \"PrcCode\" != '2000104' ORDER BY \"PrcCode\";"
      
      self.openConnection()
      self.cursor.execute(sql)
      for row in self.cursor:
          self.selectArray.append(row)
      self.closeCursor()
      return self.selectArray

    #obtiene los materiales por subarea de produccion
    def get_material_subarea(self, PrcCode):
      init_str = " OPRC.\"PrcCode\" = "
      members_str = " OR OPRC.\"PrcCode\" = ".join(f"'{x}'" for x in PrcCode)
      last_member_str = init_str + members_str
      self.selectArray.clear()
      sql = f"(SELECT itt1.\"Code\" AS Code, oitm.\"ItemName\" AS itemName, itt1.\"Quantity\" AS Quantity, oitm.\"LastPurPrc\" AS Price, oitm.\"InvntryUom\" AS Und, IFNULL(CASE WHEN itt1.\"Type\" = '290' THEN 'RECURSO' WHEN \"Type\" = '04' THEN 'ARTICULO' ELSE 'OTROS' END ,'OTROS') AS TIPO FROM SBO_TECNO_PRODUCCION.ITT1 itt1 INNER JOIN SBO_TECNO_PRODUCCION.OPRC oprc ON itt1.\"OcrCode2\" = oprc.\"PrcCode\" INNER JOIN SBO_TECNO_PRODUCCION.OITM oitm ON itt1.\"Code\" = oitm.\"ItemCode\" WHERE ({last_member_str}) AND itt1.\"Code\" NOT LIKE '%GGR%' GROUP BY itt1.\"Code\", oitm.\"ItemName\", itt1.\"Quantity\", oitm.\"LastPurPrc\", oitm.\"InvntryUom\", itt1.\"Type\") UNION (SELECT itt1.\"Code\" AS Code, oitm.\"ItemName\" AS itemName, NULL AS Quantity, oitm.\"AvgPrice\" AS Price, oitm.\"InvntryUom\" AS Und, IFNULL(CASE WHEN itt1.\"Type\" = '290' THEN 'RECURSO' WHEN \"Type\" = '04' THEN 'ARTICULO' ELSE 'OTROS' END ,'OTROS') AS TIPO FROM SBO_TECNO_PRODUCCION.ITT1 itt1 INNER JOIN SBO_TECNO_PRODUCCION.OPRC oprc ON itt1.\"OcrCode2\" = oprc.\"PrcCode\" INNER JOIN SBO_TECNO_PRODUCCION.OITM oitm ON itt1.\"Code\" = oitm.\"ItemCode\" WHERE ({last_member_str}) AND itt1.\"Code\" LIKE '%GGR%' GROUP BY itt1.\"Code\", oitm.\"ItemName\", oitm.\"AvgPrice\", oitm.\"InvntryUom\", itt1.\"Type\");"
      self.openConnection()
      self.cursor.execute(sql)
      for row in self.cursor:
          self.selectArray.append(row)
      self.closeCursor()
      return self.selectArray
    
    #obtiene el almacen que no son observados ni reproceso por subarea
    def get_warehouse_by_subarea(self, subarea):
      self.selectArray.clear()
      sql = f"SELECT * FROM (SELECT \"WhsCode\" AS WareCode, \"WhsName\" AS WareName FROM SBO_TECNO_PRODUCCION.OWHS owhs WHERE UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(owhs.\"WhsName\", 'Á', 'A'), 'É', 'E'), 'Í', 'I'), 'Ó', 'O'), 'Ú', 'U')) LIKE UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('%{subarea}%', 'Á', 'A'), 'É', 'E'), 'Í', 'I'), 'Ó', 'O'),'Ú', 'U')) AND owhs.\"Inactive\" = 'N') AS F WHERE F.WareName NOT LIKE '%OBSERVADO%' AND F.WareName NOT LIKE '%PROCESO%'"
      self.openConnection()
      self.cursor.execute(sql)
      for row in self.cursor:
          self.selectArray.append(row)
      self.closeCursor()
      return self.selectArray
 
    #obtiene canttidad de rechados de producto en subarea
    def get_fail_product_by_subarea(self, odArticle: str = "", subareaPattern: str = ""):
      self.selectArray.clear()
      sql = f"SELECT F.Stock AS Stock, F.WareCod AS WareCod FROM (SELECT table1.\"WhsCode\" AS WareCod, \"OnHand\" AS Stock FROM SBO_TECNO_PRODUCCION.OITW table1 RIGHT JOIN SBO_TECNO_PRODUCCION.OWHS table2 ON table1.\"WhsCode\" = table2.\"WhsCode\" WHERE \"ItemCode\" = '{odArticle}' AND table2.\"WhsName\" LIKE '%OBSERVADOS%' AND UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(table2.\"WhsName\", 'Á', 'A'), 'É', 'E'), 'Í', 'I'), 'Ó', 'O'), 'Ú', 'U')) LIKE UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('%{subareaPattern}%', 'Á', 'A'), 'É', 'E'), 'Í', 'I'), 'Ó', 'O'), 'Ú', 'U')) GROUP BY table1.\"WhsCode\", \"OnHand\") AS F"
      self.openConnection()
      self.cursor.execute(sql)
      for row in self.cursor:
          self.selectArray.append(row)
      self.closeCursor()
      return self.selectArray

class SLSConnection:
    def __init__(self):
        self.headers = {}
        self.session = requests.Session()
        self.URL_SAP = "https://192.168.5.2:50000/b1s/v1"
        
    def Login(self):
        self.headers.clear()
        login_url = f"{self.URL_SAP}/Login"
        
        payload = {
            "CompanyDB": 'SBO_TECNO_PRODUCCION',
            "UserName": 'manager',
            "Password": '@Cantol20'
        }

        # base de prueba
        # payload = {
        #     "CompanyDB": 'ABKP_TEC_PRUAF_050824',
        #     "UserName": 'manager',
        #     "Password": '@Cantol20'
        # }

        #here should be a try catch
        response = self.session.post(login_url, json=payload, verify=False)
        if (response.status_code == 200):
            self.headers = {
                'Authorization': "Bearer " + response.json()["SessionId"]
                }
            return True
        else:
            return False

    def Logout(self):
        self.headers.clear()
        logout_url = f"{self.URL_SAP}/Logout"
        #AQui tiene que ir un try catch
        response = self.session.post(logout_url, headers=self.headers, verify=False)
        if (response.status_code == 200 or response.status_code == 204):
            return True
        else:
            return False

    def Get_ProductionOrderById(self, poId: str = ""):
        isLogin = self.Login()
        if isLogin:
            PO_B_Id_url = f"{self.URL_SAP}/ProductionOrders({poId})"
            response = self.session.get(PO_B_Id_url, headers=self.headers, verify=False)
            status_code = response.status_code
            response_json = response.json()
            self.Logout()
            if (status_code == 200):
                results = response_json
                return results
            else:
                return {"AbsoluteEntry": "Nothing to show you"}
        else:
            return {"Detail": "Nothing to show you"}

    def Post_ProductionOrder(self, model):
        isLogin = self.Login()
        if isLogin:
            try:
                payload = self.make_OF_Payload(model)
                PO_url = f"{self.URL_SAP}/ProductionOrders"
                response = self.session.post(PO_url, json=payload, headers=self.headers, verify=False)
                status_code = response.status_code
                response_json = response.json()
                self.Logout()
                if (status_code == 200 or status_code == 201):
                    return status_code, obj({"DocumentNumber": response_json["DocumentNumber"]})
                else:
                    return 500, {"DocumentNumber": "XXXXXX"}
            except Exception as error:
                self.Logout()
                print(f"An error ocurred:{error}")
                return 500, {"DocumentNumber": "XXXXXX"}
        else:
            return 500, {"DocumentNumber": "XXXXXX"}

    def make_OF_Payload(self, model):
        ProductionOrderLines = [{"ItemNo": x.ItemNo, "PlannedQuantity": x.PlannedQuantity, "ProductionOrderIssueType": x.ProductionOrderIssueType, "Warehouse": x.Warehouse, "DistributionRule": x.DistributionRule, "DistributionRule2": x.DistributionRule2, "ItemType": x.ItemType, "StartDate": x.StartDate, "EndDate": x.EndDate, "ItemName": x.ItemName} for x in model.ProductionOrderLines if bool(x)]
        payload = {
        "ItemNo": model.ItemCode,
        "ProductionOrderStatus": "boposPlanned",
        "ProductionOrderType": "bopotSpecial",
        "PlannedQuantity": model.PlannedQty,
        "PostingDate": model.PostDate,
        "DueDate": model.DueDate,
        "ProductionOrderOrigin": "bopooManual",
        "Warehouse": model.Warehouse,
        "InventoryUOM": "UND",
        "StartDate": model.StartDate,
        "ProductDescription": model.ProdName,
        "U_MSS_USESWC": model.Usuario_Codigo_SIM,
        "U_MSS_REDSWC": "Y",
        "ProductionOrderLines": ProductionOrderLines
        }
        return payload