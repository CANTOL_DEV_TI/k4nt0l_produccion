import pymssql


class conexion_mssql:
    def __init__(self):
        self.conMsql = pymssql.connect(
            server="192.168.5.4", user="sa", password="Cantol123@abc", database="Usuarios_CANTOL"
        )
        self.curSql = self.conMsql.cursor()

    def consultas_sgc(self, vSql, vValores):
        resultado = []
        with self.curSql as cursor:
            cursor.execute(vSql, vValores)
            columns = [column[0] for column in cursor.description]
            for row in cursor.fetchall():
                resultado.append(dict(zip(columns, row)))

        self.cerrando()
        return resultado

    def ejecutar_funciones(self, vSql, vValores):
        try:
            self.curSql.execute(vSql, vValores)
            self.conMsql.commit()
            self.cerrando()
            out = {"Mensaje": "procesado", "Error": ""}
        except Exception as err:
            out = {"Mensaje": "", "Error": f"{err=}"}
        return out

    def ejecutar_funciones_multi(self, vSql, vValores):
        try:
            self.curSql.executemany(vSql, vValores)
            self.conMsql.commit()
            self.cerrando()
            out = {"Mensaje": "procesado", "Error": ""}
        except Exception as err:
            out = {"Mensaje": "", "Error": f"{err=}"}
        return out

    def ejecutar_funciones_CabDet(self, vSqlCab, vValoresCab, vSqlDet, vValoresDet):
        try:
            self.curSql.execute(vSqlCab, vValoresCab)
            self.curSql.executemany(vSqlDet, vValoresDet)
            self.conMsql.commit()
            self.cerrando()
            out = {"Mensaje": "procesado", "Error": ""}
        except Exception as err:
            self.cerrando()
            out = {"Mensaje": "", "Error": f"{err=}"}
        return out

    def ejecutar_store_crud(self, vSql, vValores):
        try:
            self.curSql.callproc(vSql, vValores)
            self.conMsql.commit()
            self.cerrando()
            out = {"Mensaje": "procesado", "Error": ""}
        except Exception as err:
            out = {"Mensaje": "", "Error": f"{err=}"}
        return out

    def cerrando(self):
        self.curSql.close()
        self.conMsql.close()



    def ejecutar_ConRetornoPK(self,vSql, vValores, tabla):
        try:
            self.curSql.execute(vSql, vValores)
            out = self.curSql.fetchall()[0][0]
            self.conMsql.commit()

        except Exception as err:
            out = False

        return out

    def ejecutar_SinRetornoPK(self,vSql, vValores):
        try:
            self.curSql.execute(vSql, vValores)
            self.conMsql.commit()
            out = True
        except Exception as err:
            out = False

        return out
