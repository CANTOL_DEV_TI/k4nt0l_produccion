from pydantic import BaseModel
from typing import Optional
from datetime import date




class cuotaDetalle(BaseModel):
    cod_zona: Optional[str] = None
    nom_zona: Optional[str] = None
    cuota_porcent: Optional[float] = None
    cuota_soles: Optional[float] = None
    nom_vendedor: Optional[str] = None
    cod_vendedor: Optional[int] = None
class PlanVenta(BaseModel):
    id: Optional[int] = None
    user_date:Optional[str] = None
    user_id:Optional[str] = None
    pr_name:Optional[str] = None
    pr_total:Optional[float] = None
    pr_year:Optional[str] = None
    pr_month:Optional[str] = None
    sc_id:Optional[str] = None
    sc_name:Optional[str] = None
    detalle_cuota:Optional[list[cuotaDetalle]] = None



