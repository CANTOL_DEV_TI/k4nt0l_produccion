from pydantic import BaseModel
from typing import Optional


class DIS_DETCUOCollection(BaseModel):
    Code:str
    LineId: Optional[str] 
    Object: str = 'CUOTA'
    LogInst: Optional[str] 
    U_DIS_CODEN:str
    U_DIS_NOMVEN:str
    U_DIS_ZONCLI: str
    U_DIS_NOMZON:str
    U_DIS_CUOZON: float
    U_DIS_CANAL:str
    U_DIS_COMRAN:str
    U_DIS_DESCTO:Optional[float] 
    U_DIS_DTOFAC:Optional[float] 
    U_DIS_DTONCR:Optional[float] 
    U_DIS_DTOPRO:Optional[float] 


class GrabarCuota(BaseModel):
    Code: str
    Name: str
    DocEntry: Optional[str] 
    Canceled:str = 'N'
    Object: str = 'CUOTA'
    LogInst:Optional[str]
    UserSign:Optional[str] 
    Transfered:str = 'N'
    CreateDate:Optional[str] 
    CreateTime:Optional[str] 
    UpdateDate:Optional[str]
    UpdateTime:Optional[str] 
    DataSource:str = 'I'
    U_DIS_FECINI:Optional[str]
    U_DIS_FECFIN:Optional[str]
    DIS_DETCUOCollection:list[DIS_DETCUOCollection]