from fastapi import APIRouter
from fastapi.responses import RedirectResponse
from comercial.functions import cuotas
from comercial.models.cuotaModel import PlanVenta
from comercial.models.grabarModel import GrabarCuota
from comercial.utils.math import truncate_number
from fastapi.responses import JSONResponse
from typing import List
from fastapi import FastAPI, Query


ruta = APIRouter(
    prefix='/cuotas',
    tags=["ventas"]
)

@ruta.get("/plan_venta_canal")
def get_plan_venta_canal(canal_id: int = None):
    data = cuotas.get_plan_venta_canal(canal_id)
    return list(map(lambda y: {'id': y[0], 'plan': y[3], 'total': y[4], 'ejercicio': y[5], 'periodo': y[6], 'canal_id': y[7], 'habilitado': y[9]}, data)) if bool(data) else None
    # return None if bool(data) else list(map(lambda x: x.__dict__, data))

@ruta.get("/canales_venta")
def get_canal_venta():
    data = cuotas.get_canal_venta()
    return list(map(lambda y: {'canal_code': y[0], 'canal_name': y[1]}, data)) if bool(data) else None

@ruta.get("/")
def update_habilitar_estado(periodo: str = None, canal_id: str = None):
    data = cuotas.get_cuotas(periodo=periodo, canal_id=canal_id)
    return list(map(lambda y: {'vendedor_id': y[0], 'vendedor': y[1], 'porcentaje': truncate_number(y[2], 2), 'zona_cuota': y[3]}, data)) if bool(data) else None

@ruta.patch("/habilitar")
def update_habilitar_estado(PlanVenta: PlanVenta):
    print("Llega a esta parte")
    data = cuotas.update_state(PlanVenta.id)
    return {
        "habilitado": bool(data)
    }

@ruta.post("/crear_plan_cuota")
def  crear_plan_cuota(planventa: PlanVenta): 
    datalayer = cuotas.grabar_cuota(planventa)
    print("pintame datalayer:",datalayer)
    if datalayer in (200,201):
        data = cuotas.insertar_plan_cuota( planventa)
        return {
            "message": data,
            "grabando SAP": datalayer
        }
    else:
        # raise HTTPException(status_code=500, detail="Error")
        return JSONResponse(
            status_code=500,
            content={"error": "Error al grabar e insertar la cuota", "status":500}
        )

@ruta.get("/listarzona_vendedor")
def  listar_zona_vendedor(canal_id: str = None):
    data = cuotas.get_zona_vendedor(canal_id)
    return data

@ruta.get("/validar_cuota_canal")
def validar_cuota_canal(anio: str = None, mes: str = None, canal: str = None):
    data = cuotas.get_cuota_periodo(anio, mes, canal)
    return {
            "message": data,
    }

@ruta.get("/zona_vendedor_defecto")
def listar_zonadefecto_vendedor(lista_codigo: List[str] = Query(...)):
    print("pintame lista:",lista_codigo)
    data = cuotas.get_zona_vendedor_defecto(lista_codigo) 
    return {
            "message": data,
    }


