from fastapi import APIRouter
from comercial.routes import  cuotas
from comercial.routes import ventas

ruta = APIRouter(
    prefix='/comercial'
)

ruta.include_router(cuotas.ruta)
ruta.include_router(ventas.ruta)