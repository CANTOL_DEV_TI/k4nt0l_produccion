from fastapi import APIRouter
from fastapi.responses import RedirectResponse
from comercial.functions import ventas
from comercial.models.cuotaModel import PlanVenta
from comercial.models.grabarModel import GrabarCuota
from comercial.utils.math import truncate_number
from fastapi.responses import JSONResponse
from typing import List
from fastapi import FastAPI, Query


ruta = APIRouter(
    prefix='/ventas',
    tags=["ventas"]
)

@ruta.get("/vendedores")
# def obtener_ferreteria_vendedores(canal_id: int = None):
def obtener_ferreteria_vendedores():
    # data = ventas.get_vendors(canal_id)
    data = ventas.get_vendors()
    return list(map(lambda x: {"code_v": x[0], "name_v": x[1]}, data))

@ruta.get("/pedidos")
def obtener_pedidos_por_vendedor(code_v: str = None, a_comercial: str = 'T', a_creditos: str = 'T'):
    data = ventas.get_sales_month(code_v, a_comercial, a_creditos)
    return list(map(lambda x: { "c_mobile": x[0], 
                                "doc_date": x[1].strftime('%d-%m-%Y') if bool(x[1]) else None,
                                "doc_num": x[2],
                                "card_code": x[3],
                                "card_name": x[4],
                                "doc_total": x[5],
                                "v_name": x[6],
                                "city": x[7],
                                "address": x[8],
                                "cat_dsct": x[9],
                                "a_comercial": x[10],
                                "a_creditos": x[11],
                                "a_cancelado": x[12],
                                "c_pago": x[13],
                                }, data))