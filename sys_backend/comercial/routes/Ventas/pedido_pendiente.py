from fastapi import APIRouter, HTTPException
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from conexiones.conexion_sql_solo import ConectaSQL_Seguridad
from conexiones.conexion_sap_solo import conexion_SAP_Distri
#from .models.statusModel import Pedido
# from ...models.statusModel import Pedido


ruta = APIRouter(
    prefix='/pedido',
    tags=["ventas"]
)

def get_connection_sql_usuario():
    return ConectaSQL_Seguridad()

def get_connection_sql():
    return ConectaSQL_Produccion()

def get_connection_sap():
    return conexion_SAP_Distri()



@ruta.get("/listarpedidopendiente")
def listarpedidoPendiente(usuario_codigo: str):
    connection_user = None
    cursor_user = None
    connection = None
    cursor = None
    
    try:
        # First connection: to get SlpCode from the user table
        connection_user = get_connection_sql_usuario()
        cursor_user = connection_user.cursor()
        sql_user = f"SELECT * FROM tUsuarios WHERE Usuario_Codigo = '{usuario_codigo}';"
        cursor_user.execute(sql_user)
        rs_user = cursor_user.fetchone()

        if not rs_user or rs_user[7] is None:
            raise HTTPException(status_code=401, detail="Unauthorized")

        SlpCode = rs_user[7]

        # Second connection: to get the list of orders based on SlpCode
        connection = get_connection_sap()
        cursor = connection.cursor()
        sql = f'''SELECT "LicTradNum", "CardName", "DocDueDate", "DocTotal"
                  FROM sbo_distri_produccion.ODRF 
                  WHERE ("U_MSSC_COM" = 'P' OR "U_MSSC_CRE" = 'P')  
                  AND "SlpCode" = {SlpCode}
                  AND "DocStatus" = 'O'
                  AND "U_MSSM_CRM" = 'Y'
                  ORDER BY "DocEntry" DESC;'''
    
        cursor.execute(sql)
        rs = cursor.fetchall()
        lista = []

        for row in rs:
            listaDic = {                    
                "LicTradNum": row[0],
                "CardName": row[1],
                "DocDueDate": row[2],
                "DocTotal": row[3]
            }
            lista.append(listaDic)

        return lista
    
    except HTTPException as http_exc:
        raise http_exc
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Ocurrió un error al obtener los datos del cliente: {str(e)}")
    
    finally:
        # Cerrar la conexión a la base de datos, cerrando en el orden inverso al que se abrieron
        if cursor is not None:
            cursor.close()
        if connection is not None:
            connection.close()
        if cursor_user is not None:
            cursor_user.close()
        if connection_user is not None:
            connection_user.close()


