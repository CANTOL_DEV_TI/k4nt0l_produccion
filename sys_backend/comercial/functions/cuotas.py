
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from conexiones.conexion_sap_solo import conexion_SAP_Prueba
import pymssql
import requests
from comercial.functions.config import serverBD


def get_connection_sql():
    return ConectaSQL_Produccion()

#base prueba
def get_connection_sap():
    return conexion_SAP_Prueba()

def get_plan_venta_canal(canal_id: int = None):
    try:
        conexion_Prod = get_connection_sql()
        cursor_Prod = conexion_Prod.cursor()
        sql_plan_venta = f"SELECT * FROM pv_cuotas_cab WHERE sc_id = %s ORDER BY pr_id DESC;" % (canal_id)
        cursor_Prod.execute(sql_plan_venta)
        rs_plan_venta = cursor_Prod.fetchall()
        return rs_plan_venta
    except pymssql.OperationalError as e:
        print("Operational error: ", e)
        return False
    except pymssql.ProgrammingError as e:
        print("Programming error: ", e)
        return False
    except pymssql.IntegrityError as e:
        print("Integrity error: ", e)
        return False
    except pymssql.DataError as e:
        print("Data error: ", e)
        return False
    except Exception as e:
        print("An error occurred: ", e)
        return False

def get_canal_venta():
    try:
        connection = get_connection_sap()
        cursor = connection.cursor()
        sql_plan_venta = f"SELECT * FROM {serverBD}.SWC_CanalesVenta"
        cursor.execute(sql_plan_venta)
        rs_plan_venta = cursor.fetchall()
        return rs_plan_venta[1:]
    except pymssql.OperationalError as e:
        print("Operational error: ", e)
        return False
    except pymssql.ProgrammingError as e:
        print("Programming error: ", e)
        return False
    except pymssql.IntegrityError as e:
        print("Integrity error: ", e)
        return False
    except pymssql.DataError as e:
        print("Data error: ", e)
        return False
    except Exception as e:
        print("An error occurred: ", e)
        return False

def get_cuotas(periodo: str = None, canal_id: str = None):
    try:

        periodo = periodo
        year, month = periodo.split('-')
        # Now, year contains '2025' and month contains '01'
        print(f"Year: {year}, Month: {month}")

        connection = get_connection_sql()
        cursor = connection.cursor()
        # sql_cuotas = f"""SELECT A, B, ((C/(SELECT sum(P."U_DIS_CUOZON") FROM {serverBD}."@DIS_DETCUO" AS P WHERE P."U_DIS_CANAL" = '{canal_id}' AND P."Code" = '{periodo}'))*100), C FROM 
        #                 (SELECT K."U_DIS_CODEN" AS A, K."U_DIS_NOMVEN" AS B, (SELECT SUM(J."U_DIS_CUOZON") 
        #                 FROM {serverBD}."@DIS_DETCUO" AS J WHERE J."U_DIS_CODEN" = K."U_DIS_CODEN" 
        #                 AND "U_DIS_CANAL" = K."U_DIS_CANAL" AND "Code" = K."Code") AS C FROM {serverBD}."@DIS_DETCUO" AS K WHERE K."U_DIS_CANAL" = '{canal_id}' AND K."Code" = '{periodo}') AS D;"""

        sql_cuotas = f"""select d.cod_vendedor as vendedor_id, d.nom_vendedor  as vendedor, 
                                d.cuota_porcent as porcentaje, 
                                d.cuota_soles as zona_cuota from pv_cuotas_cab c
                                inner join pv_cuotas_det d
                                on c.pr_id = d.pr_id
                                where c.pr_year = {year}
                                and pr_month = '{month}'
                                and sc_id = '{canal_id}'
                                """
        print("pintame cuotas pruebas:",sql_cuotas)

        cursor.execute(sql_cuotas)
        rs_cuotas = cursor.fetchall()
        return rs_cuotas
    except pymssql.OperationalError as e:
        print("Operational error: ", e)
        return False
    except pymssql.ProgrammingError as e:
        print("Programming error: ", e)
        return False
    except pymssql.IntegrityError as e:
        print("Integrity error: ", e)
        return False
    except pymssql.DataError as e:
        print("Data error: ", e)
        return False
    except Exception as e:
        print("An error occurred: ", e)
        return False



def update_state(idPlan):
    try:
        conexion_Prod = get_connection_sql()
        cursor_Prod = conexion_Prod.cursor()
        sql_plan_venta = f"UPDATE pv_cuotas_cab SET isEnabled = (CASE WHEN isEnabled = 1 THEN 0 ELSE 1 END) WHERE pr_id = {str(idPlan)};"
        cursor_Prod.execute(sql_plan_venta)
        conexion_Prod.commit()
        #obtener el estado del campo
        fetch_query = f"SELECT isEnabled FROM pv_cuotas_cab WHERE pr_id = %s;"
        cursor_Prod.execute(fetch_query, (str(idPlan),))
        # Fetch the updated value
        updated_value = cursor_Prod.fetchone()
        return updated_value[0]

    except pymssql.OperationalError as e:
        print("Operational error: ", e)
        return False
    except pymssql.ProgrammingError as e:
        print("Programming error: ", e)
        return False
    except pymssql.IntegrityError as e:
        print("Integrity error: ", e)
        return False
    except pymssql.DataError as e:
        print("Data error: ", e)
        return False
    except Exception as e:
        print("An error occurred: ", e)
        return False
    
def insertar_plan_cuota(planventa):
    try:
        print("pintame planventa xd:",planventa)
        #print("pintame detalle_cuota= xd:",planventa.detalle_cuota)
        conexion_Prod = get_connection_sql()
        cursor_Prod = conexion_Prod.cursor()
        msg = ""

        
        sqlinsertCabecera = f""" insert into pv_cuotas_cab (user_date ,user_id ,pr_name ,pr_total, pr_year, pr_month, sc_id, sc_name) 
                                                        values (                                               
                                                        '{planventa.user_date}',
                                                        '{planventa.user_id}',
                                                        '{planventa.pr_name}',
                                                        {planventa.pr_total},
                                                        '{planventa.pr_year}',
                                                        {planventa.pr_month},
                                                        '{planventa.sc_id}',
                                                        '{planventa.sc_name}'                                                               
                                                        );"""
        print("sqlinsertCabecera:",sqlinsertCabecera)
        cursor_Prod.execute(sqlinsertCabecera)

        # Obtén el ID generado en `pv_cuotas_cab`
        cabecera_id = cursor_Prod.lastrowid
        conexion_Prod.commit()

        print("pintame detalle cuota:",planventa.detalle_cuota)
        for detalle in planventa.detalle_cuota:
            print("pintame detalle unico:",detalle)
            
        
            # sqlinsertDetalle = f"""insert into pv_cuotas_det (pr_id,nom_zona,cuota_porcent,cuota_soles,nom_vendedor,cod_vendedor) 
            #                                                 values (
            #                                                 {cabecera_id},
            #                                                 '{detalle.nom_zona}',
            #                                                 {detalle.cuota_porcent},
            #                                                 {detalle.cuota_soles},
            #                                                 '{detalle.nom_vendedor}',
            #                                                 '{detalle.cod_vendedor}'
            #                                                 );"""
            
            sql_insert_detalle = """
                                    INSERT INTO pv_cuotas_det (pr_id, nom_zona, cuota_porcent, cuota_soles, nom_vendedor, cod_vendedor)
                                    VALUES (%s, %s, %s, %s, %s, %s);                                    
                                """
            valores = (
                cabecera_id,
                detalle.nom_zona,
                detalle.cuota_porcent,
                detalle.cuota_soles,
                detalle.nom_vendedor,
                detalle.cod_vendedor
            )
            
            print("sqlinsertDetalle:",sql_insert_detalle)
            cursor_Prod.execute(sql_insert_detalle, valores)
            conexion_Prod.commit()



            msg = "cuota registrada correctamente"
        

        return msg

      
    except Exception as e:
        print("Error al insertar detalle: ", e)
        return f"Error: {str(e)}"
    finally:
        if cursor_Prod:
            cursor_Prod.close()
        if conexion_Prod:
            conexion_Prod.close()

def get_zona_vendedor(canal_id):
    try:
        zona = obtener_zona()
        vendedor = obtener_vendedor(canal_id)

        listaDic = {
            "zona": zona,
            "vendedor": vendedor
        }
        
        return listaDic

    except Exception as e:
        print("Error al obtener zona vendedor: ", e)
        return f"Error: {str(e)}"
   

def obtener_zona():
    try:
        connection = get_connection_sap()
        cursor = connection.cursor()
        sql_zona = f'''SELECT * FROM {serverBD}."@MSSC_ZONA"'''
        print("pintame zona:",sql_zona)
        cursor.execute(sql_zona)
        rs_zona = cursor.fetchall()
        zona_list = []

        for row in rs_zona:
            dic_Zona = {
                "id": row[0] if row is not None else 0 ,
                "nombre":  row[1] if row is not None else ''
            }

            zona_list.append(dic_Zona)

        return zona_list


    except Exception as e:
        print("Error al obtener zona vendedor: ", e)
        return f"Error: {str(e)}"
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

def obtener_vendedor(canal_id):
    try:
        connection = get_connection_sap()
        cursor = connection.cursor()
        sql_vendedor = f"""SELECT * FROM {serverBD}.OSLP WHERE U_DIS_CANAL = '{canal_id}' AND "Active" = 'Y'"""
        print("pintame zona:",sql_vendedor)
        cursor.execute(sql_vendedor)
        rs_vendedor = cursor.fetchall()
        vendedor_list = []

        for row in rs_vendedor:
            print("pintame row:",row)
            dic_Vendedor = {
                "id": row[0] if row is not None else 0 ,
                "nombre":  row[1] if row is not None else ''
            }

            vendedor_list.append(dic_Vendedor)

        print("pintame vendedor:", vendedor_list)

        return vendedor_list


    except Exception as e:
        print("Error al obtener zona vendedor: ", e)
        return f"Error: {str(e)}"
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()    

def grabar_cuota(cabecera):
    try:
        if cabecera:
            periodo = str(cabecera.pr_year) + "-" +str(cabecera.pr_month)
            print("pintame periodo:",periodo)
            
            listaDetalle = []
            for planventa in cabecera.detalle_cuota:
                print("entras a mi for:", planventa)

                listaDicDetalle = {
                    "Code": periodo,
                    "U_DIS_CODEN": planventa.cod_vendedor,
                    "U_DIS_NOMVEN": planventa.nom_vendedor,
                    "U_DIS_ZONCLI": planventa.cod_zona,
                    "U_DIS_NOMZON": planventa.nom_zona,
                    "U_DIS_CUOZON": planventa.cuota_soles,
                    "U_DIS_CANAL": cabecera.sc_id
                }

                listaDetalle.append(listaDicDetalle)
            
            existe_periodo = validar_periodo(periodo)
            print("¿existe periodo?", existe_periodo)

            if existe_periodo:

                url = f"https://192.168.5.2:50000/b1s/v1/CUOTA('{periodo}')"
                session_id = generartoken()
                print("pintame session id:",session_id)
                headers = {
                    "Content-Type": "application/json",
                    "Cookie": f"B1SESSION={session_id}"
                }

                json_data = {
                "Code": periodo,
                "DIS_DETCUOCollection":listaDetalle
                }

                print("pintame json data:",json_data)
                response = requests.patch(url, json=json_data, headers=headers, verify=False)
                print("muestame response grabado:", response)

                if response.status_code in [200, 201, 204]:
                    logoutservicelayer(session_id)
                    return 201

                else:
                    print("Error al cerrar documento:", response)
                    logoutservicelayer(session_id)
                    return 500
            else:

                url = "https://192.168.5.2:50000/b1s/v1/CUOTA"
                session_id = generartoken()
                print("pintame session id:",session_id)
                headers = {
                    "Content-Type": "application/json",
                    "Cookie": f"B1SESSION={session_id}"
                }

                json_data = {
                "Code": periodo,
                "DIS_DETCUOCollection":listaDetalle
                }

                print("pintame json data:",json_data)
                response = requests.post(url, json=json_data, headers=headers, verify=False)
                print("muestame response grabado:", response.json())

                if response.status_code in [200, 201, 204]:
                    logoutservicelayer(session_id)
                    return 200

                else:
                    print("Error al cerrar documento:", response)
                    logoutservicelayer(session_id)
                    return 500

    
    except requests.exceptions.RequestException as e:
        print("Error al cerrar documento:", e)
        return f"Error: {str(e)}"                   

def generartoken():
    url = 'https://192.168.5.2:50000/b1s/v1/Login'

    payload = {
        "CompanyDB": f"{serverBD}",
        "Password": "@Cantol20",
        "UserName": "manager"
    }

    headers = {
        "Content-Type": "application/json"
    }

    try:
        response = requests.post(url, json=payload, headers=headers, verify=False)
        print("status:",response)
        response.raise_for_status()
        

        if response.status_code == 200:
            session_info = response.json()
            return  session_info.get("SessionId")
        else:
            print("Error en la autenticacion:", response.status_code, response.text)
    except requests.exceptions.RequestException as e:
        print("Error en la solicitud:", e)   

def logoutservicelayer(session_id):
    url = "https://192.168.5.2:50000/b1s/v1/Logout"
    
    headers  = {
        "Content-Type": "application/json",
        "Cookie": f"B1SESSION={session_id}"
    }

    try:
        response = requests.post(url, headers=headers, verify=False)
        print("pintame response logout:", response)

        if response.status_code in [200,204]:
            print("Sesion cerrada exitosamente")
            return True
        else:
            print(f"Error logging out: {response.status_code}")
            return False
    except requests.exceptions.RequestException as e:
        print(f"Error logging out: {e}")
        return False 

def get_cuota_periodo(anio, mes, canal):
    try:
        connection = get_connection_sql()
        cursor = connection.cursor()
        sql_cuota = f"""select * from pv_cuotas_cab where pr_year = '{anio}'  and pr_month = '{mes}' and sc_id = '{canal}'"""
        cursor.execute(sql_cuota)
        rs = cursor.fetchone()
        status = False

        if rs is not None:
            status = True
        else:
            status = False

        return status

    except Exception as e:
        print("Error al obtener zona vendedor: ", e)
        return f"Error: {str(e)}"
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

def get_zona_vendedor_defecto(lista_codigo):
    print("give me break:", lista_codigo)
    try:
        connection = get_connection_sap()
        cursor = connection.cursor()
        codigos_str = ", ".join(f"'{codigo}'" for codigo in lista_codigo)
        sql_cuota = f"""SELECT "SlpCode" AS codigo, "U_MSSC_ZCLI" AS zona FROM SBO_DISTRI_PRODUCCION."OSLP" WHERE "SlpCode" 
                        in ({codigos_str}) """                                 
        cursor.execute(sql_cuota)
        rs = cursor.fetchall()
        print("pintame rs:",rs)
        listazonas = []
        for row in rs:
            print("pintame row:",row)
            listaDic = {
                "codigo": row[0],
                "zona": row[1]
            }

            listazonas.append(listaDic)
        return listazonas

    except Exception as e:
        print("Error al obtener zona vendedor: ", e)
        return f"Error: {str(e)}"
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

def obtener_cuota_periodo(code):
    url = f"https://192.168.5.2:50000/b1s/v1/CUOTA('{code}')"
    session_id = generartoken()

    headers = {
        "Content-Type": "application/json",
        "Cookie": f"B1SESSION={session_id}"
    }

    try:
        response  = requests.get(url, headers=headers, verify=False)
        print("muestrame:",response.json()['DIS_DETCUOCollection'])

        if response.status_code in [200, 201, 204]:
            print("retornar detalle",response.json()['DIS_DETCUOCollection'])
            detalle_cuota = response.json()['DIS_DETCUOCollection']
            return detalle_cuota
        else:
            print(f"Error en la solicitud: {response.status_code}, {response.text}")
            return {"error": f"Error en la solicitud, código de estado: {response.status_code}", "detalle": response.text}
  
    except Exception as e:
        print("Error de conexión al obtener cuota:", str(e))
        return {"error": "Error de conexión al obtener cuota", "detalle": str(e)}
    
def validar_periodo(periodo):
    try:
        connection = get_connection_sap()
        cursor = connection.cursor()
        sqlPeriodo = f"""SELECT *FROM {serverBD}."@DIS_CABCUO" WHERE "Code" = '{periodo}'"""
        cursor.execute(sqlPeriodo)
        rs = cursor.fetchone()

        if rs is not None:
            return True
        else:
            return False
    except Exception as e:
        print("Error al validar periodo:", str(e))
        return f"Error: {str(e)}"
    
def eliminar_cuotas(code):
    url = f"https://192.168.5.2:50000/b1s/v1/CUOTA('{code}')"
    session_id = generartoken()

    headers = {
        "Content-Type": "application/json",
        "Cookie": f"B1SESSION={session_id}"
    }

    try:
        response = requests.delete(url, headers=headers, verify=False)

        if response.status_code in [200, 201, 204]:
            print("Eliminación exitosa")
            return True
        else:
            print("Error al eliminar la cuota",response.json())
            return False
    except Exception as e:
        print("Excepción al intentar eliminar:", str(e))
        return False

        

                         