
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from conexiones.conexion_sap_solo import conexion_SAP_Prueba
import pymssql
import requests
from comercial.functions.config import serverBD


def get_connection_sql():
    return ConectaSQL_Produccion()

#base prueba
def get_connection_sap():
    return conexion_SAP_Prueba()

def get_vendors():
    try:
        connection = get_connection_sap()
        cursor = connection.cursor()
        sql_plan_venta = f"""SELECT "SlpCode", "SlpName" 
                       FROM {serverBD}.OSLP 
                       WHERE "U_DIS_CANAL" = '01'
                       ORDER BY "SlpName" ASC;"""
        cursor.execute(sql_plan_venta)
        rs_plan_venta = cursor.fetchall()
        return rs_plan_venta
        # return rs_plan_venta[1:]
    except pymssql.OperationalError as e:
        print("Operational error: ", e)
        return False
    except pymssql.ProgrammingError as e:
        print("Programming error: ", e)
        return False
    except pymssql.IntegrityError as e:
        print("Integrity error: ", e)
        return False
    except pymssql.DataError as e:
        print("Data error: ", e)
        return False
    except Exception as e:
        print("An error occurred: ", e)
        return False

def get_sales_month(code_v, a_comercial, a_creditos):
    try:
        connection = get_connection_sap()
        cursor = connection.cursor()
        
        query = f""" SELECT 
                        "U_MSSM_CLM" AS CLAVE_MOBILE,
                        "DocDate" AS F_DOCUMENTO,
                        "DocNum" AS NRO_DOCUMENTO,
                        or_."CardCode" AS COD_CLIENTE,
                        "CardName" AS CLIENTE,
                        "DocTotal" AS TOTAL,
                        CV."Name" AS VENDEDOR,
                        "County" AS CIUDAD,
                        "Street" AS DIR_ENTREGA,
                        "U_DST_DESOTO" AS DSCT_CATEGORIA,
   						CASE
                        	WHEN "U_MSSC_COM" = 'P' THEN 'Pendiente'
                        	WHEN "U_MSSC_COM" = 'A' THEN 'Aprobado'
                        	ELSE 'Rechazado'
                        END AS APROBADO_COM,
                        CASE
                        	WHEN "U_MSSC_CRE" = 'P' THEN 'Pendiente'
                        	WHEN "U_MSSC_CRE" = 'A' THEN 'Aprobado'
                        	ELSE 'Rechazado'
                        END AS APROBADO_CRED,
                        CASE
                        	WHEN "CANCELED" = 'N' THEN 'No'
                        	WHEN "CANCELED" = 'Y' THEN 'Si'
                        	ELSE 'No definido'
                        END AS CANCELADO,
                        oct_."PymntGroup" AS COD_PAGO
                    FROM 
                        SBO_DISTRI_PRODUCCION.ORDR or_ 
                    INNER JOIN 
                        SBO_DISTRI_PRODUCCION."@MSSM_CVE" CV 
                        ON or_."SlpCode" = CV."Code"
                    INNER JOIN
                        SBO_DISTRI_PRODUCCION.CRD1 CR1
                        ON or_."CardCode" = CR1."CardCode"
                    INNER JOIN 
                        SBO_DISTRI_PRODUCCION.OCTG oct_
                        ON or_."GroupNum" = oct_."GroupNum"
                    WHERE 
                        "DocDate" >= '2025-02-01' AND
                        CR1."Address" = or_."ShipToCode" """
                    
        query += f"""AND or_."SlpCode" = {str(code_v)} """ if bool(code_v) else ""
        query += f"""AND or_."U_MSSC_COM" = '{a_comercial}' """ if a_comercial != 'T' else ""
        query += f"""AND or_."U_MSSC_CRE" = '{a_creditos}' """ if a_creditos != 'T' else ""
        query += f"""AND or_."DocStatus" = 'O' """
        query += """ORDER BY or_."DocDate" DESC, or_."DocEntry" DESC;"""

        cursor.execute(query)
        rs_plan_venta = cursor.fetchall()
        return rs_plan_venta
        # print(rs_plan_venta)
        # return rs_plan_venta[1:]
    except pymssql.OperationalError as e:
        print("Operational error: ", e)
        return False
    except pymssql.ProgrammingError as e:
        print("Programming error: ", e)
        return False
    except pymssql.IntegrityError as e:
        print("Integrity error: ", e)
        return False
    except pymssql.DataError as e:
        print("Data error: ", e)
        return False
    except Exception as e:
        print("An error occurred: ", e)
        return False