import math

def truncate_number(number, decimal_places):
    factor = 10 ** decimal_places
    return math.floor(number * factor) / factor