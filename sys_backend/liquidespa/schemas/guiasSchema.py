from typing import Optional
from pydantic import BaseModel
from datetime import date

class guias_Schema(BaseModel):
    DocEntry : Optional[str]
    DocNum : Optional[str]
    DocDate : Optional[date]
    DocDueDate : Optional[date]
    CardCode : Optional[str]
    CardName : Optional[str]
    U_MSSL_NCD : Optional[str]
    U_MSSL_LCD : Optional[str]
    U_MSSL_PVH : Optional[str]
    U_MSS_PRFI : Optional[str]
    U_MSS_NUFI : Optional[str]
    U_MSS_PLN : Optional[str]
    U_MSS_FGTO : Optional[str]
    U_MSS_ESTRA : Optional[str]
    U_MSS_ESLI : Optional[str]
    U_MSS_ESTLIQ : Optional[str]
    U_MSS_MONLIQ : Optional[float]
    U_MSS_MONEDA : Optional[str]
    U_MSS_NUMOP : Optional[str]
    U_MSS_TIPAG : Optional[str]
    U_MSS_OBSE : Optional[str]

    class Config:
        orm_mode = True