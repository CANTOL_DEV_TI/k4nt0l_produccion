from typing import Optional
from pydantic import BaseModel

class chofer_Schema(BaseModel):    
    Brevete : Optional[str]
    Documento : Optional[str]
    Chofer_Nombre : Optional[str]
    Chofer_Apellido : Optional[str]
    
    class Config:
        orm_mode = True