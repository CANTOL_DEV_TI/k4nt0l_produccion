from typing import Optional
from pydantic import BaseModel
from datetime import date
from liquidespa.schemas.guiasSchema import guias_Schema



class guiafiltro_Schema(BaseModel):
    
    Desde : Optional[date]
    Hasta : Optional[date]
    Chofer : Optional[str]
    Guias : Optional[list[guias_Schema]]
    
    class Config:
        orm_mode = True