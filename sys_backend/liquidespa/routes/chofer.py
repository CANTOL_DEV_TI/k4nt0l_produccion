from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db

from liquidespa.schemas.choferesSchema import chofer_Schema
from liquidespa.functions import choferFunction


ruta = APIRouter(
    prefix="/choferes",tags=["choferes"]
)

@ruta.get('/listado', status_code=status.HTTP_200_OK)
async def ListadoGuias():
    #print(filtros)
    resultado = choferFunction.ListadoChoferes()    
    return  resultado