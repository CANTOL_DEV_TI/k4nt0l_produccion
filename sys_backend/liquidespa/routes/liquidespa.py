from fastapi import APIRouter

from liquidespa.routes import guias,chofer

ruta = APIRouter(
    prefix="/liquidespa",
)

ruta.include_router(guias.ruta)
ruta.include_router(chofer.ruta)
