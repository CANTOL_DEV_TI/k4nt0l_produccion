from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db

from liquidespa.schemas.guiasSchema import guias_Schema
from liquidespa.schemas.guiasfiltroSchema import guiafiltro_Schema
from liquidespa.functions import guiasFunction


ruta = APIRouter(
    prefix="/guias_liquidacion",tags=["Guias_Liquidacion"]
)

@ruta.post('/listado', status_code=status.HTTP_200_OK)
async def ListadoGuias(filtros: guiafiltro_Schema):
    #print(filtros)
    resultado = guiasFunction.ListadoGuias(filtros)    
    return  resultado

@ruta.post('/actualizacionguias',status_code=status.HTTP_200_OK)
async def Actualizacion(pGuias:guiafiltro_Schema):
    resultado = guiasFunction.UpdGuias(pGuias)
    return resultado