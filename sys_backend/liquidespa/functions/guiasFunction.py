from conexiones.conexion_sql_solo import ConectaSQL_GDH
from liquidespa.schemas.guiasSchema import guias_Schema
from liquidespa.schemas.guiasfiltroSchema import guiafiltro_Schema
from liquidespa.functions.loginSLDFunction import logintoken
from generales.generales import CiaSAP
from os import getenv
import requests
import json
import urllib3

def ListadoGuias(pFiltro:guiafiltro_Schema):
    try:                
        urllib3.disable_warnings()
        
        sCia = CiaSAP("DTM")

        token = logintoken(sCia)        

        session = token['SessionId']
                
        payload = {}
        
        header = {
                    'Content-Type': 'application/json',
                    'Cookie': f'B1SESSION={session}; ROUTEID=.node3'
                 }
        
        Servidor = getenv("SERVIDOR_SLD")
        
        url =  f"{Servidor}/b1s/v1/DeliveryNotes?$select=DocEntry,DocNum,U_MSS_PRFI,U_MSS_NUFI,DocDate,CardCode,CardName,DocDueDate,"
        url += "U_MSS_ESTRA,U_MSS_ESLI,U_MSS_MONLIQ,U_MSS_MONEDA,U_MSS_NUMOP,U_MSS_TIPAG,U_MSS_OBSE,U_MSSL_LCD,U_MSSL_NCD,U_MSSL_PVH"
        url += f",U_MSS_FGTO,U_MSS_PLN,& $filter=U_MSS_FGTO eq '1' and not U_MSS_PLN eq null and U_MSS_ESLI eq null and DocDate eq '{pFiltro.Desde}' and U_MSSL_LCD eq '{pFiltro.Chofer}'&$top=100"
        
        response = requests.request("GET", url, headers=header, data = payload,verify=False)
        
        resultado = response.json()
                
        return resultado["value"]
    
    except Exception as err:
        print(err)
        return f"El error es : {err}"

def UpdGuias(pNroGuia:guiafiltro_Schema):
    try:
        resultado=[]
        mensaje = {"mensaje" : ""}

        urllib3.disable_warnings()
        
        sCia = CiaSAP("DTM")

        token = logintoken(sCia)   
        
        session = token['SessionId']
                                
        header = {
                    'Content-Type': 'application/json',
                    'Cookie': f'B1SESSION={session}; ROUTEID=.node3'
                 }
        
        Servidor = getenv("SERVIDOR_SLD")
        
        print(pNroGuia)

        for fila in pNroGuia.Guias:
            de = fila
            upd =   {
                        "U_MSS_ESLI" : de.U_MSS_ESLI,#Estado de Liquidacion 1-Pendiente de liquidacion 2-Liquidado 3-Rechazado
                        "U_MSS_ESTRA" : de.U_MSS_ESTRA,#Estado de Despacho de Transportista 1-Pendiente 2-Despacho Sin Cobranza 3-Despacho con Cobranza 4-Despacho a Currier 5-No despachado
                        "U_MSS_ESTLIQ" : de.U_MSS_ESTLIQ,#Estado de Despacho Liquidacion 1-Pendiente 2-Despacho Sin Cobranza 3-Despacho con Cobranza 4-Despacho a Currier 5-No despachado
                        "U_MSS_MONLIQ" : de.U_MSS_MONLIQ,#Monto a liquidar
                        "U_MSS_MONEDA" : de.U_MSS_MONEDA,#Moneda de la liquidacion
                        "U_MSS_TIPAG" : de.U_MSS_TIPAG,#Tipo de pago de la liquidacion
                        "U_MSS_OBSE" : de.U_MSS_OBSE, #Observacion de la liquidacion
                        "U_MSS_NUMOP" : de.U_MSS_NUMOP #Numero de operacion
                    }
            #print(de.DocEntry,de.U_MSS_ESLI)

            payload = json.dumps(upd)

            url = f"{Servidor}/b1s/v1/DeliveryNotes({de.DocEntry})"

            response = requests.request("PATCH", url, headers=header, data=payload,verify=False)
        
            #print(response.status_code)

            if(response.status_code!=204):
                #print(response.json())
                resultado = response.json()            
                        
            if("error" in resultado):
                mensaje = {"mensaje" : resultado['error']['message']['value']}
                resultado.append(mensaje)
            else:                
                mensaje = {"mensaje" : f"Se Actualizo la guia {de.U_MSS_PRFI}-{de.U_MSS_NUFI}."}
                resultado.append(mensaje)
                        
        return resultado
    except Exception as err:
        return f"Error en el registro : {err}"