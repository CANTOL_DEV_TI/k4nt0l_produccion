from conexiones.conexion_sql_solo import ConectaSQL_GDH
from liquidespa.schemas.choferesSchema import chofer_Schema
from liquidespa.functions.loginSLDFunction import logintoken
from generales.generales import CiaSAP

from os import getenv
import requests
import json
import urllib3

def ListadoChoferes():     
    try:                
        urllib3.disable_warnings()
        
        sCia = CiaSAP("DTM")
        
        token = logintoken(sCia)
        

        session = token['SessionId']
                
        payload = {}
        
        header = {
                    'Content-Type': 'application/json',
                    'Cookie': f'B1SESSION={session}; ROUTEID=.node3'
                    }
        
        Servidor = getenv("SERVIDOR_SLD")
        
        url =  f"{Servidor}/b1s/v1/MSS_COND?$select = U_MSS_LICE,U_MSS_NONC,U_MSS_ACON,U_MSS_NROD&$filter=U_MSS_ACTV eq 'Y'"
        
        response = requests.request("GET", url, headers=header, data = payload,verify=False)
        
        resultado = response.json()
                
        return resultado["value"]
    
    except Exception as err:
        return f"El error es : {err}"