from fastapi import APIRouter 
 
from compras_logistica.routes import importaciones,drawback,solicitudes 
 
ruta = APIRouter( 
    prefix="/compras_logistica", tags=["Compras Logistica"]
) 
 
ruta.include_router(drawback.ruta)
ruta.include_router(importaciones.ruta)
ruta.include_router(solicitudes.ruta)