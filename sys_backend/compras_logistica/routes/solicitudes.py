from fastapi import APIRouter,status

from compras_logistica.functions.solicitudFunctions import reporteSolicitudes
from compras_logistica.schemas.filtroSchemas import FiltroRepSolicitudes

ruta = APIRouter(
    prefix="/listado_solicitudes"
)

@ruta.post('/', status_code=status.HTTP_200_OK)
def lista_solicitudes(pfiltro:FiltroRepSolicitudes):
    return reporteSolicitudes(pfiltro)