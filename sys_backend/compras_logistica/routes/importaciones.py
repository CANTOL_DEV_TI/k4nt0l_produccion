from fastapi import APIRouter, Depends, status, UploadFile, File, Request  
from fastapi.responses import FileResponse  
 
from compras_logistica.functions.importacionesFunctions import reporteImportacionesStock,reporteImportacionesPedidos,ConsultaArticulos,reporteItemsStock
  
ruta = APIRouter(  
    prefix="/importaciones" 
)  
  
@ruta.get('/reporte/{pCia}/{pItem}/{pTipo}', status_code=status.HTTP_200_OK)  
def reporteStock(pCia:str, pItem: str, pTipo : str):  
    data = reporteImportacionesStock(pItem,pCia,pTipo) 
    return data  
 
@ruta.get('/reporte/detalle/{pCia}/{pItem}/{pAlmacen}', status_code=status.HTTP_200_OK) 
def listareporteStockDetalleItems(pCia:str, pItem : str, pAlmacen : str): 
    data = reporteImportacionesPedidos(pCia,pItem,pAlmacen) 
    return data

@ruta.get('/listadoarticulos/{pCia}/{pFiltro}', status_code=status.HTTP_200_OK)
def listafiltraarticulos(pCia:str,pFiltro:str):
    data = ConsultaArticulos(pCia,pFiltro)
    return data

@ruta.get('/reportestocks/{pCia}/{pItem}', status_code=status.HTTP_200_OK)  
def reporteStock(pCia:str, pItem: str):  
    data = reporteItemsStock(pItem,pCia) 
    return data  
