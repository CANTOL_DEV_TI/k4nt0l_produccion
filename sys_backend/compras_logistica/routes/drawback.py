from fastapi import APIRouter, Depends, status, UploadFile, File, Request  
from fastapi.responses import FileResponse  
 
from compras_logistica.functions.drawbackFunctions import ConsultaArticulos, ReporteTotal
  
ruta = APIRouter(  
    prefix="/drawback"
)  
  
@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK)  
def listaFormulas(txtFind: str):  
    data = ReporteTotal(txtFind) 
    return data  
 
@ruta.get('/ListaArticulos/{txtFind}', status_code=status.HTTP_200_OK) 
def listaItems(txtFind : str): 
    data = ConsultaArticulos(txtFind) 
    return data