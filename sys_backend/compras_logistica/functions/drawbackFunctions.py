from typing import List 
from decimal import Decimal 
from conexiones.conexion_sap_solo import conexion_SAP_Tecno 
from conexiones import conexion_sql_solo
 
def ReporteDrawBack(pCodigo:str): 
    lista = [] 
    fila = {} 
     
    sql_nivel_superior =    "SELECT concat('1.' , TO_VARCHAR(\"ChildNum\")) as NroOrden, \"Father\" as Nivel_S, RD.\"Code\" as Codigo_Det,RD.\"ItemName\" as Nombre_Det," 
    sql_nivel_superior +=   " CASE U_MSSC_PROC WHEN 1 THEN 'NACIONAL' WHEN 2 THEN 'IMPORTADO' END AS Procedencia,It.U_MSSC_ARE AS AreaID,"
    sql_nivel_superior +=   " IFNULL(CASE U_MSSL_TEX WHEN '02' THEN 'PRODUCTOS TERMINADOS' WHEN '04' THEN 'ENVASES' WHEN '05' THEN 'MATERIALES AUXILIARES'"
    sql_nivel_superior +=   " WHEN '08' THEN 'EMBALAJES' WHEN '99' THEN 'OTROS' ELSE 'MANO DE OBRA' END ,'MANO DE OBRA') AS Tipo_Existencia,"
    sql_nivel_superior +=   " (SELECT max(Proveedor) FROM (SELECT FPr.\"LicTradNum\" ||' - '|| \"CardName\" AS Proveedor, ROW_NUMBER () OVER "
    sql_nivel_superior +=   " (PARTITION BY FPr.\"CardName\" ORDER BY FPr.\"DocDate\" DESC) AS Fila FROM SBO_TECNO_PRODUCCION.OPCH FPr INNER JOIN SBO_TECNO_PRODUCCION.PCH1"
    sql_nivel_superior +=   " FPrD ON FPr.\"DocEntry\" = FPrD.\"DocEntry\" WHERE \"ItemCode\" = RD.\"Code\") F WHERE Fila = 1) AS Ultimo_Proveedor,"
    sql_nivel_superior +=   "\"Quantity\" as Cantidad, \"Price\" as Precio, \"Currency\" as Moneda, (SELECT count(*) FROM SBO_TECNO_PRODUCCION.ITT1 " 
    sql_nivel_superior +=   " WHERE \"Father\" = RD.\"Code\" ) AS Hijos  FROM SBO_TECNO_PRODUCCION.ITT1 RD "
    sql_nivel_superior +=   " LEFT JOIN SBO_TECNO_PRODUCCION.OITM It ON It.\"ItemCode\" = RD.\"Code\" "
    sql_nivel_superior +=   f" WHERE \"Father\" = '{pCodigo}' ORDER BY \"ChildNum\" " 
    #print(sql_nivel_superior)
    conn = conexion_SAP_Tecno() 
 
    cur_nivel_1 = conn.cursor() 
 
    cur_nivel_1.execute(sql_nivel_superior) 
 
    for hijo in cur_nivel_1: 
        sublista = [] 
        #if(hijo["Hijos"]>0): 
        #    sublista = BuscarHijos(hijo["Codigo_Det"]) 
         
        fila = {"nroorden":hijo["NroOrden"],"nivel_s":hijo["Nivel_S"],"codigo_det":hijo["Codigo_Det"],"nombre_det":hijo["Nombre_Det"],"cantidad":hijo["Cantidad"], 
                "precio":hijo["Precio"],"moneda":hijo["moneda"],"hijos" : hijo["Hijos"],"tipo" : hijo["Tipo_Existencia"], "ultimo_proveedor" : hijo["Ultimo_Proveedor"],
                "procedencia":hijo["Procedencia"],"area" : AreasNombresResp(hijo["AreaID"]) }#,#"sublista": sublista} 
         
        lista.append(fila) 
     
    conn.close() 
 
    return lista 
 
def BuscarHijos(pCodigo:str,pnivel:str): 
    lista = [] 
    fila = {} 
    sql_nivel_superior =    f"SELECT concat('{pnivel}.' , TO_VARCHAR(\"ChildNum\")) as NroOrden, \"Father\" as Nivel_S, RD.\"Code\" as Codigo_Det,RD.\"ItemName\" as Nombre_Det,"
    sql_nivel_superior +=   " CASE U_MSSC_PROC WHEN 1 THEN 'NACIONAL' WHEN 2 THEN 'IMPORTADO' END AS Procedencia,It.U_MSSC_ARE AS AreaID,"
    sql_nivel_superior +=   " IFNULL(CASE U_MSSL_TEX WHEN '02' THEN 'PRODUCTOS TERMINADOS' WHEN '04' THEN 'ENVASES' WHEN '05' THEN 'MATERIALES AUXILIARES'"
    sql_nivel_superior +=   " WHEN '08' THEN 'EMBALAJES' WHEN '99' THEN 'OTROS' ELSE 'MANO DE OBRA' END ,'MANO DE OBRA') AS Tipo_Existencia,"
    sql_nivel_superior +=   "(SELECT max(Proveedor) FROM (SELECT FPr.\"LicTradNum\" ||' - '|| \"CardName\" AS Proveedor, ROW_NUMBER () OVER "
    sql_nivel_superior +=   "(PARTITION BY FPr.\"CardName\" ORDER BY FPr.\"DocDate\" DESC) AS Fila FROM SBO_TECNO_PRODUCCION.OPCH FPr INNER JOIN SBO_TECNO_PRODUCCION.PCH1"
    sql_nivel_superior +=   " FPrD ON FPr.\"DocEntry\" = FPrD.\"DocEntry\" WHERE \"ItemCode\" = RD.\"Code\") F WHERE Fila = 1) AS Ultimo_Proveedor,"
    sql_nivel_superior +=   "\"Quantity\" as Cantidad, \"Price\" as Precio, \"Currency\" as Moneda, (SELECT count(*) FROM SBO_TECNO_PRODUCCION.ITT1 " 
    sql_nivel_superior +=   "WHERE \"Father\" = RD.\"Code\" ) AS Hijos  FROM SBO_TECNO_PRODUCCION.ITT1 RD "
    sql_nivel_superior +=   "LEFT JOIN SBO_TECNO_PRODUCCION.OITM It ON It.\"ItemCode\" = RD.\"Code\" "
    sql_nivel_superior +=   f" WHERE \"Father\" = '{pCodigo}' ORDER BY \"ChildNum\" " 
 
    conn = conexion_SAP_Tecno() 
 
    cur_nivel_1 = conn.cursor() 
 
    cur_nivel_1.execute(sql_nivel_superior) 
 
    for hijo in cur_nivel_1: 
        sublista = [] 
 
        #if(hijo["Hijos"]>0): 
        #    sublista = BuscarHijos2(hijo["Codigo_Det"]) 
 
        fila = {"nroorden":hijo["NroOrden"],"nivel_s":hijo["Nivel_S"],"codigo_det":hijo["Codigo_Det"],"nombre_det":hijo["Nombre_Det"],"cantidad":hijo["Cantidad"], 
                "precio":hijo["Precio"],"moneda":hijo["moneda"],"hijos" : hijo["Hijos"],"tipo" : hijo["Tipo_Existencia"], "ultimo_proveedor" : hijo["Ultimo_Proveedor"],
                "procedencia":hijo["Procedencia"],"area" : AreasNombresResp(hijo["AreaID"]) }#,"sublista": sublista} 
         
        lista.append(fila) 
     
    conn.close() 
 
    return lista 
 
def BuscarHijos2(pCodigo:str,pNivel:str): 
    lista = [] 
    fila = {} 
    sql_nivel_superior =    f"SELECT concat('{pNivel}.' , TO_VARCHAR(\"ChildNum\")) as NroOrden, \"Father\" as Nivel_S, RD.\"Code\" as Codigo_Det,RD.\"ItemName\" as Nombre_Det," 
    sql_nivel_superior +=   " CASE U_MSSC_PROC WHEN 1 THEN 'NACIONAL' WHEN 2 THEN 'IMPORTADO' END AS Procedencia,It.U_MSSC_ARE AS AreaID,"
    sql_nivel_superior +=   " IFNULL(CASE U_MSSL_TEX WHEN '02' THEN 'PRODUCTOS TERMINADOS' WHEN '04' THEN 'ENVASES' WHEN '05' THEN 'MATERIALES AUXILIARES'"
    sql_nivel_superior +=   " WHEN '08' THEN 'EMBALAJES' WHEN '99' THEN 'OTROS' ELSE 'MANO DE OBRA' END ,'MANO DE OBRA') AS Tipo_Existencia,"
    sql_nivel_superior +=   "(SELECT max(Proveedor) FROM (SELECT FPr.\"LicTradNum\" ||' - '|| \"CardName\" AS Proveedor, ROW_NUMBER () OVER "
    sql_nivel_superior +=   "(PARTITION BY FPr.\"CardName\" ORDER BY FPr.\"DocDate\" DESC) AS Fila FROM SBO_TECNO_PRODUCCION.OPCH FPr INNER JOIN SBO_TECNO_PRODUCCION.PCH1"
    sql_nivel_superior +=   " FPrD ON FPr.\"DocEntry\" = FPrD.\"DocEntry\" WHERE \"ItemCode\" = RD.\"Code\") F WHERE Fila = 1) AS Ultimo_Proveedor,"
    sql_nivel_superior +=   "\"Quantity\" as Cantidad, \"Price\" as Precio, \"Currency\" as Moneda, (SELECT count(*) FROM SBO_TECNO_PRODUCCION.ITT1 " 
    sql_nivel_superior +=   "WHERE \"Father\" = RD.\"Code\" ) AS Hijos  FROM SBO_TECNO_PRODUCCION.ITT1 RD "
    sql_nivel_superior +=   "LEFT JOIN SBO_TECNO_PRODUCCION.OITM It ON It.\"ItemCode\" = RD.\"Code\" "
    sql_nivel_superior +=   f" WHERE \"Father\" = '{pCodigo}' ORDER BY \"ChildNum\" " 
 
    conn = conexion_SAP_Tecno() 
 
    cur_nivel_1 = conn.cursor() 
 
    cur_nivel_1.execute(sql_nivel_superior) 
 
    for hijo in cur_nivel_1: 
        sublista = [] 
 
        #if(hijo["Hijos"]>0): 
        #    sublista = BuscarHijos3(hijo["Codigo_Det"]) 
 
        fila = {"nroorden":hijo["NroOrden"],"nivel_s":hijo["Nivel_S"],"codigo_det":hijo["Codigo_Det"],"nombre_det":hijo["Nombre_Det"],"cantidad":hijo["Cantidad"], 
                "precio":hijo["Precio"],"moneda":hijo["moneda"],"hijos" : hijo["Hijos"],"tipo" : hijo["Tipo_Existencia"], "ultimo_proveedor" : hijo["Ultimo_Proveedor"],
                "procedencia":hijo["Procedencia"],"area" : AreasNombresResp(hijo["AreaID"]) }#,"sublista": sublista} 
         
        lista.append(fila) 
     
    conn.close() 
 
    return lista 
 
def BuscarHijos3(pCodigo:str,pNivel:str): 
    lista = [] 
    fila = {} 
    sql_nivel_superior =    f"SELECT concat('{pNivel}.' , TO_VARCHAR(\"ChildNum\")) as NroOrden, \"Father\" as Nivel_S, RD.\"Code\" as Codigo_Det,RD.\"ItemName\" as Nombre_Det,"
    sql_nivel_superior +=   " CASE U_MSSC_PROC WHEN 1 THEN 'NACIONAL' WHEN 2 THEN 'IMPORTADO' END AS Procedencia,It.U_MSSC_ARE AS AreaID," 
    sql_nivel_superior +=   " IFNULL(CASE U_MSSL_TEX WHEN '02' THEN 'PRODUCTOS TERMINADOS' WHEN '04' THEN 'ENVASES' WHEN '05' THEN 'MATERIALES AUXILIARES'"
    sql_nivel_superior +=   " WHEN '08' THEN 'EMBALAJES' WHEN '99' THEN 'OTROS' ELSE 'MANO DE OBRA' END ,'MANO DE OBRA') AS Tipo_Existencia,"
    sql_nivel_superior +=   "(SELECT max(Proveedor) FROM (SELECT FPr.\"LicTradNum\" ||' - '|| \"CardName\" AS Proveedor, ROW_NUMBER () OVER "
    sql_nivel_superior +=   "(PARTITION BY FPr.\"CardName\" ORDER BY FPr.\"DocDate\" DESC) AS Fila FROM SBO_TECNO_PRODUCCION.OPCH FPr INNER JOIN SBO_TECNO_PRODUCCION.PCH1"
    sql_nivel_superior +=   " FPrD ON FPr.\"DocEntry\" = FPrD.\"DocEntry\" WHERE \"ItemCode\" = RD.\"Code\") F WHERE Fila = 1) AS Ultimo_Proveedor,"
    sql_nivel_superior +=   "\"Quantity\" as Cantidad, \"Price\" as Precio, \"Currency\" as Moneda, (SELECT count(*) FROM SBO_TECNO_PRODUCCION.ITT1 " 
    sql_nivel_superior +=   "WHERE \"Father\" = RD.\"Code\" ) AS Hijos  FROM SBO_TECNO_PRODUCCION.ITT1 RD "
    sql_nivel_superior +=   "LEFT JOIN SBO_TECNO_PRODUCCION.OITM It ON It.\"ItemCode\" = RD.\"Code\" "
    sql_nivel_superior +=   f" WHERE \"Father\" = '{pCodigo}' ORDER BY \"ChildNum\" " 
 
    conn = conexion_SAP_Tecno() 
 
    cur_nivel_1 = conn.cursor() 
 
    cur_nivel_1.execute(sql_nivel_superior) 
 
    for hijo in cur_nivel_1: 
 
        fila = {"nroorden":hijo["NroOrden"],"nivel_s":hijo["Nivel_S"],"codigo_det":hijo["Codigo_Det"],"nombre_det":hijo["Nombre_Det"],"cantidad":hijo["Cantidad"], 
                "precio":hijo["Precio"],"moneda":hijo["moneda"],"hijos" : hijo["Hijos"],"tipo" : hijo["Tipo_Existencia"], "ultimo_proveedor" : hijo["Ultimo_Proveedor"],
                "procedencia":hijo["Procedencia"],"area" : AreasNombresResp(hijo["AreaID"]) ,} 
         
        lista.append(fila) 
     
    conn.close() 
 
    return lista 

def BuscarHijos4(pCodigo:str,pNivel:str): 
    lista = [] 
    fila = {} 
    sql_nivel_superior =    f"SELECT concat('{pNivel}.' , TO_VARCHAR(\"ChildNum\")) as NroOrden, \"Father\" as Nivel_S, RD.\"Code\" as Codigo_Det,RD.\"ItemName\" as Nombre_Det,"
    sql_nivel_superior +=   " CASE U_MSSC_PROC WHEN 1 THEN 'NACIONAL' WHEN 2 THEN 'IMPORTADO' END AS Procedencia,It.U_MSSC_ARE AS AreaID,"
    sql_nivel_superior +=   " IFNULL(CASE U_MSSL_TEX WHEN '02' THEN 'PRODUCTOS TERMINADOS' WHEN '04' THEN 'ENVASES' WHEN '05' THEN 'MATERIALES AUXILIARES'"
    sql_nivel_superior +=   " WHEN '08' THEN 'EMBALAJES' WHEN '99' THEN 'OTROS' ELSE 'MANO DE OBRA' END ,'MANO DE OBRA') AS Tipo_Existencia,"
    sql_nivel_superior +=   "(SELECT max(Proveedor) FROM (SELECT FPr.\"LicTradNum\" ||' - '|| \"CardName\" AS Proveedor, ROW_NUMBER () OVER "
    sql_nivel_superior +=   "(PARTITION BY FPr.\"CardName\" ORDER BY FPr.\"DocDate\" DESC) AS Fila FROM SBO_TECNO_PRODUCCION.OPCH FPr INNER JOIN SBO_TECNO_PRODUCCION.PCH1"
    sql_nivel_superior +=   " FPrD ON FPr.\"DocEntry\" = FPrD.\"DocEntry\" WHERE \"ItemCode\" = RD.\"Code\") F WHERE Fila = 1) AS Ultimo_Proveedor,"
    sql_nivel_superior +=   "\"Quantity\" as Cantidad, \"Price\" as Precio, \"Currency\" as Moneda, (SELECT count(*) FROM SBO_TECNO_PRODUCCION.ITT1 " 
    sql_nivel_superior +=   "WHERE \"Father\" = RD.\"Code\" ) AS Hijos  FROM SBO_TECNO_PRODUCCION.ITT1 RD "
    sql_nivel_superior +=   "LEFT JOIN SBO_TECNO_PRODUCCION.OITM It ON It.\"ItemCode\" = RD.\"Code\" "
    sql_nivel_superior +=   f" WHERE \"Father\" = '{pCodigo}' ORDER BY \"ChildNum\" " 
 
    conn = conexion_SAP_Tecno() 
 
    cur_nivel_1 = conn.cursor() 
 
    cur_nivel_1.execute(sql_nivel_superior) 
 
    for hijo in cur_nivel_1: 
 
        fila = {"nroorden":hijo["NroOrden"],"nivel_s":hijo["Nivel_S"],"codigo_det":hijo["Codigo_Det"],"nombre_det":hijo["Nombre_Det"],"cantidad":hijo["Cantidad"], 
                "precio":hijo["Precio"],"moneda":hijo["moneda"],"hijos" : hijo["Hijos"],"tipo" : hijo["Tipo_Existencia"], "ultimo_proveedor" : hijo["Ultimo_Proveedor"],
                "procedencia":hijo["Procedencia"],"area" : AreasNombresResp(hijo["AreaID"]) ,} 
         
        lista.append(fila) 
     
    conn.close() 
 
    return lista 
 
def ConsultaArticulos(pNombres:str): 
    try: 
        lista = [] 
        fila = {} 
 
        sql_articulos = "SELECT TOP 100 \"ItemCode\" as ItemCode,\"ItemName\" as ItemName FROM SBO_TECNO_PRODUCCION.OITM " 
        sql_articulos += "WHERE \"InvntItem\" = 'Y' AND \"ItemCode\" LIKE 'PT%' AND \"ItmsGrpCod\" IN (100,101," 
        sql_articulos += "104,106,107,108,110,109,105,102) AND \"ItemCode\" IN (SELECT \"Code\" FROM " 
        sql_articulos += f"SBO_TECNO_PRODUCCION.OITT ) AND ((\"ItemCode\" LIKE '%{pNombres}%') OR (\"ItemName\" LIKE '%{pNombres}%')) ORDER BY \"ItemCode\"" 
        #print(sql_articulos) 
        conn = conexion_SAP_Tecno() 
 
        cur_articulos = conn.cursor() 
 
        cur_articulos.execute(sql_articulos) 
 
        for item in cur_articulos: 
            fila = {"Codigo" : item["ItemCode"], "Nombre" : item["ItemName"]} 
            lista.append(fila) 
         
        return lista 
 
    except Exception as err: 
        return f"Error en la operación : {err}." 
 
def AreasNombresResp (pIdSAP):
    try:
        xAreaNombre = ""

        sql_area = f"select nom_subarea as Nombre from sub_area sa where cod_subarea_sap = '{pIdSAP}'"
        
        conn = conexion_sql_solo.ConectaSQL_Produccion()
        
        cur_areas = conn.cursor(as_dict = True)
        
        cur_areas.execute(sql_area)

        for item in cur_areas:
            xAreaNombre = item['Nombre']
        
        conn.close()

        return xAreaNombre
    
    except Exception as err:
        return f"Error en la operación : {err}"

def ReporteTotal(pCodigo:str):

    reporte = []

    reporte_1 = ReporteDrawBack(pCodigo)
    
    for fila1 in reporte_1:
        reporte.append(fila1)
        
        reporte_2 = BuscarHijos(fila1['codigo_det'],fila1['nroorden'])

        for fila2 in reporte_2:
            reporte.append(fila2)

            reporte_3 = BuscarHijos2(fila2['codigo_det'],fila2['nroorden'])

            for fila3 in reporte_3:
                reporte.append(fila3)

                reporte_4 = BuscarHijos3(fila3['codigo_det'],fila3['nroorden'])

                for fila4 in reporte_4:
                    reporte.append(fila4)

                    reporte_5 = BuscarHijos4(fila4['codigo_det'],fila4['nroorden'])

                    for fila5 in reporte_5:
                        reporte.append(fila5)
    
    return reporte


    