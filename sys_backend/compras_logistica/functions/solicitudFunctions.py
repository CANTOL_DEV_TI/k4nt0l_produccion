from conexiones import conexion_sql_solo
from generales import generales
from compras_logistica.schemas.filtroSchemas import FiltroRepSolicitudes

def reporteSolicitudes(pFiltro:FiltroRepSolicitudes):

    conn = conexion_sql_solo.ConectaSQL_Produccion()

    cur_lista = conn.cursor(as_dict = True)

    sql =  "select distinct lrc.codigo_rq,lrc.centro_costo_id,(select distinct cc_desc from presu_cc_usuario where cia = lrc.cia and centro_costo = lrc.centro_costo_id) as cc_desc,"
    sql += "format(lrc.fecha_pedido,'yyyy-mm-dd') as fecha_pedido ,format(lrc.fecha_necesaria,'yyyy-MM-dd') as fecha_necesaria,format(lrc.fecha_vencimiento,'yyyy-MM-dd') as fecha_vencimiento,format(lrc.fecha_registro,'yyyy-MM-dd hh:mm:ss') as fecha_registro,lrc.usuario ,lrc.SAP_DocEntry ,lrc.SAP_DocNum,"
    sql += "case lrc.estado when 'P' then 'Pendiente' when 'A' then 'Aprobado' when 'E' then 'Eliminado' when 'M' then 'Migrado' end estado_desc "
    sql += "from logi_requerimiento_cabecera lrc inner join logi_requerimiento_detalle lrd on lrc.cia = lrd.cia and lrc.codigo_rq = lrd.codigo_rq inner join logi_requerimiento_aprobaciones lra on lra.cia = lrc.cia and lra.codigo_rq = lrc.codigo_rq "
    sql += f"where lrc.cia = '{pFiltro.cia}' and lrc.usuario like '%{pFiltro.usuario}%' and lra.usuario_aprueba LIKE '%{pFiltro.aprobador}%' and format(lrc.fecha_registro,'yyyyMMdd') between '{pFiltro.desde}' and '{pFiltro.hasta}' and lrc.estado = '{pFiltro.estado}'"
    print(sql)
    cur_lista.execute(sql)

    tabla = cur_lista.fetchall()

    return tabla