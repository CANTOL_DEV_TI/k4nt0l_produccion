from conexiones import conexion_sap_solo
from generales import generales

def reporteImportacionesStock(pItem:str,pCia:str,pTipo:str):
    try:
        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_sql = conn.cursor()
        sCia = generales.CiaSAP(pCia)
        
        sSql = "SELECT ti.\"ItemCode\" as ItemCodigo,ti.\"ItemName\" as ItemNombre,tia.\"WhsCode\" as AlmacenCodigo,ta.\"WhsName\" as AlmacenNombre,"
        sSql += "tia.\"OnHand\" as Stock,tia.\"IsCommited\" as Comprometido, (tia.\"OnHand\" - tia.\"IsCommited\") AS Disponible, tia.\"OnOrder\" as Pedido"
        sSql += f" FROM {sCia}.OITM ti "
        sSql += f"INNER JOIN {sCia}.OITW tia ON ti.\"ItemCode\" = tia.\"ItemCode\" "
        sSql += f"INNER JOIN {sCia}.OWHS ta ON ta.\"WhsCode\" = tia.\"WhsCode\" WHERE " 

        if(pCia == "TCN"):
            sSql += " tia.\"WhsCode\" in ('ALM16','ALM20','ALM36') and "
        
        if(pCia == "DTM"):
            sSql += " tia.\"WhsCode\" in ('ALM01','ALM12','ALM24') and "

        sSql += f"ti.U_MSSC_PROC = '{pTipo}' AND (tia.\"OnHand\" > 0  or tia.\"OnOrder\" > 0) AND ti.\"ItemCode\" ='{pItem}' order by 1,3"

        print(sSql)

        cur_sql.execute(sSql)
        
        filacab = {}
        
        for i in cur_sql:
            if len(filacab) == 0:
                filacab = {'CIA':pCia,'ItemCodigo':i["ItemCodigo"],'ItemNombre':i["ItemNombre"], 
                           'ALM16':'','Stock_16':0,'Comprometido_16':0,'Disponible_16':0,'Pedido_16':0,
                           'ALM20':'','Stock_20':0,'Comprometido_20':0,'Disponible_20':0,'Pedido_20':0,
                           'ALM36':'','Stock_36':0,'Comprometido_36':0,'Disponible_36':0,'Pedido_36':0,
                           'ALM01':'','Stock_01':0,'Comprometido_01':0,'Disponible_01':0,'Pedido_01':0,
                           'ALM12':'','Stock_12':0,'Comprometido_12':0,'Disponible_12':0,'Pedido_12':0,
                           'ALM24':'','Stock_24':0,'Comprometido_24':0,'Disponible_24':0,'Pedido_24':0}
                
            if(pCia=='TCN'):
                if(i["AlmacenCodigo"] == 'ALM16'):
                    filacab['ALM16'] = i["AlmacenNombre"]
                    filacab['Stock_16'] = i["Stock"]
                    filacab['Comprometido_16'] = i["Comprometido"]
                    filacab['Disponible_16'] = i["Disponible"]
                    filacab['Pedido_16'] = i["Pedido"]
                
                if(i["AlmacenCodigo"] == 'ALM20'):
                    filacab['ALM20'] = i["AlmacenNombre"]
                    filacab['Stock_20'] = i["Stock"]
                    filacab['Comprometido_20'] = i["Comprometido"]
                    filacab['Disponible_20'] = i["Disponible"]
                    filacab['Pedido_20'] = i["Pedido"]
                
                if(i["AlmacenCodigo"] == 'ALM36'):
                    filacab['ALM36'] = i["AlmacenNombre"]
                    filacab['Stock_36'] = i["Stock"]
                    filacab['Comprometido_36'] = i["Comprometido"]
                    filacab['Disponible_36'] = i["Disponible"]
                    filacab['Pedido_36'] = i["Pedido"]

            if(pCia=='DTM'):
                if(i["AlmacenCodigo"] == 'ALM01'):
                    filacab['ALM01'] = i["AlmacenNombre"]
                    filacab['Stock_01'] = i["Stock"]
                    filacab['Comprometido_01'] = i["Comprometido"]
                    filacab['Disponible_01'] = i["Disponible"]
                    filacab['Pedido_01'] = i["Pedido"]
                
                if(i["AlmacenCodigo"] == 'ALM12'):
                    filacab['ALM12'] = i["AlmacenNombre"]
                    filacab['Stock_12'] = i["Stock"]
                    filacab['Comprometido_12'] = i["Comprometido"]
                    filacab['Disponible_12'] = i["Disponible"]
                    filacab['Pedido_12'] = i["Pedido"]
                
                if(i["AlmacenCodigo"] == 'ALM24'):
                    filacab['ALM24'] = i["AlmacenNombre"]
                    filacab['Stock_24'] = i["Stock"]
                    filacab['Comprometido_24'] = i["Comprometido"]
                    filacab['Disponible_24'] = i["Disponible"]
                    filacab['Pedido_24'] = i["Pedido"]

        reporte = []

        if(len(filacab) > 0):
            reporte.append(filacab)

        return reporte
    
    except Exception as err:
        print (err)
        return {"error": f"error en cabecera reporte {err}"}

def reporteImportacionesPedidos(pCia:str, pItem:str,pAlmacen:str):
    try:
        sCia = generales.CiaSAP(pCia)

        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_sql = conn.cursor()

        sSql = "SELECT oc.\"DocNum\" as NroPedido,  ocd.\"OpenQty\" as Cantidad, "
        sSql += f"TO_VARCHAR(ocd.\"ShipDate\",'DD/MM/YYYY') as FechaEntrega,\"WhsCode\" as Almacen FROM {sCia}.opor oc "
        sSql += f"INNER JOIN {sCia}.POR1 ocd ON oc.\"DocEntry\" = OCD.\"DocEntry\" "
        sSql += f"WHERE ocd.\"ItemCode\" = '{pItem}' and ocd.\"WhsCode\" = '{pAlmacen}' AND oc.\"DocStatus\" = 'O' " #AND U_MSS_TIPC = 'PO' AND U_MSSC_PROC = '2' "
        sSql += " and ocd.\"OpenQty\" > 0 order by ocd.\"ShipDate\" asc"
        print(sSql)
        cur_sql.execute(sSql)

        reporte = []

        for i in cur_sql:
            fila = {'NroPedido':i["NroPedido"],'Cantidad':i["Cantidad"],'FechaEntrega':i["FechaEntrega"],'Almacen':i["Almacen"]}
            reporte.append(fila)

        return reporte
    except Exception as err:
        print(err)
        return {"error":f"error en detalle reporte {err}"}

def ConsultaArticulos(pCia,pFiltro):
    try:
        lista = []
        fila = {} 
        sCia = generales.CiaSAP(pCia)
        sql_articulos = f"SELECT TOP 100 \"ItemCode\" as ItemCode,\"ItemName\" as ItemName FROM {sCia}.OITM " 
        sql_articulos += f"WHERE ((\"ItemCode\" LIKE '%{pFiltro}%') OR (\"ItemName\" LIKE '%{pFiltro}%')) ORDER BY \"ItemCode\"" 
        print(sql_articulos) 
        conn = conexion_sap_solo.conexion_SAP_Tecno() 
 
        cur_articulos = conn.cursor() 
 
        cur_articulos.execute(sql_articulos) 
 
        for item in cur_articulos: 
            fila = {"Codigo" : item["ItemCode"], "Nombre" : item["ItemName"]} 
            lista.append(fila) 
         
        return lista 
    except Exception as err:
        print(err)
        return []


def reporteItemsStock(pItem:str,pCia:str):
    try:
        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_sql = conn.cursor()
        sCia = generales.CiaSAP(pCia)
        
        sSql = "SELECT ti.\"ItemCode\" as ItemCodigo,ti.\"ItemName\" as ItemNombre,tia.\"WhsCode\" as AlmacenCodigo,ta.\"WhsName\" as AlmacenNombre,"
        sSql += "tia.\"OnHand\" as Stock,tia.\"IsCommited\" as Comprometido, (tia.\"OnHand\" - tia.\"IsCommited\") AS Disponible, tia.\"OnOrder\" as Pedido"
        sSql += f" FROM {sCia}.OITM ti "
        sSql += f"INNER JOIN {sCia}.OITW tia ON ti.\"ItemCode\" = tia.\"ItemCode\" "
        sSql += f"INNER JOIN {sCia}.OWHS ta ON ta.\"WhsCode\" = tia.\"WhsCode\" WHERE " 
        sSql += f"ti.\"ItemCode\" = '{pItem}' order by 3"

        #print(sSql)

        cur_sql.execute(sSql)
        
        filacab = {}
        detalle_stocks = []

        for i in cur_sql:
            if len(filacab) == 0:
                filacab = {'CIA':pCia,'ItemCodigo':i["ItemCodigo"],'ItemNombre':i["ItemNombre"],'detalles' : []}
                
            filadet = {'AlmacenCod' : i["AlmacenCodigo"] ,'AlmacenNombre' : i["AlmacenNombre"],'Stock' : i["Stock"],
                       'Comprometido': i["Comprometido"],'Disponible' : i["Disponible"],'Pedido': i["Pedido"]}
                
            detalle_stocks.append(filadet)
                
        filacab['detalles'] = detalle_stocks

        reporte = []

        if(len(filacab) > 0):
            reporte.append(filacab)

        return reporte
    
    except Exception as err:
        print (err)
        return {"error": f"error en cabecera reporte {err}"}