from pydantic import BaseModel
from typing import Optional

class FiltroRepSolicitudes(BaseModel):
    cia : Optional[str]
    desde : Optional[str]
    hasta : Optional[str]
    estado : Optional[str]
    usuario : Optional[str]
    aprobador : Optional[str]