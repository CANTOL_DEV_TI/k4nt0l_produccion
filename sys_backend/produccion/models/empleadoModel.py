from sqlalchemy import Column, Integer, String, DECIMAL, ForeignKey
from sqlalchemy.orm import relationship

from conexiones.conexion_sql import Base


class empleado_Model(Base):
    __tablename__ = 'empleado'
    cod_empleado = Column(String, primary_key=True, index=True)
    cod_subarea = Column(Integer, ForeignKey("sub_area.cod_subarea"))
    nombre = Column(String)
    sw_estado = Column(String)

    def __str__(self):
        return self.nombre


    empleado = relationship("subArea_Model", back_populates="subarea3")


