from sqlalchemy import Column, Integer, String, DECIMAL, Boolean, Date
from conexiones.conexion_sql import Base


class tipoEquipo_Model(Base):
    __tablename__ = 'tipo_equipo'
    id_tpequipo = Column(Integer, primary_key=True)
    tipo_equipo = Column(String)    
    cod_usuario = Column(Integer)
    fecre = Column(Date)

    def __str__(self):
        return self.tipo_equipo