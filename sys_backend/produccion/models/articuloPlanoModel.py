from conexiones.conexion_pSql import Base
from sqlalchemy import Column, Integer, String, DECIMAL


class ArticuloPlanoModel(Base):
    __tablename__ = 'articulo_plano'
    id_art_plano = Column(Integer, primary_key=True)
    cod_articulo = Column(String)
    id_version = Column(Integer)
    articulo = Column(String)
    cod_plano = Column(String)
    sw_est_plano = Column(String)
    tipo = Column(String)
    glosa = Column(String)
    sw_estado = Column(String)
    cod_usuario = Column(String)

