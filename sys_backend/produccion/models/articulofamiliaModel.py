from sqlalchemy import Column, Integer, String, DECIMAL, Boolean, Date
from sqlalchemy.orm import relationship

from conexiones.conexion_sql import Base

class articulofamilia_Model(Base):
    __tablename__ = 'articulo_familia'
    cod_articulo = Column(String, primary_key=True)
    id_familia = Column(Integer)
    articulo = Column(String)
    abc = Column(String)
    cant_lote_min = Column(DECIMAL)
    cant_und_hora = Column(DECIMAL)    
    cob_minimo_dia = Column(DECIMAL)
    cob_ideal_dia = Column(DECIMAL)
    cob_maximo_dia =Column(DECIMAL)
    cpdp = Column(DECIMAL)
    sw_estado = Column(String)
    cod_subarea = Column(Integer)

    def __str__(self):
        return self.articulo

    ##familia_Model = relationship("familia_Model", back_populates="familia")