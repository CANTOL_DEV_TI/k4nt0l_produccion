from sqlalchemy import Column, Integer, String, DECIMAL, ForeignKey
from sqlalchemy.orm import relationship

from conexiones.conexion_sql import Base
# from produccion.models.areaModel import area_Model

class subArea_Model(Base):
    __tablename__ = 'sub_area'
    cod_subarea = Column(Integer, primary_key=True, index=True)
    cod_area = Column(Integer, ForeignKey("area.cod_area"))
    nom_subarea = Column(String)

    def __str__(self):
        return self.nom_subarea

    subarea = relationship("area_Model", back_populates="area")
    subarea2 = relationship("subAreaAlmacen_Model", back_populates="subarea")

    subarea3 = relationship("empleado_Model", back_populates="empleado")


