from sqlalchemy import Column, String, Integer, DateTime 
from conexiones.conexion_sql import Base 
 
 
class tareaprogramadausuario_Model(Base): 
    __tablename__ = 'tarea_programada_usuario' 
    id_tarea = Column(Integer, primary_key=True) 
    fecha_busqueda = Column(String) 
    fecha_tarea = Column(DateTime)     
    cod_usuario = Column(Integer)     
    ejercicio_tarea = Column(Integer)
    periodo_tarea = Column(Integer)
    tipo_tarea = Column(String)
 
    def __str__(self): 
        return self.fecha_tarea