from sqlalchemy import Column, Integer, String, DECIMAL, Date, ForeignKey
from sqlalchemy.orm import relationship

from conexiones.conexion_sql import Base


class stockUnificadoCab_Model(Base):
    __tablename__ = 'stock_unido_cab'
    id_stock = Column(Integer, primary_key=True)
    fecha_emision = Column(Date)
    def __str__(self):
        return self.id_stock

    stockUnificadoCab = relationship("stockUnificadoDet_Model", back_populates="stockUnificadoDet")


class stockUnificadoDet_Model(Base):
    __tablename__ = 'stock_unido_det'
    id_item = Column(Integer, primary_key=True)
    id_stock = Column(Integer, ForeignKey("stock_unido_cab.id_stock"))
    cod_articulo = Column(String)
    tecnopress_stock = Column(DECIMAL)
    tecnopress_comprometido = Column(DECIMAL)
    distrimax_stock = Column(DECIMAL)
    distrimax_comprometido = Column(DECIMAL)
    distrimax_pedidosborrador = Column(DECIMAL)
    stock_inicial = Column(DECIMAL)

    def __str__(self):
        return self.id_item

    stockUnificadoDet = relationship("stockUnificadoCab_Model", back_populates="stockUnificadoCab")




