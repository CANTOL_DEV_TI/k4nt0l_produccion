from sqlalchemy import Column, Integer, String, DECIMAL, Boolean, Date
from conexiones.conexion_sql import Base


class familia_Model(Base):
    __tablename__ = 'familia'
    id_familia = Column(Integer, primary_key=True)
    nom_familia = Column(String)
    sw_estado = Column(String)
    cod_usuario = Column(Integer)
    fecre = Column(Date)

    def __str__(self):
        return self.nom_familia

    


