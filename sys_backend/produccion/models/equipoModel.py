from sqlalchemy import Column, Integer, String, DECIMAL
from conexiones.conexion_sql import Base


class Equipo_Model(Base):
    __tablename__ = 'equipo'
    id_equipo = Column(Integer, primary_key=True)
    cod_subarea = Column(Integer)
    id_tpequipo = Column(Integer)
    cod_equipo = Column(String)
    equipo = Column(String)    
    factor_desviacion = Column(DECIMAL)
    oferta_horas_x_dia = Column(DECIMAL)
    sw_operativo = Column(Integer)
    total_turnos = Column(Integer)
    disponibilidad_historico = Column(DECIMAL)

    def __str__(self):
        return self.equipo