from sqlalchemy import Column, Integer, String, DECIMAL, ForeignKey
from sqlalchemy.orm import relationship

from conexiones.conexion_sql import Base

class subAreaAlmacen_Model(Base):
    __tablename__ = 'subarea_almacen'
    cod_subarea_almacen = Column(Integer, primary_key=True, index=True)
    cod_almacen = Column(String) #Column(String, ForeignKey("almacenes.cod_almacen"))
    cod_subarea = Column(Integer, ForeignKey("sub_area.cod_subarea"))
    nom_subarea_almacen = Column(String)

    def __str__(self):
        return self.nom_subarea_almacen

    subarea = relationship("subArea_Model", back_populates="subarea2")

