from sqlalchemy import Column, Integer, String, DECIMAL
from sqlalchemy.orm import relationship

from conexiones.conexion_sql import Base


class config_Proceso_Det_Model(Base):
    __tablename__ = 'config_proceso_det'
    id_fmp_det = Column(Integer, primary_key=True)
    id_fmproceso = Column(Integer, primary_key=True)
    id_equipo = Column(Integer)
    id_tpoperacion = Column(Integer)
    num_paso = Column(String)
    num_prioridad = Column(Integer)
    desc_proceso = Column(String)
    std_und_hora = Column(DECIMAL)    


    def __str__(self):
        return self.id_fmproceso
    
    ##config_Proceso_Det = relationship("config_Proceso_Cab_Model", back_populates="config_proceso_cab")