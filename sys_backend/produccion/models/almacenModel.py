from sqlalchemy import Column, String

from conexiones.conexion_sql import Base


class almacen_Model(Base):
    __tablename__ = 'almacenes'
    cod_almacen = Column(String, primary_key=True)
    nom_almacen = Column(String)


    def __str__(self):
        return self.nom_almacen