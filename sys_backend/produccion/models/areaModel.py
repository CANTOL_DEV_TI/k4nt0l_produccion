from sqlalchemy import Column, Integer, String, DECIMAL
from sqlalchemy.orm import relationship

from conexiones.conexion_sql import Base


class area_Model(Base):
    __tablename__ = 'area'
    cod_area = Column(Integer, primary_key=True)
    nom_area = Column(String)


    def __str__(self):
        return self.nom_area

    area = relationship("subArea_Model", back_populates="subarea")



