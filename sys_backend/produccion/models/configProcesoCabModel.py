from sqlalchemy import Column, Integer, String, DECIMAL
from sqlalchemy.orm import relationship

from conexiones.conexion_sql import Base


class config_Proceso_Cab_Model(Base):
    __tablename__ = 'config_proceso_cab'
    id_fmproceso = Column(Integer, primary_key=True)
    cod_articulo = Column(String)
    articulo = Column(String)
    cant_lote_min = Column(DECIMAL)


    def __str__(self):
        return self.articulo

    ##config_proceso_cab = relationship("config_Proceso_Det_Model", back_populates="config_Proceso_Det")