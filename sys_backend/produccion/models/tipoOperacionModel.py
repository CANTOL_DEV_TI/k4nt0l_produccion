from sqlalchemy import Column, Integer, String, DECIMAL, Boolean, Date
from conexiones.conexion_sql import Base


class tipoOperacion_Model(Base):
    __tablename__ = 'tipo_operacion'
    id_tpoperacion = Column(Integer, primary_key=True)
    tipo_operacion = Column(String)    

    def __str__(self):
        return self.tipo_operacion