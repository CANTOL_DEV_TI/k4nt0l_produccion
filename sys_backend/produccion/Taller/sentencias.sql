

select * from oee_cab
select * from oee_det


--####################################################
SELECT
    oec.id_oee,
    oec.sw_estado,
    CONVERT(DATE, oec.fecha_doc, 103) AS fecha_doc,
    e.nombre,
    a.articulo,
    eq.equipo,
    oec.num_paso
FROM oee_cab oec
INNER JOIN empleado e ON oec.cod_empleado=e.cod_empleado
INNER JOIN articulo_familia a ON oec.cod_articulo=a.cod_articulo
INNER JOIN equipo eq ON eq.id_equipo=oec.id_equipo
WHERE oec.sw_estado in ('1','0')
AND CONVERT(DATE, oec.fecha_doc, 103)='2023-12-21'
AND oec.cod_empleado='42151712'
ORDER BY oec.id_oee DESC
--####################################################

SELECT
    e.cod_empleado,
    e.nombre,
    e.cod_subarea,
    sa.nom_subarea
FROM empleado e
INNER JOIN sub_area sa ON sa.cod_subarea=e.cod_subarea
WHERE e.cod_empleado='42151712'

--------------------------------------------------

select
oec.id_oee,
oec.fecha_doc,
oec.articulo,
oec.cod_articulo,
oec.cod_empleado,
oec.cod_subarea,
oec.cod_turno,
oec.id_tpoperacion,
oec.id_equipo,
oec.num_of_sap,
oec.num_paso,
oec.std_und_hora,
oec.hora_fin,
oec.hora_ini,
oec.hora_planificado,
oec.sw_estado,
---
e.nombre,
t.nom_turno
---
from oee_cab oec
inner join empleado e ON e.cod_empleado=oec.cod_empleado
inner join turno_cab t ON t.cod_turno=oec.cod_turno
where
year(oec.fecha_doc) = 2024
and month(oec.fecha_doc) = 1
and day(oec.fecha_doc) = 30
and e.cod_empleado = '42151712'
order by e.nombre, oec.fecha_doc


select
oed.cantidad,
oed.causa,
oed.cod_causa,
oed.cod_motivo_causa,
oed.cod_tipo_registro,
oed.detalle,
oed.id_oee,
oed.item_oee,
oed.motivo_causa,
oed.num_vale,
oed.tipo_registro,
oed.umu
from oee_det oed
where id_oee=1
order by oed.item_oee

 select  oed.cantidad,  oed.causa,  oed.cod_causa,  oed.cod_motivo_causa,  oed.cod_tipo_registro,  oed.detalle,  oed.id_oee,  oed.item_oee,  oed.motivo_causa,  oed.num_vale,  oed.tipo_registro,  oed.umu
 from oee_det oed

--####################################################


SELECT * from sub_area;

select * from dbo.periodo_laboral

select * from turno_cab

select * from equipo;

--####################################################
select * from subarea_usuario;





select
    ic.id_config_oee,
    ic.descripcion,
    im.cod_motivo_indicador,
    im.indicador_motivo
from config_indicadores_oee ic
inner join oee_indicadores_motivo im ON im.id_config_oee=ic.id_config_oee


------------------------------------------------------------
select * from oee_tipo_registro
select * from oee_causa where cod_tipo_registro=5
select * from oee_motivo_causa where cod_causa=7 and cod_subarea=3

--####################################################

select
    tr.tipo_registro,
    c.cod_causa,
    c.causa,
    mc.motivo_causa
from oee_tipo_registro tr
inner join oee_causa c ON tr.cod_tipo_registro=c.cod_tipo_registro
inner join oee_motivo_causa mc ON mc.cod_causa=c.cod_causa
where tr.cod_tipo_registro=5


select * from oee_causa where cod_tipo_registro=5
select * from oee_motivo_causa where cod_causa in (8, 9, 10)

select * from oee_motivo_causa where motivo_causa like '%AIRE%'
select * from oee_causa where cod_causa in (8, 9, 10)


select * from config_indicadores_oee;
select * from oee_indicadores_motivo;

select * from oee_tipo_registro


select * from sub_area



select * from oee_cab where id_oee=17
select * from oee_det where id_oee=17


select
    id_config_oee,
    id_oee,
    item_indicador_oeee,
    causa,
    cantidad,
    motivo_causa,
    'OEE' as tipo_registro,
    umu,
    '5' as cod_tipo_registro,
    0 as cod_motivo_indicador
from oee_indicadores
where id_oee=10
order by id_config_oee

select * from oee_indicadores



select * from config_indicadores_oee

select * from oee_indicadores_motivo


select * from oee_tipo_registro

EXEC sp_RENAME 'oee_tipo_registro.style', 'style_principal', 'COLUMN'

alter table oee_tipo_registro
add style_secundario varchar(500)

--#####################################################

-- delete from oee_det;
-- delete from oee_cab;
-- delete from empleado;
--
-- delete from oee_motivo_causa;
-- delete from oee_causa_sub_area;
-- delete from oee_causa;


--$$$$$$$$$$$$$$




select
oec.id_tpoperacion,
oec.id_equipo,
oec.num_of_sap,
oec.num_paso,
oec.std_und_hora,
oec.hora_fin,
oec.hora_ini,
oec.hora_planificado,
oec.sw_estado,
e.nombre as empleado,
t.nom_turno,
oec.horas_operativo,
oec.produccion_total,
oec.horas_perdidas,
'' as detalle

from oee_cab oec
inner join empleado e ON e.cod_empleado=oec.cod_empleado
inner join turno_cab t ON t.cod_turno=oec.cod_turno
where
--     sql += f" year(oec.fecha_doc) = %s " %(fecha_dt.year)
--     sql += f" and month(oec.fecha_doc) = %s " %(fecha_dt.month)
--     sql += f" and day(oec.fecha_doc) = %s " %(fecha_dt.day)
oec.fecha_doc>=DATEADD(day,-5, CONVERT (date, '2024-02-16'))
-- and e.cod_empleado = '%s' " %(cod_empleado)
order by e.nombre, oec.fecha_doc



select * from oee_cab



