from fastapi import HTTPException, status
from sqlalchemy import select, and_, text
from sqlalchemy.orm import Session
from produccion.models.articuloPlanoModel import ArticuloPlanoModel
from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sql_solo import ConectaSQL_Produccion


def articuloPlano_all(cod_formulado, cod_grupo, cod_area):
    try:
        sql = f" SELECT"
        sql += f" ROW_NUMBER () OVER (ORDER BY af.articulo ASC) AS ROW_NUM,"
        sql += f" af.cod_articulo as COD_ARTICULO,"
        sql += f" af.articulo as ARTICULO,"
        sql += f" '' as UMU,"
        sql += f" af.cod_subarea as COD_AREA,"
        sql += f" sa.nom_subarea as AREA,"
        sql += f" af.id_grupo as COD_GRUPO,"
        sql += f" ags.grupo as GRUPO,"
        sql += f" ISNULL(ap.cod_plano,'') as cod_plano,"
        sql += f" ISNULL(ap.id_version,'') as id_version,"
        sql += f" ISNULL(ap.sw_est_plano,'') as sw_est_plano,"
        sql += f" isnull(left(ap.cod_plano,2),'NN') as tipo,"        
        sql += f" af.estacion as estacion,"
        sql += f" af.frecuencia_uso as frecuencia_uso"
        sql += f" FROM articulo_familia af"
        sql += f" LEFT OUTER JOIN articulo_plano ap on ap.cod_articulo = af.cod_articulo and ap.sw_estado = '1' AND ap.sw_est_plano IN ('P','O')"
        sql += f" LEFT OUTER JOIN sub_area sa on sa.cod_subarea = af.cod_subarea"
        sql += f" LEFT OUTER JOIN articulo_grupo_sap ags on ags.id_grupo = af.id_grupo"
        sql += f" WHERE af.sw_maneja_plano = '1'"
        # if cod_formulado != "0":
        sql += f" AND af.tipo_formulado = '" + cod_formulado + "'"
        if cod_grupo != "0":
            sql += f" AND af.id_grupo = '" + cod_grupo + "'"
        if cod_area != "0":
            sql += f" AND af.cod_subarea = '" + cod_area + "'"
        sql += f" ORDER BY af.articulo"
        print(sql)
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())

        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."

def articuloPlano_nuevo_codigo(tipo,cod_articulo):
    try:
        sql = f" select "
        sql += f" ( "
        sql += f" '{tipo}'+ag.abreviatura+'-'+sa.abreviatura+ "
        sql += f" ( "
        sql += f" 	select RIGHT('000' + cast(cast(isnull(SUBSTRING(max(cod_plano),8,5),'0') as int) + 1 as varchar),3) "
        sql += f" 	from articulo_plano ap2 "
        sql += f" 	inner join articulo_familia af2 on af2.cod_articulo = ap2.cod_articulo "
        sql += f" 	where af2.cod_subarea = af.cod_subarea "
        sql += f" ) "
        sql += f" ) as nuevo_codigo "
        sql += f" from articulo_familia af "
        sql += f" inner join articulo_grupo_sap ag on ag.id_grupo = af.id_grupo "
        sql += f" left outer join sub_area sa on sa.cod_subarea = af.cod_subarea "
        sql += f" where af.cod_articulo = %s "
        
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, (cod_articulo,))
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."


def articuloPlano_cod_articulo(cod_articulo, tipo, db: Session):
    try:
        sql = """ SELECT articulo,cod_articulo,cod_plano,cod_usuario,fecre,femod,glosa,id_art_plano,id_version,sw_est_plano,sw_estado, left(cod_plano,2) as tipo FROM articulo_plano WHERE cod_articulo = %s and left(cod_plano,2) = %s and sw_estado = 1 order by id_version desc """
        print(sql)
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, (cod_articulo,tipo,))
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."
    

def articuloPlano_insert(dt_articulo_plano, db: Session):
    try:
        print(dt_articulo_plano)
        # añadir valores del diccionario a la lista
        lista_datos = []
        lista_datos.append(dt_articulo_plano.cod_articulo)
        lista_datos.append(dt_articulo_plano.id_version)
        lista_datos.append(dt_articulo_plano.articulo)
        lista_datos.append(dt_articulo_plano.cod_plano)
        lista_datos.append(dt_articulo_plano.glosa)
        lista_datos.append(dt_articulo_plano.cod_usuario)

        # ejecutar procedimiento
        me_conexion = conexion_mssql()
        resultados = me_conexion.ejecutar_store_crud("ARTICULO_PLANO_INSERT", lista_datos)
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def articuloPlano_update_estado(id_art_plano, dt_articulo_plano, db: Session):
    try:
        # añadir valores del diccionario a la lista
        lista_datos = []
        lista_datos.append(id_art_plano)
        # lista_datos.append(dt_articulo_plano.id_art_plano)
        lista_datos.append(dt_articulo_plano.sw_est_plano)
        lista_datos.append(dt_articulo_plano.cod_usuario)

        # ejecutar procedimiento
        me_conexion = conexion_mssql()
        resultados = me_conexion.ejecutar_store_crud(
            "ARTICULO_PLANO_UPDATE_ESTADO", lista_datos
        )
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."

   


def articuloPlano_delete(id_art_plano, db: Session):
    try:
        sql = """ UPDATE articulo_plano SET sw_estado = '9' WHERE id_art_plano = %s AND sw_est_plano = 'R' """
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.ejecutar_funciones(sql, (id_art_plano,))
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."

def GenerarIDVersion(ptipo:str,pcod_articulo:str):
    try:
        sql = f"select isnull(count(*),0) + 1 as version from articulo_plano where cod_Articulo = '{pcod_articulo}' and left(cod_plano,2)='{ptipo}'"

        conn = ConectaSQL_Produccion()

        cur_ver = conn.cursor(as_dict=True)

        cur_ver.execute(sql)

        version_id = 1

        for r in cur_ver:
            version_id = r['version']

        return version_id
    except Exception as err:
        return f"Error en la operación : {err}."