from conexiones.conexion_sap import conexion_sap
from datetime import datetime
from produccion.functions import subAreaFunction


# variables
v_db = "SBO_TECNO_PRODUCCION"


def ordenFabricacionPrograma(dt_programa):
    # variables
    v_db = "SBO_TECNO_PRODUCCION"
    dt_resultado = []
    # fecha = datetime.strptime(dt_programa.fecha, "%Y-%m-%d")
    date_object = datetime.strptime(dt_programa.fecha, "%Y-%m-%d").date()
    c_ejercicio = date_object.year
    c_periodo = date_object.month

    # buscar areas de supervisor responsable
    if dt_programa.cod_propietario != "0":
        dt_areas = subAreaFunction.get_subarea_propietario_codigo(
            dt_programa.cod_propietario
        )

    if dt_programa.cod_subarea != "0" or dt_programa.cod_propietario == "0":
        dt_areas = [{"cod_subarea_sap": dt_programa.cod_subarea}]

    # consultar en base de datos SAP
    if len(dt_areas) > 0:
        for row in dt_areas:
            sql = (
                f"SELECT * FROM "
                + v_db
                + ".SGC_PRODUCCION_PROGRAMA_MENSUAL_COSTO(%s,%s,%s,%s,%s)"
                % (
                    c_ejercicio,
                    c_periodo,
                    dt_programa.id_grupo,
                    row["cod_subarea_sap"],
                    dt_programa.sw_todo,
                )
            )
            me_conexion = conexion_sap()
            res_datos_sap = me_conexion.consultas_sgc(sql, ("",))
            dt_resultado.extend(res_datos_sap)
    else:
        sql = (
            f"SELECT * FROM "
            + v_db
            + ".SGC_PRODUCCION_PROGRAMA_MENSUAL_COSTO(%s,%s,%s,%s,%s)"
            % (c_ejercicio, c_periodo, dt_programa.id_grupo, "0", dt_programa.sw_todo)
        )
        res_datos_sap = me_conexion.consultas_sgc(sql, ("",))
        dt_resultado.extend(res_datos_sap)

    # crear datos calculados
    lista_datos = list(
        map(
            lambda p: {
                "cod_articulo": p["COD_ARTICULO"],
                "articulo": p["ARTICULO"],
                "umu": p["UMU"],
                "cod_grupo": p["COD_GRUPO"],
                "grupo": p["GRUPO"],
                "cod_area": p["COD_AREA"],
                "area": p["AREA"],
                "lote_min": p["LOTE_MIN"],
                "und_x_hora": p["UND_X_HORA"],
                "hora_proceso": p["HORA_PROCESO"],
                "abc": p["ABC"],
                "ranking": p["RAKING"],
                "cpdh": p["CPDH"],
                "cant_stock": p["CANT_STOCK"],
                "cant_proceso": p["CANT_PROCESO"],
                "cant_plan": p["CANT_PLAN"],
                "cant_real": p["CANT_REAL_OK"] + p["CANT_REAL_PREPROCESO"],
                "cant_real_plan": (
                    float(p["CANT_PLAN"])
                    if float(p["CANT_REAL_OK"] + p["CANT_REAL_PREPROCESO"])
                    > float(p["CANT_PLAN"])
                    else float(p["CANT_REAL_OK"] + p["CANT_REAL_PREPROCESO"])
                ),
                "cant_pendiente": (
                    float(0)
                    if float(p["CANT_REAL_OK"] + p["CANT_REAL_PREPROCESO"])
                    > float(p["CANT_PLAN"])
                    else float(p["CANT_PLAN"])
                    - float(p["CANT_REAL_OK"] + p["CANT_REAL_PREPROCESO"])
                ),
                "cant_avance": (
                    float(0)
                    if float(p["CANT_PLAN"]) <= 0
                    else round(
                        (float(p["CANT_REAL_OK"] + p["CANT_REAL_PREPROCESO"]) * 100)
                        / float(p["CANT_PLAN"]),
                        2,
                    )
                ),
                "semaforo": asignar_semaforo(p),
                "hora_plan": float(p["CANT_PLAN"] * p["HORA_PROCESO"]),
                "hora_real": float(
                    (p["CANT_REAL_OK"] + p["CANT_REAL_PREPROCESO"]) * p["HORA_PROCESO"]
                ),
                "costo" : p["COSTO"]
            },
            list(dt_resultado),
        )
    )

    # ordenar segun planificado de mayor a menor
    resultado_orden = sorted(lista_datos, key=lambda x: x["cant_avance"], reverse=True)

    return resultado_orden


def asignar_semaforo(x):
    c_plan = float(x["CANT_PLAN"])
    c_real = float(x["CANT_REAL_OK"] + x["CANT_REAL_PREPROCESO"])
    c_avance = float(0) if c_plan <= 0 else (c_real * 100) / c_plan
    c_semaforo = (
        "#2A2A29"
        if c_avance > 100
        else "#0DB81E"
        if c_avance >= 93 and c_avance <= 100
        else "#F9F516"
        if c_avance >= 85 and c_avance < 93
        else "#F91616"
        if c_avance < 85 and c_plan > 0
        else "#FFF"
    )
    # print(c_semaforo)
    return c_semaforo


def ordenFabricacion_pendiente(cod_subarea):
    cod_subarea_sap = ""
    dt_areas = subAreaFunction.get_subarea_id(cod_subarea)
    for row in dt_areas:
        cod_subarea_sap = str(row["cod_subarea_sap"])

    sql = """ select"""
    sql += """ OFA."DocNum" AS "num_of","""
    sql += """ OFA."ItemCode" AS "cod_articulo","""
    sql += """ OFA."ProdName" AS "articulo","""
    sql += """ OFA."Status" AS "estado","""
    sql += """ ("""
    sql += """ 	CASE WHEN OFA."Status" = 'C' THEN 'CANCELADO'"""
    sql += """ 		 WHEN OFA."Status" = 'L' THEN 'CERRADO'"""
    sql += """ 		 WHEN OFA."Status" = 'P' THEN 'PLANIFICADO'"""
    sql += """ 		 WHEN OFA."Status" = 'R' THEN 'LIBERADO'"""
    sql += """ 		 ELSE ''"""
    sql += """ 	END"""
    sql += """ ) AS "estado_desc","""
    sql += """ TO_VARCHAR(OFA."PostDate",'YYYY/MM/DD') AS "fecha_doc","""
    sql += """ ART."U_MSSC_ARE" AS "cod_area","""
    sql += """ ("""
    sql += """ 	select "Descr" from """ + v_db + """.UFD1 """
    sql += """ 	WHERE "TableID" = 'OITM' AND "FieldID" = '21' AND "FldValue" = ART."U_MSSC_ARE" """
    sql += """ ) AS "area" """
    sql += """ from """ + v_db + """.OWOR as OFA"""
    sql += (
        """ INNER JOIN """
        + v_db
        + """.OITM AS ART ON ART."ItemCode" = OFA."ItemCode" """
    )
    sql += """ where OFA."Status" in ('P','R')"""
    sql += """ and OFA."Series" = '126' """
    sql += """ and ART."U_MSSC_ARE" = '""" + cod_subarea_sap + """' """

    sql += """ order by OFA."PostDate" desc,  OFA."DocNum" asc """
    me_conexion = conexion_sap()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta

def getCostoFabricacion(dt_Programa):
    try:
        #print(dt_Programa)
        sql = ""

        sql =  "SELECT sum(C.Costo_Produccion_Personal) as CostoMO FROM (SELECT DISTINCT FD.\"Code\",MO.\"AvgPrice\" AS Costo_Produccion_Personal FROM SBO_TECNO_PRODUCCION.ITT1 FD "
        sql += "INNER JOIN SBO_TECNO_PRODUCCION.OITM AR ON AR.\"ItemCode\" = FD.\"Father\" LEFT OUTER JOIN SBO_TECNO_PRODUCCION.OITM MO ON MO.\"ItemCode\" = FD.\"Code\" "
        sql +=	f"WHERE MO.\"ItemCode\" LIKE 'GG%' AND ((0={dt_Programa.cod_subarea}) OR (AR.U_MSSC_ARE = {dt_Programa.cod_subarea}))) C "

        me_conexion = conexion_sap()
        resultadosQ = me_conexion.consultas_sgc(sql,())
        #print(resultadosQ)
        return resultadosQ
    except Exception as err :
        return f"Error en la operación : {err}."
