from conexiones.conexion_mmsql import conexion_mssql


def insert(meData):
    try:
        valores = []
        valores.append(meData.tipo_equipo)
        valores.append(meData.cod_usuario)
        sql = "insert into tipo_equipo(tipo_equipo,cod_usuario,fecre) values(%s,%s,getdate())"

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def update(meData):
    try:
        valores = []
        valores.append(meData.tipo_equipo)
        valores.append(meData.id_tpequipo)
        sql = "update tipo_equipo set "
        sql += "tipo_equipo=%s "
        sql += "where id_tpequipo=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def delete(meData):
    try:
        valores = []
        valores.append(meData.id_tpequipo)
        sql = "delete tipo_equipo "
        sql += "where id_tpequipo=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."