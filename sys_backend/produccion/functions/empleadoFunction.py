from conexiones.conexion_mmsql import conexion_mssql


def insert(meData):
    try:
        valores = []
        valores.append(meData.cod_empleado)
        valores.append(meData.cod_subarea)
        valores.append(meData.nombre)
        valores.append(meData.sw_estado)


        sql = "insert into empleado(cod_empleado, cod_subarea, nombre, sw_estado) values(%s, %s, %s, %s)"

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        #print(sql)
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def update(meData):
    try:
        valores = []
        valores.append(meData.nombre)
        valores.append(meData.sw_estado)
        valores.append(meData.cod_empleado)
        sql = "update empleado set "
        sql += " nombre=%s, "
        sql += " sw_estado=%s "
        sql += " where cod_empleado=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        #print(sql)
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def delete(meData):
    try:
        valores = []
        valores.append(meData.cod_empleado)
        sql = " delete from empleado "
        sql += " where cod_empleado=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        #print(sql)
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."