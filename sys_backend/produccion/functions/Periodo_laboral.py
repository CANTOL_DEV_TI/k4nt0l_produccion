from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql


def periodo_laboral_all(cod_turno, cod_subarea):
    try:
        sql = f" SELECT pl.*, sa.nom_subarea, tc.nom_turno FROM periodo_laboral pl"
        sql += f" INNER JOIN sub_area sa ON sa.cod_subarea = pl.cod_subarea"
        sql += f" INNER JOIN turno_cab tc ON tc.cod_turno = pl.cod_turno"
        sql += f" WHERE pl.sw_estado in ('1','0')"
        if cod_turno != "0":
            sql += f" AND pl.cod_turno = '" + cod_turno + "'"
        if cod_subarea != "0":
            sql += f" AND pl.cod_subarea = '" + cod_subarea + "'"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."

def periodo_laboral_turno(cod_subarea):
    try:
        sql = f" SELECT distinct pl.cod_turno, tc.nom_turno, sa.nom_subarea FROM periodo_laboral pl"
        sql += f" INNER JOIN sub_area sa ON sa.cod_subarea = pl.cod_subarea"
        sql += f" INNER JOIN turno_cab tc ON tc.cod_turno = pl.cod_turno"
        sql += f" where pl.sw_estado in ('1')"
        sql += f" and pl.ejercicio = year(getdate())"
        if cod_subarea != "0":
            sql += f" AND pl.cod_subarea = '" + cod_subarea + "'"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."


def periodo_laboral_activo():
    sql = f"SELECT * FROM periodo_laboral WHERE sw_estado in ('1')"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta


def periodo_laboral_insert(dt_periodo_laboral):
    # DATOS
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(dt_periodo_laboral.ejercicio)
    lista_cab.append(dt_periodo_laboral.periodo)
    lista_cab.append(dt_periodo_laboral.cod_subarea)
    lista_cab.append(dt_periodo_laboral.cod_turno)
    lista_cab.append(dt_periodo_laboral.total_dia_lab)
    lista_cab.append(dt_periodo_laboral.sw_estado)
    lista_cab.append(dt_periodo_laboral.cod_usuario)

    # ejecutar cabecera
    v_sql = f"INSERT INTO periodo_laboral (ejercicio,periodo,cod_subarea,cod_turno,total_dia_lab,sw_estado,cod_usuario)"
    v_sql += f" VALUES (%s, %s, %s, %s, %s, %s, %s)"
    me_conexion = conexion_mssql()
    resultados = me_conexion.ejecutar_funciones(v_sql, tuple(lista_cab))
    return resultados


def periodo_laboral_insert_sub_area(dt_periodo_laboral):
    # DATOS
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(dt_periodo_laboral.ejercicio)
    lista_cab.append(dt_periodo_laboral.periodo)
    lista_cab.append(dt_periodo_laboral.cod_turno)
    lista_cab.append(dt_periodo_laboral.total_dia_lab)
    lista_cab.append(dt_periodo_laboral.sw_estado)
    lista_cab.append(dt_periodo_laboral.cod_usuario)

    # ejecutar
    me_conexion = conexion_mssql()
    resultados = me_conexion.ejecutar_store_crud(
        "PRODUCCION_PERIODO_LABORAL_INSERT_SUBAREA", tuple(lista_cab)
    )
    return resultados


def periodo_laboral_update(dt_periodo_laboral):
    # CABECERA
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(dt_periodo_laboral.total_dia_lab)
    lista_cab.append(dt_periodo_laboral.sw_estado)
    lista_cab.append(dt_periodo_laboral.cod_usuario)
    lista_cab.append(dt_periodo_laboral.id_per_lab)

    # ejecutar cabecera
    v_sql = f"UPDATE periodo_laboral SET total_dia_lab = %s, sw_estado = %s, cod_usuario = %s WHERE id_per_lab = %s"
    me_conexion = conexion_mssql()
    resultados = me_conexion.ejecutar_funciones(v_sql, tuple(lista_cab))
    return resultados


def periodo_laboral_delete(id_per_lab):
    sql = f"DELETE FROM periodo_laboral WHERE id_per_lab = %s"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.ejecutar_funciones(sql, (id_per_lab,))
    return resultadosConsulta
