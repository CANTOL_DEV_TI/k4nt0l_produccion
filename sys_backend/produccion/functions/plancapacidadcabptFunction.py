from conexiones import conexion_sql_solo
from fastapi import HTTPException
from typing import Union


def listado_plan_capacidad_pt(pEjercicio: Union[str, None] = 0):
    try:
        reporte_total = []
        reporte_r = dict()

        conn = conexion_sql_solo.ConectaSQL_Produccion()

        reporte_w = conn.cursor(as_dict=True)

        cad_sql = " select id_plan_capacidad as codigo_planificacion,"
        cad_sql += f" CAST (tpu.ejercicio_tarea as varchar(4)) + '-' + cast( tpu.periodo_tarea as varchar(2)) as periodo_planificacion,"
        cad_sql += f" case sw_estado when 'A' then 'ABIERTO' when 'C' then 'CERRADO' end as Estado,"
        cad_sql += f" ("
        cad_sql += f" 	ISNULL((select TOP 1 CASE WHEN pcpp.id_plan_capacidad_pp IS NULL THEN 0 ELSE pcpp.id_plan_capacidad_pp END"
        cad_sql += f" 	from plan_capacidad_pp_cab pcpp where pcpp.id_plan_capacidad = pcpc.id_plan_capacidad"
        cad_sql += f" 	ORDER BY id_plan_capacidad_pp DESC),0)"
        cad_sql += f" ) as id_plan_capacidad_pp"
        cad_sql += f" from plan_capacidad_pt_cab pcpc "
        cad_sql += f"inner join tarea_programada_usuario tpu on tpu.id_tarea = pcpc .id_tarea where year(fecha_emision) = {pEjercicio}"

        reporte_w.execute(cad_sql)

        for row in reporte_w:
            reporte_r = {
                "codigo_planificacion": row["codigo_planificacion"],
                "periodo_planificacion": row["periodo_planificacion"],
                "Estado": row["Estado"],
                "id_plan_capacidad_pp": row["id_plan_capacidad_pp"],
            }
            reporte_total.append(reporte_r)

        conn.close()

        return reporte_total
    except:
        return HTTPException(
            status_code=404,
            detail="Errores en la consulta, contacte con el administrador",
        )
