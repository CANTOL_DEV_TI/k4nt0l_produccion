from hdbcli import dbapi


class conexion_sap:

    def __init__(self):
        self.conSap = dbapi.connect(
            address="192.168.5.2",
            port=30013,
            user="B1ADMIN",
            password="C4nt012019$%$.",
            databasename='NDB'
        )
        self.curSap = self.conSap.cursor()

    def ejecutar_funciones(self, vSql, vValores):
        self.curSap.execute(vSql, vValores)
        self.conSap.commit()
        resultado = self.curSap.fetchall()[0][0]
        # self.cerrando()
        return resultado

    def consultas(self, vSql, vValores):
        self.curSap.execute(vSql, vValores)
        resultado = self.curSap.fetchall()
        #self.cerrando()
        return resultado
    
    def consultas_sgc(self, vSql, vValores):
        resultado = []
        with self.curSap as cursor:
            cursor.execute(vSql, vValores)
            columns = [column[0] for column in cursor.description]
            for row in cursor.fetchall():
                resultado.append(dict(zip(columns, row)))
            
        self.cerrando()
        return resultado

    def consultas_RPT(self, vSql):
        self.curSap.execute(vSql)
        cabeceras = self.curSap.description
        datos = self.curSap.fetchall()
        #self.cerrando()
        return {'cabeceras': cabeceras, 'datos': datos}

    def ejecutar_conReturnKey(self, vTabla, vCampoKey, vSql, vValores):
        self.curSap.execute(vSql, vValores)
        self.conSap.commit()
        vKey = self.hallando_key(vTabla, vCampoKey)
        # self.cerrando()
        return vKey

    def hallando_key(self, vTabla, vCampoKey):
        # HALLANDO CODIGO COMPRA ASIGNADO AUTO
        vSql = "select currval(pg_get_serial_sequence('" + \
            vTabla + "', '" + vCampoKey + "'));"
        self.curSap.execute(vSql)
        for row in self.curSap.fetchall():
            return row[0]

    def ejecutar_sinReturnKey(self, vSql, vValores):
        self.curSap.execute(vSql, vValores)
        self.conSap.commit()
        # self.cerrando()

    def cerrando(self):
        self.curSap.close()
        self.conSap.close()


# aa = conexion_sap()
# resultado = aa.consultas('select * from DISTRIMAX_PRUEB_INTEGR.OPLN', ('',))
# aa.cerrando()
#
# for row in resultado:
#     print(row)


# resultado = []
# sql = "select * from SBO_DISTRI_PRODUCCION.CtasCobrar_X_Cliente('C10409796694')"
# me_conexion = conexion_sap()
# resultadosFactura = me_conexion.consultas(sql, ('',))
#
# for val in resultadosFactura:
#     resultado.append({'llave': val[0], 'name':val[1], 'token': None})
#
# print(resultado)
