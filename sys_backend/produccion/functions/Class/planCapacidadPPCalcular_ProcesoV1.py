from conexiones.conexion_mmsql import conexion_mssql
import calendar

class planCapacidad_PI:
    def __init__(self, id_plan_capacidad_pp):
        self.__meConexionSql              = conexion_mssql()
        self.__id_plan_capacidad_pp       = id_plan_capacidad_pp
        self.__disponibilidadMaquinas     = {}

        self.__ejercicio                  = None
        self.__periodo                    = None

        self.__programacionProducto       = {}
        self.__maquinasProgramadas        = []
        self.__autoID_ProgramacionMaquina = 0



    @property
    def atributos(self):
        dic_Atributos = {"Tiempo Disponible Maquinas": self.__disponibilidadMaquinas,
                         "Maquinas Programadas": self.__maquinasProgramadas
                         }

        return dic_Atributos

    def get_ObtenerPeriodoPlanificacion(self):
        sql = " select "
        sql += " ejercicio_tarea, " #0
        sql += " periodo_tarea "    #1
        sql += " from plan_capacidad_pp_cab ppc "
        sql += " inner join plan_capacidad_pt_cab ptc ON ptc.id_plan_capacidad=ppc.id_plan_capacidad "
        sql += " inner join tarea_programada_usuario tp ON tp.id_tarea=ptc.id_tarea "
        sql += " where ppc.id_plan_capacidad_pp={} ".format(self.__id_plan_capacidad_pp)
        resultado = self.__meConexionSql.consultas(sql, ("",))
        #print("get_obtenerPeriodo ", resultado)
        for row in resultado:
            self.__ejercicio = row[0]
            self.__periodo   = row[1]


    def get_DisponibilidadMaquinas(self):
        # sql = " select id_equipo, cod_subarea, oferta_horas_x_dia, total_turnos, disponibilidad_historico "
        # sql += " from equipo "
        # sql += " order by id_equipo"
        sql = " select "
        sql += " e.id_equipo, "
        sql += " e.cod_subarea, "
        sql += " e.oferta_horas_x_dia, "
        sql += " e.total_turnos, "
        sql += " e.disponibilidad_historico, "
        # sql += " (select sum(cantidad) from preventivo_cab pc inner join preventivo_det pd on pc.id_preventivo=pd.id_preventivo where YEAR(pc.fecha_plan)={} and MONTH(pc.fecha_plan)={} and pd.id_equipo=e.id_equipo) ".format(self.__ejercicio, self.__periodo)
        sql += " (select sum(pd.cantidad) "
        sql += " from preventivo_cab pc "
        sql += " inner join preventivo_det pd on pc.id_preventivo=pd.id_preventivo "
        sql += " where pc.id_preventivo=(select id_preventivo from plan_capacidad_pp_cab where id_plan_capacidad_pp={}) ".format(self.__id_plan_capacidad_pp)
        sql += " and pd.id_equipo=e.id_equipo) "

        sql += " from equipo e "
        sql += " order by id_equipo "

        resultado1 = self.__meConexionSql.consultas(sql, ("",))
        for row1 in resultado1:
            totalHoras = self.hallarCalendario(ejercicio=self.__ejercicio, periodo=self.__periodo, cod_subarea=row1[1], top=row1[3])
            #print("##TH")
            #print(totalHoras)
            #print("#####")
            disponibilidadMaquina_Historico = 0.00 if row1[4] is None else (float(row1[4])/100.00)
            horasNoDisponibles_MantPreventivo = 0.00 if row1[5] is None else float(row1[5])
            horasDisponibles = (sum(totalHoras.values()) * disponibilidadMaquina_Historico) - horasNoDisponibles_MantPreventivo
            self.__disponibilidadMaquinas[row1[0]] = {"horas disponibilidad": horasDisponibles,
                                                      "horas consumido"     : 0.00
                                                      }

    def ejecutar(self, dicProducto):
        if dicProducto['Cant Objetivo'] <= 0:
            pass
        
        for nroPaso, machinePrioridad in sorted(dicProducto['pasos'].items()):
            self.planificar(dicProducto['id_item_pp'], nroPaso, dicProducto['Cant Objetivo'], machinePrioridad)

        # return self.__maquinasProceso
        #print("="*50)
        #print(self.__programacionProducto)
        self.__maquinasProgramadas = []
        for nroPaso, PrioridadMaquinas in self.__programacionProducto["pasos"].items():
            for nroPrioridad, maquina in PrioridadMaquinas.items():
                self.__autoID_ProgramacionMaquina += 1
                sqlTemp = "({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {})".format(self.__id_plan_capacidad_pp,
                                                                                    dicProducto['id_item_pp'],
                                                                                    self.__autoID_ProgramacionMaquina,
                                                                                    maquina['id_equipo'],
                                                                                    round(maquina['total_horas_oferta_x_maquina'], 2),
                                                                                    round(maquina['horas_consumidas'], 2),
                                                                                    round(maquina['cant_Programada'], 2),
                                                                                    int(maquina['num_paso']),
                                                                                    dicProducto['total_turnos'],
                                                                                    dicProducto['disponibilidad_historico'],
                                                                                    nroPrioridad,
                                                                                    maquina['std_und_hora'])
                self.__maquinasProgramadas.append(sqlTemp)

        ##Cambio hecho por raguirre
        self.grabar_PlanCapacidadPP_X_Maquina()



    def planificar(self, id_item_pp, nroPaso, cantObjetivo_Paso, maquinas):
        uso_EndMachine  = None
        undFabricacion  = None
        unidades_X_Hora = 0

        for ordenPrioridad, valores in sorted(maquinas.items()):
            if cantObjetivo_Paso <= 0:
                return

            horasDisponibles_X_Maquina = float(self.__disponibilidadMaquinas[valores["id_equipo"]]["horas disponibilidad"]) - float(self.__disponibilidadMaquinas[valores["id_equipo"]]["horas consumido"])
            ofertaUndFab = valores['std_und_hora'] * horasDisponibles_X_Maquina
            undFabricacion = ofertaUndFab if cantObjetivo_Paso > ofertaUndFab else cantObjetivo_Paso

            self.__programacionProducto['pasos'][nroPaso][ordenPrioridad]['cant_Programada'] = undFabricacion
            uso_EndMachine = ordenPrioridad
            cantObjetivo_Paso -= undFabricacion
            self.__disponibilidadMaquinas[valores["id_equipo"]]["horas consumido"] += (undFabricacion/valores['std_und_hora'])

            self.__programacionProducto['pasos'][nroPaso][uso_EndMachine]['total_horas_oferta_x_maquina'] = float(self.__disponibilidadMaquinas[valores["id_equipo"]]["horas disponibilidad"])
            self.__programacionProducto['pasos'][nroPaso][uso_EndMachine]['horas_consumidas'] += (undFabricacion/valores['std_und_hora'])
            unidades_X_Hora = valores['std_und_hora']

        if cantObjetivo_Paso > 0:
            self.__programacionProducto['pasos'][nroPaso][uso_EndMachine]['cant_Programada'] += cantObjetivo_Paso
            self.__programacionProducto['pasos'][nroPaso][uso_EndMachine]['horas_consumidas'] += (cantObjetivo_Paso/unidades_X_Hora)


        return


    def planificar_x_Maquinaria(self):
        sql = " select "
        sql += " pcd.cod_articulo, "    #0
        sql += " pcd.cant_planificada, "    #1
        sql += " cd.num_paso, "    #2
        sql += " cd.num_prioridad, "    #3
        sql += " cd.std_und_hora, "    #4
        sql += " e.id_equipo, "    #5
        sql += " e.total_turnos, "   #6
        sql += " e.oferta_horas_x_dia, " #7
        sql += " pcd.id_item_pp, "  #8
        sql += " e.disponibilidad_historico "  #9
        sql += " from plan_capacidad_pp_det pcd "
        sql += " inner join config_proceso_cab cc ON cc.cod_articulo=pcd.cod_articulo "
        sql += " inner join config_proceso_det cd ON cd.id_fmproceso=cc.id_fmproceso "
        sql += " inner join equipo e ON e.id_equipo=cd.id_equipo "
        sql += " where "
        sql += " pcd.cod_grupo in (110) "
        sql += " and pcd.id_plan_capacidad_pp={}".format(self.__id_plan_capacidad_pp)
        sql += " order by "
        sql += " pcd.cod_articulo, "
        sql += " cd.num_paso, "
        sql += " cd.num_prioridad "
        #print("PLanificar por maquina{")
        #print(sql)
        #print("}PLanificar por maquina")
        resultado = self.__meConexionSql.consultas(sql, ("",))
        #print(resultado)
        self.get_ObtenerPeriodoPlanificacion()
        self.get_DisponibilidadMaquinas()
        self.__programacionProducto = {'Codigo': None}

        for row in resultado:            
            if self.__programacionProducto['Codigo'] != row[0]: # NO existe CODIGO PRODUCTO ?
                #print("No coincide")
                if self.__programacionProducto['Codigo'] != None:
                    #print("Entro...")
                    self.ejecutar(self.__programacionProducto)

                self.__programacionProducto = {'Codigo'                  : row[0],
                                               'Cant Objetivo'           : float(row[1]),
                                               'id_item_pp'              : row[8],
                                               'total_turnos'            : row[6],
                                               'disponibilidad_historico': row[9],
                                               'pasos'                   : {}
                                               }


            if not row[2] in self.__programacionProducto['pasos']:      # NO existe NRO PASO ?
                self.__programacionProducto['pasos'][row[2]] = {}


            if not row[3] in self.__programacionProducto['pasos'][row[2]]:    # NO existe NRO PRIORIDAD ?
                # float(row[7]) * float(self.__disponibilidadMaquinas[row[5]])
                self.__programacionProducto['pasos'][row[2]][row[3]] = {'std_und_hora'                : float(row[4]),
                                                                        'total_turnos'                : int(row[6]),
                                                                        'totalDiasMensual'            : None,
                                                                        # No Utilizado
                                                                        # 'id_paso'                     : 0,
                                                                        'id_equipo'                   : int(row[5]),
                                                                        'num_paso'                    : int(row[2]),

                                                                        'total_horas_oferta_x_maquina': 0.00,
                                                                        'horas_consumidas'            : 0.00,
                                                                        'cant_Programada'             : 0.00,

                                                                        'num_prioridad'               : float(row[3])
                                                                        }


        return True



    def grabar_PlanCapacidadPP_X_Maquina(self):
        print("Pasos por maquina")
        sql = "insert into plan_capacidad_pp_det_xpaso(id_plan_capacidad_pp, id_item_pp, id_paso, id_equipo, total_horas_oferta_x_maquina, horas_consumidas, cant_programada, num_paso, total_turnos, disponibilidad_historico, num_prioridad, std_und_hora) values "
        sql += ", ".join(self.__maquinasProgramadas) + ";"

        # resultado = "ok"
        resultado = self.__meConexionSql.ejecutar_SinRetornoPK(sql, ('',))
        return {'grabo': resultado, 'sql': sql}


    def comprobacion(self):
        pass


    def hallarCalendario(self, **kwargs):
        sql = " select "
        sql += " td.num_dia, "
        sql += " sum(td.cant_hora_lab) "
        sql += " from periodo_laboral pl "
        sql += " inner join turno_cab tc ON tc.cod_turno=pl.cod_turno "
        sql += " inner join turno_det td ON td.cod_turno=tc.cod_turno "
        sql += " where "
        sql += " pl.ejercicio={} and pl.periodo={} ".format(kwargs['ejercicio'], kwargs['periodo'])
        sql += " and pl.cod_subarea={} ".format(kwargs['cod_subarea'])

        sql += " and tc.prioridad in (select TOP {} ".format(kwargs['top'])
        sql += " tc.prioridad "
        sql += " from periodo_laboral pl "
        sql += " inner join turno_cab tc ON tc.cod_turno=pl.cod_turno "
        sql += " where "
        sql += " pl.ejercicio={} and pl.periodo={} ".format(kwargs['ejercicio'], kwargs['periodo'])
        sql += " and pl.cod_subarea={} ".format(kwargs['cod_subarea'])
        sql += " group by pl.cod_turno, tc.prioridad "
        sql += " order by tc.prioridad asc) "
        sql += " group by td.num_dia "
        sql += " order by td.num_dia asc "


        #print("=="*10)
        #print(sql)
        #print("=="*10)

        resultado = self.__meConexionSql.consultas(sql, ("",))
        horas_X_Dia = {}
        for row in resultado:
            horas_X_Dia[int(row[0])-1] = float(row[1])


        meCalendario = calendar.Calendar()
        resultadoCalendario = {}
        for semana in meCalendario.monthdayscalendar(2023, 9):
            dia = -1
            for diaFecha in semana:
                dia += 1
                if diaFecha == 0:
                    continue
                resultadoCalendario[diaFecha] = horas_X_Dia[dia]



        sql = "select lista_periodos from feriados where ejercicio={} and periodo={}".format(kwargs['ejercicio'], kwargs['periodo'])
        #print(sql)
        resultadoFeriados = self.__meConexionSql.consultas(sql, ("",))[0][0]
        if len(str(resultadoFeriados).strip()) > 0:
            for diaFeriado in tuple(resultadoFeriados.split(",")):
                #print(tuple(resultadoFeriados.split(",")))
                #print(diaFeriado)
                resultadoCalendario[int(diaFeriado)] = 0


        return resultadoCalendario


########################################################################################################

# aa = planCapacidad_PI(27, 2023, 9)
# aa.planificar_x_Maquinaria()






# data = {'Codigo'       : 'PP2117000094',
#         'Cant Objetivo': 30000,
#         'id_item_pp'   : 111,
#         'pasos'        : {11: {1: {'std_und_hora'             : 100,
#                                    'total_turnos'             : 1,
#                                    'oferta_HorasMaquina_X_Mes': 144,
#                                    'totalDiasMensual'         : 24,
#                                    'id_equipo'                : 1,
#                                    'total_horas_oferta_x_maquina': 0,
#                                    'horas_consumidas'          : 0,
#                                    'cant_Programada'          : 0
#                                    },
#
#                                2: {'std_und_hora'             : 100,
#                                    'total_turnos'             : 1,
#                                    'oferta_HorasMaquina_X_Mes': 144,
#                                    'totalDiasMensual'         : 24,
#                                    'cant_Programada'          : 0,
#                                    'id_equipo'                : 2
#                                    },
#
#                                3: {'std_und_hora'             : 100,
#                                    'total_turnos'             : 1,
#                                    'oferta_HorasMaquina_X_Mes': 144,
#                                    'totalDiasMensual'         : 24,
#                                    'cant_Programada'          : 0,
#                                    'id_equipo'                : 3
#                                    }
#                                },
#
#                           22: {1: {'std_und_hora'             : 100,
#                                    'total_turnos'             : 1,
#                                    'oferta_HorasMaquina_X_Mes': 144,
#                                    'totalDiasMensual'         : 24,
#                                    'cant_Programada'          : 0,
#                                    'id_equipo'                : 4
#                                    },
#
#                                2: {'std_und_hora'             : 100,
#                                    'total_turnos'             : 1,
#                                    'oferta_HorasMaquina_X_Mes': 144,
#                                    'totalDiasMensual'         : 24,
#                                    'cant_Programada'          : 0,
#                                    'id_equipo'                : 5
#                                    }
#                                }
#                           }
#         }
#
#