from conexiones.conexion_mmsql import conexion_mssql
from produccion.functions.Class.planCapacidadPTCalcular_ProcesoV4 import planCapacidadPTCalcular_ProcesoV4
import calendar
import math


class planCapacidadPTCalcular:
    def __init__(self):
        self.__meConexionSql = conexion_mssql()
        self.__id_plan_capacidad = "0"
        self.__ejercicio_tarea = 0
        self.__periodo_tarea = 0
        self.__diasPeriodo = []
        self.__horarios = []
        self.__list_Data = []

    def calcular_datos(self, id_plan_capacidad):
        self.__id_plan_capacidad = id_plan_capacidad
        self.get_horaLaboral()
        self.get_planCapacidadPTDet()

        classPlanificar = planCapacidadPTCalcular_ProcesoV4(self.__horarios, self.__ejercicio_tarea, self.__periodo_tarea)
        lista_detalle_dia = []
        c_dia = 0
        for item in self.__list_Data:
            # agrupar segun familia
            resultado_familia = []
            if item["procesado"] == "NO":
                for item_g in self.__list_Data:
                    if item_g["id_familia"] == item["id_familia"]:
                        resultado_familia.append(item_g)
                        item_g["procesado"] = "SI"

            # ordenar segun ABC del grupo seleccionado
            resultado_familia = sorted(resultado_familia, key=lambda x: x["abc"])

            # proceso de calculo
            # version 3
            print(len(resultado_familia))
            if len(resultado_familia) > 0:
                # lista_envio = {}
                # lista_envio["FAM" + str(item["id_familia"])] = resultado_familia
                lista_envio = resultado_familia
                print("%"*50)
                print(lista_envio)
                print("%" * 50)
                resultado = classPlanificar.planificacion_Cant_X_Dia(c_dia, lista_envio)
                if len(resultado[1].strip()) > 1:
                    lista_detalle_dia.append(resultado[1])
                c_dia = resultado[0]

            # version 2
            """

                lista_envio = {}                
                lista_envio["fechaUltimaProgramacion"] = c_dia
                lista_envio[str(item["id_familia"])] = resultado_familia
                # print(lista_envio)
                # print("#" * 30)
                resultado = self.planificacion_Cant_X_Dia(lista_envio)
                if len(resultado[1].strip()) > 1:
                    lista_detalle_dia.append(resultado[1])
                c_dia = resultado[0]
            """

            """
            # Version 1
            for item_g in resultado_familia:
                item_g["fechaUltimaProgramacion"] = c_dia
                resultado = self.planificacion_Cant_X_Dia(item_g)

                if len(resultado[1].strip()) > 1:
                    lista_detalle_dia.append(resultado[1])
                c_dia = resultado[0]
            """

        # insertar información
        completado = self.__meConexionSql.ejecutar_SinRetornoPK("".join(lista_detalle_dia), ("",))

        self.__meConexionSql.cerrando()
        if not completado:
            return completado

    # obtener detalle plan de capacidad de PT, ordenado por mayor diferencia y ABC
    def get_planCapacidadPTDet(self):
        sql = f"SELECT "
        sql += f" pcd.id_plan_capacidad, "
        sql += f" pcd.id_item, "
        sql += f" fa.id_familia, "
        sql += f" pcd.cod_articulo, "
        sql += f" pcd.abc, "
        sql += f" pcd.cant_und_hora, "
        sql += f" pcd.cant_lote_min, "
        sql += f" pcd.cant_planificada AS objetivo_produccion, "
        sql += f" 0 AS fechaUltimaProgramacion, "
        sql += f" 'NO' AS procesado "
        sql += f" FROM plan_capacidad_pt_det pcd "
        sql += f" INNER JOIN articulo_familia af ON af.cod_articulo=pcd.cod_articulo "
        sql += f" INNER JOIN familia fa ON af.id_familia = fa.id_familia "
        sql += f" WHERE pcd.id_plan_capacidad = " + str(self.__id_plan_capacidad) + " "
        sql += f" AND pcd.cant_planificada > 0 "
        sql += f" ORDER BY objetivo_produccion desc, pcd.abc ASC "
        valores = ("",)

        me_conexion = conexion_mssql()
        self.__list_Data = me_conexion.consultas_sgc(sql, valores)

    def get_horaLaboral(self):
        sql = f" select "
        sql += f" num_dia, "  # 0
        sql += f" sum(cant_hora_lab) as cant_hora_lab,"  # 1
        sql += f" tpu.ejercicio_tarea,"  # 2
        sql += f" tpu.periodo_tarea "  # 3
        sql += f" from turno_det td "
        sql += f" inner join periodo_laboral pl on pl.cod_turno = td.cod_turno "
        sql += f" inner join tarea_programada_usuario tpu on tpu.ejercicio_tarea = pl.ejercicio and tpu.periodo_tarea = pl.periodo "
        sql += f" inner join plan_capacidad_pt_cab pcc on pcc.id_tarea = tpu.id_tarea "
        sql += f" where pcc.id_plan_capacidad = " + str(self.__id_plan_capacidad) + " "
        sql += f" and pl.cod_subarea = 11 "  #  ensamble de cerraduras
        sql += f" group by "
        sql += f" num_dia, "
        sql += f" tpu.ejercicio_tarea, "
        sql += f" tpu.periodo_tarea "
        sql += f" order by num_dia "
        valores = ("",)

        me_conexion = conexion_mssql()
        resultado = me_conexion.consultas(sql, valores)

        resultado_hora = []
        self.__ejercicio_tarea = int(resultado[0][2])
        self.__periodo_tarea = int(resultado[0][3])


        for item in resultado:
            resultado_hora.append(item[1])

        self.__horarios = resultado_hora


    def hallar_DiasCalendario(self):
        calendario = calendar.Calendar()
        # self.__diasPeriodo = calendario.monthdayscalendar(2023, 8)
        self.__diasPeriodo = calendario.monthdayscalendar(
            self.__ejercicio_tarea, self.__periodo_tarea
        )

    def planificacion_Cant_X_Dia(self, dicValores):
        id_plan_capacidad = dicValores["id_plan_capacidad"]
        id_item = dicValores["id_item"]
        fechaUltimaProgramacion = dicValores["fechaUltimaProgramacion"]
        und_X_Hora = dicValores["cant_und_hora"]
        objetivoProduccion = dicValores["objetivo_produccion"]

        self.hallar_DiasCalendario()
        avanceObjetivo = 0
        listaInsert = []
        for semana in self.__diasPeriodo:
            diaSemana = 0
            for diaFecha in semana:
                if fechaUltimaProgramacion < diaFecha:
                    horas_X_Dia = self.__horarios[diaSemana]
                    total_Fab_X_Dia = math.ceil(horas_X_Dia * und_X_Hora)  # redondeo hacia ARRIBA
                    # total_Fab_X_Dia = math.floor(horas_X_Dia * und_X_Hora)  # redondeo hacia ABAJO
                    avanceObjetivo += total_Fab_X_Dia
                    if 0 < avanceObjetivo <= objetivoProduccion:
                        fechaUltimaProgramacion = diaFecha
                        listaInsert.append(
                            "(%s, %s, %s, %s, %s)"
                            % (
                                fechaUltimaProgramacion,
                                id_plan_capacidad,
                                id_item,
                                total_Fab_X_Dia,
                                horas_X_Dia,
                            )
                        )

                diaSemana += 1

        if len(listaInsert) > 0:
            sql = "insert into plan_capacidad_det_xdia(nro_dia, id_plan_capacidad, id_item, cant, horas_x_dia) values "
            sql += ",".join(listaInsert) + ";"
        else:
            sql = ""
        return [fechaUltimaProgramacion, sql]


# tutu = planCapacidadPTCalcular()

# print(tutu.get_horaLaboral())

# print(tutu.calcular_datos("1"))

