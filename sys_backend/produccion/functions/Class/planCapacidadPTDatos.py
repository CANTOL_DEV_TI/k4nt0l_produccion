from conexiones.conexion_mmsql import conexion_mssql
from datetime import date


class planCapacidadPTDatos:
    def __init__(self):
        self.__meConexionSql = conexion_mssql()
        self.__id_plan = None
        self.__id_plan_omitido = None
        self.__id_stock = None
        self.__id_tarea = None
        self.__num_dias_mes = 0
        self.__id_plan_capacidad = None
        self.__dic_Data = {}

    def insert(self):
        self.get_id_plan_venta()  # obtener id de ultimo plan de ventas
        self.get_id_omitido()  # obtener id de ultimo articulos omitidos
        self.get_id_stock_unificado()  # obtener id de ultimo registro de stock unificado
        self.get_id_tarea_usuario()  # obtener id de ultima tarea programada
        self.get_num_dias_mes()  # obtener el total de dias laborales del area de ensamble
        self.get_paramArticuloFamiliaOmitido()
        self.get_stockInicial()
        self.get_planVenta()
        self.get_call_parametro_stock()

        # Retornar id de cabecera
        sql = f"insert into plan_capacidad_pt_cab(fecha_emision, id_plan, id_stock, id_plan_omitido, id_tarea) "
        sql += f" values(GETDATE(),%s, %s, %s, %s);" % (
            self.__id_plan,
            self.__id_stock,
            self.__id_plan_omitido,
            self.__id_tarea,
        )

        sql += f"select IDENT_CURRENT('plan_capacidad_pt_cab');"
        resultado = self.__meConexionSql.ejecutar_ConRetornoPK(
            sql, ("",), "plan_capacidad_pt_cab"
        )
        if not resultado:
            return sql

        self.__id_plan_capacidad = resultado

        # Carga de detalle
        sql = f"INSERT INTO plan_capacidad_pt_det(id_plan_capacidad, id_item, cod_articulo, articulo, abc, cant_lote_min, cant_und_hora, cob_minimo_dia, cob_ideal_dia, cob_maximo_dia, cpdp, stock_minimo, stock_ideal_min, stock_ideal, stock_ideal_max, stock_maximo, stock_inicial_almacen, cant_plan_venta) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        item = 0
        lista_det = []
        for codigo, valores in self.__dic_Data.items():
            item += 1

            lista_det.append(
                (
                    self.__id_plan_capacidad,
                    item,
                    codigo,
                    valores["articulo"],
                    valores["abc"],
                    valores["cant_lote_min"],
                    valores["cant_und_hora"],
                    valores["cob_minimo_dia"],
                    valores["cob_ideal_dia"],
                    valores["cob_maximo_dia"],
                    valores["cpdp"],
                    valores["stock_minimo"],
                    valores["stock_ideal_min"],
                    valores["stock_ideal"],
                    valores["stock_ideal_max"],
                    valores["stock_maximo"],
                    valores["stock_inicial_almacen"],
                    valores["cant_plan_venta"],
                )
            )

        resultado = self.__meConexionSql.ejecutarAll_SinRetornoPK(sql, lista_det)
        self.__meConexionSql.cerrando()
        if not resultado:
            return resultado

        return self.__id_plan_capacidad

    def get_id_plan_venta(self):
        sql = f"SELECT CASE WHEN MAX(id_plan) IS NULL THEN 0 ELSE MAX(id_plan) END AS id_plan FROM plan_venta_cab"
        me_conexion = conexion_mssql()
        resultado = me_conexion.consultas_sgc(sql, ())
        for row in resultado:
            self.__id_plan = str(row["id_plan"])

    def get_id_omitido(self):
        sql = f"SELECT CASE WHEN MAX(id_plan_omitido) IS NULL THEN 0 ELSE MAX(id_plan_omitido) END AS id_plan_omitido FROM articulo_omitido_cab"
        me_conexion = conexion_mssql()
        resultado = me_conexion.consultas_sgc(sql, ())
        for row in resultado:
            self.__id_plan_omitido = str(row["id_plan_omitido"])

    def get_id_stock_unificado(self):
        sql = f"SELECT CASE WHEN MAX(id_stock) IS NULL THEN 0 ELSE MAX(id_stock) END AS id_stock FROM stock_unido_cab"
        me_conexion = conexion_mssql()
        resultado = me_conexion.consultas_sgc(sql, ())
        for row in resultado:
            self.__id_stock = str(row["id_stock"])

    def get_id_tarea_usuario(self):
        sql = f"SELECT CASE WHEN MAX(id_tarea) IS NULL THEN 0 ELSE MAX(id_tarea) END AS id_tarea FROM tarea_programada_usuario"
        me_conexion = conexion_mssql()
        resultado = me_conexion.consultas_sgc(sql, ())
        for row in resultado:
            self.__id_tarea = str(row["id_tarea"])

    def get_num_dias_mes(self):
        sql = f" SELECT CASE WHEN AVG(total_dia_lab) IS NULL THEN 0 ELSE AVG(total_dia_lab) END AS num_dia_lab  "
        sql += f" FROM periodo_laboral pl "
        sql += f" INNER JOIN tarea_programada_usuario tpu ON tpu.ejercicio_tarea = pl.ejercicio AND tpu.periodo_tarea = pl.periodo "
        sql += f" WHERE tpu.id_tarea = " + self.__id_tarea + " "
        sql += f" AND  cod_subarea = 11 "  # area de ensamble de cerradura

        me_conexion = conexion_mssql()
        resultado = me_conexion.consultas_sgc(sql, ())
        for row in resultado:
            self.__num_dias_mes = int(row["num_dia_lab"])

    # Consulta maestro [articulo_familia] y excluye el ultimo registro de tabla [articulo_omitido_cab y articulo_omitido_det]
    def get_paramArticuloFamiliaOmitido(self):
        sql = f" SELECT "
        sql += f" af.cod_articulo, "  # 0
        sql += f" af.articulo, "  # 1
        sql += f" abc, "  # 2
        sql += f" cant_lote_min, "  # 3
        sql += f" cant_und_hora, "  # 4
        sql += f" cob_minimo_dia, "  # 5
        sql += f" cob_ideal_dia, "  # 6
        sql += f" cob_maximo_dia, "  # 7
        sql += f" cpdp "  # 8
        sql += f" FROM articulo_familia af "
        sql += f" WHERE NOT EXISTS "
        sql += f" ( "
        sql += f" 	SELECT * FROM articulo_omitido_cab aoc "
        sql += f" 	INNER JOIN articulo_omitido_det aod ON aod.id_plan_omitido = aoc.id_plan_omitido "
        sql += f" 	WHERE aod.cod_articulo = af.cod_articulo  "
        sql += f" 	AND aod.id_plan_omitido = " + self.__id_plan_omitido + ""
        sql += f" ) "
        sql += f" AND af.id_familia != '14' " # EXCLUIR PRODUCTOS EN PROCESO
        valores = ("",)

        resultado = self.__meConexionSql.consultas(sql, valores)

        item = 0
        for row in resultado:
            item += 1
            self.__dic_Data[row[0]] = {
                "articulo": row[1],
                "abc": row[2],
                "cant_lote_min": row[3],
                "cant_und_hora": row[4],
                "cob_minimo_dia": row[5],
                "cob_ideal_dia": row[6],
                "cob_maximo_dia": row[7],
                "cpdp": row[8],
                "stock_minimo": 0,
                "stock_ideal_min": 0,
                "stock_ideal": 0,
                "stock_ideal_max": 0,
                "stock_maximo": 0,
                "stock_inicial_almacen": 0,
                "cant_plan_venta": 0,
                "cant_pendiente_fabricar": 0,
            }

    def get_stockInicial(self):
        sql = f" SELECT "
        sql += f" cod_articulo, "  # 0
        sql += f" stock_inicial "  # 1
        sql += f" FROM stock_unido_det where id_stock = " + self.__id_stock + " "
        valores = ("",)

        resultado = self.__meConexionSql.consultas(sql, valores)

        for row in resultado:
            if self.__dic_Data.get(row[0]) is not None:
                self.__dic_Data[row[0]]["stock_inicial_almacen"] = row[1]

    def get_planVenta(self):
        sql = f" select "
        sql += f" fecha_doc, "  # 0
        sql += f" cod_articulo, "  # 1
        sql += f" cantidad "  # 2
        sql += f" from plan_venta_det pvd "
        sql += f" where id_plan = " + self.__id_plan + "  "
        sql += f" and exists ( "
        sql += f"     select * from tarea_programada_usuario tpu  "
        sql += f"     where tpu.id_tarea = " + self.__id_tarea + "  "
        sql += f"     and tpu.ejercicio_tarea = year(pvd.fecha_doc) "
        sql += f"     and tpu.periodo_tarea = month(pvd.fecha_doc) "
        sql += f" ) "
        valores = ("",)

        resultado = self.__meConexionSql.consultas(sql, valores)

        for row in resultado:
            if self.__dic_Data.get(row[1]) is not None:
                self.__dic_Data[row[1]]["cant_plan_venta"] = row[2]
                if self.__num_dias_mes > 0:  # si el consumo es mayor a cero
                    self.__dic_Data[row[1]]["cpdp"] = round(
                        (int(row[2]) / (self.__num_dias_mes - 2))
                    )

    def get_call_parametro_stock(self):
        for codigo, valores in self.__dic_Data.items():
            self.__dic_Data[codigo]["stock_minimo"] = round(
                int(valores["cob_minimo_dia"]) * int(valores["cpdp"])
            )
            self.__dic_Data[codigo]["stock_ideal_min"] = round(
                int(valores["cob_ideal_dia"]) * int(valores["cpdp"]) * 0.9
            )
            self.__dic_Data[codigo]["stock_ideal"] = round(
                int(valores["cob_ideal_dia"]) * int(valores["cpdp"])
            )
            self.__dic_Data[codigo]["stock_ideal_max"] = round(
                int(valores["cob_ideal_dia"]) * int(valores["cpdp"]) * 1.1
            )
            self.__dic_Data[codigo]["stock_maximo"] = round(
                int(valores["cob_maximo_dia"]) * int(valores["cpdp"])
            )


# tutu = planCapacidadPTDatos()
# print(tutu.insert())
