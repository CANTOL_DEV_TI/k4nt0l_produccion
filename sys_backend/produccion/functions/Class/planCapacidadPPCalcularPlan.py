from conexiones.conexion_mmsql import conexion_mssql
from produccion.functions.Class.planCapacidadPPCalcularBoom import planCapacidadPPCalcularBoom
from produccion.functions.Class.planCapacidadPPStockUnificado import planCapacidadPPStockUnificado
import math


class planCapacidadPPCalcularPlan:
    def __init__(self):
        self.__list_articulo = []
        self.__list_articulo_boom = []

    def call_pp_plan(self, id_planCapacidadPT):
        # parametros de articulo
        print("call_pp_plan",id_planCapacidadPT)
        self.get_lista_articulo(id_planCapacidadPT)

        # ejecutar boom
        c_boom = planCapacidadPPCalcularBoom()
        self.__list_articulo_boom = c_boom.calcular_boom(id_planCapacidadPT)

        # ejecutar stock inicial
        c_stock_ini = planCapacidadPPStockUnificado()
        self.__list_articulo_boom = c_stock_ini.get_boom_stock(
            self.__list_articulo_boom
        )

        # calcular
        self.calcular_cpd_stock_plan()

        # print(self.__list_articulo_boom)
        return self.__list_articulo_boom

    def calcular_cpd_stock_plan(self):
        c_item = 0
        for fila in self.__list_articulo_boom:
            # declarar variables
            c_item += 1

            c_articulo = ""
            c_abc = ""
            c_cpdp = 1
            c_total_dia_lab = 0
            c_cant_lote_min = 0
            c_cob_minimo_dia = 0
            c_cob_ideal_dia = 0
            c_cob_maximo_dia = 0

            # obtener articulo
            c_list_art = list(
                xx
                for xx in self.__list_articulo
                if xx["cod_articulo"] == fila["cod_articulo"]
            )

            # asignar valores
            if len(c_list_art) > 0:
                c_articulo = c_list_art[0]["articulo"]
                c_abc = c_list_art[0]["abc"]
                c_total_dia_lab = c_list_art[0]["total_dia_lab"]
                c_cant_lote_min = c_list_art[0]["cant_lote_min"]
                c_cob_minimo_dia = c_list_art[0]["cob_minimo_dia"]
                c_cob_ideal_dia = c_list_art[0]["cob_ideal_dia"]
                c_cob_maximo_dia = c_list_art[0]["cob_maximo_dia"]

            # calcular
            c_stock_ini = float(fila["stock"])
            c_cant_boom = math.ceil(fila["cantidad"])
            if c_total_dia_lab > 0:
                c_cpdp = math.ceil(c_cant_boom / c_total_dia_lab)

            c_stock_min = round(c_cpdp * c_cob_minimo_dia)
            c_stock_ide = round(c_cpdp * c_cob_ideal_dia)
            c_stock_ide_min = round(c_stock_ide * 0.9)
            c_stock_ide_max = round(c_stock_ide * 1.1)
            c_stock_max = round(c_cpdp * c_cob_maximo_dia)

            # calcular plan
            if (c_stock_ini - c_cant_boom) < c_stock_ide:
                c_cant_plan = c_cant_boom + c_stock_ide - c_stock_ini
            else:
                c_cant_plan = 0

            # añadir columna
            fila["id_item_pp"] = c_item
            fila["articulo"] = c_articulo
            fila["abc"] = c_abc
            fila["cant_lote_min"] = float(c_cant_lote_min)
            fila["cpdp"] = float(c_cpdp)
            fila["stock_minimo"] = float(c_stock_min)
            fila["stock_ideal_min"] = float(c_stock_ide_min)
            fila["stock_ideal"] = float(c_stock_ide)
            fila["stock_ideal_max"] = float(c_stock_ide_max)
            fila["stock_maximo"] = float(c_stock_max)
            fila["stock_inicial_almacen"] = float(c_stock_ini)
            fila["cant_boom"] = float(c_cant_boom)
            fila["cant_planificada"] = float(c_cant_plan)

            # Eliminar los no necesarios
            del fila["cantidad"]
            del fila["cantidad_plan"]
            del fila["sw_procesado"]
            del fila["nivel"]
            del fila["stock"]


    def get_lista_articulo(self, id_planCapacidadPT):
        #print("entro a get_lista_Articulo")
        sql = f" select "
        sql += f" af.cod_articulo, "
        sql += f" af.articulo, "
        sql += f" af.cod_subarea, "
        sql += f" ( "
        sql += f" 	ISNULL( "
        sql += f" 	(select top 1 pl.total_dia_lab from periodo_laboral pl "
        sql += f" 	where pl.cod_subarea = af.cod_subarea "
        sql += (
            f" 	and (CONVERT(VARCHAR,pl.ejercicio) + CONVERT(VARCHAR,pl.periodo)) = ( "
        )
        sql += f" 		select case when (CONVERT(VARCHAR,tpu.ejercicio_tarea) + CONVERT(VARCHAR,tpu.periodo_tarea)) is null  "
        sql += f" 		then '' "
        sql += f" 		else (CONVERT(VARCHAR,tpu.ejercicio_tarea) + CONVERT(VARCHAR,tpu.periodo_tarea)) end "
        sql += f" 		from plan_capacidad_pt_cab pcc "
        sql += f" 		inner join tarea_programada_usuario tpu on tpu.id_tarea = pcc.id_tarea "
        sql += f" 		where pcc.id_plan_capacidad = '" + id_planCapacidadPT + "' "
        sql += f" 	)),24) "
        sql += f" ) as total_dia_lab, "
        sql += f" af.abc, "
        sql += f" af.cant_lote_min, "
        sql += f" af.cob_minimo_dia, "
        sql += f" af.cob_ideal_dia, "
        sql += f" af.cob_maximo_dia  "
        sql += f" from articulo_familia af "
        sql += f" where af.cod_articulo like 'PP%' "
        me_conexion = conexion_mssql()
        c_articulo = me_conexion.consultas_sgc(sql, ("",))
        print(sql)
        self.__list_articulo = c_articulo


############################### PRUEBA ###############################
# tutu = planCapacidadPPCalcularPlan()
# tutu.call_pp_plan("1")
