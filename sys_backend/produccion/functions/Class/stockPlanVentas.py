from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap import conexion_sap
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

class stock_PlanVentas:
    def __init__(self):
        self.__meConexionSql = conexion_mssql()

    def insert(self, id_StockUnificado, fecha):
        fechaProgramacion = datetime.strptime(fecha, '%Y-%m-%d')
        fechaPlanVenta = fechaProgramacion + relativedelta(months=-1)

        sql = " UPDATE stock_unido_det "
        sql += " SET stock_unido_det.planventas_stock=plan_venta_det.cantidad "
        sql += " FROM plan_venta_det "
        sql += " WHERE "
        sql += " stock_unido_det.cod_articulo=plan_venta_det.cod_articulo "
        sql += " AND YEAR(plan_venta_det.fecha_doc)=%s " %(fechaPlanVenta.year)
        sql += " AND MONTH(plan_venta_det.fecha_doc)=%s " %(fechaPlanVenta.month)
        sql += " AND stock_unido_det.id_stock=%s " %(id_StockUnificado)
        sql += " AND plan_venta_det.id_plan=(select max(id_plan) from plan_venta_cab) "
        #print("#" * 20)
        #print(sql)
        #print("#" * 20)
        resultado = self.__meConexionSql.ejecutar_SinRetornoPK(sql, ('',))
        return resultado


