from conexiones.conexion_mmsql import conexion_mssql

class edicionAutomatizadaPP:
    def __init__(self, id_plan_capacidad_pp, dic):
        self.__meConexionSql = conexion_mssql()
        self.__id_plan_capacidad_pp = id_plan_capacidad_pp
        self.__dicEdicionProductos = dic

    def editar(self):
        listaItemAfectados = []
        print(self.__dicEdicionProductos)
        for registros in self.__dicEdicionProductos:
            dicPasos = self.findPasos(registros['cod_articulo'])
            resultados = self.ejecutarEdicion(dicPasos['id_plan_capacidad_pp'], dicPasos['dicPasos'], registros['cantidad'])
            # respuesta = self.__meConexionSql.ejecutarAll_SinRetornoPK(resultados['sql'], ('', ))
            listaItemAfectados.extend(resultados['Lista Resultados'])

        # if respuesta:
        return listaItemAfectados
        # return respuesta


    def ejecutarEdicion(self, id_plan_capacidad_pp, dicPasos, cantidad):
        listaResultados = []
        sql = ""
        for llave, valores in dicPasos.items():
            cantidadEdicion = cantidad
            for llave2, valores2 in valores.items():
                if cantidad == 0:
                    break

                cantDescontar = abs(cantidadEdicion) if abs(cantidadEdicion) <= valores2["cant_programada"] else valores2["cant_programada"]
                cantidadEdicion += cantDescontar

                sql += " update plan_capacidad_pp_det_xpaso set "
                sql += " cant_programada=cant_programada+{} ".format(cantDescontar if cantidad >= 0 else 0-cantDescontar)
                sql += " where "
                sql += " id_equipo={} ".format(valores2["id_equipo"])
                sql += " and id_plan_capacidad_pp={} ".format(id_plan_capacidad_pp)
                sql += " and id_item_pp={} ".format(valores2["id_item_pp"])
                sql += " and id_paso={}; ".format(valores2["id_paso"])

                sql += " update plan_capacidad_pp_det_xpaso set "
                sql += " horas_consumidas=ROUND(cant_programada/std_und_hora, 2) "
                sql += " where "
                sql += " id_equipo={} ".format(valores2["id_equipo"])
                sql += " and id_plan_capacidad_pp={} ".format(id_plan_capacidad_pp)
                sql += " and id_item_pp={} ".format(valores2['id_item_pp'])
                sql += " and id_paso={}; ".format(valores2['id_paso'])

                dicInterno = {'id_item_pp'   : valores2['id_item_pp'],
                              'id_paso'      : valores2['id_paso'],
                              'id_equipo'    : valores2["id_equipo"],
                              'num_paso'     : valores2["num_paso"],
                              'num_prioridad': valores2["num_prioridad"]
                              }

                listaResultados.append(dicInterno)

        print(sql)
        return {'sql': sql,
                'Lista Resultados': listaResultados}


    def findPasos(self, cod_articulo):
        sql = " SELECT "
        sql += " pd.id_plan_capacidad_pp, "  # 0
        sql += " pd.id_item_pp, "  # 1
        sql += " pdp.id_equipo, "  # 2
        sql += " pdp.total_horas_oferta_x_maquina, "  # 3
        sql += " pdp.std_und_hora, "  # 4
        sql += " pdp.num_paso, "  # 5
        sql += " pdp.num_prioridad, "  # 6
        sql += " pdp.total_turnos, "  # 7
        sql += " pdp.horas_consumidas, "  # 8
        sql += " pdp.cant_programada, "  # 9
        sql += " pdp.id_paso "  # 10
        sql += " FROM plan_capacidad_pp_det_xpaso pdp "
        sql += " INNER JOIN plan_capacidad_pp_det pd ON pd.id_plan_capacidad_pp=pdp.id_plan_capacidad_pp AND pd.id_item_pp=pdp.id_item_pp "
        # sql += " INNER JOIN articulo_familia a ON a.cod_articulo=pd.cod_articulo "
        sql += " WHERE pd.cod_articulo='{}' ".format(cod_articulo)
        sql += " AND pd.id_plan_capacidad_pp={} ".format(self.__id_plan_capacidad_pp)
        sql += " order by pdp.num_paso asc, pdp.num_prioridad desc"

        resultado = self.__meConexionSql.consultas(sql, ("",))

        dicTemporal = {}

        for reg in resultado:
            if not reg[5] in dicTemporal:
                dicTemporal[reg[5]] = {}

            dicTemporal[reg[5]][reg[6]] = {'cant_programada' : float(reg[9]),
                                           'horas_consumidas': float(reg[8]),
                                           'std_und_hora'    : float(reg[4]),
                                           'id_paso'         : reg[10],
                                           'id_equipo'       : reg[2],
                                           'id_item_pp'      : reg[1],
                                           'num_paso'        : reg[5],
                                           'num_prioridad'   : reg[6]
                                           }

        print("="*20)
        print(dicTemporal)
        print("=" * 20)
        return {
                'id_plan_capacidad_pp': self.__id_plan_capacidad_pp,
                'dicPasos'            : dicTemporal
                }



# dic = [{'cod_articulo': 'PP0817000085', 'cantidad': 1000}]
# dic = [{'cod_articulo': 'PP0817000085', 'cantidad': 1000}, {'cod_articulo': 'PP2117000117', 'cantidad': 1000}, {'cod_articulo': 'MP2728000022', 'cantidad': 568.2000}]
# aa = edicionAutomatizadaPP(25, dic)
# resultado = aa.editar()
# print(resultado)








