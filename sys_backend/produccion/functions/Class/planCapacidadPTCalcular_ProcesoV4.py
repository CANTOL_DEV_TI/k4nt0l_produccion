import calendar
import math

class planCapacidadPTCalcular_ProcesoV4:
    def __init__(self, v_horarios, vEjercicio, vPeriodo):
        self.__diasPeriodo       = []
        self.__horarios = v_horarios
        self.__ejercicio_tarea = vEjercicio
        self.__periodo_tarea = vPeriodo

        print("="*50)
        print(self.__horarios)
        print("=" * 50)


    def hallar_DiasCalendario(self):
        calendario = calendar.Calendar()
        # self.__diasPeriodo = calendario.monthdayscalendar(2023, 8)
        self.__diasPeriodo = calendario.monthdayscalendar(self.__ejercicio_tarea, self.__periodo_tarea)

    def normalizarCalendario(self):
        newCalendario = []
        for semana in self.__diasPeriodo:
            newCalendario += semana

        return newCalendario

    def hallarDiaSemana(self, diaFecha):
        for semana in self.__diasPeriodo:
            try:
                return semana.index(diaFecha)
            except:
                continue



    def planificacion_Cant_X_Dia(self, v_fechaUltimaProgramacion, dicValores):
        listaInsertRegistros = []
        self.hallar_DiasCalendario()
        fechaUltimaProgramacion = v_fechaUltimaProgramacion
        diaSemana = 0
        horas_X_Dia = 0
        total_Fab_X_Dia = 0
        horas_Acumuladas_X_Dia = 0
        for items in dicValores:    #Recorriendo Articulo de una Familia
            dic_ProductoAnalizar = {'id_plan_capacidad'      : items["id_plan_capacidad"],
                                    'id_item'                : items["id_item"],
                                    'und_X_Hora'             : items["cant_und_hora"],
                                    'objetivoProduccion'     : items["objetivo_produccion"],
                                    'cantLoteMinimo'         : items["cant_lote_min"],
                                    'cantAcumuladaProducida' : 0
                                    }

            if dic_ProductoAnalizar['cantLoteMinimo'] <= 0:
                break

            if dic_ProductoAnalizar['und_X_Hora'] <= 0:
                break

            datosPendientes = self.planificarProducto(dic_ProductoAnalizar, fechaUltimaProgramacion, horas_Acumuladas_X_Dia)
            print("*" * 20)
            print(datosPendientes)
            print("horas acumuladas x Dia prod", horas_Acumuladas_X_Dia)
            print("tutu")
            print("*" * 20)
            fechaUltimaProgramacion = datosPendientes['fechaUltimaProgramacion']
            horas_Acumuladas_X_Dia = datosPendientes['horas_Acumuladas_X_Dia']

            if len(datosPendientes['registrosOK']) > 0:
                listaInsertRegistros += datosPendientes['registrosOK']




        if len(listaInsertRegistros) > 0:
            sql = "insert into plan_capacidad_det_xdia(id_plan_capacidad, id_item, nro_dia, cant, horas_x_dia) values "
            sql += ",".join(listaInsertRegistros) + ";"
        else:
            sql = ""
        return [fechaUltimaProgramacion, sql]




    def planificarProducto(self, dic_ProductoAnalizar, fechaUltimaProgramacion, horas_Acumuladas_X_Dia):
        listaInsert              = []
        calendarioAnalizada      = self.normalizarCalendario()
        p_id_plan_capacidad      = dic_ProductoAnalizar['id_plan_capacidad']
        p_id_item                = dic_ProductoAnalizar['id_item']
        p_und_X_Hora             = dic_ProductoAnalizar['und_X_Hora']
        p_objetivoProduccion     = dic_ProductoAnalizar['objetivoProduccion']
        p_cantLoteMinimo         = dic_ProductoAnalizar['cantLoteMinimo']
        p_faltanteFabricacion    = p_objetivoProduccion
        p_cantAcumuladaProducida = dic_ProductoAnalizar['cantAcumuladaProducida']
        p_horas_Acumuladas_X_Dia = horas_Acumuladas_X_Dia


        print("="*30)
        print(dic_ProductoAnalizar)
        print("fechaUltimaProgramacion: ", fechaUltimaProgramacion)
        print("p_horas_Acumuladas_X_Dia: ", p_horas_Acumuladas_X_Dia)


        for diaFecha in calendarioAnalizada:

            if 0 < fechaUltimaProgramacion < diaFecha:
                continue


            while p_faltanteFabricacion > 0 and p_faltanteFabricacion >= p_cantLoteMinimo:

                if p_horas_Acumuladas_X_Dia == 0:
                    fechaUltimaProgramacion += 1

                diaSemana = self.hallarDiaSemana(fechaUltimaProgramacion)

                if diaSemana is None:
                    fechaUltimaProgramacion -= 1
                    break


                horas_X_DiaRestante = round(self.__horarios[diaSemana], 0) - p_horas_Acumuladas_X_Dia
                total_Fab_X_Dia = math.ceil(horas_X_DiaRestante * p_und_X_Hora)  # redondeo hacia ARRIBA
                total_Fab_X_DiaCompleto = round(self.__horarios[diaSemana], 0) * p_und_X_Hora
                totalHoras_x_LoteMinimo = round(p_cantLoteMinimo / p_und_X_Hora, 0)

                if horas_X_DiaRestante <= 0:
                    continue


                print("horas disponibles semana: ", round(self.__horarios[diaSemana], 0))


                # if horas_X_DiaRestante > 0 and total_Fab_X_Dia < p_cantLoteMinimo:
                if horas_X_DiaRestante > 0 and horas_X_DiaRestante < totalHoras_x_LoteMinimo:
                    p_horas_Acumuladas_X_Dia = 0
                    if total_Fab_X_DiaCompleto < p_cantLoteMinimo:
                        return {'fechaUltimaProgramacion': fechaUltimaProgramacion - 1,
                                'horas_Acumuladas_X_Dia': p_horas_Acumuladas_X_Dia,
                                'registrosOK': listaInsert
                                }
                    continue


                if round(self.__horarios[diaSemana], 0) <= 0:
                    break

                cantFabricar = p_faltanteFabricacion if p_faltanteFabricacion <= total_Fab_X_Dia else total_Fab_X_Dia
                # horas_DeFabricacion = math.ceil(cantFabricar / p_und_X_Hora)  # redondeo hacia ARRIBA
                horas_DeFabricacion = round(cantFabricar/p_und_X_Hora, 0)  # redondeo


                if horas_DeFabricacion <= horas_X_DiaRestante:
                    if horas_DeFabricacion == totalHoras_x_LoteMinimo:
                        cantFabricar = p_cantLoteMinimo
                    p_faltanteFabricacion    -= cantFabricar
                    p_horas_Acumuladas_X_Dia += horas_DeFabricacion
                    p_cantAcumuladaProducida += cantFabricar


                    listaInsert.append("(%s, %s, %s, %s, %s)" % (p_id_plan_capacidad,
                                                                 p_id_item,
                                                                 fechaUltimaProgramacion,
                                                                 cantFabricar,
                                                                 horas_DeFabricacion)
                                       )



                    if p_horas_Acumuladas_X_Dia >= round(self.__horarios[diaSemana], 0):
                        p_horas_Acumuladas_X_Dia = 0


            return {'fechaUltimaProgramacion': fechaUltimaProgramacion,
                    'horas_Acumuladas_X_Dia': p_horas_Acumuladas_X_Dia,
                    'registrosOK': listaInsert
                    }




############################################################################################################
############################################################################################################
################################ PROBANDO ################################

# data = { 'FAMILIA 1':[
#     {'id_plan_capacidad': 17, 'id_item': 6, 'id_familia': 1, 'cod_articulo': 'PT0101030007', 'abc': 'A', 'cant_und_hora': 359.0000, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 22581.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},
#     {'id_plan_capacidad': 17, 'id_item': 8, 'id_familia': 1, 'cod_articulo': 'PT0101030033', 'abc': 'A', 'cant_und_hora': 359.0000, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 13952.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},
#     {'id_plan_capacidad': 17, 'id_item': 10, 'id_familia': 1, 'cod_articulo': 'PT0101030044', 'abc': 'A', 'cant_und_hora': 359.0000, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 9158.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},
#     {'id_plan_capacidad': 17, 'id_item': 9, 'id_familia': 1, 'cod_articulo': 'PT0101030039', 'abc': 'A', 'cant_und_hora': 359.0000, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 7059.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},
#     {'id_plan_capacidad': 17, 'id_item': 5, 'id_familia': 1, 'cod_articulo': 'PT0101030002', 'abc': 'B', 'cant_und_hora': 419.0000, 'cant_lote_min': 3000.0000, 'objetivo_produccion': 2391.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},
#     {'id_plan_capacidad': 17, 'id_item': 7, 'id_familia': 1, 'cod_articulo': 'PT0101030032', 'abc': 'B', 'cant_und_hora': 359.0000, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 1375.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},
#     {'id_plan_capacidad': 17, 'id_item': 13, 'id_familia': 1, 'cod_articulo': 'PT0101030058', 'abc': 'B', 'cant_und_hora': 359.0000, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 1244.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},
#     {'id_plan_capacidad': 17, 'id_item': 11, 'id_familia': 1, 'cod_articulo': 'PT0101030049', 'abc': 'C', 'cant_und_hora': 116.0000, 'cant_lote_min': 1800.0000, 'objetivo_produccion': 649.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'}
# ]
# }
#
#
#
# aa = planCapacidadPTCalcular_ProcesoV4([13.75, 13.75, 13.75, 13.75, 13.75, 13.75, 7.25], 2023, 9)
# #
# resultados = aa.planificacion_Cant_X_Dia(0, data['FAMILIA 1'])
# print(resultados)




