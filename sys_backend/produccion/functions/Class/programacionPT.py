import calendar
import math

from conexiones.conexion_mmsql import conexion_mssql


class programacionPT:
    def __init__(self):
        self.__meConexionSql = conexion_mssql()
        self.__listaResultados = []
        self.__horarios = None
        self.__horasConsumidas = {}

    def hallar_DiasCalendario(self, ejercicio, periodo):
        calendario = calendar.Calendar()
        self.__diasPeriodo = calendario.monthdayscalendar(ejercicio, periodo)

    def get_horaLaboral(self, id_plan_capacidad):
        sql = f" select "
        sql += f" num_dia, "  # 0
        sql += f" sum(cant_hora_lab) as cant_hora_lab,"  # 1
        sql += f" tpu.ejercicio_tarea,"  # 2
        sql += f" tpu.periodo_tarea "  # 3
        sql += f" from turno_det td "
        sql += f" inner join periodo_laboral pl on pl.cod_turno = td.cod_turno "
        sql += f" inner join tarea_programada_usuario tpu on tpu.ejercicio_tarea = pl.ejercicio and tpu.periodo_tarea = pl.periodo "
        sql += f" inner join plan_capacidad_pt_cab pcc on pcc.id_tarea = tpu.id_tarea "
        sql += f" where pcc.id_plan_capacidad = " + str(id_plan_capacidad) + " "
        sql += f" and pl.cod_subarea = 11 "  # ensamble de cerraduras
        sql += f" group by "
        sql += f" num_dia, "
        sql += f" tpu.ejercicio_tarea, "
        sql += f" tpu.periodo_tarea "
        sql += f" order by num_dia "
        valores = ("", )
        resultado = self.__meConexionSql.consultas(sql, valores)
        resultado_hora = []

        for item in resultado:
            resultado_hora.append(item[1])

        print(resultado_hora)
        self.__horarios = resultado_hora


    def getHorasConsumidos(self, id_plan_capacidad):
        sql = " select "
        sql += " nro_dia, " #0
        sql += " sum(horas_x_dia) as consumido "    #1
        sql += " from plan_capacidad_det_xdia "
        sql += " where id_plan_capacidad=%s " %(id_plan_capacidad)
        sql += " group by nro_dia "
        print(sql)
        valores = ("",)
        resultado = self.__meConexionSql.consultas(sql, valores)
        print(resultado)
        for row in resultado:
            self.__horasConsumidas[str(row[0])] = float(row[1])



    def consultaPT(self, id_plan_capacidad, txtFind):
        # version 1
        """
        sql = " SELECT "
        sql += " pd.id_item, "  # 0
        sql += " pd.cod_articulo, "  # 1
        sql += " pd.articulo, "  # 2
        sql += " pdd.nro_dia, "  # 3
        sql += " pdd.cant, "  # 4
        sql += " pd.cant_planificada, "  # 5
        sql += " YEAR(pc.fecha_emision) as ejercicio,"  # 6
        sql += " MONTH(pc.fecha_emision) as periodo"  # 7
        sql += " FROM plan_capacidad_pt_cab pc "
        sql += " INNER JOIN plan_capacidad_pt_det pd ON pc.id_plan_capacidad=pd.id_plan_capacidad "
        sql += " INNER JOIN plan_capacidad_det_xdia pdd ON pd.id_plan_capacidad=pdd.id_plan_capacidad AND pd.id_item=pdd.id_item "
        sql += " WHERE pd.id_plan_capacidad=%s" % (id_plan_capacidad)
        sql += " AND (pd.cod_articulo+pd.articulo) like '%" + txtFind + "%'"
        sql += " order by pd.id_item, pd.cod_articulo, pdd.nro_dia "
        """
        
        # version 2
        sql = " SELECT "
        sql += " pd.id_item, "  # 0
        sql += " pd.cod_articulo, "  # 1
        sql += " pd.articulo, "  # 2
        sql += " CASE WHEN pdd.nro_dia IS NULL THEN 0 ELSE nro_dia END AS nro_dia, "  # 3
        sql += " CASE WHEN pdd.cant IS NULL THEN 0 ELSE pdd.cant END AS cant, "  # 4
        sql += " CASE WHEN pd.cant_planificada < 0 THEN 0 ELSE pd.cant_planificada END AS cant_planificada, "  # 5
        sql += " tp.ejercicio_tarea as ejercicio,"  # 6
        sql += " tp.periodo_tarea as periodo,"  # 7
        sql += " pdd.horas_x_dia "  #8
        sql += " FROM plan_capacidad_pt_cab pc "
        sql += " INNER JOIN plan_capacidad_pt_det pd ON pc.id_plan_capacidad=pd.id_plan_capacidad "
        sql += " INNER JOIN tarea_programada_usuario tp ON tp.id_tarea=pc.id_tarea "
        sql += " LEFT OUTER JOIN plan_capacidad_det_xdia pdd ON pd.id_plan_capacidad=pdd.id_plan_capacidad AND pd.id_item=pdd.id_item "
        sql += " WHERE pd.id_plan_capacidad=%s" % (id_plan_capacidad)
        sql += " AND (pd.cod_articulo+pd.articulo) like '%" + txtFind + "%'"
        sql += " order by pdd.cant DESC, pd.id_item, pd.cod_articulo, pdd.nro_dia "

        print(sql)

        resultado = self.__meConexionSql.consultas(sql, ())
        for row in resultado:
            print(row[8])
            horaConsumido = 0.00 if row[8] is None else row[8]
            cantProgramado = 0.00 if row[4] is None else row[4]
            self.evaluarProducto(row[1], row[2], row[3], row[4], row[5], horaConsumido)
            vEjercicio = row[6]
            vPeriodo = row[7]

        print(self.__listaResultados)
        self.hallar_DiasCalendario(vEjercicio, vPeriodo)
        self.get_horaLaboral(id_plan_capacidad)
        self.getHorasConsumidos(id_plan_capacidad)
        return {"data": self.__listaResultados, "calendario": self.__diasPeriodo, "horarios": self.__horarios, "horasConsumidos": self.__horasConsumidas}

        # return {'data': listaResultados,
        #         'calendario': self.__diasPeriodo
        #         }

    def hallarTotalFabricacion(self, index):
        totalFabricacion = 0
        for llave, valor in self.__listaResultados[index]["programacion"].items():
            totalFabricacion += valor[0]

        return totalFabricacion

    def evaluarProducto(self, vCodArticulo, vNomArticulo, nroDia, cantProgramacion, objProduccion, tiempoConsumido):
        vIndex = 0
        swExiste = False
        for data in self.__listaResultados:
            if data["cod_articulo"] == vCodArticulo:
                self.__listaResultados[vIndex]["programacion"][nroDia] = [float(cantProgramacion), float(tiempoConsumido)]
                # self.__listaResultados[vIndex]["totalFabricacion"] = sum(self.__listaResultados[vIndex]["programacion"].values())
                self.__listaResultados[vIndex]["totalFabricacion"] = float(self.hallarTotalFabricacion(vIndex))
                swExiste = True

            vIndex += 1

        if not swExiste:
            dicTemporal = {
                "cod_articulo": vCodArticulo,
                "nom_articulo": vNomArticulo,
                "totalFabricacion": float(cantProgramacion),
                "objProduccion": objProduccion,
                "programacion": {nroDia: [float(cantProgramacion), float(tiempoConsumido)]},
            }

            self.__listaResultados.append(dicTemporal)

    def updateProgramacion(self, meData):
        valores = []
        # actualizar
        sql = " UPDATE plan_capacidad_det_xdia SET "
        sql += " plan_capacidad_det_xdia.cant=%s, "
        sql += " plan_capacidad_det_xdia.horas_x_dia=ROUND(" + str(meData.cant) + "/plan_capacidad_pt_det.cant_und_hora, 0) "
        sql += " FROM plan_capacidad_pt_det "
        sql += " WHERE plan_capacidad_pt_det.id_plan_capacidad=plan_capacidad_det_xdia.id_plan_capacidad "
        sql += " AND plan_capacidad_pt_det.id_item=plan_capacidad_det_xdia.id_item "
        sql += " AND plan_capacidad_det_xdia.id_plan_capacidad=%s "
        sql += " AND plan_capacidad_pt_det.cod_articulo=%s "
        sql += " AND plan_capacidad_det_xdia.nro_dia=%s; "

        # insertar
        sql += " insert into plan_capacidad_det_xdia "
        sql += " select top 1 pcd.id_plan_capacidad, "
        sql += " pcd.id_item, "
        sql += " " + str(meData.nro_dia) + " as nro_dia, "
        sql += " " + str(meData.cant) + " as cant, "
        sql += " ROUND(" + str(meData.cant) + "/pcd.cant_und_hora, 0) as horas_x_dia "
        sql += " from plan_capacidad_pt_det pcd "
        sql += " left outer join plan_capacidad_det_xdia pcdi on pcdi.id_plan_capacidad = pcd.id_plan_capacidad and pcdi.id_item = pcd.id_item "
        sql += " where not exists "
        sql += " ( "
        sql += " 	select * from plan_capacidad_det_xdia pcdi2 "
        sql += " 	where pcdi2.id_plan_capacidad = pcd.id_plan_capacidad "
        sql += " 	and pcdi2.id_item = pcd.id_item "
        sql += " 	and pcdi2.nro_dia = " + str(meData.nro_dia) + " "
        sql += " ) "
        sql += " and pcd.id_plan_capacidad = " + str(meData.id_plan_capacidad) + " "
        sql += " and pcd.cod_articulo = '" + meData.cod_articulo + "'; "

        valores.append(meData.cant)
        valores.append(meData.id_plan_capacidad)
        valores.append(meData.cod_articulo)
        valores.append(meData.nro_dia)

        respuesta = self.__meConexionSql.ejecutar_funciones(sql, tuple(valores))
        print(sql)
        return respuesta


    def deleteProgramacion(self, id_plan_capacidad):
        sql = " delete from plan_capacidad_det_xdia"
        sql += " where id_plan_capacidad=%s "
        sql += " and (select COUNT(id_plan_capacidad) from plan_capacidad_pp_cab where id_plan_capacidad=%s)=0; "

        sql += " delete from plan_capacidad_pt_det"
        sql += " where id_plan_capacidad=%s "
        sql += " and (select COUNT(id_plan_capacidad) from plan_capacidad_pp_cab where id_plan_capacidad=%s)=0; "

        sql += " delete from plan_capacidad_pt_cab"
        sql += " where id_plan_capacidad=%s "
        sql += " and (select COUNT(id_plan_capacidad) from plan_capacidad_pp_cab where id_plan_capacidad=%s)=0; "

        valores = []
        valores.append(id_plan_capacidad)
        valores.append(id_plan_capacidad)
        valores.append(id_plan_capacidad)
        valores.append(id_plan_capacidad)
        valores.append(id_plan_capacidad)
        valores.append(id_plan_capacidad)

        respuesta = self.__meConexionSql.ejecutar_funciones(sql, tuple(valores))
        print(sql)
        return respuesta
