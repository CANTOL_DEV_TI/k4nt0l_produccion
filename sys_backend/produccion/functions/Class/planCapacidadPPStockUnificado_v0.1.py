from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap import conexion_sap


class planCapacidadPPStockUnificado:
    def __init__(self):
        self.__list_almacen_art = []
        self.__list_stock = []
        self.contador = 0

    def get_almacen_stock(self):
        c_lista_almacen = self.get_lista_almacen()
        self.get_stock_almacen_sap(c_lista_almacen)
        self.get_lista_almacen_articulo()

    def get_lista_almacen(self):
        # seleccionar almacen
        c_almacen = ["ALM04", "ALM27", "ALM32", "ALM35", "ALM72"]

        return c_almacen

    def get_lista_almacen_articulo(self):
        # seleccionar almacen
        c_almacen = [
            {"cod_articulo": "PP2117000195", "cod_almacen": "ALM27"},
            {"cod_articulo": "PP2117000195", "cod_almacen": "ALM04"},
            {"cod_articulo": "PP0817000057", "cod_almacen": "ALM04"},
            {"cod_articulo": "PP0817000057", "cod_almacen": "ALM32"},
        ]

        self.__list_almacen_art = c_almacen

    def get_boom_stock(self, list_plan_pp):
        self.get_almacen_stock()
        for fila in list_plan_pp:
            # obtener los almacenes de articulo
            c_almacen = list(
                map(
                    lambda p: p["cod_almacen"],
                    list(
                        filter(
                            lambda item: item["cod_articulo"] == fila["cod_articulo"],
                            self.__list_almacen_art,
                        )
                    ),
                )
            )

            # sumar stock
            total = sum(
                d["stock"]
                for d in self.__list_stock
                if d["cod_almacen"] in c_almacen
                and d["cod_articulo"] == fila["cod_articulo"]
            )

            # añadir columna
            fila["stock"] = total

        print(list_plan_pp)

    def get_stock_almacen_sap(self, list_almacen):
        if len(list_almacen) > 0:
            c_lista_almacen = str(tuple(list_almacen))

            sql = """ select """
            sql += """ st."ItemCode" as "cod_articulo", """
            sql += """ st."WhsCode" as "cod_almacen", """
            sql += """ ( """
            sql += """ 	case when (st."OnHand"-st."IsCommited"+st."OnOrder") > 0 """
            sql += """ 	then (st."OnHand"-st."IsCommited"+st."OnOrder") """
            sql += """ 	else 0 end """
            sql += """ ) as "stock" """
            sql += """ from SBO_TECNO_PRODUCCION.OITM ar """
            sql += """ inner join SBO_TECNO_PRODUCCION.OITW st on st."ItemCode" =  ar."ItemCode" """
            sql += """ WHERE ar."ItmsGrpCod" IN ('110') """
            sql += """ and st."WhsCode" in """ + c_lista_almacen + """ """
            sql += """ and (st."OnHand"-st."IsCommited"+st."OnOrder") > 0 """
            me_conexion = conexion_sap()
            self.__list_stock = me_conexion.consultas_sgc(sql, ("",))


############################### PRUEBA ###############################
list_plan_pp = [
    {"cod_articulo": "PP2117000195", "cantidad": "100"},
    {"cod_articulo": "PP0817000028", "cantidad": "100"},
    {"cod_articulo": "PP0817000035", "cantidad": "100"},
    {"cod_articulo": "PP0817000057", "cantidad": "100"},
    {"cod_articulo": "PP0817124578", "cantidad": "100"},
]

tutu = planCapacidadPPStockUnificado()
tutu.get_boom_stock(list_plan_pp)
