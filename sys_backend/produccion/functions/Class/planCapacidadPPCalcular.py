# 1. listar pt a fabricar = planCapacidadPPCalcularPlan
# 2. realizar boom con lista pt = planCapacidadPPCalcularPlan
# 3. unificacion stock de PP = planCapacidadPPCalcularPlan
# 4. calculo de PI a fabricar = planCapacidadPPCalcularPlan
# 5. calculo PI por maquina


from datetime import date
from conexiones.conexion_mmsql import conexion_mssql
from produccion.functions.Class.planCapacidadPPCalcularPlan import planCapacidadPPCalcularPlan
from produccion.functions.Class.planCapacidadPPCalcular_ProcesoV1 import planCapacidad_PI

class planCapacidadPPCalcular:

    def __init__(self, id_planCapacidadPT):
        self.__meConexionSql = conexion_mssql()
        self.__id_plan_capacidad = id_planCapacidadPT
        self.__id_plan_capacidad_pp = None


    def calcular(self):
        #print("calcular",self.__id_plan_capacidad)
        mePlanificacion = planCapacidadPPCalcularPlan()
        resultado = mePlanificacion.call_pp_plan(self.__id_plan_capacidad)
        #print("###Cal")
        #print(resultado)
        #print("###endcal")
        self.saveTable(resultado)

        self.get_PlanificarPorMaquina()

        return True


    def saveTable(self, dicSql):
        dicCab = ({"fecha_emision"    : str(date.today()), #"{}-{}-{}".format(str(date.today()).split("-")[2], str(date.today()).split("-")[1], str(date.today()).split("-")[0],),
                   "sw_estado"        : 1,
                   "id_plan_capacidad": self.__id_plan_capacidad
                   },)

        
        resultado = self.builderInsertSql(None, *dicCab)
        sql = "insert into plan_capacidad_pp_cab {} values {}; ".format(resultado['campos'], resultado['valores'])
        sql += "select IDENT_CURRENT('plan_capacidad_pp_cab');"
        self.__id_plan_capacidad_pp = self.__meConexionSql.ejecutar_ConRetornoPK(sql, ('', ), 'plan_capacidad_pp_cab')

        sqlUpdate = "update plan_capacidad_pp_cab "
        sqlUpdate +=  " set id_preventivo = "
        sqlUpdate +=  " ( "
        sqlUpdate +=  " select max(id_preventivo) from preventivo_cab pvc "
        sqlUpdate +=  " inner join plan_capacidad_pt_cab pcc on pcc.id_plan_capacidad = plan_capacidad_pp_cab.id_plan_capacidad "
        sqlUpdate +=  " inner join tarea_programada_usuario tpu on tpu.id_tarea = pcc.id_tarea "
        sqlUpdate +=  " where year(pvc.fecha_plan) = tpu.ejercicio_tarea and month(pvc.fecha_plan) = tpu.periodo_tarea "
        sqlUpdate +=  " ) "
        sqlUpdate +=  " where id_plan_capacidad_pp = {}".format(self.__id_plan_capacidad_pp)
        respuesta = self.__meConexionSql.ejecutarAll_SinRetornoPK(sqlUpdate, ('',))

        #print(sqlUpdate)

        resultado = self.builderInsertSql(int(self.__id_plan_capacidad_pp), *dicSql)
        sql = "insert into plan_capacidad_pp_det {} values {};".format(resultado['campos'], resultado['valores'])
        respuesta = self.__meConexionSql.ejecutarAll_SinRetornoPK(sql, ('', ))



    def builderInsertSql(self, id_plan_capacidad_pp=None, *args):
        listCampos = ""
        listValores = []
        for registro in args:
            if id_plan_capacidad_pp is not None:
                registro['id_plan_capacidad_pp'] = id_plan_capacidad_pp

            listCampos = tuple(sorted(registro.keys()))
            Valores = [val for key, val in sorted(registro.items())]
            listValores.append(str(tuple(Valores)))

        return {"campos" : str(listCampos).replace("'", ""),
                "valores": ", ".join(listValores)
                }


    def get_PlanificarPorMaquina(self):
        #print("entro a plan por maquina")
        #print(self.__id_plan_capacidad_pp)
        mePlanificacion = planCapacidad_PI(self.__id_plan_capacidad_pp)
        resultado = mePlanificacion.planificar_x_Maquinaria()




# aa = planCapacidadPPCalcular('19')
# resultado = aa.calcular()
# print(resultado)

