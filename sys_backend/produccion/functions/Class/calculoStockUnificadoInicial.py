from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap import conexion_sap
from datetime import date

class calculo_stockUnificadoInicial:
    def __init__(self):
        self.__meConexionSql = conexion_mssql()

    def ejecutar(self, id_StockUnificado):
        sql = " UPDATE stock_unido_det set "
        sql += " planventas_stock=0 "
        sql += " where planventas_stock is null "
        sql += " and id_stock=%s; " %(id_StockUnificado)

        sql += " UPDATE stock_unido_det set "
        sql += " tecnopress_pendientesproduccion=0 "
        sql += " where tecnopress_pendientesproduccion is null "
        sql += " and id_stock=%s; " %(id_StockUnificado)

        sql += " UPDATE stock_unido_det "
        sql += " SET stock_inicial=tecnopress_pendientesproduccion"
        sql += "-planventas_stock "
        sql += "+(distrimax_stock-distrimax_comprometido-distrimax_pedidosborrador) "
        sql += "+(tecnopress_stock-tecnopress_comprometido) "
        sql += " WHERE id_stock=%s; " %(id_StockUnificado)

        sql += " UPDATE stock_unido_det "
        sql += " SET stock_inicial=0 "
        sql += " WHERE stock_inicial<0 AND id_stock=%s; " % (id_StockUnificado)

        print("#"*20)
        print(sql)
        print("#" * 20)

        resultado = self.__meConexionSql.ejecutar_SinRetornoPK(sql, ('',))

        return resultado

