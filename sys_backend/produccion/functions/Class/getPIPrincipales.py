from conexiones.conexion_sap import conexion_sap
class Get_PIPrincipales:
    def __init__(self, codArticulo):
        self.__conexion_sap = conexion_sap()
        self.__codArticulo = codArticulo
        self.__listaPIFinales = []

    @property
    def getListaPIFinales(self):
        return self.__listaPIFinales

    def getExecute(self, ListaArticulos):
        ListaResultado = []
        sql = """ SELECT """
        sql += """ DISTINCT "Father", "Code" """
        sql += """ FROM SBO_TECNO_PRODUCCION.ITT1 """
        sql += """ WHERE  """
        sql += """ "Type" IN (4) """
        sql += """ AND "IssueMthd" IN ('M') """
        sql += """ AND "Code" IN ('{}') """.format("','".join(ListaArticulos))
        sql += """ ORDER BY "Father" """
        print(sql)
        resultado = self.__conexion_sap.consultas(sql, ("",))
        if len(resultado) <= 0:
            # self.__listaPIFinales = ListaArticulos
            return self.__listaPIFinales

        for registro in resultado:
            if 'PT' in registro[0] and not registro[1] in self.__listaPIFinales:
                self.__listaPIFinales.append(registro[1])

            ListaResultado.append(registro[0])

        self.getExecute(ListaResultado)



# bb = Get_PIPrincipales('aa')
# bb.getExecute(['MP2829000007'])
# print(bb.getListaPIFinales)




