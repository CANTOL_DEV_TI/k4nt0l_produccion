import calendar
import math

class planCapacidadPTCalcular_ProcesoV2:
    def __init__(self, v_horarios, vEjercicio, vPeriodo):
        self.__diasPeriodo       = []
        self.__horarios = v_horarios
        self.__ejercicio_tarea = vEjercicio
        self.__periodo_tarea = vPeriodo


    def hallar_DiasCalendario(self):
        calendario = calendar.Calendar()
        # self.__diasPeriodo = calendario.monthdayscalendar(2023, 8)
        self.__diasPeriodo = calendario.monthdayscalendar(self.__ejercicio_tarea, self.__periodo_tarea)


    def planificacion_Cant_X_Dia(self, v_fechaUltimaProgramacion, dicValores):
        listaInsert = []
        self.hallar_DiasCalendario()
        fechaUltimaProgramacion = v_fechaUltimaProgramacion
        horas_Acumuladas_X_Dia = 0
        diaSemana = 0
        horas_X_Dia = 0
        total_Fab_X_Dia = 0

        for items in dicValores:    #Recorriendo Articulo de una Familia
            id_plan_capacidad      = items["id_plan_capacidad"]
            id_item                = items["id_item"]
            und_X_Hora             = items["cant_und_hora"]
            objetivoProduccion     = items["objetivo_produccion"]
            cantLoteMinimo         = items["cant_lote_min"]
            faltanteFabricacion    = objetivoProduccion
            cantAcumuladaProducida = 0

            if cantLoteMinimo <= 0:
                break

            if und_X_Hora <= 0:
                break

            for semana in self.__diasPeriodo:
                for diaFecha in semana:

                    if fechaUltimaProgramacion < diaFecha:
                        diaSemana = semana.index(fechaUltimaProgramacion)
                        horas_X_Dia = self.__horarios[diaSemana]
                        total_Fab_X_Dia = math.ceil(horas_X_Dia * und_X_Hora)  # redondeo hacia ARRIBA
                        # total_Fab_X_Dia = math.floor(horas_X_Dia * und_X_Hora)  # redondeo hacia ABAJO
                        totalHoras_x_LoteMinimo = cantLoteMinimo / und_X_Hora


                        print("=" * 20)
                        print(total_Fab_X_Dia)
                        print(id_item)
                        print(horas_X_Dia)
                        print(diaSemana)
                        print(fechaUltimaProgramacion)
                        print(semana)
                        print("=" * 20)


                        while faltanteFabricacion > 0 and faltanteFabricacion >= cantLoteMinimo:

                            if horas_Acumuladas_X_Dia == 0:
                                fechaUltimaProgramacion += 1


                            if horas_X_Dia <= 0:
                                continue

                            cantFabricar = faltanteFabricacion if faltanteFabricacion <= total_Fab_X_Dia else total_Fab_X_Dia

                            horas_DeFabricacion = math.ceil(cantFabricar / und_X_Hora)  # redondeo hacia ARRIBA

                            if not (horas_Acumuladas_X_Dia + horas_DeFabricacion) <= horas_X_Dia:
                                cantFabricar = und_X_Hora * (horas_X_Dia-horas_Acumuladas_X_Dia)
                                horas_DeFabricacion = math.ceil(cantFabricar / und_X_Hora)  # redondeo hacia ARRIBA

                            if (horas_Acumuladas_X_Dia + horas_DeFabricacion) <= horas_X_Dia:
                                faltanteFabricacion -= cantFabricar
                                horas_Acumuladas_X_Dia += horas_DeFabricacion
                                cantAcumuladaProducida += cantFabricar

                                listaInsert.append("(%s, %s, %s, %s, %s)" % (id_plan_capacidad,
                                                                             id_item,
                                                                             fechaUltimaProgramacion,
                                                                             cantFabricar,
                                                                             horas_X_Dia)
                                                   )

                                print("prod:%s => fecha: %s, cant: %s, hora Fab: %s, falta Fab: %s" % (
                                id_item, fechaUltimaProgramacion, cantFabricar, horas_DeFabricacion,
                                faltanteFabricacion))


                                if horas_Acumuladas_X_Dia >= horas_X_Dia:
                                    # fechaUltimaProgramacion += 1
                                    horas_Acumuladas_X_Dia = 0


                        # fechaUltimaProgramacion = fechaProgramacion





            # if cantAcumuladaProducida > 0:
            #     fechaUltimaProgramacion += 1



        if len(listaInsert) > 0:
            sql = "insert into plan_capacidad_det_xdia(id_plan_capacidad, id_item, nro_dia, cant, horas_x_dia) values "
            sql += ",".join(listaInsert) + ";"
        else:
            sql = ""
        return [fechaUltimaProgramacion, sql]






############################################################################################################
############################################################################################################
################################ PROBANDO ################################

# data = { 'FAMILIA 1': [{"id_plan_capacidad" : 1,
#                         "id_item"                   : 1,
#                         "fechaUltimaProgramacion"   : '',
#                         "cant_und_hora"             : 10,
#                         "objetivo_produccion"       : 200,
#                         "cant_lote_min"             : 70
#                         },
#
#                         {"id_plan_capacidad"        : 1,
#                         "id_item"                   : 2,
#                         "fechaUltimaProgramacion"   : '',
#                         "cant_und_hora"             : 10,
#                         "objetivo_produccion"       : 50,
#                         "cant_lote_min"             : 20
#                         },
#
#                         {"id_plan_capacidad"      : 1,
#                         "id_item"                : 3,
#                         "fechaUltimaProgramacion": '',
#                         "cant_und_hora"          : 10,
#                         "objetivo_produccion"    : 170,
#                         "cant_lote_min"          : 20
#                         },
#
#                        {"id_plan_capacidad"      : 1,
#                         "id_item"                : 4,
#                         "fechaUltimaProgramacion": '',
#                         "cant_und_hora"          : 10,
#                         "objetivo_produccion"    : 4,
#                         "cant_lote_min"          : 50
#                         },
#
#                         {"id_plan_capacidad"      : 1,
#                         "id_item"                : 5,
#                         "fechaUltimaProgramacion": '',
#                         "cant_und_hora"          : 10,
#                         "objetivo_produccion"    : 210,
#                         "cant_lote_min"          : 70
#                         }
#
#                        ],
# }
#
#


data = {'FAMILIA 1': [

    {'id_plan_capacidad': 7, 'id_item': 14, 'id_familia': 1, 'cod_articulo': 'PT0101030033', 'abc': 'A', 'cant_und_hora': 359.0000, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 2834.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},

    {'id_plan_capacidad': 7, 'id_item': 8, 'id_familia': 1, 'cod_articulo': 'PT0101030007', 'abc': 'A', 'cant_und_hora': 359.0000, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 2065.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},

    {'id_plan_capacidad': 7, 'id_item': 18, 'id_familia': 1, 'cod_articulo': 'PT0101030053', 'abc': 'B', 'cant_und_hora': 463.0000, 'cant_lote_min': 3000.0000, 'objetivo_produccion': 1321.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},

    {'id_plan_capacidad': 7, 'id_item': 13, 'id_familia': 1, 'cod_articulo': 'PT0101030032', 'abc': 'B', 'cant_und_hora': 359.0000, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 1318.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},

    {'id_plan_capacidad': 7, 'id_item': 7, 'id_familia': 1, 'cod_articulo': 'PT0101030002', 'abc': 'B', 'cant_und_hora': 419.0000, 'cant_lote_min': 3000.0000, 'objetivo_produccion': 42.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},

    {'id_plan_capacidad': 7, 'id_item': 24, 'id_familia': 1, 'cod_articulo': 'PT0101030071', 'abc': 'C', 'cant_und_hora': 359, 'cant_lote_min': 3000.0000, 'objetivo_produccion': 5000.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},

    {'id_plan_capacidad': 7, 'id_item': 25, 'id_familia': 1, 'cod_articulo': 'PT0101030072', 'abc': 'C', 'cant_und_hora': 359, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 5000.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},

    {'id_plan_capacidad': 7, 'id_item': 26, 'id_familia': 1, 'cod_articulo': 'PT0101030073', 'abc': 'C', 'cant_und_hora': 359, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 5000.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},

    {'id_plan_capacidad': 7, 'id_item': 27, 'id_familia': 1, 'cod_articulo': 'PT0101030074', 'abc': 'C', 'cant_und_hora': 359, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 5000.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'},

    {'id_plan_capacidad': 7, 'id_item': 34, 'id_familia': 1, 'cod_articulo': 'PT0101040042', 'abc': 'C', 'cant_und_hora': 359, 'cant_lote_min': 2604.0000, 'objetivo_produccion': 5000.0000, 'fechaUltimaProgramacion': 0, 'procesado': 'SI'}

]
}

aa = planCapacidadPTCalcular_ProcesoV2([8, 8, 8, 8, 8, 8, 0], 2023, 8)
resultados = aa.planificacion_Cant_X_Dia(0, data['FAMILIA 1'])
print(resultados)




