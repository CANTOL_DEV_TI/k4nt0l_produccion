from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap import conexion_sap


class planCapacidadPPCalcularBoom:
    def __init__(self):
        self.__list_listMat = None
        self.__list_planPt = None
        self.__list_planPP = []
        self.contador = 0

    def calcular_boom(self, id_planCapacidadPT):
        self.get_lista_materiales_sap()  # lista de materiales desde SAP
        self.get_plan_pt(
            id_planCapacidadPT
        )  # lista de plan de capacidad de producto terminado

        self.procesar(self.__list_planPt)
        return self.__list_planPP

    def calcular_boom_nivel_inferior(self, cod_articulo, cantidad):
        self.get_lista_materiales_sap()  # lista de materiales desde SAP
        self.__list_planPt = [
            {
                "id_plan_capacidad": 0,
                "cod_articulo": cod_articulo,
                "cantidad_plan": cantidad,
            }
        ]

        # PRIMER NODO
        fila = {
            "cod_articulo": cod_articulo,
            "cantidad": cantidad,
        }
        self.__list_planPP.append(fila)

        self.procesar(self.__list_planPt)
        return self.__list_planPP

    def procesar(self, lis_data):
        for item in lis_data:
            lista = self.calcular_lista(item)
            self.agrupar_lista(lista)

        # nueva lista para migrar
        c_lista = list(
            filter(
                lambda item: item["sw_procesado"] == "NO",
                self.__list_planPP,
            ),
        )

        self.contador += 1
        if len(c_lista) > 0:
            self.procesar(c_lista)

    def agrupar_lista(self, list_calMat):
        for fila in list_calMat:
            c_existe = list(
                {"item": indice, "data": dato}
                for indice, dato in enumerate(self.__list_planPP)
                if dato["cod_articulo"] == fila["cod_articulo"]
            )

            if len(c_existe) == 0:
                self.__list_planPP.append(fila)
            else:
                c_new = dict(c_existe[0]["data"])
                c_indice = int(c_existe[0]["item"])

                c_new["cantidad"] = c_new["cantidad"] + fila["cantidad"]
                c_new["cantidad_plan"] = c_new["cantidad_plan"] + fila["cantidad_plan"]
                c_new["sw_procesado"] = "NO"
                c_new["nivel"] = str(c_new["nivel"]) + "|" + str(self.contador)
                self.__list_planPP[c_indice] = c_new

    def calcular_lista(self, dict_art):
        list_materiales = list(
            map(
                lambda p: {
                    "cod_grupo": p["cod_grupo"],
                    "grupo": p["grupo"],
                    "cod_articulo": p["cod_hijo"],
                    "cantidad": p["cantidad"] * dict_art["cantidad_plan"],
                    "cantidad_plan": p["cantidad"] * dict_art["cantidad_plan"],
                    "sw_procesado": "NO",
                    "nivel": str(self.contador),
                },
                list(
                    filter(
                        lambda item: item["cod_padre"] == dict_art["cod_articulo"],
                        self.__list_listMat,
                    )
                ),
            )
        )

        # cambiar estado del padre y reiniciar [cantidad_plan]
        c_ind = [
            indice
            for indice, dato in enumerate(self.__list_planPP)
            if dato["cod_articulo"] == dict_art["cod_articulo"]
        ]
        if len(c_ind) > 0:
            self.__list_planPP[c_ind[0]]["sw_procesado"] = "SI"
            self.__list_planPP[c_ind[0]]["cantidad_plan"] = 0

        return list_materiales

    def get_lista_materiales_sap(self):
        sql = """ select """
        sql += """ FC."Code" AS "cod_padre", """
        sql += """ ART."ItmsGrpCod" AS "cod_grupo", """
        sql += """ UPPER(GR."ItmsGrpNam") AS "grupo", """
        sql += """ FD."Code" AS "cod_hijo", """
        sql += """ FD."Quantity" AS "cantidad" """
        sql += """ from SBO_TECNO_PRODUCCION.OITT FC """
        sql += (
            """ inner join SBO_TECNO_PRODUCCION.ITT1 FD ON FD."Father" = FC."Code" """
        )
        sql += """ inner join SBO_TECNO_PRODUCCION.OITM ART ON ART."ItemCode" = FD."Code" """
        sql += """ inner join SBO_TECNO_PRODUCCION.OITB GR ON GR."ItmsGrpCod" = ART."ItmsGrpCod" """
        sql += """ WHERE FD."IssueMthd" = 'M' AND FD."Quantity" > 0 """
        me_conexion = conexion_sap()
        self.__list_listMat = me_conexion.consultas_sgc(sql, ("",))

    def get_plan_pt(self, id_planCapacidadPT):
        sql = f" SELECT "
        sql += f" pd.id_plan_capacidad, "
        sql += f" pd.cod_articulo, "
        sql += f" ( "
        sql += f" 	CASE WHEN SUM(pcx.cant) IS NULL THEN 0 ELSE SUM(pcx.cant) END "
        sql += f" ) AS cantidad_plan "
        sql += f" FROM plan_capacidad_pt_det pd "
        sql += f" INNER JOIN plan_capacidad_pt_cab pc ON pc.id_plan_capacidad = pd.id_plan_capacidad "
        sql += f" INNER JOIN tarea_programada_usuario ta ON ta.id_tarea = pc.id_tarea "
        sql += f" INNER JOIN plan_capacidad_det_xdia pcx ON pcx.id_plan_capacidad = pd.id_plan_capacidad AND pcx.id_item = pd.id_item "
        sql += f" WHERE pd.id_plan_capacidad = " + str(id_planCapacidadPT) + " "
        # sql += f" AND pd.cod_articulo = 'PT0101030033' "
        sql += f" GROUP BY "
        sql += f" pd.id_plan_capacidad, "
        sql += f" pd.cod_articulo "
        me_conexion = conexion_mssql()
        self.__list_planPt = me_conexion.consultas_sgc(sql, ("",))


# tutu = planCapacidadPPCalcularBoom()
# tutu.insert(1)
