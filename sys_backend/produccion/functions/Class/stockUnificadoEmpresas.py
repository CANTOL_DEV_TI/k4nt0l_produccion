from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap import conexion_sap
from datetime import date

class stockUnificado_Empresas:
    def __init__(self):
        self.__meConexionSap = conexion_sap()
        self.__meConexionSql = conexion_mssql()
        self.__id_stock = None
        self.__dic_Data = {}

    def insert(self, fecha):
        self.get_StockTecnopress()
        self.get_StockDistrimax()
        self.get_PedidosBorradorDistrimax()

        sql = "insert into stock_unido_cab(fecha_emision) values('%s');" %(fecha)
        sql += "select IDENT_CURRENT('stock_unido_cab');"
        resultado = self.__meConexionSql.ejecutar_ConRetornoPK(sql, ('',), 'stock_unido_cab')
        if not resultado:
            return resultado

        self.__id_stock = resultado

        sql = "insert into stock_unido_det(id_stock, id_item, cod_articulo, distrimax_stock, distrimax_comprometido, distrimax_pedidosborrador, tecnopress_stock, tecnopress_comprometido, stock_inicial) values"
        item = 0
        for codigo, valores in self.__dic_Data.items():
            item += 1
            sql += " (%s, %s, '%s', %s, %s, %s, %s, %s, %s), " %(self.__id_stock, item, codigo, valores['distrimax_stock'], valores['distrimax_comprometido'], valores['distrimax_pedidosborrador'], valores['tecnopress_stock'], valores['tecnopress_comprometido'], valores['stock_inicial'])


        resultado = self.__meConexionSql.ejecutar_SinRetornoPK(sql[:-2], ('',))
        self.__meConexionSql.cerrando()
        if not resultado:
            return resultado

        return self.__id_stock


    def get_StockTecnopress(self):
        sql = """ select """
        sql += """ ap."ItemCode", """  # 0
        sql += """ p."U_MSS_CODIS", """  # 1
        sql += """ SUM(ap."OnHand") as "Stock", """  # 2
        sql += """ SUM(ap."IsCommited") as "Comprometido" """  # 3
        sql += """ from SBO_TECNO_PRODUCCION.OITW ap """
        sql += """ INNER JOIN SBO_TECNO_PRODUCCION.OITM p ON ap."ItemCode"=p."ItemCode" """
        sql += """ where ap."WhsCode" in ('ALM20','ALM71','ALM43')  """
        sql += """ and p."ItmsGrpCod" in ('102') """
        sql += """ and ap."ItemCode" not like ('%-R%') """
        sql += """ group by ap."ItemCode", p."U_MSS_CODIS" """
        sql += """ order by ap."ItemCode" """
        valores = ('',)

        resultado = self.__meConexionSap.consultas(sql, valores)

        item = 0
        for row in resultado:
            item += 1
            self.__dic_Data[row[0]] = {'distrimax_stock'          : 0,
                                       'distrimax_comprometido'   : 0,
                                       'distrimax_pedidosborrador': 0,
                                       'tecnopress_stock'         : row[2],
                                       'tecnopress_comprometido'  : row[3],
                                       'stock_inicial'            : 0
                                       }



    def get_StockDistrimax(self):
        sql = """ select """
        sql += """ ap."ItemCode", """  # 0
        sql += """ p."U_MSS_COTEC", """  # 1
        sql += """ SUM(ap."OnHand") as "Stock", """  # 2
        sql += """ SUM(ap."IsCommited") as "Comprometido" """  # 3
        sql += """ from SBO_DISTRI_PRODUCCION.OITW ap """
        sql += """ INNER JOIN SBO_DISTRI_PRODUCCION.OITM p ON ap."ItemCode"=p."ItemCode" """
        sql += """ where ap."WhsCode" in ('ALM01','ALM12','ALM19','ALM22','ALM24') """
        sql += """ and p."ItmsGrpCod" in ('103') """
        sql += """ and p."U_MSS_COTEC" in %s """ % (str(tuple(self.__dic_Data.keys())))
        sql += """ group by ap."ItemCode", p."U_MSS_COTEC" """
        sql += """ order by ap."ItemCode" """
        valores = ('',)

        resultado = self.__meConexionSap.consultas(sql, valores)

        for row in resultado:
            self.__dic_Data[row[1]]['distrimax_stock'] = row[2]
            self.__dic_Data[row[1]]['distrimax_comprometido'] = row[3]



    def get_PedidosBorradorDistrimax(self):
        dia_Now = date.today().day
        periodo_Now = date.today().month
        ejercicio_Now = date.today().year

        iniFecha = "%s-%s-%s" %(ejercicio_Now, periodo_Now, "01")
        finFecha = "%s-%s-%s" %(ejercicio_Now, periodo_Now, str(dia_Now).zfill(2))

        sql = """ select """
        sql += """ md."Articulo" as "cod_articulo", """ #0
        sql += """ p."U_MSS_COTEC", """ #1
        sql += """ SUM(CAST(md."Cantidad" AS NUMERIC(20,4))) as "cant" """  #2
        sql += """ from SBO_MSS_MOBILE.ORDR mc """
        sql += """ INNER JOIN SBO_MSS_MOBILE.RDR1 md ON mc."ClaveMovil" = md."ClaveMovil" """
        sql += """ INNER JOIN SBO_DISTRI_PRODUCCION.ODRF bc ON bc."U_MSSM_CLM" = mc."ClaveMovil" """
        sql += """ INNER JOIN SBO_DISTRI_PRODUCCION.OCRD c ON bc."CardCode" = c."CardCode"  """
        sql += """ INNER JOIN SBO_DISTRI_PRODUCCION."@MSSC_ZONA" z ON bc."U_MSSC_ZCLI" = z."Code" """
        sql += """ INNER JOIN SBO_DISTRI_PRODUCCION.OITM p ON md."Articulo"=p."ItemCode" """
        sql += """ WHERE  """
        sql += """ bc."ObjType"=17 """
        sql += """ and bc."CANCELED" = 'N' """
        sql += """ and bc."GroupNum" not in ('416') """
        sql += """ and mc."ClaveMovil" not in (select "U_MSSM_CLM" from SBO_DISTRI_PRODUCCION.ORDR where "U_MSSM_CLM" <> '') """
        sql += """ and bc."TaxDate" between '%s' and '%s' """ %(iniFecha, finFecha)
        sql += """ and p."U_MSS_COTEC" is not null """
        sql += """ group by md."Articulo", p."U_MSS_COTEC" """
        sql += """ order by md."Articulo" """
        valores = ('', )

        print(sql)

        resultado = self.__meConexionSap.consultas(sql, valores)

        for row in resultado:
            if row[1] in self.__dic_Data:
                self.__dic_Data[row[1]]['distrimax_pedidosborrador'] = row[2]

