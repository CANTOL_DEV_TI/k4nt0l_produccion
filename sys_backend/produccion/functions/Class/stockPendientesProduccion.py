from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap import conexion_sap
from datetime import date

class stock_PendientesProduccion:

    def __init__(self):
        self.__meConexionSql = conexion_mssql()

    def insert(self, id_StockUnificado):

        sql = " UPDATE stock_unido_det "
        sql += " SET stock_unido_det.tecnopress_pendientesproduccion=produccion_pendiente.cantidad "
        sql += " FROM produccion_pendiente "
        sql += " WHERE "
        sql += " stock_unido_det.cod_articulo=produccion_pendiente.cod_articulo "
        sql += " AND stock_unido_det.id_stock=%s " %(id_StockUnificado)

        print("#" * 20)
        print(sql)
        print("#" * 20)

        resultado = self.__meConexionSql.ejecutar_SinRetornoPK(sql, ('',))

        return resultado


