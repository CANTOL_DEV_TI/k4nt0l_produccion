from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap import conexion_sap

class planCapacidadPPStockUnificado:
    def __init__(self):
        self.__list_subarea_almacen = []
        self.__list_stock = []
        self.contador = 0

    def get_almacen_stock(self):
        self.get_lista_subarea_almacen()
        c_lista_almacen = self.get_lista_almacen()
        self.get_stock_almacen_sap(c_lista_almacen)
        # print(self.__list_stock)

    def get_lista_almacen(self):
        c_lista_almacen = list(xx["cod_almacen"] for xx in self.__list_subarea_almacen)
        c_almacen = list(set(c_lista_almacen))
        return c_almacen

    def get_boom_stock(self, list_plan_pp):
        self.get_almacen_stock()
        for fila in list_plan_pp:
            # obtener area de articulo unico
            c_subArea = list(
                xx
                for xx in self.__list_stock
                if xx["cod_articulo"] == fila["cod_articulo"]
            )

            # obtener los almacenes segun area
            c_almacen = []
            if len(c_subArea) > 0:
                c_almacen = list(
                    xx["cod_almacen"]
                    for xx in self.__list_subarea_almacen
                    if xx["cod_subarea_sap"] == c_subArea[0]["cod_area_resp"]
                )

            # sumar stock
            c_stock = sum(
                d["stock"]
                for d in c_subArea
                if d["cod_almacen"] in c_almacen
                and d["cod_articulo"] == fila["cod_articulo"]
            )

            # añadir columna
            fila["stock"] = c_stock

        #print(list_plan_pp)
        return list_plan_pp

    def get_lista_subarea_almacen(self):
        sql = f"select "
        sql += f"distinct "
        sql += f"sa.cod_subarea_sap, "
        sql += f"sal.cod_almacen "
        sql += f"from subarea_almacen sal "
        sql += f"inner join sub_area sa on sa.cod_subarea = sal.cod_subarea "
        me_conexion = conexion_mssql()
        c_almacen = me_conexion.consultas_sgc(sql, ("",))

        self.__list_subarea_almacen = c_almacen

    def get_stock_almacen_sap(self, list_almacen):
        if len(list_almacen) > 0:
            c_lista_almacen = str(tuple(list_almacen))

            sql = """ select """
            sql += """ st."ItemCode" as "cod_articulo", """
            sql += """ ar."U_MSSC_ARE" AS "cod_area_resp", """  # area responsable sap
            sql += """ st."WhsCode" as "cod_almacen", """
            sql += """ ( """
            sql += """ 	case when (st."OnHand"-st."IsCommited"+st."OnOrder") > 0 """
            sql += """ 	then (st."OnHand"-st."IsCommited"+st."OnOrder") """
            sql += """ 	else 0 end """
            sql += """ ) as "stock" """
            sql += """ from SBO_TECNO_PRODUCCION.OITM ar """
            sql += """ inner join SBO_TECNO_PRODUCCION.OITW st on st."ItemCode" =  ar."ItemCode" """
            sql += """ WHERE ar."ItmsGrpCod" IN ('110') """
            sql += """ and st."WhsCode" in """ + c_lista_almacen + """ """
            sql += """ and (st."OnHand"-st."IsCommited"+st."OnOrder") > 0 """
            print("CADUNI" * 3)
            print(sql)
            print("CADUNI" * 3)
            me_conexion = conexion_sap()
            self.__list_stock = me_conexion.consultas_sgc(sql, ("",))


############################### PRUEBA ###############################
"""
list_plan_pp = [
    {"cod_articulo": "PP0817000049", "cantidad": "100"},
    {"cod_articulo": "PP5017000001", "cantidad": "100"},
    {"cod_articulo": "PP2318000007", "cantidad": "100"},
    {"cod_articulo": "PP0817000057", "cantidad": "100"},
    {"cod_articulo": "PP2318000020", "cantidad": "100"},
]

tutu = planCapacidadPPStockUnificado()
tutu.get_boom_stock(list_plan_pp)
"""
