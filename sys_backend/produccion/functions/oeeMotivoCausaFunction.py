from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql


def oee_motivo_causa_all(cod_subarea, cod_causa, sw_estado):
    sql = f" SELECT pl.* FROM oee_motivo_causa pl"
    sql += f" WHERE pl.sw_estado in ('1','0')"
    if cod_subarea != "0":
        sql += f" AND pl.cod_subarea = '" + cod_subarea + "'"
    if cod_causa != "0":
        sql += f" AND pl.cod_causa = '" + cod_causa + "'"
    if sw_estado != "0":
        sql += f" AND pl.sw_estado = '" + sw_estado + "'"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta


def oee_motivo_causa_all_Get(cod_causa, sw_estado):
    sql = f" SELECT "
    sql += f" m.cod_subarea, "
    sql += f" m.cod_motivo_causa, "
    sql += f" sa.nom_subarea, "
    sql += f" m.motivo_causa, "
    sql += f" m.sw_estado, "
    sql += f" m.cod_causa "
    sql += f" FROM oee_motivo_causa m "
    sql += f" INNER JOIN sub_area sa ON sa.cod_subarea=m.cod_subarea "
    sql += f" WHERE m.sw_estado in ('1','0')"

    if cod_causa != "0":
        sql += f" AND m.cod_causa = '" + cod_causa + "'"
    if sw_estado != "0":
        sql += f" AND m.sw_estado = '" + sw_estado + "'"

    sql += f" order by sa.nom_subarea, m.motivo_causa "

    #print(sql)

    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta


def oee_motivo_causa_insert(dt_oee_motivo_causa):
    # DATOS
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(dt_oee_motivo_causa.cod_causa)
    # lista_cab.append(dt_oee_motivo_causa.cod_motivo_causa)
    lista_cab.append(dt_oee_motivo_causa.cod_subarea)
    lista_cab.append(dt_oee_motivo_causa.motivo_causa)
    lista_cab.append(dt_oee_motivo_causa.sw_estado)

    v_pk = "(select max(cod_motivo_causa)+1 from oee_motivo_causa)"


    # ejecutar cabecera
    v_sql = f"INSERT INTO oee_motivo_causa (cod_causa, cod_motivo_causa, cod_subarea, motivo_causa, sw_estado)"
    v_sql += f" VALUES (%s, " + v_pk + ", %s, %s, %s)"
    me_conexion = conexion_mssql()
    resultados = me_conexion.ejecutar_funciones(v_sql, tuple(lista_cab))
    return resultados


def oee_motivo_causa_update(dt_oee_motivo_causa):
    # CABECERA
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(dt_oee_motivo_causa.cod_subarea)
    lista_cab.append(dt_oee_motivo_causa.motivo_causa)
    lista_cab.append(dt_oee_motivo_causa.cod_motivo_causa)

    # ejecutar cabecera
    v_sql = f"UPDATE oee_motivo_causa SET cod_subarea = %s, motivo_causa = %s WHERE cod_motivo_causa = %s"
    me_conexion = conexion_mssql()
    resultados = me_conexion.ejecutar_funciones(v_sql, tuple(lista_cab))
    return resultados


def oee_motivo_causa_delete(dt_oee_motivo_causa):
    lista_cab = []
    lista_cab.append(dt_oee_motivo_causa.cod_motivo_causa)

    sql = f"DELETE FROM oee_motivo_causa WHERE cod_motivo_causa = %s"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.ejecutar_funciones(sql, tuple(lista_cab))
    return resultadosConsulta
