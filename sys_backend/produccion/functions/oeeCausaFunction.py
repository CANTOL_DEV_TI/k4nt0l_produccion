from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql


def oee_causa_all(cod_tipo_registro, cod_subarea, sw_estado):
    sql = f" SELECT pl.*,"
    sql += f" (ISNULL((select top 1 osa.cantidad from oee_causa_sub_area osa"
    sql += f" where osa.cod_causa = pl.cod_causa"
    if cod_subarea != "0":
        sql += f" and osa.cod_subarea = '" + cod_subarea + "'"
    sql += f" ),0)) as cantidad"
    sql += f" FROM oee_causa pl"
    sql += f" WHERE pl.sw_estado in ('1','0')"
    if cod_tipo_registro != "0":
        sql += f" AND pl.cod_tipo_registro = '" + cod_tipo_registro + "'"
    if sw_estado != "0":
        sql += f" AND pl.sw_estado = '" + sw_estado + "'"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta


def get_Oee_Causa(txt):
    sql = """ select
            cod_causa,
            cod_tipo_registro,
            causa,
            sw_estado,
            tiempo_default
        from oee_causa
        where causa like '{}{}{}'
        order by causa """.format("%", txt.strip() , "%")

    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta



def oee_causa_insert(dt_oee_causa):
    # DATOS
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(dt_oee_causa.cod_causa)
    lista_cab.append(dt_oee_causa.cod_tipo_registro)
    lista_cab.append(dt_oee_causa.causa)
    lista_cab.append(dt_oee_causa.sw_estado)
    lista_cab.append(dt_oee_causa.tiempo_default)


    # ejecutar cabecera
    v_sql = f"INSERT INTO oee_causa (cod_causa, cod_tipo_registro, causa, sw_estado, tiempo_default)"
    v_sql += f" VALUES (%s, %s, %s, %s, %s)"
    me_conexion = conexion_mssql()
    resultados = me_conexion.ejecutar_funciones(v_sql, tuple(lista_cab))
    return resultados


def oee_causa_update(dt_oee_causa):
    # CABECERA
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(dt_oee_causa.causa)
    lista_cab.append(dt_oee_causa.sw_estado)
    lista_cab.append(dt_oee_causa.tiempo_default)
    lista_cab.append(dt_oee_causa.cod_causa)

    # ejecutar cabecera
    v_sql = f"UPDATE oee_causa SET "
    v_sql += f"causa = %s, "
    v_sql += f"sw_estado = %s, "
    v_sql += f"tiempo_default = %s "
    v_sql += f"WHERE cod_causa = %s"
    #print(v_sql)
    #print(tuple(lista_cab))
    me_conexion = conexion_mssql()
    resultados = me_conexion.ejecutar_funciones(v_sql, tuple(lista_cab))
    return resultados


def oee_causa_delete(cod_causa):
    sql = f"DELETE FROM oee_causa WHERE cod_causa = %s"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.ejecutar_funciones(sql, (cod_causa,))
    return resultadosConsulta
