from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap_solo import conexion_SAP_Tecno


def insert(meData):
    try:
        conn = ConectaSQL_Produccion()

        cur_val_formula = conn.cursor(as_dict=True)

        cad_id = f"select isnull(max(id_fmproceso),0) + 1 as ID from config_proceso_cab "
        cur_val_formula.execute(cad_id)
        idformula = 0

        for row in cur_val_formula:
            idformula = row["ID"]

        #print(meData)

        if idformula == 0:
            conn.close()
            return "Error en el registro de la formula"

        cad_add = f"insert into config_proceso_cab(id_fmproceso,cod_articulo, articulo,cant_lote_min) values({idformula},'{meData.cod_articulo}','{meData.articulo}',{meData.cant_lote_min})"

        cur_val_formula.execute(cad_add)
        conn.commit()
        
        iddetformula = 1

        for Det in meData.detalles:
            cad_det = f"insert into config_proceso_det(id_fmproceso,id_fmp_det,num_paso,id_equipo, id_tpoperacion,num_prioridad,desc_proceso,std_und_hora)"
            cad_det = (
                cad_det
                + f" values({idformula},{iddetformula},'{Det.num_paso}',{Det.id_equipo},{Det.id_tpoperacion},'{Det.num_prioridad}','{Det.desc_proceso}',{Det.std_und_hora})"
            )
            
            iddetformula += 1
            cur_val_formula.execute(cad_det)
            conn.commit()

        conn.close()
        ##auditoria.auditausuarios(3 ,pCab.cod_usuario,pCab.empresa_aud,f"Creacion de formula {idformula} para {pCab.cod_articulo}")

        return "Registro exitoso de la formula"
    except Exception as err:
        print(err)
        return f"Error en la operación : {err}."


def update(meData):
    try:
        conn = ConectaSQL_Produccion()

        cur_val_formula = conn.cursor(as_dict=True)

        #print(meData)

        if meData.id_fmproceso == 0:
            conn.close()
            return "Error en el registro de la formula"

        cad_limpia_det = (
            f"delete from config_proceso_det where id_fmproceso={meData.id_fmproceso}"
        )
        cur_val_formula.execute(cad_limpia_det)
        conn.commit()

        cad_upd = f"update config_proceso_cab set cod_articulo ='{meData.cod_articulo}', articulo = '{meData.articulo}', cant_lote_min = '{meData.cant_lote_min}' where id_fmproceso = {meData.id_fmproceso}"

        cur_val_formula.execute(cad_upd)
        conn.commit()

        iddetformula = 1

        for Det in meData.detalles:
            cad_det = f"insert into config_proceso_det(id_fmproceso,id_fmp_det,num_paso,id_equipo, id_tpoperacion,num_prioridad,desc_proceso,std_und_hora)"
            cad_det = (
                cad_det
                + f" values({meData.id_fmproceso},{iddetformula},'{Det.num_paso}',{Det.id_equipo},{Det.id_tpoperacion},'{Det.num_prioridad}','{Det.desc_proceso}',{Det.std_und_hora})"
            )
            
            iddetformula += 1
            cur_val_formula.execute(cad_det)
            conn.commit()

        conn.close()

        return "Registro exitoso de la formula"
    
    except Exception as err:
        return f"Error en la operación : {err}."


def delete(meData):
    try:
        valores = []
        valores.append(meData.id_fmproceso)

        sql = "delete from config_proceso_det where id_fmproceso=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))

        sql = "delete from config_proceso_cab where id_fmproceso=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))

        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."
    

def NombreArticulo(pCodigo):
    try:
        lista_total = []
        lista_fila = dict()
        # conn = conexion_SAP_Tecno()
        conn = ConectaSQL_Produccion()

        cur_val_lista = conn.cursor(as_dict=True)

        # cad_lista = f"SELECT \"ItemName\" as nombre_item FROM SBO_TECNO_PRODUCCION.OITM WHERE \"ItemCode\" = '{pCodigo}'"
        cad_lista = f"select articulo as nombre_item from articulo_familia (nolock) where cod_articulo = '{pCodigo}'"

        cur_val_lista.execute(cad_lista)

        for row in cur_val_lista:
            lista_fila = {"nombre": row["nombre_item"]}
            lista_total.append(lista_fila)

        conn.close()

        return lista_total
    except Exception as err:
        return f"Error en la operación : {err}."


def config_proceso_det_articulo(cod_articulo):
    try:
        sql = f" select"
        sql += f" cpd.id_fmp_det,"
        sql += f" cpd.num_paso,"
        sql += f" cpd.id_equipo,"
        sql += f" eq.cod_equipo,"
        sql += f" eq.equipo,"
        sql += f" cpd.id_tpoperacion,"
        sql += f" tpo.tipo_operacion,"
        sql += f" cpd.std_und_hora"
        sql += f" from config_proceso_cab cpc"
        sql += f" inner join config_proceso_det cpd on cpd.id_fmproceso = cpc.id_fmproceso"
        sql += f" inner join equipo eq on eq.id_equipo = cpd.id_equipo"
        sql += f" inner join tipo_operacion tpo on tpo.id_tpoperacion = cpd.id_tpoperacion"
        sql += f" where cpc.cod_articulo = '" + cod_articulo + "'"
        sql += f" order by cpd.num_paso"

        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."
