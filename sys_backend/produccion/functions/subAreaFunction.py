from conexiones.conexion_mmsql import conexion_mssql


def get_subarea_id(cod_area):
    try:
        sql = f"select * from sub_area where cod_subarea = %s"
        meConexion = conexion_mssql()
        respuesta = meConexion.consultas_sgc(sql, (cod_area,))
        return respuesta
    except Exception as err:        
        return f"Error en la operación : {err}." 


def get_subarea_operacion(cod_area):
    try:
        sql = f"select * from sub_area where cod_area = %s"
        meConexion = conexion_mssql()
        respuesta = meConexion.consultas_sgc(sql, (cod_area,))
        return respuesta
    except Exception as err:        
            return f"Error en la operación : {err}." 


def get_subarea_operacion_usuario(cod_usuario):
    try:
        # sql = " select sba.cod_subarea, sba.nom_subarea, sba.cod_subarea_sap from subarea_usuario sbu "
        # sql += " inner join sub_area sba on sba.cod_subarea = sbu.cod_subarea "
        # sql += " where sbu.cod_usuario = '" + cod_usuario + "' "
        # sql += " and sbu.sw_estado = '1' "
        # sql += " 	union all "
        # sql += " select cod_subarea, nom_subarea, cod_subarea_sap from sub_area where not exists "
        # sql += " ( "
        # sql += " 	select * from subarea_usuario where cod_usuario = '" + cod_usuario + "' "
        # sql += " ) "
        # sql += " and nom_subarea not like '%SERVICIO%' "

        sql = "select cod_subarea, nom_subarea, cod_subarea_sap "
        sql += " from sub_area "
        sql += " where nom_subarea not like '%SERVICIO%'"
        sql += " order by nom_subarea "

        meConexion = conexion_mssql()
        respuesta = meConexion.consultas_sgc(sql, ())
        return respuesta
    except Exception as err:        
        return f"Error en la operación : {err}." 


def get_subarea_lista():
    try:
        sql = f"select cod_subarea,nom_subarea from sub_area where cod_area = 1"
        meConexion = conexion_mssql()
        respuesta = meConexion.consultas_sgc(sql)
        return respuesta
    except Exception as err:        
        return f"Error en la operación : {err}." 


def get_subarea_propietario():
    try:
        sql = f"select distinct cod_propietario, propietario from sub_area where cod_area = 1 and cod_propietario != '' "
        meConexion = conexion_mssql()
        respuesta = meConexion.consultas_sgc(sql, ())
        return respuesta
    except Exception as err:        
        return f"Error en la operación : {err}." 


def get_subarea_propietario_codigo(cod_propietario):
    try:
        sql = f"select distinct cod_subarea_sap from sub_area where cod_propietario = %s "
        meConexion = conexion_mssql()
        respuesta = meConexion.consultas_sgc(sql, (cod_propietario))
        return respuesta
    except Exception as err:        
        return f"Error en la operación : {err}." 


def get_subarea_operacion_plano(cod_area):
    try:
        sql = f"select cod_subarea as COD_AREA, nom_subarea as AREA from sub_area where cod_area = %s"
        meConexion = conexion_mssql()
        respuesta = meConexion.consultas_sgc(sql, (cod_area,))
        return respuesta
    except Exception as err:        
        return f"Error en la operación : {err}." 


def insert(meData):
    try:
        valores = []
        valores.append(meData.cod_area)
        valores.append(meData.nom_subarea)
        sql = "insert into sub_area(cod_area, nom_subarea) values(%s, %s)"

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        #print(sql)
        return respuesta
    except Exception as err:        
        return f"Error en la operación : {err}." 


def update(meData):
    try:
        valores = []
        valores.append(meData.cod_area)
        valores.append(meData.nom_subarea)
        valores.append(meData.cod_subarea)
        sql = "update sub_area set "
        sql += " cod_area=%s, "
        sql += "nom_subarea=%s "
        sql += "where cod_subarea=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        #print(sql)
        return respuesta
    except Exception as err:        
        return f"Error en la operación : {err}." 


def delete(meData):
    try:
        valores = []
        valores.append(meData.cod_subarea)
        sql = "delete from sub_area "
        sql += "where cod_subarea=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        #print(sql)
        return respuesta
    except Exception as err:        
        return f"Error en la operación : {err}." 