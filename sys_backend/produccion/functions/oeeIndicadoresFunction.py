from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql

from datetime import datetime


def oee_Indicadores_All():
    sql = f" SELECT id_config_oee, descripcion, umu FROM config_indicadores_oee "
    sql += f" WHERE sw_estado in ('1')"
    sql += f" order by id_config_oee"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta


def oee_Indicadores_get(fecha, tipo_filtro, txtFind):
    fecha_dt = datetime.strptime(fecha, '%Y-%m-%d')

    # sql = f" select "
    # sql += f" oec.id_oee, "
    # sql += f" CAST(oec.fecha_doc as DATE) as fecha_doc, "
    # sql += f" oec.articulo, "
    # sql += f" oec.cod_articulo, "
    # sql += f" oec.cod_empleado, "
    # sql += f" oec.cod_subarea, "
    # sql += f" oec.cod_turno, "
    # sql += f" oec.id_tpoperacion, "
    # sql += f" oec.id_equipo, "
    # sql += f" oec.num_of_sap, "
    # sql += f" oec.num_paso, "
    # sql += f" oec.std_und_hora, "
    # sql += f" oec.hora_fin, "
    # sql += f" oec.hora_ini, "
    # sql += f" oec.hora_planificado, "
    # sql += f" oec.sw_estado, "
    # sql += f" e.nombre as empleado, "
    # sql += f" t.nom_turno, "
    # sql += f" oec.horas_operativo, "
    # sql += f" oec.produccion_total, "
    # sql += f" oec.horas_perdidas, "
    # sql += f" '' as detalle, "
    # sql += f" eq.equipo "
    # sql += f" from oee_cab oec "
    # sql += f" inner join empleado e ON e.cod_empleado=oec.cod_empleado "
    # sql += f" inner join turno_cab t ON t.cod_turno=oec.cod_turno "
    # sql += f" inner join equipo eq ON eq.id_equipo=oec.id_equipo "
    # sql += f" where "
    # # sql += f" year(oec.fecha_doc) = %s " %(fecha_dt.year)
    # # sql += f" and month(oec.fecha_doc) = %s " %(fecha_dt.month)
    # # sql += f" and day(oec.fecha_doc) = %s " %(fecha_dt.day)
    # sql += f" oec.fecha_doc>=DATEADD(day,-5, CONVERT (date, '%s'))" % (fecha_dt)
    #
    # # if cod_empleado > 0:
    # #     sql += f" and e.cod_empleado = '%s' " %(cod_empleado)
    #
    # sql += f" and eq.equipo like '%s' " %("%" + cod_empleado + "%")

    sql = """ select
                 oec.id_oee,
                 CAST(oec.fecha_doc as DATE) as fecha_doc,
                 oec.cod_subarea,
                 oec.id_equipo,
                 oec.cod_turno,

                 convert(char(5), hora_ini, 108) as hora_ini,
                 convert(char(5), hora_fin, 108) as hora_fin,

                 oec.hora_planificado,
                 oec.horas_operativo,
                 oec.produccion_total,
                 oec.horas_perdidas,

                 oec.hora_pausas,
                 oec.hora_paradas,

                 '' as detalle,
                 oec.sw_estado,
                 t.nom_turno,
                 eq.equipo
             from oee_cab oec
                 --inner join empleado e ON e.cod_empleado=oec.cod_empleado
                 inner join turno_cab t ON t.cod_turno=oec.cod_turno
                 inner join equipo eq ON eq.id_equipo=oec.id_equipo
             where
                 oec.fecha_doc>=DATEADD(day,-5, CONVERT (date, '{}')) """.format(fecha_dt)

    if tipo_filtro == 'MAQUINA':
        sql += """ and eq.equipo like '%{}%' """.format(txtFind)

    if tipo_filtro == 'EMPLEADO':
        sql += """ and (select COUNT(*) from oee_det where id_oee=oec.id_oee and cod_empleado='{}')>0 """.format(txtFind)


    sql += """ order by oec.fecha_doc desc , oec.id_oee desc, eq.equipo """

    # sql += f" order by oec.fecha_doc desc, oec.num_paso, e.nombre"

    #print("!" * 30)
    #print(sql)
    #print("!" * 30)

    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta


def oee_Indicadores_Det_Ind_get(id_oee):
    #print("&&&&&&&&")
    sql = f" select "
    sql += f" oed.id_oee, "
    sql += f" oed.item_oee, "
    sql += f" oed.cod_tipo_registro, "
    sql += f" oed.tipo_registro, "
    sql += f" oed.umu, "
    sql += f" oed.cod_causa, "
    sql += f" oed.causa, "
    sql += f" oed.cod_motivo_causa, "
    sql += f" oed.motivo_causa, "
    sql += f" oed.cantidad_prod_buena, "
    sql += f" oed.detalle, "
    sql += f" oed.num_vale, "
    sql += f" oed.cantidad_prod_mala, "
    sql += f" oed.cantidad_total, "
    sql += f" oed.tiempo_min, "

    # sql += f" oed.hora_ini, "
    # sql += f" oed.hora_fin "

    sql += f" convert(char(5), hora_ini, 108) as hora_ini, "
    sql += f" convert(char(5), hora_fin, 108) as hora_fin, "

    sql += f" oed.cod_empleado, "
    sql += f" e.nombre as empleado, "
    sql += f" oed.num_of_sap, "
    sql += f" oed.cod_articulo, "
    sql += f" oed.articulo, "
    sql += f" oed.num_paso, "

    sql += f" oed.std_und_hora, "
    sql += f" oed.id_tpoperacion, "

    sql += f" oed.rendimiento_porcentaje "

    sql += f" from oee_det oed "
    sql += f" left join empleado e on e.cod_empleado=oed.cod_empleado"
    sql += f" where oed.id_oee=%s " % (id_oee)
    sql += f" order by oed.item_oee "

    # ==============================================================
    sql_Indicador = "select "
    sql_Indicador += " id_config_oee, "
    sql_Indicador += " id_oee, "
    sql_Indicador += " item_indicador_oeee, "
    sql_Indicador += " causa, "
    sql_Indicador += " cantidad, "
    sql_Indicador += " motivo_causa, "
    sql_Indicador += " 'OEE' as tipo_registro, "
    sql_Indicador += " umu, "
    sql_Indicador += " '5' as cod_tipo_registro, "
    sql_Indicador += " 0 as cod_motivo_indicador "
    sql_Indicador += " from oee_indicadores  "
    sql_Indicador += " where id_oee=%s " % (id_oee)
    sql_Indicador += " order by id_config_oee "

    me_conexion = conexion_mssql()
    resultadosConsultaDet = me_conexion.consultas_sgc(sql, ())
    me_conexion = conexion_mssql()
    resultadosConsultaIndicador = me_conexion.consultas_sgc(sql_Indicador, ())

    resultado = {'detalle'        : resultadosConsultaDet,
                 'listIndicadores': resultadosConsultaIndicador
                 }

    return resultado

def export_Indicadores(cod_subarea, fecha_ini, fecha_fin):
    sql = """ (select
                FORMAT(ec.fecha_doc, 'yyyy-MM-dd') as fecha_doc,
                CAST(ec.id_oee as varchar(500)) as id_oee,
                ed.item_oee,
                a.nom_subarea,
                t.nom_turno,
                e.equipo,                
                (select cod_empleado from empleado where cod_empleado=ed.cod_empleado) as dni,
                (select nombre from empleado where cod_empleado=ed.cod_empleado) as nombre,                
                ed.num_of_sap as nro_orden_fab,
                (select tipo_operacion from tipo_operacion where id_tpoperacion=ed.id_tpoperacion) as tipo_operacion,
                ed.num_paso,
                ed.std_und_hora,
                ed.articulo,
                ed.tipo_registro,
                ed.causa,
                ed.motivo_causa,
                ed.cantidad_prod_mala,
                ed.cantidad_prod_buena,
                ed.tiempo_min,
                ed.rendimiento_porcentaje,
                ed.detalle
            from oee_cab ec
            inner join equipo e on e.id_equipo=ec.id_equipo
            inner join turno_cab t on t.cod_turno=ec.cod_turno
            inner join sub_area a on a.cod_subarea=ec.cod_subarea
            inner join oee_det ed on ec.id_oee=ed.id_oee
            --inner join empleado emp on emp.cod_empleado=ed.cod_empleado
            where FORMAT(ec.fecha_doc, 'yyyy-MM-dd') between '{}' and '{}'
            and ec.cod_subarea='{}' )            
            UNION
            (select
                FORMAT(mc.fecha_doc, 'yyyy-MM-dd') as fecha_doc,                
                CAST(mc.id_oee as varchar(500)) as id_oee,
                md.item as item_oee,
                sa.nom_subarea,
                t.nom_turno,
                eq.equipo,
                mc.cod_empleado as dni,
                e.nombre as nombre,
                '' as nro_orden_fab,
                '' as tipo_operacion,
                '' as num_paso,
                0 as std_und_hora,
                '' as articulo,
                'PARADA' as tipo_registro,
                (select causa from oee_causa where cod_causa=md.cod_causa) as causa,
                (select motivo_causa from oee_motivo_causa where cod_causa=md.cod_causa and cod_motivo_causa=md.cod_motivo_causa) as motivo_causa,
                0 as cantidad_prod_mala,
                0 as cantidad_prod_buena,
                /*(select cant_hora_lab*60 from turno_det where num_dia=DATEPART(weekday, mc.fecha_doc) and cod_turno=t.cod_turno)*/ tiempo_min as tiempo_min,
                0 as rendimiento_porcentaje,
                '' as detalle            
            from maquinas_detenidas_cab mc
            inner join empleado e on e.cod_empleado=mc.cod_empleado
            inner join sub_area sa on sa.cod_subarea=mc.cod_subarea
            inner join turno_cab t on t.cod_turno=mc.cod_turno
            inner join maquinas_detenidas_det md on md.id_oee=mc.id_oee
            inner join equipo eq on eq.id_equipo=md.id_equipo
            where
            FORMAT(mc.fecha_doc, 'yyyy-MM-dd') between '{}' and '{}'
            and mc.cod_subarea='{}')            
            order by 1, 2, 3 """.format(fecha_ini, fecha_fin, cod_subarea, fecha_ini, fecha_fin, cod_subarea)

    #print(sql)
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    
    return resultadosConsulta

