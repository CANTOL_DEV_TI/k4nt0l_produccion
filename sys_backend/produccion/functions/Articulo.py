# from conexiones.conexion_sap import conexion_sap
from conexiones.conexion_sap import conexion_sap

v_db = "SBO_TECNO_PRODUCCION"


class Articulo:
    def __init__(self):
        self.__meConexion = conexion_sap()

    def get_articulo_sap(self):
        sql = """ select """
        sql += """ AR."ItemCode" AS "cod_articulo", """
        sql += """ AR."ItemName" AS "articulo", """
        sql += """ AR."ItmsGrpCod" AS "cod_grupo", """
        sql += """ IFNULL(AR."U_MSSC_CMI",0) AS "cob_min", """
        sql += """ IFNULL(AR."U_MSSC_CID",0) AS "cob_ide", """
        sql += """ IFNULL(AR."U_MSSC_CMA",0) AS "cob_max", """
        sql += """ IFNULL(AR."U_MSSC_ABC",'B') AS "abc", """
        sql += """ IFNULL(AR."U_MSSC_UXH",0) AS "und_x_hora", """
        sql += """ IFNULL(AR."U_MSSC_TIFO",'0') AS "tipo_formulado" """
        sql += """ from """ + v_db + """.OITM AS AR """
        sql += """ where AR."ItemType" = 'I' """
        sql += """ AND AR."ItemCode" NOT LIKE '%-R' """
        sql += """ AND AR."ItemCode" NOT LIKE '%-E' """
        sql += (
            """ AND AR."validFor" = 'Y' """
        )
        sql += """ AND AR."InvntItem" = 'Y' """
        #print(sql)
        me_conexion = conexion_sap()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ("",))
        return resultadosConsulta

    def get_articulo_sap_filtro(self,pFiltro):
        sql = """ select top 10 AR."ItemCode" AS "cod_articulo",  AR."ItemName" AS "articulo",  AR."ItmsGrpCod" AS "cod_grupo",  """
        sql += """IFNULL(AR."U_MSSC_CMI",0) AS "cob_min",  IFNULL(AR."U_MSSC_CID",0) AS "cob_ide",  IFNULL(AR."U_MSSC_CMA",0) AS "cob_max", """
        sql += """IFNULL(AR."U_MSSC_ABC",'B') AS "abc",  IFNULL(AR."U_MSSC_UXH",0) AS "und_x_hora",  IFNULL(AR."U_MSSC_TIFO",'0') AS "tipo_formulado" """
        sql += f"""from {v_db}.OITM AS AR  where AR."ItemType" = 'I'  AND AR."ItemCode" NOT LIKE '%-R'  AND AR."ItemCode" NOT LIKE '%-E'  """
        sql += f"""AND AR."validFor" = 'Y'  AND AR."InvntItem" = 'Y' AND AR."ItemName" LIKE '%{pFiltro}%' """

        me_conexion = conexion_sap()
        resultadosConsulta = me_conexion.consultas_sgc(sql,("",))
        
        return resultadosConsulta