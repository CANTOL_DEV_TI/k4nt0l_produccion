from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap_solo import conexion_SAP_Tecno


def insert(meData):
    try:
        valores = []
        valores.append(meData.cod_almacen)
        valores.append(meData.nom_almacen)
        sql = "insert into almacenes(cod_almacen,nom_almacen) values(%s,%s)"

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        ##print(sql)
        return respuesta 
    except Exception as err:
        return f"Error en la operación : {err}."


def update(meData):
    try:
        valores = []
        valores.append(meData.nom_almacen)
        valores.append(meData.cod_almacen)
        sql = "update almacenes set "
        sql += "nom_almacen=%s "
        sql += "where cod_almacen=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def delete(meData):
    try:
        valores = []
        valores.append(meData.cod_almacen)
        sql = "delete from almacenes "
        sql += "where cod_almacen=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."

def NombreAlmacen(pCodigo):
    try:
        lista_total = []
        lista_fila = dict()  
        conn = conexion_SAP_Tecno()
        
        cur_val_lista = conn.cursor()

        cad_lista = f"SELECT \"WhsName\" as nombre_almacen FROM SBO_TECNO_PRODUCCION.OWHS WHERE \"WhsCode\" = '{pCodigo}'"
            
        cur_val_lista.execute(cad_lista)

        for row in cur_val_lista:
            lista_fila = {"nombre" : row['nombre_almacen']}
            lista_total.append(lista_fila)
                
        conn.close()

        return lista_total
    except Exception as err:
        print(err)
        return f"Error en la operación : {err}."