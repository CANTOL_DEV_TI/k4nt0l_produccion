from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap import conexion_sap
from datetime import datetime
from decimal import Decimal
from produccion.functions.Class.planCapacidadPPCalcularBoom import (
    planCapacidadPPCalcularBoom,
)
from produccion.functions.Class.edicionAutomatizadaPP import edicionAutomatizadaPP
from produccion.functions.Class.getPIPrincipales import Get_PIPrincipales
from produccion.functions import equipoFunction


def planCapacidadPeriodo(id_plan_capacidad):
    sql = f"SELECT ejercicio_tarea, periodo_tarea FROM plan_capacidad_pt_cab pc "
    sql += f" INNER JOIN plan_capacidad_pp_cab pcp on pcp.id_plan_capacidad = pc.id_plan_capacidad"
    sql += f" INNER JOIN tarea_programada_usuario tp on tp.id_tarea = pc.id_tarea"
    sql += f" WHERE pcp.id_plan_capacidad_pp = %s"
    me_conexion = conexion_mssql()
    resultados = me_conexion.consultas_sgc(sql, (id_plan_capacidad,))
    return resultados


def produccion_real(id_plan_capacidad):
    # obtener ejercicio y periodo
    c_ejercicio = 0
    c_periodo = 0
    c_json_res = planCapacidadPeriodo(id_plan_capacidad)
    for row in c_json_res:
        c_ejercicio = str(row["ejercicio_tarea"])
        c_periodo = str(row["periodo_tarea"])

    if c_ejercicio == 0 or c_periodo == 0:
        return False

    # valida para ejecutar solo cuando sea mes actual
    c_a_actual = str(datetime.now().year)
    c_m_actua = str(datetime.now().month)
    if (c_ejercicio + c_periodo) != (c_a_actual + c_m_actua):
        return False

    # obtener producción real sap
    v_db = "SBO_TECNO_PRODUCCION"
    sql = (
        f"SELECT * FROM "
        + v_db
        + ".SGC_PRODUCCION_MENSUAL_OK_PP(%s,%s)" % (c_ejercicio, c_periodo)
    )
    me_conexion = conexion_sap()
    res_datos_sap = me_conexion.consultas_sgc(sql, ("",))

    # borrar columna producción real
    sql = f"UPDATE plan_capacidad_pp_cab_det SET cant_real = 0 WHERE id_plan_capacidad_pp = %s"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.ejecutar_funciones(sql, (id_plan_capacidad,))

    # actualizar producción real
    lista = []
    for row in res_datos_sap:
        lista.append(
            (
                row["PRODUCIDO"] + row["REPROCESO"],
                id_plan_capacidad,
                row["COD_ARTICULO"],
            )
        )

    sql = f"UPDATE plan_capacidad_pp_det SET cant_real = %s WHERE id_plan_capacidad_pp = %s and cod_articulo = %s;"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.ejecutar_funciones_multi(sql, lista)
    #print(resultadosConsulta)
    return resultadosConsulta


def planCapacidadPPDet_all(id_plan_capacidad):
    sql = f"SELECT id_item_pp, cod_articulo, articulo FROM plan_capacidad_pp_det det "
    sql += f" WHERE det.id_plan_capacidad_pp = %s"
    me_conexion = conexion_mssql()
    resultados = me_conexion.consultas_sgc(sql, (id_plan_capacidad,))
    return resultados


def planCapacidadPPDet_consolidado(id_plan_capacidad, cod_subarea, abc):
    # actualizar datos de producción real
    resultado = produccion_real(id_plan_capacidad)

    # consulta
    sql = f"SELECT * FROM PRODUCCION_PLAN_CAPACIDAD_PP_SEARCH (%s,%s,%s)"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(
        sql, (id_plan_capacidad, cod_subarea, abc)
    )
    # ordenar segun planificado de mayor a menor
    resultado_orden = sorted(
        resultadosConsulta, key=lambda x: x["cant_planificada"], reverse=True
    )

    return resultado_orden


def planCapacidadPPDet_equipo(id_plan_capacidad, cod_subarea):
    # actualizar datos de producción real
    resultado = produccion_real(id_plan_capacidad)

    # consulta
    sql = f"SELECT * FROM PRODUCCION_PLAN_CAPACIDAD_PP_EQUIPO (%s,%s)"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(
        sql, (id_plan_capacidad, cod_subarea)
    )
    # ordenar segun planificado de mayor a menor
    resultado_orden = sorted(
        resultadosConsulta, key=lambda x: x["porc_capacidad"], reverse=True
    )

    return resultado_orden


def planCapacidadPPDet_equipo_articulo(id_plan_capacidad, id_equipo):
    # consulta
    sql = f"SELECT * FROM PRODUCCION_PLAN_CAPACIDAD_PP_EQUIPO_ARTICULO (%s,%s)"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, (id_plan_capacidad, id_equipo))

    # ordenar segun planificado de mayor a menor
    resultado_orden = sorted(
        resultadosConsulta, key=lambda x: x["cant_programado"], reverse=True
    )

    return resultado_orden


def planCapacidadPPDet_articulo(id_plan_capacidad, cod_articulo):
    # consulta
    sql = f"SELECT * FROM PRODUCCION_PLAN_CAPACIDAD_PP_ARTICULO (%s,%s)"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(
        sql, (id_plan_capacidad, cod_articulo)
    )

    # ordenar segun planificado de mayor a menor
    resultado_orden = sorted(resultadosConsulta, key=lambda x: x["num_paso"])

    return resultado_orden


def planCapacidadPPDet_nivel_update(dt_articulo):
    # obtener lista de niveles a afectar
    obj = planCapacidadPPCalcularBoom()
    data = obj.calcular_boom_nivel_inferior(
        dt_articulo.cod_articulo, Decimal(dt_articulo.cantidad)
    )
    lista_datos = list(
        map(
            lambda p: {
                "cod_articulo": p["cod_articulo"],
                "cantidad": (
                    float(p["cantidad"] * (-1))
                    if dt_articulo.sw_modo == "S"
                    else float(p["cantidad"])
                ),
            },
            list(data),
        )
    )

    # grabar los niveles
    procesar = edicionAutomatizadaPP(dt_articulo.id_plan_capacidad_pp, lista_datos)
    dt_lista = procesar.editar()

    
    dt_lista = [
        {
            "id_item_pp": 40,
            "id_paso": 130,
            "id_equipo": 52,
            "num_paso": 1,
            "num_prioridad": 1,
        }
    ]

    # maestros
    list_articulo = planCapacidadPPDet_all(dt_articulo.id_plan_capacidad_pp)
    list_equipo = equipoFunction.equipo_buscar()

    for fila_art in dt_lista:
        # cabeceras para añadir
        c_cod_equipo = "cod_equipo"
        c_equipo = "equipo"
        c_cod_articulo = "cod_articulo"
        c_articulo = "articulo"

        filt_equipo = [
            dictionary
            for dictionary in list_equipo
            if dictionary["id_equipo"] == fila_art["id_equipo"]
        ]
        filt_art = [
            dictionary
            for dictionary in list_articulo
            if dictionary["id_item_pp"] == fila_art["id_item_pp"]
        ]

        if c_cod_equipo not in fila_art:
            fila_art.update(
                {
                    c_cod_equipo: filt_equipo[0]["cod_equipo"],
                    c_equipo: filt_equipo[0]["equipo"],
                    c_cod_articulo: filt_art[0]["cod_articulo"],
                    c_articulo: filt_art[0]["articulo"],
                }
            )

    return dt_lista


def planCapacidadPPDet_nivel_articulo(id_plan_capacidad, cod_articulo):
    # obtener lista de niveles a afectar
    obj = Get_PIPrincipales(cod_articulo)
    obj.getExecute(list([cod_articulo]))
    resultado = obj.getListaPIFinales
    resultadosConsulta = planCapacidadPPDet_get_nivel_articulo(
        id_plan_capacidad, resultado
    )

    return resultadosConsulta


def planCapacidadPPDet_get_nivel_articulo(id_plan_capacidad, lista_articulo):
    sql = f"select"
    sql += f" pcd.id_plan_capacidad_pp,"
    sql += f" pcd.cod_articulo,"
    sql += f" pcd.articulo,"
    sql += f" sum(pcdx.cant_programada) as cant_programado,"
    sql += f" sum(pcdx.cant_programada) as cant_programado_mod,"
    sql += f" sum(pcd.cant_planificada) as cant_objetivo,"
    sql += f" sum(pcd.cant_boom) as cant_boom"
    sql += f" from plan_capacidad_pp_det_xpaso pcdx"
    sql += f" inner join plan_capacidad_pp_det pcd on pcd.id_plan_capacidad_pp = pcdx.id_plan_capacidad_pp and pcd.id_item_pp = pcdx.id_item_pp"
    sql += f" where pcd.id_plan_capacidad_pp = " + id_plan_capacidad
    sql += """ and pcd.cod_articulo in ('{}') """.format("','".join(lista_articulo))
    sql += f" and pcdx.cant_programada > 0 "
    sql += f" and pcdx.num_paso = 1 "
    sql += f" group by"
    sql += f" pcd.id_plan_capacidad_pp,"
    sql += f" pcd.cod_articulo,"
    sql += f" pcd.articulo"

    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta


def planCapacidadPPDet_update(dt_plan_pp):
    # DETALLE
    lista_det = []
    v_sql = f" update plan_capacidad_pp_det_xpaso set"
    v_sql += f" cant_programada = %s,"
    v_sql += f" horas_consumidas = %s"
    v_sql += f" where id_plan_capacidad_pp = %s"
    v_sql += f" and id_item_pp = %s"
    v_sql += f" and num_paso = %s"
    v_sql += f" and id_equipo = %s"

    for row in dt_plan_pp.detalle:
        rowD = row.dict()
        lista_det.append(
            (
                rowD["cant_programado"],
                rowD["hora_programado"],
                rowD["id_plan_capacidad_pp"],
                rowD["id_item_pp"],
                rowD["num_paso"],
                rowD["id_equipo"],
            )
        )

    # insert
    me_conexion = conexion_mssql()
    if len(lista_det) > 0:
        resultados = me_conexion.ejecutar_funciones_multi(v_sql, lista_det)
    else:
        resultados = HTTPException(status_code=401, detail="Problemas al insertar")

    return resultados
