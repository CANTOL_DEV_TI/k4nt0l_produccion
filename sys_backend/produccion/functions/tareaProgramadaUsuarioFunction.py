from conexiones.conexion_mmsql import conexion_mssql 
 
 
def insert(meData): 
    try:
        valores = [] 
        valores.append(meData.fecha_tarea) 
        valores.append(meData.fecha_busqueda) 
        valores.append(meData.cod_usuario) 
        valores.append(meData.ejercicio_tarea)
        valores.append(meData.periodo_tarea)
        valores.append(meData.tipo_tarea)

        #print(meData) 
        #print(valores) 
        sql = "insert into tarea_programada_usuario(fecha_tarea,fecha_busqueda,cod_usuario,ejercicio_tarea,periodo_tarea,tipo_tarea) values(convert(datetime,%s),%s,%s,%s,%s,%s)" 
    
        meConexion = conexion_mssql() 
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
        
        return respuesta 
    except Exception as err:
        return f"Error en la operación : {err}."
 
 
def update(meData): 
    try:
        valores = [] 
        valores.append(meData.fecha_tarea) 
        valores.append(meData.id_tarea)
        valores.append(meData.ejercicio_tarea)
        valores.append(meData.periodo_tarea)
        valores.append(meData.tipo_tarea)
        
        sql = "update tarea_programada_usuario set " 
        sql += "fecha_tarea=%s " 
        sql += "where id_tarea=%s " 
    
        meConexion = conexion_mssql() 
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
        
        return respuesta 
    except Exception as err:
        return f"Error en la operación : {err}."
 
 
def delete(meData): 
    try:
        respuesta = ""
        existe = 0
        existe = validaPlanTarea(meData.id_tarea)
    
        if(existe[0]["Existe"]==0):
            valores = [] 
            valores.append(meData.id_tarea) 
            sql = "delete from tarea_programada_usuario where id_tarea=%s " 

            meConexion = conexion_mssql() 
            respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
            #print(sql) 
        else:
            respuesta = {"Mensaje": "", "Error": "Existen planificaciones asociadas a esta tarea, por lo tanto no se puede eliminar"} 
            
        return respuesta 
    except Exception as err:
        return f"Error en la operación : {err}."

def validaPlanTarea(Tarea):
    try:
        valores = []
        valores.append(Tarea)
        sql = "select count(*) as Existe from plan_capacidad_pt_cab where id_tarea=%s"

        meConexion = conexion_mssql()
        respuesta = meConexion.consultas_sgc(sql,tuple(valores))

        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."