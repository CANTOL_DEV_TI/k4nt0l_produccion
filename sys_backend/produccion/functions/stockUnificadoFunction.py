from produccion.functions.Class.stockUnificadoEmpresas import stockUnificado_Empresas
from produccion.functions.Class.stockPlanVentas import stock_PlanVentas
from produccion.functions.Class.stockPendientesProduccion import stock_PendientesProduccion
from produccion.functions.Class.calculoStockUnificadoInicial import calculo_stockUnificadoInicial
from produccion.functions import planCapacidadPTDatosFunction

def insert(meData):
    try:
        exe_StockUnificado = stockUnificado_Empresas()
        rpt_StockUnificado = exe_StockUnificado.insert(meData)

        if not rpt_StockUnificado:
            return {"Mensage": False}

        exe_StockPlanVentas = stock_PlanVentas()
        rpta_StockPlanVentas = exe_StockPlanVentas.insert(rpt_StockUnificado, meData)

        if not rpta_StockPlanVentas:
            return {"Mensage": rpta_StockPlanVentas}

        exe_StockPendientesProduccion = stock_PendientesProduccion()
        rpta_StockPendientesProduccion = exe_StockPendientesProduccion.insert(rpt_StockUnificado)

        if not rpta_StockPendientesProduccion:
            return {"Mensage": rpta_StockPendientesProduccion}

        exe_CalculoStockUnificadoProduccion = calculo_stockUnificadoInicial()
        rpt_CalculoStockUnificadoProduccion = exe_CalculoStockUnificadoProduccion.ejecutar(rpt_StockUnificado)

        if not rpt_CalculoStockUnificadoProduccion:
            return {"Mensage": rpt_CalculoStockUnificadoProduccion}

        # crea plan de capacidad de producción
        exe_calcular_plan = planCapacidadPTDatosFunction.insert()

        return {"Mensage": exe_calcular_plan}
    except Exception as err:
        return f"Error en la operación : {err}."
    







