from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap import conexion_sap

from produccion.functions import subAreaFunction

# ============================================================================

def Find_SubArea(cod_usuario):
    sql = """ select
                    emp.cod_subarea,
                    s.nom_subarea
                from sub_area s
                inner join empleado emp on emp.cod_subarea=s.cod_subarea
                where
                emp.cod_empleado='{}'
                order by s.nom_subarea """.format(cod_usuario)

    #print(sql)
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, (cod_usuario, ))
    return resultadosConsulta

def Update_StopMachine(dt_oee):
    v_id_oee = dt_oee.id_oee

    me_conexion = conexion_mssql()

    # DETALLE DEL
    v_sql_det_DEL = "delete from maquinas_detenidas_det where id_oee=%s"
    lista_DEL = []
    lista_DEL.append(v_id_oee)

    # DETALLE
    lista_det = []
    v_sql_det = f"INSERT INTO maquinas_detenidas_det(item, id_oee, tiempo_min, id_equipo, cod_causa, cod_motivo_causa) VALUES (%s, %s, %s, %s, %s, %s) "
    for row in dt_oee.detalle:
        rowD = row.dict()
        lista_det.append(
            (
                rowD["item"],
                v_id_oee,
                rowD["tiempo_min"],
                rowD["id_equipo"],
                rowD["cod_causa"],
                rowD["cod_motivo_causa"],
            )
        )

    dic_DataSave = [{'tipo': 'only', 'columns': v_sql_det_DEL, 'data': tuple(lista_DEL)},
                    {'tipo': 'multi', 'columns': v_sql_det, 'data': lista_det}
                    ]

    if len(lista_det) > 0:
        resultados = me_conexion.ejecutar_funciones_Registro(dic_DataSave)

    else:
        resultados = HTTPException(status_code=401, detail="No hay datos")

    return resultados




def Insert_StopMachine(dt_oee):
    v_id_oee = "{}_{}_{}".format(dt_oee.fecha_doc, dt_oee.cod_empleado, dt_oee.cod_turno)
    #print(v_id_oee)

    me_conexion = conexion_mssql()

    # CABECERA
    v_sql_cab = f"INSERT INTO maquinas_detenidas_cab(id_oee, fecha_doc, cod_empleado, cod_turno, cod_subarea) VALUES (%s, %s, %s, %s, %s)"
    me_conexion = conexion_mssql()
    lista_cab = []
    lista_cab.append(v_id_oee)
    lista_cab.append(dt_oee.fecha_doc)
    lista_cab.append(dt_oee.cod_empleado)
    lista_cab.append(dt_oee.cod_turno)
    lista_cab.append(dt_oee.cod_subarea)


    # DETALLE
    lista_det = []
    v_sql_det = f"INSERT INTO maquinas_detenidas_det(item, id_oee, tiempo_min, id_equipo, cod_causa, cod_motivo_causa) VALUES (%s, %s, %s, %s, %s, %s) "
    for row in dt_oee.detalle:
        rowD = row.dict()
        lista_det.append(
            (
                rowD["item"],
                v_id_oee,
                rowD["tiempo_min"],
                rowD["id_equipo"],
                rowD["cod_causa"],
                rowD["cod_motivo_causa"],
            )
        )


    dic_DataSave = [{'tipo': 'only', 'columns': v_sql_cab, 'data': tuple(lista_cab)},
                    {'tipo': 'multi', 'columns': v_sql_det, 'data': lista_det}
                    ]

    if len(lista_det) > 0:
        # resultados = me_conexion.ejecutar_funciones_CabDet(v_sql_cab, tuple(lista_cab), v_sql_det, lista_det)
        resultados = me_conexion.ejecutar_funciones_Registro(dic_DataSave)

    else:
        resultados = HTTPException(status_code=401, detail="No hay datos")



    return resultados




def Get_StopMachine(dni_empleado, fecha, cod_subarea, cod_turno):

    sqlFind = """ select
                    id_oee
                from maquinas_detenidas_cab
                where cod_empleado='{}'
                and FORMAT(fecha_doc, 'yyyy-MM-dd')='{}'
                and cod_subarea={}
                and cod_turno={} """.format(dni_empleado, fecha, cod_subarea, cod_turno)

    me_conexion = conexion_mssql()
    Resultado_id_oee = me_conexion.consultas(sqlFind, ('',))

    V_id_oee = Resultado_id_oee[0][0] if len(Resultado_id_oee) > 0 else 0



    sql = """ select
                mc.id_oee,
                mc.fecha_doc,
                md.item,
                case when md.tiempo_min is null then 0 else md.tiempo_min end as tiempo_min,
                case when md.cod_causa is null then 0 else md.cod_causa end as cod_causa,
                case when md.cod_motivo_causa is null then 0 else md.cod_motivo_causa end as cod_motivo_causa,
                case when mc.cod_empleado is null then 0 else mc.cod_empleado end as cod_empleado,
                case when mc.cod_turno is null then 0 else mc.cod_turno end as cod_turno,
                e.id_equipo,
                e.equipo,
                case when m.motivo_causa is null then '' else m.motivo_causa end as motivo_causa,
                case when c.causa is null then '' else c.causa end as causa,
                e.cod_subarea
            from equipo e
            inner join empleado emp on emp.cod_subarea=e.cod_subarea
            left join maquinas_detenidas_det md on md.id_equipo=e.id_equipo and md.id_oee='{}'
            left join maquinas_detenidas_cab mc on mc.id_oee=md.id_oee
            left join oee_motivo_causa m on m.cod_causa=md.cod_causa and m.cod_motivo_causa=md.cod_motivo_causa
            left join oee_causa c on c.cod_causa=m.cod_causa
            where
            emp.cod_empleado='{}'
            order by e.equipo """.format(V_id_oee, dni_empleado.strip())

    me_conexion = conexion_mssql()
    resultados = me_conexion.consultas_sgc(sql, ('',))
    return {
        'resultados': resultados,
        'cod_subarea': cod_subarea,
        'id_oee': V_id_oee,
        'cod_empleado': dni_empleado,
        'cod_turno': cod_turno,
    }


def oee_Get_CausaDefault():
    sql = """ select """
    sql += """ 0 as id_oee, """
    sql += """ row_number() OVER (ORDER BY c.causa) as item_oee, """
    sql += """ c.cod_tipo_registro, """
    sql += """ tr.tipo_registro, """
    sql += """ tr.umu, """
    sql += """ c.cod_causa, """
    sql += """ c.causa, """
    sql += """ 0 as cod_motivo_causa, """
    sql += """ '' as motivo_causa, """
    sql += """ 0 as cantidad_prod_buena, """
    sql += """ '' as detalle, """
    sql += """ '' as num_vale, """
    sql += """ 0 as cantidad_prod_mala, """
    sql += """ 0 as cantidad_total, """
    sql += """ c.tiempo_default as tiempo_min, """

    sql += """ '0:00' as hora_ini, """
    sql += """ '0:00' as hora_fin, """

    sql += """ 0 as id_tpoperacion, """
    sql += """ '' as cod_articulo, """
    sql += """ '' as articulo, """
    sql += """ '' as cod_empleado, """
    sql += """ '' as num_of_sap, """
    sql += """ '' as num_paso, """
    sql += """ 0 as std_und_hora """

    sql += """ from oee_tipo_registro tr """
    sql += """ inner join oee_causa c on c.cod_tipo_registro=tr.cod_tipo_registro """
    sql += """ where tr.sw_default='1' """
    sql += """ order by c.causa """
    me_conexion = conexion_mssql()
    resultados = me_conexion.consultas_sgc(sql, ('',))
    return resultados

def oee_Get_NumPaso(cod_articulo, id_equipo):
    sql = """ select DISTINCT """
    sql += """ cd.num_paso, """
    sql += """ t.tipo_operacion, """
    sql += """ cd.id_tpoperacion, """
    sql += """ cd.std_und_hora """
    sql += """ from config_proceso_cab cc """
    sql += """ INNER JOIN config_proceso_det cd ON cc.id_fmproceso=cd.id_fmproceso """
    sql += """ INNER JOIN tipo_operacion t ON t.id_tpoperacion=cd.id_tpoperacion """
    sql += """ WHERE cc.cod_articulo='{}' """.format(cod_articulo)
    sql += """ and cd.id_equipo={} """.format(id_equipo)
    sql += """ ORDER BY cd.num_paso, t.tipo_operacion """
    me_conexion = conexion_mssql()
    resultados = me_conexion.consultas_sgc(sql, (cod_articulo,))
    return resultados

def oee_Get_OnlyMaquinas(cod_subarea):
    sql = """ select
                id_equipo,
                equipo
            from equipo
            where cod_subarea={}
            order by equipo""".format(cod_subarea)

    me_conexion = conexion_mssql()
    resultados = me_conexion.consultas_sgc(sql, ('',))
    return resultados


def oee_Get_Maquinas(cod_articulo, nro_paso, cod_tipo_proceso):
    sql = """ select """
    sql += """ cd.num_paso, """
    sql += """ cd.id_equipo, """
    sql += """ e.equipo, """
    sql += """ cd.id_tpoperacion, """
    sql += """ cd.std_und_hora, """
    sql += """ cd.num_prioridad """
    sql += """ from equipo e """
    sql += """ inner join config_proceso_det cd on cd.id_equipo=e.id_equipo """
    sql += """ inner join config_proceso_cab cc on cc.id_fmproceso=cd.id_fmproceso """
    sql += """ where """
    sql += """ cc.cod_articulo='{}' """.format(cod_articulo)
    sql += """ and cd.num_paso='{}' """.format(nro_paso)
    sql += """ and cd.id_tpoperacion={} """.format(cod_tipo_proceso)
    sql += """ order by e.equipo,cd.num_prioridad """

    me_conexion = conexion_mssql()
    resultados = me_conexion.consultas_sgc(sql, (cod_articulo, nro_paso))
    return resultados

# ============================================================================
def oee_OrdenFabricacion(cod_subarea, nroOrdenFab):
    v_db = "SBO_TECNO_PRODUCCION"

    cod_subarea_sap = ""
    dt_areas = subAreaFunction.get_subarea_id(cod_subarea)
    for row in dt_areas:
        cod_subarea_sap = str(row["cod_subarea_sap"])

    sql = """ select"""
    sql += """ OFA."DocNum" AS "num_of","""
    sql += """ OFA."ItemCode" AS "cod_articulo","""
    sql += """ OFA."ProdName" AS "articulo","""
    sql += """ OFA."Status" AS "estado","""
    sql += """ ("""
    sql += """ 	CASE WHEN OFA."Status" = 'C' THEN 'CANCELADO'"""
    sql += """ 		 WHEN OFA."Status" = 'L' THEN 'CERRADO'"""
    sql += """ 		 WHEN OFA."Status" = 'P' THEN 'PLANIFICADO'"""
    sql += """ 		 WHEN OFA."Status" = 'R' THEN 'LIBERADO'"""
    sql += """ 		 ELSE ''"""
    sql += """ 	END """
    sql += """ ) AS "estado_desc","""
    sql += """ TO_VARCHAR(OFA."PostDate",'YYYY/MM/DD') AS "fecha_doc","""
    sql += """ ART."U_MSSC_ARE" AS "cod_area","""
    sql += """ ("""
    sql += """ 	select "Descr" from """ + v_db + """.UFD1 """
    sql += """ 	WHERE "TableID" = 'OITM' AND "FieldID" = '21' AND "FldValue" = ART."U_MSSC_ARE" """
    sql += """ ) AS "area" """
    sql += """ from """ + v_db + """.OWOR as OFA"""
    sql += """ INNER JOIN """ + v_db + """.OITM AS ART ON ART."ItemCode" = OFA."ItemCode" """

    sql += """ where OFA."Status" in ('P','R')"""
    sql += """ and OFA."Series" = '126' """
    sql += """ and ART."U_MSSC_ARE" = '""" + cod_subarea_sap + """' """

    sql += """ and UPPER(CONCAT(OFA."DocNum", OFA."ProdName")) like UPPER('%""" + nroOrdenFab + """%') """

    sql += """ order by OFA."PostDate" desc,  OFA."DocNum" asc """

    #print(sql)

    me_conexion = conexion_sap()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta


def oee_empleados(cod_empleado, cod_subarea=0):
    #print("tututut")
    sql = """ SELECT
                    e.cod_empleado,
                    e.nombre,
                    e.cod_subarea,
                    sa.nom_subarea
                FROM empleado e
                INNER JOIN sub_area sa ON sa.cod_subarea=e.cod_subarea
                WHERE e.cod_empleado='{}' 
                AND e.cod_subarea={} """.format(cod_empleado, cod_subarea)
    #print("@"*20)
    #print(sql)
    #print("@" * 20)
    me_conexion = conexion_mssql()
    resultados = me_conexion.consultas_sgc(sql, (cod_empleado, ))
    return resultados



def oee_nuevo():
    sql = f"SELECT CASE WHEN max(id_oee) is null THEN 1 ELSE max(id_oee) + 1 END AS nuevo_id FROM oee_cab"
    me_conexion = conexion_mssql()
    resultados = me_conexion.consultas_sgc(sql, ())
    return resultados

def oee_all(fecha, cod_empleado):
    sql = "SELECT "
    sql += " oec.id_oee, "
    sql += " oec.sw_estado, "
    sql += " CONVERT(DATE, oec.fecha_doc, 103) AS fecha_doc, "
    sql += " e.nombre,  "
    sql += " a.articulo,  "
    sql += " eq.equipo,  "
    sql += " oec.num_paso  "
    sql += " FROM oee_cab oec "
    sql += " INNER JOIN empleado e ON oec.cod_empleado=e.cod_empleado "
    sql += " INNER JOIN articulo_familia a ON oec.cod_articulo=a.cod_articulo "
    sql += " INNER JOIN equipo eq ON eq.id_equipo=oec.id_equipo  "
    sql += " WHERE oec.sw_estado in ('1','0')  "
    sql += " AND CONVERT(DATE, oec.fecha_doc, 103)='{}' ".format(fecha)
    sql += " AND oec.cod_empleado='{}'  ".format(cod_empleado)
    sql += " ORDER BY oec.id_oee DESC "


    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, (fecha, cod_empleado))
    return resultadosConsulta


def oee_activo():
    sql = f"SELECT * FROM oee_cab WHERE sw_estado in ('1')"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta


def oee_det(id_oee):
    sql = f" SELECT * FROM oee_det WHERE id_oee = (%s)"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, (id_oee,))
    return resultadosConsulta


def oee_insert(dt_oee):
    # obtener id de plan de ventas
    c_id = "0"
    c_json_res = oee_nuevo()
    for row in c_json_res:
        c_id = str(row["nuevo_id"])

    if c_id == 0:
        return HTTPException(status_code=401, detail="Error en generar ID")

    # CABECERA
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(c_id)
    lista_cab.append(dt_oee.fecha_doc)
    lista_cab.append(dt_oee.cod_subarea)
    lista_cab.append(dt_oee.num_of_sap)
    lista_cab.append(dt_oee.cod_empleado)
    lista_cab.append(dt_oee.cod_articulo)
    lista_cab.append(dt_oee.articulo)
    lista_cab.append(dt_oee.num_paso)
    lista_cab.append(dt_oee.id_equipo)
    lista_cab.append(dt_oee.id_tpoperacion)
    lista_cab.append(dt_oee.cod_turno)
    lista_cab.append(dt_oee.hora_ini)
    lista_cab.append(dt_oee.hora_fin)
    lista_cab.append(dt_oee.std_und_hora)
    lista_cab.append(dt_oee.hora_planificado)
    lista_cab.append(dt_oee.sw_estado)

    # CABECERA
    v_sql_cab = f"INSERT INTO oee_cab (id_oee,fecha_doc,cod_subarea,num_of_sap,cod_empleado,cod_articulo,articulo,num_paso,id_equipo,id_tpoperacion,cod_turno,hora_ini,hora_fin,std_und_hora,hora_planificado,sw_estado) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    me_conexion = conexion_mssql()

    # DETALLE
    lista_det = []
    v_sql_det = f"INSERT INTO oee_det (id_oee,item_oee,cod_tipo_registro,tipo_registro,cod_causa,causa,cod_motivo_causa,motivo_causa,umu,cantidad_prod_buena,detalle,num_vale, cantidad_prod_mala, cantidad_total, tiempo_min) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    for row in dt_oee.detalle:
        rowD = row.dict()
        lista_det.append(
            (
                c_id,
                rowD["item_oee"],
                rowD["cod_tipo_registro"],
                rowD["tipo_registro"],
                rowD["cod_causa"],
                rowD["causa"],
                rowD["cod_motivo_causa"],
                rowD["motivo_causa"],
                rowD["umu"],
                rowD["cantidad_prod_buena"],
                rowD["detalle"],
                rowD["num_vale"],

                rowD["cantidad_prod_mala"],
                rowD["cantidad_total"],
                rowD["tiempo_min"],
            )
        )

    # resultados = me_conexion.ejecutar_funciones_multi(v_sql, lista_det)}
    if len(lista_det) > 0:
        resultados = me_conexion.ejecutar_funciones_CabDet(
            v_sql_cab, tuple(lista_cab), v_sql_det, lista_det
        )
    else:
        resultados = HTTPException(status_code=401, detail="No hay datos")

    return resultados


def oee_insertRegistro(dt_oee):
    #print("@"*20)
    #print(dt_oee)
    #print("@" * 20)

    # obtener id de plan de ventas
    c_id = "0"
    c_json_res = oee_nuevo()
    for row in c_json_res:
        c_id = str(row["nuevo_id"])

    if c_id == 0:
        return HTTPException(status_code=401, detail="Error en generar ID")

    # CABECERA
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(c_id)
    lista_cab.append(dt_oee.fecha_doc)
    lista_cab.append(dt_oee.cod_subarea)
    # lista_cab.append(dt_oee.num_of_sap)
    # lista_cab.append(dt_oee.cod_empleado)
    # lista_cab.append(dt_oee.cod_articulo)
    # lista_cab.append(dt_oee.articulo)
    # lista_cab.append(dt_oee.num_paso)
    lista_cab.append(dt_oee.id_equipo)
    # lista_cab.append(dt_oee.id_tpoperacion)
    lista_cab.append(dt_oee.cod_turno)
    lista_cab.append(dt_oee.hora_ini)
    lista_cab.append(dt_oee.hora_fin)
    # lista_cab.append(dt_oee.std_und_hora)
    lista_cab.append(dt_oee.hora_planificado)
    lista_cab.append(dt_oee.sw_estado)

    lista_cab.append(dt_oee.horas_operativo)
    lista_cab.append(dt_oee.produccion_total)
    lista_cab.append(dt_oee.horas_perdidas)

    lista_cab.append(dt_oee.hora_pausas)
    lista_cab.append(dt_oee.hora_paradas)



    # CABECERA
    # v_sql_cab = f"INSERT INTO oee_cab (id_oee,fecha_doc,cod_subarea,num_of_sap,cod_empleado,cod_articulo,articulo,num_paso,id_equipo,id_tpoperacion,cod_turno,hora_ini,hora_fin,std_und_hora,hora_planificado,sw_estado, horas_operativo, produccion_total, horas_perdidas) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    v_sql_cab = f"INSERT INTO oee_cab (id_oee,fecha_doc,cod_subarea,id_equipo,cod_turno,hora_ini,hora_fin,hora_planificado,sw_estado, horas_operativo, produccion_total, horas_perdidas, hora_pausas, hora_paradas) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    me_conexion = conexion_mssql()

    # DETALLE
    lista_det = []
    # v_sql_det = f"INSERT INTO oee_det (id_oee,item_oee,cod_tipo_registro,tipo_registro,cod_causa,causa,cod_motivo_causa,motivo_causa,umu,cantidad_prod_buena,detalle,num_vale, cantidad_prod_mala, cantidad_total, tiempo_min, hora_ini, hora_fin) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    v_sql_det = f"INSERT INTO oee_det (id_oee,item_oee,cod_tipo_registro,tipo_registro,cod_causa,causa,cod_motivo_causa,motivo_causa,umu,cantidad_prod_buena,detalle,num_vale, cantidad_prod_mala, cantidad_total, tiempo_min, hora_ini, hora_fin, id_tpoperacion, cod_articulo, articulo, cod_empleado, num_of_sap, num_paso, std_und_hora, rendimiento_porcentaje) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

    #print("5"*20)
    #print(dt_oee.detalle)
    #(print
    # ("5" * 20))

    for row in dt_oee.detalle:
        rowD = row.dict()
        lista_det.append(
            (
                c_id,
                rowD["item_oee"],
                rowD["cod_tipo_registro"],
                rowD["tipo_registro"],
                rowD["cod_causa"],
                rowD["causa"],
                rowD["cod_motivo_causa"],
                rowD["motivo_causa"],
                rowD["umu"],
                rowD["cantidad_prod_buena"],
                rowD["detalle"],
                rowD["num_vale"],
                rowD["cantidad_prod_mala"],
                rowD["cantidad_total"],
                rowD["tiempo_min"],

                rowD["hora_ini"],
                rowD["hora_fin"],

                rowD["id_tpoperacion"],
                rowD["cod_articulo"],
                rowD["articulo"],
                rowD["cod_empleado"],
                rowD["num_of_sap"],
                rowD["num_paso"],
                rowD["std_und_hora"],

                rowD["rendimiento_porcentaje"],

            )
        )

    # INDICADORES
    lista_indicadores = []
    v_sql_indicadores = f"INSERT INTO oee_indicadores(id_oee, cod_motivo_indicador, motivo_causa, cantidad, tipo_registro, causa, id_config_oee, umu) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    n=0
    for row in dt_oee.indicadores:
        n+=1
        rowD = row.dict()
        lista_indicadores.append(
            (
                c_id,
                rowD["cod_motivo_indicador"],
                rowD["motivo_causa"],
                rowD["cantidad"],
                rowD["tipo_registro"],
                rowD["causa"],
                rowD["id_config_oee"],
                rowD["umu"]
            )
        )


    dic_DataSave = [{'tipo': 'only', 'columns': v_sql_cab, 'data': tuple(lista_cab)},
                    {'tipo': 'multi', 'columns': v_sql_det, 'data': lista_det},
                    {'tipo': 'multi', 'columns': v_sql_indicadores, 'data': lista_indicadores}
                    ]


    if len(lista_det) > 0:
        resultados = me_conexion.ejecutar_funciones_Registro(dic_DataSave)
    else:
        resultados = HTTPException(status_code=401, detail="No hay datos")

    return resultados


def oee_updateRegistro(dt_oee):
    # DELETE
    v_sql_DelReg = f"DELETE FROM oee_det WHERE id_oee=%s"
    listaDelDet = []
    listaDelDet.append(dt_oee.id_oee)

    v_sql_DelInd = f"DELETE FROM oee_indicadores WHERE id_oee=%s"
    listaDelInd = []
    listaDelInd.append(dt_oee.id_oee)


    # CABECERA
    v_sql_Cab = f"UPDATE oee_cab SET"
    # v_sql_Cab += f" std_und_hora=%s,"
    v_sql_Cab += f" hora_planificado=%s,"
    v_sql_Cab += f" horas_operativo=%s,"
    v_sql_Cab += f" produccion_total=%s, "
    v_sql_Cab += f" horas_perdidas=%s, "
    v_sql_Cab += f" cod_turno=%s, "

    v_sql_Cab += f" hora_pausas=%s, "
    v_sql_Cab += f" hora_paradas=%s "

    v_sql_Cab += f" WHERE id_oee=%s"
    listaCab = []
    # listaCab.append(dt_oee.std_und_hora)
    listaCab.append(dt_oee.hora_planificado)
    listaCab.append(dt_oee.horas_operativo)
    listaCab.append(dt_oee.produccion_total)
    listaCab.append(dt_oee.horas_perdidas)
    listaCab.append(dt_oee.cod_turno)
    listaCab.append(dt_oee.hora_pausas)
    listaCab.append(dt_oee.hora_paradas)
    listaCab.append(dt_oee.id_oee)



    # DETALLE
    lista_det = []
    # v_sql_det = f"-- INSERT INTO oee_det (id_oee,item_oee,cod_tipo_registro,tipo_registro,cod_causa,causa,cod_motivo_causa,motivo_causa,umu,cantidad_prod_buena,detalle,num_vale, cantidad_prod_mala, cantidad_total, tiempo_min) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    v_sql_det = f"INSERT INTO oee_det (id_oee,item_oee,cod_tipo_registro,tipo_registro,cod_causa,causa,cod_motivo_causa,motivo_causa,umu,cantidad_prod_buena,detalle,num_vale, cantidad_prod_mala, cantidad_total, tiempo_min, hora_ini, hora_fin, id_tpoperacion, cod_articulo, articulo, cod_empleado, num_of_sap, num_paso, std_und_hora, rendimiento_porcentaje) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    for row in dt_oee.detalle:
        rowD = row.dict()
        lista_det.append(
            (
                dt_oee.id_oee,
                rowD["item_oee"],
                rowD["cod_tipo_registro"],
                rowD["tipo_registro"],
                rowD["cod_causa"],
                rowD["causa"],
                rowD["cod_motivo_causa"],
                rowD["motivo_causa"],
                rowD["umu"],
                rowD["cantidad_prod_buena"],
                rowD["detalle"],
                rowD["num_vale"],

                rowD["cantidad_prod_mala"],
                rowD["cantidad_total"],
                rowD["tiempo_min"],

                rowD["hora_ini"],
                rowD["hora_fin"],

                rowD["id_tpoperacion"],
                rowD["cod_articulo"],
                rowD["articulo"],
                rowD["cod_empleado"],
                rowD["num_of_sap"],
                rowD["num_paso"],
                rowD["std_und_hora"],

                rowD["rendimiento_porcentaje"],
            )
        )

    # INDICADORES
    lista_indicadores = []
    v_sql_indicadores = f"INSERT INTO oee_indicadores(id_oee, cod_motivo_indicador, motivo_causa, cantidad, tipo_registro, causa, id_config_oee, umu) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    n = 0
    for row in dt_oee.indicadores:
        n += 1
        rowD = row.dict()
        lista_indicadores.append(
            (
                dt_oee.id_oee,
                rowD["cod_motivo_indicador"],
                rowD["motivo_causa"],
                rowD["cantidad"],
                rowD["tipo_registro"],
                rowD["causa"],
                rowD["id_config_oee"],
                rowD["umu"]
            )
        )

    dic_DataSave = [{'tipo': 'only', 'columns': v_sql_DelReg, 'data': tuple(listaDelDet)},
                    {'tipo': 'only', 'columns': v_sql_DelInd, 'data': tuple(listaDelInd)},
                    {'tipo': 'only', 'columns': v_sql_Cab, 'data': tuple(listaCab)},
                    {'tipo': 'multi', 'columns': v_sql_det, 'data': lista_det},
                    {'tipo': 'multi', 'columns': v_sql_indicadores, 'data': lista_indicadores}
                    ]

    me_conexion = conexion_mssql()
    if len(lista_det) > 0:
        resultados = me_conexion.ejecutar_funciones_Registro(dic_DataSave)
    else:
        resultados = HTTPException(status_code=401, detail="No hay datos")

    return resultados



def oee_update(dt_oee):
    # CABECERA
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(dt_oee.fecha_doc)
    lista_cab.append(dt_oee.glosa)
    lista_cab.append(dt_oee.sw_estado)
    lista_cab.append(dt_oee.cod_usuario)
    lista_cab.append(dt_oee.id_oee)

    # ejecutar cabecera
    v_sql_cab = f"DELETE FROM oee_det WHERE id_oee = " + str(dt_oee.id_oee) + ";"
    v_sql_cab += f"UPDATE oee_cab SET fecha_doc = %s, glosa = %s, sw_estado = %s, cod_usuario = %s WHERE id_oee = %s"

    # DETALLE
    lista_det = []
    v_sql_det = f"INSERT INTO oee_det (id_oee,cod_articulo,nom_articulo,cod_grupo) VALUES (%s, %s, %s, %s)"
    for row in dt_oee.detalle:
        rowD = row.dict()
        lista_det.append(
            (
                str(dt_oee.id_oee),
                rowD["cod_articulo"],
                rowD["nom_articulo"],
                rowD["cod_grupo"],
            )
        )
    # resultados = me_conexion.ejecutar_funciones_multi(v_sql, lista_det)}
    me_conexion = conexion_mssql()
    if len(lista_det) > 0:
        resultados = me_conexion.ejecutar_funciones_CabDet(
            v_sql_cab, tuple(lista_cab), v_sql_det, lista_det
        )
    else:
        resultados = HTTPException(status_code=401, detail="No hay datos")
    return resultados


def oee_delete(id_oee):
    sql_Det = """ DELETE FROM oee_det WHERE id_oee=%s; """
    ListDet = []
    ListDet.append(id_oee)


    sql_Ind = """ DELETE FROM oee_indicadores WHERE id_oee=%s; """
    ListInd = []
    ListInd.append(id_oee)


    sql_Cab = """ DELETE FROM oee_cab WHERE id_oee=%s; """
    ListCab = []
    ListCab.append(id_oee)


    dic_DataSave = [{'tipo': 'only', 'columns': sql_Det, 'data': tuple(ListDet)},
                    {'tipo': 'only', 'columns': sql_Ind, 'data': tuple(ListInd)},
                    {'tipo': 'only', 'columns': sql_Cab, 'data': tuple(ListCab)}
                    ]


    me_conexion = conexion_mssql()
    if len(ListDet) > 0:
        resultados = me_conexion.ejecutar_funciones_Registro(dic_DataSave)
    else:
        resultados = HTTPException(status_code=401, detail="No hay datos")

    return resultados