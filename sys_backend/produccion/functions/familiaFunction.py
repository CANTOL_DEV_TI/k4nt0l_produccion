from conexiones.conexion_mmsql import conexion_mssql


def insert(meData):
    try:
        valores = []
        valores.append(meData.nom_familia)
        valores.append(meData.cod_usuario)
        sql = "insert into familia(nom_familia,sw_estado,cod_usuario,fecre) values(%s,'1',%s,getdate())"

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def update(meData):
    try:
        valores = []
        valores.append(meData.nom_familia)
        valores.append(meData.id_familia)
        sql = "update familia set "
        sql += "nom_familia=%s "
        sql += "where id_familia=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def delete(meData):
    try:
        valores = []
        valores.append(meData.id_familia)
        sql = "update familia set sw_estado='9' "
        sql += "where id_familia=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        #print(sql)
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."