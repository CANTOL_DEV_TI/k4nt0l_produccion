from conexiones.conexion_mmsql import conexion_mssql


def insert(meData):
    try:
        valores = []
        valores.append(meData.nom_area)
        sql = "insert into area(nom_area) values(%s)"

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def update(meData):
    try:
        valores = []
        valores.append(meData.nom_area)
        valores.append(meData.cod_area)
        sql = "update area set "
        sql += "nom_area=%s "
        sql += "where cod_area=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def delete(meData):
    try:
        valores = []
        valores.append(meData.cod_area)
        sql = " delete from area "
        sql += " where cod_area=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."