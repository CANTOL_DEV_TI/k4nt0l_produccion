from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql


def preventivo_nuevo():
    try:
        sql = f"SELECT CASE WHEN max(id_preventivo) is null THEN 1 ELSE max(id_preventivo) + 1 END AS nuevo_id FROM preventivo_cab"
        me_conexion = conexion_mssql()
        resultados = me_conexion.consultas_sgc(sql, ())
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def preventivo_all():
    try:
        sql = f" SELECT "
        sql += f" pc.id_preventivo, "
        sql += f" CONVERT(DATE, pc.fecha_plan, 103) AS fecha_plan, "
        sql += f" pc.glosa, "
        sql += f" ("
        sql += f" 	select CONVERT(varchar,count(*))"
        sql += f" 	from plan_capacidad_pp_cab where id_preventivo = pc.id_preventivo"
        sql += f" ) as sw_preventivo"
        sql += f" FROM preventivo_cab pc"
        sql += f" ORDER BY id_preventivo DESC"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."


def preventivo_det(id_preventivo):
    try:
        sql = f" select"
        sql += f" det.id_preventivo,"
        sql += f" det.id_equipo,"
        sql += f" eq.cod_equipo,"
        sql += f" eq.equipo,"
        sql += f" det.cantidad"
        sql += f" from preventivo_det det"
        sql += f" inner join equipo eq on eq.id_equipo = det.id_equipo"
        sql += f" where det.id_preventivo = (%s)"

        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, (id_preventivo,))
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."


def preventivo_insert(dt_preventivo):
    try:
        # obtener id de plan de ventas
        c_id = "0"
        c_json_res = preventivo_nuevo()
        for row in c_json_res:
            c_id = str(row["nuevo_id"])

        if c_id == 0:
            return HTTPException(status_code=401, detail="Error en generar ID")

        # CABECERA
        # añadir valores del diccionario cabecera
        lista_cab = []
        lista_cab.append(c_id)
        lista_cab.append(dt_preventivo.fecha_plan)
        lista_cab.append(dt_preventivo.glosa)

        # CABECERA
        v_sql_cab = f"INSERT INTO preventivo_cab (id_preventivo,fecha_plan,glosa) VALUES (%s, %s, %s)"
        me_conexion = conexion_mssql()

        # DETALLE
        lista_det = []
        v_sql_det = f"INSERT INTO preventivo_det (id_preventivo,id_equipo,cantidad) VALUES (%s, %s, %s)"
        for row in dt_preventivo.detalle:
            rowD = row.dict()
            lista_det.append(
                (c_id, rowD["id_equipo"], rowD["cantidad"])
            )
        # resultados = me_conexion.ejecutar_funciones_multi(v_sql, lista_det)}
        if len(lista_det) > 0:
            resultados = me_conexion.ejecutar_funciones_CabDet(
                v_sql_cab, tuple(lista_cab), v_sql_det, lista_det
            )
        else:
            resultados = HTTPException(status_code=401, detail="No hay datos")

        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def preventivo_update(dt_preventivo):
    try:
        # CABECERA
        # añadir valores del diccionario cabecera
        lista_cab = []
        lista_cab.append(dt_preventivo.fecha_plan)
        lista_cab.append(dt_preventivo.glosa)
        lista_cab.append(dt_preventivo.id_preventivo)

        # ejecutar cabecera
        v_sql_cab = (
            f"DELETE FROM preventivo_det WHERE id_preventivo = "
            + str(dt_preventivo.id_preventivo)
            + ";"
        )
        v_sql_cab += f"UPDATE preventivo_cab SET fecha_plan = %s, glosa = %s WHERE id_preventivo = %s"

        # DETALLE
        lista_det = []
        v_sql_det = f"INSERT INTO preventivo_det (id_preventivo,id_equipo,cantidad) VALUES (%s, %s, %s)"
        for row in dt_preventivo.detalle:
            rowD = row.dict()
            lista_det.append(
                (
                    str(dt_preventivo.id_preventivo),
                    rowD["id_equipo"],
                    rowD["cantidad"],
                )
            )
        # resultados = me_conexion.ejecutar_funciones_multi(v_sql, lista_det)}
        me_conexion = conexion_mssql()
        if len(lista_det) > 0:
            resultados = me_conexion.ejecutar_funciones_CabDet(
                v_sql_cab, tuple(lista_cab), v_sql_det, lista_det
            )
        else:
            resultados = HTTPException(status_code=401, detail="No hay datos")
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def preventivo_delete(id_preventivo):
    try:
        # ejecutar procedimiento
        me_conexion = conexion_mssql()
        resultados = me_conexion.ejecutar_store_crud(
            "PRODUCION_PREVENTIVO_DELETE", (id_preventivo,)
        )
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."
