# from conexiones.conexion_sap import conexion_sap
from conexiones.conexion_sap import conexion_sap

v_db = "SBO_TECNO_PRODUCCION"


class AreaResponsable:
    def __init__(self):
        self.__meConexion = conexion_sap()
    
    def get_areaResponsable_sap(self):
        sql = """ SELECT * """
        sql += """ FROM """ + v_db + """.SGC_INVENTARIO_AREA_RESPONSABLE"""
        me_conexion = conexion_sap()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ('',))
        return resultadosConsulta