from produccion.functions.Class.planCapacidadPPCalcular import planCapacidadPPCalcular
from conexiones.conexion_mmsql import conexion_mssql


def saveProgramacion(meData):
    #print("save Programacion:",meData)
    meSave = planCapacidadPPCalcular(meData)
    resultados = meSave.calcular()
    #print("resultado savePro:",resultados)
    return resultados


def deleteProgramacion(meData):
    valores = []
    valores.append(meData)
    valores.append(meData)
    valores.append(meData)
    sql = " delete from plan_capacidad_pp_det_xpaso "
    sql += " where id_plan_capacidad_pp=%s; "

    sql += " delete from plan_capacidad_pp_det "
    sql += " where id_plan_capacidad_pp=%s; "

    sql += " delete from plan_capacidad_pp_cab "
    sql += " where id_plan_capacidad_pp=%s; "

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    #print(sql)
    return respuesta


