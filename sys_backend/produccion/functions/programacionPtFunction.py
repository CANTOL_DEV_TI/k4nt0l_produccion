from produccion.functions.Class.programacionPT import programacionPT
# programacionPtFunction


def getProgramacion(idPlaneacion, txtFind):
    try:
        meConsulta = programacionPT()
        resultados = meConsulta.consultaPT(idPlaneacion, txtFind)
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."

def updateProgramacion(meData):
    try:
        meUpdate = programacionPT()
        resultados = meUpdate.updateProgramacion(meData)
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def deleteProgramacion(meData):
    try:
        meDelete = programacionPT()
        resultados = meDelete.deleteProgramacion(meData)
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."