from conexiones.conexion_mmsql import conexion_mssql

def insert(meData):
    try:
        valores = []
        valores.append(meData.cod_almacen)
        valores.append(meData.cod_subarea)
        valores.append(meData.nom_subarea_almacen)
        sql = "insert into subarea_almacen(cod_almacen, cod_subarea, nom_subarea_almacen) values(%s, %s, %s)"

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."

def update(meData):
    try:
        valores = []
        valores.append(meData.cod_almacen)
        valores.append(meData.cod_subarea)
        valores.append(meData.nom_subarea_almacen)
        valores.append(meData.cod_subarea_almacen)
        sql = "update subarea_almacen set "
        sql += "cod_almacen=%s, "
        sql += "cod_subarea=%s, "
        sql += "nom_subarea_almacen=%s "
        sql += "where cod_subarea_almacen=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."

def delete(meData):
    try:
        valores = []
        valores.append(meData.cod_subarea_almacen)
        sql = "delete from subarea_almacen "
        sql += "where cod_subarea_almacen=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."




