from produccion.functions.Class.planCapacidadPTDatos import planCapacidadPTDatos
from produccion.functions.Class.planCapacidadPTCalcular import planCapacidadPTCalcular
from conexiones.conexion_mmsql import conexion_mssql
from conexiones.conexion_sap import conexion_sap
from datetime import datetime


def insert():
    exe_CapacidadPTDatos = planCapacidadPTDatos()
    rpta_CapacidadPTDatos = exe_CapacidadPTDatos.insert()
    # print(rpta_CapacidadPTDatos)
    if not rpta_CapacidadPTDatos:
        return {"Mensage": False}

    exe_planCapacidadPTCalcular = planCapacidadPTCalcular()
    rpta_ok = exe_planCapacidadPTCalcular.calcular_datos(rpta_CapacidadPTDatos)

    return {"Mensage": rpta_ok}


def planCapacidadPeriodo(id_plan_capacidad):
    sql = f"SELECT ejercicio_tarea, periodo_tarea FROM plan_capacidad_pt_cab pc "
    sql += f" INNER JOIN tarea_programada_usuario tp on tp.id_tarea = pc.id_tarea"
    sql += f" WHERE pc.id_plan_capacidad = %s"
    me_conexion = conexion_mssql()
    resultados = me_conexion.consultas_sgc(sql, (id_plan_capacidad,))
    return resultados


def produccion_real(id_plan_capacidad):
    # obtener ejercicio y periodo
    c_ejercicio = 0
    c_periodo = 0
    c_json_res = planCapacidadPeriodo(id_plan_capacidad)
    for row in c_json_res:
        c_ejercicio = str(row["ejercicio_tarea"])
        c_periodo = str(row["periodo_tarea"])

    if c_ejercicio == 0 or c_periodo == 0:
        return False

    # valida para ejecutar solo cuando sea mes actual
    c_a_actual = str(datetime.now().year)
    c_m_actua = str(datetime.now().month)
    if (c_ejercicio + c_periodo) != (c_a_actual + c_m_actua):
        return False

    # obtener producción real sap
    v_db = "SBO_TECNO_PRODUCCION"
    sql = (
        f"SELECT * FROM "
        + v_db
        + ".SGC_PRODUCCION_MENSUAL_OK(%s,%s)" % (c_ejercicio, c_periodo)
    )
    me_conexion = conexion_sap()
    res_datos_sap = me_conexion.consultas_sgc(sql, ("",))

    # borrar columna producción real
    sql = f"UPDATE plan_capacidad_pt_det SET cant_real = 0 WHERE id_plan_capacidad = %s"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.ejecutar_funciones(sql, (id_plan_capacidad,))

    # actualizar producción real
    lista = []
    for row in res_datos_sap:
        lista.append(
            (
                row["PRODUCIDO"] + row["REPROCESO"],
                id_plan_capacidad,
                row["COD_ARTICULO"],
            )
        )

    sql = f"UPDATE plan_capacidad_pt_det SET cant_real = %s WHERE id_plan_capacidad = %s and cod_articulo = %s;"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.ejecutar_funciones_multi(sql, lista)
    return resultadosConsulta


def planCapacidadPTDet_consolidado(id_plan_capacidad, id_familia, abc):
    # actualizar datos de producción real
    resultado = produccion_real(id_plan_capacidad)

    # consulta
    sql = f"SELECT * FROM PRODUCCION_PLAN_CAPACIDAD_PT_SEARCH (%s,%s,%s)"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(
        sql, (id_plan_capacidad, id_familia, abc)
    )
    # ordenar segun planificado de mayor a menor
    resultado_orden = sorted(
        resultadosConsulta, key=lambda x: x["cant_planificada"], reverse=True
    )

    return resultado_orden
