from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql


def oee_tipo_registro_all(cod_tipo_registro, sw_estado, cod_subarea):
    sql = f" SELECT "
    sql += f" pl.*, "
    sql += f" (select sw_estado from oee_config_formulario where cod_tipo_registro=pl.cod_tipo_registro and cod_subarea=" + str(cod_subarea) + ") as sw_horario"
    sql += f" FROM oee_tipo_registro pl"
    sql += f" WHERE pl.sw_estado in ('1','0')"
    if cod_tipo_registro != "0":
        sql += f" AND pl.cod_tipo_registro = '" + cod_tipo_registro + "'"
    if sw_estado != "0":
        sql += f" AND pl.sw_estado = '" + sw_estado + "'"

    # sql += f" AND pl.sw_default<>'1' "

    #print(sql)

    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta

def get_oee_TipoRegistro(txt):
    sql = """ select
                cod_tipo_registro,
                tipo_registro,
                umu,
                sw_estado,
                label_name,
                style_principal,
                style_secundario,
                sw_default
            from oee_tipo_registro
            where upper(tipo_registro) like UPPER('{}{}{}')
            order by tipo_registro """.format('%', txt, '%')

    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta


def oee_tipo_registro_insert(dt_oee_tipo_registro):
    # DATOS
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(dt_oee_tipo_registro.ejercicio)
    lista_cab.append(dt_oee_tipo_registro.periodo)
    lista_cab.append(dt_oee_tipo_registro.sw_estado)
    lista_cab.append(dt_oee_tipo_registro.cod_tipo_registro)
    lista_cab.append(dt_oee_tipo_registro.total_dia_lab)
    lista_cab.append(dt_oee_tipo_registro.sw_estado)
    lista_cab.append(dt_oee_tipo_registro.cod_usuario)

    # ejecutar cabecera
    v_sql = f"INSERT INTO oee_tipo_registro (ejercicio,periodo,sw_estado,cod_tipo_registro,total_dia_lab,sw_estado,cod_usuario)"
    v_sql += f" VALUES (%s, %s, %s, %s, %s, %s, %s)"
    me_conexion = conexion_mssql()
    resultados = me_conexion.ejecutar_funciones(v_sql, tuple(lista_cab))
    return resultados


def oee_tipo_registro_update(dt_oee_tipo_registro):
    # CABECERA
    # añadir valores del diccionario cabecera
    lista_cab = []
    lista_cab.append(dt_oee_tipo_registro.total_dia_lab)
    lista_cab.append(dt_oee_tipo_registro.sw_estado)
    lista_cab.append(dt_oee_tipo_registro.cod_usuario)
    lista_cab.append(dt_oee_tipo_registro.id_per_lab)

    # ejecutar cabecera
    v_sql = f"UPDATE oee_tipo_registro SET total_dia_lab = %s, sw_estado = %s, cod_usuario = %s WHERE id_per_lab = %s"
    me_conexion = conexion_mssql()
    resultados = me_conexion.ejecutar_funciones(v_sql, tuple(lista_cab))
    return resultados


def oee_tipo_registro_delete(id_per_lab):
    sql = f"DELETE FROM oee_tipo_registro WHERE id_per_lab = %s"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.ejecutar_funciones(sql, (id_per_lab,))
    return resultadosConsulta
