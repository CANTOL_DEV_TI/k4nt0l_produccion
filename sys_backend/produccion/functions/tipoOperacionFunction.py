from conexiones.conexion_mmsql import conexion_mssql


def insert(meData):
    try:
        valores = []
        valores.append(meData.tipo_operacion)    
        sql = "insert into tipo_operacion(tipo_operacion) values(%s)"

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def update(meData):
    try:
        valores = []
        valores.append(meData.tipo_operacion)
        valores.append(meData.id_tpoperacion)
        sql = "update tipo_operacion set "
        sql += "tipo_operacion=%s "
        sql += "where id_tpoperacion=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def delete(meData):
    try:
        valores = []
        valores.append(meData.id_tpoperacion)
        sql = "delete tipo_operacion "
        sql += "where id_tpoperacion=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."
