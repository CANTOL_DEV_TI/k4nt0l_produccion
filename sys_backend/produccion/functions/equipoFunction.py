from conexiones.conexion_mmsql import conexion_mssql

def equipo_buscar():
    try:
        sql = f"SELECT id_equipo, cod_equipo, equipo FROM equipo ORDER BY equipo"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."

def insert(meData):
    try:
        valores = []
        valores.append(meData.cod_subarea)
        valores.append(meData.id_tpequipo)
        valores.append(meData.cod_equipo)
        valores.append(meData.equipo)
        valores.append(meData.oferta_horas_x_dia)
        valores.append(meData.factor_desviacion)
        #valores.append(meData.sw_operativo)
        valores.append(meData.total_turnos)
        valores.append(meData.disponibilidad_historico)
        #print(valores)
        sql = "insert into equipo(cod_subarea,id_tpequipo,cod_equipo,equipo,oferta_horas_x_dia,factor_desviacion,sw_operativo,total_turnos,disponibilidad_historico) values(%s,%s,%s,%s,%s,%s,1,%s,%s)"
        #print(sql)
        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def update(meData):
    try:
        valores = []
        valores.append(meData.cod_subarea)
        valores.append(meData.id_tpequipo)
        valores.append(meData.cod_equipo)
        valores.append(meData.equipo)
        valores.append(meData.oferta_horas_x_dia)
        valores.append(meData.factor_desviacion)
        valores.append(bool(meData.sw_operativo))
        valores.append(meData.total_turnos)
        valores.append(meData.disponibilidad_historico)
        valores.append(meData.id_equipo)
        
        sql = "update equipo set "
        sql += "cod_subarea = %s , id_tpequipo = %s , cod_equipo = %s , equipo = %s , oferta_horas_x_dia = %s , factor_desviacion = %s , sw_operativo = %s , total_turnos = %s , disponibilidad_historico = %s "
        sql += "where id_equipo=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def delete(meData):
    try:
        valores = []
        valores.append(meData.id_equipo)
        sql = "delete equipo "
        sql += "where id_equipo=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        #print(sql)
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."