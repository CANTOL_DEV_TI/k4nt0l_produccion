from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql

def oee_IndicadoresMotivo_get(id_config_oee, cod_subarea):
    sql = f" SELECT cod_motivo_indicador, indicador_motivo FROM oee_indicadores_motivo "
    sql += f" WHERE sw_estado in ('1')"
    sql += f" AND id_config_oee=%s " %(id_config_oee)
    sql += f" AND cod_subarea=%s " %(cod_subarea)
    sql += f" order by indicador_motivo"
    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta
