from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql


def turno_nuevo():
    try:
        sql = f"SELECT CASE WHEN max(cod_turno) is null THEN 1 ELSE max(cod_turno) + 1 END AS nuevo_id FROM turno_cab"
        me_conexion = conexion_mssql()
        resultados = me_conexion.consultas_sgc(sql, ())
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def turno_all():
    try:
        sql = f"SELECT * FROM turno_cab WHERE sw_estado in ('1','0')"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."


def turno_activo():
    try:
        sql = f"SELECT * FROM turno_cab WHERE sw_estado in ('1')"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."


def turno_det(cod_turno):
    try:
        sql = f" SELECT * FROM turno_det WHERE cod_turno = (%s) ORDER BY num_dia"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, (cod_turno,))
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."


def turno_insert(dt_turno):
    try:
        # obtener id de plan de ventas
        c_id = "0"
        c_json_res = turno_nuevo()
        for row in c_json_res:
            c_id = str(row["nuevo_id"])

        if c_id == 0:
            return HTTPException(status_code=401, detail="Error en generar ID")

        # CABECERA
        # añadir valores del diccionario cabecera
        lista_cab = []
        lista_cab.append(c_id)
        lista_cab.append(dt_turno.nom_turno)
        lista_cab.append(dt_turno.glosa)
        lista_cab.append(dt_turno.sw_estado)
        lista_cab.append(dt_turno.cod_usuario)

        # ejecutar cabecera
        v_sql_cab = f"INSERT INTO turno_cab (cod_turno,nom_turno,glosa,sw_estado,cod_usuario) VALUES (%s, %s, %s, %s, %s)"
        me_conexion = conexion_mssql()

        # DETALLE
        lista_det = []
        # v_sql = """INSERT INTO PLAN_VENTA_DET (id_plan,fecha_doc,cod_articulo,cantidad) VALUES (%s, %s, %s, %s)"""
        v_sql_det = f"INSERT INTO turno_det (cod_turno,num_dia,nom_dia,cant_hora_lab) VALUES (%s, %s, %s, %s)"
        for row in dt_turno.detalle:
            rowD = row.dict()
            lista_det.append(
                (c_id, rowD["num_dia"], rowD["nom_dia"], rowD["cant_hora_lab"])
            )
        # resultados = me_conexion.ejecutar_funciones_multi(v_sql, lista_det)}
        if len(lista_det) > 0:
            resultados = me_conexion.ejecutar_funciones_CabDet(
                v_sql_cab, tuple(lista_cab), v_sql_det, lista_det
            )
        else:
            resultados = HTTPException(status_code=401, detail="No hay datos")

        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def turno_update(dt_turno):
    try:
        # CABECERA
        # añadir valores del diccionario cabecera
        lista_cab = []
        lista_cab.append(dt_turno.nom_turno)
        lista_cab.append(dt_turno.glosa)
        lista_cab.append(dt_turno.sw_estado)
        lista_cab.append(dt_turno.cod_usuario)
        lista_cab.append(dt_turno.cod_turno)

        # ejecutar cabecera
        v_sql_cab = (
            f"DELETE FROM turno_det WHERE cod_turno = " + str(dt_turno.cod_turno) + ";"
        )
        v_sql_cab += f"UPDATE turno_cab SET nom_turno = %s, glosa = %s, sw_estado = %s, cod_usuario = %s WHERE cod_turno = %s"

        # DETALLE
        lista_det = []
        v_sql_det = f"INSERT INTO turno_det (cod_turno,num_dia,nom_dia,cant_hora_lab) VALUES (%s, %s, %s, %s)"
        for row in dt_turno.detalle:
            rowD = row.dict()
            lista_det.append(
                (
                    str(dt_turno.cod_turno),
                    rowD["num_dia"],
                    rowD["nom_dia"],
                    rowD["cant_hora_lab"],
                )
            )
        # resultados = me_conexion.ejecutar_funciones_multi(v_sql, lista_det)}
        me_conexion = conexion_mssql()
        if len(lista_det) > 0:
            resultados = me_conexion.ejecutar_funciones_CabDet(
                v_sql_cab, tuple(lista_cab), v_sql_det, lista_det
            )
        else:
            resultados = HTTPException(status_code=401, detail="No hay datos")
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def turno_delete(cod_turno):
    try:
        sql = f"UPDATE turno_cab SET sw_estado = '9' WHERE cod_turno = %s"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.ejecutar_funciones(sql, (cod_turno,))
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."
