from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql


def produccion_pendiente_all():
    try:
        sql = f"SELECT pp.*, af.articulo FROM produccion_pendiente pp"
        sql += f" LEFT OUTER JOIN articulo_familia af ON af.cod_articulo = pp.cod_articulo"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())
        return resultadosConsulta
    except Exception as err:
            return f"Error en la operación : {err}."


def produccion_pendiente_insert(dt_produccion_pendiente):
    try:
        # CABECERA
        # añadir valores del diccionario cabecera
        lista_cab = []

        # ejecutar cabecera
        v_sql_cab = f"DELETE FROM produccion_pendiente; "

        # DETALLE
        lista_det = []
        v_sql_det = (
            f"INSERT INTO produccion_pendiente (cod_articulo,cantidad) VALUES (%s, %s)"
        )
        for row in dt_produccion_pendiente.detalle:
            rowD = row.dict()
            lista_det.append((rowD["cod_articulo"], rowD["cantidad"]))
        # resultados = me_conexion.ejecutar_funciones_multi(v_sql, lista_det)}
        me_conexion = conexion_mssql()
        if len(lista_det) > 0:
            resultados = me_conexion.ejecutar_funciones_CabDet(
                v_sql_cab, (), v_sql_det, lista_det
            )
        else:
            resultados = HTTPException(status_code=401, detail="No hay datos")

        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def produccion_pendiente_delete(cod_articulo):
    try:
        sql = f"DELETE FROM produccion_pendiente WHERE cod_articulo = %s"
        me_conexion = conexion_mssql()
        resultados = me_conexion.ejecutar_funciones(sql, (cod_articulo,))
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."
