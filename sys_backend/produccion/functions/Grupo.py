from conexiones.conexion_mmsql import conexion_mssql


class Grupo:
    def __init__(self):
        self.__meConexion = conexion_mssql()

    def get_grupo_sap_activo(self):
        sql = """ select  """
        sql += """ id_grupo, """
        sql += """ grupo """
        sql += """ from articulo_grupo_sap """
        sql += """ where sw_estado = '1' """
        sql += """ order by grupo """

        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ("",))
        return resultadosConsulta

    def get_grupo_pt_pp(self):
        sql = """ select  """
        sql += """ id_grupo, """
        sql += """ grupo """
        sql += """ from articulo_grupo_sap """
        sql += """ where sw_estado = '1' """
        sql += """ and id_grupo in (102,110) """
        sql += """ order by grupo """

        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ("",))
        return resultadosConsulta
