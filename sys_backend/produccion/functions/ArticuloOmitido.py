from fastapi import HTTPException, status
from conexiones.conexion_mmsql import conexion_mssql


def articuloOmitido_nuevo():
    try:
        sql = f"SELECT CASE WHEN max(id_plan_omitido) is null THEN 1 ELSE max(id_plan_omitido) + 1 END AS nuevo_id FROM articulo_omitido_cab"
        me_conexion = conexion_mssql()
        resultados = me_conexion.consultas_sgc(sql, ())
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def articuloOmitido_all():
    try:
        sql = f"SELECT id_plan_omitido, CONVERT(DATE, fecha_emision, 103) AS fecha_emision, glosa, cod_usuario, sw_estado FROM articulo_omitido_cab WHERE sw_estado in ('1','0') ORDER BY id_plan_omitido DESC"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."


def articuloOmitido_activo():
    try:
        sql = f"SELECT * FROM articulo_omitido_cab WHERE sw_estado in ('1')"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."


def articuloOmitido_det(id_plan_omitido):
    try:
        sql = f" SELECT * FROM articulo_omitido_det WHERE id_plan_omitido = (%s)"
        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, (id_plan_omitido,))
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."


def articuloOmitido_insert(dt_omitido):
    try:
        # obtener id de plan de ventas
        c_id = "0"
        c_json_res = articuloOmitido_nuevo()
        for row in c_json_res:
            c_id = str(row["nuevo_id"])

        if c_id == 0:
            return HTTPException(status_code=401, detail="Error en generar ID")

        # CABECERA
        # añadir valores del diccionario cabecera
        lista_cab = []
        lista_cab.append(c_id)
        lista_cab.append(dt_omitido.fecha_emision)
        lista_cab.append(dt_omitido.glosa)
        lista_cab.append(dt_omitido.sw_estado)
        lista_cab.append(dt_omitido.cod_usuario)

        # CABECERA
        v_sql_cab = f"INSERT INTO articulo_omitido_cab (id_plan_omitido,fecha_emision,glosa,sw_estado,cod_usuario) VALUES (%s, %s, %s, %s, %s)"
        me_conexion = conexion_mssql()

        # DETALLE
        lista_det = []
        v_sql_det = f"INSERT INTO articulo_omitido_det (id_plan_omitido,cod_articulo,nom_articulo,cod_grupo) VALUES (%s, %s, %s, %s)"
        for row in dt_omitido.detalle:
            rowD = row.dict()
            lista_det.append(
                (c_id, rowD["cod_articulo"], rowD["nom_articulo"], rowD["cod_grupo"])
            )
        # resultados = me_conexion.ejecutar_funciones_multi(v_sql, lista_det)}
        if len(lista_det) > 0:
            resultados = me_conexion.ejecutar_funciones_CabDet(
                v_sql_cab, tuple(lista_cab), v_sql_det, lista_det
            )
        else:
            resultados = HTTPException(status_code=401, detail="No hay datos")

        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def articuloOmitido_update(dt_omitido):
    try:
        # CABECERA
        # añadir valores del diccionario cabecera
        lista_cab = []
        lista_cab.append(dt_omitido.fecha_emision)
        lista_cab.append(dt_omitido.glosa)
        lista_cab.append(dt_omitido.sw_estado)
        lista_cab.append(dt_omitido.cod_usuario)
        lista_cab.append(dt_omitido.id_plan_omitido)

        # ejecutar cabecera
        v_sql_cab = (
            f"DELETE FROM articulo_omitido_det WHERE id_plan_omitido = "
            + str(dt_omitido.id_plan_omitido)
            + ";"
        )
        v_sql_cab += f"UPDATE articulo_omitido_cab SET fecha_emision = %s, glosa = %s, sw_estado = %s, cod_usuario = %s WHERE id_plan_omitido = %s"

        # DETALLE
        lista_det = []
        v_sql_det = f"INSERT INTO articulo_omitido_det (id_plan_omitido,cod_articulo,nom_articulo,cod_grupo) VALUES (%s, %s, %s, %s)"
        for row in dt_omitido.detalle:
            rowD = row.dict()
            lista_det.append(
                (
                    str(dt_omitido.id_plan_omitido),
                    rowD["cod_articulo"],
                    rowD["nom_articulo"],
                    rowD["cod_grupo"],
                )
            )
        # resultados = me_conexion.ejecutar_funciones_multi(v_sql, lista_det)}
        me_conexion = conexion_mssql()
        if len(lista_det) > 0:
            resultados = me_conexion.ejecutar_funciones_CabDet(
                v_sql_cab, tuple(lista_cab), v_sql_det, lista_det
            )
        else:
            resultados = HTTPException(status_code=401, detail="No hay datos")
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def articuloOmitido_delete(id_plan_omitido):
    try:
        # ejecutar procedimiento
        me_conexion = conexion_mssql()
        resultados = me_conexion.ejecutar_store_crud(
            "PRODUCION_ARTICULO_OMITIDO_DELETE", (id_plan_omitido,)
        )
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."
