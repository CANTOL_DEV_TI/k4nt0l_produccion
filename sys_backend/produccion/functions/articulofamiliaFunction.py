from conexiones.conexion_mmsql import conexion_mssql


def get_all(cod_formulado, cod_grupo, cod_area):
    try:
        sql = f" select"
        sql += f" af.*,"
        sql += f" fam.nom_familia as familia,"
        sql += f" sba.nom_subarea as subarea,"
        sql += f" grp.grupo,"
        sql += f" (case when tipo_formulado = 1 then 'FORMULADO' ELSE 'NO FORMULADO' END) as formulado_desc"
        sql += f" from articulo_familia af"
        sql += f" left outer join familia fam on fam.id_familia = af.id_familia"
        sql += f" left outer join sub_area sba on sba.cod_subarea = af.cod_subarea"
        sql += f" left outer join articulo_grupo_sap grp on grp.id_grupo = af.id_grupo"
        sql += f" where af.sw_estado in ('1','0')"
        if cod_formulado != "0":
            sql += f" AND af.tipo_formulado = '" + cod_formulado + "'"
        if cod_grupo != "0":
            sql += f" AND af.id_grupo = '" + cod_grupo + "'"
        if cod_area != "0":
            sql += f" AND af.cod_subarea = '" + cod_area + "'"
        sql += f" ORDER BY af.articulo"

        me_conexion = conexion_mssql()
        resultados = me_conexion.consultas_sgc(sql, ())
        return resultados
    except Exception as err:
        return f"Error en la operación : {err}."


def articuloFamilia_activo(cod_formulado, cod_grupo, cod_area):
    try:
        sql = f" SELECT cod_articulo, articulo FROM articulo_familia af"
        sql += f" WHERE sw_estado in ('1')"
        if cod_formulado != "0":
            sql += f" AND af.tipo_formulado = '" + cod_formulado + "'"
        if cod_grupo != "0":
            sql += f" AND af.id_grupo = '" + cod_grupo + "'"
        if cod_area != "0":
            sql += f" AND af.cod_subarea = '" + cod_area + "'"
        sql += f" ORDER BY af.articulo"

        me_conexion = conexion_mssql()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ())
        return resultadosConsulta
    except Exception as err:
        return f"Error en la operación : {err}."


def insert(meData):
    try:
        valores = []
        valores.append(meData.cod_articulo)
        valores.append(meData.id_familia)
        valores.append(meData.articulo)
        valores.append(meData.abc)
        valores.append(meData.cant_lote_min)
        valores.append(meData.cant_und_hora)
        valores.append(meData.cob_minimo_dia)
        valores.append(meData.cob_ideal_dia)
        valores.append(meData.cob_maximo_dia)
        valores.append(meData.cpdp)
        valores.append(meData.sw_estado)
        valores.append(meData.cod_subarea)
        valores.append(meData.id_grupo)
        valores.append(meData.sw_maneja_plano)
        valores.append(meData.tipo_formulado)
        valores.append(meData.id_maquina)
        valores.append(meData.estacion)
        valores.append(meData.frecuencia_uso)

        sql = "insert into articulo_familia(cod_articulo,id_familia,articulo,abc,cant_lote_min,cant_und_hora,cob_minimo_dia,cob_ideal_dia,cob_maximo_dia,cpdp,sw_estado,cod_subarea, id_grupo, sw_maneja_plano, tipo_formulado, id_maquina, estacion, frecuencia_uso) "
        sql += "values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def update(meData):
    try:
        valores = []

        valores.append(meData.id_familia)
        valores.append(meData.articulo)
        valores.append(meData.abc)
        valores.append(meData.cant_lote_min)
        valores.append(meData.cant_und_hora)
        valores.append(meData.cob_minimo_dia)
        valores.append(meData.cob_ideal_dia)
        valores.append(meData.cob_maximo_dia)
        valores.append(meData.cpdp)
        valores.append(meData.cod_subarea)
        valores.append(meData.id_grupo)
        valores.append(meData.sw_maneja_plano)
        valores.append(meData.tipo_formulado)
        valores.append(meData.id_maquina)
        valores.append(meData.estacion)
        valores.append(meData.frecuencia_uso)
        valores.append(meData.cod_articulo)
        sql = "update articulo_familia set id_familia=%s, articulo=%s, abc=%s, cant_lote_min=%s, cant_und_hora=%s, cob_minimo_dia=%s, cob_ideal_dia=%s, cob_maximo_dia=%s, cpdp=%s, cod_subarea=%s , id_grupo=%s, sw_maneja_plano=%s, tipo_formulado=%s, id_maquina=%s, estacion=%s, frecuencia_uso=%s"
        sql += "where cod_articulo=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def delete(meData):
    try:
        valores = []
        valores.append(meData.cod_articulo)
        sql = "update articulo_familia set sw_estado='9' "
        sql += "where cod_articulo=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."
