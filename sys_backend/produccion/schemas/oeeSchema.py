from typing import List, Optional
from pydantic import BaseModel
from datetime import datetime
from decimal import Decimal


class OeeIndicadoresSchema(BaseModel):
    item_indicador_oeee: Optional[int]
    id_oee: Optional[int]
    tipo_registro: Optional[str]
    causa: Optional[str]
    motivo_causa: Optional[str]
    umu: Optional[str]
    cantidad: Optional[Decimal]
    cod_motivo_indicador: Optional[int]
    id_config_oee: Optional[int]

class OeeDetSchema(BaseModel):
    id_oee: Optional[int]
    item_oee: Optional[int]
    cod_tipo_registro: Optional[int]
    cod_causa: Optional[int]
    cod_motivo_causa: Optional[int]
    tipo_registro: Optional[str]
    causa: Optional[str]
    motivo_causa: Optional[str]
    umu: Optional[str]
    cantidad_prod_buena: Optional[Decimal]
    detalle: Optional[str]
    num_vale: Optional[str]

    cantidad_prod_mala: Optional[Decimal]
    cantidad_total: Optional[Decimal]
    tiempo_min: Optional[Decimal]

    hora_ini: Optional[str]
    hora_fin: Optional[str]


    id_tpoperacion: Optional[str]
    cod_articulo: Optional[str]
    articulo: Optional[str]
    cod_empleado: Optional[str]
    num_of_sap: Optional[str]
    num_paso: Optional[str]
    std_und_hora: Optional[Decimal]

    rendimiento_porcentaje: Optional[Decimal]




class OeeCabSchema(BaseModel):
    id_oee: Optional[int]
    fecha_doc: Optional[datetime]
    cod_subarea: Optional[int]
    # num_of_sap: Optional[str]
    # cod_empleado: Optional[str]
    # cod_articulo: Optional[str]
    # articulo: Optional[str]
    # num_paso: Optional[int]
    id_equipo: Optional[int]
    # id_tpoperacion: Optional[int]
    cod_turno: Optional[int]
    hora_ini: Optional[str]
    hora_fin: Optional[str]
    # std_und_hora: Optional[Decimal]
    hora_planificado: Optional[Decimal]

    horas_operativo: Optional[Decimal]
    produccion_total: Optional[Decimal]

    horas_perdidas: Optional[Decimal]

    hora_pausas: Optional[Decimal]
    hora_paradas: Optional[Decimal]

    sw_estado: Optional[str]
    detalle: List[OeeDetSchema]
    indicadores: List[OeeIndicadoresSchema]


class OeeStopMachine_Det(BaseModel):
    item: Optional[int]
    id_oee: Optional[str]
    tiempo_min: Optional[Decimal]


    id_equipo: Optional[int]
    cod_causa: Optional[int]
    cod_motivo_causa: Optional[int]




class OeeStopMachine_Cab(BaseModel):
    id_oee: Optional[str]
    fecha_doc: Optional[datetime]
    cod_empleado: Optional[str]
    cod_turno: Optional[int]
    cod_subarea: Optional[int]
    detalle: List[OeeStopMachine_Det]
