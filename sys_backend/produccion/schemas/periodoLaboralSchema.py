from typing import List, Optional
from pydantic import BaseModel
from decimal import Decimal

class PeriodoLaboralSchema(BaseModel):
    id_per_lab: Optional[int]
    ejercicio: Optional[int]
    periodo: Optional[int]
    cod_subarea: Optional[int]
    cod_turno: Optional[int]    
    total_dia_lab: Optional[Decimal]
    sw_estado: Optional[str]
    cod_usuario: Optional[str]


