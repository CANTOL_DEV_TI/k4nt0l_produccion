from typing import List, Optional
from pydantic import BaseModel
from datetime import datetime


class ArticuloOmitidoDetSchema(BaseModel):
    id_plan_omitido: Optional[int]
    cod_articulo: Optional[str]
    nom_articulo: Optional[str]
    cod_grupo: Optional[str]

class ArticuloOmitidoCabSchema(BaseModel):
    id_plan_omitido: Optional[int]
    fecha_emision: Optional[datetime]
    glosa: Optional[str]
    sw_estado: Optional[str]
    cod_usuario: Optional[str]
    detalle : List[ArticuloOmitidoDetSchema]


