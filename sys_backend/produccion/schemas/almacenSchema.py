from typing import Optional
from pydantic import BaseModel


class almacen_Schema(BaseModel):
    cod_almacen: Optional[str]
    nom_almacen: Optional[str]

    class Config:
        orm_mode = True