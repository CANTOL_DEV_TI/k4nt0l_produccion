from typing import List, Optional
from pydantic import BaseModel
from decimal import Decimal

class OeeTipoRegistroSchema(BaseModel):
    cod_tipo_registro: Optional[int]
    tipo_registro: Optional[str]
    umu: Optional[str]
    sw_estado: Optional[str]
    label_name: Optional[str]

    style_Principal: Optional[str]
    style_secundario: Optional[str]
    sw_default: Optional[str]


