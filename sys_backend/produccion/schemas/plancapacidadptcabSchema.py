from typing import Optional
from pydantic import BaseModel
from datetime import datetime

class plan_capacidad_pt_cab_Schema(BaseModel):
    id_plan_capacidad : Optional[int]
    fecha_emision : Optional[datetime]
    id_plan : Optional[int]
    id_stock : Optional[int]
    id_plan_omitido : Optional[int]
    id_tarea : Optional[int]
    sw_estado: Optional[str]
    
    class Config:
        orm_mode = True
