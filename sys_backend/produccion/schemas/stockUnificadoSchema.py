from typing import Optional
from pydantic import BaseModel
from decimal import Decimal
from datetime import date


class stockUnificadoCab_Schema(BaseModel):
    id_stock : int
    fecha_emision: date

    class Config:
        orm_mode = True


class stockUnificadoDet_Schema(BaseModel):
    id_item: int
    id_stock: int
    cod_articulo: str
    tecnopress_stock: Decimal
    tecnopress_comprometido: Decimal
    distrimax_stock: Decimal
    distrimax_comprometido: Decimal
    distrimax_pedidosborrador: Decimal
    stock_inicial: Decimal

    class Config:
        orm_mode = True


