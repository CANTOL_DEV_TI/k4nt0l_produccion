from typing import Optional
from pydantic import BaseModel
from decimal import Decimal


class area_Schema(BaseModel):
    cod_area: Optional[int]
    nom_area: Optional[str]

    class Config:
        orm_mode = True


