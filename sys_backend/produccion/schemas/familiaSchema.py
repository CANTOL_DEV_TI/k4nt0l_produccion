from typing import Optional
from pydantic import BaseModel
from decimal import Decimal


class familia_Schema(BaseModel):
    id_familia: Optional[int]
    nom_familia: Optional[str]
    cod_usuario: Optional[str]
    sw_estado: Optional[str]
    
    class Config:
        orm_mode = True


