from typing import Optional
from pydantic import BaseModel
from datetime import datetime


class tipoOperacion_Schema(BaseModel):
    id_tpoperacion: Optional[int]
    tipo_operacion: Optional[str]    
    
    class Config:
        orm_mode = True