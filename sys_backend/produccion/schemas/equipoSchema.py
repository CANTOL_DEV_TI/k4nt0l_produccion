from typing import Optional
from pydantic import BaseModel
from decimal import Decimal

class Equipo_Schema(BaseModel):
    id_equipo : Optional[int]
    cod_subarea : Optional[int]
    id_tpequipo : Optional[int]
    cod_equipo : Optional[str]
    equipo : Optional[str]
    factor_desviacion : Optional[Decimal]
    oferta_horas_x_dia : Optional[Decimal]
    sw_operativo : Optional[int]
    total_turnos : Optional[int]
    disponibilidad_historico : Optional[Decimal]
    
    class Config:
        orm_mode = True