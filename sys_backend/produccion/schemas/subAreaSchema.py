from typing import Optional
from pydantic import BaseModel
from decimal import Decimal


class subArea_Schema(BaseModel):
    cod_subarea : int
    cod_area : int
    nom_subarea : str

    class Config:
        orm_mode = True


