from typing import List,Optional
from pydantic import BaseModel
from decimal import Decimal

class config_Proceso_Det_Schema(BaseModel):
    id_fmp_det : Optional[int]
    id_fmproceso : Optional[int]
    id_equipo : Optional[int]
    id_tpoperacion : Optional[int]
    num_paso : Optional[str]
    num_prioridad :Optional[int]
    desc_proceso : Optional[str]
    std_und_hora : Optional[Decimal]    

##    class Config:
##        orm_mode = True

class config_Proceso_Cab_Schema(BaseModel):
    id_fmproceso: Optional[int]
    cod_articulo: Optional[str]
    articulo : Optional[str]
    cant_lote_min : Optional[int]
    detalles : Optional[List[config_Proceso_Det_Schema]]

    class Config:
        orm_mode = True

