from typing import List, Optional
from pydantic import BaseModel
from datetime import datetime


class formaPagoSchema(BaseModel):
    moneda: Optional[str]
    tipo: Optional[str]
    monto: Optional[float]

class cuotasSchema(BaseModel):
    moneda: Optional[str]    
    monto: Optional[float]
    fechaPago: Optional[datetime]

class addressSchema(BaseModel):
    direccion: Optional[str]
    provincia: Optional[str]
    departamento: Optional[str]
    distrito: Optional[str]
    ubigueo: Optional[str]

class guiasSchema(BaseModel):
    tipoDoc: Optional[str]
    nroDoc: Optional[str]

class clientSchema(BaseModel):
    tipoDoc: Optional[str]
    numDoc: Optional[str]
    rznSocial: Optional[str]
    address : dict[addressSchema]

class companySchema(BaseModel):
    ruc: Optional[str]
    razonSocial: Optional[str]
    nombreComercial: Optional[str]
    address : dict[addressSchema]

class DocumentoDetSchema(BaseModel):
    codProducto: Optional[str]
    unidad: Optional[str]
    descripcion: Optional[str]
    cantidad: Optional[float]
    mtoValorUnitario: Optional[float]
    mtoValorVenta: Optional[float]
    mtoBaseIgv: Optional[float]
    porcentajeIgv: Optional[float]
    igv: Optional[float]
    tipAfeIgv: Optional[float]
    totalImpuestos: Optional[float]
    mtoPrecioUnitario: Optional[float]

class legendsSchema(BaseModel):
    code: Optional[str]
    value: Optional[str]

class DocumentoCabSchema(BaseModel):
    ublVersion: Optional[str]
    fecVencimiento: Optional[datetime]
    tipoOperacion: Optional[str]
    tipoDoc: Optional[str]
    serie: Optional[str]
    correlativo: Optional[str]
    fechaEmision: Optional[datetime]
    formaPago : dict[formaPagoSchema]
    cuotas : List[cuotasSchema]
    tipoMoneda: Optional[str]
    guias : List[guiasSchema]
    client : dict[clientSchema]
    company : dict[companySchema]
    mtoOperGravadas: Optional[float]
    mtoIGV: Optional[float]
    valorVenta: Optional[float]
    totalImpuestos: Optional[float]
    subTotal: Optional[float]
    mtoImpVenta: Optional[float]
    details : List[DocumentoDetSchema]
    legends : List[legendsSchema]