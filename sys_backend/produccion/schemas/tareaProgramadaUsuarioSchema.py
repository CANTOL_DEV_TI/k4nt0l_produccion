from typing import Optional 
from pydantic import BaseModel 
from datetime import datetime 
 
 
class tareaprogramadausuario_Schema(BaseModel): 
    id_tarea: Optional[int] 
    fecha_busqueda : Optional[str] 
    fecha_tarea: Optional[str] 
    cod_usuario: Optional[str]     
    ejercicio_tarea : Optional[int]
    periodo_tarea : Optional[int]
    tipo_tarea : Optional[str]
     
    class Config: 
        orm_mode = True