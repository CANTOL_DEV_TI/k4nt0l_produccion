from typing import List, Optional
from pydantic import BaseModel
from datetime import datetime
from decimal import Decimal


class PreventivoDetSchema(BaseModel):
    id_preventivo: Optional[int]
    id_equipo: Optional[str]
    cantidad: Optional[Decimal]


class PreventivoCabSchema(BaseModel):
    id_preventivo: Optional[int]
    fecha_plan: Optional[datetime]
    glosa: Optional[str]
    detalle: List[PreventivoDetSchema]
