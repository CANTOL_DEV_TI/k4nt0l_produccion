from typing import List, Optional
from pydantic import BaseModel
from decimal import Decimal

class OeeCausaSchema(BaseModel):
    cod_causa: Optional[int]
    cod_tipo_registro: Optional[int]
    causa: Optional[str]
    sw_estado: Optional[str]

    tiempo_default: Optional[Decimal]


