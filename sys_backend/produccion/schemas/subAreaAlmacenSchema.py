from typing import Optional
from pydantic import BaseModel
from decimal import Decimal


class subAreaAlmacen_Schema(BaseModel):
    cod_subarea_almacen: int
    cod_almacen: str
    cod_subarea : int
    nom_subarea_almacen : str

    class Config:
        orm_mode = True


