from typing import List, Optional
from pydantic import BaseModel


class ProduccionPendienteDetSchema(BaseModel):
    cod_articulo: Optional[str]
    cantidad: Optional[int]

class ProduccionPendienteSchema(BaseModel):
    cod_articulo: Optional[str]
    detalle : List[ProduccionPendienteDetSchema]


