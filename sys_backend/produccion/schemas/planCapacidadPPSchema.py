from typing import List, Optional
from pydantic import BaseModel
from decimal import Decimal


class PlanCapacidadPPDetMaquinaSchema(BaseModel):
    id_plan_capacidad_pp: Optional[str]
    id_item_pp: Optional[str]
    num_paso: Optional[str]
    id_equipo: Optional[str]
    cant_programado: Optional[Decimal]
    hora_programado: Optional[Decimal]

class PlanCapacidadPPUpdate(BaseModel):
    detalle : List[PlanCapacidadPPDetMaquinaSchema]

class PlanCapacidadPPArticulo(BaseModel):
    id_plan_capacidad_pp: Optional[str]
    cod_articulo: Optional[str]
    cantidad: Optional[str]
    sw_modo: Optional[str] # S: SUMA, R: RESTA
