from typing import List, Optional
from pydantic import BaseModel
from decimal import Decimal

class OeeMotivoCausaSchema(BaseModel):
    cod_motivo_causa: Optional[int]
    cod_subarea: Optional[int]
    cod_causa: Optional[int]
    motivo_causa: Optional[str]
    sw_estado: Optional[str]


