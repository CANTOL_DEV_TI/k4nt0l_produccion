from typing import List, Optional
from pydantic import BaseModel, Field
from datetime import datetime
from decimal import Decimal


class OrdenFabricacionSapSchema(BaseModel):
    id_plan_capacidad_pp: Optional[str]
    id_item_pp: Optional[str]
    num_paso: Optional[str]
    id_equipo: Optional[str]
    cant_programado: Optional[Decimal]
    hora_programado: Optional[Decimal]


class consultaPrograma(BaseModel):
    fecha: Optional[str]
    id_grupo: Optional[str] = Field(default="0")
    cod_subarea: Optional[str] = Field(default="0")
    cod_propietario: Optional[str] = Field(default="0")
    # Considerar los que no tengan plan, Si: ['1'], No: ['0']
    sw_todo: Optional[str] = Field(default="0")
