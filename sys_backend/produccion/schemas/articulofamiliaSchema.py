from typing import Optional
from pydantic import BaseModel
from decimal import Decimal


class articulofamilia_Schema(BaseModel):
    cod_articulo : Optional[str]
    id_familia : Optional[int]
    articulo : Optional[str]
    abc : Optional[str]
    cant_lote_min : Optional[Decimal]
    cant_und_hora : Optional[Decimal]                
    cob_minimo_dia : Optional[Decimal]
    cob_ideal_dia : Optional[Decimal]
    cob_maximo_dia : Optional[Decimal]
    cpdp : Optional[Decimal]
    sw_estado : Optional[str]
    cod_subarea : Optional[int]
    id_grupo : Optional[int]
    sw_maneja_plano : Optional[str]
    tipo_formulado : Optional[str]
    id_maquina : Optional[str]
    estacion : Optional[str]
    frecuencia_uso : Optional[int]
    
    class Config:
        orm_mode = True
