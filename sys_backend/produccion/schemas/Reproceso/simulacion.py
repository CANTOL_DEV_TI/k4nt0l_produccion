from pydantic import BaseModel
from typing import List

class ProductionOrderLine(BaseModel):
    ItemNo: str
    PlannedQuantity: float
    ProductionOrderIssueType: str | None = 'im_Manual'
    Warehouse: str
    DistributionRule: str | None = '20001'
    DistributionRule2: str
    ItemType: str
    StartDate: str
    EndDate: str
    ItemName: str
    baseCost: float

class Simulacion(BaseModel):
    ItemCode: str
    ProdName: str
    Type: str | None = 'P'
    PlannedQty: float
    SimDate: str
    PostDate: str | None = None
    DueDate: str | None = None
    StartDate: str | None = None
    Warehouse: str
    OriginType: str | None = 'M'
    PrcCode: str
    Usuario_Codigo_SIM: str
    Usuario_Codigo_VAL: str | None = None
    cod_motivo_causa: int | None = None
    isChecked: bool | None = False
    ProductionOrderLines: List[ProductionOrderLine]