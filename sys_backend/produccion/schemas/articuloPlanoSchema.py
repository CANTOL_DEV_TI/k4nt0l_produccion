from typing import Optional
from pydantic import BaseModel
from decimal import Decimal


class ArticuloPlanoSchema(BaseModel):
    id_art_plano: Optional[int]    
    cod_articulo: Optional[str]
    id_version: Optional[int]
    articulo: Optional[str]
    cod_plano: Optional[str]
    sw_est_plano: Optional[str]
    glosa: Optional[str]
    sw_estado: Optional[str]
    cod_usuario: Optional[str]
    tipo:Optional[str]    

