from typing import Optional
from pydantic import BaseModel
from decimal import Decimal


class config_Proceso_Det_Schema(BaseModel):
    id_fmp_det : Optional[int]
    id_fmproceso : Optional[int]
    id_equipo : Optional[int]
    num_paso : Optional[str]
    num_prioridad :Optional[int]
    desc_proceso : Optional[str]
    std_und_hora : Optional[Decimal]
    id_tpoperacion : Optional[int]

    class Config:
        orm_mode = True
