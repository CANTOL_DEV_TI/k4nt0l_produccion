from typing import List, Optional
from pydantic import BaseModel


class TurnoDetSchema(BaseModel):
    cod_turno: Optional[int]
    num_dia: Optional[int]
    nom_dia: Optional[str]
    cant_hora_lab: Optional[float]

class TurnoCabSchema(BaseModel):
    cod_turno: Optional[int]
    nom_turno: Optional[str]
    glosa: Optional[str]
    sw_estado: Optional[str]
    cod_usuario: Optional[str]
    detalle : List[TurnoDetSchema]


