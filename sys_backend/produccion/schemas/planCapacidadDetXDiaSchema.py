from typing import Optional
from pydantic import BaseModel
from decimal import Decimal

class planCapacidadDetXDia_Schema(BaseModel):
    id_plan_capacidad: int
    cod_articulo: str
    nro_dia: int
    cant: Decimal


