from typing import Optional
from pydantic import BaseModel
from decimal import Decimal

class empleado_Schema(BaseModel):
    cod_empleado: str
    cod_subarea: Optional[int]
    nombre: Optional[str]
    sw_estado: Optional[str]

    class Config:
        orm_mode = True


