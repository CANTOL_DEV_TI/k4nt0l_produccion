from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.functions import programacionPPFunction

ruta = APIRouter(prefix="/programacionpi", tags=["programacion PI"])

@ruta.post('/{id_planCapacidadPT}')
async def grabar(id_planCapacidadPT: str):
    #print("Comienzo de savePro")
    return programacionPPFunction.saveProgramacion(id_planCapacidadPT)

@ruta.delete('/{id_planCapacidadPP}')
async def eliminar(id_planCapacidadPP: str):
    programacionPPFunction.deleteProgramacion(id_planCapacidadPP)


