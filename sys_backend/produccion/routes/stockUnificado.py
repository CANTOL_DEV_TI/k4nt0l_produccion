from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.models.areaModel import area_Model
from produccion.schemas.areaSchema import area_Schema
from produccion.functions import stockUnificadoFunction


ruta = APIRouter(
    prefix="/stockunificado",tags=["Stock Unificado"]
)



@ruta.post('/{fecha}')
async def grabar(fecha: str):
    return stockUnificadoFunction.insert(fecha)
