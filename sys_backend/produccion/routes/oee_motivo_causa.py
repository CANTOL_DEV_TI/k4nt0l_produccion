from fastapi import APIRouter, Depends, status 
from produccion.schemas.oeeMotivoCausaSchema import OeeMotivoCausaSchema 
from produccion.functions import oeeMotivoCausaFunction 
from typing import Union 
 
ruta = APIRouter(prefix="/oee_motivo_causa", tags=["OEE Motivo Causa"]) 
 
 
@ruta.get("/") 
def oee_motivo_causa_all(cod_subarea: Union[str, None] = "0", cod_causa: Union[str, None] = "0",sw_estado: Union[str, None] = "0"): 
    data = oeeMotivoCausaFunction.oee_motivo_causa_all(cod_subarea,cod_causa, sw_estado) 
    return data


@ruta.get("/get/")
def oee_motivo_causa_all_Get(cod_causa: Union[str, None] = "0", sw_estado: Union[str, None] = "0"):
    data = oeeMotivoCausaFunction.oee_motivo_causa_all_Get(cod_causa, sw_estado)
    return data


@ruta.post("/", status_code=status.HTTP_201_CREATED) 
def oee_motivo_causa_insert(dt_oee_motivo_causa: OeeMotivoCausaSchema): 
    data = oeeMotivoCausaFunction.oee_motivo_causa_insert(dt_oee_motivo_causa) 
    # return {"respuesta": "Insertado satisfactoriamente!!"} 
    return data 
 
 
@ruta.put("/", status_code=status.HTTP_200_OK) 
def oee_motivo_causa_update(dt_oee_motivo_causa: OeeMotivoCausaSchema): 
    oeeMotivoCausaFunction.oee_motivo_causa_update(dt_oee_motivo_causa) 
    return {"respuesta": "Modificado satisfactoriamente!!"} 
 
 
@ruta.delete("/", status_code=status.HTTP_200_OK)
def oee_motivo_causa_delete(dt_oee_motivo_causa: OeeMotivoCausaSchema):
    oeeMotivoCausaFunction.oee_motivo_causa_delete(dt_oee_motivo_causa)
    return {"respuesta": "Eliminado satisfactoriamente!!"} 
