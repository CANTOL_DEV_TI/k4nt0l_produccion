from fastapi import APIRouter, Depends, status
from produccion.functions import oeeIndicadoresFunction


ruta = APIRouter(prefix="/oee_indicadores", tags=["OEE Indicadores"])

@ruta.get("/")
def oee_IndicadoresAll():
    data = oeeIndicadoresFunction.oee_Indicadores_All()
    return data
