from fastapi import APIRouter, Depends, status
from produccion.schemas.periodoLaboralSchema import PeriodoLaboralSchema
from produccion.functions import Periodo_laboral
from typing import Union

ruta = APIRouter(prefix="/periodo_laboral", tags=["Periodo Laboral"])


@ruta.get("/")
def periodo_laboral_all(
    cod_turno: Union[str, None] = "0", cod_subarea: Union[str, None] = "0"
):
    data = Periodo_laboral.periodo_laboral_all(cod_turno, cod_subarea)
    return data


@ruta.get("/activo")
def periodo_laboral_activo():
    data = Periodo_laboral.periodo_laboral_activo()
    return data


@ruta.get("/turno")
def periodo_laboral_turno(cod_subarea: Union[str, None] = "0"):
    data = Periodo_laboral.periodo_laboral_turno(cod_subarea)
    return data


@ruta.post("/", status_code=status.HTTP_201_CREATED)
def periodo_laboral_insert(dt_periodo_laboral: PeriodoLaboralSchema):
    data = Periodo_laboral.periodo_laboral_insert(dt_periodo_laboral)
    # return {"respuesta": "Insertado satisfactoriamente!!"}
    return data


@ruta.post("/sub_area", status_code=status.HTTP_201_CREATED)
def periodo_laboral_insert_sub_area(dt_periodo_laboral: PeriodoLaboralSchema):
    data = Periodo_laboral.periodo_laboral_insert_sub_area(dt_periodo_laboral)
    # return {"respuesta": "Insertado satisfactoriamente!!"}
    return data


@ruta.put("/", status_code=status.HTTP_200_OK)
def periodo_laboral_update(dt_periodo_laboral: PeriodoLaboralSchema):
    Periodo_laboral.periodo_laboral_update(dt_periodo_laboral)
    return {"respuesta": "Modificado satisfactoriamente!!"}


@ruta.delete("/{id_per_lab}", status_code=status.HTTP_200_OK)
def periodo_laboral_delete(id_per_lab: Union[str, None] = "0"):
    Periodo_laboral.periodo_laboral_delete(id_per_lab)
    return {"respuesta": "Eliminado satisfactoriamente!!"}
