from fastapi import APIRouter, Depends, status
from produccion.schemas.articuloOmitidoSchema import ArticuloOmitidoCabSchema
from produccion.functions import ArticuloOmitido
from typing import Union

ruta = APIRouter(prefix="/articulo_omitido", tags=["Articulo Omitido"])


@ruta.get("/")
def articuloOmitido_all():
    data = ArticuloOmitido.articuloOmitido_all()
    return data


@ruta.get("/activo")
def articuloOmitido_activo():
    data = ArticuloOmitido.articuloOmitido_activo()
    return data


@ruta.get("/articulo_familia")
def articuloFamilia_activo():
    data = ArticuloOmitido.articuloFamilia_activo()
    return data


@ruta.get("/det/{id_plan_omitido}")
def articuloOmitido_det(id_plan_omitido: Union[str, None] = "0"):
    data = ArticuloOmitido.articuloOmitido_det(id_plan_omitido)
    return data


@ruta.post("/", status_code=status.HTTP_201_CREATED)
def articuloOmitido_insert(dt_articuloOmitido: ArticuloOmitidoCabSchema):
    responser = ArticuloOmitido.articuloOmitido_insert(dt_articuloOmitido)
    return responser


@ruta.put("/", status_code=status.HTTP_200_OK)
def articuloOmitido_update(dt_articuloOmitido: ArticuloOmitidoCabSchema):
    ArticuloOmitido.articuloOmitido_update(dt_articuloOmitido)
    return {"respuesta": "Modificado satisfactoriamente!!"}


@ruta.delete("/{id_plan_omitido}", status_code=status.HTTP_200_OK)
def articuloOmitido_delete(id_plan_omitido: Union[str, None] = "0"):
    ArticuloOmitido.articuloOmitido_delete(id_plan_omitido)
    return {"respuesta": "Eliminado satisfactoriamente!!"}
