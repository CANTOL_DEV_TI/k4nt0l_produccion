from fastapi import APIRouter, Depends, status
from typing import Union

from conexiones.conexion_sql import get_db
from produccion.models.articulofamiliaModel import articulofamilia_Model
from produccion.schemas.articulofamiliaSchema import articulofamilia_Schema
from produccion.functions import articulofamiliaFunction

##from middlewares.verify_token_route import VerificaTokenRoute

ruta = APIRouter(
    prefix="/articulofamilia",
    tags=["Articulo Familia"],  ##,route_class=VerificaTokenRoute
)


@ruta.get("/all", status_code=status.HTTP_200_OK)
def articuloFamilia_all(
    cod_formulado: Union[str, None] = "0",
    cod_grupo: Union[str, None] = "0",
    cod_subarea: Union[str, None] = "0",
):
    data = articulofamiliaFunction.get_all(cod_formulado, cod_grupo, cod_subarea)
    return data

@ruta.get("/articulofamilia_activo") 
def articuloFamilia_activo( 
    cod_formulado: Union[str, None] = "0", 
    cod_grupo: Union[str, None] = "0", 
    cod_subarea: Union[str, None] = "0", 
): 
    data = articulofamiliaFunction.articuloFamilia_activo( 
        cod_formulado, cod_grupo, cod_subarea 
    ) 
    return data 


@ruta.post("/")
async def grabar(info: articulofamilia_Schema):
    # resultado = await info.json()
    return articulofamiliaFunction.insert(info)


@ruta.put("/")
async def actualizar(info: articulofamilia_Schema):
    return articulofamiliaFunction.update(info)


@ruta.delete("/")
async def eliminar(info: articulofamilia_Schema):
    return articulofamiliaFunction.delete(info)
