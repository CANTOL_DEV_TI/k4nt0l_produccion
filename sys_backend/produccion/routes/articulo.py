from fastapi import APIRouter, Depends, status
from typing import Union

# from produccion.functions.Articulo import Articulo
from produccion.functions import ArticuloPlano
from produccion.functions.Articulo import Articulo

# from conexiones.conexion_sql import get_db

ruta = APIRouter()

"""
@ruta.get("/get_articulo_plano_sap")
def articuloPlano_all(
    cod_formulado: Union[str, None] = "0",
    cod_grupo: Union[str, None] = "0",
    cod_area: Union[str, None] = "0",
):
    # meConsulta = Articulo()
    # articulo
    # dt_articulos = meConsulta.get_articulo_plano_sap(cod_area)

    # planos
    dt_articulos_planos = ArticuloPlano.articuloPlano_all(
        cod_formulado, cod_grupo, cod_area
    )

    # unir
    
    for fila_art in dt_articulos:
        # cabeceras para añadir
        c_cod_plano = "cod_plano"
        c_id_version = "id_version"
        c_sw_est_plano = "sw_est_plano"

        # buscar datos en la otra lista/diccionario
        for fila_plano in dt_planos:
            # dic_plano = vars(fila_plano)  # convertir un modelo a diccionario
            dic_plano = fila_plano  # convertir un modelo a diccionario
            if dic_plano["cod_articulo"] == fila_art["COD_ARTICULO"]:
                if c_cod_plano not in fila_art:
                    fila_art.update(
                        {
                            c_cod_plano: dic_plano["cod_plano"],
                            c_id_version: dic_plano["id_version"],
                            c_sw_est_plano: dic_plano["sw_est_plano"],
                        }
                    )
    

    return dt_articulos_planos
"""


@ruta.get("/get_articulo_sap")
def get_articulo_sap():
    me_articulo = Articulo()
    dt_articulos_planos = me_articulo.get_articulo_sap()

    return dt_articulos_planos

@ruta.get("/get_articulo_sap_filtro/{pfiltro}")
def get_articulo_sap_filtro(pfiltro):
    me_articulo = Articulo()
    dt_articulos_planos = me_articulo.get_articulo_sap_filtro(pfiltro)

    return dt_articulos_planos