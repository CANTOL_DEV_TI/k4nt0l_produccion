from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from produccion.functions.Class.planCapacidadPPCalcular import planCapacidadPPCalcular

ruta = APIRouter(
    prefix="/programacion_pp", tags=["Plan Programacion PP"]
)


@ruta.post('/{id}')
async def save(id: str):
    meProgramacion = planCapacidadPPCalcular(id)
    resultado = False
    if meProgramacion.calcular():
        resultado = True
    return {'estado': resultado}


