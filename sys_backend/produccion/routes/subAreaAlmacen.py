from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.models.subAreaAlmacenModel import subAreaAlmacen_Model
from produccion.schemas.subAreaAlmacenSchema import subAreaAlmacen_Schema
from produccion.functions import subAreaAlmacenFunction
from typing import Union

ruta = APIRouter(prefix="/subareaAlmacen", tags=["Sub Area Almacen"])

@ruta.get("/{txtFind}", status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = (
        db.query(subAreaAlmacen_Model).filter(subAreaAlmacen_Model.nom_subarea_almacen.contains(txtFind.strip())).all()
    )
    return data

@ruta.post('/')
async def grabar(info: subAreaAlmacen_Schema):
    # resultado = await info.json()
    return subAreaAlmacenFunction.insert(info)

@ruta.put("/")
async def actualizar(info: subAreaAlmacen_Schema):
    return subAreaAlmacenFunction.update(info)

@ruta.delete("/")
async def eliminar(info: subAreaAlmacen_Schema):
    return subAreaAlmacenFunction.delete(info)




