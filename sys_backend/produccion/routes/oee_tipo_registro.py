from fastapi import APIRouter, Depends, status
from produccion.schemas.oeeTipoRegistroSchema import OeeTipoRegistroSchema
from produccion.functions import oeeTipoRegistroFunction
from typing import Union

ruta = APIRouter(prefix="/oee_tipo_registro", tags=["OEE Tipo Registro"])


@ruta.get("/mostrar/{txt}")
def oee_tipo_registro_all(txt: Union[str, None] = ""):
    data = oeeTipoRegistroFunction.get_oee_TipoRegistro(txt.strip())
    return data


@ruta.get("/")
def oee_tipo_registro_all(cod_tipo_registro: Union[str, None] = "0", sw_estado: Union[str, None] = "0", cod_subarea: Union[str, None] = "0"):
    data = oeeTipoRegistroFunction.oee_tipo_registro_all(cod_tipo_registro, sw_estado, cod_subarea)
    return data


@ruta.post("/", status_code=status.HTTP_201_CREATED)
def oee_tipo_registro_insert(dt_oee_tipo_registro: OeeTipoRegistroSchema):
    data = oeeTipoRegistroFunction.oee_tipo_registro_insert(dt_oee_tipo_registro)
    # return {"respuesta": "Insertado satisfactoriamente!!"}
    return data


@ruta.put("/", status_code=status.HTTP_200_OK)
def oee_tipo_registro_update(dt_oee_tipo_registro: OeeTipoRegistroSchema):
    oeeTipoRegistroFunction.oee_tipo_registro_update(dt_oee_tipo_registro)
    return {"respuesta": "Modificado satisfactoriamente!!"}


@ruta.delete("/{id_per_lab}", status_code=status.HTTP_200_OK)
def oee_tipo_registro_delete(id_per_lab: Union[str, None] = "0"):
    oeeTipoRegistroFunction.oee_tipo_registro_delete(id_per_lab)
    return {"respuesta": "Eliminado satisfactoriamente!!"}
