from fastapi import APIRouter, Depends, status
from produccion.functions import oeeIndicadoresMotivoFunction



ruta = APIRouter(prefix="/oee_indicadores_motivo", tags=["OEE Indicadores Motivo"])

@ruta.get("/{id_config_oee}/{cod_subarea}")
def oee_IndicadoresMotivoAll(id_config_oee: int, cod_subarea: int):
    data = oeeIndicadoresMotivoFunction.oee_IndicadoresMotivo_get(id_config_oee, cod_subarea)
    return data
