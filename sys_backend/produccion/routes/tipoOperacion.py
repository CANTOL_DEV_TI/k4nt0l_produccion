from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.models.tipoOperacionModel import tipoOperacion_Model
from produccion.schemas.tipoOperacionSchema import tipoOperacion_Schema
from produccion.functions import tipoOperacionFunction
##from middlewares.verify_token_route import VerificaTokenRoute

ruta = APIRouter(
    prefix="/tipoOperacion",tags=["Tipo Operacion"]##,route_class=VerificaTokenRoute
)

@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = db.query(tipoOperacion_Model).filter(tipoOperacion_Model.tipo_operacion.contains(txtFind.strip())).all()
    return data

@ruta.get('/ListaTipoOperacion/', status_code=status.HTTP_200_OK)
def ListaTipoOperacion(db: Session = Depends(get_db)):
    data = db.query(tipoOperacion_Model).all()
    return data

@ruta.post('/')
async def grabar(info: tipoOperacion_Schema):
    # resultado = await info.json()
    return tipoOperacionFunction.insert(info)

@ruta.put('/')
async def actualizar(info: tipoOperacion_Schema):
    return tipoOperacionFunction.update(info)

@ruta.delete('/')
async def eliminar(info: tipoOperacion_Schema):
    return tipoOperacionFunction.delete(info)