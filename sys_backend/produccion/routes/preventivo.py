from fastapi import APIRouter, Depends, status
from produccion.schemas.preventivoSchema import PreventivoCabSchema
from produccion.functions import preventivoFunction
from typing import Union

ruta = APIRouter(prefix="/preventivo", tags=["Preventivo Equipo"])


@ruta.get("/")
def preventivo_all():
    data = preventivoFunction.preventivo_all()
    return data


@ruta.get("/det/{id_preventivo}")
def preventivo_det(id_preventivo: Union[str, None] = "0"):
    data = preventivoFunction.preventivo_det(id_preventivo)
    return data


@ruta.post("/", status_code=status.HTTP_201_CREATED)
def preventivo_insert(dt_preventivo: PreventivoCabSchema):
    responser = preventivoFunction.preventivo_insert(dt_preventivo)
    return responser


@ruta.put("/", status_code=status.HTTP_200_OK)
def preventivo_update(dt_preventivo: PreventivoCabSchema):
    preventivoFunction.preventivo_update(dt_preventivo)
    return {"respuesta": "Modificado satisfactoriamente!!"}


@ruta.delete("/{id_preventivo}", status_code=status.HTTP_200_OK)
def preventivo_delete(id_preventivo: Union[str, None] = "0"):
    preventivoFunction.preventivo_delete(id_preventivo)
    return {"respuesta": "Eliminado satisfactoriamente!!"}
