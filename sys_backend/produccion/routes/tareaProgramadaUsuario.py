from fastapi import APIRouter, Depends, status, UploadFile, File, Request 
from fastapi.responses import FileResponse 
from sqlalchemy.orm.session import Session 
 
from conexiones.conexion_sql import get_db 
from produccion.models.tareaProgramadaUsuarioModel import tareaprogramadausuario_Model 
from produccion.schemas.tareaProgramadaUsuarioSchema import tareaprogramadausuario_Schema 
from produccion.functions import tareaProgramadaUsuarioFunction
 
 
ruta = APIRouter( 
    prefix="/tareaprogramadausuario",tags=["Tarea Programado Usuario"] 
) 
 
@ruta.get('/{txtInput}', status_code=status.HTTP_200_OK) 
def showFilter(txtInput: str, db: Session = Depends(get_db)): 
    xEjercicio = txtInput[:4]
    xPeriodo = str(int(txtInput[4:6]))

    data = db.query(tareaprogramadausuario_Model).filter(tareaprogramadausuario_Model.ejercicio_tarea.contains(xEjercicio.strip()),tareaprogramadausuario_Model.periodo_tarea.contains(xPeriodo.strip())).all()     
    return data 
 
@ruta.post('/') 
async def grabar(info: tareaprogramadausuario_Schema): 
    # resultado = await info.json() 
    return tareaProgramadaUsuarioFunction.insert(info) 
 
@ruta.put('/') 
async def actualizar(info: tareaprogramadausuario_Schema): 
    return tareaProgramadaUsuarioFunction.update(info) 
 
@ruta.delete('/') 
async def eliminar(info: tareaprogramadausuario_Schema): 
    return tareaProgramadaUsuarioFunction.delete(info) 