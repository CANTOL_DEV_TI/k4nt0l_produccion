from fastapi import APIRouter
from produccion.functions import subAreaFunction

ruta = APIRouter( )

@ruta.get("/get_area_responsable_sap")
def get_area_responsable():
    resultado = subAreaFunction.get_subarea_operacion_plano("1")
    return resultado