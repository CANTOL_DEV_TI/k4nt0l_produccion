from fastapi import APIRouter
from typing import Union
from produccion.functions import planCapacidadPPDatosFunction
from produccion.schemas.planCapacidadPPSchema import (
    PlanCapacidadPPUpdate,
    PlanCapacidadPPArticulo,
)


ruta = APIRouter(prefix="/plan_capacidad_pp", tags=["Plan de Capacidad de PP"])


@ruta.get("/consolidado")
async def planCapacidadPPDet_consolidado(
    id_plan_capacidad: Union[str, None] = "0",
    cod_subarea: Union[str, None] = "0",
    abc: Union[str, None] = "0",
):
    return planCapacidadPPDatosFunction.planCapacidadPPDet_consolidado(
        id_plan_capacidad, cod_subarea, abc
    )


@ruta.get("/equipo")
async def planCapacidadPPDet_equipo(
    id_plan_capacidad: Union[str, None] = "0", cod_subarea: Union[str, None] = "0"
):
    return planCapacidadPPDatosFunction.planCapacidadPPDet_equipo(
        id_plan_capacidad, cod_subarea
    )


@ruta.get("/equipo_articulo")
async def planCapacidadPPDet_equipo_articulo(
    id_plan_capacidad: Union[str, None] = "0", id_equipo: Union[str, None] = "0"
):
    return planCapacidadPPDatosFunction.planCapacidadPPDet_equipo_articulo(
        id_plan_capacidad, id_equipo
    )


@ruta.get("/articulo")
async def planCapacidadPPDet_articulo(
    id_plan_capacidad: Union[str, None] = "0", cod_articulo: Union[str, None] = "0"
):
    return planCapacidadPPDatosFunction.planCapacidadPPDet_articulo(
        id_plan_capacidad, cod_articulo
    )


@ruta.post("/update")
async def planCapacidadPPDet_update(dt_plan_pp: PlanCapacidadPPUpdate):
    return planCapacidadPPDatosFunction.planCapacidadPPDet_update(dt_plan_pp)


@ruta.post("/boom_nivel")
async def planCapacidadPPDet_boom_nivel_update(dt_articulo: PlanCapacidadPPArticulo):
    data = planCapacidadPPDatosFunction.planCapacidadPPDet_nivel_update(dt_articulo)
    return data


@ruta.get("/get_nivel_articulo")
async def planCapacidadPPDet_nivel_articulo(
    id_plan_capacidad: Union[str, None] = "0", cod_articulo: Union[str, None] = "0"
):
    data = planCapacidadPPDatosFunction.planCapacidadPPDet_nivel_articulo(
        id_plan_capacidad, cod_articulo
    )
    return data
