from fastapi import APIRouter, Depends, status
from produccion.schemas.oeeSchema import OeeCabSchema, OeeStopMachine_Cab
from produccion.functions import oeeFunction
from typing import Union

from produccion.functions import oeeIndicadoresFunction




ruta = APIRouter(prefix="/oee", tags=["Eficiencia Productiva OEE"])


@ruta.get("/excel/{cod_subarea}/{fecha_ini}/{fecha_fin}", status_code=status.HTTP_201_CREATED)
def oee_export_excel(cod_subarea: str, fecha_ini: str, fecha_fin: str):
    datos = oeeIndicadoresFunction.export_Indicadores(cod_subarea, fecha_ini, fecha_fin)
    return datos

@ruta.get("/finOrdenFabricacion/{cod_subarea}/{nroOrdenFab}")
def oee_Find_OrdenFabricacion(cod_subarea: str, nroOrdenFab: str):
    datos = oeeFunction.oee_OrdenFabricacion(cod_subarea, nroOrdenFab)
    return datos


@ruta.get("/findEmpleado/{cod_empleado}/{cod_subarea}")
def oee_empleados(cod_empleado: str, cod_subarea: str):
    datos = oeeFunction.oee_empleados(cod_empleado, cod_subarea)
    return datos

# @ruta.get("/{fecha}/{cod_empleado}")
# def oee_all(fecha: str, cod_empleado: str):
#     data = oeeFunction.oee_all(fecha, cod_empleado)
#     return data


@ruta.get("/activo")
def oee_activo():
    data = oeeFunction.oee_activo()
    return data


@ruta.get("/det/{id_plan_omitido}")
def oee_det(id_plan_omitido: Union[str, None] = "0"):
    data = oeeFunction.oee_det(id_plan_omitido)
    return data


@ruta.post("/", status_code=status.HTTP_201_CREATED)
def oee_insert(dt_oee: OeeCabSchema):
    # responser = oeeFunction.oee_insert(dt_oee)
    responser = oeeFunction.oee_insertRegistro(dt_oee)
    print(responser)
    return responser


@ruta.put("/", status_code=status.HTTP_200_OK)
def oee_update(dt_oee: OeeCabSchema):
    responser = oeeFunction.oee_updateRegistro(dt_oee)
    return responser
    # oeeFunction.oee_update(dt_oee)
    # return {"respuesta": "Modificado satisfactoriamente!!"}


@ruta.delete("/{id_plan_omitido}", status_code=status.HTTP_200_OK)
def oee_delete(id_plan_omitido: Union[str, None] = "0"):
    oeeFunction.oee_delete(id_plan_omitido)
    return {"respuesta": "Eliminado satisfactoriamente!!"}



@ruta.get("/findcab/{fecha}/{tipo_filtro}/{txtFind}", status_code=status.HTTP_200_OK)
def oee_findCab(fecha: str, tipo_filtro: str, txtFind: str):
    responser = oeeIndicadoresFunction.oee_Indicadores_get(fecha, tipo_filtro, txtFind)
    return responser

@ruta.get("/finddet/{id_oee}", status_code=status.HTTP_200_OK)
def oee_findDet(id_oee: int):
    responser = oeeIndicadoresFunction.oee_Indicadores_Det_Ind_get(id_oee)
    return responser


# @ruta.get("/findindicadores/{id_oee}", status_code=status.HTTP_200_OK)
# def oee_findIndicadores(id_oee: int):
#     responser = oeeIndicadoresFunction.oee_Indicadores_Indi_Get(id_oee)
#     return responser

@ruta.get("/find_pasos/{codigo}/{id_equipo}", status_code=status.HTTP_200_OK)
def oee_Find_NumPaso(codigo: str, id_equipo: int):
    responser = oeeFunction.oee_Get_NumPaso(codigo, id_equipo)
    return responser

@ruta.get("/find_maquinas/{codigo}/{nropaso}/{cod_tipo_proceso}", status_code=status.HTTP_200_OK)
def oee_Find_Maquina(codigo: str, nropaso: int, cod_tipo_proceso: int):
    responser = oeeFunction.oee_Get_Maquinas(codigo, nropaso, cod_tipo_proceso)
    return responser

@ruta.get("/find_onlymaquinas/{cod_subarea}", status_code=status.HTTP_200_OK)
def oee_FindOnly_Maquina(cod_subarea: int):
    responser = oeeFunction.oee_Get_OnlyMaquinas(cod_subarea)
    return responser

@ruta.get("/fin_causasdefault", status_code=status.HTTP_200_OK)
def oee_Find_CausasDefault():
    response = oeeFunction.oee_Get_CausaDefault()
    return response


@ruta.get("/lista_stop_machine/{dni_empleado}/{fecha}/{cod_subarea}/{cod_turno}", status_code=status.HTTP_200_OK)
def oee_FindStopMachine(dni_empleado: str, fecha: str, cod_subarea: str, cod_turno: str):
    response = oeeFunction.Get_StopMachine(dni_empleado, fecha, cod_subarea, cod_turno)
    return response


@ruta.post("/stop_machine/", status_code=status.HTTP_201_CREATED)
def oee_StopMachine(dt_oee: OeeStopMachine_Cab):
    print("@"*20)
    print(dt_oee)
    # responser = oeeFunction.oee_insert(dt_oee)
    responser = oeeFunction.Insert_StopMachine(dt_oee)
    print(responser)
    return responser


@ruta.put("/stop_machine/", status_code=status.HTTP_201_CREATED)
def oee_StopMachine(dt_oee: OeeStopMachine_Cab):
    print("@"*20)
    print(dt_oee)
    # responser = oeeFunction.oee_insert(dt_oee)
    responser = oeeFunction.Update_StopMachine(dt_oee)
    print(responser)
    return responser


@ruta.get("/stop_machine/subarea/{cod_usuario}", status_code=status.HTTP_201_CREATED)
def oee_StopMachine_SubArea(cod_usuario: str):
    responser = oeeFunction.Find_SubArea(cod_usuario)
    print(responser)
    return responser




