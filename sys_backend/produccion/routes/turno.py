from fastapi import APIRouter, Depends, status
from produccion.schemas.turnoSchema import TurnoCabSchema
from produccion.functions import Turno
from typing import Union

ruta = APIRouter(prefix="/turno", tags=["Turno Laboral"])


@ruta.get("/")
def turno_all():
    data = Turno.turno_all()
    return data


@ruta.get("/activo")
def turno_activo():
    data = Turno.turno_activo()
    return data


@ruta.get("/{cod_turno}")
def turno_det(cod_turno: Union[str, None] = "0"):
    data = Turno.turno_det(cod_turno)
    return data


@ruta.post("/", status_code=status.HTTP_201_CREATED)
def turno_insert(dt_turno: TurnoCabSchema):
    Turno.turno_insert(dt_turno)
    return {"respuesta": "Insertado satisfactoriamente!!"}


@ruta.put("/", status_code=status.HTTP_200_OK)
def turno_update(dt_turno: TurnoCabSchema):
    Turno.turno_update(dt_turno)
    return {"respuesta": "Modificado satisfactoriamente!!"}


@ruta.delete("/{cod_turno}", status_code=status.HTTP_200_OK)
def turno_delete(cod_turno: Union[str, None] = "0"):
    Turno.turno_delete(cod_turno)
    return {"respuesta": "Eliminado satisfactoriamente!!"}
