from fastapi import APIRouter
from typing import Union
from produccion.functions import planCapacidadPTDatosFunction
from produccion.functions.plancapacidadcabptFunction import listado_plan_capacidad_pt


ruta = APIRouter(
    prefix="/plan_capacidad_pt",  tags=["Plan de Capacidad de PT"]
)

@ruta.post('/')
async def plan_capacidad_pt_insert():
    return planCapacidadPTDatosFunction.insert()

@ruta.get('/consolidado')
async def planCapacidadPTDet_consolidado(id_plan_capacidad: Union[str, None] = "0", id_familia: Union[str, None] = "0", abc: Union[str, None] = "0"):
    return planCapacidadPTDatosFunction.planCapacidadPTDet_consolidado(id_plan_capacidad,id_familia,abc)

@ruta.get('/{pEjercicio}')
async def plancapacidadpt_listado(pEjercicio:str):
    return listado_plan_capacidad_pt(pEjercicio)