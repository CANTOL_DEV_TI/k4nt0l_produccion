from fastapi import APIRouter, Depends, status
from produccion.schemas.produccion_pendienteSchema import ProduccionPendienteSchema
from produccion.functions import Produccion_pendiente
from typing import Union

ruta = APIRouter(prefix="/produccion_pendiente", tags=["Produccion Pendiente"])


@ruta.get("/")
def produccion_pendiente_all():
    data = Produccion_pendiente.produccion_pendiente_all()
    return data

@ruta.post("/", status_code=status.HTTP_201_CREATED)
def produccion_pendiente_insert(dt_produccion_pendiente: ProduccionPendienteSchema):
    Produccion_pendiente.produccion_pendiente_insert(dt_produccion_pendiente)
    return {"respuesta": "Insertado satisfactoriamente!!"}


@ruta.delete("/{cod_articulo}", status_code=status.HTTP_200_OK)
def produccion_pendiente_delete(cod_articulo: Union[str, None] = ""):
    Produccion_pendiente.produccion_pendiente_delete(cod_articulo)
    return {"respuesta": "Eliminado satisfactoriamente!!"}