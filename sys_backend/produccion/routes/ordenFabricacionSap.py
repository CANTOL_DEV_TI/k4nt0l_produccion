from fastapi import APIRouter
from typing import Union
from produccion.functions import ordenFabricacionSapFunction
from produccion.schemas.ordenFabricacionSapSchema import consultaPrograma


ruta = APIRouter(prefix="/orden_fabricacion", tags=["Orden de Fabricación SAP"])


@ruta.post("/programa")
async def ordenFabricacionPrograma(dt_programa: consultaPrograma):
    return ordenFabricacionSapFunction.ordenFabricacionPrograma(dt_programa)

@ruta.get("/pendiente")
async def ordenFabricacionPrograma(cod_subarea: Union[str, None] = "0"):
    return ordenFabricacionSapFunction.ordenFabricacion_pendiente(cod_subarea)

@ruta.post("/costos_fabricacion")
async def ordenFabricacionCF(dt_programa: consultaPrograma):
    return ordenFabricacionSapFunction.getCostoFabricacion(dt_programa)