from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.functions import programacionPtFunction

ruta = APIRouter(prefix="/programacionpt", tags=["Programacion PT"])


@ruta.delete('/{id_planCapacidadPT}')
async def eliminar(id_planCapacidadPT: str):
    programacionPtFunction.deleteProgramacion(id_planCapacidadPT)


