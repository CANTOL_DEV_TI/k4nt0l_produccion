from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db


from produccion.models.empleadoModel import empleado_Model
from produccion.schemas.empleadoSchema import empleado_Schema
from produccion.functions import empleadoFunction


ruta = APIRouter(
    prefix="/empleados",tags=["Empleados OEE"]
)

@ruta.get("/{cod_subarea}/{txtFind}", status_code=status.HTTP_200_OK)
def showFilter(cod_subarea: int, txtFind:str, db: Session = Depends(get_db)):
    data = (db.query(empleado_Model).filter(empleado_Model.nombre.contains(txtFind.strip()), empleado_Model.cod_subarea==cod_subarea).all())
    return data


@ruta.post("/", status_code=status.HTTP_201_CREATED)
async def grabar(info: empleado_Schema):
    return empleadoFunction.insert(info)

@ruta.put("/")
async def actualizar(info: empleado_Schema):
    return empleadoFunction.update(info)


@ruta.delete("/")
async def eliminar(info: empleado_Schema):
    return empleadoFunction.delete(info)



