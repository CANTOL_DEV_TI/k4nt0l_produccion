from fastapi import APIRouter
from produccion.functions.Grupo import Grupo

ruta = APIRouter(
    prefix="/grupo_sap", tags=["Articulo Grupo SAP"]
)  ##,route_class=VerificaTokenRoute


@ruta.get("/activo")
def get_grupo_sap_activo():
    meConsulta = Grupo()
    resultado = meConsulta.get_grupo_sap_activo()
    return resultado

@ruta.get("/pt_pp")
def get_grupo_pt_pp():
    meConsulta = Grupo()
    resultado = meConsulta.get_grupo_pt_pp()
    return resultado