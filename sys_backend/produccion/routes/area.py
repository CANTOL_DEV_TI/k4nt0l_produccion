from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.models.areaModel import area_Model
from produccion.schemas.areaSchema import area_Schema
from produccion.functions import areaFunction


ruta = APIRouter(
    prefix="/area",tags=["Areas"]
)


@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = db.query(area_Model).filter(area_Model.nom_area.contains(txtFind.strip())).all()
    return data

@ruta.post('/')
async def grabar(info: area_Schema):
    # resultado = await info.json()
    return areaFunction.insert(info)

@ruta.put('/')
async def actualizar(info: area_Schema):
    return areaFunction.update(info)

@ruta.delete('/')
async def eliminar(info: area_Schema):
    return areaFunction.delete(info)
