from fastapi import APIRouter, Depends, status, UploadFile, File
from fastapi.responses import FileResponse
from produccion.schemas.articuloPlanoSchema import ArticuloPlanoSchema
from produccion.functions import ArticuloPlano
from conexiones.conexion_sql import get_db
from sqlalchemy.orm import Session
from typing import Union
import os

ruta = APIRouter(prefix="/articulo_plano", tags=["Artículo Plano"])


@ruta.get("/all", status_code=status.HTTP_200_OK)
def articuloPlano_all(
    cod_formulado: Union[str, None] = "0",
    cod_grupo: Union[str, None] = "0",
    cod_area: Union[str, None] = "0",
):
    # planos
    dt_articulos_planos = ArticuloPlano.articuloPlano_all(
        cod_formulado, cod_grupo, cod_area
    )
    return dt_articulos_planos


@ruta.get("/historial/{cod_articulo}/{tipo}", status_code=status.HTTP_200_OK)
def articuloPlano_historial(cod_articulo: str,tipo:str, db: Session = Depends(get_db)):
    data = ArticuloPlano.articuloPlano_cod_articulo(cod_articulo,tipo, db)
    return data


@ruta.get("/codigo_nuevo", status_code=status.HTTP_200_OK)
def articuloPlano_nuevo_codigo(tipo:str, cod_articulo: str):
    data = ArticuloPlano.articuloPlano_nuevo_codigo(tipo,cod_articulo)
    return data


@ruta.get("/plano/{nombre_plano}")
def get_file(nombre_plano: str):
    file_path = "../data_planos/" + nombre_plano
    if os.path.exists(file_path):
        return FileResponse(file_path)
    return {"message": nombre_plano + ": Plano no existe"}


"""
@router.get("/download/{name_file}")
def download_file(name_file: str):
    return FileResponse(getcwd() + "/" + name_file, media_type="application/octet-stream", filename=name_file)

"""


@ruta.post("/", status_code=status.HTTP_201_CREATED)
def insertar_articulo_plano(
    dt_articulo_plano: ArticuloPlanoSchema, db: Session = Depends(get_db)
):
    ArticuloPlano.articuloPlano_insert(dt_articulo_plano, db)
    return {"respuesta": "Plano creado satisfactoriamente!!"}


@ruta.post("/upload_plano", status_code=status.HTTP_201_CREATED)
def upload_articulo_plano(file: UploadFile = File(...), db: Session = Depends(get_db)):
    #
    # print(file.filename)
    # agregar plano a directorio
    # file_ext = file.filename.split(".").pop() # extraer la extension
    # file_name = dt_articulo_plano.cod_articulo + "_" + (dt_articulo_plano.id_version)
    # file_name = "PP2117000188"
    # file_path = f"data_planos/{file_name}.{file_ext}"

    file_path = f"../data_planos/{file.filename}"
    with open(file_path, "wb+") as f:
        content = file.file.read()
        f.write(content)  # grabar
        f.close()

    res = {
        "success": True,
        "file_path": file_path,
        "message": "File upload successfully",
    }
    return res


@ruta.put("/estado/{id_art_plano}", status_code=status.HTTP_200_OK)
def actualizar_articulo_plano(
    id_art_plano: int,
    dt_articulo_plano: ArticuloPlanoSchema,
    db: Session = Depends(get_db),
):
    res = ArticuloPlano.articuloPlano_update_estado(id_art_plano, dt_articulo_plano, db)
    return res


@ruta.delete("/{id_art_plano}", status_code=status.HTTP_200_OK)
def eliminar_articulo_plano(id_art_plano: int, db: Session = Depends(get_db)):
    res = ArticuloPlano.articuloPlano_delete(id_art_plano, db)
    return res

@ruta.get("/gen_version/{ptipo}/{particulo}", status_code=status.HTTP_200_OK)
def getversion(ptipo,particulo):
    return ArticuloPlano.GenerarIDVersion(ptipo,particulo)