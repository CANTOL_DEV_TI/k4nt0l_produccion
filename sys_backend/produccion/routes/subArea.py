from fastapi import APIRouter, Depends, status
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.models.subAreaModel import subArea_Model
from produccion.schemas.subAreaSchema import subArea_Schema
from produccion.functions import subAreaFunction
from typing import Union

ruta = APIRouter(prefix="/subarea", tags=["Sub Area"])


@ruta.get("/{txtFind}", status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = (
        db.query(subArea_Model)
        .filter(subArea_Model.nom_subarea.contains(txtFind.strip()))
        .all()
    )
    return data

@ruta.get("/cod_area/{cod_area}", status_code=status.HTTP_200_OK)
def get_subarea_operacion(cod_area: Union[str, None] = "0"):
    data = subAreaFunction.get_subarea_operacion(cod_area)
    return data

@ruta.get("/subarea_usuario/", status_code=status.HTTP_200_OK)
def get_subarea_operacion_usuario(cod_usuario: Union[str, None] = "0"):
    data = subAreaFunction.get_subarea_operacion_usuario(cod_usuario)
    return data

@ruta.get("/ListaSubAreas", status_code=status.HTTP_200_OK)
def listaSubAreas(db: Session = Depends(get_db)):
    data = (
        db.query(subArea_Model)        
        .all()
    )
    return data

@ruta.get("/propietario/", status_code=status.HTTP_200_OK)
def get_subarea_propietario():
    data = subAreaFunction.get_subarea_propietario()
    return data

@ruta.post("/")
async def grabar(info: subArea_Schema):
    # resultado = await info.json()
    return subAreaFunction.insert(info)


@ruta.put("/")
async def actualizar(info: subArea_Schema):
    return subAreaFunction.update(info)


@ruta.delete("/")
async def eliminar(info: subArea_Schema):
    return subAreaFunction.delete(info)
