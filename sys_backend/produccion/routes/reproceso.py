from fastapi import APIRouter
from .Reproceso import (
    area,
    almacen,
    usuario,
    materiales,
    simulacion
)

ruta = APIRouter(
    prefix="/reproceso",
)

ruta.include_router(area.ruta)
ruta.include_router(almacen.ruta)
ruta.include_router(usuario.ruta)
ruta.include_router(materiales.ruta)
ruta.include_router(simulacion.ruta)


