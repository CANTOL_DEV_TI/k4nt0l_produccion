from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.models.almacenModel import almacen_Model
from produccion.schemas.almacenSchema import almacen_Schema
from produccion.functions import almacenFunction

ruta = APIRouter(
    prefix="/almacen",tags=["Almacenes"]
)


@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = db.query(almacen_Model).filter(almacen_Model.nom_almacen.contains(txtFind.strip())).all()
    return data

@ruta.get('/NombreAlmacen/{txtFind}', status_code=status.HTTP_200_OK)
def nombreAlmacen(txtFind:str):
    return almacenFunction.NombreAlmacen(txtFind)

@ruta.post('/')
async def grabar(info: almacen_Schema):
    # resultado = await info.json()
    return almacenFunction.insert(info)

@ruta.put('/')
async def actualizar(info: almacen_Schema):
    return almacenFunction.update(info)

@ruta.delete('/')
async def eliminar(info: almacen_Schema):
    return almacenFunction.delete(info)


