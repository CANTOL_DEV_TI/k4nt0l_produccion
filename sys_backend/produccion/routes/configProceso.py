from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.models.configProcesoCabModel import config_Proceso_Cab_Model
from produccion.models.configProcesoDetModel import config_Proceso_Det_Model
from produccion.schemas.configProcesoCabSchema import config_Proceso_Cab_Schema
from produccion.schemas.configProcesoDetSchema import config_Proceso_Det_Schema
from produccion.functions import configProcesoFunction
from typing import Union

##from middlewares.verify_token_route import VerificaTokenRoute

ruta = APIRouter(
    prefix="/configproceso",
    tags=["Configuracion Proceso"],  ##,route_class=VerificaTokenRoute
)


@ruta.get("/{txtFind}", status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = (
        db.query(config_Proceso_Cab_Model)
        .filter(config_Proceso_Cab_Model.articulo.contains(txtFind.strip()))
        .all()
    )
    #print(data)
    return data


@ruta.get("/NombreItem/{txtFind}", status_code=status.HTTP_200_OK)
def nombreItem(txtFind: str):
    return configProcesoFunction.NombreArticulo(txtFind)


@ruta.get("/DetalleConfig/{txtID}", status_code=status.HTTP_200_OK)
def DetalleConfig(txtID: int, db: Session = Depends(get_db)):
    data = db.query(config_Proceso_Det_Model).filter_by(id_fmproceso=txtID).all()
    return data


@ruta.get("/det_articulo/")
def config_proceso_det_articulo(cod_articulo: Union[str, None] = "0"):
    data = configProcesoFunction.config_proceso_det_articulo(cod_articulo)
    return data


@ruta.post("/")
async def grabar(info: config_Proceso_Cab_Schema):
    return configProcesoFunction.insert(info)


@ruta.put("/")
async def actualizar(info: config_Proceso_Cab_Schema):
    return configProcesoFunction.update(info)


@ruta.delete("/")
async def eliminar(info: config_Proceso_Cab_Schema):
    return configProcesoFunction.delete(info)
