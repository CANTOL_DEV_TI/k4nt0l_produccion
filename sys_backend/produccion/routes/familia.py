from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.models.familiaModel import familia_Model
from produccion.schemas.familiaSchema import familia_Schema
from produccion.functions import familiaFunction
##from middlewares.verify_token_route import VerificaTokenRoute

ruta = APIRouter(
    prefix="/familia",tags=["Familia"]##,route_class=VerificaTokenRoute
)

@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = db.query(familia_Model).filter(familia_Model.nom_familia.contains(txtFind.strip())).all()
    return data

@ruta.get('/Activos/', status_code=status.HTTP_200_OK)
def showActivos(db: Session = Depends(get_db)):
    data = db.query(familia_Model).filter(familia_Model.sw_estado.contains("1")).all()
    return data

@ruta.post('/')
async def grabar(info: familia_Schema):
    # resultado = await info.json()
    return familiaFunction.insert(info)

@ruta.put('/')
async def actualizar(info: familia_Schema):
    return familiaFunction.update(info)

@ruta.delete('/')
async def eliminar(info: familia_Schema):
    return familiaFunction.delete(info)
