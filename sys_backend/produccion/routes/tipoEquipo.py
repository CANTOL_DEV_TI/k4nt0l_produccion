from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.models.tipoEquipoModel import tipoEquipo_Model
from produccion.schemas.tipoEquipoSchema import tipoEquipo_Schema
from produccion.functions import tipoEquipoFunction
##from middlewares.verify_token_route import VerificaTokenRoute

ruta = APIRouter(
    prefix="/tipoEquipo",tags=["Tipo Equipo"]##,route_class=VerificaTokenRoute
)

@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = db.query(tipoEquipo_Model).filter(tipoEquipo_Model.tipo_equipo.contains(txtFind.strip())).all()
    return data

@ruta.get('/ListaTipoEquipo/', status_code=status.HTTP_200_OK)
def ListaTipoEquipo(db: Session = Depends(get_db)):
    data = db.query(tipoEquipo_Model).all()
    return data

@ruta.post('/')
async def grabar(info: tipoEquipo_Schema):
    # resultado = await info.json()
    return tipoEquipoFunction.insert(info)

@ruta.put('/')
async def actualizar(info: tipoEquipo_Schema):
    return tipoEquipoFunction.update(info)

@ruta.delete('/')
async def eliminar(info: tipoEquipo_Schema):
    return tipoEquipoFunction.delete(info)