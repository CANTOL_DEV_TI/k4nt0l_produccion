from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from produccion.functions import programacionPtFunction
from produccion.schemas.planCapacidadDetXDiaSchema import planCapacidadDetXDia_Schema

ruta = APIRouter(
    prefix="/programacion_pt", tags=["Plan Programacion"]
)



@ruta.get('/{idplaneacion}/{txt}')
async def showFilter(idplaneacion: str, txt: str):
    return programacionPtFunction.getProgramacion(idplaneacion, txt)

@ruta.put('/')
async def update(info: planCapacidadDetXDia_Schema):
    return programacionPtFunction.updateProgramacion(info)


