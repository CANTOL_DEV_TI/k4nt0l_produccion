from fastapi import APIRouter

from . import (
    areaResponsable,
    grupo,
    articulo,
    articulo_plano,
    planCapacidadPT,
    planCapacidadPP,
)
from . import area, subArea
from . import familia, articulofamilia, tareaProgramadaUsuario


from . import stockUnificado
from . import turno, periodo_laboral, articuloOmitido, produccion_pendiente

from . import subAreaAlmacen

from . import planProgramacionPT

from . import tipoOperacion, tipoEquipo, equipo, almacen, configProceso

from . import preventivo

from . import programacionPP
from . import programacionPT

from . import ordenFabricacionSap

from . import oee_tipo_registro, oee_causa, oee_motivo_causa, oeeEficiencia, oee_Config_Indicadores, oee_Config_Indicadores_Motivo, empleado

from . import reproceso




ruta = APIRouter(
    prefix="/produccion",
)

# control de planos
ruta.include_router(areaResponsable.ruta)
ruta.include_router(grupo.ruta)
ruta.include_router(articulo.ruta)
ruta.include_router(articulo_plano.ruta)

# capacidad
ruta.include_router(area.ruta)
ruta.include_router(subArea.ruta)
ruta.include_router(turno.ruta)
ruta.include_router(periodo_laboral.ruta)
ruta.include_router(articuloOmitido.ruta)
ruta.include_router(produccion_pendiente.ruta)
ruta.include_router(tareaProgramadaUsuario.ruta)

# produccion
ruta.include_router(familia.ruta)
ruta.include_router(articulofamilia.ruta)
ruta.include_router(stockUnificado.ruta)
ruta.include_router(planCapacidadPT.ruta)
ruta.include_router(planCapacidadPP.ruta)

ruta.include_router(planProgramacionPT.ruta)
ruta.include_router(tipoOperacion.ruta)
ruta.include_router(tipoEquipo.ruta)
ruta.include_router(equipo.ruta)
ruta.include_router(almacen.ruta)
ruta.include_router(configProceso.ruta)

ruta.include_router(subAreaAlmacen.ruta)

ruta.include_router(preventivo.ruta)

ruta.include_router(programacionPP.ruta)
ruta.include_router(programacionPT.ruta)
ruta.include_router(ordenFabricacionSap.ruta)



ruta.include_router(oee_tipo_registro.ruta)
ruta.include_router(oee_causa.ruta)
ruta.include_router(oee_motivo_causa.ruta)
ruta.include_router(oeeEficiencia.ruta)
ruta.include_router(oee_Config_Indicadores.ruta)
ruta.include_router(oee_Config_Indicadores_Motivo.ruta)

ruta.include_router(empleado.ruta)

# produccion - simulador de reproceso
ruta.include_router(reproceso.ruta)
