from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from produccion.models.equipoModel import Equipo_Model
from produccion.schemas.equipoSchema import Equipo_Schema
from produccion.functions import equipoFunction

##from middlewares.verify_token_route import VerificaTokenRoute

ruta = APIRouter(prefix="/Equipo", tags=["Equipo"])  ##,route_class=VerificaTokenRoute


@ruta.get("/{txtFind}", status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = (
        db.query(Equipo_Model)
        .filter(Equipo_Model.equipo.contains(txtFind.strip()))
        .all()
    )
    return data


@ruta.get("/ListaEquipos/", status_code=status.HTTP_200_OK)
def showList(db: Session = Depends(get_db)):
    data = db.query(Equipo_Model).all()
    return data


@ruta.get("/buscar/", status_code=status.HTTP_200_OK)
def equipo_buscar():
    data = equipoFunction.equipo_buscar()
    return data


@ruta.post("/")
async def grabar(info: Equipo_Schema):
    # resultado = await info.json()
    return equipoFunction.insert(info)


@ruta.put("/")
async def actualizar(info: Equipo_Schema):
    return equipoFunction.update(info)


@ruta.delete("/")
async def eliminar(info: Equipo_Schema):
    return equipoFunction.delete(info)
