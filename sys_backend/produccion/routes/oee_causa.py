from fastapi import APIRouter, Depends, status
from produccion.schemas.oeeCausaSchema import OeeCausaSchema
from produccion.functions import oeeCausaFunction
from typing import Union

ruta = APIRouter(prefix="/oee_causa", tags=["OEE Causa"])



@ruta.get("/mostrar/{txt}")
def get_OeeCausa(txt: Union[str, None] = ""):
    data = oeeCausaFunction.get_Oee_Causa(txt)
    return data




@ruta.get("/")
def oee_causa_all(
    cod_tipo_registro: Union[str, None] = "0",
    cod_subarea: Union[str, None] = "0",
    sw_estado: Union[str, None] = "0",
):
    data = oeeCausaFunction.oee_causa_all(cod_tipo_registro, cod_subarea, sw_estado)
    return data


@ruta.post("/", status_code=status.HTTP_201_CREATED)
def oee_causa_insert(dt_oee_causa: OeeCausaSchema):
    data = oeeCausaFunction.oee_causa_insert(dt_oee_causa)
    # return {"respuesta": "Insertado satisfactoriamente!!"}
    return data


@ruta.put("/", status_code=status.HTTP_200_OK)
def oee_causa_update(dt_oee_causa: OeeCausaSchema):
    oeeCausaFunction.oee_causa_update(dt_oee_causa)
    return {"respuesta": "Modificado satisfactoriamente!!"}


@ruta.delete("/{id_per_lab}", status_code=status.HTTP_200_OK)
def oee_causa_delete(id_per_lab: Union[str, None] = "0"):
    oeeCausaFunction.oee_causa_delete(id_per_lab)
    return {"respuesta": "Eliminado satisfactoriamente!!"}
