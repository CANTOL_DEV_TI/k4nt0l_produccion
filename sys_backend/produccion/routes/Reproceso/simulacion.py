from fastapi import APIRouter, HTTPException
from produccion.schemas.Reproceso.simulacion import Simulacion
from produccion.schemas.Reproceso.validacion import Validacion
from conexiones.Reproceso.conexion_sql import dbConnection
from conexiones.Reproceso.conexion_sap import SLSConnection
from produccion.utils.functions import obj


ruta = APIRouter(
    prefix='/simulacion',
    tags=["Simulador de Reproceso"]
)

dbObj = dbConnection()
slsObj = SLSConnection()

@ruta.get("/")
async def get_simulations_by_userid(usuarioid: str = 'XXXX', dynamic_offset: int = 0, static_offset: int = 20):
    #usuario XXXX para obtener todo
    response = dbObj.get_whole_simulations(usuarioid, dynamic_offset, static_offset)
    try:
        format_response = list(map(format_response_,response))
        return format_response
    except Exception as error:
        print(f"An error ocurred: {error}")
        raise HTTPException(status_code=500)

def format_response_(x):
    return {
        "SimId": x[0],
        "ItemCode": x[1],
        "ProdName": x[2],
        "PlannedQty": x[3],
        "SimDate": x[4],
        "PostDate": x[5],
        "DueDate": x[6],
        "StartDate": x[7],
        "Warehouse": x[8],
        "Usuario_Nombre": x[9],
        "Motivo_causa": x[10],
        "isOk": x[11],
        "DocumentNumber": x[12],
        "initialCost": int(x[13]),
        "additionalCost": int(x[14]) if (x[14] is not None) else 0,
        "finalCost": int(x[13] + x[14]) if (x[13] is not None and x[14] is not None) else None,
        "percentage": int(100 * float(x[14])/float(x[13])) if (x[13] is not None and x[14] is not None) else 0,
}

@ruta.get("/total_by_userid")
async def get_total_number_simulations(usuarioid: str = ''):
    #usuario XXXX para obtener todo
    response = dbObj.get_total_number_simulations(usuarioid)
    try:
        return {
            "data": response
        }
    except Exception as error:
        print(f"An error ocurred: {error}")
        raise HTTPException(status_code=500)

@ruta.post("/")
async def post_simulation_on_Sap(simulacion: Simulacion):
    if (simulacion.isChecked):
        #AGREGA PRIMERO SAP
        status, SAPResponse = slsObj.Post_ProductionOrder(simulacion)
    elif(not(simulacion.isChecked)):
        status, SAPResponse = (200, obj({"DocumentNumber": "-1"}))
    
    if (status == 201 or status == 200):
        #AGREGA SEGUNDO EN SWC DATA BASE
        status_swc, SWC_ID_OBJ = dbObj.postSimulation(simulacion, str(SAPResponse.DocumentNumber), simulacion.isChecked)
        if (status_swc == 201):
            status_response =  201 if (status == 201 and status_swc==201) else (200 if (status != 201 and status_swc==201) else 500)
            raise HTTPException(status_code=status_response, detail= {
                    "DocumentNumber": SAPResponse.DocumentNumber,
                    "SWC_ID": SWC_ID_OBJ["SWC_ID"]
                })
        else:
            raise HTTPException(status_code=500, detail= {
                "DocumentNumber": "XXXXXXXX",
                "SWC_ID": "XXXXXXXX"
                })
    else:
        raise HTTPException(status_code=500, detail= {
        "DocumentNumber": "XXXXXXXX",
        "SWC_ID": "XXXXXXXX"
        })

        

@ruta.put("/validar")
async def validate_sim_by_user(aprobarCuerpo: Validacion):
    db_model, status = dbObj.get_database_model(aprobarCuerpo.id_cab, aprobarCuerpo.checkDate)
    if status:
        status, SAPResponse = slsObj.Post_ProductionOrder(db_model)
        if status == 201:
            ##abajo se actualiza la db del SWC
            response_set_validate_sim = dbObj.set_validate_sim(aprobarCuerpo.Usuario_Codigo_VAL, aprobarCuerpo.id_cab, SAPResponse.DocumentNumber, db_model.PostDate, db_model.StartDate, db_model.DueDate )
            if response_set_validate_sim:
                raise HTTPException(status_code=201, detail= {
                "DocumentNumber": SAPResponse.DocumentNumber
                })
            else:
                raise HTTPException(status_code=500, detail= {
                "DocumentNumber": "XXXXXXXX"
                })
        else:
            raise HTTPException(status_code=500, detail= {
                "DocumentNumber": "XXXXXXXX"
                })
    else:
        raise HTTPException(status_code=201, detail= {
                "DocumentNumber": "XXXXXXXX"
                })
    raise HTTPException(status_code=200, detail= {"message": "This method is not built yet"})
