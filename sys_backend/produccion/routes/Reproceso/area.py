from fastapi import APIRouter, Request, Response
from conexiones.Reproceso.conexion_sql import dbConnection
from conexiones.Reproceso.conexion_sap import sapConnection

ruta = APIRouter(
    prefix='/area', tags=["Simulador de Reproceso"]
)

dbobj = dbConnection()
sapobj = sapConnection()


@ruta.get("/")
async def get_areas():
    data = sapobj.get_areas()
    newData = list(map(do_format_sa, enumerate(data)))
    return {
        "data": newData
    }

def do_format_sa(data):
    index, value = data
    if value[1] == 'FORJA Y TALADROS':
        return {
        "id": index,
        "prcCode": value[0],
        "subareaName": value[1],
        "members": ['2000103', '2000104','2000113'],
        }
    else:
        return {
        "id": index,
        "prcCode": value[0],
        "subareaName": value[1],
        "members": [value[0]],
        }

@ruta.get("/usuario")
async def get_areas_by_user(iduser : str = ""):
    data = dbobj.get_areas_by_user(iduser)
    newData = list(map(do_format_sa_user, enumerate(data)))
    return {
        "data": newData
    }

def do_format_sa_user(data):
    index, value = data
    return {
        "id": index,
        "subarea": value[0]
    }     