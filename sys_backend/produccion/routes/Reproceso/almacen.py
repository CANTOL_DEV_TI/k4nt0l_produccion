from fastapi import APIRouter
from conexiones.Reproceso.conexion_sap import sapConnection

ruta = APIRouter(
    prefix='/almacen',
    tags=["Simulador de Reproceso"]
)
sapobj = sapConnection()


@ruta.get("/subarea/")
async def get_wareHouses_by_area(odSubArea: str = 'PINTURA'):
    odSubArea = 'CALIDAD' if 'CONTROL DE CALIDAD' in odSubArea else odSubArea
    data = sapobj.get_warehouse_by_subarea(odSubArea)
    newData = list(map(get_warehouse_subarea, enumerate(data)))
    return (
        {
        "data": newData
        }
    )

def get_warehouse_subarea(data):
    index, value = data
    return {
    "WareCode": value[0],
    "WareName": value[1].replace('ALMACEN DE','').strip() if 'ALMACEN DE' in value[1] else value[1].replace('ALMACEN','').strip(),
    }