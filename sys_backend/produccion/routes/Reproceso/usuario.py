from fastapi import APIRouter
from conexiones.Reproceso.conexion_sql import dbConnection

ruta = APIRouter(
    prefix='/usuario',
    tags=["Simulador de Reproceso"]
)

dbobj = dbConnection()

@ruta.get("/validarol")
async def validate_user_rol(usuarioid: str = 'XXXX', rol_id: int = 4):
    isOk = dbobj.validateRol(usuarioid,rol_id)
    returnedValue = (True if not(isOk is None) else False)
    return {
        "data": returnedValue,
}