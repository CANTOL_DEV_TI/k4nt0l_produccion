# -*- coding: utf-8 -*-
# !/usr/bin/env python3.6


# import configparser
import psycopg2


class conexion_Postgresql:

    def __init__(self, **kwargs):
        print(kwargs)
        # return

        conexion = "dbname='{}' user='{}' host={} password='{}'".format(kwargs['dbname'], kwargs['user'], kwargs['host'], kwargs['password'])
        print(conexion)
        self.con = psycopg2.connect(conexion)
        self.cur = self.con.cursor()
        print("tutu")

    def ejecutar_funciones(self, vSql, vValores):
        self.cur.execute(vSql, vValores)
        self.con.commit()
        resultado = self.cur.fetchall()[0][0]
        # self.cerrando()
        return resultado

    def consultas(self, vSql, vValores):
        if len(vValores) <= 0:
            self.cur.execute(vSql)
        if len(vValores) > 0:
            self.cur.execute(vSql, vValores)

        # ----------------------------------------------------------------------------
        resultado = self.cur.fetchall()
        # self.cerrando()
        return resultado

    def consultas_RPT(self, vSql):
        self.cur.execute(vSql)
        cabeceras = self.cur.description
        datos     = self.cur.fetchall()
        # self.cerrando()
        return {'cabeceras': cabeceras, 'datos': datos}


    def ejecutar_conReturnKey(self, vTabla, vCampoKey, vSql, vValores):
        self.cur.execute(vSql, vValores)
        self.con.commit()
        vKey = self.hallando_key(vTabla, vCampoKey)
        # self.cerrando()
        return vKey


    def hallando_key(self, vTabla, vCampoKey):
        # HALLANDO CODIGO COMPRA ASIGNADO AUTO
        vSql = "select currval(pg_get_serial_sequence('" + vTabla + "', '" + vCampoKey + "'));"
        self.cur.execute(vSql)
        for row in self.cur.fetchall():
            return row[0]


    def ejecutar_sinReturnKey(self, vSql, vValores):
        self.cur.execute(vSql, vValores)
        self.con.commit()
        # self.cerrando()


    def cerrando(self):
        self.cur.close()
        self.con.close()
