from datetime import date

def unificacionStock(self):
    ejercicio = date.today().year
    periodo = date.today().month
    dia = date.today().day

    sql = " select top 1 "
    sql += " id_tarea, "  # 0
    sql += " fecha_busqueda, "  # 1
    sql += " fecha_tarea, "  # 2
    sql += " ejercicio_tarea, "  # 3
    sql += " periodo_tarea, "  # 4
    sql += " tipo_tarea, "  # 5
    sql += " cod_usuario "  # 6
    sql += " from tarea_programada_usuario "
    sql += " where YEAR(fecha_tarea)=%s " % (ejercicio)
    sql += " AND MONTH(fecha_tarea)=%s " % (periodo)
    sql += " AND DAY(fecha_tarea)=%s " % (dia)
    sql += " ORDER BY id_tarea DESC "

    meConexionSQL = conexion_mssql()
    resultado = meConexionSQL.consultar(sql, ('',))

    for row in resultado:
        v_Ejercicio = row[3]
        v_Periodo = row[4]
        url = "http://127.0.0.1:8000/produccion/stockunificado/%s-%s-%s" % (
        v_Ejercicio, str(v_Periodo).zfill(2), str(dia).zfill(2))
        meJSON = {}
        me_EXE = requests.post(url, json=meJSON)
        return me_EXE.text
