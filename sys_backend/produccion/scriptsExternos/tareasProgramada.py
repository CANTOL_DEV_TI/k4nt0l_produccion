### Librerias Sistema
from datetime import date
import requests



class tareasProgramada:
    def __init__(self):
        pass

    def unificacionStock(self):
        ### Librerias Personales
        from conexion_mmsql import conexion_mssql

        ejercicio = date.today().year
        periodo = date.today().month
        dia = date.today().day

        sql = " select top 1 "
        sql += " id_tarea, "    #0
        sql += " fecha_busqueda, "    #1
        sql += " fecha_tarea, "    #2
        sql += " ejercicio_tarea, "    #3
        sql += " periodo_tarea, "    #4
        sql += " tipo_tarea, "    #5
        sql += " cod_usuario "    #6
        sql += " from tarea_programada_usuario "
        sql += " where YEAR(fecha_tarea)=%s " %(ejercicio)
        sql += " AND MONTH(fecha_tarea)=%s " %(periodo)
        sql += " AND DAY(fecha_tarea)=%s " %(dia)
        sql += " ORDER BY id_tarea DESC "

        meConexionSQL = conexion_mssql()
        resultado = meConexionSQL.consultar(sql, ('', ))

        for row in resultado:
            v_Ejercicio = row[3]
            v_Periodo = row[4]
            url = "http://127.0.0.1:8000/produccion/stockunificado/%s-%s-%s" %(v_Ejercicio, str(v_Periodo).zfill(2), str(dia).zfill(2))
            meJSON = {}
            me_EXE = requests.post(url, json=meJSON)
            return me_EXE.text


    def tarea_Dos(self, Data):

        from conexion_Postgresql import conexion_Postgresql
        me_Conexion = conexion_Postgresql(dbname=Data['dbname'], host=Data['host'], user=Data['user'], password=Data['password'])
        resultado = me_Conexion.consultas(Data['sentencia'], ('',))
        print(resultado)


###############################################################################
############################## EJECUTANDO TAREAS ##############################
Tareas = tareasProgramada()
# rpta = Tareas.unificacionStock()
# print(rpta)

dic = {
'dbname'    : 'mAGGIE_Distrimax',
'user'      : 'postgres',
'host'      : '192.168.5.4',
'password'  : '1qaz2wsx',
'sentencia' : 'select * from recepcion_cab_sap where cod_recepcion>600'
}


Tareas.tarea_Dos(dic)

