from fastapi import FastAPI  
from fastapi.middleware.cors import CORSMiddleware  
from presupuesto.routes import presupuesto  
from produccion.routes import produccion  
from capacidad.routes import capacidad  
from venta.routes import venta
 
from seguridad.routes import seguridad  
from login.routes import Login  
 
from gdh.routes import gdh 
from calidad.routes import calidad 
from creditos_cobranzas.routes import creditoscobranzas
from compras_logistica.routes import compras_logistica
from marketing.routes import marketing 
from comercial.routes import comercial
from requerimientos.routers import requerimientos
#from tmpEma.routes import temporal  
 
#from seguridad.routes import usuario  
#from login.routes import Login  
 
 
# from tmpEma.routes import temporal 
 
# ============================================ 
from despachos.routes import gestionRetails 
 
# ============================================ 
 
 
 
 
from liquidespa.routes import liquidespa 
from tesoreria.routes import tesoreria 
from solpe.routes import solicitud 
 
from dotenv import load_dotenv  
  
app = FastAPI(  
    title="ERP Cantol",  
    description="Servicios modulares e integración con Sap B1",  
    version="1.1.5"  
)  
  
load_dotenv()  
  
origins = ["*"]  
  
app.add_middleware(  
    CORSMiddleware,  
    allow_origins=origins,  
    allow_credentials=True,  
    allow_methods=["*"],  
    allow_headers=["*"],  
)  
  
# HOME  
  
  
@app.get('/', tags=["Principal"])  
def hola():  
    return ("Bienvenido")  
  
# MODULO USUARIO  
# app.include_router(usuario.ruta)  
  
  
# MODULO PRODUCCION  
app.include_router(produccion.ruta)  
  
# MODULO PRESUPUESTO  
app.include_router(presupuesto.ruta)  
  
# MODULO CAPACIDAD  
app.include_router(capacidad.ruta)  
  
# MODULO VENTA  
app.include_router(venta.ruta)  
  
#MODULO SEGURIDAD  
app.include_router(seguridad.ruta)  
  
#LOGIN  
app.include_router(Login.ruta)  
 
##GDH 
app.include_router(gdh.ruta) 
#Calidad 
app.include_router(calidad.ruta) 
#Liquidacion y Despachos 
app.include_router(liquidespa.ruta) 
  
app.include_router(solicitud.ruta) 
 
#TEMPORAL 
# app.include_router(temporal.ruta) 
 
#DESPACHO 
app.include_router(gestionRetails.ruta) 
 
#Tesoreria 
app.include_router(tesoreria.ruta) 
 
 
#DrawBack 
#app.include_router(drawback.ruta)
app.include_router(compras_logistica.ruta)

#creditos cobranzas
app.include_router(creditoscobranzas.ruta)

##GDH 
app.include_router(marketing.ruta) 

#VENTAS
app.include_router(comercial.ruta)

#Requerimientos
app.include_router(requerimientos.ruta)