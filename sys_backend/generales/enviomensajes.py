import pywhatkit
from conexiones.conexion_sql_solo import ConectaSQL_Seguridad


def GetTelefono(pUsuario):
    sql = f"select isnull(nrotelefono,'') as nrotelefono from tusuarios where usuario_codigo='{pUsuario}'"

    conn = ConectaSQL_Seguridad()

    cur_sql = conn.cursor(as_dict = True)

    cur_sql.execute(sql)

    dato = ""

    for i in cur_sql:
        dato = i['nrotelefono']

    return dato

def EnviarMensaje(pUsuario,pMensaje):
    
    nro_telefono = GetTelefono(pUsuario)

    if(nro_telefono == ""):
        return "No existe telefono a enviar"
    else:
        mensaje = pMensaje

        pywhatkit.sendwhatmsg_instantly(nro_telefono,mensaje)

        print("Mensaje de WhatsApp enviado!!!!.")