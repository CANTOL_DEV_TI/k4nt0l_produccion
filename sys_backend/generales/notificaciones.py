import hashlib 
import random
import string
import ssl
import smtplib 
from email.message import EmailMessage
from conexiones.conexion_mmsql_seguridad import conexion_mssql
from conexiones.conexion_sql_solo import ConectaSQL_Seguridad

def ExtraerCorreoUsuario(pUsuario):
    email = ""
    nombre = ""

    sqlT = f"select usuario_email,usuario_nombre from tusuarios where usuario_codigo='{pUsuario}' or usuario_email='{pUsuario}'"    
    conn = ConectaSQL_Seguridad()

    cur_email = conn.cursor(as_dict = True)

    cur_email.execute(sqlT)

    for i in cur_email:
        email = i['usuario_email']
        nombre = i['usuario_nombre']
    
    if((email=="")or(email == None)):
        return {"Resultado":0,"Email":None,"Usuario":None}
    
    return {"Resultado":1,"Email": email,"Usuario":nombre}


def actualizarclave(pUsuario,pClaveE):
    valores = []
    valores.append(pClaveE)
    valores.append(pUsuario)
    
    sql = "update tUsuarios set usuario_password=%s where Usuario_Codigo=%s"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    #print(sql)
    return respuesta

def enviarcorreoresetclave(pUsuario,pClave):
    try:
        usuario_correo = ExtraerCorreoUsuario(pUsuario)
        #print(usuario_correo['Email'])
        destinatario = usuario_correo['Email']
        nombre = usuario_correo['Usuario']
        remitente = 'notificaciones-swc@cantolonsafe.com'

        mensaje = f'Hola {nombre} \n Tu contraseña se ha reseteado debes usar la siguiente {pClave} \r Se requerira que cambie la contraseña. \n En caso de no haber solicitado por favor avisar al area de TI'

        correo = EmailMessage()
        correo['From'] = remitente
        correo['To'] = destinatario
        correo['Subject'] = 'Reseteo de contraseña - SWC'
        correo.set_content(mensaje)
                
        print("creando contexto seguro SSL...")
        Context = ssl.create_default_context()
        print("Seteando valores...")
        print("comenzando conexion...")        
        with smtplib.SMTP_SSL(host='mail.cantolonsafe.com',port=465) as smtp: 
            print("login...")
            smtp.login(remitente,'Zn&(WJ_F*HDc')
            print("enviando correo...=>")
            smtp.sendmail(remitente,destinatario,correo.as_string())
            print("cerrando conexion...")            
    except smtplib.SMTPException as errE :
        print(f"Error E : {errE}")
    except smtplib.SMTPDataError as errE :
        print(f"Error DE : {errE}")
    except smtplib.SMTPConnectError as errE :
        print(f"Error CE : {errE}")
    except Exception as err:
        print(f"Error General : {err}")        
        return f"Error al enviar el mensaje : {err}"
    finally:
        #smtp.quit()
        return f"Correo enviado..."

def enviarnotificacionrq(pCia,pUsuario,pAprobador,pNro):
    try:
        usuario_correo = ExtraerCorreoUsuario(pAprobador)
        usuario_creador = ExtraerCorreoUsuario(pUsuario)
        #print(usuario_correo['Email'])
        destinatario = usuario_correo['Email']
        nombre = usuario_correo['Usuario']
        remitente_correo = usuario_creador['Email']
        remitente_nombre = usuario_creador['Usuario']
        remitente = 'notificaciones-swc@cantolonsafe.com'
        sCia = ""

        if(pCia == "CNT"):
            sCia = "CANTOL S.A.C."

        if(pCia == "DTM"):
            sCia = "DISTRIMAX S.A.C."
        
        if(pCia == "TCN"):
            sCia = "TECNOPRESS S.A.C."

        mensaje = f'Hola {nombre} \n Tienes la RQ {pNro} por aprobar en la bandeja de Requerimientos\n Creada por {remitente_nombre} {remitente_correo} \r Por la empresa {sCia}. \n \n Saludos'

        correo = EmailMessage()
        correo['From'] = remitente
        correo['To'] = destinatario
        correo['Subject'] = f'Notificacion RQ {pNro}'
        correo.set_content(mensaje)
                
        print("creando contexto seguro SSL...")
        Context = ssl.create_default_context()
        print("Seteando valores...")
        print("comenzando conexion...")        
        with smtplib.SMTP_SSL(host='mail.cantolonsafe.com',port=465) as smtp: 
            print("login...")
            smtp.login(remitente,'Zn&(WJ_F*HDc')
            print("enviando correo...=>")
            smtp.sendmail(remitente,destinatario,correo.as_string())
            print("cerrando conexion...")            
    except smtplib.SMTPException as errE :
        print(f"Error E : {errE}")
    except smtplib.SMTPDataError as errE :
        print(f"Error DE : {errE}")
    except smtplib.SMTPConnectError as errE :
        print(f"Error CE : {errE}")
    except Exception as err:
        print(f"Error General : {err}")        
        return f"Error al enviar el mensaje : {err}"
    finally:
        #smtp.quit()
        return f"Correo enviado..."

def enviarnotificacionaprobacion(pCia:str,pUsuario:str,pAprobador,pNro,pNroSAP):
    try:
        usuario_aprobador = ExtraerCorreoUsuario(pAprobador)
        usuario_creador = ExtraerCorreoUsuario(pUsuario)

        #print(usuario_correo['Email'])
        email_destinatario_aprobador = usuario_aprobador['Email']
        email_destinatario_creador = usuario_creador['Email']
        nombre_creador = usuario_creador['Usuario']
        nombre_aprobador = usuario_aprobador['Usuario']
        
        remitente = 'notificaciones-swc@cantolonsafe.com'
        sCia = ""

        if(pCia == "CNT"):
            sCia = "CANTOL S.A.C."

        if(pCia == "DTM"):
            sCia = "DISTRIMAX S.A.C."
        
        if(pCia == "TCN"):
            sCia = "TECNOPRESS S.A.C."

        mensaje = f'Hola {nombre_creador} \n Tu RQ {pNro} fue aprobada por {nombre_aprobador} [{email_destinatario_aprobador}] \n generandose la Solicitud de Compra en SAP Nro {pNroSAP} \r Por la empresa {sCia}. \n \n Saludos'

        correo = EmailMessage()
        correo['From'] = remitente
        correo['To'] = email_destinatario_aprobador + "|" + email_destinatario_creador
        correos_a_enviar = email_destinatario_aprobador + "|" + email_destinatario_creador
        correo['CC'] = email_destinatario_creador        
        correo['Subject'] = f'Notificacion de aprobación de RQ {pNro}'
        correo.set_content(mensaje)
                
        print("creando contexto seguro SSL...")
        Context = ssl.create_default_context()
        print("Seteando valores...")
        print("comenzando conexion...")        
        with smtplib.SMTP_SSL(host='mail.cantolonsafe.com',port=465) as smtp: 
            print("login...")
            smtp.login(remitente,'Zn&(WJ_F*HDc')
            print("enviando correo...=>")
            smtp.sendmail(remitente,correos_a_enviar,correo.as_string())
            print("cerrando conexion...")            
    except smtplib.SMTPException as errE :
        print(f"Error E : {errE}")
    except smtplib.SMTPDataError as errE :
        print(f"Error DE : {errE}")
    except smtplib.SMTPConnectError as errE :
        print(f"Error CE : {errE}")
    except Exception as err:
        print(f"Error General : {err}")        
        return f"Error al enviar el mensaje : {err}"
    finally:
        #smtp.quit()
        return f"Correo enviado..."
    

def generarclave(pUsuario):
    try:
        N = 7

        nueva = ''.join(random.choices(string.ascii_uppercase + string.digits, k = N))

        dc_contrasena = hashlib.md5(str(nueva).encode()).hexdigest()

        actualizarclave(pUsuario,dc_contrasena)

        print(pUsuario,nueva,dc_contrasena)

        resultado = enviarcorreoresetclave(pUsuario,nueva)        

        return resultado
    except Exception as err:
        print(err)
        return f"Error al generar : {err}"