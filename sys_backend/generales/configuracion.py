from conexiones.conexion_sql_solo import ConectaSQL_Seguridad

def GetSeries(pCia):
    conn = ConectaSQL_Seguridad()
    cur_series = conn.cursor(as_dict = True)

    sql = "select serie_pagos_efectuados, serie_pagos_recibidos, serie_boletas, serie_facturas, serie_orden_venta ,serie_nota_credito ,"
    sql += f"serie_nota_debito, serie_letras_refinanciadas, serie_solicitud_compra from tEmpresa where empresa_codigo='{pCia}'"

    cur_series.execute(sql)
    resultado = {}
    for i in cur_series:
        resultado = { "serie_pagos_efectuados" : i['serie_pagos_efectuados'], "serie_pagos_recibidos" : i['serie_pagos_recibidos'], "serie_boletas" : i['serie_boletas'],
                      "serie_facturas" : i['serie_facturas'], "serie_orden_venta" : i['serie_orden_venta'] ,"serie_nota_credito" : i['serie_nota_credito'],
                      "serie_nota_debito":i['serie_nota_debito'], "serie_letras_refinanciadas" : i['serie_letras_refinanciadas'], 
                      "serie_solicitud_compra" : i['serie_solicitud_compra']}

    return resultado