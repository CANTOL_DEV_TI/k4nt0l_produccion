from conexiones.conexion_sql_solo import ConectaSQL_Produccion

def horario():
    conn = ConectaSQL_Produccion()
    
    cur_cab = conn.cursor(as_dict = True)
    
    sqlC = "select horario_id,fecha_horario from cabecera order by horario_id"
    
    tabla = []

    cur_cab.execute(sqlC)
    
    for filac in cur_cab:
        columna = {'columna': filac['fecha_horario'], 'filas' : []}
        horarioid = filac['horario_id']    
        

        sqlD = f"select linea,horario,cupos from detalle where horario_id = {horarioid} order by horario_id"
    
        conn2 = ConectaSQL_Produccion()
        cur_det = conn2.cursor(as_dict = True)

        cur_det.execute(sqlD)

        lista_filas = []

        for filad in cur_det:            
            lista_filas.append({'fila' : filad['horario'],'cupos':filad['cupos']})
        
        columna['filas'] = lista_filas

        tabla.append(columna)

    return tabla
