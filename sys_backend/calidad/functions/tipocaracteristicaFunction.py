from conexiones.conexion_mmsql import conexion_mssql 
 
def insert(meData): 
    valores = [] 
    valores.append(meData.tipcar_codigo) 
    valores.append(meData.tipcar_nombre) 
         
    sql = "insert into cali_tipo_caracteristica(tipcar_codigo,tipcar_nombre) values(%s,%s)" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    #print(sql) 
    return respuesta 
 
def update(meData): 
    valores = []     
    valores.append(meData.tipcar_nombre) 
    valores.append(meData.tipcar_codigo)     
     
    sql = "update cali_tipo_caracteristica set tipcar_nombre=%s where tipcar_codigo=%s" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))     
    return respuesta 
 
def delete(meData): 
    valores = [] 
    valores.append(meData.tipcar_codigo) 
 
    sql = "delete from cali_tipo_caracteristica where tipcar_codigo=%s" 
     
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    #print(sql) 
    return respuesta