from pymssql import *
from decimal import Decimal

from calidad.schemas.formulasSchema import Valores

def formula_LIE(valor_a):
    
    s_formula = float(valor_a) - 0.1

    return s_formula

def formula_LSE(valor_a):
    
    s_formula = float(valor_a) + 0.1

    return s_formula

def formula_LSC(valor_a, valor_b):
    
    s_formula = float(valor_a) - (((float(valor_a) - float(valor_b)) * 0.30 / 2 )) 

    return s_formula

def formula_LIC(valor_a, valor_b):
    s_formula = float(valor_a) + (((float(valor_b) - float(valor_a)) * 0.30 / 2 )) 

    return s_formula

def calcular_formulas_torno(valores : Valores):
    valor = valores.valor_1
    valor_LIE = '{:.2f}'.format(round(formula_LIE(valor), 2))
    valor_LSE = '{:.2f}'.format(round(formula_LSE(valor), 2))
    valor_LSC = '{:.2f}'.format(round(formula_LSC(valor_LSE,valor_LIE), 2))
    valor_LIC = '{:.2f}'.format(round(formula_LIC(valor_LIE,valor_LSE), 2))

    resultado = {"LIE" : valor_LIE, "LSE" : valor_LSE, "LSC" : valor_LSC , "LIC": valor_LIC}
    
    return resultado


