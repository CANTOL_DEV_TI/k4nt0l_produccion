from conexiones.conexion_mmsql import conexion_mssql 
 
def insert(meData): 
    valores = [] 
    valores.append(meData.fampie_codigo) 
    valores.append(meData.fampie_nombre) 
         
    sql = "insert into cali_familia_piezas(fampie_codigo,fampie_nombre) values(%s,%s)" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    print(sql) 
    return respuesta 
 
def update(meData): 
    valores = []     
    valores.append(meData.fampie_nombre) 
    valores.append(meData.fampie_codigo)     
     
    sql = "update cali_familia_piezas set fampie_nombre=%s where fampie_codigo=%s" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))     
    return respuesta 
 
def delete(meData): 
    valores = [] 
    valores.append(meData.fampie_codigo) 
 
    sql = "delete from cali_familia_piezas where fampie_codigo=%s" 
     
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    print(sql) 
    return respuesta