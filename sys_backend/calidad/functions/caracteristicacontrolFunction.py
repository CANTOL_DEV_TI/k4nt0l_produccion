from conexiones.conexion_mmsql import conexion_mssql 
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
 
def insert(meData): 
    valores = [] 
    valores.append(meData.carcon_codigo) 
    valores.append(meData.carcon_nombre) 
         
    sql = "insert into cali_caracteristica_control(carcon_codigo,carcon_nombre) values(%s,%s)" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    print(sql) 
    return respuesta 
 
def update(meData): 
    valores = []     
    valores.append(meData.carcon_nombre) 
    valores.append(meData.carcon_codigo)     
     
    sql = "update cali_caracteristica_control set carcon_nombre=%s where carcon_codigo=%s" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))     
    return respuesta 
 
def delete(meData): 
    valores = [] 
    valores.append(meData.carcon_codigo) 
 
    sql = "delete from cali_caracteristica_control where carcon_codigo=%s" 
     
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    print(sql) 
    return respuesta

def ListaCarConxArticulo(pArticulo:str):
    print("Entro LCCA")
    cad_sql = "select cpa.carcon_codigo as carcon_codigo  ,ccc.carcon_nombre as carcon_nombre from cali_propiedades_articulo cpa (nolock)"
    cad_sql += f" inner join cali_caracteristica_control ccc (nolock) on cpa.carcon_codigo = ccc.carcon_codigo where cpa.cod_articulo = '{pArticulo}'"
    
    print(cad_sql)

    conn = ConectaSQL_Produccion()
    cur_val_carcon = conn.cursor(as_dict = True)

    cur_val_carcon.execute(cad_sql)

    car_con = []

    for row in cur_val_carcon:
       fila = {"carcon_codigo" : row['carcon_codigo'],"carcon_nombre" : row['carcon_nombre']}
       car_con.append(fila)    
                                                      
    conn.close()

    return car_con