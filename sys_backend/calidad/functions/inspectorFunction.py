from conexiones.conexion_sql_solo import ConectaSQL_Produccion

def InspectorValida(pUsuario):
    conn = ConectaSQL_Produccion()
    cur_val_inspector = conn.cursor(as_dict = True)
       
    cad_sql = f"select count(*) as existe from cali_personal p inner join cali_roles r on p.rol_id = r.rol_id and r.rol_id = 1 where p.usuarioid = '{pUsuario}'"
    #print(cad_sql)
    cur_val_inspector.execute(cad_sql)

    valido = 0

    for row in cur_val_inspector:
       valido = (row['existe'])    
                                                      
    conn.close()    
    
    return valido