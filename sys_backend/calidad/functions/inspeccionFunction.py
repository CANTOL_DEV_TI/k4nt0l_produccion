from conexiones.conexion_sql_solo import ConectaSQL_Produccion  
from conexiones.conexion_mmsql import conexion_mssql  
from conexiones.conexion_sap_solo import conexion_SAP_Tecno  
  
def insert(meData):  
    #print (meData) 
    conn = ConectaSQL_Produccion()  
     
    cur_val_inspeccion = conn.cursor(as_dict = True)  
  
    cad_id = f"select '{meData.fecha_inspeccion}' + '{meData.inspector}' + RIGHT ('000' + cast(isnull(count(*),0) + 1 as varchar(3)),3) as ID from cali_inspeccion_cabecera (nolock)"  
    cad_id += f"where fecha_inspeccion = '{meData.fecha_inspeccion}' and inspector='{meData.inspector}' "  
 
    cur_val_inspeccion.execute(cad_id)  
    inspeccionid = ''  
      
    for row in cur_val_inspeccion:  
       inspeccionid = (row['ID'])  
  
    if inspeccionid=='':  
        conn.close()  
        return "Error en el registro de la Inspeccion"  
              
    cad_add = f"insert into cali_inspeccion_cabecera(inspeccionid,fampie_codigo, cod_articulo, id_art_plano,comentario,fecha_inspeccion,inspector,id_equipo,fecha_registro)"   
    cad_add += f"values('{inspeccionid}','{meData.fampie_codigo}','{meData.cod_articulo}','{meData.id_art_plano}','{meData.comentario}',"  
    cad_add += f"'{meData.fecha_inspeccion}','{meData.inspector}',{meData.id_equipo},getdate())"  
     
    cur_val_inspeccion.execute(cad_add)  
    conn.commit()     
    print("Guardar...")  
      
    iddetinspeccion = 1  
  
    for Det in meData.detalles:          
        cad_det = "insert into cali_inspeccion_detalle(inspeccionid,linea,tipcar_codigo,carcon_codigo, LIE,LIC,OBJ,LSC,LSE,referencias,instrumento,"  
        cad_det += f"frecuencia_inspeccion_hora,cantidad_muestra,valor_inspeccion,valor_inspeccion_2,valor_inspeccion_3) values ('{inspeccionid}',{iddetinspeccion},'{Det.tipcar_codigo}','{Det.carcon_codigo}',{Det.LIE},"  
        cad_det += f"{Det.LIC},{Det.OBJ},{Det.LSC},{Det.LSE},'{Det.referencias}','{Det.instrumento}','{Det.frecuencia_inspeccion_hora}' ,{Det.cantidad_muestra},{Det.valor_inspeccion}," 
        cad_det += f"{Det.valor_inspeccion_2},{Det.valor_inspeccion_3})"  
         
        iddetinspeccion += 1  
        cur_val_inspeccion.execute(cad_det)  
        conn.commit()   
  
    conn.close()                   
    ##auditoria.auditausuarios(3 ,pCab.cod_usuario,pCab.empresa_aud,f"Creacion de formula {idformula} para {pCab.cod_articulo}")  
  
    return f"Registro exitoso de la Inspeccion : {inspeccionid}."  
  
def update(meData):  
    conn = ConectaSQL_Produccion()  
      
    cur_val_inspeccion = conn.cursor(as_dict = True)  
      
    #print(meData)  
  
    if meData.inspeccionid=='':  
        conn.close()  
        return "Error en el registro de la formula"  
      
    cad_limpia_det = f"delete from cali_inspeccion_detalle where inspeccionid='{meData.inspeccionid}'"      
    cur_val_inspeccion.execute(cad_limpia_det)  
    conn.commit();  
                                      
    cad_upd =  f"update cali_inspeccion_cabecera set fampie_codigo ='{meData.fampie_codigo}', cod_articulo = '{meData.cod_articulo}',"  
    cad_upd += f"id_art_plano = '{meData.id_art_plano}', comentario='{meData.comentario}', fecha_inspeccion = '{meData.fecha_inspeccion}', inspector = '{meData.inspector}'"  
    cad_upd += f",id_equipo = {meData.id_equipo} where inspeccionid = '{meData.inspeccionid}'"  
    #print(cad_upd) 
      
    cur_val_inspeccion.execute(cad_upd)  
    conn.commit()     
          
    iddetinspeccion = 1  
  
    for Det in meData.detalles:          
        cad_det = f"insert into cali_inspeccion_detalle(inspeccionid,linea,tipcar_codigo,carcon_codigo, LIE,LIC,OBJ,LSC,LSE,referencias,instrumento,"  
        cad_det += f"frecuencia_inspeccion_hora,cantidad_muestra,valor_inspeccion,valor_inspeccion_2,valor_inspeccion_3) values ('{meData.inspeccionid}',{iddetinspeccion},'{Det.tipcar_codigo}','{Det.carcon_codigo}',{Det.LIE},"  
        cad_det += f"{Det.LIC},{Det.OBJ},{Det.LSC},{Det.LSE},'{Det.referencias}','{Det.instrumento}','{Det.frecuencia_inspeccion_hora}',{Det.cantidad_muestra},{Det.valor_inspeccion},"      
        cad_det += f"{Det.valor_inspeccion_2},{Det.valor_inspeccion_3})" 
          
        #print(cad_det)  
        iddetinspeccion += 1  
        cur_val_inspeccion.execute(cad_det)  
        conn.commit()   
  
    conn.close()  
 
    return f"Actualización exitosa de la Inspeccion : {meData.inspeccionid}."  
  
def delete(meData):  
    valores = []  
    valores.append(meData.inspeccionid) 
     
    nivel = ValidaNivel(meData.inspector) 
 
    #print(meData) 
    #print(nivel) 
 
    if(nivel == 0): 
        if(meData.inspector == meData.usuario): 
     
            sql = "delete from cali_inspeccion_detalle where inspeccionid=%s "  
         
            meConexion = conexion_mssql()  
            respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))  
         
            sql = "delete from cali_inspeccion_cabecera where inspeccionid=%s "  
         
            meConexion = conexion_mssql()  
            respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))  
        else: 
            respuesta = "Solo los inspectores de nivel superior pueden borrar inspecciones de otros inspectores" 
    else: 
            sql = "delete from cali_inspeccion_detalle where inspeccionid=%s "  
         
            meConexion = conexion_mssql()  
            respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))  
         
            sql = "delete from cali_inspeccion_cabecera where inspeccionid=%s "  
         
            meConexion = conexion_mssql()  
            respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))  
  
    return respuesta  
  
def NombreArticulo(pCodigo):  
      
    lista_total = []  
    lista_fila = dict()    
    #conn = conexion_SAP_Tecno()  
    conn = ConectaSQL_Produccion()  
      
    cur_val_lista = conn.cursor(as_dict = True)  
  
    #cad_lista = f"SELECT \"ItemName\" as nombre_item FROM SBO_TECNO_PRODUCCION.OITM WHERE \"ItemCode\" = '{pCodigo}'"  
    cad_lista = f"select articulo as nombre_item from articulo_familia (nolock) where cod_articulo = '{pCodigo}'"  
  
    cur_val_lista.execute(cad_lista)  
  
    for row in cur_val_lista:  
        lista_fila = {"nombre" : row['nombre_item']}  
        lista_total.append(lista_fila)  
              
    conn.close()  
  
    return lista_total  
  
def listaFamiliaPiezas():  
    lista_total = []  
    lista_fila = dict()  
  
    conn = ConectaSQL_Produccion()  
  
    cur_val_lista = conn.cursor(as_dict = True)  
  
    cad_lista = "select fampie_nombre as nombre, fampie_codigo as codigo from cali_familia_piezas (nolock) "  
  
    cur_val_lista.execute(cad_lista)  
  
    for row in cur_val_lista:  
        lista_fila = {"nombre" : row['nombre'], "codigo" : row['codigo']}  
        lista_total.append(lista_fila)  
              
    conn.close()  
  
    return lista_total  
  
def listaCaracteristicaControl():  
    lista_total = []  
    lista_fila = dict()  
  
    conn = ConectaSQL_Produccion()  
  
    cur_val_lista = conn.cursor(as_dict = True)  
  
    cad_lista = "select carcon_nombre as nombre, carcon_codigo as codigo from cali_caracteristica_control (nolock) "  
  
    cur_val_lista.execute(cad_lista)  
  
    for row in cur_val_lista:  
        lista_fila = {"nombre" : row['nombre'], "codigo" : row['codigo']}  
        lista_total.append(lista_fila)  
              
    conn.close()  
  
    return lista_total  
  
def listaTipoCaracteristica():  
    lista_total = []  
    lista_fila = dict()  
  
    conn = ConectaSQL_Produccion()  
  
    cur_val_lista = conn.cursor(as_dict = True)  
  
    cad_lista = "select tipcar_nombre as nombre, tipcar_codigo as codigo from cali_tipo_caracteristica (nolock) "  
  
    cur_val_lista.execute(cad_lista)  
  
    for row in cur_val_lista:  
        lista_fila = {"nombre" : row['nombre'], "codigo" : row['codigo']}  
        lista_total.append(lista_fila)  
              
    conn.close()  
  
    return lista_total  
  
def listaArticulos(pNombre):  
    lista_total = []  
    lista_fila = dict()  
  
    conn = ConectaSQL_Produccion()  
  
    cur_val_lista = conn.cursor(as_dict = True)  
  
    cad_lista = "select af.cod_articulo as cod_articulo,af.articulo as articulo,ap.cod_plano as plano,ap.id_version as version,ap.sw_est_plano as estado from articulo_familia af "  
    cad_lista += "inner join articulo_plano ap on ap.cod_articulo = af.cod_articulo and ap.sw_est_plano in ('O','P') "  
    cad_lista += f"where af.cod_articulo like 'PP%' and af.articulo like '%{pNombre}%'"  
  
    cur_val_lista.execute(cad_lista)  
  
    for row in cur_val_lista:  
        lista_fila = {"nombre" : row['articulo'], "codigo" : row['cod_articulo'], "plano" : row['plano'] , "version" : row['version'], "estado" : row['estado']}  
        lista_total.append(lista_fila)  
      
    conn.close()  
  
    return lista_total 
 
def listaInspeccion(pDesde,pHasta,pArticulo): 
    lista_total = [] 
    lista_fila = dict() 
 
    conn = ConectaSQL_Produccion() 
 
    cur_val_lista = conn.cursor(as_dict = True) 
 
    cad_lista = "select convert(varchar(10),cic.fecha_inspeccion,23) as fecha_inspeccion,cic.inspeccionid,cfp.fampie_codigo as fampie_codigo,cfp.fampie_nombre as FamiliaPiezas,"     
    cad_lista += "convert(varchar(50),cic.fecha_registro,20) as fecha_registro," 
    cad_lista += "cic.cod_articulo,af.articulo,cic.id_art_plano,cic.inspector,isnull(cic.id_equipo,0) as id_equipo,isnull(e.equipo,'SIN ASIGNAR') as maquina,cic.comentario, " 
    cad_lista += "(select case when avg(obj) = avg(valor_inspeccion) and avg(obj) = avg(valor_inspeccion_2) and avg(obj) = avg(valor_inspeccion_3) then 'SATISFACTORIO' " 
    cad_lista += " else 'NO CUMPLE' end as Estado from cali_inspeccion_detalle cid (nolock) where cid.inspeccionid = cic.inspeccionid) as estado" 
    cad_lista += " from cali_inspeccion_cabecera cic (nolock) inner join cali_familia_piezas cfp (nolock) on cic.fampie_codigo = cfp.fampie_codigo " 
    cad_lista += " inner join articulo_familia af (nolock) on af.cod_articulo = cic.cod_articulo left join equipo e (nolock) on e.id_equipo = cic.id_equipo" 
    cad_lista += f" where af.articulo like '%{pArticulo}%' and cic.fecha_inspeccion BETWEEN '{pDesde}' and '{pHasta}'" 
    #print(cad_lista) 
    cur_val_lista.execute(cad_lista) 
 
    for row in cur_val_lista: 
        lista_fila = {"inspeccionid" : row['inspeccionid'],"familiapiezas" : row['FamiliaPiezas'],"cod_articulo" : row['cod_articulo'],"articulo" : row['articulo'], 
                      "id_art_plano" : row['id_art_plano'],"inspector" : row['inspector'],"fecha_inspeccion" : row['fecha_inspeccion'],"maquina" : row['maquina'] , 
                      "id_equipo" : row['id_equipo'],"fampie_codigo" : row['fampie_codigo'], "comentario" : row['comentario'], "estado" : EvaluarMuestrasXInspeccion(row['inspeccionid']),  
                      "fecha_registro" : row['fecha_registro']} 
        lista_total.append(lista_fila) 
             
    conn.close() 
 
    return lista_total 
 
def ValidaNivel(pUsuario): 
    nivel_inspector = 0 
 
    conn = ConectaSQL_Produccion() 
 
    cur_val_propiedad = conn.cursor(as_dict = True) 
 
    cad_sql = f"select rol_nivel from cali_roles cr inner join cali_personal cp on cp.rol_id = cr.rol_id where cp.usuarioid = '{pUsuario}'" 
 
    cur_val_propiedad.execute(cad_sql) 
 
    for row in cur_val_propiedad: 
        nivel_inspector = {"rol_nivel" : row['rol_nivel']} 
     
    conn.close() 
 
    return nivel_inspector 
 
def DetalleInspeccion(pInspeccionID): 
    listaDetalle = [] 
    conn = ConectaSQL_Produccion() 
 
    cur_val_detalle =  conn.cursor(as_dict = True) 
 
    cad_detalle = "select inspeccionid,linea,tipcar_codigo ,carcon_codigo ,LIE ,LIC ,OBJ ,LSC ,LSE ,referencias ,instrumento ,frecuencia_inspeccion_hora" 
    cad_detalle += f",cantidad_muestra,valor_inspeccion,valor_inspeccion_2,valor_inspeccion_3 from cali_inspeccion_detalle cid (nolock) where inspeccionid = '{pInspeccionID}'" 
 
    cur_val_detalle.execute(cad_detalle) 
 
    for row in cur_val_detalle: 
        fila = {"inspeccionid" : row['inspeccionid'], "linea" : row['linea'] , "tipcar_codigo" : row['tipcar_codigo'], "carcon_codigo" : row['carcon_codigo'], "LIE" : row['LIE'], "LIC" : row['LIC'], 
                "OBJ" : row['OBJ'], "LSC" : row['LSC'],"LSE" : row['LSE'], "referencias" : row['referencias'],"instrumento" : row['instrumento'],"frecuencia_inspeccion_hora" : row['frecuencia_inspeccion_hora'], 
                "cantidad_muestra" : row['cantidad_muestra'], "valor_inspeccion" : row['valor_inspeccion'], "valor_inspeccion_2" : row['valor_inspeccion_2'], "valor_inspeccion_3" : row['valor_inspeccion_3']} 
        listaDetalle.append(fila) 
 
    conn.close() 
 
    return listaDetalle 
 
def ListaEquiposxArticulo(pArticulo): 
    listaDetalle = [] 
 
    conn = ConectaSQL_Produccion() 
     
    cur_val_equipos = conn.cursor(as_dict = True) 
 
    cad_equipos = "select cpd.id_equipo, e.equipo from config_proceso_det cpd (nolock) inner join config_proceso_cab cpc (nolock) on cpc.id_fmproceso = cpd.id_fmproceso " 
    cad_equipos += f"inner join equipo e (nolock) on e.id_equipo = cpd.id_equipo where cpc.cod_articulo = '{pArticulo}'" 
 
    cur_val_equipos.execute(cad_equipos) 
 
    for row in cur_val_equipos: 
        fila = {"id_equipo" : row['id_equipo'],"equipo" : row['equipo']} 
        listaDetalle.append(fila) 
     
    conn.close() 
 
    return listaDetalle 
 
def EvaluarMuestrasXInspeccion(pInspeccion): 
    resultado = "SIN MUESTRAS" 
    lista = [] 
 
    conn = ConectaSQL_Produccion() 
 
    cur_val_muestras = conn.cursor(as_dict = True) 
 
    cad_muestras = "select case when round((valor_inspeccion  + valor_inspeccion_2 + valor_inspeccion_3) / 3,2) between lic and lsc then 'CUMPLE' else 'NO CUMPLE' end"  
    cad_muestras += f" as muestra from cali_inspeccion_detalle cid where inspeccionid = '{pInspeccion}'" 
 
    cur_val_muestras.execute(cad_muestras) 
 
    for row in cur_val_muestras: 
        fila = {"muestra" : row['muestra']} 
        lista.append(fila) 
     
    for m in lista: 
        if(m['muestra'] == "CUMPLE"): 
            if(resultado!="NO CUMPLE"): 
                resultado = "CONFORME" 
            else: 
                resultado = "RECHAZADO" 
        else: 
            resultado = "RECHAZADO" 
 
    return resultado 