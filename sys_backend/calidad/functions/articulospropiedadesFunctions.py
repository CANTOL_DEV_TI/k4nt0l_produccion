from conexiones import conexion_sql_solo
from decimal import Decimal
from conexiones.conexion_mmsql import conexion_mssql

from calidad.schemas.formulasSchema import Valores

def Extraer(pArticulo,pccontrol):
    propArti = {}

    conn = conexion_sql_solo.ConectaSQL_Produccion()

    cur_valida = conn.cursor(as_dict = True)

    sqlVal = f"select count(*) as Valido from cali_propiedades_articulo where cod_articulo='{pArticulo}' and carcon_codigo='{pccontrol}'"

    cur_valida.execute(sqlVal)

    for row in cur_valida:
        if(row['Valido'] == 1 ):
            sql = f"select LIE,LIC,OBJ,LSC,LSE from cali_propiedades_articulo where cod_articulo='{pArticulo}' and carcon_codigo='{pccontrol}'"
            #print(sql)
            
            #conn = conexion_sql_solo.ConectaSQL_Produccion()

            cur_propiedades = conn.cursor(as_dict = True)

            cur_propiedades.execute(sql)

            for row in cur_propiedades:
                propArti = {"LIE" : row['LIE'],"LIC":row['LIC'],"OBJ":row['OBJ'],"LSC":row['LSC'],"LSE":row['LSE']}
        else:
            propArti = {"LIE" : -1,"LIC" : -1,"OBJ" : -1,"LSC":-1,"LSE":-1}

    conn.close()

    return propArti

def insert(meData):
    valores = [] 
    valores.append(meData.cod_articulo) 
    valores.append(meData.carcon_codigo) 
    valores.append(meData.LIE)
    valores.append(meData.LIC)
    valores.append(meData.OBJ)
    valores.append(meData.LSC)
    valores.append(meData.LSE)

         
    sql = "insert into cali_propiedades_articulo(cod_articulo,carcon_codigo,LIE,LIC,OBJ,LSC,LSE) values(%s,%s,%s,%s,%s,%s,%s)" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    print(sql) 
    return respuesta 
    

def update(meData):
    valores = [] 
    valores.append(meData.LIE)
    valores.append(meData.LIC)
    valores.append(meData.OBJ)
    valores.append(meData.LSC)
    valores.append(meData.LSE)
    valores.append(meData.cod_articulo) 
    valores.append(meData.carcon_codigo) 
    
         
    sql = "update cali_propiedades_articulo set LIE=%s,LIC=%s,OBJ=%s,LSC=%s,LSE=%s  where cod_articulo=%s and carcon_codigo=%s" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    print(sql) 
    return respuesta 
    

def delete(meData):

    valores = []     
    valores.append(meData.cod_articulo) 
    valores.append(meData.carcon_codigo) 
    
         
    sql = "delete cali_propiedades_articulo where cod_articulo=%s and carcon_codigo=%s" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    print(sql) 
    return respuesta 