from typing import Optional
from pydantic import BaseModel 
from decimal import Decimal
 
 
class articulospropiedades_Schema(BaseModel): 
    cod_articulo : Optional[str] 
    carcon_codigo : Optional[str] 
    LIE : Optional[Decimal]
    LIC : Optional[Decimal]
    OBJ : Optional[Decimal]
    LSC : Optional[Decimal]
    LSE : Optional[Decimal]
 
    class Config: 
        orm_mode = True