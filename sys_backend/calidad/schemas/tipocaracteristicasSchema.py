from typing import Optional 
from pydantic import BaseModel 
 
 
class tipocaracteristica_Schema(BaseModel): 
    tipcar_codigo : Optional[str] 
    tipcar_nombre : Optional[str] 
 
    class Config: 
        orm_mode = True