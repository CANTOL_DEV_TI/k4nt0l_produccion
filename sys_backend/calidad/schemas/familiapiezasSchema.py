from typing import Optional 
from pydantic import BaseModel 
 
 
class familiapiezas_Schema(BaseModel): 
    fampie_codigo : Optional[str] 
    fampie_nombre : Optional[str] 
 
    class Config: 
        orm_mode = True