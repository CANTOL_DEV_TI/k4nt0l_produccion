from typing import List,Optional
from pydantic import BaseModel

class Valores(BaseModel):
    valor_1 : Optional[float]
    valor_2 : Optional[float]
    valor_3 : Optional[float]
    valor_4 : Optional[float]
    valor_5 : Optional[float]
    valor_6 : Optional[float]
    valor_7 : Optional[float]
    valor_8 : Optional[float]
    valor_9 : Optional[float]

    class Config:
        orm_mode = True