from typing import Optional 
from pydantic import BaseModel 
 
 
class caracteristicacontrol_Schema(BaseModel): 
    carcon_codigo : Optional[str] 
    carcon_nombre : Optional[str] 
 
    class Config: 
        orm_mode = True