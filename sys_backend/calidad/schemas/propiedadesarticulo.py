from typing import Optional
from pydantic import BaseModel

class PropiedadesFiltro(BaseModel):
    cod_articulo : Optional[str]
    carcon_codigo : Optional[str]

    class Config:
        orm_mode = True