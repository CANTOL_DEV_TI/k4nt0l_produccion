from typing import List,Optional 
from pydantic import BaseModel 
from decimal import Decimal 
from datetime import datetime
 
class Inspeccion_Det_Schema(BaseModel): 
    inspeccionid : Optional[str] 
    linea : Optional[int] 
    tipcar_codigo : Optional[str] 
    carcon_codigo : Optional[str] 
    LIE : Optional[Decimal] 
    LIC : Optional[Decimal] 
    OBJ : Optional[Decimal] 
    LSC : Optional[Decimal] 
    LSE : Optional[Decimal] 
    referencias : Optional[str]    
    instrumento : Optional[str] 
    frecuencia_inspeccion_hora : Optional[str] 
    cantidad_muestra : Optional[int]
    valor_inspeccion : Optional[Decimal]   
    valor_inspeccion_2 : Optional[Decimal]   
    valor_inspeccion_3 : Optional[Decimal]   
 
class Inspeccion_Cab_Schema(BaseModel): 
    inspeccionid : Optional[str] 
    fampie_codigo : Optional[str] 
    cod_articulo : Optional[str] 
    articulo : Optional[str] 
    id_art_plano : Optional[str] 
    comentario : Optional[str] 
    fecha_inspeccion : Optional[str] 
    inspector : Optional[str]
    id_equipo : Optional[int]
    usuario : Optional[str] 
    detalles : Optional[List[Inspeccion_Det_Schema]] 
 
    class Config: 
        orm_mode = True 
