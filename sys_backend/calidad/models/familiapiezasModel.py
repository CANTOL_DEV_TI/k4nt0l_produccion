from sqlalchemy import Column, String 
from conexiones.conexion_sql import Base 
 
 
class familiapiezas_Model(Base): 
    __tablename__ = 'cali_familia_piezas' 
    fampie_codigo = Column(String, primary_key=True) 
    fampie_nombre = Column(String) 
 
 
    def __str__(self): 
        return self.fampie_nombre