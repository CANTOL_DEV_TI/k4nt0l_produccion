from sqlalchemy import Column, String, DateTime
from conexiones.conexion_sql import Base 
 
 
class inspeccion_Model(Base): 
    __tablename__ = 'cali_inspeccion_cabecera' 
    inspeccionid = Column(String, primary_key=True) 
    fampie_codigo = Column(String) 
    cod_articulo = Column(String) 
    id_art_plano = Column(String) 
    comentario = Column(String) 
    fecha_inspeccion = Column(DateTime) 
    inspector = Column(String) 
    
 
    def __str__(self): 
        return self.cod_articulo