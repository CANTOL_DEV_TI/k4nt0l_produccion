from sqlalchemy import Column, String 
from conexiones.conexion_sql import Base 
 
 
class tipocaracteristica_Model(Base): 
    __tablename__ = 'cali_tipo_caracteristica' 
    tipcar_codigo = Column(String, primary_key=True) 
    tipcar_nombre = Column(String) 
 
 
    def __str__(self): 
        return self.tipcar_nombre