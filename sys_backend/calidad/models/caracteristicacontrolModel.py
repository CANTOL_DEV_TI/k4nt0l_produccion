from sqlalchemy import Column, String 
from conexiones.conexion_sql import Base 
 
 
class caracteristicacontrol_Model(Base): 
    __tablename__ = 'cali_caracteristica_control' 
    carcon_codigo = Column(String, primary_key=True) 
    carcon_nombre = Column(String) 
 
 
    def __str__(self): 
        return self.carcon_nombre