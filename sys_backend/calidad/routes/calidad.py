from fastapi import APIRouter

from calidad.routes import caracteristicacontrol,familiapiezas,tipocaracteristica,inspeccion,formulas,inspector,articulopropiedades

ruta = APIRouter(
    prefix="/calidad",
)

ruta.include_router(caracteristicacontrol.ruta)
ruta.include_router(familiapiezas.ruta)
ruta.include_router(tipocaracteristica.ruta)
ruta.include_router(inspeccion.ruta)
ruta.include_router(formulas.ruta)
ruta.include_router(inspector.ruta)
ruta.include_router(articulopropiedades.ruta)
