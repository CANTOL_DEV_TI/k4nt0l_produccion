from fastapi import APIRouter, Depends, status, UploadFile, File, Request 
from fastapi.responses import FileResponse 
from sqlalchemy.orm.session import Session 
 
from conexiones.conexion_sql import get_db 
from calidad.schemas.caracteristicacontrolSchema import caracteristicacontrol_Schema 
from calidad.models.caracteristicacontrolModel import caracteristicacontrol_Model 
from calidad.functions import caracteristicacontrolFunction 
 
 
ruta = APIRouter( 
    prefix="/caracteristicacontrol",tags=["CaracteristicaControl"] 
) 
 
@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK) 
def showFilter(txtFind: str, db: Session = Depends(get_db)): 
    data = db.query(caracteristicacontrol_Model).filter(caracteristicacontrol_Model.carcon_nombre.contains(txtFind.strip())).all() 
    return data 
 
@ruta.post('/') 
async def grabar(info: caracteristicacontrol_Schema): 
    # resultado = await info.json() 
    return caracteristicacontrolFunction.insert(info) 
 
@ruta.put('/') 
async def actualizar(info: caracteristicacontrol_Schema): 
    return caracteristicacontrolFunction.update(info) 
 
@ruta.delete('/') 
async def eliminar(info: caracteristicacontrol_Schema): 
    return caracteristicacontrolFunction.delete(info)

@ruta.get('/listaxarticulo/{pArticulo}')
async def listaxarticulo(pArticulo:str):
    print(pArticulo)
    resultado = caracteristicacontrolFunction.ListaCarConxArticulo(pArticulo)
    return resultado