from fastapi import APIRouter, Depends, status, UploadFile, File, Request 
from fastapi.responses import FileResponse 
from sqlalchemy.orm.session import Session 
 
from conexiones.conexion_sql import get_db 
from calidad.schemas.familiapiezasSchema import familiapiezas_Schema 
from calidad.models.familiapiezasModel import familiapiezas_Model 
from calidad.functions import familiapiezasFunction 
 
 
ruta = APIRouter( 
    prefix="/familiapiezas",tags=["FamiliaPiezas"] 
) 
 
@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK) 
def showFilter(txtFind: str, db: Session = Depends(get_db)): 
    data = db.query(familiapiezas_Model).filter(familiapiezas_Model.fampie_nombre.contains(txtFind.strip())).all() 
    return data 
 
@ruta.post('/') 
async def grabar(info: familiapiezas_Schema): 
    # resultado = await info.json() 
    return familiapiezasFunction.insert(info) 
 
@ruta.put('/') 
async def actualizar(info: familiapiezas_Schema): 
    return familiapiezasFunction.update(info) 
 
@ruta.delete('/') 
async def eliminar(info: familiapiezas_Schema): 
    return familiapiezasFunction.delete(info)