from fastapi import APIRouter, Depends, status, UploadFile, File, Request

from calidad.schemas.formulasSchema import Valores
from calidad.schemas.propiedadesarticulo import PropiedadesFiltro
from calidad.functions import formulasFunction,articulospropiedadesFunctions


ruta = APIRouter(
    prefix="/formulas",tags=["Formulas"]
)

@ruta.post('/formula_torno')
async def formula_torno(valores:Valores):
    
    resultado = formulasFunction.calcular_formulas_torno(valores)
    
    return resultado

@ruta.post('/propiedadesarticulo')
async def propiedadesarticulo(filtro:PropiedadesFiltro):

    resultado = articulospropiedadesFunctions.Extraer(filtro.cod_articulo,filtro.carcon_codigo)

    return resultado