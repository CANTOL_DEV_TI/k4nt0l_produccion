from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from calidad.functions import inspectorFunction

ruta = APIRouter(
    prefix="/inspector",tags=["Inspector"]
)

@ruta.get('/ValidaInspector/{pInspector}')
async def ValidaInspector(pInspector : str):
    return inspectorFunction.InspectorValida(pInspector)