from fastapi import APIRouter, Depends, status, UploadFile, File, Request 
from fastapi.responses import FileResponse 
from sqlalchemy.orm.session import Session
 
from conexiones.conexion_sql import get_db 
from calidad.models.inspeccionModel import inspeccion_Model 
from calidad.schemas.inspeccionSchema import Inspeccion_Cab_Schema 
from calidad.functions import inspeccionFunction 
 
 
ruta = APIRouter( 
    prefix="/inspeccion",tags=["Inspeccion"] 
) 
 
@ruta.get('/{pDesde} - {pHasta} - {pArticulo}', status_code=status.HTTP_200_OK) 
def showFilter(pDesde: str, pHasta : str , pArticulo: str, db: Session = Depends(get_db)): 
    #data = db.query(inspeccion_Model).filter(inspeccion_Model.fecha_inspeccion.between(pDesde,pHasta) ).all()     
    data = inspeccionFunction.listaInspeccion(pDesde,pHasta,pArticulo)
    return data 
 
 
@ruta.post('/') 
async def grabar(info: Inspeccion_Cab_Schema): 
    # resultado = await info.json() 
    return inspeccionFunction.insert(info) 
 
@ruta.put('/') 
async def actualizar(info: Inspeccion_Cab_Schema): 
    return inspeccionFunction.update(info) 
 
@ruta.delete('/') 
async def eliminar(info: Inspeccion_Cab_Schema): 
    return inspeccionFunction.delete(info) 
 
@ruta.get('/ListaFamiliaPiezas') 
async def listaFamiliaPiezas(): 
    resultado = inspeccionFunction.listaFamiliaPiezas() 
    return resultado 
 
@ruta.get('/ListaCaracteristicaControl') 
async def listaCaracteristicaControl(): 
    resultado = inspeccionFunction.listaCaracteristicaControl() 
    return resultado 
 
@ruta.get('/ListaTipoCaracteristica') 
async def listaTipoCaracteristica(): 
    resultado = inspeccionFunction.listaTipoCaracteristica() 
    return resultado 
 
@ruta.get('/ListaArticulos/{tNombre}') 
async def listaArticulos(tNombre : str): 
    resultado = inspeccionFunction.listaArticulos(tNombre) 
    return resultado

@ruta.get('/ListaDetalles/{pID}')
async def listaDetalles(pID : str):
    resultado = inspeccionFunction.DetalleInspeccion(pID)
    return resultado

@ruta.get('/listaEquiposxArticulo/{pArticulo}')
async def listaEquiposxArticulo(pArticulo : str):
    resultado = inspeccionFunction.ListaEquiposxArticulo(pArticulo)
    return resultado