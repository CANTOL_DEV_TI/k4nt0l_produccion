from fastapi import APIRouter, Depends, status, UploadFile, File, Request 
from fastapi.responses import FileResponse 
from sqlalchemy.orm.session import Session 
 
from conexiones.conexion_sql import get_db 
from calidad.schemas.articulospropiedadesSchema import articulospropiedades_Schema 
from calidad.functions import articulospropiedadesFunctions 
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
 
 
ruta = APIRouter( 
    prefix="/articulospropiedades",tags=["Articulos Propiedades"] 
) 

@ruta.get("/{pArticulo}",status_code=status.HTTP_200_OK)
async def ListaCarConxArticulo(pArticulo:str):    
    cad_sql = "select cpa.cod_articulo,articulo,cpa.carcon_codigo , carcon_nombre,LIE,LIC,OBJ,LSC,LSE from cali_propiedades_articulo cpa (nolock)"
    cad_sql += f" inner join cali_caracteristica_control ccc (nolock) on cpa.carcon_codigo = ccc.carcon_codigo"
    cad_sql += f" inner join articulo_familia af (nolock) on af.cod_articulo = cpa.cod_articulo where af.articulo like '%{pArticulo}%'"
    
    conn = ConectaSQL_Produccion()
    cur_val_carcon = conn.cursor(as_dict = True)

    cur_val_carcon.execute(cad_sql)

    car_con = []

    for row in cur_val_carcon:
       fila = {"cod_articulo" : row['cod_articulo'],"articulo" : row['articulo'] ,"carcon_codigo" : row['carcon_codigo'],"carcon_nombre" : row['carcon_nombre'],"LIE":row['LIE'],"LIC":row['LIC'],"OBJ":row['OBJ'],"LSC":row['LSC'],"LSE":row['LSE']}
       car_con.append(fila)    
                                                      
    conn.close()

    return car_con

@ruta.post('/')
async def Grabar(info:articulospropiedades_Schema):
    return articulospropiedadesFunctions.insert(info)

@ruta.put('/')
async def Actualizar(info:articulospropiedades_Schema):
    return articulospropiedadesFunctions.update(info)

@ruta.delete('/')
async def Eliminar(info:articulospropiedades_Schema):
    return articulospropiedadesFunctions.delete(info)