from fastapi import APIRouter, Depends, status, UploadFile, File, Request 
from fastapi.responses import FileResponse 
from sqlalchemy.orm.session import Session 
 
from conexiones.conexion_sql import get_db 
from calidad.schemas.tipocaracteristicasSchema import tipocaracteristica_Schema 
from calidad.models.tipocaracteristicasModel import tipocaracteristica_Model 
from calidad.functions import tipocaracteristicaFunction 
 
 
ruta = APIRouter( 
    prefix="/tipocaracteristica",tags=["TipoCaracteristica"] 
) 
 
@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK) 
def showFilter(txtFind: str, db: Session = Depends(get_db)): 
    data = db.query(tipocaracteristica_Model).filter(tipocaracteristica_Model.tipcar_nombre.contains(txtFind.strip())).all() 
    return data 
 
@ruta.post('/') 
async def grabar(info: tipocaracteristica_Schema): 
    # resultado = await info.json() 
    return tipocaracteristicaFunction.insert(info) 
 
@ruta.put('/') 
async def actualizar(info: tipocaracteristica_Schema): 
    return tipocaracteristicaFunction.update(info) 
 
@ruta.delete('/') 
async def eliminar(info: tipocaracteristica_Schema): 
    return tipocaracteristicaFunction.delete(info)