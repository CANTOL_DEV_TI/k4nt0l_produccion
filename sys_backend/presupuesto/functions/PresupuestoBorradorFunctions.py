from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from conexiones.conexion_sap_solo import conexion_SAP_Tecno
#from generales.enviomensajes import EnviarMensaje
from presupuesto.schemas.presupuestoSchema import CabeceraPresupuesto_Schema
from presupuesto.schemas.borradorSchema import CabeceraBorrador_Schema
from presupuesto.schemas.importacionSchema import import_detalles
from presupuesto.schemas.filtrospresupuestoSchema import filtroPresupuestoxUsuario
from typing import List

def AddPresupuesto (info:CabeceraBorrador_Schema):
    try:       
        Codigo = GeneraCodigoBorrador(info.cia,info.año,info.usuario,info.cc)
        
        #print(Codigo)

        fechaini = str(info.año) + '0101'
        fechafin = str(info.año) + '1231'
        montoT = 0

        for filaT in info.detalles:
            montoT = montoT + filaT.enero + filaT.febrero + filaT.marzo + filaT.abril + filaT.mayo + filaT.junio + filaT.julio + filaT.agosto + filaT.setiembre + filaT.octubre + filaT.noviembre + filaT.diciembre

        sqlCabIns = "insert into presu_cabecera(Cia,Code,Name,Object,U_MSS_EJERCI,U_MSS_MONEDA,U_MSS_ACTIVO,U_MSS_FEINI,U_MSS_FEFIN,U_MSS_USUA,U_MSS_TOP,Estado,fecha_creacion) values ("
        sqlCabIns += f"'{info.cia}','{Codigo}','{info.code}','PRESUPUESTOS',{info.año},'{info.moneda}','A',"
        sqlCabIns += f"'{fechaini}','{fechafin}','{info.usuario}',{montoT},'B',getdate());"
        #print(sqlCabIns)
        conn = ConectaSQL_Produccion()

        cur_inserta = conn.cursor()

        cur_inserta.execute(sqlCabIns)
        
        fila = 1
        for filaG in info.detalles:
            montolinea = filaG.enero + filaG.febrero + filaG.marzo + filaG.abril + filaG.mayo + filaG.junio + filaG.julio + filaG.agosto + filaG.setiembre + filaG.octubre + filaG.noviembre + filaG.diciembre
            
            sqlDetIns = "insert into presu_detalle(Cia,Code,LineId,Object,U_MSS_CUENTA,U_MSS_DESC,U_MSS_CC1,U_MSS_CC2,U_MSS_TIPO,U_MSS_ENE,U_MSS_FEB,U_MSS_MAR,U_MSS_ABR,"
            sqlDetIns += "U_MSS_MAY,U_MSS_JUN,U_MSS_JUL,U_MSS_AGO,U_MSS_SEP,U_MSS_OCT,U_MSS_NOV,U_MSS_DIC,U_MSS_TOTANU,U_MSS_NC1,U_MSS_NC2,U_MSS_CAPRE,U_MSS_DETCA,U_MSS_CON"
            sqlDetIns += f") values('{info.cia}','{Codigo}',{fila},'PRESUPUESTOS','{filaG.cuenta}','{filaG.cuenta_desc}','{filaG.cc1}','{filaG.cc2}',"
            sqlDetIns += f"'{filaG.tipo}',{filaG.enero},{filaG.febrero},{filaG.marzo},{filaG.abril},{filaG.mayo},{filaG.junio},{filaG.julio},"
            sqlDetIns += f"{filaG.agosto},{filaG.setiembre},{filaG.octubre},{filaG.noviembre},{filaG.diciembre},{montolinea},'{filaG.ccosto_desc}',"
            sqlDetIns += f"'{filaG.ccosto_desc}','{filaG.catpre}','{filaG.catpre_desc}','{filaG.concepto}');"
            fila = fila + 1
        
            cur_inserta.execute(sqlDetIns)

        conn.commit()

        conn.close()

        #EnviarMensaje(info.usuario,f"Se registro correctamente su presupuesto con codigo {Codigo}.")

        mensaje = {"Resultado" : 0, "Mensaje" : "Registro borrador de presupuesto ingresado"}

        return mensaje
    
    except Exception as err:
        mensajeError = {"Resultado": -1, "Mensaje" : f"Error a la hora de insertar - {err}" }
        return mensajeError

def UpdPresupuesto (info : CabeceraBorrador_Schema):
    try:
        print("Actualizar Borrador...")
        fechaini = str(info.año) + '0101'
        fechafin = str(info.año) + '1231'
        montoT = 0
        filasC = 0        
        sVersion = ""

        for filaT in info.detalles:
            montoT = montoT + filaT.enero + filaT.febrero + filaT.marzo + filaT.abril + filaT.mayo + filaT.junio + filaT.julio + filaT.agosto + filaT.setiembre + filaT.octubre + filaT.noviembre + filaT.diciembre
            filasC = filasC + 1

        sVersion = GuardarVersionAnterior(info.cia,info.code,montoT,filasC,info.detalles)

        print(sVersion)

        sqlCabIns = f"update presu_cabecera set Name='{info.code}',U_MSS_EJERCI={info.año},U_MSS_MONEDA='{info.moneda}', estado='B',"
        sqlCabIns += f"U_MSS_ACTIVO='A',U_MSS_FEINI='{fechaini}',U_MSS_FEFIN='{fechafin}',U_MSS_USUA='{info.usuario}', usuario_edicion='{info.usuario_edicion}',fecha_edicion=getdate() "
        sqlCabIns += f",U_MSS_TOP={montoT} where Code='{info.code}' and cia='{info.cia}'"
        
        sqlDelDet = f"delete from presu_detalle where Code='{info.code}' and cia='{info.cia}'"
        
        conn = ConectaSQL_Produccion()

        cur_actualiza = conn.cursor()

        cur_actualiza.execute(sqlCabIns)

        cur_actualiza.execute(sqlDelDet)

        fila = 1

        for filaG in info.detalles:
            
            montolinea = filaG.enero + filaG.febrero + filaG.marzo + filaG.abril + filaG.mayo + filaG.junio + filaG.julio + filaG.agosto + filaG.setiembre + filaG.octubre + filaG.noviembre + filaG.diciembre
            
            sqlDetIns = "insert into presu_detalle(Cia,Code,LineId,Object,U_MSS_CUENTA,U_MSS_DESC,U_MSS_CC1,U_MSS_CC2,U_MSS_TIPO,U_MSS_ENE,U_MSS_FEB,U_MSS_MAR,U_MSS_ABR,"
            sqlDetIns += "U_MSS_MAY,U_MSS_JUN,U_MSS_JUL,U_MSS_AGO,U_MSS_SEP,U_MSS_OCT,U_MSS_NOV,U_MSS_DIC,U_MSS_TOTANU,U_MSS_NC1,U_MSS_NC2,U_MSS_CAPRE,U_MSS_DETCA,U_MSS_CON"
            sqlDetIns += f") values('{info.cia}','{info.code}',{fila},'PRESUPUESTOS','{filaG.cuenta}','{filaG.cuenta_desc}','{filaG.cc1}','{filaG.cc2}',"
            sqlDetIns += f"'{filaG.tipo}',{filaG.enero},{filaG.febrero},{filaG.marzo},{filaG.abril},{filaG.mayo},{filaG.junio},{filaG.julio},"
            sqlDetIns += f"{filaG.agosto},{filaG.setiembre},{filaG.octubre},{filaG.noviembre},{filaG.diciembre},{montolinea},'{filaG.ccosto_desc}',"
            sqlDetIns += f"'{filaG.ccosto_desc}','{filaG.catpre}','{filaG.catpre_desc}','{filaG.concepto}');"
            fila = fila + 1
            #print(sqlDetIns)
            cur_actualiza.execute(sqlDetIns)

        mensaje = {"Resultado" : 0, "Mensaje" : "Registro borrador de presupuesto actualizado"}

        conn.commit()

        conn.close()

        return mensaje
    
    except Exception as err:
        mensajeError = {"Resultado": -1, "Mensaje" : f"Error a la hora de actualizar - {err}" }
        return mensajeError

def DelPresupuesto(info:CabeceraBorrador_Schema):
    try:
        
        sqlCabDel = f"delete from presu_cabecera where Code = '{info.code}' and cia='{info.cia}'"
        sqlDelDet = f"delete from presu_detalle where Code='{info.code}' and cia='{info.cia}'"

        #print(sqlDelDet)
        #print(sqlCabDel)

        conn = ConectaSQL_Produccion()

        cur_borra = conn.cursor(as_dict = True)

        cur_borra.execute(sqlDelDet)

        cur_borra.execute(sqlCabDel)

        conn.commit()
        
        mensaje = {"Resultado" : 0, "Mensaje" : "Registro borrador de presupuesto eliminado"}

        conn.close()

        return mensaje
    
    except Exception as err:
        mensajeError = {"Resultado": -1, "Mensaje" : err }
        print(mensajeError)
        return mensajeError

def ManejaPresupuesto(info:CabeceraBorrador_Schema):
    try:
        #print(info)
        sqlPreUpd = f"update presu_cabecera set estado='{info.estado}', "
        
        if(info.estado == 'A'):
            sqlPreUpd += f"usuario_aprobacion='{info.usuario_aprueba}', fecha_aprobacion=getdate() "
        
        if(info.estado == 'R'):
            sqlPreUpd += f"usuario_rechazo='{info.usuario_rechaza}', fecha_rechazo=getdate() "
        
        sqlPreUpd += f"where Code='{info.code}' and Cia='{info.cia}'"

        conn = ConectaSQL_Produccion()

        cur_aprueba = conn.cursor(as_dict = True)

        cur_aprueba.execute(sqlPreUpd)

        conn.commit()

        mensaje = {"Resultado" : 0, "Mensaje" : "Registro borrador de presupuesto Aprubado"}

        conn.close()

        return mensaje
    
    except Exception as err:
        mensajeError = {"Resultado" : -1, "Mensaje" : err}
        return mensajeError

def ListaPresupuestosBorradores(filtro:filtroPresupuestoxUsuario):
    try:
        consulta = []

        sqlConsulta = "select pc.Cia as Cia, pc.Code as Codigo, U_MSS_EJERCI as Año, U_MSS_MONEDA as Moneda,Estado,"
        sqlConsulta += "case Estado when 'A' then 'Aprobado' when 'R' then 'Rechazado' when 'B' then 'Borrador' when 'M' then 'Migrado' end as Estado_d , U_MSS_USUA as Usuario,"
        sqlConsulta += "sum (pd.U_MSS_ENE + pd.U_MSS_FEB + pd.U_MSS_MAR + pd.U_MSS_ABR + pd.U_MSS_MAY + pd.U_MSS_JUN + pd.U_MSS_JUL + pd.U_MSS_AGO + pd.U_MSS_SEP + "
        sqlConsulta += "pd.U_MSS_OCT + pd.U_MSS_NOV + pd.U_MSS_DIC) as Total_Presupuesto from presu_cabecera pc inner join presu_detalle pd on pc.Code  = pd.Code "
        sqlConsulta += f" and pc.Cia = pd.Cia where pc.U_MSS_USUA = '{filtro.Usuario}' and pc.Cia='{filtro.Compañia}' group by pc.Cia, pc.Code, U_MSS_EJERCI, U_MSS_MONEDA,Estado,"
        sqlConsulta += "case Estado when 'A' then 'Aprobado' when 'R' then 'Rechazado' when 'B' then 'Borrador' when 'M' then 'Migrado' end,U_MSS_USUA"
        #print(sqlConsulta)
        conn = ConectaSQL_Produccion()

        cur_consulta = conn.cursor(as_dict = True)

        cur_consulta.execute(sqlConsulta)

        consulta = cur_consulta.fetchall()

        conn.close()
        #print("@$"*10)
        #print(sqlConsulta)

        return consulta
    
    except Exception as err:
        mensajeError = {"Resultado" : -1, "Mensaje" : err}
        return mensajeError

def ListaDetalleBorradores (pCia:str,pCodigo:str):
    try:
        consulta = []

        sqlDetConsulta = "select pc.Cia as Cia, pc.code as Codigo, lineid as lineaid,  U_MSS_CUENTA as cuenta, U_MSS_DESC as cuenta_desc,U_MSS_CC1 as cc1, U_MSS_CC2 as cc2, U_MSS_CAPRE as catpre, U_MSS_DETCA as catpre_desc, U_MSS_NC2 as ccosto_desc,U_MSS_CON as concepto,"
        sqlDetConsulta += "U_MSS_TIPO as tipo,round(U_MSS_ENE,2) as enero,round(U_MSS_FEB,2) as febrero,round(U_MSS_MAR,2) as marzo,round(U_MSS_ABR,2) as abril,round(U_MSS_MAY,2) as mayo,round(U_MSS_JUN,2) as junio,"
        sqlDetConsulta += "round(U_MSS_JUL,2) as julio,round(U_MSS_AGO,2) as agosto,round(U_MSS_SEP,2) as setiembre,round(U_MSS_OCT,2) as octubre,round(U_MSS_NOV,2) as noviembre,round(U_MSS_DIC,2) as diciembre "
        sqlDetConsulta += f"from presu_cabecera pc inner join presu_detalle pd on pc.Code = pd.Code  and pc.Cia = pd.Cia where pc.Code = '{pCodigo}' and pc.Cia = '{pCia}'"
        #print(sqlDetConsulta)
        conn = ConectaSQL_Produccion()

        cur_consulta = conn.cursor(as_dict = True)

        cur_consulta.execute(sqlDetConsulta)

        consulta = cur_consulta.fetchall()

        conn.close()

        return consulta
    
    except Exception as err:
        mensajeError = {"Resultado" : -1, "Mensaje" : err}
        return mensajeError

def GeneraCodigoBorrador(pCia,pAño,pUser,pCC):
    try:
        sqlGetCC = f"select cc_desc as cc_desc from presu_cc_usuario pcu where usuario='{pUser}' and cia='{pCia}' and centro_costo='{pCC}'"
        #print(sqlGetCC)
        conn = ConectaSQL_Produccion()

        cur_get = conn.cursor(as_dict = True)

        cur_get.execute(sqlGetCC)

        CC_Desc = cur_get.fetchall()
        
        vDesc = ""
                
        for res in CC_Desc:
            vDesc = res["cc_desc"]
        
        sqlGenCod = f"select CONCAT('{pAño}-',cast(count(*) + 1 as varchar(3)),'-{ vDesc }') as Codigo "
        sqlGenCod += f"from presu_cabecera pc (nolock) where U_MSS_EJERCI = {pAño} and Cia = '{pCia}' and Code like '%{vDesc}%'"
        #sqlGenCod += f" and Codigo = CONCAT('{pAño}-',cast(count(*) + 1 as varchar(3)),'-{ vDesc }') U_MSS_USUA = '{pUser}' and"
        #print(sqlGenCod)
        cur_gen = conn.cursor(as_dict = True)

        cur_gen.execute(sqlGenCod)

        Resultado = cur_gen.fetchall()

        Codigo = ""

        for resf in Resultado:
            Codigo = resf["Codigo"]

        return Codigo
    except Exception as err:
        mensajeError = {"Resultado" : -1, "Mensaje" : err}
        return mensajeError

def BandejaBorradores(pCia :str, pCode: str, pAño:int, pUsuario:str):
    try:        
        #print(pCode)
        #print(pAño)
        #print(pUsuario)

        if(pCode=="undefined" or pCode=="20%"):
            pCode=""
        
        if(pUsuario=="undefined" or pUsuario=="20%"):
            pUsuario=""

        sqlBandeja = "select distinct pc.Cia as Cia, pc.Code,pc.U_MSS_MONEDA as Moneda, pc.U_MSS_EJERCI as Año, format(pc.U_MSS_TOP,'N2') as Total_Presupuesto,"
        sqlBandeja += "U_MSS_USUA as Usuario, Estado as Estado, usuario_aprobacion as Aprobado_por, format(fecha_aprobacion,'dd/MM/yyyy') as Aprobado_el, usuario_edicion as Editado_por, format(fecha_edicion,'dd/MM/yyyy') as Editado_el,usuario_rechazo as Rechazado_por,format(fecha_rechazo,'dd/MM/yyyy') as Rechazado_el, "
        sqlBandeja += "case pc.Estado when 'B' then 'Borrador' when 'A' then 'Aprobado' when 'R' then 'Rechazado' when 'E' then 'Editado' when 'M' then 'Migrado' end as Estado_d "
        sqlBandeja += "from presu_cabecera pc inner join presu_detalle pd on pc.Cia = pd.Cia and pc.Code = pd.Code "
        sqlBandeja += f"where pc.Cia = '{pCia}' and U_MSS_EJERCI = {pAño} and pd.Code like '%{pCode}%' and pc.U_MSS_USUA like '%{pUsuario}%'"
        #print(sqlBandeja)
        conn = ConectaSQL_Produccion()

        cur_bandeja = conn.cursor(as_dict = True)

        cur_bandeja.execute(sqlBandeja)

        resultado = cur_bandeja.fetchall()

        return resultado
    except Exception as err:
        mensajeError = {"Resultado" : -1, "Mensaje" : err}
        return mensajeError

def ExtraerValoresAnteriores(pCia:str,pCode : str):
    try:
        sqlA = "select Cia,Code,U_MSS_EJERCI,U_MSS_MONEDA,U_MSS_ACTIVO,U_MSS_FEINI,U_MSS_FEFIN,U_MSS_USUA,U_MSS_TOP,Estado,"
        sqlA += "fecha_aprobacion,usuario_edicion,fecha_rechazo,usuario_rechazo,fecha_edicion,fecha_creacion "
        sqlA += f"from presu_cabecera where Cia = '{pCia}' and Code='{pCode}'"
        
        conn = ConectaSQL_Produccion()

        cur_datos = conn.cursor(as_dict = True)

        cur_datos.execute(sqlA)

        sResultados = cur_datos.fetchall()

        conn.close()

        return sResultados
    except Exception as err:
        return {"error":err}

def ExtraeCantFilasAnteriores(pCia,pCode):
    try:
        sqlA = f"select count(*) as Cant_Filas from presu_detalle where Cia = '{pCia}' and Code='{pCode}'"
        
        conn = ConectaSQL_Produccion()

        cur_datos = conn.cursor(as_dict = True)

        cur_datos.execute(sqlA)

        sResultados = cur_datos.fetchall()
        
        conn.close()
        
        return sResultados
    
    except Exception as err:
        return {"error":err}

def ExtraerDatosFilasAnteriores(pCia,pCode):
    try:

        sqlA = f"select Cia,Code,LineId,U_MSS_CUENTA,U_MSS_CC2,U_MSS_CAPRE,U_MSS_ENE,U_MSS_FEB,U_MSS_MAR,U_MSS_ABR,U_MSS_MAY,U_MSS_JUN,U_MSS_JUL,U_MSS_AGO,U_MSS_SEP,U_MSS_OCT,U_MSS_NOV,U_MSS_DIC from presu_detalle where Cia = '{pCia}' and Code='{pCode}'"
        
        conn = ConectaSQL_Produccion()

        cur_datos = conn.cursor(as_dict = True)

        cur_datos.execute(sqlA)

        sResultados = cur_datos.fetchall()

        conn.close()
        
        return sResultados
    except Exception as err:
        return {"error":err}

def GuardarVersionAnterior(pCia : str, pCode : str, pMonto : float, pFilas : int, pDet ):
    try:
        print("evaluar cambios...")
        sCambio = "no"
        sResultado = ""

        print("Backup Valores Anteriores...")
        dOriginal = ExtraerValoresAnteriores(pCia,pCode) 
        #print(dOriginal)
        print("---------")

        print("Extraer Cant Filas anteriores...")
        dCantFilas = ExtraeCantFilasAnteriores(pCia,pCode)
        #print(dCantFilas)
        print("---------")

        print("Extraer Filas de detalle anteriores...")
        dDetalle = ExtraerDatosFilasAnteriores(pCia,pCode)
        #print(dDetalle)
        print("---------")

        if(pMonto!=0):
            sCambio = "si"

        #print(dOriginal)
        if(pMonto!=dOriginal[0]['U_MSS_TOP']):
            sCambio = "si"
        
        #print(dCantFilas)
        if(pFilas!=dCantFilas[0]['Cant_Filas']):
            sCambio = "si"
        else:
            for i in dDetalle:
                #print(i)
                for f in pDet:
                    #print(f)
                    if(i['U_MSS_CUENTA']==f['U_MSS_CUENTA']) and (i['U_MSS_CC2']==f['U_MSS_CC2']) and (i['U_MSS_CAPRE']==f['U_MSS_CAPRE']):
                        if(i['U_MSS_ENE']!=f['enero']):
                            sCambio = "si"
                        
                        if(i['U_MSS_FEB']!=f['febrero']):
                            sCambio = "si"

                        if(i['U_MSS_MAR']!=f['marzo']):
                            sCambio = "si"
                        
                        if(i['U_MSS_ABR']!=f['abril']):
                            sCambio = "si"
                        
                        if(i['U_MSS_MAY']!=f['mayo']):
                            sCambio = "si"

                        if(i['U_MSS_JUN']!=f['junio']):
                            sCambio = "si"
                        
                        if(i['U_MSS_JUL']!=f['julio']):
                            sCambio = "si"
                        
                        if(i['U_MSS_AGO']!=f['agosto']):
                            sCambio = "si"

                        if(i['U_MSS_SEP']!=f['septiembre']):
                            sCambio = "si"

                        if(i['U_MSS_OCT']!=f['octubre']):
                            sCambio = "si"

                        if(i['U_MSS_NOV']!=f['noviembre']):
                            sCambio = "si"

                        if(i['U_MSS_DIC']!=f['diciembre']):
                            sCambio = "si"                    
                    
        if(sCambio=="si"):
            
            conn = ConectaSQL_Produccion()
            sLlave = ""
            cur_version = conn.cursor(as_dict = True)
            sqlkey = "select concat(CONVERT(varchar(10),GETDATE(),112),' ' ,LEFT(CONVERT(varchar(10),GETDATE(),108),5)) as ID"
            cur_version.execute(sqlkey)

            for i in cur_version:
                sLlave = i["ID"]

            #print(sLlave)

            sqlversion = "insert into presu_cabecera_historial(Cia ,Code ,Name,	[Object] ,U_MSS_EJERCI ,U_MSS_MONEDA,U_MSS_ACTIVO ,U_MSS_FEINI,U_MSS_FEFIN,U_MSS_USUA ,U_MSS_TOP,"
            sqlversion += "Estado ,fecha_aprobacion ,usuario_aprobacion ,usuario_edicion ,fecha_rechazo ,usuario_rechazo ,fecha_edicion ,fecha_creacion,fecha_version) "
            sqlversion += "select 	Cia ,Code ,Name,[Object] ,U_MSS_EJERCI ,U_MSS_MONEDA,U_MSS_ACTIVO ,U_MSS_FEINI,U_MSS_FEFIN,U_MSS_USUA ,U_MSS_TOP,"
            sqlversion += "Estado ,fecha_aprobacion ,usuario_aprobacion ,usuario_edicion ,fecha_rechazo , usuario_rechazo ,fecha_edicion ,fecha_creacion,"
            sqlversion += f"'{sLlave}' as fecha_version "
            sqlversion += f"from presu_cabecera where Cia = '{pCia}' and Code = '{pCode}'"

            #print(sqlversion)
            
            cur_version.execute(sqlversion)

            sqlversiond = "insert into presu_detalle_historial (Cia,Code,LineId,U_MSS_CUENTA,U_MSS_DESC,U_MSS_CC1,U_MSS_CC2,U_MSS_TIPO,U_MSS_ENE,U_MSS_FEB,"
            sqlversiond += "U_MSS_MAR,U_MSS_ABR,U_MSS_MAY,U_MSS_JUN,U_MSS_JUL,U_MSS_AGO,U_MSS_SEP,U_MSS_OCT,U_MSS_NOV,U_MSS_DIC,U_MSS_TOTANU,U_MSS_NC1,U_MSS_NC2,"
            sqlversiond += "U_MSS_CON,U_MSS_CAPRE,U_MSS_DETCA,fecha_version) "
            sqlversiond += "select Cia,Code,LineId,U_MSS_CUENTA,U_MSS_DESC,U_MSS_CC1,U_MSS_CC2,U_MSS_TIPO,U_MSS_ENE,U_MSS_FEB,U_MSS_MAR,U_MSS_ABR,U_MSS_MAY,U_MSS_JUN,"
            sqlversiond += "U_MSS_JUL,U_MSS_AGO,U_MSS_SEP,U_MSS_OCT,U_MSS_NOV,U_MSS_DIC,U_MSS_TOTANU,U_MSS_NC1,U_MSS_NC2,U_MSS_CON,U_MSS_CAPRE,U_MSS_DETCA, "
            sqlversiond += f"'{sLlave}' as fecha_version "
            sqlversiond += f"from presu_detalle where cia='{pCia}' and code='{pCode}'"

            #print(sqlversiond)

            cur_version.execute(sqlversiond)

            conn.commit()
            
            conn.close()

            sResultado = {"mensaje":"se guardo la version en el historial"}
        else: 
            sResultado = {"mensaje":"no hubo cambios, no se genero version en el historial"}

        #print(sResultado)
        print("termino de evaluar cambios...")

        return sResultado    
    except Exception as err:
        #print(err)
        return {"Resultado" : -1, "Mensaje" : f"No se pudo guardar la version previa del borrador : {err}"}

def VerHistorialPresupuesto(pCia :str, pCode : str):

    try:        
        sqlH = "select fecha_version, Cia,Code,Name,U_MSS_EJERCI,U_MSS_MONEDA,U_MSS_ACTIVO,U_MSS_FEINI,U_MSS_FEFIN,U_MSS_USUA,"
        sqlH += f"U_MSS_TOP,Estado,fecha_creacion,fecha_edicion,usuario_edicion from presu_cabecera_historial where cia = '{pCia}' and Code='{pCode}'"
        
        conn = ConectaSQL_Produccion()

        cur_historial = conn.cursor(as_dict = True)

        cur_historial.execute(sqlH)

        sResultados = cur_historial.fetchall()

        return sResultados
    except Exception as err:
        return {"error en el historial":err}

def ValidarCuenta(pCia:str, pCuenta:str):
    try:
        sCia = ""        
        if(pCia == "CNT"):
            sCia = "SBO_CANTOL_PRODUCCION"
        
        if(pCia == "DTM"):
            sCia = "SBO_DISTRI_PRODUCCION"
        
        if(pCia == "TCN"):            
            sCia = "SBO_TECNO_PRODUCCION"

        sSqlCC = f"select count(*) as Validar,\"AcctName\" as NOMBRE from {sCia}.oact where \"AcctCode\" = '{pCuenta}' group by \"AcctName\" "
        #print(sSqlCC)
        conn = conexion_SAP_Tecno()

        cur_val_cta = conn.cursor()

        cur_val_cta.execute(sSqlCC)

        valida = -1
        nombrecc = ""
        for re in cur_val_cta:
            valida  = re["Validar"]
            nombrecc = re["NOMBRE"]

        conn.close()

        return {"codigo" : valida, "mensaje" : nombrecc}
    except Exception as err:
        #print(err)
        return {"codigo": -1, "mensaje" : "No existe cuenta contable en la cia"}

def ValidarCentroCosto(pCia:str,pCodigoCC:str):
    try:
        sCia = ""        
        if(pCia == "CNT"):
            sCia = "SBO_CANTOL_PRODUCCION"
        
        if(pCia == "DTM"):
            sCia = "SBO_DISTRI_PRODUCCION"
        
        if(pCia == "TCN"):            
            sCia = "SBO_TECNO_PRODUCCION"

        sSqlCC = f"select count(*) as Validar,\"PrcName\" as NOMBRE,LEFT(\"PrcCode\",5) AS CC1 from {sCia}.OPRC where \"PrcCode\" = '{pCodigoCC}' group by \"PrcName\",LEFT(\"PrcCode\",5) "
        #print(sSqlCC)
        conn = conexion_SAP_Tecno()

        cur_val_cta = conn.cursor()

        cur_val_cta.execute(sSqlCC)

        valida = -1
        nombrecc = ""
        cc1 = ""

        for re in cur_val_cta:
            valida  = re["Validar"]
            nombrecc = re["NOMBRE"]
            cc1 = re["CC1"]
        #print({"codigo" : valida, "mensaje" : nombrecc, "cc1" : cc1})
        return {"codigo" : valida, "mensaje" : nombrecc, "cc1" : cc1}
    except Exception as err:
        return {"codigo": -1, "mensaje" : "No existe cuenta contable en la cia"}
    
def ValidarCategorias(pCia:str, pCateCod:str, pCateNom:str, pCta:str):
    try:
        valida = -1        
        sCia = ""  

        if(pCia == "CNT"):
            sCia = "SBO_CANTOL_PRODUCCION"
        
        if(pCia == "DTM"):
            sCia = "SBO_DISTRI_PRODUCCION"
        
        if(pCia == "TCN"):            
            sCia = "SBO_TECNO_PRODUCCION"



        sSqlCC = f"SELECT count(*) AS Validar,\"Code\" as Code,\"Name\" as Name,U_MSS_CUCON FROM {sCia}.\"@MSS_CATPRE\" " 
        sSqlCC += f"WHERE \"Code\" = '{pCateCod.rjust(5,'0')}' AND UPPER(\"Name\")='{pCateNom.strip()}' AND U_MSS_CUCON = '{pCta}' GROUP BY \"Code\",\"Name\",U_MSS_CUCON"
        #print(sSqlCC)
        conn = conexion_SAP_Tecno()

        cur_val_cta = conn.cursor()

        cur_val_cta.execute(sSqlCC)

        for re in cur_val_cta:
            valida = re["Validar"]

        return {"codigo" : valida, "mensaje" : "No existe o no esta asociada a la cuenta esta categoria presupuestal"}
    except Exception as err:
        return {"codigo": -1,"mensaje" : "No existe la categoria presupuestal"}

def ValidarImportacion(pData):
    try:
        lResultado = []
        dLinea = {}
        sMensajeTotal = ""
        sCC = ""
        sCta = ""
        sLinea = ""

        for fila in pData.detalles:            
            sCia = fila.cia
            sCta = fila.cuenta
            sCC = fila.cc2
            sLinea = fila.lineaid
            sCatCod = fila.catpre
            sCatNom = fila.catpre_desc

            vValCC = ValidarCentroCosto(sCia, sCC)
            vValCta = ValidarCuenta(sCia,sCta)
            vValCat = ValidarCategorias(sCia,sCatCod,sCatNom,sCta)

            if(vValCC['codigo'] == -1 ):
                sMensajeTotal = f"El Centro de Costo : {sCC} en la linea {sLinea}, no se encuentra en la sociedad."
                dLinea = {"mensaje" : sMensajeTotal}
                lResultado.append(dLinea)

            if(vValCta['codigo'] == -1):
                sMensajeTotal = f"La Cuenta Contable : {sCta} en la linea {sLinea}, no se encuentra en la sociedad."
                dLinea = {"mensaje" : sMensajeTotal}
                lResultado.append(dLinea)
            
            print(vValCat["codigo"])

            if(vValCat["codigo"] == -1):
                sMensajeTotal = f"La Categoria : {sCatNom} en la linea {sLinea}, no se encuentra en la sociedad o no esta asociada a la cuenta {sCta}."
                dLinea = {"mensaje" : sMensajeTotal}
                lResultado.append(dLinea)

        if(len(lResultado) == 0):
            dLinea = {"mensaje" : "No se encontro errores en la carga."}

        return {"codigo" : len(lResultado), "resultado" : lResultado}
    
    except Exception as err:
        print(err)
        return {"codigo": -1, "resultado" : "error al analizar la importacion..."}