from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from presupuesto.functions.loginSLDFunction import logintoken
from presupuesto.schemas.borradorSchema import CabeceraBorrador_Schema

from os import getenv
import requests
import json
import urllib3

def GetPresupuestoSQL(pId:str,pCia:str):
    pre_cab = {}
    pre_det = []

    conn = ConectaSQL_Produccion()

    cur_cab = conn.cursor(as_dict = True)

    sqlC = f"select cia,code,name,u_mss_ejerci,u_mss_moneda,u_mss_activo,u_mss_feini,u_mss_fefin,u_mss_usua,_u_mss_top from presu_cabecera where cia='{pCia}' and code='{pId}'"

    cur_cab.execute(sqlC)

    for filac in cur_cab:
        pre_cab = {"Code":filac['code'],"Name":filac['name'],"U_MSS_EJERCI":filac['u_mss_ejerci'],"U_MSS_MONEDA":filac['u_mss_moneda'],
                   "U_MSS_FEINI":filac['u_mss_feini'],"U_MSS_FEFIN":filac['u_mss_fefin'],"U_MSS_USUA":filac['u_mss_usua'],"MSS_PRE1" : []}

        cur_det = conn.cursor(as_dict = True)

        sqlD =  "select cia,code,lineid,u_mss_cuenta,u_mss_desc,u_mss_cc1,u_mss_cc2,u_mss_nc1,u_mss_nc2,u_mss_tipo,u_mss_con,u_mss_capre,u_mss_detca,"
        sqlD += "u_mss_ene,u_mss_feb,u_mss_mar,u_mss_abr,u_mss_may,u_mss_jun,u_mss_ago,u_mss_sep,u_mss_oct,u_mss_nov,u_mss_dic,u_mss_totanu " 
        sqlD += f"from presu_detalle where cia='{pCia}' and code='{pId}'"

        cur_det.execute(sqlD)

        for filad in cur_det:
            fila = {"LineId" : int(filad['lineid']),"U_MSS_CUENTA" : filad['u_mss_cuenta'],"U_MSS_DESC" : filad['u_mss_desc'],"U_MSS_CC1" : filad['u_mss_cc1'],
                    "U_MSS_CC2" : filad['u_mss_cc2'],"U_MSS_TIPO" : filad['u_mss_tipo'],"U_MSS_ENE" : filad['u_mss_ene'], "U_MSS_FEB" : filad['u_mss_feb'],
                    "U_MSS_MAR" : filad['u_mss_mar'],"U_MSS_ABR" : filad['u_mss_abr'],"U_MSS_MAY" : filad['u_mss_may'],"U_MSS_JUN" : filad['u_mss_jun'],
                    "U_MSS_JUL" : filad['u_mss_jul'],"U_MSS_AGO" : filad['u_mss_ago'],"U_MSS_SEP" : filad['u_mss_sep'],"U_MSS_OCT" : filad['u_mss_oct'],
                    "U_MSS_NOV" : filad['u_mss_nov'],"U_MSS_DIC" : filad['u_mss_dic'],"U_MSS_TOTANU" : filad['u_mss_totanu'],"U_MSS_NC1" : filad['u_mss_nc1'],
                    "U_MSS_NC2" : filad['u_mss_nc2'],"U_MSS_CON" : filad['u_mss_con'],"U_MSS_CAPRE" : str(filad['u_mss_capre']).rjust(5,'0'),"U_MSS_DETCA" : filad['u_mss_detca']}

            pre_det.append(fila)
        
        pre_cab["MSS_PRE1Collection" : pre_det]

    return pre_cab

def GuardarSAP(pData:CabeceraBorrador_Schema):
    try:
        sCia = ""
        resultado = []
        respuesta = {}
        print(pData)
        if(pData.cia == "CNT"):
            sCia = "SBO_CANTOL_PRODUCCION"
        
        if(pData.cia == "DTM"):
            sCia = "SBO_DISTRI_PRODUCCION"
        
        if(pData.cia == "TCN"):
            sCia = "SBO_TECNO_PRODUCCION"

        dPresupuesto = GetPresupuestoSQL(pData.code, pData.cia)

        urllib3.disable_warnings()

        token = logintoken(sCia)

        session = token['SessionId']

        header = {
                    'Content-Type' : 'application/json',
                    'Cookie' : f'B1SESSION={session}; ROUTEID=.node3'
                 }

        Servidor = getenv("SERVIDOR_SLD")

        payload = json.dumps(dPresupuesto)

        url = f"{Servidor}/b1s/v1/PRESUPUESTO"

        response = requests.request("POST",url,headers=header,data=payload,verify=False)

        if(response.status_code!=204):
            respuesta = response.json()
        
        if("error" in respuesta):
            mensaje = {"mensaje" : respuesta['error']['message']['value']}
            resultado.append(mensaje)
        else:                
            mensaje = {"mensaje" : f"Se migro el presupuesto a SAP."}
            resultado.append(mensaje)
        
        return resultado
    except Exception as err:
        print(err)        
        return {"codigo" : -1, "mensaje" : err}