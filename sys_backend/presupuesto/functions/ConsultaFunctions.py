from conexiones import conexion_sap_solo
from generales.generales import CiaSAP
#from ldap3 import Server,Connection,SAFE_SYNC


def PresupuestosListado(pCia : str, pUser : str):
    try:
        sCia = ""
        reporte = []
        fila = {}

        sCia = CiaSAP(pCia)
        
        sql = "SELECT \"Code\" as Code,U_MSS_EJERCI , U_MSS_MONEDA , CASE U_MSS_ACTIVO WHEN 'A' THEN 'Activo' ELSE 'InActivo' END AS Estado, "
        sql += "round((SELECT sum(U_MSS_ENE + U_MSS_FEB + U_MSS_MAR + U_MSS_ABR + U_MSS_MAY + U_MSS_JUN + U_MSS_JUL + U_MSS_AGO + "
        sql += f"U_MSS_SEP + U_MSS_OCT + U_MSS_NOV + U_MSS_DIC) FROM {sCia}.\"@MSS_PRE1\" PrD WHERE PrC.\"Code\" = PrD.\"Code\"),2) AS Total_Presupuestado,"
        sql += "round((SELECT sum(U_MSS_ENE1 + U_MSS_FEB2 + U_MSS_MAR2 + U_MSS_ABR2 + U_MSS_MAY2 + U_MSS_JUN2 + U_MSS_JUL2 + U_MSS_AGO2 + U_MSS_SEP2 "
        sql += f"+ U_MSS_OCT2 + U_MSS_NOV2 + U_MSS_DIC2) FROM {sCia}.\"@MSS_PRE1\" PrD WHERE PrC.\"Code\" = PrD.\"Code\"),2) AS Total_Ejecutado "
        sql += f"FROM {sCia}.\"@MSS_OPRE\" PrC  WHERE \"U_MSS_USUA\" = '{pUser}'"        
        
        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_lista = conn.cursor()

        cur_lista.execute(sql)

        for registro in cur_lista:            
            fila = {"Code" : registro['Code'], "U_MSS_EJERCI" : registro['U_MSS_EJERCI'], "U_MSS_MONEDA" : registro['U_MSS_MONEDA'], "Estado": registro['Estado'],
                    "Total_Presupuestado" : registro['Total_Presupuestado'], "Total_Ejecutado" : registro['Total_Ejecutado'], "Cia" : pCia, "User" : pUser,
                    "Detalle" : PresupuestoDetID(pCia,registro['Code'],pUser)}
            
            reporte.append(fila)

        conn.close()

        return reporte
    except Exception as err:
        print(err)
        return f"Error en Listado de Presupuesto : {err}."

def PresupuestoDetID(pCia:str, pCodigo : str ,pUser:str,):
    try:
        sCia = ""
        reporte = []
        fila = {}

        sCia = CiaSAP(pCia)

        sql = "SELECT D.\"Code\" as Code, D.\"LineId\" as LineId , D.U_MSS_CUENTA , D.U_MSS_DESC ,D.U_MSS_CC1 , D.U_MSS_CC2 , D.U_MSS_TIPO , D.U_MSS_CAPRE , D.U_MSS_DETCA,"
        sql += "round(D.U_MSS_ENE,2) as U_MSS_ENE , round(D.U_MSS_FEB,2) as U_MSS_FEB ,round(D.U_MSS_MAR,2) as U_MSS_MAR ,round(D.U_MSS_ABR,2) as U_MSS_ABR"
        sql += ",round(D.U_MSS_MAY,2) as U_MSS_MAY ,round(D.U_MSS_JUN,2) as U_MSS_JUN ,round(D.U_MSS_JUL,2) as U_MSS_JUL ,round(D.U_MSS_AGO,2) as U_MSS_AGO"
        sql += ",round(D.U_MSS_SEP,2) as U_MSS_SEP ,round(D.U_MSS_OCT,2) as U_MSS_OCT ,round(D.U_MSS_NOV,2) as U_MSS_NOV ,round(D.U_MSS_DIC,2) as U_MSS_DIC,"
        sql += "round(D.U_MSS_ENE1,2) as U_MSS_ENE1 , round(D.U_MSS_FEB2,2) as U_MSS_FEB2 ,round(D.U_MSS_MAR2,2) as U_MSS_MAR2 ,round(D.U_MSS_ABR2,2) as U_MSS_ABR2"
        sql += ",round(D.U_MSS_MAY2,2) as U_MSS_MAY2 ,round(D.U_MSS_JUN2,2) as U_MSS_JUN2 ,round(D.U_MSS_JUL2,2) as U_MSS_JUL2 ,round(D.U_MSS_AGO2,2) as U_MSS_AGO2"
        sql += ",round(D.U_MSS_SEP2,2) as U_MSS_SEP2 ,round(D.U_MSS_OCT2,2) as U_MSS_OCT2 ,round(D.U_MSS_NOV2,2) as U_MSS_NOV2 ,round(D.U_MSS_DIC2,2) as U_MSS_DIC2,"
        sql += "round(D.U_MSS_ENE - D.U_MSS_ENE1,2) as U_MSS_ENEG,round(D.U_MSS_FEB - D.U_MSS_FEB2,2) as U_MSS_FEBG,round(D.U_MSS_MAR - D.U_MSS_MAR2,2) as U_MSS_MARG,"
        sql += "round(D.U_MSS_ABR - D.U_MSS_ABR2,2) as U_MSS_ABRG,round(D.U_MSS_MAY - D.U_MSS_MAY2,2) as U_MSS_MAYG,round(D.U_MSS_JUN - D.U_MSS_JUN2,2) as U_MSS_JUNG,"
        sql += "round(D.U_MSS_JUL - D.U_MSS_JUL2,2) as U_MSS_JULG,round(D.U_MSS_AGO - D.U_MSS_AGO2,2) as U_MSS_AGOG,round(D.U_MSS_SEP - D.U_MSS_SEP2,2) as U_MSS_SEPG,"
        sql += "round(D.U_MSS_OCT - D.U_MSS_OCT2,2) as U_MSS_OCTG,round(D.U_MSS_NOV - D.U_MSS_NOV2,2) as U_MSS_NOVG,round(D.U_MSS_DIC - D.U_MSS_DIC2,2) as U_MSS_DICG"
        sql += f" FROM {sCia}.\"@MSS_PRE1\" D INNER JOIN {sCia}.\"@MSS_OPRE\" C ON C.\"Code\" = D.\"Code\" "
        sql += f"WHERE C.U_MSS_USUA = '{pUser}' AND C.\"Code\" = '{pCodigo}' ORDER BY D.\"LineId\""
        
        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_lista = conn.cursor()

        cur_lista.execute(sql)

        for registro in cur_lista:
            fila = {"Code" : registro['Code'],"LineId" : registro['LineId'],"U_MSS_CUENTA" : registro['U_MSS_CUENTA'],"U_MSS_DESC" : registro['U_MSS_DESC'],
                    "U_MSS_CC1" : registro['U_MSS_CC1'],"U_MSS_CC2" : registro['U_MSS_CC2'],"U_MSS_TIPO" : registro['U_MSS_TIPO'],
                    "U_MSS_CAPRE" : registro['U_MSS_CAPRE'],"U_MSS_DETCA" : registro['U_MSS_DETCA'],
                    "U_MSS_ENE" : registro['U_MSS_ENE'],"U_MSS_FEB" : registro['U_MSS_FEB'],"U_MSS_MAR" : registro['U_MSS_MAR'],"U_MSS_ABR" : registro['U_MSS_ABR'],
                    "U_MSS_MAY" : registro['U_MSS_MAY'],"U_MSS_JUN" : registro['U_MSS_JUN'],"U_MSS_JUL" : registro['U_MSS_JUL'],"U_MSS_AGO" : registro['U_MSS_AGO'],
                    "U_MSS_SEP" : registro['U_MSS_SEP'],"U_MSS_OCT" : registro['U_MSS_OCT'],"U_MSS_NOV" : registro['U_MSS_NOV'],"U_MSS_DIC" : registro['U_MSS_DIC'],
                    "U_MSS_ENE1" : registro['U_MSS_ENE1'],"U_MSS_FEB2" : registro['U_MSS_FEB2'],"U_MSS_MAR2" : registro['U_MSS_MAR2'],"U_MSS_ABR2" : registro['U_MSS_ABR2'],
                    "U_MSS_MAY2" : registro['U_MSS_MAY2'],"U_MSS_JUN2" : registro['U_MSS_JUN2'],"U_MSS_JUL2" : registro['U_MSS_JUL2'],"U_MSS_AGO2" : registro['U_MSS_AGO2'],
                    "U_MSS_SEP2" : registro['U_MSS_SEP2'],"U_MSS_OCT2" : registro['U_MSS_OCT2'],"U_MSS_NOV2" : registro['U_MSS_NOV2'],"U_MSS_DIC2" : registro['U_MSS_DIC2'],
                    "U_MSS_ENEG" : registro['U_MSS_ENEG'],"U_MSS_FEBG" : registro['U_MSS_FEBG'],"U_MSS_MARG" : registro['U_MSS_MARG'],"U_MSS_ABRG" : registro['U_MSS_ABRG'],
                    "U_MSS_MAYG" : registro['U_MSS_MAYG'],"U_MSS_JUNG" : registro['U_MSS_JUNG'],"U_MSS_JULG" : registro['U_MSS_JULG'],"U_MSS_AGOG" : registro['U_MSS_AGOG'],
                    "U_MSS_SEPG" : registro['U_MSS_SEPG'],"U_MSS_OCTG" : registro['U_MSS_OCTG'],"U_MSS_NOVG" : registro['U_MSS_NOVG'],"U_MSS_DICG" : registro['U_MSS_DICG']}
            
            reporte.append(fila)

        return reporte
    
    except Exception as err:
        print(err)
        return f"Error en traer informacion de Presupuesto : {err}."

def PresupuestoListadoTotal(pCia : str, pEjercicio : str):
    try:
        sCia = ""
        reporte = []
        fila = {}

        sCia = CiaSAP(pCia)
        
        sql = "SELECT \"Code\" as Code,U_MSS_EJERCI , U_MSS_MONEDA , CASE U_MSS_ACTIVO WHEN 'A' THEN 'Activo' ELSE 'InActivo' END AS Estado, "
        sql += "round((SELECT sum(U_MSS_ENE + U_MSS_FEB + U_MSS_MAR + U_MSS_ABR + U_MSS_MAY + U_MSS_JUN + U_MSS_JUL + U_MSS_AGO + "
        sql += f"U_MSS_SEP + U_MSS_OCT + U_MSS_NOV + U_MSS_DIC) FROM {sCia}.\"@MSS_PRE1\" PrD WHERE PrC.\"Code\" = PrD.\"Code\"),2) AS Total_Presupuestado,"
        sql += "round((SELECT sum(U_MSS_ENE1 + U_MSS_FEB2 + U_MSS_MAR2 + U_MSS_ABR2 + U_MSS_MAY2 + U_MSS_JUN2 + U_MSS_JUL2 + U_MSS_AGO2 + U_MSS_SEP2 "
        sql += f"+ U_MSS_OCT2 + U_MSS_NOV2 + U_MSS_DIC2) FROM {sCia}.\"@MSS_PRE1\" PrD WHERE PrC.\"Code\" = PrD.\"Code\"),2) AS Total_Ejecutado "
        sql += f"FROM {sCia}.\"@MSS_OPRE\" PrC where U_MSS_EJERCI={pEjercicio}"        
        #print(sql)
        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_lista = conn.cursor()

        cur_lista.execute(sql)

        for registro in cur_lista:            
            fila = {"Code" : registro['Code'], "U_MSS_EJERCI" : registro['U_MSS_EJERCI'], "U_MSS_MONEDA" : registro['U_MSS_MONEDA'], "Estado": registro['Estado'],
                    "Total_Presupuestado" : registro['Total_Presupuestado'], "Total_Ejecutado" : registro['Total_Ejecutado'], "Cia" : pCia, "User" : "",
                    "Detalle" : PresupuestoDetIDTotal(pCia,registro['Code'])}
            
            reporte.append(fila)

        conn.close()

        return reporte
    except Exception as err:
        print(err)
        return f"Error en Listado de Presupuesto : {err}."    

def PresupuestoDetIDTotal(pCia : str, pCodigo : str):
    try:
        sCia = ""
        reporte = []
        fila = {}

        sCia = CiaSAP(pCia)

        sql = "SELECT D.\"Code\" as Code, D.\"LineId\" as LineId , D.U_MSS_CUENTA , D.U_MSS_DESC ,D.U_MSS_CC1 , D.U_MSS_CC2 , D.U_MSS_TIPO , D.U_MSS_CAPRE , D.U_MSS_DETCA,"
        sql += "round(D.U_MSS_ENE,2) as U_MSS_ENE , round(D.U_MSS_FEB,2) as U_MSS_FEB ,round(D.U_MSS_MAR,2) as U_MSS_MAR ,round(D.U_MSS_ABR,2) as U_MSS_ABR"
        sql += ",round(D.U_MSS_MAY,2) as U_MSS_MAY ,round(D.U_MSS_JUN,2) as U_MSS_JUN ,round(D.U_MSS_JUL,2) as U_MSS_JUL ,round(D.U_MSS_AGO,2) as U_MSS_AGO"
        sql += ",round(D.U_MSS_SEP,2) as U_MSS_SEP ,round(D.U_MSS_OCT,2) as U_MSS_OCT ,round(D.U_MSS_NOV,2) as U_MSS_NOV ,round(D.U_MSS_DIC,2) as U_MSS_DIC,"
        sql += "round(D.U_MSS_ENE1,2) as U_MSS_ENE1 , round(D.U_MSS_FEB2,2) as U_MSS_FEB2 ,round(D.U_MSS_MAR2,2) as U_MSS_MAR2 ,round(D.U_MSS_ABR2,2) as U_MSS_ABR2"
        sql += ",round(D.U_MSS_MAY2,2) as U_MSS_MAY2 ,round(D.U_MSS_JUN2,2) as U_MSS_JUN2 ,round(D.U_MSS_JUL2,2) as U_MSS_JUL2 ,round(D.U_MSS_AGO2,2) as U_MSS_AGO2"
        sql += ",round(D.U_MSS_SEP2,2) as U_MSS_SEP2 ,round(D.U_MSS_OCT2,2) as U_MSS_OCT2 ,round(D.U_MSS_NOV2,2) as U_MSS_NOV2 ,round(D.U_MSS_DIC2,2) as U_MSS_DIC2,"
        sql += "round(D.U_MSS_ENE - D.U_MSS_ENE1,2) as U_MSS_ENEG,round(D.U_MSS_FEB - D.U_MSS_FEB2,2) as U_MSS_FEBG,round(D.U_MSS_MAR - D.U_MSS_MAR2,2) as U_MSS_MARG,"
        sql += "round(D.U_MSS_ABR - D.U_MSS_ABR2,2) as U_MSS_ABRG,round(D.U_MSS_MAY - D.U_MSS_MAY2,2) as U_MSS_MAYG,round(D.U_MSS_JUN - D.U_MSS_JUN2,2) as U_MSS_JUNG,"
        sql += "round(D.U_MSS_JUL - D.U_MSS_JUL2,2) as U_MSS_JULG,round(D.U_MSS_AGO - D.U_MSS_AGO2,2) as U_MSS_AGOG,round(D.U_MSS_SEP - D.U_MSS_SEP2,2) as U_MSS_SEPG,"
        sql += "round(D.U_MSS_OCT - D.U_MSS_OCT2,2) as U_MSS_OCTG,round(D.U_MSS_NOV - D.U_MSS_NOV2,2) as U_MSS_NOVG,round(D.U_MSS_DIC - D.U_MSS_DIC2,2) as U_MSS_DICG"
        sql += f" FROM {sCia}.\"@MSS_PRE1\" D INNER JOIN {sCia}.\"@MSS_OPRE\" C ON C.\"Code\" = D.\"Code\" "
        sql += f"WHERE C.\"Code\" = '{pCodigo}' ORDER BY D.\"LineId\""

        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_lista = conn.cursor()

        cur_lista.execute(sql)

        for registro in cur_lista:
            fila = {"Code" : registro['Code'],"LineId" : registro['LineId'],"U_MSS_CUENTA" : registro['U_MSS_CUENTA'],"U_MSS_DESC" : registro['U_MSS_DESC'],
                    "U_MSS_CC1" : registro['U_MSS_CC1'],"U_MSS_CC2" : registro['U_MSS_CC2'],"U_MSS_TIPO" : registro['U_MSS_TIPO'],
                    "U_MSS_CAPRE" : registro['U_MSS_CAPRE'],"U_MSS_DETCA" : registro['U_MSS_DETCA'],
                    "U_MSS_ENE" : registro['U_MSS_ENE'],"U_MSS_FEB" : registro['U_MSS_FEB'],"U_MSS_MAR" : registro['U_MSS_MAR'],"U_MSS_ABR" : registro['U_MSS_ABR'],
                    "U_MSS_MAY" : registro['U_MSS_MAY'],"U_MSS_JUN" : registro['U_MSS_JUN'],"U_MSS_JUL" : registro['U_MSS_JUL'],"U_MSS_AGO" : registro['U_MSS_AGO'],
                    "U_MSS_SEP" : registro['U_MSS_SEP'],"U_MSS_OCT" : registro['U_MSS_OCT'],"U_MSS_NOV" : registro['U_MSS_NOV'],"U_MSS_DIC" : registro['U_MSS_DIC'],
                    "U_MSS_ENE1" : registro['U_MSS_ENE1'],"U_MSS_FEB2" : registro['U_MSS_FEB2'],"U_MSS_MAR2" : registro['U_MSS_MAR2'],"U_MSS_ABR2" : registro['U_MSS_ABR2'],
                    "U_MSS_MAY2" : registro['U_MSS_MAY2'],"U_MSS_JUN2" : registro['U_MSS_JUN2'],"U_MSS_JUL2" : registro['U_MSS_JUL2'],"U_MSS_AGO2" : registro['U_MSS_AGO2'],
                    "U_MSS_SEP2" : registro['U_MSS_SEP2'],"U_MSS_OCT2" : registro['U_MSS_OCT2'],"U_MSS_NOV2" : registro['U_MSS_NOV2'],"U_MSS_DIC2" : registro['U_MSS_DIC2'],
                    "U_MSS_ENEG" : registro['U_MSS_ENEG'],"U_MSS_FEBG" : registro['U_MSS_FEBG'],"U_MSS_MARG" : registro['U_MSS_MARG'],"U_MSS_ABRG" : registro['U_MSS_ABRG'],
                    "U_MSS_MAYG" : registro['U_MSS_MAYG'],"U_MSS_JUNG" : registro['U_MSS_JUNG'],"U_MSS_JULG" : registro['U_MSS_JULG'],"U_MSS_AGOG" : registro['U_MSS_AGOG'],
                    "U_MSS_SEPG" : registro['U_MSS_SEPG'],"U_MSS_OCTG" : registro['U_MSS_OCTG'],"U_MSS_NOVG" : registro['U_MSS_NOVG'],"U_MSS_DICG" : registro['U_MSS_DICG']}
            
            reporte.append(fila)

        return reporte
    except Exception as err:
        print(err)
        return f"Error en traer informacion de Presupuesto : {err}."
    
def CentroCostoSAP(pCia : str, pCtaD : str):
    try:
        sCia = ""
        reporte = []   
        fila = {}     

        sCia = CiaSAP(pCia)

        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_cc = conn.cursor()

        sql = f"SELECT TOP 100 LEFT(\"PrcCode\",5) AS CC1,\"PrcCode\" AS CC2,\"PrcName\" AS CC_Desc FROM {sCia}.OPRC cc where (UCASE(\"PrcCode\") LIKE '%{pCtaD.upper()}%' OR UCASE(\"PrcName\") like '%{pCtaD.upper()}%') AND \"Locked\" = 'N'  AND LENGTH(\"PrcCode\") = 7"

        print(sql)

        cur_cc.execute(sql)

        for reg in cur_cc:
            fila = {"CC1" : reg["CC1"],"CC2" : reg["CC2"],"CC_Desc" : reg["CC_Desc"]} 
            reporte.append(fila)

        return reporte
    
    except Exception as err:
        print(err)
        return f"Error en traer informacion de Centro de Costo : {err}."
    
def CuentasSAP(pCia : str, pCC : str):
    try:
        sCia = ""
        reporte = []   
        fila = {}     

        sCia = CiaSAP(pCia)

        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_ctas = conn.cursor()

        sql = f"SELECT TOP 100 \"AcctCode\" AS Cta, \"AcctName\" AS Cta_Desc FROM {sCia}.OACT WHERE \"AcctCode\" LIKE '9%' AND LENGTH(\"AcctCode\") > 5 AND \"AcctName\" LIKE '%{pCC.upper()}%' "
        print(sql)
        cur_ctas.execute(sql)

        for reg in cur_ctas:
            fila = {"Cta" : reg["Cta"],"Cta_Desc" : reg["Cta_Desc"]} 
            reporte.append(fila)

        return reporte
    
    except Exception as err:
        print(err)
        return f"Error en traer informacion de Cuentas Contables : {err}."

def CategoriaPreSAP(pCia : str, pCate : str, pCta : str):

    try:
        sCia = ""
        reporte = []   
        fila = {}     

        sFiltro = pCate.strip()

        if(sFiltro == " "):
            sFiltro = ""

        sCia = CiaSAP(pCia)

        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_cate = conn.cursor()

        sql = f"SELECT TOP 100 \"Code\" AS Cat_Codigo,\"Name\" AS Cat_Desc,U_MSS_CUCON AS Cta_Cont FROM {sCia}.\"@MSS_CATPRE\" mc WHERE UPPER(\"Name\") LIKE '%{sFiltro.upper()}%' AND U_MSS_CUCON = '{pCta}' "

        cur_cate.execute(sql)

        for reg in cur_cate:
            fila = {"Cat_Codigo" : reg["Cat_Codigo"],"Cat_Desc" : reg["Cat_Desc"]} 
            reporte.append(fila)

        return reporte
    
    except Exception as err:
        print(err)        
        return f"Error en traer informacion de Categorias Presupuestales : {err}."