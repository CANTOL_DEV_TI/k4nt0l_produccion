from conexiones.conexion_sql_solo import ConectaSQL_Produccion

def ReportePresupuesto(pCia:str,pUsuario:str):
    try:
        sqlC = "select distinct pc.cia,pc.code,pc.U_MSS_USUA ,pcu.centro_costo ,pcu.cc_desc, case pc.Estado when 'B' then 'Borrador' when 'A' then 'Aprobado' when 'M' then 'Migrado' when 'R' then 'Rechazado' end as Estado, pc.U_MSS_TOP "  
        sqlC += "from presu_cabecera pc inner join presu_detalle pd on pc.cia = pd.cia and pc.code = pd.code "
        sqlC += "inner join presu_cc_usuario pcu on pcu.cia = pc.Cia and pcu.usuario = pc.U_MSS_USUA and pcu.centro_costo = pd.U_MSS_CC2  "
        sqlC += f"where pc.cia='{pCia}' and pc.Estado ='B' and pc.U_MSS_USUA in ( "
        sqlC += "select distinct usuario from presu_cc_usuario where centro_costo in ( select pcu.centro_costo from presu_cc_usuario pcu "
        sqlC += f" where pcu.rol_requerimiento = 'A' and pcu.cia = '{pCia}' and usuario='{pUsuario}'))"
        #print(sqlC)
        conn = ConectaSQL_Produccion()

        cur_lista = conn.cursor(as_dict = True)

        cur_lista.execute(sqlC)
                
        lista = cur_lista.fetchall()
        
        tabla = []

        for f in lista:
            #print(f)
            sqlD = "select pc.cia,pc.U_MSS_USUA ,pc.code,pd.U_MSS_CC2, pd.U_MSS_NC2 , pd.U_MSS_CUENTA ,pd.U_MSS_DESC,pd.U_MSS_CAPRE,pd.U_MSS_DETCA,"
            sqlD += "u_mss_ene as Enero,u_mss_feb as Febrero, pd.U_MSS_MAR as Marzo, pd.U_MSS_ABR as Abril, pd.U_MSS_MAY as Mayo, pd.U_MSS_JUN as Junio,"
            sqlD += "pd.U_MSS_JUL as Julio, pd.U_MSS_AGO as Agosto, pd.U_MSS_SEP as Septiembre, pd.U_MSS_OCT as Octubre, pd.U_MSS_NOV as Noviembre, pd.U_MSS_DIC as Diciembre,"
            sqlD += "round(u_mss_ene + u_mss_feb + U_MSS_MAR + U_MSS_ABR + U_MSS_MAY + U_MSS_JUN + U_MSS_JUL + U_MSS_AGO + U_MSS_SEP + U_MSS_OCT + U_MSS_NOV + U_MSS_DIC,2) as Total_Linea "
            sqlD += f"from presu_cabecera pc inner join presu_detalle pd on pc.cia = pd.cia and pc.code = pd.code where pc.cia='{pCia}' and pc.code='{f['code']}'"
            #print(sqlD)
            cur_detalle = conn.cursor(as_dict = True)

            cur_detalle.execute(sqlD)

            detalle = cur_detalle.fetchall()

            fila = {'cia':f['cia'], 'code':f['code'], 'usuario':f['U_MSS_USUA'], 'centro_costo':f['centro_costo'],'cc_desc':f['cc_desc'],'estado' : f['Estado'],
                    'Total':f['U_MSS_TOP'],'detalle' : detalle}
            
            tabla.append(fila)
        
        return tabla
    except Exception as err:
        print(err)
        return err