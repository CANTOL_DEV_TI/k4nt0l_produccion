from conexiones import conexion_sap_solo
from presupuesto.schemas.CategoriaPresupuestalSchema import CategoriaPresupuestal_schema
from presupuesto.functions.loginSLDFunction import logintoken
from generales.generales import CiaSAP
from os import getenv
import requests
import json
import urllib3

def CatePresupuestal(pCia:str,pFiltro:str):
    try:
        sCia = ""
        reporte = []
        fila = {}
        sFiltro = pFiltro.strip()
        
        if(sFiltro==" "):
            sFiltro == str()
        
        sCia = CiaSAP(pCia)
                
        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_cc = conn.cursor()

        sql = f"SELECT \"Code\" as Code,\"Name\" as Name,U_MSS_CUCON,A.\"AcctName\" AS Cuenta_Nombre FROM {sCia}.\"@MSS_CATPRE\" INNER JOIN {sCia}.oact A ON U_MSS_CUCON=A.\"AcctCode\" WHERE UPPER(\"Name\") LIKE rtrim('%{sFiltro.upper()}%') order by \"Code\" ASC"

        #print(sql)

        cur_cc.execute(sql)

        for reg in cur_cc: 
            fila = {"Code" : reg["Code"],"Name" : reg["Name"],"U_MSS_CUCON" : reg["U_MSS_CUCON"],"Cuenta_Nombre" : reg["Cuenta_Nombre"]} 
            reporte.append(fila)

        return reporte

    except Exception as err:
        print(err)
        return {"codigo" : -1 , "mensaje" : err}

def GeneraCorrelativo(pCia:str):
    try:
        
        fila = {}

        conn = conexion_sap_solo.conexion_SAP_Tecno()

        cur_cc = conn.cursor()

        sql = "SELECT CONCAT( CASE WHEN (max(CAST(\"Code\" AS int)) + 1) < 999 THEN '00' WHEN (max(CAST(\"Code\" AS int)) + 1) < 9999 THEN '0' "
        sql += f"WHEN (max(CAST(\"Code\" AS int)) + 1) < 9999 THEN '' END  ,(max(CAST(\"Code\" AS int)) + 1)) AS Correlativo FROM {pCia}.\"@MSS_CATPRE\" "

        #print(sql)

        cur_cc.execute(sql)

        for reg in cur_cc:
            fila = {"Correlativo" : reg["Correlativo"]} 
            
        return fila

    except Exception as err:
        print(err)        
        return {"codigo" : -1 , "mensaje" : err}
    
def GuardarCategoria(pData:CategoriaPresupuestal_schema):
    try:
        sCia = ""
        resultado = []
        respuesta = {}            

        sCia = CiaSAP(pData.cia)

        nuevocodigo = GeneraCorrelativo(sCia)

        dCate = {"Code" : nuevocodigo['Correlativo'], "Name" : nuevocodigo['Correlativo'] + "-" + pData.Name , "U_MSS_CUCON" : pData.U_MSS_CUCON}

        urllib3.disable_warnings()

        token = logintoken(sCia)

        session = token['SessionId']

        header = {
                    'Content-Type' : 'application/json',
                    'Cookie' : f'B1SESSION={session}; ROUTEID=.node3'
                 }

        Servidor = getenv("SERVIDOR_SLD")

        payload = json.dumps(dCate)

        url = f"{Servidor}/b1s/v1/U_MSS_CATPRE"

        response = requests.request("POST", url, headers=header,data=payload,verify=False)

        if(response.status_code!=204):
            respuesta = response.json()

        if("error" in respuesta):
            mensaje = {"mensaje" : respuesta['error']['message']['value']}
            resultado.append(mensaje)
        else:                
            mensaje = {"mensaje" : f"Se agrego la categoria presupuestal."}
            resultado.append(mensaje)
                        
        return resultado
        
    except Exception as err:        
        print(err)        
        return {"codigo" : -1, "mensaje" : err}
    
def ActualizarCategoria(pData:CategoriaPresupuestal_schema):
    try:
        sCia = ""
        resultado = []

        sCia = CiaSAP(pData.cia)
        
        dCate = { "Name" : pData.Name , "U_MSS_CUCON" : pData.U_MSS_CUCON}

        urllib3.disable_warnings()

        token = logintoken(sCia)

        session = token['SessionId']

        header = {
                    'Content-Type' : 'application/json',
                    'Cookie' : f'B1SESSION={session}; ROUTEID=.node3'
                 }

        Servidor = getenv("SERVIDOR_SLD")

        payload = json.dumps(dCate)

        url = f"{Servidor}/b1s/v1/U_MSS_CATPRE('{pData.Code}')"

        response = requests.request("PATCH", url, headers=header,data=payload,verify=False)
        
        

        if(response.status_code!=204):        
            resultado = response.json()
        
        if("error" in resultado):
            mensaje = {"mensaje" : resultado['error']['message']['value']}
            resultado.append(mensaje)
        else:
            #print("entro al no error")                
            mensaje = {"mensaje" : f"Se actualizo la categoria presupuestal."}
            resultado.append(mensaje)
                        
        return resultado
        
    except Exception as err:
        print(err)
        return {"codigo" : -1, "mensaje" : err}
    
def EliminarCategoria(pData:CategoriaPresupuestal_schema):
    try:
        sCia = ""
        resultado = []

        sCia = CiaSAP(pData.cia)
        
        dCate = {"Code" : pData.Code, "Name" : pData.Name , "U_MSS_CUCON" : pData.U_MSS_CUCON}

        urllib3.disable_warnings()

        token = logintoken(sCia)

        session = token['SessionId']

        header = {
                    'Content-Type' : 'application/json',
                    'Cookie' : f'B1SESSION={session}; ROUTEID=.node3'
                 }

        Servidor = getenv("SERVIDOR_SLD")

        payload = json.dumps(dCate)

        url = f"{Servidor}/b1s/v1/U_MSS_CATPRE('{pData.Code}')"

        response = requests.request("DELETE", url, headers=header,data=payload,verify=False)

        if(response.status_code!=204):
            resultado = response.json()

        if("error" in resultado):
            mensaje = {"mensaje" : resultado['error']['message']['value']}
            resultado.append(mensaje)
        else:                
            mensaje = {"mensaje" : f"Se elimino la categoria presupuestal."}
            resultado.append(mensaje)
                        
        return resultado
        
    except Exception as err:
        print(err)
        return {"codigo" : -1, "mensaje" : err}