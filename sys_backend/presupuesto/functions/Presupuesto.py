# from conexiones.conexion_sap import conexion_sap
from conexiones.conexion_sap import conexion_sap

v_db = "SBO_TECNO_PRODUCCION"


class Presupuesto:
    def __init__(self):
        self.__meConexion = conexion_sap()

    def get_ejercicio(self):
        sql = """ SELECT EJERCICIO """
        sql += """ FROM """ + v_db + """.SGC_PRESUPUESTO_EJERCICIO """
        me_conexion = conexion_sap()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ('',))
        return resultadosConsulta

    def get_ccosto_n2(self, cod_ejercicio, cod_usuario):
        sql = """ SELECT COD_CCOSTO_2, CCOSTO_2 """
        sql += """ FROM """ + v_db + """.SGC_PRESUPUESTO_CCOSTO_N2 ('%s','%s') """ % (
            cod_ejercicio, cod_usuario)
        me_conexion = conexion_sap()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ('',))
        return resultadosConsulta

    def get_presupuesto_general(self, cod_ejercicio, cod_usuario, cod_cc2):
        sql = """ SELECT * """
        sql += """ FROM """ + v_db + """.SGC_PRESUPUESTO_GENERAL ('%s','%s','%s') """ % (
            cod_ejercicio, cod_usuario, cod_cc2)
        me_conexion = conexion_sap()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ('',))
        return resultadosConsulta

    def get_presupuesto_periodo(self, cod_ejercicio, cod_periodo, cod_usuario, cod_cc2):
        sql = """ SELECT * """
        sql += """ FROM """ + v_db + """.SGC_PRESUPUESTO_PERIODO ('%s','%s','%s','%s') """ % (
            cod_ejercicio, cod_periodo, cod_usuario, cod_cc2)
        me_conexion = conexion_sap()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ('',))
        return resultadosConsulta

    def get_presupuesto_documento(self, cod_ejercicio, cod_periodo, cod_ccontable, cod_cc2):
        sql = """ SELECT * """
        sql += """ FROM """ + v_db + """.SGC_PRESUPUESTO_DOCUMENTO ('%s','%s','%s','%s') """ % (
            cod_ejercicio, cod_periodo, cod_ccontable, cod_cc2)
        me_conexion = conexion_sap()
        resultadosConsulta = me_conexion.consultas_sgc(sql, ('',))
        return resultadosConsulta
