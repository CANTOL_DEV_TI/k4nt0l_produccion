from typing import List,Optional 
from pydantic import BaseModel 

class CategoriaPresupuestal_schema(BaseModel):
    cia : Optional[str]
    Code : Optional[str]
    Name : Optional[str]
    U_MSS_CUCON : Optional[str]

    class Config:
        orm_mode = True