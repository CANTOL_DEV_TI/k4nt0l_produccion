from typing import List,Optional 
from pydantic import BaseModel 

class import_detalles(BaseModel):
    cia : Optional[str]    
    lineaid :Optional[int]
    cuenta : Optional[str]
    cuenta_desc : Optional[str]
    cc1 : Optional[str]
    cc2 : Optional[str]
    tipo : Optional[str]
    concepto : Optional[str]
    enero : Optional[float]
    febrero : Optional[float]
    marzo : Optional[float]
    abril : Optional[float]
    mayo : Optional[float]
    junio : Optional[float]
    julio : Optional[float]
    agosto : Optional[float]
    setiembre : Optional[float]
    octubre : Optional[float]
    noviembre : Optional[float]
    diciembre : Optional[float]    
    ccosto_desc : Optional[str]    
    catpre : Optional[str]
    catpre_desc : Optional[str]

class import_cab(BaseModel):
    detalles : Optional[List[import_detalles]]