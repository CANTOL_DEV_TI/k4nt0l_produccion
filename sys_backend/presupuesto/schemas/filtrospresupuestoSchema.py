from typing import List,Optional 
from pydantic import BaseModel 
from decimal import Decimal 

class filtroPresupuestoxUsuario(BaseModel):
    Compañia : Optional[str]
    Usuario : Optional[str]
    Codigo : Optional[str]
    Ejercicio : Optional[int]