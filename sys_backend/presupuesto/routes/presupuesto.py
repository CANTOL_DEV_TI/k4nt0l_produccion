from fastapi import APIRouter,status
from typing import Union,List
from presupuesto.functions.Presupuesto import Presupuesto
from presupuesto.functions.ConsultaFunctions import PresupuestosListado,PresupuestoDetID,PresupuestoDetIDTotal,PresupuestoListadoTotal,CuentasSAP,CentroCostoSAP,CategoriaPreSAP
from presupuesto.functions.PresupuestoBorradorFunctions import AddPresupuesto,UpdPresupuesto,DelPresupuesto,ListaPresupuestosBorradores,ManejaPresupuesto,ListaDetalleBorradores,BandejaBorradores,VerHistorialPresupuesto,ValidarCuenta,ValidarCentroCosto,ValidarImportacion
from presupuesto.functions.PresupuestoSAPFunctions import GuardarSAP
from presupuesto.schemas.filtrospresupuestoSchema import filtroPresupuestoxUsuario
from presupuesto.schemas.importacionSchema import import_cab
from presupuesto.schemas.presupuestoSchema import CabeceraPresupuesto_Schema
from presupuesto.schemas.borradorSchema import CabeceraBorrador_Schema
from presupuesto.schemas.CategoriaPresupuestalSchema import CategoriaPresupuestal_schema
from presupuesto.functions.CategoriaPresupuestalFunctions import CatePresupuestal,GuardarCategoria,ActualizarCategoria,EliminarCategoria
from presupuesto.functions.reportePresupuestos import ReportePresupuesto

ruta = APIRouter(
    prefix="/presupuesto",
    tags=["Presupuesto"]
)

@ruta.get("/get_ejercicio", tags=["Presupuesto"])
def get_presupuesto_general():
    meConsulta = Presupuesto()
    resultado = meConsulta.get_ejercicio()
    return resultado


@ruta.get("/get_ccosto_n2", tags=["Presupuesto"])
def get_presupuesto_general(cod_ejercicio: Union[str, None] = "0", cod_usuario: Union[str, None] = "0"):
    meConsulta = Presupuesto()
    resultado = meConsulta.get_ccosto_n2(cod_ejercicio, cod_usuario)
    return resultado


@ruta.get("/get_presupuesto_general", tags=["Presupuesto"])
def get_presupuesto_general(cod_ejercicio: Union[str, None] = "0", cod_usuario: Union[str, None] = "0", cod_cc2: Union[str, None] = "0"):
    meConsulta = Presupuesto()
    resultado = meConsulta.get_presupuesto_general(
        cod_ejercicio, cod_usuario, cod_cc2)
    return resultado


@ruta.get("/get_presupuesto_periodo", tags=["Presupuesto"])
def get_presupuesto_periodo(cod_ejercicio: Union[str, None] = "0", cod_periodo: Union[str, None] = "0", cod_usuario: Union[str, None] = "0", cod_cc2: Union[str, None] = "0"):
    meConsulta = Presupuesto()
    resultado = meConsulta.get_presupuesto_periodo(
        cod_ejercicio, cod_periodo, cod_usuario, cod_cc2)
    return resultado

@ruta.get("/get_presupuesto_documento", tags=["Presupuesto"])
def get_presupuesto_periodo(cod_ejercicio: Union[str, None] = "0", cod_periodo: Union[str, None] = "0", cod_ccontable: Union[str, None] = "0", cod_cc2: Union[str, None] = "0"):
    meConsulta = Presupuesto()
    resultado = meConsulta.get_presupuesto_documento(
        cod_ejercicio, cod_periodo, cod_ccontable, cod_cc2)
    return resultado

@ruta.post("/presupuestoxusuario/", tags=["Presupuesto"])
def get_presupuesto_x_usuario(pValores:filtroPresupuestoxUsuario):
    #print(pValores)
    sCia = pValores.Compañia
    sUser = pValores.Usuario
    resultado = PresupuestosListado(sCia,sUser)
    return resultado

@ruta.post("/presupuestoxcodigo/", tags=["Presupuesto"])
def get_presupuesto_x_usuario(pValores:filtroPresupuestoxUsuario):
    print(pValores)
    sCia = pValores.Compañia
    sUser = pValores.Usuario
    sId = pValores.Codigo
    resultado = PresupuestoDetID(sCia,sId,sUser)
    return resultado

@ruta.post("/presupuestototal/", tags=["Presupuesto"])
def get_presupuestolistado(pValores:filtroPresupuestoxUsuario):
    print(pValores)
    sCia = pValores.Compañia
    sEjercicio = pValores.Ejercicio
    resultado = PresupuestoListadoTotal(sCia,sEjercicio)
    return resultado

@ruta.post("/presupuestototaldetalle/", tags=["Presupuesto"])
def get_presupuestodetalle(pValores:filtroPresupuestoxUsuario):
    print(pValores)
    sCia = pValores.Compañia
    sUser = pValores.Usuario
    sId = pValores.Codigo
    resultado = PresupuestoDetIDTotal(sCia,sId)
    return resultado

@ruta.post("/Borradores/Listado/", tags = ["Presupuesto"])
def get_listadoborradores(pFiltro:filtroPresupuestoxUsuario):
    print(pFiltro)
    resultado = ListaPresupuestosBorradores(pFiltro)
    return resultado

@ruta.post("/Borrador/Agregar/", tags=["Presupuesto"])
def Agregar(pData:CabeceraBorrador_Schema):
    resultado = AddPresupuesto(pData)
    return resultado

@ruta.put("/Borrador/Actualizar/",tags=["Presupuesto"])
def Actualizar(pData:CabeceraBorrador_Schema):
    print("Entro a Actualizar")
    print(pData)
    resultado = UpdPresupuesto(pData)
    return resultado

@ruta.delete("/Borrador/Eliminar/", tags=["Presupuesto"])
def Eliminar(pData:CabeceraBorrador_Schema):
    resultado = DelPresupuesto(pData)
    return resultado

@ruta.put("/Borrador/Autorizar/", tags=["Presupuesto"])
def Manejar(pData:CabeceraBorrador_Schema):
    resultado = ManejaPresupuesto(pData)
    return resultado

@ruta.post("/Borrador/Detalle/", tags=["Presupuesto"])
def get_detalleborrador(pFiltro : filtroPresupuestoxUsuario):   
    xCia = pFiltro.Compañia
    xCodigo = pFiltro.Codigo
    resultado = ListaDetalleBorradores(xCia,xCodigo)
    return resultado

@ruta.get("/Borrador/CuentasSAP/{pCia}-{pCta}/", tags=["Presupuesto"],status_code=status.HTTP_200_OK)
def getCtaSAP(pCia:Union[str, None] = " ",pCta:Union[str, None] = " "):
    return CuentasSAP(pCia,pCta)

@ruta.get("/Borrador/CentroCostoSAP/{pCia}-{pCC}/", tags=["Presupuesto"],status_code=status.HTTP_200_OK)
def getCtaSAP(pCia:Union[str, None] = " ",pCC:Union[str, None] = " "):
    return CentroCostoSAP(pCia,pCC)

@ruta.get("/Borrador/CategoriaPreSAP/{pCia}-{pCate}-{pCta}/", tags=["Presupuesto"],status_code=status.HTTP_200_OK)
def getCategoriaPreSAP(pCia:Union[str, None] = " ",pCate:Union[str, None] = " ",pCta:Union[str, None] = " "):
    return CategoriaPreSAP(pCia,pCate,pCta)

@ruta.get("/Borrador/Bandeja/{pCia}/{pCode}/{pEjercicio}/{pUsuario}/", tags=["Presupuesto"],status_code=status.HTTP_200_OK)
def getBandeja(pCia:Union[str,None]=" ", pCode:Union[str,None] = " ", pEjercicio:Union[int,None] = 0, pUsuario : Union[str,None] = " " ):    
    return BandejaBorradores(pCia,pCode,pEjercicio,pUsuario)

@ruta.get("/Borrador/Historial/{pCia}/{pCode}/",status_code=status.HTTP_200_OK)
async def getHistorial(pCia:str, pCode:str):
    return VerHistorialPresupuesto(pCia,pCode)

@ruta.get("/Borrador/ValidaCta/{pCia}/{pCuenta}/",status_code=status.HTTP_200_OK)
async def valCta(pCia:str,pCuenta:str):
    return ValidarCuenta(pCia,pCuenta)

@ruta.get("/Borrador/ValidaCC/{pCia}/{pCC}/",status_code=status.HTTP_200_OK)
async def valCC(pCia:str,pCC:str):
    return ValidarCentroCosto(pCia,pCC)

@ruta.post("/Borrador/ValidarImportacion/", tags=["Presupuesto"])
async def valImportacion(pData:import_cab):    
    return ValidarImportacion(pData)

@ruta.get("/CategoriasPresupuestales/{pCia}/{pFiltro}", status_code=status.HTTP_200_OK)
async def ListaCatePre(pCia:str, pFiltro:str):
    return CatePresupuestal(pCia,pFiltro)

@ruta.post("/CategoriaPresupuestal/", status_code=status.HTTP_200_OK)
async def AgregaCatePresu(pData:CategoriaPresupuestal_schema):
    return GuardarCategoria(pData)

@ruta.patch("/CategoriaPresupuestal/", status_code=status.HTTP_200_OK)
async def ActualizarCatePresu(pData:CategoriaPresupuestal_schema):
    return ActualizarCategoria(pData)

@ruta.delete("/CategoriaPresupuestal/", status_code=status.HTTP_200_OK)
async def EliminarCatePresu(pData:CategoriaPresupuestal_schema):
    return EliminarCategoria(pData)

@ruta.post("/Borrador/MigracionSAP/", status_code=status.HTTP_200_OK)
async def Migracion(pData:CabeceraBorrador_Schema):
    return GuardarSAP(pData)

@ruta.get('/reportepresupuestos/{pCia}/{pUsuario}/',status_code=status.HTTP_202_ACCEPTED)
async def reportePresupuestosG(pCia:str,pUsuario:str):
    return ReportePresupuesto(pCia,pUsuario)
