from conexiones.conexion_sap_solo import conexion_SAP_Tecno
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from creditos_cobranzas.schemas.facturasletrasSAPSchemas import DocSAP
from creditos_cobranzas.schemas.refinanciarfacturasSAPSchemas import ListaFL
from creditos_cobranzas.functions.loginSLDFunction import logintoken
from creditos_cobranzas.functions.condicionespagoFunctions import ExtraerPrevioPagos
from os import getenv
import requests
import json
import urllib3
import datetime
from typing import List
from generales.generales import CiaSAP
from generales.configuracion import GetSeries

def ListadoLetras(meData):
    print("Listar....")
    #print(meData)

    sCia = CiaSAP(meData.Compañia)
    reporte = []
    
    sql = " SELECT row_number() over(ORDER BY \"DocEntry\") AS indice,o.\"DocEntry\" as DocEntry ,o.\"DocNum\" as DocNum , o.\"LicTradNum\" as LicTradNum,o.\"CardCode\" as CardCode ,o.\"CardName\" as CardName "
    sql += ", o.\"FolioPref\" as FolioPref, o.\"FolioNum\" as FolioNum,TO_VARCHAR(o.\"DocDueDate\",'YYYY-MM-DD') as DocDueDate ,o.\"DocCur\" as DocCur,o.\"DocTotal\" as DocTotal"
    sql += f",o.\"PaidToDate\" as PaidToDate ,s.\"SeriesName\" as SeriesName,o.\"Comments\" as Remark, o.\"ShipToCode\" as ShipToCode, o.\"PayToCode\" as PayToCode,o.\"Address\" as Address FROM {sCia}.OINV o "
    sql += f"INNER JOIN {sCia}.NNM1 s ON o.\"Series\" = s.\"Series\"  "
    sql += f" WHERE o.\"Series\" IN (6,7,8,9,10,11,12,13,58,68,62,57,77) AND o.\"PaidToDate\" < o.\"DocTotal\" AND o.\"DocDueDate\" BETWEEN '{meData.Desde}' AND '{meData.Hasta}' "
    sql += f" AND o.\"CardCode\" = '{meData.Proveedor}' ORDER BY o.\"LicTradNum\" "
    #print(sql)
    conn = conexion_SAP_Tecno()

    cur_lista = conn.cursor()

    cur_lista.execute(sql)

    for registro in cur_lista:
        fila = {"indice":registro["indice"],"DocEntry" : registro['DocEntry'], "DocNum" : registro['DocNum'], "LicTradNum" : registro['LicTradNum'], "CardCode" : registro['CardCode'],
                "CardName" : registro['CardName'], "FolioPref" : registro['FolioPref'], "FolioNum" : registro['FolioNum'], "DocDueDate" : registro['DocDueDate'],
                "DocCur" : registro['DocCur'], "DocTotal" : registro['DocTotal'], "PaidToDate" : registro['PaidToDate'], "SeriesName" : registro['SeriesName'],
                "Remark" : registro['Remark'], "PayToCode" : registro['PayToCode'], "ShipToCode" : registro['ShipToCode'], "Address" : registro['Address']
                }
        
        reporte.append(fila)
    
    conn.close()

    return reporte
    
def CerrarPagos(pCia : str, pTrama :ListaFL):
    try:
        #print(pTrama)
        
        resultado = ""
        pSociedad = ""

        pSociedad = CiaSAP(pCia)

        urllib3.disable_warnings()

        token = logintoken(pSociedad)

        session = token['SessionId']

        mensaje = ""
        #print("antes del payload")
        payload = json.dumps(pTrama)
        #print(payload)
        header = {
                    'Content-Type': 'application/json',
                    'Cookie': f'B1SESSION={session}; ROUTEID=.node3'
                 }


        Servidor = getenv("SERVIDOR_SLD")        
        url = f"{Servidor}/b1s/v1/IncomingPayments"

        response = requests.request("POST", url, headers=header, data=payload,verify=False)

        if(response.status_code!=204):
           #print(response.json())
           resultado = response.json()            

        mensaje = ""

        if("error" in resultado):
            mensaje = resultado['error']['message']['value']
        else:
            print("Asiento de cierre")
            print(resultado["DocNum"])
            mensaje = f"Se genero el pago recibido {resultado['DocNum']} para cerrar en SAP."

        return mensaje                
    except Exception as err:
        return {"Resultados" : -1 , "Mensaje" : f"Cerrar Pagos : Error en la operación : {err}."}
    
def ListaClientes(pCia : str, pFiltro : str):
    try:
        
        sCia = CiaSAP(pCia)

        dResultados = []
            
        sql = f"SELECT \"CardCode\" AS Codigo,\"CardName\" AS Razon_Social,\"LicTradNum\" AS NroDoc FROM {sCia}.ocrd WHERE \"CardType\" = 'C' AND \"CardName\" LIKE '%{pFiltro.upper()}%' AND \"VatStatus\" = 'Y' AND \"CardCode\" LIKE'C%'"
        #print(sql)
        conn = conexion_SAP_Tecno()

        cur_clientes = conn.cursor()

        cur_clientes.execute(sql)

        for fila in cur_clientes:
            registro = {"Codigo":fila['Codigo'],"Razon_Social" : fila['Razon_Social'], "NroDoc" : fila['NroDoc']}
            dResultados.append(registro)

        return dResultados
    
    except Exception as err:
        return {"Resultado" : -1, "Mensaje" : f"Error al extraer datos : {err}"}

def RefinanciarDocumentos(pDocs:List[DocSAP]):
    try:        
        Cia = ""
        CondP = ""
        CardCode = ""
        CardName = ""
        PayToCode = ""
        Address = ""
        Comments = ""
        TotalMonto = 0
        DocCurrency = ""
        FederalTaxId = ""
        ShipToCode = ""
        Comentario_Nueva_Letra = ""
        Linea = 0
        Facturas = []

        for filaTM in pDocs:            
            Cia = filaTM.cia
            CardCode = filaTM.cardcode
            CardName = filaTM.cardname
            PayToCode = filaTM.paytocode
            Address = filaTM.address
            Comments = filaTM.comments
            DocCurrency = filaTM.doccur
            FederalTaxId = filaTM.lictradnum
            ShipToCode = filaTM.shiptocode
            
            if(filaTM.condp!=""):
                CondP = filaTM.condp

            TotalMonto = TotalMonto + (float(str(filaTM.doctotal).strip()) - float(str(filaTM.paidtodate).strip()))
            
            LineaDocumento = {  'LineNum': Linea, 'DocEntry': filaTM.docentry, 'SumApplied': filaTM.doctotal,'AppliedFC': 0.0, 'AppliedSys': 0.0, 'DocRate': 0.0, 
                                'DocLine': Linea, 'InvoiceType': "it_Invoice", 'DiscountPercent': 0.0,'PaidSum': filaTM.paidtodate}
            
            Comentario_Nueva_Letra += filaTM.foliopref + "-" + filaTM.folionum + " | "
            
            Facturas.append(LineaDocumento)

            Linea += 1

        #CerrarDocs = ListaFL

        #serieSAP = GetSeries(fila["cia"])  

        CerrarDocs = {
            "DocType" : "rCustomer",
            "CardCode" : CardCode,
            "CardName" : CardName,
            "Address" : Address,
            "CashAccount" : "1233109",#temporal por confirmar con contabilidad
            "DocCurrency" : DocCurrency,
            "CashSum" : TotalMonto,
            "Reference1" : "",
            "Reference2" : "Desde el SWC",
            "Remarks" : Comments,
            "JournalRemarks" : Comentario_Nueva_Letra,
            "Series" : 124, #Serie PR02
            "PayToCode" : PayToCode,
            "IsPayToBank" : "tNO",
            "PaymentPriority" : "bopp_Priority_6",
            "PaymentType" : "bopt_None",
            "DocObjectCode" : "bopot_IncomingPayments",
            "DocTypte" : "rCustomer",
            "ControlAccount" : "1232101", #temporal por confirmar con contabilidad
            "U_MSSL_CPP" : "N", #Percepcion
            "U_MSS_ANLR" : "N", #Reversion 
            "U_SCR_TOS" : "0", #Tipo de Operacion
            "U_SCR_MGD" : "N", #Doc Enviado SICER
            "U_MSS_ESRE" : "0", #Estado de reversion
            "U_MSSM_CRM" : "N", #Creado desde el movil
            "U_MSSM_TRM" : "01", #Transaccion Movil
            "U_MSS_ESTR" : "0", #Estado de reversion electronica            
            "PaymentInvoices" : Facturas
        }
        #print(CerrarDocs)
        #print("@" * 20)
        
        sMensajeCerrar = CerrarPagos(Cia,CerrarDocs)

        if(sMensajeCerrar[:26] != "Se genero el pago recibido"):
            return sMensajeCerrar

        NuevasLetras = ExtraerPrevioPagos(Cia,CondP,TotalMonto)

        #print("@" * 20)
                        
        #print(NuevasLetras)        

        dhoy = datetime.datetime.now()
        shoy = str(dhoy.year) + "-" + str(dhoy.month) + "-" + str(dhoy.day)
        sSeries = GetSeries(Cia)

        dict_Cabecera = {"DocType" : "dDocument_Service", "DocDate" : shoy, "CardCode" : CardCode, "CardName" : CardName, "Address" : Address, "NumAtCard" : "Letra ReFinanciada", "DocCurrency" : DocCurrency, 
                         "Comments" : "LETRA REFINANCIADA : " + Comentario_Nueva_Letra, "JournalMemo" : Comentario_Nueva_Letra, "Comments" : Comments, "PaymentGroupCode" : CondP, "SalesPersonCode" : 0, "Series" : int(sSeries['serie_letras_refinanciadas']) ,
                         "TaxDate" : shoy, "DocObjectCode" : "oInvoices", "ShipToCode" : ShipToCode, "FederalTaxID" : FederalTaxId, "PaymentMethod": "LET_EMITIDAS_MN",   #"FolioPrefixString": "LT", "FolioNumber": 0,
                         "DocumentSubType": "bod_InvoiceExempt", "Address2": Address, "DocumentStatus": "bost_Open", "PeriodIndicator": "Valor de p", "PayToCode": PayToCode, "IsPayToBank": "tNO",
                         "DownPaymentType": "dptInvoice", "Cancelled": "tNO","ControlAccount": "1232101"}
        
        sMensaje = GenerarNuevasLetras(Cia, dict_Cabecera,NuevasLetras)

        return sMensajeCerrar + "\n" + sMensaje
    
    except Exception as err:        
        return {"Resultado" : -1, "Mensaje" : f"Error al extraer datos : {err}"}
    
def PrevioLetrasRefinanciadas(pDocs:List[DocSAP]):
    try:       
        #print(pDocs)

        Cia = ""
        CardCode = ""
        CardName = ""
        PayToCode = ""
        Address = ""
        Comments = ""
        TotalMonto = 0
        DocCurrency = ""
        Comentario_Nueva_Letra = ""
        Linea = 0
        CondP = ""
        Facturas = []

        for filaTM in pDocs:            
            Cia = filaTM.cia
            CardCode = filaTM.cardcode
            CardName = filaTM.cardname
            PayToCode = filaTM.paytocode
            Address = filaTM.address
            Comments = filaTM.comments
            DocCurrency = filaTM.doccur
            
            if(filaTM.condp!=""):
                CondP = filaTM.condp

            TotalMonto = TotalMonto + (float(str(filaTM.doctotal).strip()) - float(str(filaTM.paidtodate).strip()))
            
            LineaDocumento = {  'LineNum': Linea, 'DocEntry': filaTM.docentry, 'SumApplied': filaTM.doctotal,'AppliedFC': 0.0, 'AppliedSys': 0.0, 'DocRate': 0.0, 
                                'DocLine': Linea, 'InvoiceType': "it_Invoice", 'DiscountPercent': 0.0,'PaidSum': filaTM.paidtodate}
            
            Comentario_Nueva_Letra += filaTM.foliopref + "-" + filaTM.folionum + " | "
            
            Facturas.append(LineaDocumento)

            Linea += 1

        CerrarDocs = {}#ListaFL
        seriePR = GetSeries(Cia)
        #print(seriePR["serie_pagos_recibidos"])
        CerrarDocs = {
            "DocType" : "rCustomer",
            "CardCode" : CardCode,
            "CardName" : CardName,
            "Address" : Address,
            "CashAccount" : "1233109",#temporal por confirmar con contabilidad
            "DocCurrency" : DocCurrency,
            "CashSum" : TotalMonto,           
            "Reference2" : "Desde el SWC",
            "Remarks" : Comentario_Nueva_Letra,
            "JournalRemarks" : Comments,
            "Series" :  seriePR["serie_pagos_recibidos"],#124, #Serie PR02
            "PayToCode" : PayToCode,
            "IsPayToBank" : "tNO",
            "PaymentPriority" : "bopp_Priority_6",
            "PaymentType" : "bopt_None",
            "DocObjectCode" : "bopot_IncomingPayments",
            "DocTypte" : "rCustomer",
            "ControlAccount" : "1232101", #temporal por confirmar con contabilidad
            "U_MSSL_CPP" : "N", #Percepcion
            "U_MSS_ANLR" : "N", #Reversion 
            "U_SCR_TOS" : "0", #Tipo de Operacion
            "U_SCR_MGD" : "N", #Doc Enviado SICER
            "U_MSS_ESRE" : "0", #Estado de reversion
            "U_MSSM_CRM" : "N", #Creado desde el movil
            "U_MSSM_TRM" : "01", #Transaccion Movil
            "U_MSS_ESTR" : "0", #Estado de reversion electronica
            "PaymentInvoices" : Facturas
        }
        
        return ExtraerPrevioPagos(Cia,CondP,TotalMonto)
    
    except Exception as err:        
        print(err)
        return {"Resultado" : -1, "Mensaje" : f"Error al extraer datos : {err}"}

def ExtraeConfiguracionLetrasRefinanciadas(pCia:str):
    try:
        conn = ConectaSQL_Produccion()

        sql_conf = "select cp.cia, cuenta_control,cuenta_detalle,codigo_impuesto,porcentaje_impuesto, serie,correlativo,afectacion_impuesto,serie_letra_int from parametros_letras_rf cp inner join parametros_letras_rf_det_nume cdn on cp.cia = cdn.cia"
        sql_conf += f" where cp.cia = '{pCia}'  and LEFT(serie,2) = RIGHT(YEAR(GETDATE()),2) and abs(right(serie,2)) = abs(month(getdate()))"
        #print(sql_conf)

        cur_conf = conn.cursor(as_dict = True)

        cur_conf.execute(sql_conf)

        return cur_conf.fetchall()
    except Exception as err:
        print(err)

def GuardarConfiguracionLetrasRefinanciadas(pCia:str, pUltimoCorrelativo:str, pSerie:str):
    try:
        conn = ConectaSQL_Produccion()

        sql_conf = f"update parametros_letras_rf_det_nume set correlativo = '{pUltimoCorrelativo}' where serie = '{pSerie}' and cia='{pCia}'"
        #print(sql_conf)
        cur_conf = conn.cursor()

        cur_conf.execute(sql_conf)

        conn.commit()

        print("Correlativo Actualizado")        
    except Exception as err:
        print(err)
        
def ExtraerDireccionSN(pCia:str,pSocioNegocio:str,pDireccion:str):
    try:
        conn =  conexion_SAP_Tecno()
        cur_dire = conn.cursor()
        sql = f"select \"Street\" as Calle ,\"Block\" as Bloque,\"ZipCode\" as CodigoPostal,\"City\" as Ciudad,\"County\" as Provincia,\"State\" as Departamento from {pCia}.CRD1 where \"CardCode\" = '{pSocioNegocio}' AND \"Address\" = '{pDireccion}'"
        cur_dire.execute(sql)
        dDireccion = {}
        for i in cur_dire:
            dDireccion = {"Calle":i['Calle'],"Bloque":i['Bloque'],"CodigoPostal":i['CodigoPostal'],"Ciudad":i['Ciudad'],"Provincia":i['Provincia'],"Departamento":i['Departamento']}
        return dDireccion
    except Exception as err:
        return {"Error" : f"mensaje:{err}"}

def GuardarFacturaExtentaSAP(pCia:str,pNuevaLetra):
    try:
        print("genera letras....")
        resultado = []

        urllib3.disable_warnings()

        #sCia = CiaSAP_Test(pCia)

        token = logintoken(pCia)

        session = token['SessionId']

        payload = json.dumps(pNuevaLetra)

        header = {
            'Content-Type': 'application/json',
            'Cookie' : f'B1SESSION={session}; ROUTEID=.node3'
        }

        Servidor = getenv("SERVIDOR_SLD")

        url = f"{Servidor}/b1s/v1/Invoices"

        response = requests.request("POST", url, headers=header,data=payload,verify=False)

        if(response.status_code!=204):
            #print(response.json())
            resultado = response.json()
        
        mensaje = ""

        if("error" in resultado):
            mensaje = {"resultado" : -1, "error" : resultado['error']['message']['value']}
        else:
            #print("Error"*3)
            #print(resultado)
            mensaje = {"resultado" : 0, "mensaje" : resultado}

        return mensaje
    except Exception as err:
        print(err)
        return f"Error en Guardar Factura en SAP : {err}"

def GenerarNuevasLetras(pCia, pCabecera, pNL):
    try:
        print("nuevas letras")
        dconfiguracion = ExtraeConfiguracionLetrasRefinanciadas(pCia)
        #print(pCia)
        #print(dconfiguracion)
        sCia = ""
        sCuentaControl = ""
        sCuentaDetalle = ""
        sCodigoImpuesto = ""
        fPorcentaje = 0.00
        sSerie = ""
        sCorrelativo = ""
        sCorrelativoSiguiente = ""
        sSerieInt = ""
        sMensaje = ""
        sMensajeTotal = ""

        for config in dconfiguracion:
            sCia = config["cia"]
            sCuentaControl = config["cuenta_control"]
            sCuentaDetalle = config["cuenta_detalle"]
            sCodigoImpuesto = config["codigo_impuesto"]
            fPorcentaje = config["porcentaje_impuesto"]
            sSerie = config["serie"]
            sCorrelativo = config["correlativo"]
            sSerieInt =  config['serie_letra_int']
            sAfectacionImpuesto = config["afectacion_impuesto"]
            sCondicionPago = 472
        
        sNroSiguiente = int(sCorrelativo)
        
        #print(sNroSiguiente)

        xCia = CiaSAP(sCia)
        
        print(pNL)

        for iNuevaLetra in pNL:
            
            sCorrelativoSiguiente = '{:04d}'.format(sNroSiguiente)
        
            dDireccion = ExtraerDireccionSN(xCia,pCabecera["CardCode"],pCabecera["PayToCode"])
            iSerie = GetSeries(sCia)
            #print('Serie')
            #print(iSerie)
            
            FacturaR = {"DocType" : pCabecera["DocType"] , 
                        "PaymentGroupCode" : sCondicionPago,
                        "DocDate" : pCabecera["DocDate"] , 
                        "DocDueDate" : iNuevaLetra["Fecha_Letra"],
                        "CardCode" : pCabecera["CardCode"], 
                        "CardName" : pCabecera["CardName"], 
                        "Address" : pCabecera["Address"], 
                        "NumAtCard" :  "LTR/" + sSerie + "-" + sCorrelativoSiguiente + "-REFINANCIADA",
                        "DocTotal" : iNuevaLetra["Monto_Letra"],
                        "DocCurrency" : pCabecera["DocCurrency"],
                        "Comments" : pCabecera["NumAtCard"], 
                        "JournalMemo" : pCabecera["JournalMemo"], 
                        #pCabecera["PaymentGroupCode"], 
                        "SalesPersonCode" : pCabecera["SalesPersonCode"], 
                        "TaxDate" : pCabecera["TaxDate"],
                        "Series" : int(iSerie["serie_letras_refinanciadas"]),#pCabecera["Series"],
                        #"ShipToCode" : pCabecera["ShipToCode"], 
                        "FederalTaxID" : pCabecera["FederalTaxID"], 
                        "PaymentMethod": pCabecera["PaymentMethod"], 
                        "FolioPrefixString" : sSerieInt, 
                        "FolioNumber": sNroSiguiente,
                        "DocumentSubType": pCabecera["DocumentSubType"], 
                        "Address2": pCabecera["Address2"], 
                        "DocumentStatus": pCabecera["DocumentStatus"], 
                        "PayToCode": pCabecera["PayToCode"], 
                        "IsPayToBank": pCabecera["IsPayToBank"], 
                        "DownPaymentType": pCabecera["DownPaymentType"], 
                        "U_VLE_RENOV": "N",
                        "U_VLE_DOC" : pCabecera["JournalMemo"],
                        "ControlAccount": sCuentaControl,                         
                        "DocumentLines" : 
                        [
                            { "LineNum" : 0, 
                                "ItemCode" : None,
                                "ItemDescription" : "LETRA EN REFINANCIACION" , 
                                "Quantity" : 0, 
                                "Price" : iNuevaLetra["Monto_Letra"], 
                                "Currency" : pCabecera["DocCurrency"], 
                                "AccountCode" : sCuentaDetalle, 
                                "TaxCode": sCodigoImpuesto, 
                                "LineTotal": iNuevaLetra["Monto_Letra"], 
                                "TaxPercentagePerRow": 0.0, 
                                "TaxTotal": 0.0, 
                                "U_MSSC_BONI": "N",
                                "U_MSS_ITEMBONIF": "N", 
                                "U_MSSL_ICBPER": "N", 
                                "U_MSSF_AFEC": sAfectacionImpuesto,
                                "LineTaxJurisdictions": [
                                    {"JurisdictionCode": sCodigoImpuesto,
                                        "JurisdictionType": 7,
                                        "TaxAmount": 0.0,
                                        "TaxAmountSC": 0.0,
                                        "TaxAmountFC": 0.0,"TaxRate": 0.0,
                                        "LineNumber": 0,"RowSequence": 0}]
                                    }
                        ],
                        #"DocumentInstallments" : 
                        #[
                        #    {"DueDate" : iNuevaLetra["Fecha_Letra"], "Percentage" : 100.0, "Total" : iNuevaLetra["Monto_Letra"], "PaymentOrdered": "tNO"}
                        #],
                        "TaxExtension" : 
                            {
                                "StreetS": dDireccion['Calle'],               
                                "CityS": dDireccion['Ciudad'],
                                "ZipCodeS": dDireccion['CodigoPostal'],
                                "CountyS": dDireccion['Provincia'],
                                "StateS": dDireccion['Departamento'],
                                "CountryS": "PE",
                                "StreetB": dDireccion['Calle'],               
                                "CityB": dDireccion['Ciudad'],
                                "ZipCodeB": dDireccion['CodigoPostal'],
                                "CountyB": dDireccion['Provincia'],
                                "StateB": dDireccion['Departamento'],
                                "CountryB": "PE",
                                "ImportOrExportType": "et_IpmortsOrExports"
                            },
                        "AddressExtension" : 
                            {
                                "ShipToStreet": dDireccion['Calle'],        
                                "ShipToCity": dDireccion['Ciudad'],
                                "ShipToZipCode": dDireccion['CodigoPostal'],
                                "ShipToCounty": dDireccion['Provincia'],
                                "ShipToState": dDireccion['Departamento'],
                                "ShipToCountry": "PE",        
                                "BillToStreet": dDireccion['Calle'],        
                                "BillToCity": dDireccion['Ciudad'],
                                "BillToZipCode": dDireccion['CodigoPostal'],
                                "BillToCounty": dDireccion['Provincia'],
                                "BillToState": dDireccion['Departamento'],
                                "BillToCountry": "PE",        
                                "U_MSSC_PRVS": "N",
                                "U_MSSC_PRVB": "Y",        
                                "U_MSS_DVLUS": "N",
                                "U_MSS_DVLUB": "N",
                                "U_MSS_DVMAS": "N",
                                "U_MSS_DVMAB": "N",
                                "U_MSS_DVMIS": "N",
                                "U_MSS_DVMIB": "N",
                                "U_MSS_DVJUS": "N",
                                "U_MSS_DVJUB": "N",
                                "U_MSS_DVVIS": "N",
                                "U_MSS_DVVIB": "N",
                                "U_MSS_DVSAS": "N",
                                "U_MSS_DVSAB": "N",
                                "U_MSS_DVDOS": "N",
                                "U_MSS_DVDOB": "N",
                                "U_MSS_FRECS": "000",
                                "U_MSS_FRECB": "000"
                            }
                        }
            
            print(FacturaR)

            sMensajeLR = GuardarFacturaExtentaSAP(xCia,FacturaR)
            #print("@"*5)        
            #print(sMensajeLR)

            if ("error" in sMensajeLR):
                stextoMensaje = sMensajeLR["error"]
                sMensaje = f"No se genero por lo siguiente : { stextoMensaje }."
            else:
                sMensaje = f"Se genero la Letra {sSerie}-{sCorrelativoSiguiente}."

            if(sMensajeTotal==""):
                sMensajeTotal = sMensaje + "\n" 
            else:
                sMensajeTotal = sMensajeTotal + sMensaje + "\n"
            
            sNroSiguiente = sNroSiguiente + 1

        sCorrelativoSiguiente = '{:04d}'.format(sNroSiguiente)

        GuardarConfiguracionLetrasRefinanciadas(sCia, sCorrelativoSiguiente, sSerie)

        return sMensajeTotal
    
    except Exception as err:        
        print(f"Nuevo error: {err}")
        return err