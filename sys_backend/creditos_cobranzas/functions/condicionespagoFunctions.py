from conexiones.conexion_sap_solo import conexion_SAP_Tecno
from creditos_cobranzas.functions.loginSLDFunction import logintoken
from os import getenv
import requests
import json
import urllib3
from typing import List
import datetime
from generales.generales import CiaSAP

def ListaCondPago(pCia:str):
    ListaCP = []

    sCia = CiaSAP(pCia)

    conn = conexion_SAP_Tecno()

    cur_condp = conn.cursor()

    sqlCP =  f"SELECT \"GroupNum\" as Codigo,\"PymntGroup\" as Nombre FROM {sCia}.OCTG o WHERE U_TEC_CLPA = 'L' AND \"PymntGroup\" LIKE '%LT%' AND \"DiscCode\" = 'V' ORDER BY \"PymntGroup\" "

    cur_condp.execute(sqlCP)

    for fila in cur_condp:
        filacp = {"Codigo":fila["Codigo"],"Nombre":fila["Nombre"]}
        ListaCP.append(filacp)

    return ListaCP

def ExtraerPrevioPagos(pCia : str, pConPID : str, pMonto : float):
    listaDiasPago = []
    #print(pMonto)
    sCia = CiaSAP(pCia)

    conn = conexion_SAP_Tecno()

    cur_pagos = conn.cursor()

    sqlPagos = f"SELECT \"IntsNo\" as Orden,\"InstDays\" as Dias,\"InstPrcnt\" as Porcentaje FROM {sCia}.CTG1 WHERE \"CTGCode\" = {pConPID} ORDER BY \"CTGCode\", \"IntsNo\" "
    print(sqlPagos)
    cur_pagos.execute(sqlPagos)

    dHoy = datetime.datetime.now()
    
    for fila in cur_pagos:
        filacp = {"Orden": fila["Orden"],"Fecha_Letra" : (dHoy + datetime.timedelta(days=fila["Dias"])).strftime("%Y-%m-%d"),   "Monto_Letra" : round(pMonto * (float(fila["Porcentaje"])) / 100 ,2)}
        listaDiasPago.append(filacp)

    print(listaDiasPago)

    return listaDiasPago