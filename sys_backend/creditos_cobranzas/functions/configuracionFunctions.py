from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from conexiones.conexion_mmsql import conexion_mssql
from creditos_cobranzas.schemas.configuracionSchemas import Configuracion

def getConfig(pCia):
    try:
        resultado = []
        conn = ConectaSQL_Produccion()
        cur_lista = conn.cursor(as_dict = True)

        sqlLista = f"select cia,cuenta_control,cuenta_detalle,codigo_impuesto,porcentaje_impuesto,afectacion_impuesto from parametros_letras_rf where cia='{pCia}'"

        cur_lista.execute(sqlLista)

        for f in cur_lista:
            fila = {"cia":f['cia'],"cuenta_control":f['cuenta_control'],"cuenta_detalle":f['cuenta_detalle'],"codigo_impuesto":f['codigo_impuesto'],
                    "porcentaje_impuesto":f['porcentaje_impuesto'],"afectacion_impuesto":f['afectacion_impuesto']}
            resultado.append(fila)       

        conn.close()

        return resultado
    except Exception as err:
        print(err)
        return{"error":err}


def AgregarConfig(pData):   
    valores = []
    valores.append(pData.cia)
    valores.append(pData.cuenta_control)
    valores.append(pData.cuenta_detalle)
    valores.append(pData.codigo_impuesto)
    valores.append(pData.porcentaje_impuesto)
    valores.append(pData.afectacion_impuesto)

    sql = "insert into parametros_letras_rf(cia,cuenta_control,cuenta_detalle,codigo_impuesto,porcentaje_impuesto,afectacion_impuesto)"
    sql += " values(%s,%s,%s,%s,%s,%s)"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql,tuple(valores))

    return respuesta

def ActualizarConfig(pData):
    valores = []
    valores.append(pData.cuenta_control)
    valores.append(pData.cuenta_detalle)
    valores.append(pData.codigo_impuesto)
    valores.append(pData.porcentaje_impuesto)
    valores.append(pData.afectacion_impuesto)
    valores.append(pData.cia)

    sql = "update parametros_letras_rf set cuenta_control=%s , cuenta_detalle=%s , codigo_impuesto=%s , porcentaje_impuesto=%s , afectacion_impuesto=%s where cia=%s"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql,tuple(valores))

    return respuesta

def EliminarConfig(pData):
    valores = []
    valores.append(pData.cia)

    sql = "delete from parametros_letras_rf where cia=%s"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql,tuple(valores))
    
    return respuesta