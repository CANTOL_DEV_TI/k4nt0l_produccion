from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.exception_handlers import http_exception_handler,request_validation_exception_handler

from creditos_cobranzas.functions.configuracionFunctions import getConfig,AgregarConfig,ActualizarConfig,EliminarConfig
from creditos_cobranzas.schemas.configuracionSchemas import Configuracion

ruta = APIRouter(
    prefix="/configuracion", tags=["Configuracion de Refinanciamiento"]
)

@ruta.get('/ver/{pCia}',status_code=status.HTTP_302_FOUND)
async def VerConfiguracion(pCia:str):
    return getConfig(pCia)

@ruta.post('/',status_code=status.HTTP_201_CREATED)
async def InsertaConfig(info:Configuracion):
    return AgregarConfig(info)

@ruta.put('/',status_code=status.HTTP_202_ACCEPTED)
async def UpdateConfig(info:Configuracion):
    return ActualizarConfig(info)

@ruta.delete('/',status_code=status.HTTP_202_ACCEPTED)
async def DeleteConfig(info:Configuracion):
    return EliminarConfig(info)
