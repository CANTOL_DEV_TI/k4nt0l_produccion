from fastapi import APIRouter 
 
from creditos_cobranzas.routes import renovacionletras,condicionespago,configuracion
 
ruta = APIRouter( 
    prefix="/creditoscobranzas", 
) 

ruta.include_router(configuracion.ruta) 
ruta.include_router(renovacionletras.ruta)
ruta.include_router(condicionespago.ruta)