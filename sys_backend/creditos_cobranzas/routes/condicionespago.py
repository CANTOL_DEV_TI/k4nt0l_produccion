from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.exception_handlers import http_exception_handler,request_validation_exception_handler

from creditos_cobranzas.functions.condicionespagoFunctions import ListaCondPago,ExtraerPrevioPagos
from creditos_cobranzas.schemas.renovacionesletrasSchemas import Filtros

ruta = APIRouter(
    prefix="/condpago",tags=["Condiciones de pago"]
)

@ruta.get('/listado/{pCia}',status_code=status.HTTP_200_OK)
def listaCP(pCia:str):
    return ListaCondPago(pCia)

@ruta.get('/pagosxcond/{pCia}-{pCP}-{pMonto}',status_code=status.HTTP_200_OK)
def ListaCantPagos(pCia:str,pCP:str,pMonto:float):
    return ExtraerPrevioPagos(pCia,pCP,pMonto)
