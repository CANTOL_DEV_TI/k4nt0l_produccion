from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.exception_handlers import http_exception_handler,request_validation_exception_handler

from creditos_cobranzas.functions.renovacionletrasFunctions import ListadoLetras,ListaClientes,RefinanciarDocumentos,PrevioLetrasRefinanciadas
from creditos_cobranzas.schemas.renovacionesletrasSchemas import Filtros
from creditos_cobranzas.schemas.refinanciarfacturasSAPSchemas import ListaFL
from creditos_cobranzas.schemas.facturasletrasSAPSchemas import DocSAP
from typing import List

ruta = APIRouter(
    prefix="/renovaciones",tags=["Renovaciones"]
)

# @ruta.get('/ListarRecepciones/{txtFind}')
@ruta.post('/listado/')
def listado(info:Filtros):
    return ListadoLetras(info)

@ruta.get('/listaclientes/{xCia}-{xFiltro}/', status_code=status.HTTP_200_OK)
def listaClientes(xCia:str,xFiltro:str):    
    return ListaClientes(xCia, xFiltro)

@ruta.post('/refinanciar/')
def refinanciar(info:List[DocSAP]):
    return RefinanciarDocumentos(info)

@ruta.put('/refinanciarprevio/')
def refinanciarprevio(info:List[DocSAP]):
    return PrevioLetrasRefinanciadas(info)