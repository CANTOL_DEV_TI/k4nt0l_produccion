from typing import Optional
from pydantic import BaseModel

class Filtros(BaseModel):
    Compañia : Optional[str]
    Proveedor : Optional[str]
    Desde : Optional[str]
    Hasta : Optional[str]

    class config :
        orm_mode = True