from typing import Optional
from pydantic import BaseModel

class Configuracion(BaseModel):
    cia	: Optional[str]
    cuenta_control : Optional[str]
    cuenta_detalle : Optional[str]
    codigo_impuesto : Optional[str]
    porcentaje_impuesto : Optional[int]
    afectacion_impuesto : Optional[str]

    class config :
        orm_mode : True