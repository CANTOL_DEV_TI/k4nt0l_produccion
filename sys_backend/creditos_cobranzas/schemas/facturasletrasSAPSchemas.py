from typing import Optional,List
from pydantic import BaseModel

class DocSAP(BaseModel):
    cia : Optional[str]
    condp : Optional[str]
    indice : Optional[int]
    docentry : Optional[str]
    seriesname : Optional[str]
    lictradnum : Optional[str]
    cardcode : Optional[str]
    cardname : Optional[str]
    doccur : Optional[str]
    docduedate : Optional[str]
    paidtodate : Optional[float]
    docnum : Optional[str]
    foliopref : Optional[str]
    folionum : Optional[str]
    doctotal : Optional[float]
    paytocode : Optional[str]
    address : Optional[str]
    comments: Optional[str]
    shiptocode : Optional[str]

#class Documentos(BaseModel):
#    Facturas : Optional[List[DocSAP]]

    class config : 
        orm_mode : True