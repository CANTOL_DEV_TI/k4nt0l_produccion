from typing import Optional,List
from pydantic import BaseModel

class AddressExtension (BaseModel): 
        ShipToStreet: Optional[str]
        ShipToStreetNo: Optional[str]
        ShipToBlock: Optional[str]
        ShipToBuilding: Optional[str]
        ShipToCity: Optional[str] #ANCON
        ShipToZipCode: Optional[str] #150102,
        ShipToCounty: Optional[str] #LIMA,
        ShipToState: Optional[str] #15,
        ShipToCountry: Optional[str] #PE,
        ShipToAddressType: Optional[str]
        BillToStreet: Optional[str]#MZA. G8 LOTE. 14 A.H. LOS ROSALES,
        BillToStreetNo: Optional[str]
        BillToBlock: Optional[str]
        BillToBuilding: Optional[str]
        BillToCity: Optional[str] #ANCON,
        BillToZipCode: Optional[str] #150102,
        BillToCounty: Optional[str] #LIMA,
        BillToState: Optional[str] #15,
        BillToCountry: Optional[str] #PE,
        BillToAddressType: Optional[str]
        ShipToGlobalLocationNumber: Optional[str]
        BillToGlobalLocationNumber: Optional[str]
        ShipToAddress2: Optional[str]
        ShipToAddress3: Optional[str]
        BillToAddress2: Optional[str]
        BillToAddress3: Optional[str]
        PlaceOfSupply: Optional[str]
        PurchasePlaceOfSupply: Optional[str]
        DocEntry: Optional[int]
        GoodsIssuePlaceBP: Optional[str]
        GoodsIssuePlaceCNPJ: Optional[str]
        GoodsIssuePlaceCPF: Optional[str]
        GoodsIssuePlaceStreet: Optional[str]
        GoodsIssuePlaceStreetNo: Optional[str]
        GoodsIssuePlaceBuilding: Optional[str]
        GoodsIssuePlaceZip: Optional[str]
        GoodsIssuePlaceBlock: Optional[str]
        GoodsIssuePlaceCity: Optional[str]
        GoodsIssuePlaceCounty: Optional[str]
        GoodsIssuePlaceState: Optional[str]
        GoodsIssuePlaceCountry: Optional[str]
        GoodsIssuePlacePhone: Optional[str]
        GoodsIssuePlaceEMail: Optional[str]
        GoodsIssuePlaceDepartureDate: Optional[str]
        DeliveryPlaceBP: Optional[str]
        DeliveryPlaceCNPJ: Optional[str]
        DeliveryPlaceCPF: Optional[str]
        DeliveryPlaceStreet: Optional[str]
        DeliveryPlaceStreetNo: Optional[str]
        DeliveryPlaceBuilding: Optional[str]
        DeliveryPlaceZip: Optional[str]
        DeliveryPlaceBlock: Optional[str]
        DeliveryPlaceCity: Optional[str]
        DeliveryPlaceCounty: Optional[str]
        DeliveryPlaceState: Optional[str]
        DeliveryPlaceCountry: Optional[str]
        DeliveryPlacePhone: Optional[str]
        DeliveryPlaceEMail: Optional[str]
        DeliveryPlaceDepartureDate: Optional[str]
        U_MSS_RUTAS: Optional[str]
        U_MSS_RUTAB: Optional[str]
        U_MSS_NOVES: Optional[str]
        U_MSS_NOVEB: Optional[str]
        U_MSS_CANAS: Optional[str]
        U_MSS_CANAB: Optional[str]
        U_MSS_ZONAS: Optional[str]
        U_MSS_ZONAB: Optional[str]
        U_MSS_GIROS: Optional[str]
        U_MSS_GIROB: Optional[str]
        U_MSSC_SCLS: Optional[str]
        U_MSSC_SCLB: Optional[str]
        U_MSSC_PRVS: Optional[str] #N,
        U_MSSC_PRVB: Optional[str] #N,
        U_MSSM_LATS: Optional[str]
        U_MSSM_LATB: Optional[str]
        U_MSSM_LONS: Optional[str]
        U_MSSM_LONB: Optional[str]
        U_MSSM_FIVS: Optional[str]
        U_MSSM_FIVB: Optional[str]
        U_MSS_COVES: Optional[112] #112
        U_MSS_COVEB: Optional[112] #112
        U_MSS_DVLUS: Optional[str] #N
        U_MSS_DVLUB: Optional[str] #N
        U_MSS_DVMAS: Optional[str] #N
        U_MSS_DVMAB: Optional[str] #N
        U_MSS_DVMIS: Optional[str] #N
        U_MSS_DVMIB: Optional[str] #N
        U_MSS_DVJUS: Optional[str] #N
        U_MSS_DVJUB: Optional[str] #N
        U_MSS_DVVIS: Optional[str] #N
        U_MSS_DVVIB: Optional[str] #N
        U_MSS_DVSAS: Optional[str] #N
        U_MSS_DVSAB: Optional[str] #N
        U_MSS_DVDOS: Optional[str] #N
        U_MSS_DVDOB: Optional[str] #N
        U_MSS_FRECS: Optional[int] #000
        U_MSS_FRECB: Optional[int] #N000,
        U_MSSC_ZCLIS: Optional[str] #03
        U_MSSC_ZCLIB: Optional[str] #sN#,
        U_MSSFE_LONS: Optional[str] 
        U_MSSFE_LONB: Optional[str] 
        U_MSSFE_LATS: Optional[str] #N
        U_MSSFE_LATB: Optional[str] #N
        U_MSSFE_COD_ESTS: Optional[str] #N
        U_MSSFE_COD_ESTB: Optional[str] #N
    
class TaxExtension(BaseModel) : 
    TaxId0: Optional[str]
    TaxId1: Optional[str]
    TaxId2: Optional[str]
    TaxId3: Optional[str]
    TaxId4: Optional[str]
    TaxId5: Optional[str]
    TaxId6: Optional[str]
    TaxId7: Optional[str]
    TaxId8: Optional[str]
    TaxId9: Optional[str]
    State: Optional[str]
    County: Optional[str]
    Incoterms: Optional[str]
    Vehicle: Optional[str]
    VehicleState: Optional[str]
    NFRef: Optional[str]
    Carrier: Optional[str]
    PackQuantity: Optional[int]
    PackDescription: Optional[str]
    Brand: Optional[str]
    ShipUnitNo: Optional[str]
    NetWeight: Optional[float]
    GrossWeight: Optional[float]
    StreetS: Optional[str]
    BlockS: Optional[str]
    BuildingS: Optional[str]
    CityS: Optional[str]
    ZipCodeS: Optional[str]
    CountyS: Optional[str]
    StateS: Optional[str]
    CountryS: Optional[str]
    StreetB: Optional[str]
    BlockB: Optional[str]
    BuildingB: Optional[str]
    CityB: Optional[str]
    ZipCodeB: Optional[str]
    CountyB: Optional[str]
    StateB: Optional[str]
    CountryB: Optional[str]
    ImportOrExport: Optional[str]
    MainUsage: Optional[str]
    GlobalLocationNumberS: Optional[str]
    GlobalLocationNumberB: Optional[str]
    TaxId12: Optional[str]
    TaxId13: Optional[str]
    BillOfEntryNo:Optional[str] 
    BillOfEntryDate: Optional[str]
    OriginalBillOfEntryNo: Optional[str]
    OriginalBillOfEntryDate: Optional[str]
    ImportOrExportType: Optional[str] #et_IpmortsOrExports,
    PortCode: Optional[str]
    DocEntry: Optional[int]
    BoEValue: Optional[float]
    ClaimRefund: Optional[str]
    DifferentialOfTaxRate: Optional[str]
    IsIGSTAccount: Optional[str]

class LineTaxJurisdictions(BaseModel):                
    JurisdictionCode: Optional[str] #IGV_EXE,
    JurisdictionType: Optional[int] #7,
    TaxAmount: Optional[float]
    TaxAmountSC: Optional[float]
    TaxAmountFC: Optional[float]
    TaxRate: Optional[float]
    DocEntry: Optional[int]
    LineNumber: Optional[int]
    RowSequence: Optional[int]
    ExternalCalcTaxRate: Optional[str]
    ExternalCalcTaxAmount: Optional[str]
    ExternalCalcTaxAmountFC: Optional[str]
    ExternalCalcTaxAmountSC: Optional[str]

class DocumentLines(BaseModel) :       
    LineNum: Optional[int]
    ItemCode: Optional[str]
    ItemDescription: Optional[str]#LETRA EN CARTERA,
    Quantity: Optional[float] #Optional[float],
    ShipDate: Optional[str]
    Price: Optional[float] #1007.60,
    PriceAfterVAT: Optional[float] #1007.60,
    Currency: Optional[str] #S/,
    Rate: Optional[float] #Optional[float],
    DiscountPercent: Optional[float] #Optional[float],
    VendorNum: Optional[str]
    SerialNum: Optional[str]
    WarehouseCode: Optional[str]
    SalesPersonCode: Optional[int] #vendedor
    CommisionPercent: Optional[float] #Optional[float],
    TreeType: Optional[str] #iNotATree,
    AccountCode: Optional[str] #1232109 por confirmar 
    UseBaseUnits: Optional[str] #tNO,
    SupplierCatNum: Optional[str]
    CostingCode: Optional[str]
    ProjectCode: Optional[str]
    BarCode: Optional[str]
    VatGroup: Optional[str]
    Height1: Optional[float] #Optional[float],
    Hight1Unit: Optional[str]
    Height2: Optional[float]
    Height2Unit: Optional[str]
    Lengh1: Optional[float]
    Lengh1Unit: Optional[str]
    Lengh2: Optional[float]
    Lengh2Unit: Optional[str]
    Weight1: Optional[float]
    Weight1Unit: Optional[str]
    Weight2: Optional[float]
    Weight2Unit: Optional[str]
    Factor1: Optional[float]
    Factor2: Optional[float]
    Factor3: Optional[float]
    Factor4: Optional[float]
    BaseType: Optional[int] #-1,
    BaseEntry: Optional[str]
    BaseLine: Optional[str]
    Volume: Optional[float]
    VolumeUnit: Optional[str]
    Width1: Optional[float]
    Width1Unit: Optional[str]
    Width2: Optional[float]
    Width2Unit: Optional[str]
    Address: Optional[str]
    TaxCode: Optional[str] #IGV_EXE,
    TaxType: Optional[str] #tt_Yes,
    TaxLiable: Optional[str] #tYES,
    PickStatus: Optional[str] #tNO,
    PickQuantity: Optional[float]
    PickListIdNumber: Optional[str]
    OriginalItem: Optional[str]
    BackOrder: Optional[str]
    FreeText: Optional[str]
    ShippingMethod: Optional[int] #-1
    POTargetNum: Optional[str]
    POTargetEntry: Optional[str]
    POTargetRowNum: Optional[str]
    CorrectionInvoiceItem: Optional[str] #ciis_ShouldBe,
    CorrInvAmountToStock: Optional[float]
    CorrInvAmountToDiffAcct: Optional[float]
    AppliedTax: Optional[float]
    AppliedTaxFC: Optional[float]
    AppliedTaxSC: Optional[float]
    WTLiable: Optional[str] #tNO,
    DeferredTax: Optional[str] #tNO,
    EqualizationTaxPercent: Optional[float]
    TotalEqualizationTax: Optional[float]
    TotalEqualizationTaxFC: Optional[float]
    TotalEqualizationTaxSC: Optional[float]
    NetTaxAmount: Optional[float]
    NetTaxAmountFC: Optional[float]
    NetTaxAmountSC: Optional[float]
    MeasureUnit: Optional[str]
    UnitsOfMeasurment: Optional[float]
    LineTotal: Optional[float]#1007.60,
    TaxPercentagePerRow: Optional[float]
    TaxTotal: Optional[float] #impuesto
    ConsumerSalesForecast: Optional[str]
    ExciseAmount: Optional[float]
    TaxPerUnit: Optional[float]
    TotalInclTax: Optional[float]
    CountryOrg: Optional[str]
    SWW: Optional[str]
    TransactionType: Optional[str]
    DistributeExpense: Optional[str] #tYES,
    RowTotalFC: Optional[float]
    RowTotalSC: Optional[float]#265.020,
    LastBuyInmPrice: Optional[float]
    LastBuyDistributeSumFc: Optional[float]
    LastBuyDistributeSumSc: Optional[float]
    LastBuyDistributeSum: Optional[float]
    StockDistributesumForeign: Optional[float]
    StockDistributesumSystem: Optional[float]
    StockDistributesum: Optional[float]
    StockInmPrice: Optional[float]
    PickStatusEx: Optional[str] #dlps_NotPicked
    TaxBeforeDPM: Optional[float]
    TaxBeforeDPMFC: Optional[float]
    TaxBeforeDPMSC: Optional[float]
    CFOPCode: Optional[str]
    CSTCode: Optional[str]
    Usage: Optional[str]
    TaxOnly: Optional[str] #tNO,
    VisualOrder: Optional[int]
    BaseOpenQuantity: Optional[float]
    UnitPrice: Optional[float] #1007.60,
    LineStatus: Optional[str] #bost_Open
    PackageQuantity: Optional[float]
    Text: Optional[str]
    LineType: Optional[str] #dlt_Regular
    COGSCostingCode: Optional[str]
    COGSAccountCode: Optional[str]
    ChangeAssemlyBoMWarehouse: Optional[str]
    GrossBuyPrice: Optional[float]
    GrossBase: Optional[int] #-11,
    GrossProfitTotalBasePrice: Optional[float] #1007.60,
    CostingCode2: Optional[str]
    CostingCode3: Optional[str]
    CostingCode4: Optional[str]
    CostingCode5: Optional[str]
    ItemDetails: Optional[str]
    LocationCode: Optional[str]
    ActualDeliveryDate: Optional[str]
    RemainingOpenQuantity: Optional[float]
    OpenAmount: Optional[float] #1007.60,
    OpenAmountFC: Optional[float]
    OpenAmountSC: Optional[float] #265.020,
    ExLineNo: Optional[str]
    RequiredDate: Optional[str]
    RequiredQuantity: Optional[float]
    COGSCostingCode2: Optional[str]
    COGSCostingCode3: Optional[str]
    COGSCostingCode4: Optional[str]
    COGSCostingCode5: Optional[str]
    CSTforIPI: Optional[str]
    CSTforPIS: Optional[str]
    CSTforCOFINS: Optional[str]
    CreditOriginCode: Optional[str]
    WithoutInventoryMovement: Optional[str] #tNO,
    AgreementNo: Optional[str]
    AgreementRowNumber: Optional[str]
    ActualBaseEntry: Optional[str]
    ActualBaseLine: Optional[str]
    DocEntry: Optional[int]
    Surpluses: Optional[float]
    DefectAndBreakup: Optional[float]
    Shortages: Optional[float]
    ConsiderQuantity: Optional[str] #tNO,
    PartialRetirement: Optional[str] #tNO,
    RetirementQuantity: Optional[float]
    RetirementAPC: Optional[float]
    ThirdParty: Optional[str] #tNO,
    PoNum: Optional[str]
    PoItmNum: Optional[str]
    ExpenseType: Optional[str]
    ReceiptNumber: Optional[str]
    ExpenseOperationType: Optional[str]
    FederalTaxID: Optional[str]
    GrossProfit: Optional[float]
    GrossProfitFC: Optional[float]
    GrossProfitSC: Optional[float]
    PriceSource: Optional[str] #dpsManual,
    StgSeqNum: Optional[str]
    StgEntry: Optional[str]
    StgDesc: Optional[str]
    UoMEntry: Optional[int]
    UoMCode: Optional[str]
    InventoryQuantity: Optional[float]
    RemainingOpenInventoryQuantity: Optional[float]
    ParentLineNum: Optional[str]
    Incoterms: Optional[int]
    TransportMode: Optional[int]
    NatureOfTransaction: Optional[str]
    DestinationCountryForImport: Optional[str]
    DestinationRegionForImport: Optional[str]
    OriginCountryForExport: Optional[str]
    OriginRegionForExport: Optional[str]
    ItemType: Optional[str] #dit_Item,
    ChangeInventoryQuantityIndependently: Optional[str]#tNO,
    FreeOfChargeBP: Optional[str] #tNO,
    SACEntry: Optional[str]
    HSNEntry: Optional[str]
    GrossPrice: Optional[float] #1007.60,
    GrossTotal: Optional[float] #1007.60        
    GrossTotalFC: Optional[float]
    GrossTotalSC: Optional[float] #265.020,
    NCMCode: Optional[int] #-1,
    NVECode: Optional[str]
    IndEscala: Optional[str] #tNO,
    CtrSealQty: Optional[float]
    CNJPMan: Optional[str]
    CESTCode: Optional[str]
    UFFiscalBenefitCode: Optional[str]
    ShipToCode: Optional[str]
    ShipToDescription: Optional[str]
    ExternalCalcTaxRate: Optional[float]
    ExternalCalcTaxAmount: Optional[float]
    ExternalCalcTaxAmountFC: Optional[float]
    ExternalCalcTaxAmountSC: Optional[float]
    StandardItemIdentification: Optional[str]
    CommodityClassification: Optional[str]
    UnencumberedReason: Optional[str]
    U_MSSL_CGP: Optional[str]
    U_MSSL_CGD: Optional[str]
    U_MSSL_INR: Optional[float]
    U_MSS_STOCK: Optional[str]
    U_MSSC_MAQUILA: Optional[str]
    U_MSSC_DCAL: Optional[str]
    U_MSSC_ULPR: Optional[float]
    U_MSSC_NV1: Optional[str]
    U_MSSC_NV2: Optional[str]
    U_MSSC_NV3: Optional[str]
    U_MSSC_DSC: Optional[str]
    U_MSSC_FAM: Optional[str]
    U_MSSC_SUF: Optional[str]
    U_MSSC_DSCU: Optional[str]
    U_MSSC_BONI: Optional[str] #N,
    U_MSS_ITEMBONIF: Optional[str] #N,
    U_MSSC_MORE: Optional[str]
    U_MSSC_MONO: Optional[str]
    U_MSSC_LOTE: Optional[str]
    U_MSSF_AFEC: Optional[str]
    U_MSSF_CUTL: Optional[float]
    U_MSSF_CNML: Optional[float]
    U_MSSF_CEFT: Optional[float]
    U_MSSF_DDVI: Optional[str]
    U_MSSM_PRF: Optional[float]
    U_MSS_CMN: Optional[str]
    U_MSSC_DSC2: Optional[str]
    U_MSSL_ICBPER: Optional[str] #N,
    LineTaxJurisdictions: Optional[List[LineTaxJurisdictions]]
    ExportProcesses: Optional[List]
    EBooksDetails: Optional[List]
    DocumentLineAdditionalExpenses: Optional[List]
    WithholdingTaxLines: Optional[List]
    SerialNumbers: Optional[List]
    BatchNumbers: Optional[List]
    CCDNumbers: Optional[List]
    DocumentLinesBinAllocations: Optional[List]            
        
class DocumentInstallments(BaseModel) :
    DueDate: Optional[str]
    Percentage: Optional[float] #10.00
    Total: Optional[float] #1007.60,
    LastDunningDate: Optional[str]
    DunningLevel: Optional[int]
    TotalFC: Optional[float]
    InstallmentId: Optional[int]
    PaymentOrdered: Optional[str] #tNO        

class Incoming(BaseModel):
    DocEntry: Optional[int]
    DocNum: Optional[int]
    DocType: Optional[str] #dDocument_Service,
    HandWritten: Optional[str] #tNO,
    Printed: Optional[str] #psNo,
    DocDate: Optional[str]
    DocDueDate: Optional[str]
    CardCode: Optional[str]
    CardName: Optional[str]
    Address: Optional[str]
    NumAtCard: Optional[str] #LT/2407-0030-CARTERA,
    DocTotal: Optional[float]
    AttachmentEntry: Optional[str]
    DocCurrency: Optional[str] #S/,
    DocRate: Optional[float]
    Reference1: Optional[int] #Nom Pago
    Reference2: Optional[str]
    Comments: Optional[str] #LETRA EN CARTERA - LT/2407-0030,
    JournalMemo: Optional[str] #LT/2407-0030,
    PaymentGroupCode: Optional[int] #condicion de pago 472,
    DocTime: Optional[str]
    SalesPersonCode: Optional[str] #Vendedor
    TransportationCode: Optional[int] #-1,
    Confirmed: Optional[str] #tYES,
    ImportFileNum: Optional[str]
    SummeryType: Optional[str] #dNoSummary,
    ContactPersonCode: Optional[str]
    ShowSCN: Optional[str] #tNO,
    Series: Optional[int] #Serie de documento SAP
    TaxDate: Optional[str] #2024-07-05,
    PartialSupply: Optional[str] #tYES,
    DocObjectCode: Optional[str] #oInvoices,
    ShipToCode: Optional[str] #DIRECCION DE ALMACEN I,
    Indicator: Optional[str]
    FederalTaxID: Optional[str] #20608455087,
    DiscountPercent: Optional[float] #Optional[float],
    PaymentReference: Optional[str] #null,
    FinancialPeriod: Optional[str]
    TransNum: Optional[int] #1029755,
    VatSum: Optional[float]
    VatSumSys: Optional[float]
    VatSumFc: Optional[float]
    NetProcedure: Optional[str] #tNO,
    DocTotalFc: Optional[float]
    DocTotalSys: Optional[float]
    Form1099: Optional[str]
    Box1099: Optional[str]
    RevisionPo: Optional[str] #tNO,
    RequriedDate: Optional[str]
    CancelDate: Optional[str]
    BlockDunning: Optional[str] #tNO,
    Submitted: Optional[str] #tNO,
    Segment: Optional[int] #0
    PickStatus: Optional[str] #tNO,
    Pick: Optional[str] #tNO,
    PaymentMethod: Optional[str] #LET_EMITIDAS_MN,
    PaymentBlock: Optional[str] #tNO,
    PaymentBlockEntry: Optional[str]
    CentralBankIndicator: Optional[str]
    MaximumCashDiscount: Optional[str] #tNO,
    Reserve: Optional[str] #tNO,
    Project: Optional[str]
    ExemptionValidityDateFrom: Optional[str]
    ExemptionValidityDateTo: Optional[str]
    WareHouseUpdateType: Optional[str]#dwh_Stock,
    Rounding: Optional[str] #tNO,
    ExternalCorrectedDocNum: Optional[str]
    InternalCorrectedDocNum: Optional[str]
    NextCorrectingDocument: Optional[str]
    DeferredTax: Optional[str] #tNO,
    TaxExemptionLetterNum: Optional[str]
    WTApplied: Optional[float]
    WTAppliedFC: Optional[float]
    BillOfExchangeReserved: Optional[str] #tNO,
    AgentCode: Optional[str]
    WTAppliedSC: Optional[float]
    TotalEqualizationTax: Optional[float]
    TotalEqualizationTaxFC: Optional[float]
    TotalEqualizationTaxSC: Optional[float]
    NumberOfInstallments: Optional[int] #1
    ApplyTaxOnFirstInstallment: Optional[str] #tNO,
    WTNonSubjectAmount: Optional[float]
    WTNonSubjectAmountSC: Optional[float]
    WTNonSubjectAmountFC: Optional[float]
    WTExemptedAmount: Optional[float]
    WTExemptedAmountSC: Optional[float]
    WTExemptedAmountFC: Optional[float]
    BaseAmount: Optional[float]
    BaseAmountSC: Optional[float]
    BaseAmountFC: Optional[float]
    WTAmount: Optional[float]
    WTAmountSC: Optional[float]
    WTAmountFC: Optional[float]
    VatDate: Optional[str]
    DocumentsOwner: Optional[str]
    FolioPrefixString: Optional[str] #LT,
    FolioNumber: Optional[int] #30,
    DocumentSubType: Optional[str] #bod_InvoiceExempt,
    BPChannelCode: Optional[str]
    BPChannelContact: Optional[str]
    Address2: Optional[str]
    DocumentStatus: Optional[str] #bost_Open,
    PeriodIndicator: Optional[str]
    PayToCode: Optional[str] #DOMICILIO_FISCAL,
    ManualNumber: Optional[str]
    UseShpdGoodsAct: Optional[str] #tNO,
    IsPayToBank: Optional[str] #tNO,
    PayToBankCountry: Optional[str]
    PayToBankCode: Optional[str]
    PayToBankAccountNo: Optional[str]
    PayToBankBranch: Optional[str]
    BPL_IDAssignedToInvoice: Optional[str]
    DownPayment: Optional[float]
    ReserveInvoice: Optional[str] #tNO,    
    TrackingNumber: Optional[str]
    PickRemark: Optional[str]
    ClosingDate: Optional[str]
    SequenceCode: Optional[str]
    SequenceSerial: Optional[str]
    SeriesString: Optional[str]
    SubSeriesString: Optional[str]
    SequenceModel: Optional[int]
    UseCorrectionVATGroup: Optional[str] #tNO,
    TotalDiscount: Optional[float]
    DownPaymentAmount: Optional[float]
    DownPaymentPercentage: Optional[float]
    DownPaymentType: Optional[str] #dptInvoice
    DownPaymentAmountSC: Optional[float]
    DownPaymentAmountFC: Optional[float]
    VatPercent: Optional[float]
    ServiceGrossProfitPercent: Optional[float]
    OpeningRemarks: Optional[str]
    ClosingRemarks: Optional[str]
    RoundingDiffAmount: Optional[float]
    RoundingDiffAmountFC: Optional[float]
    RoundingDiffAmountSC: Optional[float] 
    Cancelled: Optional[str] #tNO,
    SignatureInputMessage: Optional[str]
    SignatureDigest: Optional[str]
    CertificationNumber: Optional[str]
    PrivateKeyVersion: Optional[str]
    ControlAccount: Optional[str] #1232101 por confirmar por contabilida y creditos
    InsuranceOperation347: Optional[str] #tNO,
    ArchiveNonremovableSalesQuotation: Optional[str] #tNO,
    GTSChecker: Optional[str]
    GTSPayee: Optional[str]
    ExtraMonth: Optional[int]
    ExtraDays: Optional[int]
    CashDiscountDateOffset: Optional[int]
    StartFrom: Optional[str] #pdt_None,
    NTSApproved: Optional[str] #tNO,
    ETaxWebSite: Optional[str]
    ETaxNumber: Optional[str]
    NTSApprovedNumber: Optional[str]
    EDocGenerationType: Optional[str] #edocNotRelevant,
    EDocSeries: Optional[str]
    EDocNum: Optional[str]
    EDocExportFormat: Optional[str]
    EDocStatus: Optional[str] #edoc_Ok,
    EDocErrorCode: Optional[str]
    EDocErrorMessage: Optional[str]
    DownPaymentStatus: Optional[str] #so_Open,
    GroupSeries: Optional[str]
    GroupNumber: Optional[str]
    GroupHandWritten: Optional[str] #tNO,
    ReopenOriginalDocument: Optional[str]
    ReopenManuallyClosedOrCanceledDocument: Optional[str]
    CreateOnlineQuotation: Optional[str] #tNO,
    POSEquipmentNumber: Optional[str]
    POSManufacturerSerialNumber: Optional[str]
    POSCashierNumber: Optional[str]
    ApplyCurrentVATRatesForDownPaymentsToDraw: Optional[str] #tNO,
    ClosingOption: Optional[str] #coByCurrentSystemDate,
    SpecifiedClosingDate: Optional[str]
    OpenForLandedCosts: Optional[str] #tYES,
    AuthorizationStatus: Optional[str] #dasWithout,
    TotalDiscountFC: Optional[float]
    TotalDiscountSC: Optional[float]
    RelevantToGTS: Optional[str] #tNO,
    BPLName: Optional[str]
    VATRegNum: Optional[str]
    AnnualInvoiceDeclarationReference: Optional[str]
    Supplier: Optional[str]
    Releaser: Optional[str]
    Receiver: Optional[str]
    BlanketAgreementNumber: Optional[str]
    IsAlteration: Optional[str] #tNO,
    CancelStatus: Optional[str] #csNo,
    AssetValueDate: Optional[str]
    InvoicePayment: Optional[str] #tNO,
    DocumentDelivery: Optional[str] #ddtNoneSeleted,
    AuthorizationCode: Optional[str]
    StartDeliveryDate: Optional[str]
    StartDeliveryTime: Optional[str]
    EndDeliveryDate: Optional[str]
    EndDeliveryTime: Optional[str]
    VehiclePlate: Optional[str]
    ATDocumentType: Optional[str]
    ElecCommStatus: Optional[str]
    ElecCommMessage: Optional[str]
    ReuseDocumentNum: Optional[str] #tNO,
    ReuseNotaFiscalNum: Optional[str] #tNO,
    PrintSEPADirect: Optional[str] #tNO,
    FiscalDocNum: Optional[str]
    POSDailySummaryNo: Optional[str]
    POSReceiptNo: Optional[str]
    PointOfIssueCode: Optional[str]
    Letter: Optional[str]
    FolioNumberFrom: Optional[str]
    FolioNumberTo: Optional[str]
    InterimType: Optional[str] #boidt_None,
    RelatedType: Optional[str] #-1,
    RelatedEntry: Optional[str]
    SAPPassport: Optional[str]
    DocumentTaxID: Optional[str]
    DateOfReportingControlStatementVAT: Optional[str]
    ReportingSectionControlStatementVAT: Optional[str]
    ExcludeFromTaxReportControlStatementVAT: Optional[str] #tNO,
    POS_CashRegister: Optional[str]    
    CreateQRCodeFrom: Optional[str]
    PriceMode: Optional[str] #pmdNet,
    Revision: Optional[str] #tNO,
    OriginalRefNo: Optional[str]
    OriginalRefDate: Optional[str]
    GSTTransactionType: Optional[str]
    OriginalCreditOrDebitNo: Optional[str]
    OriginalCreditOrDebitDate: Optional[str]
    ECommerceOperator: Optional[str]
    ECommerceGSTIN: Optional[str]
    ShipFrom: Optional[str] #DIRECCION DE ALMACEN I,
    CommissionTrade: Optional[str] #ct_Empty,
    CommissionTradeReturn: Optional[str] #tNO,
    UseBillToAddrToDetermineTax: Optional[str] #tNO,
    IssuingReason: Optional[str] #1,
    Cig: Optional[str]
    Cup: Optional[str]
    EDocType: Optional[str] #edocFE,
    PaidToDate: Optional[float] #Optional[float],
    PaidToDateFC: Optional[float] #Optional[float],
    PaidToDateSys: Optional[float] #Optional[float],
    BaseType: Optional[int] #-1,
    BaseEntry: Optional[str]
    FatherCard: Optional[str]
    FatherType: Optional[str] #cPayments_sum,
    ShipState: Optional[str]
    ShipPlace: Optional[str]
    CustOffice: Optional[str]
    FCI: Optional[str]
    U_MSSL_TOP: Optional[str]
    U_MSSL_TBI: Optional[str]
    U_MSSL_TDC: Optional[str]
    U_MSSL_CBS: Optional[str]
    U_MSSL_TDO: Optional[str]
    U_MSSL_SDO: Optional[str]
    U_MSSL_CDO: Optional[str]
    U_MSSL_FDO: Optional[str]
    U_MSSL_ADU: Optional[str]
    U_MSSL_CAD: Optional[str]
    U_MSSL_TRH: Optional[str]
    U_MSSL_PRH: Optional[str]
    U_MSSL_TAN: Optional[str]
    U_MSSL_ADT: Optional[str]
    U_MSSL_AUD: Optional[str]
    U_MSSL_CDT: Optional[str]
    U_MSSL_PDT: Optional[float]
    U_MSSL_MDT: Optional[float]
    U_MSSL_ODT: Optional[str]
    U_MSSL_TRT: Optional[str]
    U_MSSL_MSP: Optional[str]
    U_MSSL_DDT: Optional[str]
    U_MSSL_FDT: Optional[str]
    U_MSSL_MTR: Optional[str]
    U_MSSL_RTR: Optional[str]
    U_MSSL_NTR: Optional[str]
    U_MSSL_DTR: Optional[str]
    U_MSSL_NCD: Optional[str]
    U_MSSL_LCD: Optional[str]
    U_MSSL_MVH: Optional[str]
    U_MSSL_PVH: Optional[str]
    U_MSSL_TLV: Optional[str]
    U_MSSL_DEX: Optional[str]
    U_MSSL_EXP: Optional[str] #N,
    U_MSSL_RCI: Optional[str] #N,
    U_MSSL_RDT: Optional[str]
    U_MSSL_PDE: Optional[str]
    U_MSSL_FPC: Optional[str]
    U_MSSL_FNC: Optional[str]
    U_MSSL_PRT: Optional[str] #N,
    U_MSSL_BTD: Optional[str]
    U_MSSC_CDIS: Optional[str]
    U_MSSC_NREC: Optional[str]
    U_MSS_FETR: Optional[str]
    U_MSS_ORIG: Optional[str]
    U_MSS_DEST: Optional[str]
    U_MSS_RUCD: Optional[str]
    U_MSS_RZOD: Optional[str]
    U_MSS_CECI: Optional[str]
    U_MSSM_CRM: Optional[str] #N,
    U_Peso: Optional[str]
    U_MSS_DOCADI: Optional[str]
    U_MSS_IMPRO: Optional[str]
    U_MSS_CSNF: Optional[str]
    U_MSS_PRFI: Optional[str]
    U_MSS_NUFI: Optional[str]
    U_MSS_RSNF: Optional[str]
    U_MSS_PLN: Optional[str]
    U_MSS_FGTO: Optional[str] #1,
    U_MSSC_TGAS: Optional[str]
    U_MSSC_ZCLI: Optional[str]
    U_MSSC_NUIM: Optional[str]
    U_MSS_ESTADO: Optional[str] #CN,
    U_MSS_MTRAN: Optional[str]
    U_MSS_MAQUILA: Optional[str]
    U_MSS_TIPC: Optional[str]
    U_VLE_FACE: Optional[str] #2024-07-05,
    U_VLE_FEMI: Optional[str] #2024-07-05,
    U_VLE_DOCE: Optional[float] #131759,
    U_VLE_IMPE: Optional[float] #153.70,
    U_VLE_POPE: Optional[float] #2.0,
    U_VLE_DOC: Optional[str] #F001 - 23112,
    U_VLE_RENOV: Optional[str] #N,
    U_VLE_RLETR: Optional[str]
    U_VLE_TREN: Optional[str] #N,
    U_VLE_TDEP: Optional[str]
    U_VLE_NUNI: Optional[str]
    U_VLE_FDEP: Optional[str]
    U_VLE_CBAN: Optional[str]
    U_VLE_NBAN: Optional[str]
    U_VLE_CTAB: Optional[str]
    U_VLE_ILET: Optional[str]
    U_VLE_TGBA: Optional[str]
    U_VLE_PLDE: Optional[str]
    U_VLE_PLEM: Optional[str]
    U_VLE_NURE: Optional[str] #LT/2407-0030-CARTERA
    U_MSSI_CAC: Optional[str]
    U_MSSI_TEA: Optional[str]
    U_MSSI_MCA: Optional[str]
    U_MSSI_NAC: Optional[str]
    U_MSSC_MORE: Optional[str]
    U_MSSC_GRFA: Optional[str]
    U_MSSC_PROC: Optional[str]
    U_MSSC_FERE: Optional[str]
    U_MSSC_MONO: Optional[str]
    U_MSS_ORPT: Optional[str]
    U_SCR_ADJ: Optional[str]
    U_SCR_TOS: Optional[str] #0
    U_SCR_COS: Optional[str]
    U_SCR_COP: Optional[str]
    U_SCR_URN: Optional[str]
    U_SCR_MGD: Optional[str] #N,
    U_SCR_MMI: Optional[str]
    U_MSS_ESTLIQ: Optional[str]
    U_MSS_ESTRA: Optional[str]
    U_MSS_ESLI: Optional[str]
    U_MSSC_COM: Optional[str] #P
    U_MSSC_CRE: Optional[str] #P
    U_MSS_MONLIQ: Optional[float]
    U_MSS_MONEDA: Optional[str]
    U_MSS_NUMOP: Optional[str]
    U_MSS_TIPAG: Optional[str]
    U_MSS_MON: Optional[str]
    U_MSS_OBSE: Optional[str]
    U_MSS_CUENTA: Optional[str]
    U_MSS_ESRP: Optional[str] #-,
    U_MSS_ESTR: Optional[str]
    U_MSS_COMCOM: Optional[str]
    U_MSS_COMCRE: Optional[str]
    U_MSS_TSNP: Optional[str]
    U_MSS_OKBON: Optional[str] #N,
    U_MSSF_CEX1: Optional[str]
    U_MSSF_CEX2: Optional[str]
    U_MSSF_CEX3: Optional[str]
    U_MSSF_CEX4: Optional[str]
    U_MSSF_CEX5: Optional[str]
    U_MSSF_CEX6: Optional[str]
    U_MSSC_TIFO: Optional[str]
    U_MSSF_TDND: Optional[str]
    U_MSSF_TDNC: Optional[str]
    U_MSSL_TBA: Optional[str]
    U_MSS_FEEN: Optional[str]
    U_MSSF_FEST: Optional[str]
    U_MSSF_OBSF: Optional[str]
    U_MSSF_TIOP: Optional[str]
    U_MSSF_MTNT: Optional[str]
    U_MSSF_MPGO: Optional[str]
    U_MSSF_BEST: Optional[str] #0
    U_MSSF_BOBS: Optional[str]
    U_MSSF_BVAL: Optional[str] #N,
    U_MSSF_MBAJ: Optional[str]
    U_MSSF_PDF: Optional[str]
    U_MSSF_XML: Optional[str]
    U_MSSF_CDR: Optional[str]
    U_MSSF_MOTTRAS: Optional[str]
    U_MSSF_MODTRAN: Optional[str]
    U_MSSF_TDCO: Optional[str]
    U_MSSF_NDCO: Optional[str]
    U_MSSF_FINITRAS: Optional[str]
    U_MSSF_BUPA: Optional[str]
    U_MSSF_ACD: Optional[str]
    U_MSSF_TDRL: Optional[str]
    U_MSSF_NDRL: Optional[str]
    U_MSSF_CMT: Optional[str]
    U_MSSF_TRAP: Optional[bool] #false,
    U_MSSF_EGRA: Optional[str] #N,
    U_MSSF_INC: Optional[str] #FOB,
    U_MSSF_FEMB: Optional[str]
    U_MSSF_VIA: Optional[str]
    U_MSSF_NPED: Optional[str]
    U_MSSF_ORDC: Optional[str]
    U_MSSF_MTO: Optional[float] #Optional[float],
    U_MSSF_FPG: Optional[str]
    U_MSSM_CLM: Optional[str]
    U_MSSM_TRM: Optional[str] #01,
    U_MSSM_MOL: Optional[str] #N,
    U_MSSM_RAN: Optional[str] #03,
    U_MSSM_VEN: Optional[str]
    U_MSSM_IMG: Optional[str]
    U_MSSM_PFL: Optional[str] #N,
    U_MSSF_TELT: Optional[str]
    U_MSSF_UBIT: Optional[str]
    U_MSSF_DEPA: Optional[str]
    U_MSSF_PROVA: Optional[str]
    U_MSSF_URBA: Optional[str]
    U_MSSF_DISA: Optional[str]
    U_MSSF_CUA: Optional[str]
    U_MSSM_LAT: Optional[str]
    U_MSSM_LON: Optional[str]
    U_MSSM_HOR: Optional[str]
    U_MSSC_TICA: Optional[str]
    U_MSS_PREP: Optional[str]
    U_MSSL_ITR: Optional[str]
    U_MSS_APAN: Optional[str]#N,
    U_MSSF_TELT2: Optional[str]
    U_MSSC_RESP: Optional[str]
    U_MSS__PVH2: Optional[str]
    U_MSS_ARTR: Optional[str]
    U_MSS_ANTR: Optional[str]
    U_MSS_ADTR: Optional[str]
    U_MSS_RTR: Optional[str]
    U_MSS_NTR: Optional[str]
    U_MSSF_TICKET: Optional[str]
    U_MSSM_COM: Optional[str]
    U_MSS_ACDC: Optional[str]
    U_MSS_DELN: Optional[str]
    U_MSS_PVH2: Optional[str]
    U_MSS_BULTOS: Optional[float]
    U_MSSL_REF: Optional[float]
    U_MSS_PESO: Optional[float]
    U_MSSF_SDIN: Optional[str] #N,
    U_MSSD_ESDO: Optional[str] #0
    U_MSSD_OBDO: Optional[str]
    U_MSSD_EXCL: Optional[str] #N,
    U_MSS_FECP: Optional[str]
    U_MSS_NRORQ: Optional[str]
    U_MSS_PDFGRE: Optional[str]
    U_DIS_PDFGRE: Optional[str]
    U_MSS_DIALMA: Optional[str]
    U_MSSL_SECL: Optional[str]
    U_MSSFE_ENT_AUT: Optional[str]
    U_MSSFE_NUM_AUT: Optional[str]
    U_MSSF_CODTRAS: Optional[str]
    U_MSSF_TDOCR: Optional[str]
    U_MSSF_NDOCR: Optional[str]
    U_MSSF_SUS_DIF: Optional[str]
    U_MSSFE_NRO_CONT: Optional[str]
    U_MSSF_RG_MTC: Optional[str]
    U_MSSF_TRS_ML: Optional[str]
    U_MSSF_RT_ENV: Optional[str]
    U_MSSF_RTVH: Optional[str]
    U_MSSF_DAM_DS: Optional[str]
    U_MSSF_IND_VH_COND: Optional[str]
    U_MSSF_PESO: Optional[float]
    U_MSSF_CSN: Optional[str]
    U_MSSFE_NRO_PRECI: Optional[str]
    U_MSSFE_PUERTO: Optional[str]
    U_MSSFE_AEROPUE: Optional[str]
    U_MSSFE_DOC1: Optional[str]
    U_MSSFE_DOC2: Optional[str]
    U_MSSFE_NUM_DOC1: Optional[str]
    U_MSSFE_NUM_DOC2: Optional[str]
    U_MSSF_RUC_GE: Optional[str]
    U_MSS_NROEFA: Optional[str]
    U_MSS_PROVI: Optional[str] #N
    U_MSSL_RVI: Optional[str]
    U_MSSL_MRV: Optional[str]
    U_MSSL_FRV: Optional[str]
    U_MSSL_RCE: Optional[str]
    U_MSSL_FRC: Optional[str]
    U_SCR_COT: Optional[str]
    U_SCR_DTS: Optional[str]
    U_SCR_CAD1: Optional[str]
    U_SCR_CAD2: Optional[str]
    U_MSS_DOPC: Optional[str] #N,
    Document_ApprovalRequests: Optional[List]
    DocumentLines: Optional[List[DocumentLines]]    
    DocumentAdditionalExpenses: Optional[List]
    WithholdingTaxDataWTXCollection: Optional[List]
    WithholdingTaxDataCollection: Optional[List]
    DocumentPackages: Optional[List]
    DocumentSpecialLines: Optional[List]
    DocumentInstallments: Optional[List[DocumentInstallments]]
    DownPaymentsToDraw: Optional[List]
    TaxExtension: TaxExtension
    AddressExtension: AddressExtension
    DocumentReferences: Optional[List]

    class config : 
        orm_mode : True