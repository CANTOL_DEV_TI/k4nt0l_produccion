from typing import Optional,List
from pydantic import BaseModel

class FacturasLetras(BaseModel):
    LineNum : Optional[int]
    DocEntry : Optional[str]
    SumApplied : Optional[float]#subtotal
    AppliedFC : Optional[float]
    AppliedSys : Optional[float] #impuesto
    DocRate : Optional[float]
    DocLine : Optional[int]
    InvoiceType : Optional[str] #it_Invoice
    DiscountPercent : Optional[float]
    PaidSum : Optional[float]
    InstallmentId : Optional[int]
    WitholdingTaxApplied : Optional[float]
    WitholdingTaxAppliedFC : Optional[float]
    WitholdingTaxAppliedSC : Optional[float]
    LinkDate : Optional[str]
    DistributionRule :  Optional[str]
    DistributionRule2 : Optional[str]
    DistributionRule3 : Optional[str]
    DistributionRule4 : Optional[str]
    DistributionRule5 : Optional[str]
    TotalDiscount : Optional[float]
    TotalDiscountFC : Optional[float]
    TotalDiscountSC: Optional[float]
    U_MSSC_TIPO : Optional[str]
    DocNum : Optional[str]
    FolioPref : Optional[str]
    FolioNum : Optional[str]
    DocTotal : Optional[float]

class ListaFL(BaseModel):
    Cia : Optional[str]
    CondP : Optional[int]    
    DocType : Optional[str]
    DocDate : Optional[str]
    CardCode : Optional[str]
    CardName : Optional[str]
    Address : Optional[str]
    CashAccount : Optional[str]
    DocCurrency : Optional[str]
    CashSum : Optional[str]    
    Reference1 : Optional[str]
    Remarks : Optional[str]
    JournalRemarks : Optional[str]
    TaxDate : Optional[str]
    Series : Optional[int]
    CashSumSys : Optional[float]
    PayToCode : Optional[str]
    IsPayToBank : Optional[str]
    PaymentType : Optional[str]
    DocObjectCode : Optional[str]
    DueDate : Optional[str]
    DocTypte : Optional[str]
    ControlAccount : Optional[str]
    U_MSSL_CPP : Optional[str]
    U_SCR_TOS : Optional[str]
    U_SCR_MGD : Optional[str]
    U_MSS_ANLR : Optional[str]
    U_MSS_ESRE : Optional[str]
    U_MSSM_CRM : Optional[str]
    U_MSSM_TRM : Optional[str]
    U_MSS_ESTR : Optional[str]
    PaymentInvoices : Optional[List[FacturasLetras]]
        
    class config : 
        orm_mode : True


#######
#DocType" : fila["DocType"], "DocDate" : fila["fecha"] , "CardCode" : fila["cliente_cod"],
#                        "DocCurrency" : fila["moneda"], "CounterReference" : fila["referencia"], "TaxDate" : fila["fecha_cont"], "Series" : fila["serie"],
#                        "DueDate" : fila["fecha_venc"], "U_MSSL_TMP" : fila["U_MSSL_TMP"],
#                        "ControlAccount" : fila["CuentaMayor"], "TransferAccount" : fila["CuentaTransferencia"], "TransferDate" : fila["fecha"], 
#                        "TransferReference" : "Prueba WEB", "TransferSum" : str(fila["monto"])
#######