from conexiones.conexion_maggie import conexion_maggie

def getRecepciones(Ejercicio, Periodo, txtFind):
    DB_NOM = ""

    sql = "SELECT  "
    sql += " rc.cod_recepcion,"  # 0
    sql += " rc.RAZON_SOCIAL,"  # 1
    sql += " rc.tipo_reposicion, "  # 2
    sql += " rc.fecha_recep, "  # 3 -- fecha recepcion
    sql += " rc.fecha_despacho,"  # 4 -- vencimiento
    sql += " (SELECT count(DISTINCT oc) FROM " + str(DB_NOM) + "recepcion_det_SAP WHERE cod_recepcion=rc.cod_recepcion) AS total_oc,"  # 5
    sql += " (SELECT count(DISTINCT cod_sucursal) FROM " + str(DB_NOM) + "recepcion_det_SAP WHERE cod_recepcion=rc.cod_recepcion) AS total_tienda,"  # 6
    sql += " (SELECT count(DISTINCT speed_cod_prod) FROM " + str(DB_NOM) + "recepcion_det_SAP WHERE cod_recepcion=rc.cod_recepcion) AS total_prod, "  # 7
    sql += " (SELECT sum(oc_cant) FROM " + str(DB_NOM) + "recepcion_det_SAP WHERE cod_recepcion=rc.cod_recepcion) AS total_cant, "  # 8
    sql += " CASE WHEN rc.sw_atendido=0 THEN 'NO' ELSE 'SI' END AS ATENDIDO,"  # 9
    sql += " rc.cod_clie,"  # 10
    sql += " (SELECT CASE WHEN count(cod_recepcion)>0 THEN 1 ELSE 0 END FROM " + str(DB_NOM) + "recepcion_det_SAP WHERE (sw_error_lp+sw_error_stock+sw_error_prec)>=1 AND cod_recepcion=rc.cod_recepcion) as sw_error,"  # 11
    sql += " (SELECT CASE WHEN count(cod_recepcion)>0 THEN 1 ELSE 0 END FROM " + str(DB_NOM) + "reportados_SAP WHERE sw_activo=1 AND cod_recepcion=rc.cod_recepcion) as sw_report ,"  # 12

    sql += " (SELECT sw_end FROM " + str(DB_NOM) + "historial_SAP WHERE cod_proceso=2 AND cod_recepcion=rc.cod_recepcion) AS sw_picking,"  # 13
    sql += " (SELECT sw_end FROM " + str(DB_NOM) + "historial_SAP WHERE cod_proceso=3 AND cod_recepcion=rc.cod_recepcion) AS sw_packing,"  # 14
    sql += " (SELECT sw_end FROM " + str(DB_NOM) + "historial_SAP WHERE cod_proceso=4 AND cod_recepcion=rc.cod_recepcion) AS sw_lpn,"  # 15
    sql += " (SELECT sw_end FROM " + str(DB_NOM) + "historial_SAP WHERE cod_proceso=5 AND cod_recepcion=rc.cod_recepcion) AS sw_pedido,"  # 16

    sql += " rc.COD_TIPO_DOC_IDENTIDAD AS IDE_CLIE,"  # 17

    sql += " (SELECT count(rd.COD_SUCURSAL) FROM " + str(DB_NOM) + "RECEPCION_DET_SAP rd WHERE SUBSTR(rd.NOM_SUCURSAL,1,7)='Maestro' AND rd.COD_RECEPCION=rc.cod_recepcion) as CLIE_TRUE "  # 18

    sql += " FROM " + str(DB_NOM) + "recepcion_cab_SAP rc "


    # sql += " WHERE SUBSTRING(replace(to_char(rc.fecha_despacho,'YYYY-MM'),'-',''),1,6)='" + str(self.ui.dateEdit.date().year()) + str(vv_mes) + "'"
    sql += " WHERE "
    sql += " EXTRACT(YEAR FROM rc.fecha_despacho)={}".format(Ejercicio)
    sql += " AND EXTRACT(MONTH FROM rc.fecha_despacho)={}".format(Periodo)

    # if not self.ui.checkBox.isChecked():
    #     sql += " AND rc.cod_clie='" + str(vv_cod_clie) + "'"

    # if self.ui.radioButton.isChecked():
    #     sql += " AND rc.sw_atendido=0"
    # if self.ui.radioButton_2.isChecked():
    #     sql += " AND rc.sw_atendido=1"

    sql += " AND upper(rc.tipo_reposicion) LIKE '%" + str(txtFind).upper() + "%' "

    sql += " ORDER BY rc.fecha_despacho asc, rc.fecha_recep asc "  # , ,  desc"

    vValores = ()

    me_conexion = conexion_maggie()
    resultadosConsulta = me_conexion.consultas(sql, vValores)
    respuestaJSON = []
    for row in resultadosConsulta:
        dicTemporal = {'cod_recepcion'  : row[0],
                       'RAZON_SOCIAL'   : row[1],
                       'tipo_reposicion': row[2],
                       'fecha_recep'    : row[3],
                       'fecha_despacho' : row[4],
                       'total_oc'       : row[5],
                       'total_tienda'   : row[6],
                       'total_prod'     : row[7],
                       'total_cant'     : row[8],
                       'ATENDIDO'       : row[9],
                       'cod_clie'       : row[10],
                       'sw_error'       : row[11],
                       'sw_report '     : row[12],
                       'sw_picking'     : row[13],
                       'sw_packing'     : row[14],
                       'sw_lpn'         : row[15],
                       'sw_pedido'      : row[16],
                       'IDE_CLIE'       : row[17],
                       'CLIE_TRUE'      : row[18]
                       }
        respuestaJSON.append(dicTemporal)


    return respuestaJSON





