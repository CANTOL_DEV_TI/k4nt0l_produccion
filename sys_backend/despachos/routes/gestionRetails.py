
from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from sqlalchemy.orm.session import Session

from despachos.functions.gestionRetailsFunction import (getRecepciones)


ruta = APIRouter(
    prefix="/despacho",tags=["despachos"]
)

# @ruta.get('/ListarRecepciones/{txtFind}')
@ruta.get('/ListarRecepciones/')
def showRecepciones(txtFind: str, ejercicio: int, periodo: int):
    return getRecepciones(ejercicio, periodo, txtFind)





