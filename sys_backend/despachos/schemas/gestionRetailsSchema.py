from typing import Optional
from pydantic import BaseModel

class gestionRetailsCab_Schema(BaseModel):
    cod_recepcion          : int
    fecha_recep            : str
    fecha_despacho         : str
    cod_usuario            : str
    cod_clie               : str
    fecha_save             : str
    tipo_reposicion        : str
    sw_atendido            : int
    cod_vendedor           : str
    cod_forma_pago         : str
    cod_tipo_doc           : str
    fecha_entrega          : str
    razon_social           : str
    cod_tipo_doc_identidad : str
    
    class Config:
        orm_mode = True



