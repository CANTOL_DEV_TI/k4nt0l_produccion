from conexiones.conexion_mmsql import conexion_mssql

def agregar(meData):
    try:
        valores = []
        valores.append(meData.emp_codigo)
        valores.append(meData.rubro_nombre)
        valores.append(meData.codigo_cc)

        sql = "insert into teso_rubro(emp_codigo,rubro_nombre,codigo_cc) values(%s,%s,%s)"

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        print(sql)
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def actualizar(meData):
    try:
        valores = []
        valores.append(meData.emp_codigo)
        valores.append(meData.rubro_nombre)
        valores.append(meData.codigo_cc)
        valores.append(meData.rubro_id)

        sql = "update teso_rubro set "
        sql += "emp_codigo=%s, rubro_nombre=%s, codigo_cc=%s "
        sql += "where rubro_id=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        print(sql)
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def eliminar(meData):
    try:
        valores = []
        valores.append(meData.rubro_id)
        sql = "delete from teso_rubro "
        sql += "where rubro_id=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        print(sql)
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."