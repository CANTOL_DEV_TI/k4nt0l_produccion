from conexiones.conexion_sap_solo import conexion_SAP_Tecno 
from conexiones.conexion_sql_solo import ConectaSQL_Produccion 
from generales.generales import CiaSAP
 
def lista_pagos(pCia, pDesde, pHasta, pProveedor, pMoneda, pOrden, pTipoOrden, pDetraccion): 
    try: 
        sCia = "" 
        repo_fila = {} 
        repo_total = [] 
 
        sCia = CiaSAP(pCia)
 
        sqlCab = f"SELECT DISTINCT row_number() over(ORDER BY FP.\"DocEntry\") AS indice,'N' as Seleccion, FP.\"DocEntry\" as Nro_Interno,FP.\"DocNum\" as Nro_Documento , '{pCia}' as Cia,FP.\"CardCode\" as Cliente_Cod,FPD.\"OcrCode\" as CC1_Codigo" 
        sqlCab += ",CC.\"OcrName\" as CC1_Nombre,TD.\"Name\" as Tipo_Documento ,Pr.\"LicTradNum\" AS RUC, FP.\"CardName\" as Cliente_RS,CAST(FP.\"DocDate\" as varchar(10)) as Fecha_Registro," 
        sqlCab += "FP.\"FolioPref\" as Serie, FP.\"FolioNum\" as Nro , FP.\"JrnlMemo\" as Motivo,CAST (FP.\"TaxDate\" AS varchar(10)) as Fecha,CP.\"PymntGroup\" as Cond_Pago ,CAST (FPP.\"DueDate\" AS varchar(10)) as Fecha_Venc" 
        sqlCab += " ,FPP.\"InstPrcnt\" as Porc,CASE FP.\"DocCur\" WHEN 'US$' THEN FPP.\"InsTotalFC\" - FPP.\"PaidSys\" ELSE FPP.\"InsTotal\" - FPP.\"PaidToDate\" END  as Porc_Total, DAYS_BETWEEN(FPP.\"DueDate\",NOW()) AS Atraso_dias,MONTH (NOW()) AS Mes_Pago,YEAR (NOW()) AS Ano_Pago, " 
        sqlCab += f" FP.\"DocCur\" as Moneda , CASE FP.\"DocCur\" WHEN 'US$' THEN FP.\"DocTotalFC\" ELSE FP.\"DocTotal\" END as Total_Doc , case FP.U_MSSL_ADT when 'Y' then 'SI' else 'NO' end AS Aplica_Detraccion, FP.\"PaidToDate\" as Pagado,IFNULL(RET.\"WTAmnt\" ,0) AS Retencion"
        sqlCab += f" ,U_SWC_COMENTARIO as swc_comentario FROM {sCia}.OPCH FP " 
        sqlCab += f" INNER JOIN {sCia}.PCH6 FPP ON FP.\"DocEntry\" = FPP.\"DocEntry\" INNER JOIN {sCia}.OCRD Pr ON Pr.\"CardCode\" = FP.\"CardCode\" " 
        sqlCab += f" INNER JOIN {sCia}.OCTG CP ON CP.\"GroupNum\" = FP.\"GroupNum\" "  
        sqlCab += f" INNER JOIN (SELECT DISTINCT D.\"DocEntry\",D.\"OcrCode\" FROM {sCia}.PCH1 D ) FPD ON FPD.\"DocEntry\" = FP.\"DocEntry\" "  
        sqlCab += f" INNER JOIN {sCia}.OOCR CC ON CC.\"OcrCode\" = FPD.\"OcrCode\" "  
        sqlCab += f" INNER JOIN {sCia}.OIDC TD ON TD.\"Code\" = FP.\"Indicator\" "  
        sqlCab += f" LEFT OUTER JOIN {sCia}.PCH5 RET ON FP.\"DocEntry\" = RET.\"AbsEntry\" " 
        sqlCab += f" WHERE FP.\"CANCELED\" = 'N' AND FP.\"DocStatus\" <> 'C' AND FPP.\"Status\" <> 'C'  AND FP.U_MSSF_EGRA = 'N' AND (FPP.\"InsTotal\" <> FP.\"PaidToDate\") AND FP.\"DocCur\"='{pMoneda}'" 
        sqlCab += f" AND (FP.\"CardName\" LIKE '%{pProveedor}%' OR FP.\"LicTradNum\" LIKE '%{pProveedor}%' ) AND FPP.\"DueDate\" BETWEEN '{pDesde}' AND '{pHasta}' and FP.U_MSSL_ADT='{pDetraccion}' " 
        sqlCab += f"ORDER BY {pOrden} {pTipoOrden}" 
         
        print(sqlCab) 
 
        conn = conexion_SAP_Tecno() 
 
        cur_var_repo = conn.cursor() 
 
        cur_var_repo.execute(sqlCab) 
 
        for row in cur_var_repo: 
 
            programado = validar_programacion(row['Nro_Interno'],row['Cia']) 
             
            val_pro = "" 
            val_monto = 0
            print(len(programado))
            if(len(programado) != 0): 
                print(row['Porc_Total'],programado['monto']) 
                if(row['Porc_Total']==programado['monto']): 
                    print('Total') 
                    val_pro = "Programado Total"     
                    val_monto = programado['monto']
                if(row['Porc_Total']!=programado['monto']): 
                    print('Parcial') 
                    val_pro = "Programado Parcial"
                    val_monto = programado['monto']
            else: 
                val_pro = "Sin Programacion"
                val_monto = 0 
 
            #print(val_pro) 
 
            repo_fila = {"indice" : row['indice'],"Seleccion" : row['Seleccion'],"Cia" : row['Cia'], "Nro_Interno" : row['Nro_Interno'],"Nro_Documento" : row['Nro_Documento'], "Cliente_Cod" : row['Cliente_Cod'], "CC1_Codigo" : row['CC1_Codigo'],  
                        "CC1_Nombre" : row['CC1_Nombre'] , "Tipo_Documento" : row['Tipo_Documento'],"Tipo_Documento" : row['Tipo_Documento'],"RUC" : row['RUC'],"Cliente_RS" : row['Cliente_RS'], 
                        "Serie" : row['Serie'],"Nro" : row['Nro'],"Motivo" : row['Motivo'], "Fecha" : row['Fecha'], "Cond_Pago" : row['Cond_Pago'], "Fecha_Venc" : row['Fecha_Venc'], 
                        "Porc" : row['Porc'],"Porc_Total" : row['Porc_Total'],"Atraso_dias":row['Atraso_Dias'],"Mes_Pago" : row['Mes_Pago'], "Ano_Pago" : row['Ano_Pago'],"Moneda":row['Moneda'], 
                        "Total_Doc" : row['Total_Doc'] , "Aplica_Detraccion" : row['Aplica_Detraccion'], "Pagado" : row['Pagado'], "CC2" : detalle_cc_n2(sCia,row['Nro_Interno']),  
                        "Estado" : val_pro, "Retencion" : row['Retencion'], "Fecha_Registro": row['Fecha_Registro'], "swc_comentario":row['swc_comentario'], 
                        "total_programado" : val_monto } 
            repo_total.append(repo_fila) 
 
        conn.close() 

        #print(repo_total)
 
        return repo_total 
    except Exception as err: 
        return f"Error en la operación [Consultar] : {err}." 
 
def detalle_cc_n2(pCia,pNro_Interno): 
    try: 
        vLineaFinal = "" 
        vLineaParcial = "" 
 
        sqlDet = f"SELECT DISTINCT trim(cc2.\"OcrName\") AS CC FROM {pCia}.PCH1 FDC INNER JOIN {pCia}.OOCR CC2 ON FDC.\"OcrCode2\" = CC2.\"OcrCode\" WHERE FDC.\"DocEntry\" = {pNro_Interno};" 
 
        conn = conexion_SAP_Tecno() 
 
        cur_var_cc = conn.cursor() 
 
        cur_var_cc.execute(sqlDet) 
 
        for row in cur_var_cc: 
            vLineaParcial = row['CC'] 
            vLineaFinal += vLineaParcial + "-" 
 
        conn.close() 
 
        return vLineaFinal[0:len(vLineaFinal) - 1] 
    except Exception as err: 
        return f"Error en la operación [detalle cc2] : {err}." 
     
def validar_programacion(pNro_Interno, pCia): 
    try: 
        #print("Entro a validar...")
        vlinea = {}         
 
        sql = "select nro_interno, nro_documento,sum(monto_programado) as monto_programado_sumado from teso_planificaciones tp (nolock) " 
        sql += f"where nro_interno = '{pNro_Interno}' and emp_codigo = '{pCia}' and estado_plan not in ('C') group by nro_interno, nro_documento" 
        print(sql)
        conn =  ConectaSQL_Produccion() 
 
        cur_var_vp = conn.cursor(as_dict=True) 
 
        cur_var_vp.execute(sql) 
 
        for row in cur_var_vp: 
            vlinea = {"monto": row['monto_programado_sumado'], "nro_interno" : row['nro_interno'], "nro_documento" : row['nro_documento']} 
             
        conn.close() 
 
        return vlinea 
    except Exception as err: 
        print(f"Error en la operación [validar programacion] : {err}.")
        return f"Error en la operación [validar programacion] : {err}." 
     
def total_pagos(pCia, pDesde, pHasta, pProveedor, pMoneda, pDetraccion): 
    try: 
        sCia = "" 
        totales = {} 
         
        sCia = CiaSAP(pCia)
 
        sqlCab = f"SELECT count(*) AS Cant_Pagos, CASE FP.\"DocCur\" WHEN 'US$' THEN sum(FPP.\"InsTotalFC\" - FPP.\"PaidSys\") ELSE sum(FPP.\"InsTotal\" - FPP.\"PaidToDate\") END AS Total FROM {sCia}.OPCH FP " 
        sqlCab += f" INNER JOIN {sCia}.PCH6 FPP ON FP.\"DocEntry\" = FPP.\"DocEntry\" INNER JOIN {sCia}.OCRD Pr ON Pr.\"CardCode\" = FP.\"CardCode\" " 
        sqlCab += f" INNER JOIN {sCia}.OCTG CP ON CP.\"GroupNum\" = FP.\"GroupNum\" "  
        sqlCab += f" INNER JOIN (SELECT DISTINCT D.\"DocEntry\",D.\"OcrCode\" FROM {sCia}.PCH1 D ) FPD ON FPD.\"DocEntry\" = FP.\"DocEntry\" "  
        sqlCab += f" INNER JOIN {sCia}.OOCR CC ON CC.\"OcrCode\" = FPD.\"OcrCode\" "  
        sqlCab += f" INNER JOIN {sCia}.OIDC TD ON TD.\"Code\" = FP.\"Indicator\" "          
        sqlCab += f" WHERE FP.\"CANCELED\" = 'N' AND FP.\"DocStatus\" <> 'C' AND FPP.\"Status\" <> 'C' AND FP.U_MSSF_EGRA = 'N' AND (FPP.\"InsTotal\" <> FP.\"PaidToDate\") AND FP.\"DocCur\"='{pMoneda}'" 
        sqlCab += f" AND (FP.\"CardName\" LIKE '%{pProveedor}%' OR FP.\"LicTradNum\" LIKE '%{pProveedor}%' ) AND FPP.\"DueDate\" BETWEEN '{pDesde}' AND '{pHasta}' and FP.U_MSSL_ADT='{pDetraccion}' group by FP.\"DocCur\"" 
 
        #print(sqlCab) 
 
        conn = conexion_SAP_Tecno() 
 
        cur_var_repo = conn.cursor() 
 
        cur_var_repo.execute(sqlCab) 
 
        for row in cur_var_repo: 
 
            totales = {"Cant_Pagos" : row['Cant_Pagos'],"Total" : row['Total']} 
             
        conn.close() 
 
        return totales 
    except Exception as err: 
        return f"Error en la operación [Consultar] : {err}."