from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from tesoreria.schemas.centrocostousuarioSchema import CCxUsuario

def AgregaCC(pData:CCxUsuario):
    try:
        sql = f"insert into presu_cc_usuario values('{pData.Cia}','{pData.Usuario}','{pData.CCCodigo}','{pData.CCNombre}')"

        conn = ConectaSQL_Produccion()

        cur_add = conn.cursor()

        cur_add.execute(sql)

        conn.commit()

        conn.close()

        return {"resultado" : 0, "mensaje" : "Centro de Costo agregado al usuario"}
    
    except Exception as err:
        return {"resultado" : -1, "mensaje" : "Error al agregar CC de Usuario"}

def ListarCCxUsuario(pUsuario,pCia):
    try:
        lista = []

        sql = f"select centro_costo,cc_desc from presu_cc_usuario where usuario = upper('{pUsuario}') and cia='{pCia}'"
        print(sql)
        conn = ConectaSQL_Produccion()

        cur_select = conn.cursor(as_dict = True)

        cur_select.execute(sql)

        for i in cur_select:            
            fila = {"codigo":i["centro_costo"], "nombre":i["cc_desc"]}
            lista.append(fila)

        conn.close()

        return lista
    
    except Exception as err:
        return {"resultado" : -1, "mensaje" : f"Error al agregar CC de Usuario : {err}"}