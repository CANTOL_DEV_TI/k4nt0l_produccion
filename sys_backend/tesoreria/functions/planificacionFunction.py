from conexiones.conexion_mmsql import conexion_mssql 
from tesoreria.schemas.planificacionSchema import planificacion_Schema 
from conexiones.conexion_sql_solo import ConectaSQL_Produccion 
from tesoreria.functions.pagosSAPFunction import GrabarPagoEfectuado
from decimal import Decimal
from generales.configuracion import GetSeries
from tesoreria.functions.loginSLDFunction import logintoken
from generales.generales import CiaSAP
from os import getenv
import requests
import json
import urllib3
from typing import List
from decimal import Decimal
from datetime import date

def agregar(meData): 
    try:        
        valores = [] 
        valores.append(meData.emp_codigo) 
        valores.append(meData.nro_interno) 
        valores.append(meData.nro_documento) 
        valores.append(meData.cliente_cod) 
        valores.append(meData.cliente_rs) 
        valores.append(meData.ruc)     
        valores.append(meData.cc1_codigo) 
        valores.append(meData.cc1_nombre) 
        valores.append(meData.tipo_documento) 
        valores.append(meData.serie) 
        valores.append(meData.nro) 
        valores.append(meData.motivo) 
        valores.append(meData.fecha) 
        valores.append(meData.cond_pago) 
        valores.append(meData.fecha_venc) 
        valores.append(meData.porc) 
        valores.append(meData.porc_total) 
        valores.append(meData.atraso_dias) 
        valores.append(meData.mes_pago) 
        valores.append(meData.ano_pago) 
        valores.append(meData.moneda) 
        valores.append(meData.total_doc) 
        valores.append(meData.aplica_detraccion) 
        valores.append(meData.pagado) 
        valores.append(meData.cc2) 
        valores.append(meData.fecha_programado)    
        valores.append(meData.monto_programado) 
        valores.append(meData.tiporq_id) 
        valores.append(meData.retencion) 
        valores.append(meData.banco) 
        valores.append(meData.cuenta) 
        valores.append(meData.usuario) 
        valores.append(meData.fecha_emision) 

        retencion = Decimal

        if(meData.retencion > 0):
            retencion = meData.monto_programado * Decimal("0.03")
        else:
            retencion = Decimal("0")
        
        #print(valores) 
        sql = "insert into teso_planificaciones(emp_codigo,nro_interno,nro_documento,cliente_cod,cliente_rs,ruc,cc1_codigo,cc1_nombre,tipo_documento,serie,nro,motivo,fecha," 
        sql += "cond_pago,fecha_venc,porc,porc_total,atraso_dias,mes_pago,ano_pago,moneda,total_doc,aplica_detraccion,pagado,cc2,fecha_programado,monto_programado,tiporq_id," 
        sql += "fecha_registrado,retencion,banco,cuenta,usuario,estado_plan,fecha_emision) values" 
        sql += f"('{meData.emp_codigo}','{meData.nro_interno}','{meData.nro_documento}','{meData.cliente_cod}','{meData.cliente_rs}','{meData.ruc}','{meData.cc1_codigo}','{meData.cc1_nombre}','{meData.tipo_documento}','{meData.serie}','{meData.nro}','{meData.motivo}','{meData.fecha}'," 
        sql += f"'{meData.cond_pago}','{meData.fecha_venc}',{meData.porc},{meData.porc_total},{meData.atraso_dias},{meData.mes_pago},{meData.ano_pago},'{meData.moneda}',{meData.total_doc},'{meData.aplica_detraccion}',{meData.pagado},'{meData.cc2}','{meData.fecha_programado}',{meData.monto_programado},{meData.tiporq_id}," 
        sql += f"getdate(),{retencion},'{meData.banco}','{meData.cuenta}','{meData.usuario}','A','{meData.fecha_emision}')" 
        #print(sql) 
        meConexion = conexion_mssql() 
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
        #print(sql) 
        #print(respuesta) 
        return respuesta 
    except Exception as err: 
        print(err)
        return f"Error en la operación [agregar] : {err}." 
 
def actualizar(meData): 
    try: 
        valores = [] 
        valores.append(meData.emp_codigo) 
        valores.append(meData.nro_interno) 
        valores.append(meData.nro_documento) 
        valores.append(meData.cliente_cod) 
        valores.append(meData.cliente_rs) 
        valores.append(meData.ruc)     
        valores.append(meData.cc1_codigo) 
        valores.append(meData.cc1_nombre) 
        valores.append(meData.tipo_documento) 
        valores.append(meData.serie) 
        valores.append(meData.nro) 
        valores.append(meData.motivo) 
        valores.append(meData.fecha) 
        valores.append(meData.cond_pago) 
        valores.append(meData.fecha_venc) 
        valores.append(meData.porc) 
        valores.append(meData.porc_total) 
        valores.append(meData.atraso_dias) 
        valores.append(meData.mes_pago) 
        valores.append(meData.ano_pago) 
        valores.append(meData.moneda) 
        valores.append(meData.total_doc) 
        valores.append(meData.aplica_detraccion) 
        valores.append(meData.pagado) 
        valores.append(meData.cc2) 
        valores.append(meData.fecha_programado) 
        valores.append(meData.monto_programado) 
        valores.append(meData.tiporq_id) 
        valores.append(meData.retencion) 
        valores.append(meData.banco) 
        valores.append(meData.cuenta) 
        valores.append(meData.fecha_emision) 
        valores.append(meData.planificacion_id) 
 
        sql = "update teso_planificaciones set emp_codigo=%s,nro_interno=%s,nro_documento=%s,cliente_cod=%s," 
        sql += "cliente_rs=%s,ruc=%s,cc1_codigo=%s,cc1_nombre=%s,tipo_documento=%s,serie=%s,nro=%s,motivo=%s,fecha=%s," 
        sql += "cond_pago=%s,fecha_venc=%s,porc=%s,porc_total=%s,atraso_dias=%s,mes_pago=%s,ano_pago=%s,moneda=%s,total_doc=%s,aplica_detraccion=%s,pagado=%s,cc2=%s," 
        sql += "fecha_programado=%s, monto_programado=%s, tiporq_id=%s, retencion=%s, banco=%s, cuenta=%s , fecha_emision=%s" 
        sql += " where planificacion_id=%s " 
 
        meConexion = conexion_mssql() 
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
        #print(sql) 
        return respuesta 
    except Exception as err: 
        return f"Error en la operación : {err}." 
 
def eliminar(meData): 
    try: 
        valores = [] 
        valores.append(meData.planificacion_id) 
 
        sql = "delete from teso_planificaciones " 
        sql += "where planificacion_id=%s " 
 
        meConexion = conexion_mssql() 
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
        #print(sql) 
        return respuesta 
    except Exception as err: 
        return f"Error en la operación : {err}." 
 
def ListadoProgramados(meData): 
    try: 
        reporte = [] 
        linea = {} 
 
        sql = "select ROW_NUMBER() OVER(ORDER BY planificacion_id ASC) AS indice, planificacion_id, ce.emp_codigo, ce.emp_nombre, nro_interno, nro_documento, cliente_cod, cliente_rs, ruc, cc1_codigo," 
        sql += " cc1_nombre, tipo_documento,serie, nro, motivo, fecha, cond_pago, fecha_venc, porc, porc_total, atraso_dias, mes_pago, ano_pago, moneda, total_doc,aplica_detraccion," 
        sql += "pagado,cc2, convert(varchar(10),fecha_programado,103) as fecha_programado, Retencion, round(monto_programado - retencion,2) as monto_programado, Retencion, tp.tiporq_id , ttr.tiporq_nombre , fecha_registrado, usuario, " 
        sql += "case estado_plan when 'T' then 'TERMINADO' when 'R' then 'REPROGRAMADO' when 'C' then 'ANULADO' else 'ACTIVO' end estado from teso_planificaciones tp (nolock) " 
        sql += "inner join config_empresa ce (nolock) on ce.emp_codigo = tp.emp_codigo inner join teso_tipo_requerimiento ttr (nolock) on ttr.tiporq_id = tp.tiporq_id " 
        sql += f"where tp.emp_codigo = '{meData.emp_codigo}' and tp.moneda='{meData.moneda}' " 
        sql += f"and (('S' = '{meData.todo_fpr}') OR (fecha_programado BETWEEN '{meData.fprdesde}' and '{meData.fprhasta}'))" 
        sql += f"and (('S' = '{meData.todo_fv}') OR (fecha_venc BETWEEN '{meData.fvdesde}' and '{meData.fvhasta}'))" 
        sql += f"and (('S' = '{meData.todo_proveedor}') or (cliente_rs like '%{meData.proveedor}%'))" 
        sql += f"and (('S' = '{meData.todo_cc}') or (cc1_nombre like '%{meData.cc}%'))" 
        sql += f"and (('S' = '{meData.todo_cp}') or (cond_pago like '%{meData.cond_pago}%'))" 
        sql += f"and (('S' = '{meData.todo_usuario}') or (usuario like '%{meData.usuario}%'))" 
        sql += f"and (('S' = '{meData.todo_bancos}') or (banco = '{meData.banco}'))" 
        sql += f"and (('S' = '{meData.todo_cuentas}') or (cuenta = '{meData.cuenta}'))" 
 
        conn = ConectaSQL_Produccion() 
        
 
        cur_sql_lp = conn.cursor(as_dict = True) 
        #print(sql) 
        cur_sql_lp.execute(sql) 
 
        for row in cur_sql_lp: 
            linea = { "planificacion_id" : row['planificacion_id'],"emp_codigo" : row['emp_codigo'],"emp_nombre" : row['emp_nombre'],"nro_interno" : row['nro_interno'],"nro_documento" : row['nro_documento'], 
                        "cliente_cod" : row['cliente_cod'],"cliente_rs" : row['cliente_rs'],"ruc" : row['ruc'],"cc1_codigo" : row['cc1_codigo'],"cc1_nombre" : row['cc1_nombre'], 
                        "tipo_documento" : row['tipo_documento'],"serie" : row['serie'],"nro" : row['nro'],"motivo" : row['motivo'],"fecha" : row['fecha'],"cond_pago" : row['cond_pago'], 
                        "fecha_venc" : row['fecha_venc'],"porc" : row['porc'],"porc_total" : row['porc_total'],"atraso_dias" : row['atraso_dias'],"mes_pago" : row['mes_pago'], 
                        "ano_pago" : row['ano_pago'],"moneda" : row['moneda'],"total_doc" : row['total_doc'],"aplica_detraccion" : row['aplica_detraccion'],"pagado" : row['pagado'], 
                        "cc2" : row['cc2'],"fecha_programado" : row['fecha_programado'],"monto_programado" : row['monto_programado'],"tiporq_id" : row['tiporq_id'], 
                        "tiporq_nombre" : row['tiporq_nombre'],"fecha_registrado" : row['fecha_registrado'],"usuario" : row['usuario'], "indice" : row['indice'], "estado" : row['estado'], 
                        "Retencion" : row['Retencion']} 
             
            reporte.append(linea) 
 
        conn.close() 
 
        return reporte 
     
    except Exception as err: 
        return f"Error en el operación : {err}" 
     
def TerminarPago(meData): 
    try: 

        #print(meData)
        planid = meData.planificacion_id
        ciaid = meData.emp_codigo

        #print(planid)
        #print(ciaid)

        result = GrabarPagoEfectuado(ciaid,planid)
        
        #print(result)

        valores = [] 
        valores.append(meData.planificacion_id) 

        sql = "update teso_planificaciones set estado_plan='T' " 
        sql += "where planificacion_id=%s " 
        #print(sql)
         
        meConexion = conexion_mssql() 
        
        resultsql = meConexion.ejecutar_funciones(sql, tuple(valores)) 

        respuesta = result + "\n" + resultsql['Mensaje']
        #print(sql) 
        return respuesta 
         
    except Exception as err: 
        print(err)
        return f"Error en la operación[Terminar Pago] : {err}." 
     
def ReProgramarPago(meData): 
    try: 
        valores = [] 
        valores.append(meData.fecha_programado) 
        valores.append(meData.usuario) 
        valores.append(meData.banco) 
        valores.append(meData.cuenta) 
        valores.append(meData.planificacion_id) 
                
        sql = "update teso_planificaciones set estado_plan='R', fecha_programado= %s, usuario= %s, banco=%s, cuenta=%s" 
        sql += " where planificacion_id=%s " 
 
        meConexion = conexion_mssql() 
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
        #print(sql) 
        return respuesta 
         
    except Exception as err: 
        return f"Error en la operación[Reprogramar Pago] : {err}." 
 
def AnularPago(meData): 
    try: 
        valores = [] 
        valores.append(meData.planificacion_id) 
 
        sql = "update teso_planificaciones set estado_plan='C' " 
        sql += "where planificacion_id=%s " 
 
        meConexion = conexion_mssql() 
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
        #print(sql) 
        return respuesta 
 
    except Exception as err: 
        return f"Error en la operación[Anular Pago] : {err}." 
 
def HistorialProgramaciones(meData): 
    try:         
        #print(meData) 
        lista = [] 
        fila = {} 
 
        sql = f"select convert(varchar(10),fecha_programado,103) as fecha_programado,convert(varchar(50),fecha_cambio,120) as fecha_cambio,usuario from teso_historial_reprogramaciones (nolock) where planificacion_id={meData.planificacion_id}" 
        #print(sql) 
        conn = ConectaSQL_Produccion()         

        cur_historial = conn.cursor(as_dict = True) 
 
        cur_historial.execute(sql) 
 
        for row in cur_historial: 
            fila = {"fecha_programado" : row['fecha_programado'],"fecha_cambio" : row['fecha_cambio'], "usuario" : row['usuario']} 
            lista.append(fila) 
 
        conn.close() 
 
        return lista 
    except Exception as err: 
        return f"Error en la operación[Historial Programaciones] : {err}." 
     
def TotalesListadoProgramados(meData): 
    try: 
        
        linea = {} 
 
        sql = "select count(*) as Cant_Prog,sum(porc_total) as Total_Prog from teso_planificaciones tp (nolock) " 
        sql += "inner join config_empresa ce (nolock) on ce.emp_codigo = tp.emp_codigo inner join teso_tipo_requerimiento ttr (nolock) on ttr.tiporq_id = tp.tiporq_id " 
        sql += f"where tp.emp_codigo = '{meData.emp_codigo}' and tp.moneda='{meData.moneda}' and estado_plan not in ('T','C') " 
        sql += f"and (('S' = '{meData.todo_fpr}') OR (fecha_programado BETWEEN '{meData.fprdesde}' and '{meData.fprhasta}')) " 
        sql += f"and (('S' = '{meData.todo_fv}') OR (fecha_venc BETWEEN '{meData.fvdesde}' and '{meData.fvhasta}')) " 
        sql += f"and (('S' = '{meData.todo_proveedor}') or (cliente_rs like '%{meData.proveedor}%')) " 
        sql += f"and (('S' = '{meData.todo_cc}') or (cc1_nombre like '%P{meData.cc}%')) " 
        sql += f"and (('S' = '{meData.todo_cp}') or (cond_pago like '%{meData.cond_pago}%')) " 
        sql += f"and (('S' = '{meData.todo_usuario}') or (usuario like '%{meData.usuario}%')) "
        sql += f"and (('S' = '{meData.todo_bancos}') or (banco = '{meData.banco}')) " 
        sql += f"and (('S' = '{meData.todo_cuentas}') or (cuenta = '{meData.cuenta}')) " 
 
        conn = ConectaSQL_Produccion() 
        
        cur_sql_lp = conn.cursor(as_dict = True) 
        #print(sql) 
        cur_sql_lp.execute(sql) 
 
        for row in cur_sql_lp: 
            linea = { "Cant_Prog" : row['Cant_Prog'],"Total_Prog" : row['Total_Prog']} 
                         
        conn.close() 
 
        return linea 
     
    except Exception as err: 
        return f"Error en el operación : {err}" 
     
def PagosdeHoy(pCia):     
    try: 
        total = [] 
        linea = {} 
 
        sql = "select planificacion_id, nro_documento, cliente_rs, ruc,serie, nro, motivo, fecha, cond_pago, porc_total,convert(varchar(10),fecha_programado,103) as fecha_programado," 
        sql += "monto_programado, ttr.tiporq_nombre from teso_planificaciones tp (nolock) inner join config_empresa ce (nolock) on ce.emp_codigo = tp.emp_codigo " 
        sql += f"inner join teso_tipo_requerimiento ttr (nolock) on ttr.tiporq_id = tp.tiporq_id where tp.emp_codigo = '{pCia}' and estado_plan not in ('C','T') and " 
        sql += "convert(varchar(10),fecha_programado,103) = convert(varchar(10),GETDATE(),103)" 
         
        conn = ConectaSQL_Produccion() 
        
 
        cur_sql_hoy = conn.cursor(as_dict = True) 
 
        cur_sql_hoy.execute(sql) 
 
        for row in cur_sql_hoy: 
            linea = {   "planificacion_id" : row['planificacion_id'], "nro_documento" : row['nro_documento'], "proveedor" : row['cliente_rs'],"ruc":row['ruc'],"serie" : row['serie'],  
                        "nro" : row['nro'], "motivo" : row['motivo'], "fecha" : row['fecha'], "cond_pago" : row['cond_pago'], "porc_total" : row['porc_total'],  
                        "fecha_programado" : row['fecha_programado'], "monto_programado" : row['monto_programado'], "tiporq_nombre" : row['tiporq_nombre']} 
            total.append(linea) 
 
        return total 
    except Exception as err : 
        return f"Error en el operación : {err}" 
    
def GuardarComentario(meData):
    try:
        print("Enviando a SAP...")

        trama = {"U_SWC_COMENTARIO" : meData.swc_comentarios.strip()}

        pSociedad = CiaSAP(meData.emp_codigo)

        urllib3.disable_warnings()

        token = logintoken(pSociedad)

        session = token['SessionId']

        #print(session)

        payload = json.dumps(trama)
        
        #print(payload)

        header = {
                    'Content-Type': 'application/json',
                    'Cookie': f'B1SESSION={session}; ROUTEID=.node3'
                }
        
        
        Servidor = getenv("SERVIDOR_SLD")        
        url = f"{Servidor}/b1s/v2/PurchaseInvoices({meData.nro_interno})"
        #print(url)
        response = requests.request("PATCH", url, headers=header, data=payload,verify=False)
        #print(response)
        resultado = ""
        mensaje = ""

        if(response.status_code!=204):
            #print(response.json())
            resultado = response.json()            
                
        if("error" in resultado):
        #    print("error")
            mensaje = resultado['error']['message']['value']
        #    print(mensaje)
        else:
            #print(resultado['DocNum'])
            mensaje = f"Se guardo el comentario en la factura de SAP"


        return mensaje
                    
    except Exception as err:
        print(f"Error en la operación : {err}.")
        return f"Error en la operación : {err}."