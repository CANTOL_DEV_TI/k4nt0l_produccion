from conexiones.conexion_sap_solo import conexion_SAP_Tecno
from conexiones.conexion_sql_solo import ConectaSQL_Produccion
from tesoreria.schemas.planificacionSchema import planificacion_Schema
from tesoreria.functions.loginSLDFunction import logintoken
from tesoreria.functions.bancosFunction import DataCuenta
from generales.generales import CiaSAP
from generales.configuracion import GetSeries
from os import getenv
import requests
import json
import urllib3
from typing import List
from decimal import Decimal
from datetime import date


#def GrabarPagoEfectuado(pCia : str, pPagosEfectuados : List[planificacion_Schema]):
def GrabarPagoEfectuado(pCia : str, pPlanificacionID : str):
    try:
        print("Enviando a SAP...")
        resultado = ""
        pSociedad = ""
        mensaje = ""

        pSociedad = CiaSAP(pCia)

        urllib3.disable_warnings()

        token = logintoken(pSociedad)

        session = token['SessionId']
                        
        datos = ExtraerDatosFacturaProveedor(pPlanificacionID)        

        payload = json.dumps(datos)
        
        header = {
                    'Content-Type': 'application/json',
                    'Cookie': f'B1SESSION={session}; ROUTEID=.node3'
                }
        
        
        Servidor = getenv("SERVIDOR_SLD")        
        url = f"{Servidor}/b1s/v1/VendorPayments"

        response = requests.request("POST", url, headers=header, data=payload,verify=False)
        
        if(response.status_code!=204):
            #print(response.json())
            resultado = response.json()            
                
        if("error" in resultado):
            print("error")
            mensaje = resultado['error']['message']['value']
            print(mensaje)
        else:
            print(resultado['DocNum'])
            print(resultado)
            x = ActualizarNroSAP(pPlanificacionID,pCia,resultado['DocNum'])
            print(x)
            mensaje = f"Se genero el pago efectuado en SAP nro {resultado['DocNum']}."

        return mensaje
                    
    except Exception as err:
        print(err)
        return f"Error en la operación : {err}."
    
def ExtraerDatosFacturaProveedor(pPlanificacion : str):
    try:
        datos = {}
        
        sql = "select emp_codigo as cia,nro_interno,'rSupplier' as DocType, convert(varchar(10),GETDATE(),23) as DocDate,cliente_cod as CardCode,cliente_rs as CardName,moneda as DocCurrency,"
        sql += "cuenta as TransferAccount,monto_programado - retencion as TransferSum,convert(varchar(10),GETDATE(),23) as TransferDate, convert(varchar(10),GETDATE(),23) as DueDate,0 as DocRate, nro_documento as Reference1, "
        sql += "concat('Pagos Efectuados via web - ',rtrim(cliente_cod)) as JournalRemarks,convert(varchar(10),GETDATE(),23) as TaxDate, 68 as Series, "
        sql += "'DOMICILIO_FISCAL' as PayToCode, 'tNO' as IsPayToBank, convert(varchar(10),GETDATE(),23) as VatDate, 'Operacion Prueba' as CounterReference,"
        sql += "case moneda when 'S/' then '4212101' else '4212102' end  as ControlAccount,'003' as U_MSSL_TMP, 0 as LineNum, nro_interno as DocEntry, "
        sql += "monto_programado as SumApplied, round(monto_programado / 3.7,2) as ApplyedSys, 1 as DocLine, 'it_PurchaseInvoice' as InvoiceType,"
        sql += "'41314' as CashFlowAssignmentsID, 7 as CashFlowLineItemID,  0 as Credit, 'pmtBankTransfer' as PaymentMeans, monto_programado  as AmountLC, banco as BankCode, retencion "
        sql += f" from teso_planificaciones tp where planificacion_id = {pPlanificacion}"
        
        #print(sql)
        
        conn = ConectaSQL_Produccion()

        cur_data = conn.cursor(as_dict = True)
        
        cur_data.execute(sql)

        for fila in cur_data:     
            Cta = DataCuenta(fila["cia"],fila["BankCode"],fila["TransferAccount"])

            sCia = CiaSAP(fila["cia"]) 
            
            dataEx = extraerDatosExtra( sCia,fila["nro_interno"])
            dataTC = extraeTC(sCia)   
            serieSAP = GetSeries(fila["cia"])  
            print("Serie...")
            print(serieSAP["serie_pagos_efectuados"])    

            TC_diario = 0.00
            monto_SumApplied = 0.00
            monto_AppliedSys = 0.00
            monto_AppliedFC = 0.00

            if(fila["DocCurrency"] == "S/"):
                TC_diario = 1
                monto_SumApplied = round(float(fila["TransferSum"]),2)
                monto_AppliedSys = round(float(fila["TransferSum"]),2)
                monto_AppliedFC = 0.00 #round(float(fila["TransferSum"]) / float(dataTC["TC"]),2)
            else:
                TC_diario = float(dataTC["TC"])
                monto_SumApplied = round(float(fila["TransferSum"]),2)
                monto_AppliedSys = round(float(fila["TransferSum"]) / float(dataTC["TC"]),2)
                monto_AppliedFC = round(float(fila["TransferSum"]) / float(dataTC["TC"]),2)

            datos = {   
                        "DocType" : fila["DocType"], 
                        "DocDate" : fila["DocDate"] , 
                        "CardCode" : fila["CardCode"],
                        "CardName" : fila["CardName"],
                        "Address" : dataEx["Address"],
                        "DocCurrency" : fila["DocCurrency"],
                        "TransferAccount" : fila["TransferAccount"], 
                        "TransferSum" : round(float(fila["TransferSum"]),2),
                        "TaxDate" : fila["TaxDate"], 
                        "Series" : int(serieSAP["serie_pagos_efectuados"]),
                        "DueDate" : fila["DueDate"],
                        #"BoeAccount" : "4231101",#fila["ControlAccount"], 
                        "TransferDate" : fila["TransferDate"],
                        "TransferReference" : "Prueba WEB",
                        "ControlAccount" : fila["ControlAccount"],  
                        "DocRate" :  TC_diario,                      
                        "U_MSSL_TMP" : fila["U_MSSL_TMP"],
                        "U_MSSL_CPP": "N",    
                        "U_SCR_TOS": "0",    
                        "U_SCR_MGD": "N",    
                        "U_MSS_ESTR": "0",    
                        "U_MSS_ANLR": "N",    
                        "U_MSS_ESRE": "0",                                                                                             
                        "PaymentInvoices" : [
                            {
                                "LineNum" : 0, 
                                "DocLine" : 0, 
                                "DocEntry" : fila["nro_interno"], 
                                "DocRate" : TC_diario,
                                #"AppliedSys" : monto_AppliedSys,
                                #"AppliedFC" : monto_AppliedFC,
                                "SumApplied" : monto_SumApplied ,
                                "WitholdingTaxApplied": round(float(fila["retencion"]),2),                                
                                "WitholdingTaxAppliedSC": round(float(fila["retencion"]) / float(dataTC["TC"]),2),
                                #"PaidSum" : 0 ,
                                "InvoiceType" : "it_PurchaseInvoice" 
                            }
                        ]#,
                        #"CashFlowAssignments" : [
                        #    {
                        #        "CashFlowLineItemID": 8,
                        #        "Credit": 0.0,
                        #        "PaymentMeans": "pmtBankTransfer",
                        #        #"CheckNumber":,
                        #        "AmountLC": monto_SumApplied,
                        #        "AmountFC": monto_AppliedSys,
                        #        "JDTLineId": 0}
                        #]                        
                         
                    }
            print(datos)

        return datos
    
    except Exception as err:
        return f"Error al extraer datos : {err}"

def extraeTC(pCia):
    sfecha = date.today().strftime('%Y%m%d')

    sqlT = f"SELECT \"Rate\" as TC FROM {pCia}.ORTT WHERE \"RateDate\" = '{sfecha}'"
    print(sqlT)
    datos = {}

    conn = conexion_SAP_Tecno()

    cur_data = conn.cursor()

    cur_data.execute(sqlT)

    for fila in cur_data:        
        datos = {"TC" : fila['TC']}
    
    conn.close()
        
    return datos

def extraerDatosExtra(pCia,pId):
    sqlT = f"SELECT \"Address\" as Address,\"DocRate\" as DocRate FROM {pCia}.OPCH WHERE \"DocEntry\"={pId}"
    #print(sqlT)
    datos = {}

    conn = conexion_SAP_Tecno()

    cur_data = conn.cursor()

    cur_data.execute(sqlT)

    for fila in cur_data:
        Direccion = fila['Address']
        datos = {"Address" : Direccion.replace("\r"," ") , "DocRate" : fila['DocRate']}
    
    conn.close()
        
    return datos

def ActualizarNroSAP(pID:str,pCia:str,pCodSAP:str):
    try:
        sqlU = f"update teso_planificaciones set docnum_sap='{pCodSAP}' where emp_codigo='{pCia}' and planificacion_id={pID}"
        print(sqlU)
        conn = ConectaSQL_Produccion()

        cur_data = conn.cursor()
        
        cur_data.execute(sqlU)

        conn.commit()

        return "Guardado..."
    except Exception as err:
        print(f"Error:{err}")
        return f"Error:{err}"