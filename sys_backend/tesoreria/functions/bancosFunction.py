from conexiones.conexion_sap_solo import conexion_SAP_Tecno
from generales.generales import CiaSAP

def ListaBancos(pCia):
    try:
        sCia = ""

        sCia = CiaSAP(pCia)
        
        fila = {}
        lista = []
        
        sql = f"SELECT \"BankCode\" as Codigo, \"BankName\" as Nombre FROM {sCia}.ODSC"

        conn = conexion_SAP_Tecno()

        cur_LB = conn.cursor()

        cur_LB.execute(sql)

        for row in cur_LB:
            fila = {"Codigo" : row['Codigo'], "Nombre" : row['Nombre']}
            lista.append(fila)
        
        conn.close()

        return lista
    except Exception as err:
        return f"Error en la operación [Lista Bancos] : {err}."
    
def ListaCuentas(pCia, pBanco):
    try:
        sCia = ""

        sCia = CiaSAP(pCia)

        if(pBanco == ""):
           pBanco = "XXX"


        fila = {}
        lista = []
        
        sql = f"SELECT \"GLAccount\" as Codigo, \"AcctName\" as Nombre FROM {sCia}.DSC1 WHERE \"BankCode\" = '{pBanco}' "

        conn = conexion_SAP_Tecno()

        cur_LB = conn.cursor()

        cur_LB.execute(sql)

        fila = {"Codigo" : "NONE", "Nombre" : "Seleccione..."}
        lista.append(fila)

        for row in cur_LB:
            fila = {"Codigo" : row['Codigo'], "Nombre" : row['Nombre']}
            lista.append(fila)
        
        conn.close()

        return lista
    except Exception as err:
        return f"Error en la operación [Lista Cuentas Bancarias] : {err}."

def DataCuenta(pCia, pBanco, pCta):
    try:
        sCia = ""
        data = ""
        
        sCia = CiaSAP(pCia)

        if(pBanco == ""):
           pBanco = "XXX"
                        
        sql = f"SELECT \"Account\" as Cta FROM {sCia}.DSC1 WHERE \"BankCode\" = '{pBanco}' and \"GLAccount\" = '{pCta}' "

        conn = conexion_SAP_Tecno()

        cur_LB = conn.cursor()

        cur_LB.execute(sql)
                
        for row in cur_LB:
            data = row['Cta']

        print("entro a datacuenta")    
        #print(data)
        conn.close()

        return data
    except Exception as err:
        return f"Error en la operación [Datos Cuentas Bancarias] : {err}."