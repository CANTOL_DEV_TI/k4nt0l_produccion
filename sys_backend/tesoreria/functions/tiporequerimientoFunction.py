from conexiones.conexion_mmsql import conexion_mssql

def agregar(meData):
    try:
        valores = []
        valores.append(meData.tiporq_nombre)    

        sql = "insert into teso_tipo_requerimiento(tiporq_nombre) values(%s)"

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        #print(sql)
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def actualizar(meData):
    try:
        valores = []
        valores.append(meData.tiporq_nombre)
        valores.append(meData.tiporq_id)
        
        sql = "update teso_tipo_requerimiento set "
        sql += "tiporq_nombre=%s "
        sql += "where tiporq_id=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        #print(sql)
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."


def eliminar(meData):
    try:
        valores = []
        valores.append(meData.tiporq_id)
        sql = "delete from teso_tipo_requerimiento "
        sql += "where tiporq_id=%s "

        meConexion = conexion_mssql()
        respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
        #print(sql)
        return respuesta
    except Exception as err:
        return f"Error en la operación : {err}."