from sqlalchemy import Column, String, Integer

from conexiones.conexion_sql import Base


class rubro_Model(Base):
    __tablename__ = 'teso_rubro'
    rubro_id = Column(Integer, primary_key=True)
    emp_codigo = Column(String)
    rubro_nombre = Column(String)
    codigo_cc = Column(String)


    def __str__(self):
        return self.rubro_nombre