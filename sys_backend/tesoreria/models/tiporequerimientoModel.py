from sqlalchemy import Column, String, Integer

from conexiones.conexion_sql import Base


class tiporequerimiento_Model(Base):
    __tablename__ = 'teso_tipo_requerimiento'
    tiporq_id = Column(Integer, primary_key=True)
    tiporq_nombre = Column(String)    


    def __str__(self):
        return self.tiporq_nombre