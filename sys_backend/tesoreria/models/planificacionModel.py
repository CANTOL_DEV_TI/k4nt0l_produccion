from sqlalchemy import Column, String, Integer, DECIMAL, DateTime

from conexiones.conexion_sql import Base


class planificacion_Model(Base):
    __tablename__ = 'teso_planificaciones'
    planificacion_id = Column(Integer, primary_key=True)
    emp_codigo = Column(String)
    nro_interno = Column(String)
    nro_documento = Column(String)
    cliente_cod = Column(String)
    cliente_rs = Column(String)
    ruc = Column(String)
    cc1_codigo = Column(String)
    cc1_nombre = Column(String)
    tipo_documento = Column(String)
    serie = Column(String)
    nro = Column(String)
    motivo = Column(String)
    fecha = Column(DateTime)
    cond_pago = Column(String)
    fecha_venc = Column(DateTime)
    porc = Column(DECIMAL)
    porc_total = Column(DECIMAL)
    atraso_dias = Column(Integer)
    mes_pago = Column(Integer)
    ano_pago = Column(Integer)
    moneda = Column(String)
    total_doc = Column(String)
    aplica_detraccion = Column(String)
    pagado = Column(DECIMAL)
    cc2 = Column(String)


    def __str__(self):
        return self.cliente_rs