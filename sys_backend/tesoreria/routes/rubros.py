from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from sqlalchemy.orm.session import Session
from conexiones.conexion_sql import get_db

from tesoreria.functions.rubrosFunction import agregar,actualizar,eliminar
from tesoreria.schemas.rubrosSchema import rubro_Schema
from tesoreria.models.rubrosModel import rubro_Model

ruta = APIRouter(
    prefix="/rubros", tags=["Rubros"]
)

@ruta.get('/{txtCia}-{txtFind}', status_code=status.HTTP_200_OK)
def listado(txtCia : str, txtFind: str, db: Session = Depends(get_db)):
    data = db.query(rubro_Model).filter(rubro_Model.emp_codigo == txtCia, rubro_Model.rubro_nombre.contains(txtFind.strip())).all()
    return data

@ruta.post('/')
async def insert(info: rubro_Schema):    
    return agregar(info)

@ruta.put('/')
async def update(info:rubro_Schema):
    return actualizar(info)

@ruta.delete('/')
async def delete(info:rubro_Schema):
    return eliminar(info)