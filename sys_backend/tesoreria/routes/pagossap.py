from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from typing import List
from tesoreria.functions.pagosSAPFunction import GrabarPagoEfectuado
from tesoreria.schemas.planificacionSchema import planificacion_Schema

ruta = APIRouter(
    prefix="/pagossap", tags=["Pagos SAP"]
)

@ruta.post("/")
async def GenerarPagosSAP(xCia:str, info:List[planificacion_Schema]):
    #print(info)
    return GrabarPagoEfectuado(xCia,info)