from fastapi import APIRouter
from tesoreria.routes import pagos,rubros,tiporequerimiento,programados,bancos,pagossap,centrocostousuario

ruta = APIRouter(
    prefix="/tesoreria"
)

ruta.include_router(pagos.ruta)
ruta.include_router(rubros.ruta)
ruta.include_router(tiporequerimiento.ruta)
ruta.include_router(programados.ruta)
ruta.include_router(bancos.ruta)
ruta.include_router(pagossap.ruta)
ruta.include_router(centrocostousuario.ruta)