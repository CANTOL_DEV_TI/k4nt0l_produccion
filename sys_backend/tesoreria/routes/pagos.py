from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse

from tesoreria.schemas.pagosSchema import FiltroListaFacturas
from tesoreria.schemas.planificacionSchema import planificacion_Schema
from tesoreria.functions.pagosFunction import lista_pagos,total_pagos
from tesoreria.functions.planificacionFunction import agregar,actualizar,eliminar

ruta = APIRouter(
    prefix="/pagos", tags=["Pagos"]
)


@ruta.post('/ListadoPendientes')
async def ListadoPendientes(info: FiltroListaFacturas):    
    return lista_pagos(info.Cia,info.Desde,info.Hasta,info.Proveedor,info.Moneda,info.Ordenar,info.TipoOrden,info.Detraccion)

@ruta.post('/')
async def AgregarProgramacion(info: planificacion_Schema):    
    return agregar(info)

@ruta.put('/')
async def ActualizaProgramacion(info:planificacion_Schema):
    return actualizar(info)

@ruta.delete('/')
async def EliminarProgramacion(info:planificacion_Schema):
    return eliminar(info)

@ruta.post('/TotalesPendientes')
async def TotalesPagosPendientes(info:FiltroListaFacturas):
    return total_pagos(info.Cia,info.Desde,info.Hasta,info.Proveedor,info.Moneda, info.Detraccion)