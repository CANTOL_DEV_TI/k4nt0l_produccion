from fastapi import APIRouter, Depends, status, UploadFile, File, Request

from tesoreria.functions.centrocostoFunctions import AgregaCC,ListarCCxUsuario
from tesoreria.schemas.centrocostousuarioSchema import CCxUsuario

ruta = APIRouter(
    prefix="/ccxusuario", tags=["Centro Costo por Usuario"]
)

@ruta.post('/', status_code=status.HTTP_200_OK)
def listadoCuentas(pData : CCxUsuario):
    data = AgregaCC(pData)
    return data

@ruta.get('/{pCia}/{pUsuario}',status_code=status.HTTP_200_OK)
def listaccs(pCia,pUsuario):
    return ListarCCxUsuario(pUsuario,pCia)
