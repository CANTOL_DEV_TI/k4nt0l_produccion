from fastapi import APIRouter, Depends, status, UploadFile, File, Request

from tesoreria.functions.bancosFunction import ListaBancos, ListaCuentas

ruta = APIRouter(
    prefix="/bancos", tags=["Bancos"]
)

@ruta.get('/{pCia}', status_code=status.HTTP_200_OK)
def listado(pCia : str):
    data = ListaBancos(pCia)
    return data

@ruta.get('/Cuentas/{pCia}-{pBanco}', status_code=status.HTTP_200_OK)
def listadoCuentas(pCia : str, pBanco : str):
    data = ListaCuentas(pCia, pBanco)
    return data
