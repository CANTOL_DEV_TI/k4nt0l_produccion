from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from tesoreria.schemas.reportepagosfiltroSchema import FiltroReportePagos
from tesoreria.schemas.planificacionSchema import planificacion_Schema
from tesoreria.functions.planificacionFunction import ListadoProgramados,TerminarPago,ReProgramarPago,AnularPago,HistorialProgramaciones,TotalesListadoProgramados, PagosdeHoy,GuardarComentario

ruta = APIRouter(
    prefix="/pagosprogramados", tags=["Pagos Programados"]
)

@ruta.post('/')
async def ListadoPagosProgramados(info:FiltroReportePagos):
    return ListadoProgramados(info)

@ruta.post('/terminar/')
async def TerminarPagoProgramado(info:planificacion_Schema):
    #print(info)
    return TerminarPago(info)

@ruta.put('/reprogramar/')
async def ReprogramarPagoProgramado(info:planificacion_Schema):
    return ReProgramarPago(info)

@ruta.delete('/anular/')
async def AnularPagoProgramado(info:planificacion_Schema):
    return AnularPago(info)

@ruta.post('/historial/')
async def HistorialCambios(info:planificacion_Schema):
    return HistorialProgramaciones(info)

@ruta.post('/totales/')
async def TotalesProgramados(info:FiltroReportePagos):
    return TotalesListadoProgramados(info)

@ruta.get('/hoy/{pCia}')
async def getPagosHoy(pCia:str):
    return PagosdeHoy(pCia)

@ruta.patch('/guardarcomentario/')
async def guardarComentarios(info:planificacion_Schema):
    print(info)
    return GuardarComentario(info)