from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from sqlalchemy.orm.session import Session
from conexiones.conexion_sql import get_db

from tesoreria.functions.tiporequerimientoFunction import agregar,actualizar,eliminar
from tesoreria.schemas.tiporequerimientoSchema import tiporequerimiento_Schema
from tesoreria.models.tiporequerimientoModel import tiporequerimiento_Model

ruta = APIRouter(
    prefix="/tiporequerimiento", tags=["Tipo Requerimiento"]
)

@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK)
def listado( txtFind: str, db: Session = Depends(get_db)):
    data = db.query(tiporequerimiento_Model).filter(tiporequerimiento_Model.tiporq_nombre.contains(txtFind.strip())).all()
    return data

@ruta.post('/')
async def insert(info: tiporequerimiento_Schema):    
    return agregar(info)

@ruta.put('/')
async def update(info:tiporequerimiento_Schema):
    return actualizar(info)

@ruta.delete('/')
async def delete(info:tiporequerimiento_Schema):
    return eliminar(info)