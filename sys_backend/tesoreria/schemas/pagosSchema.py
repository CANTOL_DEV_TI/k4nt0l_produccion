from typing import List, Optional
from pydantic import BaseModel

class FiltroListaFacturas(BaseModel):
    Cia : Optional[str]
    Desde : Optional[str]
    Hasta : Optional[str]
    Proveedor : Optional[str]
    Moneda : Optional[str]
    Ordenar : Optional[str]
    TipoOrden : Optional[str]
    Detraccion : Optional[str]



