from typing import List, Optional
from pydantic import BaseModel
from tesoreria.schemas.planificacionSchema import planificacion_Schema

class GuardarMasivo_Schema(BaseModel):
    Masivo : Optional[List[planificacion_Schema]]
    TRQ : Optional[int]
    FechaRQ : Optional[str]
    usuario : Optional[str]