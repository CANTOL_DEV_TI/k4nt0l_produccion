from typing import Optional
from pydantic import BaseModel


class tiporequerimiento_Schema(BaseModel):
    tiporq_id : Optional[int]
    tiporq_nombre : Optional[str]
    
    class Config:
        orm_mode = True