from typing import List, Optional
from pydantic import BaseModel
from decimal import Decimal

class PaymentInvoices(BaseModel):
    LineNum : Optional[int]
    DocEntry : Optional[int]
    SumApplied : Optional[Decimal]
    AppliedFC : Optional[Decimal]
    AppliedSys : Optional[Decimal]
    DocRate : Optional[Decimal]
    DocLine : Optional[int]
    InvoiceType : Optional[str] #"it_PurchaseInvoice",
    DiscountPercent : Optional[Decimal]
    PaidSum : Optional[Decimal]
    InstallmentId : Optional[int]
    WitholdingTaxApplied : Optional[Decimal]
    WitholdingTaxAppliedFC : Optional[Decimal]
    WitholdingTaxAppliedSC : Optional[Decimal]
    LinkDate : Optional[str]
    DistributionRule : Optional[str]
    DistributionRule2 : Optional[str]
    DistributionRule3 : Optional[str]
    DistributionRule4 : Optional[str]
    DistributionRule5 : Optional[str]
    TotalDiscount : Optional[Decimal]
    TotalDiscountFC : Optional[Decimal]
    TotalDiscountSC : Optional[Decimal] 

class CashFlowAssignments(BaseModel):   
    CashFlowAssignmentsID : Optional[int] #41316
    CashFlowLineItemID : Optional[int] # 7
    Credit : Optional[Decimal] #0.0
    PaymentMeans : Optional[str] #"pmtBankTransfer"
    CheckNumber : Optional[str] # null
    AmountLC : Optional[Decimal] #4171.710,
    AmountFC : Optional[Decimal] #1110.680,
    JDTLineId : Optional[int]#0        
                
class VendorPayment(BaseModel):
    DocNum : Optional[int]
    DocType : Optional[str] #"rSupplier",
    HandWritten : Optional[str] #"tNO"
    Printed : Optional[str] #"tNO",
    DocDate : Optional[str] #"2024-04-29",
    CardCode : Optional[str] #"P20551547168",
    CardName : Optional[str]#"MSS SEIDOR PERU S.A.C.",
    Address : Optional[str] #"JR. VITTORE SCARPAZZA CARPACC NRO. 250 INT. 505 URB. SAN BORJA 1º ETAPA\r\r150130 SAN BORJA\rPERU",
    CashAccount : Optional[str]
    DocCurrency : Optional[str] #"US$",
    CashSum : Optional[Decimal]
    CheckAccount : Optional[str]
    TransferAccount : Optional[str] #"1041104",
    TransferSum : Optional[Decimal] #4171.710,
    TransferDate : Optional[str] #"2024-04-29",
    TransferReference : Optional[str] #"29042024"
    LocalCurrency : Optional[str] #"tNO",
    DocRate : Optional[Decimal] #3.7560,
    Reference1 : Optional[str] #"5607918",
    Reference2 : Optional[str]
    CounterReference : Optional[str]
    Remarks : Optional[str]
    JournalRemarks : Optional[str] #"Pagos efectuados - P20551547168",
    SplitTransaction : Optional[str] #"tNO"
    ContactPersonCode : Optional[int] #8492,
    ApplyVAT : Optional[str] #"tNO"
    TaxDate : Optional[str] #"2024-04-29"
    Series : Optional[int] #64
    BankCode : Optional[str]
    BankAccount : Optional[str]
    DiscountPercent : Optional[Decimal]
    ProjectCode : Optional[str]
    CurrencyIsLocal : Optional[str] #"tNO"
    DeductionPercent : Optional[Decimal]
    DeductionSum : Optional[Decimal]
    CashSumFC : Optional[Decimal]
    CashSumSys : Optional[Decimal]
    BoeAccount : Optional[str] #"4231101"
    BillOfExchangeAmount : Optional[Decimal]
    BillofExchangeStatus : Optional[str]
    BillOfExchangeAmountFC : Optional[Decimal]
    BillOfExchangeAmountSC : Optional[Decimal]
    BillOfExchangeAgent : Optional[str]
    WTCode : Optional[str]
    WTAmount : Optional[Decimal]
    WTAmountFC : Optional[Decimal]
    WTAmountSC : Optional[Decimal]
    WTAccount : Optional[str]
    WTTaxableAmount : Optional[Decimal]
    Proforma : Optional[str] #"tNO"
    PayToBankCode : Optional[str]
    PayToBankBranch : Optional[str]
    PayToBankAccountNo : Optional[str]
    PayToCode : Optional[str] #"DOMICILIO_FISCAL"
    PayToBankCountry : Optional[str]
    IsPayToBank : Optional[str] #"tNO"
    DocEntry : Optional[int]
    PaymentPriority : Optional[str] #"bopp_Priority_6"
    TaxGroup : Optional[str]
    BankChargeAmount : Optional[Decimal]
    BankChargeAmountInFC : Optional[Decimal]
    BankChargeAmountInSC : Optional[Decimal]
    UnderOverpaymentdifference : Optional[Decimal]
    UnderOverpaymentdiffSC : Optional[Decimal]
    WtBaseSum : Optional[Decimal]
    WtBaseSumFC : Optional[Decimal]
    WtBaseSumSC : Optional[Decimal]
    VatDate : Optional[str] #"2024-04-29"
    TransactionCode : Optional[str]
    PaymentType : Optional[str] #"bopt_None"
    TransferRealAmount : Optional[Decimal]
    DocObjectCode : Optional[str] #"bopot_OutgoingPayments"
    DocTypte : Optional[str] #"rSupplier"
    DueDate : Optional[str] #"2024-04-29"
    LocationCode : Optional[str]
    Cancelled : Optional[str] #"tNO"
    ControlAccount : Optional[str] #"4212102"
    UnderOverpaymentdiffFC : Optional[Decimal]
    AuthorizationStatus : Optional[str] #"pasWithout"
    BPLID : Optional[str]
    BPLName : Optional[str]
    VATRegNum : Optional[str]
    BlanketAgreement : Optional[str]
    PaymentByWTCertif : Optional[str] #"tNO"
    Cig : Optional[str]
    Cup : Optional[str]
    AttachmentEntry : Optional[str]
    U_MSSL_TMP : Optional[str] #"003"
    U_MSSL_NCD : Optional[str]
    U_MSSL_FCD : Optional[str]
    U_MSSL_SCR : Optional[str]
    U_MSSL_NCR : Optional[str]
    U_MSSL_SNB : Optional[str]
    U_MSSL_BAN : Optional[str]
    U_MSSL_CPP : Optional[str] #"N"
    U_MSS_SerCertPerc : Optional[str]
    U_MSS_CorCertPerc : Optional[str]
    U_MSS_CCOBRADOR : Optional[str]
    U_MSS_CHOJARUTA : Optional[str]
    U_MSS_ORPT : Optional[str]
    U_SCR_TOS : Optional[str]
    U_SCR_COS : Optional[str]
    U_SCR_MGD : Optional[str]
    U_SCR_ADJ : Optional[str]
    U_SCR_MMI : Optional[str]
    U_MSS_ESTR : Optional[str]
    U_MSS_OBSRE : Optional[str]
    U_MSS_ANLR : Optional[str]
    U_MSS_MORE : Optional[str]
    U_MSS_ESRE : Optional[str]
    U_MSS_OBS : Optional[str]    
    PaymentInvoices : Optional[List[PaymentInvoices]]
    CashFlowAssignments : Optional[List[CashFlowAssignments]]