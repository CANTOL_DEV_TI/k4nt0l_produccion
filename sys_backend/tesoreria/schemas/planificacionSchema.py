from typing import Optional 
from pydantic import BaseModel 
from datetime import datetime 
from decimal import Decimal 
 
class planificacion_Schema(BaseModel):     
    planificacion_id : Optional[int] 
    emp_codigo : Optional[str] 
    nro_interno : Optional[str] 
    nro_documento : Optional[str] 
    cliente_cod : Optional[str] 
    cliente_rs : Optional[str] 
    ruc : Optional[str] 
    cc1_codigo : Optional[str] 
    cc1_nombre : Optional[str] 
    tipo_documento : Optional[str] 
    serie : Optional[str] 
    nro : Optional[str] 
    motivo : Optional[str] 
    fecha : Optional[str] 
    cond_pago : Optional[str] 
    fecha_venc : Optional[str] 
    porc : Optional[Decimal] 
    porc_total : Optional[Decimal] 
    atraso_dias : Optional[int] 
    mes_pago : Optional[int] 
    ano_pago : Optional[int] 
    moneda : Optional[str] 
    total_doc : Optional[Decimal] 
    aplica_detraccion : Optional[str] 
    pagado : Optional[Decimal] 
    cc2 : Optional[str] 
    seleccion : Optional[str] 
    fecha_programado : Optional[str] 
    monto_programado : Optional[Decimal] 
    tiporq_id : Optional[int] 
    usuario : Optional[str] 
    retencion : Optional[Decimal] 
    banco : Optional[str] 
    cuenta : Optional[str] 
    fecha_emision : Optional[str]
    swc_comentarios : Optional[str] 
 
    class Config: 
        orm_mode = True