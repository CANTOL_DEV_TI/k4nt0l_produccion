from typing import Optional
from pydantic import BaseModel

class FiltroReportePagos(BaseModel):
    emp_codigo : Optional[str]
    todo_fpr : Optional[str]
    fprdesde : Optional[str]
    fprhasta : Optional[str]
    todo_fv : Optional[str]
    fvdesde : Optional[str]
    fvhasta : Optional[str]
    todo_proveedor : Optional[str]
    proveedor : Optional[str]
    todo_cc : Optional[str]
    cc : Optional[str]
    todo_cp : Optional[str]
    cond_pago : Optional[str]
    todo_usuario : Optional[str]
    todo_bancos : Optional[str]
    banco : Optional[str]
    todo_cuentas : Optional[str]
    cuenta : Optional[str]
    usuario : Optional[str]
    moneda : Optional[str]