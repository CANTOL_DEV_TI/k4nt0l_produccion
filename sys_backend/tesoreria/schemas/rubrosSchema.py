from typing import Optional
from pydantic import BaseModel


class rubro_Schema(BaseModel):
    rubro_id : Optional[int]
    emp_codigo : Optional[str]
    rubro_nombre : Optional[str]
    codigo_cc : Optional[str]

    class Config:
        orm_mode = True