from typing import Optional
from pydantic import BaseModel

class CCxUsuario(BaseModel):
    Cia : Optional[str]
    Usuario : Optional[str]
    CCCodigo : Optional[str]
    CCNombre : Optional[str]

    class Config: 
        orm_mode = True