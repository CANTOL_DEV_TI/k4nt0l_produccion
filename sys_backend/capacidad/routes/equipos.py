from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from capacidad.models.EquiposModel import EquipoModel
from capacidad.schemas.EquipoSchema import EquipoSchema
from capacidad.functions import EquipoFunction


ruta = APIRouter(
    prefix="/Equipos",tags=["Equipos"]
)

@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = db.query(EquipoModel).filter(EquipoModel.equipo.contains(txtFind.strip())).all()
    return data

@ruta.get('/Activos/', status_code=status.HTTP_200_OK)
def showActivos(db: Session = Depends(get_db)):
    data = db.query(EquipoModel).filter(EquipoModel.sw_estado.contains("1")).all()
    return data

@ruta.post('/')
async def grabar(info: EquipoSchema):
    # resultado = await info.json()
    return EquipoFunction.insert(info)

@ruta.put('/')
async def actualizar(info: EquipoSchema):
    return EquipoFunction.update(info)

@ruta.delete('/')
async def eliminar(info: EquipoSchema):
    return EquipoFunction.delete(info)