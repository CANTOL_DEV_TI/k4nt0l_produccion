from fastapi import APIRouter, Depends, status
from capacidad.schemas.tipoEquipoSchema import TipoEquipoSchema
from capacidad.functions import TipoEquipo
from conexiones.conexion_sql import get_db
from sqlalchemy.orm import Session
from typing import Union

ruta = APIRouter(
    prefix="/tipo_equipo"
)


@ruta.get('/',  tags=["Tipo Equipo"], status_code=status.HTTP_200_OK)
def tipoEquipo_all(db: Session = Depends(get_db)):
    data = TipoEquipo.tipoEquipo_all(db)
    return data


@ruta.get('/{id_tpequipo}', tags=["Tipo Equipo"], status_code=status.HTTP_200_OK)
def obtener_tipo_equipo(id_tpequipo: int, db: Session = Depends(get_db)):
    data = TipoEquipo.tipoEquipo_id(id_tpequipo, db)
    return data


@ruta.post('/', tags=["Tipo Equipo"], status_code=status.HTTP_201_CREATED)
def crear_tipo_equipo(tipo_equipo: TipoEquipoSchema, db: Session = Depends(get_db)):
    TipoEquipo.tipoEquipo_insert(tipo_equipo, db)
    return {"respuesta": "Tipo Equipo creado satisfactoriamente!!"}


@ruta.put('/{id_tpequipo}', tags=["Tipo Equipo"], status_code=status.HTTP_200_OK)
def actualizar_tipo_equipo(id_tpequipo: int, tipo_equipo: TipoEquipoSchema, db: Session = Depends(get_db)):
    res = TipoEquipo.tipoEquipo_update(id_tpequipo, tipo_equipo, db)
    return res

@ruta.delete('/{id_tpequipo}',tags=["Tipo Equipo"], status_code=status.HTTP_200_OK)
def eliminar_tipo_equipo(id_tpequipo:int,db:Session = Depends(get_db)):
    res = TipoEquipo.tipoEquipo_delete(id_tpequipo, db)
    return res 