from fastapi import APIRouter, Depends, status
from capacidad.schemas.periodoLaboralSchema import PeriodoLaboralSchema
from capacidad.functions import PeriodoLaboral
from conexiones.conexion_sql import get_db
from sqlalchemy.orm import Session
from typing import Union

ruta = APIRouter(
    prefix="/periodo_laboral"
)


@ruta.get('/',  tags=["Periodo Laboral"], status_code=status.HTTP_200_OK)
def lista_periodo_laboral(db: Session = Depends(get_db)):
    data = PeriodoLaboral.periodoLaboral_all(db)
    return data


@ruta.get('/{id_per_lab}', tags=["Periodo Laboral"], status_code=status.HTTP_200_OK)
def obtener_periodo_laboral(id_per_lab: int, db: Session = Depends(get_db)):
    data = PeriodoLaboral.periodoLaboral_id(id_per_lab, db)
    return data


@ruta.post('/', tags=["Periodo Laboral"], status_code=status.HTTP_201_CREATED)
def crear_periodo_laboral(periodo_laboral: PeriodoLaboralSchema, db: Session = Depends(get_db)):
    PeriodoLaboral.periodoLaboral_insert(periodo_laboral, db)
    return {"respuesta": "Tipo Equipo creado satisfactoriamente!!"}


@ruta.put('/{id_per_lab}', tags=["Periodo Laboral"], status_code=status.HTTP_200_OK)
def actualizar_periodo_laboral(id_per_lab: int, periodo_laboral: PeriodoLaboralSchema, db: Session = Depends(get_db)):
    res = PeriodoLaboral.periodoLaboral_update(id_per_lab, periodo_laboral, db)
    return res

@ruta.delete('/{id_per_lab}',tags=["Periodo Laboral"], status_code=status.HTTP_200_OK)
def eliminar_periodo_laboral(id_per_lab:int,db:Session = Depends(get_db)):
    res = PeriodoLaboral.periodoLaboral_delete(id_per_lab, db)
    return res 