from fastapi import APIRouter, Depends, status, UploadFile, File, Request
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql import get_db
from capacidad.models.tipoOperacionModel import TipoOperacionModel
from capacidad.schemas.tipoOperacionSchema import TipoOperacionSchema
from capacidad.functions import tipoOperacionFunction


ruta = APIRouter(
    prefix="/tipoOperacion",tags=["TipoOperacion"]
)

@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = db.query(TipoOperacionModel).filter(TipoOperacionModel.tipo_operacion.contains(txtFind.strip())).all()
    return data

@ruta.get('/Activos/', status_code=status.HTTP_200_OK)
def showActivos(db: Session = Depends(get_db)):
    data = db.query(TipoOperacionModel).filter(TipoOperacionModel.sw_estado.contains("1")).all()
    return data

@ruta.post('/')
async def grabar(info: TipoOperacionSchema):
    # resultado = await info.json()
    return tipoOperacionFunction.insert(info)

@ruta.put('/')
async def actualizar(info: TipoOperacionSchema):
    return tipoOperacionFunction.update(info)

@ruta.delete('/')
async def eliminar(info: TipoOperacionSchema):
    return tipoOperacionFunction.delete(info)
