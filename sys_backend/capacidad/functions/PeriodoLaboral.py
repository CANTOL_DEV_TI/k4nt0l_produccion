from fastapi import HTTPException, status
from sqlalchemy import select
from sqlalchemy.orm import Session
from capacidad.models.periodoLaboralModel import PeriodoLaboralModel


def periodoLaboral_all(db: Session):
    data = db.query(PeriodoLaboralModel).all()
    # data = db.scalars(select(PeriodoLaboralModel).where(PeriodoLaboralModel.id_tpequipo == 1)).all()

    # data = db.execute(select(PeriodoLaboralModel)).all()
    # data = db.execute(select(PeriodoLaboralModel).order_by(PeriodoLaboralModel.id_tpequipo)).scalars()

    return data


def periodoLaboral_id(id_per_lab, db: Session):
    usuario = db.query(PeriodoLaboralModel).filter(
        PeriodoLaboralModel.id_tpequipo == id_per_lab).first()
    if not usuario:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No existe el usuario con el id {id_per_lab}"
        )
    return usuario


def periodoLaboral_insert(tipo_equipo, db: Session):
    tipo_equipo = tipo_equipo.dict()
    try:
        nuevo_tipo_equipo = PeriodoLaboralModel(
            tipo_equipo=tipo_equipo["tipo_equipo"],            
            cod_usuario=tipo_equipo["cod_usuario"],
            sw_estado="1",
        )

        db.add(nuevo_tipo_equipo)
        # db.execute(select(User).where(User.id == 5))
        db.commit()
        db.refresh(nuevo_tipo_equipo)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"Error creando tipo equipo {e}"
        )


def periodoLaboral_update(id_per_lab, u_tipo_equipo, db: Session):
    tipo_equipo = db.query(PeriodoLaboralModel).filter(
        PeriodoLaboralModel.id_tpequipo == id_per_lab)
    if not tipo_equipo.first():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No existe el tipo equipo con el id {id_per_lab}"
        )
    tipo_equipo.update(u_tipo_equipo.dict(exclude_unset=True))
    db.commit()
    return {"respuesta": "tipo equipo actualizado correctamente!"}


def periodoLaboral_delete(id_per_lab, db: Session):
    tipo_equipo = db.query(PeriodoLaboralModel).filter(
        PeriodoLaboralModel.id_tpequipo == id_per_lab)
    if not tipo_equipo.first():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No existe el tipo equipo con el id {id_per_lab} por lo tanto no se elimina"
        )
    tipo_equipo.delete(synchronize_session=False)
    db.commit()
    return {"respuesta": "tipo equipo eliminado correctamente!"}
