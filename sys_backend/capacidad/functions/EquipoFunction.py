from conexiones.conexion_mmsql import conexion_mssql


def insert(meData):
    valores = []
    valores.append(meData.cod_equipo)
    valores.append(meData.cod_ccosto)
    valores.append(meData.id_tpequipo)
    valores.append(meData.equipo)
    valores.append(meData.cant_turnos)
    valores.append(meData.cant_personas)    
    valores.append(meData.cod_usuario)
    sql = "insert into equipo(cod_equipo,cod_ccosto,id_tpequipo,equipo,cant_turnos,cant_personas,sw_estado,cod_usuario,fecre) values(%s,%s,%s,%s,%s,%s,'1',%s,getdate())"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta


def update(meData):
    valores = []
    valores.append(meData.cod_equipo)
    valores.append(meData.cod_ccosto)
    valores.append(meData.id_tpequipo)
    valores.append(meData.equipo)
    valores.append(meData.cant_turnos)
    valores.append(meData.cant_personas)
    valores.append(meData.id_equipo)
    sql = "update equipo set cod_equipo=%s, cod_ccosto=%s, id_tpequipo=%s, equipo=%s, cant_turnos=%s, cant_personas=%s "    
    sql += "where id_equipo=%s"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta


def delete(meData):
    valores = []
    valores.append(meData.id_equipo)
    sql = "update equipo set sw_estado='9' "
    sql += "where id_equipo=%s "

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta