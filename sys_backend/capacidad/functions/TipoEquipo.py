from fastapi import HTTPException, status
from sqlalchemy import select
from sqlalchemy.orm import Session
from capacidad.models.tipoEquipoModel import TipoEquipoModel


def tipoEquipo_all(db: Session):
    data = db.query(TipoEquipoModel).all()
    # data = db.scalars(select(TipoEquipoModel).where(TipoEquipoModel.id_tpequipo == 1)).all()

    # data = db.execute(select(TipoEquipoModel)).all()
    # data = db.execute(select(TipoEquipoModel).order_by(TipoEquipoModel.id_tpequipo)).scalars()

    return data


def tipoEquipo_id(id_tipo_equipo, db: Session):
    usuario = db.query(TipoEquipoModel).filter(
        TipoEquipoModel.id_tpequipo == id_tipo_equipo).first()
    if not usuario:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No existe el usuario con el id {id_tipo_equipo}"
        )
    return usuario


def tipoEquipo_insert(tipo_equipo, db: Session):
    tipo_equipo = tipo_equipo.dict()
    try:
        nuevo_tipo_equipo = TipoEquipoModel(
            tipo_equipo=tipo_equipo["tipo_equipo"],            
            cod_usuario=tipo_equipo["cod_usuario"],
            sw_estado="1",
        )

        db.add(nuevo_tipo_equipo)
        # db.execute(select(User).where(User.id == 5))
        db.commit()
        db.refresh(nuevo_tipo_equipo)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"Error creando tipo equipo {e}"
        )


def tipoEquipo_update(id_tipo_equipo, u_tipo_equipo, db: Session):
    tipo_equipo = db.query(TipoEquipoModel).filter(
        TipoEquipoModel.id_tpequipo == id_tipo_equipo)
    if not tipo_equipo.first():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No existe el tipo equipo con el id {id_tipo_equipo}"
        )
    tipo_equipo.update(u_tipo_equipo.dict(exclude_unset=True))
    db.commit()
    return {"respuesta": "tipo equipo actualizado correctamente!"}


def tipoEquipo_delete(id_tipo_equipo, db: Session):
    tipo_equipo = db.query(TipoEquipoModel).filter(
        TipoEquipoModel.id_tpequipo == id_tipo_equipo)
    if not tipo_equipo.first():
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"No existe el tipo equipo con el id {id_tipo_equipo} por lo tanto no se elimina"
        )
    tipo_equipo.delete(synchronize_session=False)
    db.commit()
    return {"respuesta": "tipo equipo eliminado correctamente!"}
