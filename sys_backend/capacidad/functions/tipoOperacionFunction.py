from conexiones.conexion_mmsql import conexion_mssql


def insert(meData):
    valores = []
    valores.append(meData.codigo_tipooperacion)
    valores.append(meData.tipo_operacion)
    valores.append(meData.cod_usuario)
    sql = "insert into tipo_operacion(codigo_tipooperacion,tipo_operacion,sw_estado,cod_usuario,fecre) values(%s,%s,'1',%s,getdate())"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta


def update(meData):
    valores = []
    valores.append(meData.codigo_tipooperacion)
    valores.append(meData.tipo_operacion)
    valores.append(meData.id_tpooperacion)
    sql = "update tipo_operacion set codigo_tipooperacion=%s, tipo_operacion=%s where id_tpoperacion=%s"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta


def delete(meData):
    valores = []
    valores.append(meData.id_tpooperacion)
    sql = "update tipo_operacion set sw_estado='9' "
    sql += "where id_tpoperacion=%s "

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta