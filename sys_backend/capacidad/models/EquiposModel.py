from conexiones.conexion_pSql import Base
from sqlalchemy import Column, Integer, String, Date


class EquipoModel(Base):
    __tablename__ = 'equipo'
    id_equipo = Column(Integer, primary_key=True)
    cod_equipo = Column(String, unique=True)
    cod_ccosto = Column(String)
    id_tpequipo = Column(Integer)
    equipo = Column(String)
    cant_turnos = Column(Integer)
    cant_personas = Column(Integer)    
    sw_estado = Column(String)
    cod_usuario = Column(String)
    fecre = Column(Date)