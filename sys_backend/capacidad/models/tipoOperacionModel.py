from conexiones.conexion_pSql import Base
from sqlalchemy import Column, Integer, String, Date


class TipoOperacionModel(Base):
    __tablename__ = 'tipo_operacion'
    id_tpooperacion = Column(Integer, primary_key=True)
    codigo_tipooperacion = Column(String, unique=True)
    tipo_operacion = Column(String)         
    sw_estado = Column(String)
    cod_usuario = Column(String)
    fecre = Column(Date)