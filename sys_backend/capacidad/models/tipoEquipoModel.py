from conexiones.conexion_pSql import Base
from sqlalchemy import Column, Integer, String


class TipoEquipoModel(Base):
    __tablename__ = 'tipo_equipo'
    id_tpequipo = Column(Integer, primary_key=True)
    tipo_equipo = Column(String, unique=True)
    sw_estado = Column(String)
    cod_usuario = Column(String)
