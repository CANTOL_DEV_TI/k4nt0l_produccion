from conexiones.conexion_pSql import Base
from sqlalchemy import Column, Integer, String, DECIMAL


class PeriodoLaboralModel(Base):
    __tablename__ = 'periodo_laboral'
    id_per_lab = Column(Integer, primary_key=True)
    ejercicio = Column(Integer)
    periodo = Column(Integer)
    cod_ccosto = Column(String)
    cant_dia_lab = Column(DECIMAL)
    cant_personas = Column(DECIMAL)
    cant_hora_lab = Column(DECIMAL)
    sw_estado = Column(String)
    cod_usuario = Column(String)
