from typing import Optional
from pydantic import BaseModel


class EquipoSchema(BaseModel):
    id_equipo: Optional[int]
    cod_equipo: Optional[str]
    cod_ccosto:Optional[str]
    id_tpequipo:Optional[int]
    equipo: Optional[str]
    cant_turnos:Optional[int]
    cant_personas:Optional[int]    
    sw_estado: Optional[str]
    cod_usuario: Optional[str]