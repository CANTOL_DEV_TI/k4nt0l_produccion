from typing import Optional
from pydantic import BaseModel
from datetime import datetime


class tipoEquipo_Schema(BaseModel):
    id_tpequipo: Optional[int]
    tipo_equipo: Optional[str]
    cod_usuario: Optional[str]
    fecre: Optional[datetime]
    
    class Config:
        orm_mode = True