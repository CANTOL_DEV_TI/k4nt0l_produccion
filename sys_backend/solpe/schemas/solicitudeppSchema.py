from typing import List,Optional 
from pydantic import BaseModel 
from decimal import Decimal 
 
class solicitudeppdet_Schema(BaseModel): 
    Line : Optional[int] 
    ItemCode : Optional[str] 
    Quantity : Optional[Decimal] 
    UoMEntry : Optional[int] 
    UseBaseUnits : Optional[int] 
    WarehouseCode : Optional[str] 
    AccountCode : Optional[str] 
    CostingCode : Optional[str] 
    CostingCode2 : Optional[str]     
    U_MSSC_COND : Optional[str] 
 
class solicitudepp_Schema(BaseModel): 
    Series : Optional[str] 
    Comments : Optional[str] 
    JournalMemo : Optional[str] 
    U_MSSL_TOP : Optional[str] 
    U_MSSC_TICA : Optional[str] 
    U_MSSC_NUSO : Optional[str] 
    U_MSSC_NOSO : Optional[str] 
    U_MSSC_RESP : Optional[str] 
    DocumentLines : Optional[List[solicitudeppdet_Schema]]  
 
    class Config: 
        orm_mode = True