from typing import Optional 
from pydantic import BaseModel 
 
class busquedasolpe_Schema(BaseModel): 
    DocNum : Optional[str] 
    DocDate : Optional[str]         
    U_MSSC_NUSO : Optional[str] 
    U_MSSC_NOSO : Optional[str] 
    U_MSSC_RESP : Optional[str]    