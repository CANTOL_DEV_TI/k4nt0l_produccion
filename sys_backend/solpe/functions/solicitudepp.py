from solpe.schemas.solicitudeppSchema import solicitudepp_Schema  
from solpe.schemas.solpebusquedaSchema import busquedasolpe_Schema  
from solpe.functions.loginSLDFunction import logintoken  
from conexiones.conexion_sap_solo import conexion_SAP_Tecno 
from os import getenv  
import requests  
import json  
import urllib3  
from datetime import date  

BASESAP = "SBO_TECNO_PRODUCCION" #"BK_TECNO_20241120"
  
def GrabarSolpe(pSolicitud : solicitudepp_Schema):  
      
    try:          
        urllib3.disable_warnings()  
  
        token = logintoken(BASESAP)  
  
        session = token['SessionId']  
  
        Tramafinal = {'Series' : "", 'Comments' : "" , 'JournalMemo' : "" , 'U_MSSL_TOP' : "", 'U_MSSC_TICA' : "" ,   
                      'U_MSSC_NUSO' : "", 'U_MSSC_NOSO' : "" , 'U_MSSC_RESP' : "" , 'DocumentLines' : [] }  
  
        Tramafinal["Series"] = pSolicitud.Series  
        Tramafinal["Comments"] = pSolicitud.Comments  
        Tramafinal["JournalMemo"] = pSolicitud.JournalMemo  
        Tramafinal["U_MSSL_TOP"] = pSolicitud.U_MSSL_TOP  
        Tramafinal["U_MSSC_TICA"] = pSolicitud.U_MSSC_TICA  
        Tramafinal["U_MSSC_NUSO"] = pSolicitud.U_MSSC_NUSO  
        Tramafinal["U_MSSC_NOSO"] = pSolicitud.U_MSSC_NOSO  
        Tramafinal["U_MSSC_RESP"] = pSolicitud.U_MSSC_RESP  
  
        for d in pSolicitud.DocumentLines:              
            detalletramafinal = {"Line" : d.Line ,"ItemCode": d.ItemCode, "Quantity": int(d.Quantity),"UoMEntry" : d.UoMEntry,   
                                 "UseBaseUnits" : d.UseBaseUnits, "WarehouseCode": d.WarehouseCode, "AccountCode": d.AccountCode,   
                                 "CostingCode": d.CostingCode, "CostingCode2": d.CostingCode2, "U_MSSC_COND" : d.U_MSSC_COND}  
              
            Tramafinal["DocumentLines"].append(detalletramafinal)  
          
        payload = json.dumps(Tramafinal)  
          
        header = {  
                    'Content-Type': 'application/json',  
                    'Cookie': f'B1SESSION={session}; ROUTEID=.node3'  
                 }  
          
        Servidor = getenv("SERVIDOR_SLD")  
        url = f"{Servidor}/b1s/v1/InventoryGenExits"  
  
        response = requests.request("POST", url, headers=header, data=payload,verify=False)  
  
        resultado = json.loads(response.text)  
        print(resultado)
  
        return f"Se genero la salida número : {resultado['DocNum']}."  
          
    except Exception as err:  
        print(err)
        return f"Error en la operación : {err}."  
      
def ListaSolpe(pbusqueda : str):  
    try:          
        urllib3.disable_warnings()  
  
        token = logintoken(BASESAP)  
  
        session = token['SessionId']  
  
        Servidor = getenv("SERVIDOR_SLD")  
              
        url = f"{Servidor}/b1s/v1/InventoryGenExits?$select=DocEntry,DocNum,DocDate,U_MSSC_NUSO,U_MSSC_NOSO,U_MSSC_RESP,Comments&$filter=U_MSSC_TICA eq '17' and DocDate eq '{pbusqueda}' &$orderby=DocNum&$top=100"  
          
        header = {  
                    'Content-Type': 'application/json',  
                    'Cookie': f'B1SESSION={session}; ROUTEID=.node3'  
                 }  
                  
        response = requests.request("GET", url, headers=header , verify=False)  
  
        resultado = json.loads(response.text)  
          
        lista = []  
                  
        for d in resultado['value']:  
             lista.append(d)  
  
        return lista  
  
    except Exception as err:  
        return f"Error en la consulta : {err}."  
  
def listaItemsEpp():  
    try:  
        urllib3.disable_warnings()  
  
        token = logintoken(BASESAP)  
  
        session = token['SessionId']  
  
        Servidor = getenv("SERVIDOR_SLD")  
  
        url = f"{Servidor}/b1s/v1/Items?$select=ItemCode,ItemName,U_MSSC_CORE,U_MSS_AREA,U_MSS_SUBAR,ItemWarehouseInfoCollection,ItemPrices,U_MSSC_FAM,U_MSSC_SUF&$filter=SalesItem eq 'N' and U_MSSC_FAM eq '14' &$top=100"  
  
        header = {  
                    'Content-Type': 'application/json',  
                    'Cookie': f'B1SESSION={session}; ROUTEID=.node3'  
                 }  
                  
        response = requests.request("GET", url, headers=header , verify=False)  
  
        resultado = json.loads(response.text)  
  
        lista = []  
        item = {}  
  
        for d in resultado['value']:  
              
            item = { "ItemCode" : d['ItemCode'], "ItemName" : d['ItemName'] , "Area" : d['U_MSS_AREA'] , "SubArea" : d['U_MSS_SUBAR'], "Stock" : 0 , "Precio" : 0 }       
  
            for dw in d['ItemWarehouseInfoCollection']:  
                if(dw['WarehouseCode'] == 'ALM03'):  
                    item ["Stock"] = dw['InStock']   
              
            for dp in d['ItemPrices']:  
                if(dp['PriceList'] == '10'):  
                    item["Precio"] = dp['Price']  
  
            lista.append(item)  
          
        return lista  
  
    except Exception as err:  
        return f"Error en la consulta : {err}." 
 
def listaItemsEpp2(): 
    try: 
        reporte = [] 
 
        sql = "SELECT I.\"ItemCode\" as ItemCode,I.\"ItemName\" as ItemName,Fam.\"Name\" AS Familia,SuF.\"Name\" AS SubFamilia, StockItem.En_Stock,Comprometido,Pedido,Disponible FROM " 
        sql += f"{BASESAP}.OITM AS I, {BASESAP}.\"@MSSC_ARFA\" AS Fam, {BASESAP}.\"@MSSC_ARSU\" AS SuF ," 
        sql += "(SELECT S.\"ItemCode\",\"OnHand\" AS En_Stock,\"IsCommited\" AS Comprometido,S.\"OnOrder\" AS Pedido, ((\"OnHand\" + \"OnOrder\") - \"IsCommited\") AS Disponible" 
        sql += f" FROM {BASESAP}.OITW S WHERE S.\"WhsCode\" = 'ALM03') StockItem WHERE Fam.\"Code\" = I.U_MSSC_FAM AND  SuF.\"Code\" = I.U_MSSC_SUF" 
        sql += " AND StockItem.\"ItemCode\" = I.\"ItemCode\" AND I.U_MSSC_FAM = '14'" 
        #print(sql) 
        conn = conexion_SAP_Tecno() 
 
        cur_lista = conn.cursor() 
 
        cur_lista.execute(sql) 
 
        for row in cur_lista: 
            fila = {"ItemCode":row['ItemCode'],"ItemName":row['ItemName'],"Familia":row['Familia'],"SubFamilia":row['SubFamilia'],"En_Stock":row['En_Stock'], 
                    "Comprometido":row['Comprometido'],"Pedido":row['Pedido'],"Disponible":row['Disponible'] } 
            reporte.append(fila)  
 
        conn.close() 
 
        return reporte 
 
    except Exception as err: 
        return f"Error en la consulta : {err}." 
     
def ListadoSuministros(): 
    try: 
        reporte = [] 
 
        sql = "SELECT I.\"ItemCode\" as ItemCode,I.\"ItemName\" as ItemName,Fam.\"Name\" AS Familia,SuF.\"Name\" AS SubFamilia, StockItem.En_Stock,Comprometido,Pedido,Disponible FROM " 
        sql += f"{BASESAP}.OITM AS I, {BASESAP}.\"@MSSC_ARFA\" AS Fam, {BASESAP}.\"@MSSC_ARSU\" AS SuF ," 
        sql += f"(SELECT S.\"ItemCode\",\"OnHand\" AS En_Stock,\"IsCommited\" AS Comprometido,S.\"OnOrder\" AS Pedido, ((\"OnHand\" + \"OnOrder\") - \"IsCommited\") AS Disponible" 
        sql += f" FROM {BASESAP}.OITW S WHERE S.\"WhsCode\" = 'ALM03') StockItem WHERE Fam.\"Code\" = I.U_MSSC_FAM AND  SuF.\"Code\" = I.U_MSSC_SUF" 
        sql += " AND StockItem.\"ItemCode\" = I.\"ItemCode\" AND I.U_MSSC_FAM = '14'" 
        #print(sql) 
        conn = conexion_SAP_Tecno() 
 
        cur_lista = conn.cursor() 
 
        cur_lista.execute(sql) 
 
        for row in cur_lista: 
            fila = {"ItemCode":row['ItemCode'],"ItemName":row['ItemName'],"Familia":row['Familia'],"SubFamilia":row['SubFamilia'],"En_Stock":row['En_Stock'], 
                    "Comprometido":row['Comprometido'],"Pedido":row['Pedido'],"Disponible":row['Disponible'] } 
            reporte.append(fila)  
 
        conn.close() 
 
        return reporte 
 
    except Exception as err: 
        return f"Error en la consulta : {err}." 
    
def ReportePlanificado():
    try:
        resultado = []
        fila = {}

        año = date.today().year
        mes = date.today().month

        sPeriodo = str(año) + "{:02d}".format(mes)

        sql = "SELECT distinct RIGHT(scc.U_NOM_CATALOGO,6) AS Periodo,scd.U_COD_CCOSTO Cod_CCosto, cc.U_MSSC_DESC as Desc_CCosto,  U_COD_ARTICULO as Cod_Articulo,"
        sql += f" \"ItemName\" as Nombre, U_CANTIDAD as Cantidad FROM {BASESAP}.\"@SGC_CAT_DET\" scd INNER JOIN {BASESAP}.OITM I ON I.\"ItemCode\"=scd.U_COD_ARTICULO "
        sql += f"INNER JOIN {BASESAP}.\"@MSSC_CECO\" cc ON cc.U_MSSC_CECO =  scd.U_COD_CCOSTO INNER JOIN (SELECT top 1 scc.\"Code\",scc.U_NOM_CATALOGO"
        sql += f" FROM {BASESAP}.\"@SGC_CAT_CAB\" scc WHERE scc.U_NOM_CATALOGO like '%{sPeriodo}%' ORDER BY scc.U_VERSION DESC  ) scc ON scc.\"Code\" = scd.\"Code\" "
        sql += "WHERE U_MSSC_ARE = 20 "
        
        #print(sql)

        conn = conexion_SAP_Tecno()

        cur_lista = conn.cursor()

        cur_lista.execute(sql)

        for f in cur_lista:
            fila = {"Periodo" : f['Periodo'], "Cod_CCosto" : f['Cod_CCosto'], "Desc_CCosto" : f['Desc_CCosto'], "Cod_Articulo" : f['Cod_Articulo'],
                    "Nombre" : f['Nombre'], "Cantidad" : f['Cantidad']}
            resultado.append(fila)

        return resultado
    
    except Exception as err:
        return f"Error en la consulta : {err}."    