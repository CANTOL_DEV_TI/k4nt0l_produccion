from os import getenv 
import requests 
import json 
import urllib3 
 
def logintoken(pCia : str):     
    urllib3.disable_warnings() 
    Servidor = getenv("SERVIDOR_SLD") 
    dbHANA = pCia 
    userHANA = getenv("SAP_USUARIO") 
    pwdHANA = getenv("SAP_PWD") 
    
    url = f"{Servidor}/b1s/v1/Login" 
    
    payload = json.dumps({ 
    "CompanyDB": dbHANA, 
    "Password": pwdHANA, 
    "UserName": userHANA 
    }) 
    print(payload)
    headers = { 
    'Content-Type': 'application/json'     
    } 
 
    response = requests.request("POST", url, headers=headers, data=payload,verify=False) 
     
    response.text 
     
    return json.loads(response.text)