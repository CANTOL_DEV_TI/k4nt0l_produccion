from fastapi import APIRouter, Depends, status, UploadFile, File, Request,Header 
from fastapi.responses import FileResponse 
from sqlalchemy.orm.session import Session 
 
from solpe.schemas.solicitudeppSchema import solicitudepp_Schema 
from solpe.schemas.solpebusquedaSchema import busquedasolpe_Schema 
from solpe.functions.solicitudepp import GrabarSolpe,ListaSolpe,listaItemsEpp2,ReportePlanificado
 
ruta = APIRouter( 
    prefix="/solpe",tags=["Solpe"] 
) 
 
@ruta.get("/Lista", status_code=status.HTTP_200_OK) 
async def Listado(pfecha : str):     
    return ListaSolpe(pfecha) 
 
@ruta.post("/", status_code=status.HTTP_200_OK) 
async def Guardar(info : solicitudepp_Schema):     
    return GrabarSolpe(info) 
 
@ruta.get("/ListadoEPP",status_code=status.HTTP_200_OK) 
async def ListaEPP(): 
    return listaItemsEpp2()

@ruta.get("/Planificado", status_code=status.HTTP_200_OK)
async def ListaPlanificado():
    return ReportePlanificado()