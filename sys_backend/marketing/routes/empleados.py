from fastapi import APIRouter, status
from typing import List
from typing import Union

from gdh.schemas.empleadosSchema import empleado_Schema
from gdh.functions import empleadosFunction

ruta = APIRouter(prefix="/empleados", tags=["Empleados"])


@ruta.post("/", status_code=status.HTTP_200_OK)
async def Guardar(info: list[empleado_Schema]):
    return empleadosFunction.GrabarEmpleados(info)


@ruta.get("/subarea", status_code=status.HTTP_200_OK)
def articuloFamilia_all(cod_subarea: Union[str, None] = "0"):
    data = empleadosFunction.empleado_subarea(cod_subarea)
    return data

@ruta.get("/listadoADRYANSAP",status_code=status.HTTP_200_OK)
def listadoAS(pSap:Union [str,None],pAdryan: Union[str,None]):
    data = empleadosFunction.CompararEmpleados(pAdryan,pSap)
    return data

@ruta.get("/subareaA", status_code=status.HTTP_200_OK)
def empleadosAdryan(pTodas: Union[str, None]= "", pCC : Union[str, None] = ""):
    data =empleadosFunction.empleado_subarea_adryan(pTodas,pCC)
    return data