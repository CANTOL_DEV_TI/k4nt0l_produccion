from fastapi import APIRouter
from marketing.routes.Productooriginal import cliente, familia, login, producto, usuario


# Llamar a la función connection() para obtener la conexión
#connection = conn()


# Inicializar la aplicación FastAPI
# app = FastAPI()

ruta = APIRouter(
    prefix='/productooriginal',
)
ruta.include_router(cliente.ruta)
ruta.include_router(familia.ruta)
ruta.include_router(login.ruta)
ruta.include_router(producto.ruta)
ruta.include_router(usuario.ruta)
