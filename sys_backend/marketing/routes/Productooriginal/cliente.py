from fastapi import APIRouter, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from conexiones.conexion_sql_solo import ConectaSQL_Produccion


ruta = APIRouter(
    prefix='/cliente',
    tags=["productooriginal"]
)

def get_connection():
    return ConectaSQL_Produccion()





@ruta.post("/registrarcliente")
def registrarcliente(cliente:dict):
    connection = get_connection()
    try:
        cursor = connection.cursor()
        client = cliente['cliente']
        identificador = cliente['identificador']
        email = cliente['email']
        lugarcompra = cliente['lugarcompra']
        ciudadcompra = cliente['ciudadcompra']
        numeroserie = cliente['numeroserie']

        sqlconsulta = f"select * from poo_producto where numeroserie='{numeroserie}' and estado = 0;"
        print("pintame consulta sql:",sqlconsulta)
        cursor.execute(sqlconsulta)
        rs = cursor.fetchone()
        msg = ""
        
        if rs:
            sqlupdate = f"update poo_producto set estado = 1, cantvalidacion = 1 where numeroserie = '{numeroserie}';"
            cursor.execute(sqlupdate)
            connection.commit()

            sqlinsertcliente = f"insert into poo_cliente (cliente,identificador,email,lugarcompra,ciudadcompra,numeroserie,idproducto, estado) values ('{client}', '{identificador}', '{email}','{lugarcompra}','{ciudadcompra}', '{numeroserie}', {rs[0]}, 1);"
            cursor.execute(sqlinsertcliente)
            connection.commit()
            msg = "el producto fue validado correctamente"
        else:
            sqlconsulta = f"select * from poo_producto where numeroserie='{numeroserie}';"
            cursor.execute(sqlconsulta)
            rsc = cursor.fetchone()

            cantvalidacion = rsc[7] + 1

            sqlupdate = f"update poo_producto set cantvalidacion = {cantvalidacion} where numeroserie = '{numeroserie}';"
            cursor.execute(sqlupdate)
            connection.commit()


            sqlinsertcliente = f"insert into poo_cliente (cliente,identificador,email,lugarcompra,ciudadcompra,numeroserie,idproducto, estado) values ('{client}', '{identificador}', '{email}','{lugarcompra}','{ciudadcompra}', '{numeroserie}', {rsc[0]}, 0);"
            cursor.execute(sqlinsertcliente)
            connection.commit()
            msg = "el registro ya existe no se realizó nada:"
        
        return msg
    
    except KeyError as e:
        return f"Error: Se espera el campo{str(e)}"
    except Exception as e:
        return f"Error:{str(e)}"
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

@ruta.get("/listarcliente")
def listarcliente(page_num: int, page_size: int, numeroserie:str):
    start = (page_num - 1) * page_size
    end = 50
    connection = get_connection()
    try:
        with connection.cursor() as cursor:
            sql = f"""select * from poo_cliente where numeroserie = '{numeroserie}' order by idcliente desc
                      OFFSET {start} ROWS
					  FETCH NEXT {end} ROWS ONLY;"""
            print("pintame consulta sql detalle:",sql)
            cursor.execute(sql)
            rs = cursor.fetchall()
            lista = []
            

            count_sql = f"select count(*) as total from poo_cliente where numeroserie = '{numeroserie}'"
            cursor.execute(count_sql)
            total_count = cursor.fetchone()
            print("total_count:", total_count[0])

            for row in rs:
                fecha_str = row[8]
                fecha_formateada = fecha_str.strftime("%Y-%m-%d %H:%M:%S")
                producto = {
                    "idcliente": row[0],
                    "nombre": row[1],
                    "identificador": row[2],
                    "email": row[3],
                    "lugar": row[4],
                    "ciudad": row[5],
                    "numeroserie": row[6],
                    "idproducto": row[7],
                    "fecha":fecha_formateada,
                    "estado": row[9]
                }
                lista.append(producto)
            response = {
                "data": lista,
                "total": total_count[0],
                "page" : page_num,
                "limit":page_size
            }
            return response

    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error el datos del cliente")
    finally:
        # Cerrar la conexión a la base de datos
        if cursor:
            cursor.close()
        if connection:
            connection.close()

@ruta.post("/buscardetallecliente")
def buscarcliente(cliente:dict):
    connection = get_connection()
    try:
        print("muestrame cliente:",cliente)
        cursor = connection.cursor()
        datos = cliente['data']

        sql = f"""select * from poo_cliente where numeroserie = '{cliente['numserie']}'
                        and identificador like '%{datos}%'order by idcliente desc
                        OFFSET 0 ROWS
                        FETCH NEXT 50 ROWS ONLY;"""
        print("data sql:",sql)
        cursor.execute(sql)
        rs = cursor.fetchall()
        lista = []

        for row in rs:
            fecha_str = row[8]
            fecha_formateada = fecha_str.strftime("%Y-%m-%d %H:%M:%S")
            producto = {
                    "idcliente": row[0],
                    "nombre": row[1],
                    "identificador": row[2],
                    "email": row[3],
                    "lugar": row[4],
                    "ciudad": row[5],
                    "numeroserie": row[6],
                    "idproducto": row[7],
                    "fecha":fecha_formateada,
                    "estado": row[9]
            }
            lista.append(producto)
        return lista
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los productos")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()            

