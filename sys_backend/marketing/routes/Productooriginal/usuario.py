from fastapi import APIRouter, HTTPException
from fastapi.middleware.cors import CORSMiddleware
import json
import bcrypt
from conexiones.conexion_sql_solo import ConectaSQL_Produccion

ruta = APIRouter(
    prefix='/usuario',
    tags=["productooriginal"]
)

def get_connection():
    return ConectaSQL_Produccion()




@ruta.get("/listarusuario")
def listarusuario():
    connection = get_connection()
    try:
        with connection.cursor() as cursor:
            sql = "select * from usuario;"
            cursor.execute(sql)
            rs = cursor.fetchall()
            lista = []

            for row in rs:
                perfil = json.loads(row[4])
                perfilname = perfil[0]['nombre']
                usuario = {
                    "idusuario":row[0],
                    "dni":row[1],
                    "nombre":row[2],
                    "idperfil":perfilname,
                    "perfil": row[4],
                    "email":row[5]
                }
                lista.append(usuario)
            return lista
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener las categorias")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

@ruta.post("/crearusuario")
def crearusuario(usuarios:dict):
    connection = get_connection()
    cursor = None
    try:
        dni = usuarios['dni']
        nombres = usuarios['nombre'] + " " +usuarios['apellido']
        correo = usuarios['correo']
        perfil = usuarios['selectedPerfil']
        print("pintame perfil:",perfil)
        cursor = connection.cursor()
        claveEncriptada = encriptar_contraseña(dni)
        clave_bytes_encriptada = str(claveEncriptada)[2:-1]
        msj = ""
        
        sqlconsultausuario = f"select * from usuario where dni = {dni}"
        cursor.execute(sqlconsultausuario)
        rs = cursor.fetchone()

        if rs:
            msj = "usuario ya existe"
            return  1 , msj
        else:
            sqlinsertarregistro = f"insert into usuario (dni,nombre,clave,idperfil,email) values ('{dni}','{nombres}','{clave_bytes_encriptada}','{perfil}', '{correo}')"
            cursor.execute(sqlinsertarregistro)
            connection.commit()
            msj = "Usuario creado exitosamente"
            return 2, msj

    except Exception as e:
        print("pintame e:",e)
        raise e
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

def encriptar_contraseña(clave):
    hashed_clave = bcrypt.hashpw(clave.encode('utf-8'), bcrypt.gensalt())
    return hashed_clave


@ruta.post("/actualizarusuario")
def actualizarusuario(usuarios:dict):
    connection = get_connection()
    cursor = None
    try:
        dni = usuarios['dni']
        nombre = usuarios['nombre']
        correo = usuarios['correo']
        perfil = usuarios['selectedPerfil']

        cursor = connection.cursor()
        sqlupdate = f"update usuario set nombre = '{nombre}', email = '{correo}', idperfil = '{perfil}' where dni = '{dni}'"
        print("pintame consulta:",sqlupdate)
        cursor.execute(sqlupdate)
        connection.commit()
        msj = "usuario registrado correctamente"

        return msj
    except Exception as e:
        print("pintame e:",e)
        raise e
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()


@ruta.post("/buscarusuario")
def buscarusuario(usuarios:dict):
    connection = get_connection()
    try:
        cursor = connection.cursor()
        datos = usuarios['datos']

        sql = f"select * from usuario where(upper(dni) like upper('%{datos}%') or upper(nombre) like upper('%{datos}%')) order by id asc;"
        cursor.execute(sql)
        rs = cursor.fetchall()

        lista = []

        for row in rs:
            perfil = json.loads(row[4])
            perfilname = perfil[0]['nombre']
            usuario = {
                "idusuario":row[0],
                "dni":row[1],
                "nombre":row[2],
                "idperfil":perfilname,
                "perfil": row[4],
                "email":row[5]
            }
            lista.append(usuario)
        return lista
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener las categorias")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()
            






