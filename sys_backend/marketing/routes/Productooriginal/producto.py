from fastapi import FastAPI, HTTPException,Request, Query
from fastapi.middleware.cors import CORSMiddleware
import qrcode
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
import qrcode.constants
import qrcode
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from io import BytesIO
from reportlab.lib.utils import ImageReader
from fastapi.responses import StreamingResponse
from fastapi import APIRouter, HTTPException
import io
from datetime import datetime
import time
from zipfile import ZipFile
import zipfile
import gc
from conexiones.conexion_sql_solo import ConectaSQL_Produccion

ruta = APIRouter(
    prefix='/productos',
    tags=["productooriginal"]
)

def get_connection():
    return ConectaSQL_Produccion()


@ruta.get("/listardetalle")
def listardetalleproducto(page_num: int, page_size: int, codigoregistro:str):
    start = (page_num - 1) * page_size
    end = 50
    connection = get_connection()
    try:
        with connection.cursor() as cursor:
            sql = f"""select p.idproducto, p.numeroserie, p.nombre as producto, f.familia as familia , f.articulo as articulo ,p.fecharegistro, p.estado, p.codigoregistro  from poo_producto p 
                        inner join poo_familia f 
                        on p.uuid_familia  = f.uuid_familia
                        where p.codigoregistro = '{codigoregistro}'
                        order by  p.idproducto desc
                        OFFSET {start} ROWS
						FETCH NEXT {end} ROWS ONLY; """
            cursor.execute(sql)
            rs = cursor.fetchall()
            lista = []
            print("pintame consulta sql detalle:",sql)

            count_sql = f"select count(*) as total from poo_producto where codigoregistro = '{codigoregistro}'"
            cursor.execute(count_sql)
            total_count = cursor.fetchone()
            print("total_count:", total_count[0])

            for row in rs:
                fecha_str = row[5]
                fecha_formateada = fecha_str.strftime("%Y-%m-%d %H:%M:%S")
                producto = {
                    "idproducto": row[0],
                    "numeroserie": row[1],
                    "producto": row[2],
                    "familia": row[3],
                    "articulo": row[4],
                    "fecharegistro": fecha_formateada,
                    "estado": row[6]
                }
                lista.append(producto)
            response = {
                "data": lista,
                "total": total_count[0],
                "page" : page_num,
                "limit":page_size
            }
            return response

    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los productos")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

@ruta.get("/listarproductototal")
def listarconsolidado(page_num: int = Query(1, gt=0), page_size: int = Query(50, gt=0, le=100)):
    start = (page_num - 1) * page_size
    end = 50
    connection = get_connection()
    try:
        with connection.cursor() as cursor:
            sql = f"select * from poo_registroproducto order by id desc OFFSET {start} ROWS FETCH NEXT {end} ROWS ONLY;"
            cursor.execute(sql)
            rs = cursor.fetchall()
            lista = []

            count_sql = """SELECT COUNT(id) as total FROM poo_registroproducto;"""
            cursor.execute(count_sql)
            total_count = cursor.fetchone()
            print("total_count:", total_count[0])

            for row in rs:
                fecha_str = row[5]
                fecha_formateada = fecha_str.strftime("%Y-%m-%d %H:%M:%S")

                producto = {
                    "codigoregistro": row[1],
                    "cantidad": row[2],
                    "producto": row[3],
                    "categoria": row[4],
                    "fecharegistro": fecha_formateada
                }
                lista.append(producto)
            response = {
                "data": lista,
                "total": total_count[0],
                "page" : page_num,
                "limit":page_size
            }
            return response
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los productos")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()


@ruta.get("/listarproducto")
def listarproductosvalidados(page_num: int = Query(1, gt=0), page_size:int = Query(50, gt=0, le=100)):
    start = (page_num - 1) * page_size
    end = 50
    connection = get_connection()
    try:
        with connection.cursor() as cursor:
            sql = f"""select p.idproducto , p.numeroserie, p.nombre as producto, f.familia as familia, f.articulo,  p.fecharegistro, p.estado  from poo_producto p 
                        inner join poo_familia f on p.uuid_familia = f.uuid_familia  where p.estado = 1 order by p.idproducto asc
                        OFFSET {start} ROWS
                        FETCH NEXT {end} ROWS ONLY;"""
            cursor.execute(sql)
            rs = cursor.fetchall()
            lista = []

            count_sql = """select count(idproducto) as total from poo_producto where estado = 1"""
            cursor.execute(count_sql)
            total_count = cursor.fetchone()
            print("total_count:",total_count[0])


            for row in rs:
                fecha_str = row[5]
                fecha_formateada = fecha_str.strftime("%Y-%m-%d %H:%M:%S")
                producto = {
                    "idproducto": row[0],
                    "numeroserie": row[1],
                    "producto": row[2],
                    "categoria": row[3],
                    "articulo": row[4],
                    "fecharegistro": fecha_formateada,
                    "estado": row[6]
                }
                lista.append(producto)
            response = {
                "data": lista,
                "total": total_count[0],
                "page": page_num,
                "limit": page_size
            }
            return response
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los productos")
    finally:
        # Cerrar la conexión a la base de datos
        if cursor:
            cursor.close()
        if connection:
            connection.close()





@ruta.post("/registrarproducto")
async def registrarproducto(request:Request):
    connection = get_connection()
    try:
        form = await request.form()
        codigoregistro = form.get('codigoregistro')
        familianombre = form.get('familianombre')
        uuid_familia = form.get('producto')
        productonombre = form.get('productonombre')
        codquivalente = form.get('codquivalente')
        cantidadproducto = form.get('cantidad')
        codequivalentereal = form.get('codequivalentereal')
        lista = []
        fileName = productonombre +"-"+cantidadproducto + ".pdf"
        
        print("pintame fileName:", fileName)
        
        print("pintame codquivalente:",codquivalente)
        print("pintame codquivalente real:",codequivalentereal)

        base_url = "http://productooriginal.cantol.com.pe/controlproductoscliente?phwgc="
        codquivalentenew = ''

        if len(codquivalente) == 2:
            codquivalentenew = codquivalente + "--"
            print("show me:",codquivalentenew)
        elif len(codquivalente) == 3:
            codquivalentenew = codquivalente + "-"
            print("show me:",codquivalentenew)
        else:
            codquivalentenew = codquivalente[:4]
            print("show me:",codquivalentenew)


        cursor = connection.cursor()

        for _ in range(int(cantidadproducto)):
            current_timestamp = int(time.time()*1000)
            serial_number = f"{codquivalentenew}-{current_timestamp}"
            lista.append(serial_number)
            time.sleep(0.001)
            print("muestrame lista de series:",serial_number)
            print("lista total:",len(lista))
        
        print("muestrame cantidad de lista:",len(lista))
       #Generar pdf
        data_list = [f"{base_url}{code}" for code in lista]
        pdf_buffer = io.BytesIO()
        crear_pdf_qr_codes(data_list, pdf_buffer, codequivalentereal, familianombre)
        pdf_buffer.seek(0)

        # Crear un buffer para el archivo ZIP
        zip_buffer = io.BytesIO()
        with zipfile.ZipFile(zip_buffer, 'w', zipfile.ZIP_DEFLATED) as zip_file:
            pdf_content = pdf_buffer.getvalue()
            if not pdf_content:
                return "Error: PDF no generado correctamente."
            zip_file.writestr(fileName, pdf_content)

        # Guardar el archivo ZIP localmente para pruebas
        '''with open('codigos_qr.zip', 'wb') as f:
            f.write(zip_buffer.getvalue())'''
        
        

        # Depuración: Imprimir el contenido del buffer ZIP
        zip_content = zip_buffer.getvalue()
        print(f"Tamaño del contenido del ZIP: {len(zip_content)} bytes")


        # Insertar registros en la base de datos en una sola consulta
        sqlinsertregistro = f"insert into poo_registroproducto (codigoregistro, cantidad, producto, familia, fecharegistro) values ('{codigoregistro}', {cantidadproducto}, '{productonombre}' , '{familianombre}', default);"
        cursor.execute(sqlinsertregistro)
        connection.commit()

        for row in lista:
            sqlinsert = f"insert into poo_producto(uuid_familia, numeroserie, nombre,  estado, fecharegistro, codigoregistro, cantvalidacion) values ({uuid_familia},'{row}','{productonombre}', 0, default, '{codigoregistro}', default);"
            print("íntame consulta sql:",sqlinsert)
            cursor.execute(sqlinsert)
            connection.commit()
        
        zip_buffer.seek(0)
        return StreamingResponse(zip_buffer, media_type='application/zip',
                                 headers={"Content-Disposition":"attachment;filename=codigos_qr.zip"})
    except KeyError as e:
        return f"Error: Se espera el campo {str(e)}"
    except Exception as e:
        return f"Error:{str(e)}"
    finally:
        # Liberar memoria de la lista
        del lista
        del data_list
        
        # Cerrar la conexión a la base de datos
        if cursor:
            cursor.close()
        if connection:
            connection.close()

        # Limpiar buffers y conexiones
        if pdf_buffer and not pdf_buffer.closed:
            pdf_buffer.close()
        
        gc.collect()


        



@ruta.post("/generarpdf")
async def generate_pdf_qr_codes(codigoregistro:str):
    connection = get_connection()
    try:

        cursor = connection.cursor()
        sqlconsulta = f"select numeroserie, uuid_familia from poo_producto where codigoregistro = '{codigoregistro}'"
        cursor.execute(sqlconsulta) 
        rs = cursor.fetchall()
        print("pintame rs:",rs)
        uuidfamilia = rs[0][1]
        lista = []

        #obtenemos el codigo equivalente

        sqlfamilia = f"select codequivalentereal, familia from poo_familia where uuid_familia = {uuidfamilia}"
        print("sql familia:",sqlfamilia)
        cursor.execute(sqlfamilia)
        rsf = cursor.fetchone()
        codequivalente = rsf[0]
        familia = rsf[1]


        #consultar cantidad y  nombre de producto
        sqlconsultaprod = f"select cantidad, producto, fecharegistro from poo_registroproducto where codigoregistro  = '{codigoregistro}'"
        print("muestrame consulta sqlconsultaprod:",sqlconsultaprod)
        cursor.execute(sqlconsultaprod) 
        rsp = cursor.fetchone()
        cantidadprod = str(rsp[0])
        nomproduct = rsp[1]
        fileName = nomproduct + "-" +cantidadprod + ".pdf"
       
        
        for row in rs:
            lista.append(row[0])


        base_url = "http://productooriginal.cantol.com.pe/controlproductoscliente?phwgc="
        data_lista = [f"{base_url}{code}" for code in lista]
        pdf_buffer = io.BytesIO()
        crear_pdf_qr_codes(data_lista, pdf_buffer, codequivalente, familia)
        pdf_buffer.seek(0)

        
        #Crear un archivo ZIP en memoria
        zip_buffer = io.BytesIO()
        with zipfile.ZipFile(zip_buffer, 'w', zipfile.ZIP_DEFLATED) as zip_file:
            zip_file.writestr(fileName, pdf_buffer.getvalue())
        
        zip_buffer.seek(0)

        return StreamingResponse(zip_buffer, media_type='application/zip', headers={"Content-Disposition": "attachment;filename=codigos_qr.zip"})
    
    except Exception as e:
        return f"Error:{str(e)}"
    finally:
        # Eliminar lista
        del lista
        del data_lista

        # Cerrar la conexión a la base de datos
        if cursor:
            cursor.close()
        if connection:
            connection.close()
        
        # Limpiar buffers y conexiones
        if pdf_buffer and not pdf_buffer.closed:
            pdf_buffer.close()

        #import gc
        gc.collect()


#Crear el PDF y agregar los codigos QR

def crear_pdf_qr_codes(data_list, pdf_path, codquivalente, familianombre):
    print("entras a generar pdf:")
    c = canvas.Canvas(pdf_path, pagesize = letter)
    width, height = letter
    qr_size = 1.5 * inch #Tamaño de cada codigo QR
    margin = 0.5 * inch #Margen de la pagina

    x = margin
    y = height - margin - qr_size

    for data in data_list:
        qr_img = generate_qr_code(data)
        buffered = BytesIO()
        qr_img.save(buffered, format="PNG")
        buffered.seek(0)
        img_reader = ImageReader(buffered)

        c.drawImage(img_reader, x, y, qr_size, qr_size)

        x += qr_size + margin / 2
        if x + qr_size + margin / 2> width:
            x = margin
            y -= qr_size + margin / 2
            if y < margin:
                # Agregar el pie de página con el codquivalente                
                c.setFont("Helvetica", 10)
                c.drawString(margin, 0.75 * inch, f"MODELO DE PRODUCTO: {codquivalente} - FAMILIA: {familianombre}")
                c.showPage()
                y = height - margin - qr_size

    # Agregar el pie de página en la última página, sin llamar a showPage()
    if x!=margin or y != height - margin -qr_size: #verificar si se agregó contenido
        c.setFont("Helvetica", 10)
        c.drawString(margin, 0.75 * inch, f"MODELO DE PRODUCTO: {codquivalente} - FAMILIA: {familianombre}")

    #Finalizar el documento
    c.save()



    


def generate_qr_code(data):
    print("entras a generar codigo qr:",data)
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
    )
    qr.add_data(data)
    qr.make(fit=True)
    img = qr.make_image(fill='black', back_color='white')
    return img



@ruta.post("/buscarproducto")
def buscarproducto(producto:dict):
    connection = get_connection()
    try:
        cursor = connection.cursor()
        datos = producto['datos']

        sql = f"select * from poo_registroproducto where(upper(codigoregistro) like upper('%{datos}%') or upper(producto) like upper('%{datos}%')) order by id asc OFFSET 0 ROWS FETCH NEXT 50 ROWS ONLY;"
        cursor.execute(sql)
        rs = cursor.fetchall()

        lista = []

        for row in rs:
            fecha_str = row[5]
            fecha_formateada = fecha_str.strftime("%Y-%m-%d %H:%M:%S")
            productodict = {
                "codigoregistro": row[1],
                "cantidad": row[2],
                "producto": row[3],
                "categoria": row[4],
                "fecharegistro": fecha_formateada
            }
            lista.append(productodict)
        return lista
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los productos")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

@ruta.post("/buscarproductovalidado")
def buscarproductovalidado(producto:dict):
    connection = get_connection()
    try:
        print("pintame los producto:",producto)
        cursor = connection.cursor()
        datos = producto['datos']

        print("pintame los datos:",datos)
        
        sql = """select p.idproducto , p.numeroserie, p.nombre as producto, f.familia  as categoria ,
                    f.articulo, p.fecharegistro, p.estado  from poo_producto p inner join poo_familia f on p.uuid_familia  = f.uuid_familia
                    where p.estado = 1 and (upper(numeroserie) like upper('%{}%') or upper(numeroserie) like upper('%{}%'))  order by idproducto asc OFFSET 0 ROWS
					FETCH NEXT 50 ROWS ONLY;""".format(datos, datos)

        print("pintame consulta sql:",sql)
        cursor.execute(sql)
        rs = cursor.fetchall()

        print("pintame consulta rs:",rs)

        lista = []

        for row in rs:
            fecha_str = row[5]
            fecha_formateada = fecha_str.strftime("%Y-%m-%d %H:%M:%S")
            productodict = {
                    "idproducto": row[0],
                    "numeroserie": row[1],
                    "producto": row[2],
                    "categoria": row[3],
                    "articulo": row[4],
                    "fecharegistro": fecha_formateada,
                    "estado": row[6]
                }
            lista.append(productodict)
        return lista
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los productos")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()


@ruta.get("/obtenercodigo")
def obtenerCodigo():
    connection = get_connection()
    try:
        with connection.cursor() as cursor:
            sql = f'SELECT MAX(codigoregistro) FROM poo_registroproducto;'
            cursor.execute(sql)
            rs = cursor.fetchone()
            last_code = rs[0]
        if last_code:
            last_number = int(last_code[4:])
            new_code = f'CODR{last_number + 1:03}'
        else:
            new_code = 'CODR001'
        
        return {"codigoregistro": new_code}
    except Exception as e:
        raise HTTPException(status_code=500, detail="Error al mostrar registro")
    finally:
        # Cerrar la conexión a la base de datos
        if cursor:
            cursor.close()
        if connection:
            connection.close()    



@ruta.post("/actualizarproducto")
async def actualizarfamilia(request:Request):
    connection = get_connection()
    try:
        cursor = connection.cursor()
        form = await request.form()

        idcategoria = form.get('categoria')
        nombrecategoria = form.get('nombre')
        tipocategoria = form.get('tipo')
        descripcion = form.get('descripcion')

        sql = f"update poo_familia set nombre = '{nombrecategoria}', tipo = '{tipocategoria}', descripcion = '{descripcion}' where idcategoria = {idcategoria}"
        cursor.execute(sql)
        connection.commit()

        return "categoria actualizada correctamente"
    except KeyError as e:
        return f"Error: Se esperaba el campo {str(e)}"
    except Exception as e:
        return f"Error: {str(e)}"
    finally:
        # Cerrar la conexión a la base de datos
        if cursor:
            cursor.close()
        if connection:
            connection.close()

@ruta.post("/registrarcategoria")
async def registrarcategoria(request:Request):
    connection = get_connection()
    try:
        form = await request.form()
        
        nombre = form.get('nombre')
        tipo = form.get('tipo')
        descripcion = form.get('descripcion')

        cursor = connection.cursor()
        sqlinsert = f"insert into poo_familia (nombre, tipo, descripcion) values ('{nombre}','{tipo}', '{descripcion}')"
        cursor.execute(sqlinsert)
        connection.commit()

        return "categoria registrada correctamente"
    except KeyError as e:
        return f"Error: Se espera el campo {str(e)}"
    except Exception as e:
        return f"Error:{str(e)}"
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

@ruta.post("/buscardetalleproducto")
def buscarproducto(producto:dict):
    connection = get_connection()
    try:
        cursor = connection.cursor()
        datos = producto['datos']

        sql = f"""select p.idproducto, p.numeroserie, p.nombre as producto, f.familia as familia , f.articulo as articulo ,p.fecharegistro, p.estado, p.codigoregistro  from poo_producto p 
                        inner join poo_familia f 
                        on p.uuid_familia  = f.uuid_familia
                        where(upper(p.numeroserie) like upper('%{datos}%'))
                        order by  p.idproducto desc
                        OFFSET 0 ROWS
						FETCH NEXT 50 ROWS ONLY;"""
        print("data sql:",sql)
        cursor.execute(sql)
        rs = cursor.fetchall()
        lista = []

        for row in rs:
            fecha_str = row[5]
            fecha_formateada = fecha_str.strftime("%Y-%m-%d %H:%M:%S")
            producto = {
                    "idproducto": row[0],
                    "numeroserie": row[1],
                    "producto": row[2],
                    "familia": row[3],
                    "articulo": row[4],
                    "fecharegistro": fecha_formateada,
                    "estado": row[6]
            }
            lista.append(producto)
        return lista
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener los productos")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()


   