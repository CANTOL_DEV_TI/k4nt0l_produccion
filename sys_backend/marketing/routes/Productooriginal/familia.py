from fastapi import APIRouter, HTTPException,Query
from typing import List, Dict, Optional
from fastapi.middleware.cors import CORSMiddleware
from conexiones.conexion_sql_solo import ConectaSQL_Produccion

ruta = APIRouter(
    prefix='/familia',
    tags=["productooriginal"]
)

def get_connection():
    return ConectaSQL_Produccion()




@ruta.get("/obtenerfamilia")
def obtenerfamilia():
    connection = get_connection()
    try:
            
            with connection.cursor() as cursor:
                sql = "select DISTINCT idfamilia, familia from poo_familia where familia like '%PERILL%';"
                #sql = "select DISTINCT idfamilia, familia from familia order by idfamilia asc;"
                cursor.execute(sql)
                rs = cursor.fetchall()
                lista = []

                for row in rs:
                    familia = {
                        "idfamilia": row[0],
                        "familia": row[1] if row[1] is not None else '-'
                    }
                    lista.append(familia)
                return lista
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener las familias")
    finally:
        # Cerrar la conexión a la base de datos
        if cursor:
            cursor.close()
        if connection:
            connection.close()

@ruta.post("/obtenerproductofamilia")
def obtenerproductofamilia(idfamilia:str):
    connection = get_connection()
    try:
            print("pintame id familia:",idfamilia)
            with connection.cursor() as cursor:
                sql = f"select uuid_familia, idfamilia, familia,codigoproducto,nombreproducto, codigoequivalente, codequivalentereal from poo_familia where idfamilia = {idfamilia};"
                cursor.execute(sql)
                rs = cursor.fetchall()
                lista = []

                for row in rs:
                    familia = {
                        "uuid_familia": row[0],
                        "idfamilia": row[1],
                        "familia": row[2],
                        "codigoproducto": row[3],
                        "nombreproducto": row[4],
                        "codigoequivalente": row[5],
                        "codequivalentereal": row[6]
                    }
                    lista.append(familia)
                print("pintame la lista de prod:",familia)
                return lista
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener producto por familias")
    finally:
        # Cerrar la conexión a la base de datos
        if cursor:
            cursor.close()
        if connection:
            connection.close()

@ruta.get("/listarfamilia")
#def listarfamilia(page_num:int=1, page_size: int = 50):
def listarfamilia(page_num: int = Query(1, gt=0), page_size: int = Query(50, gt=0, le=100)):
    start = (page_num - 1) * page_size
    end = 50
    connection = get_connection()
    try:
        with connection.cursor() as cursor:
            #sql = "select * from familia;"
            sql = f"""
            SELECT * 
            FROM poo_familia
            ORDER BY idfamilia
            OFFSET {start} ROWS FETCH NEXT {end} ROWS ONLY;
            """
            print("muestram consulta sql:",sql)
            cursor.execute(sql)
            rs = cursor.fetchall()
            lista = []

            count_sql = """SELECT COUNT(idfamilia) as total FROM poo_familia;"""
            cursor.execute(count_sql)
            total_count = cursor.fetchone()
            print("total_count:",total_count[0])

            for row in rs:
                familia = '-' if row[2] == 'None' else row[2]
                categoria = {
                    "uuid_familia": row[0] if row[0] is not None else None,
                    "idfamilia": row[1] if row[1] is not None else None,
                    "familia": familia,
                    "idarticulo": row[3] if row[3] is not None else None,
                    "articulo": row[4] if row[4] is not None else None,
                    "codigoproducto": row[5] if row[5] is not None else None,
                    "nombre": row[6] if row[6] is not None else None
                }
                lista.append(categoria)
            #Preparar la respuesta
            response = {
                "data": lista,
                "total": total_count[0],
                "page" : page_num,
                "limit":page_size
            }
            return response
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener las categorias")
    finally:
        # Cerrar la conexión a la base de datos
        if cursor:
            cursor.close()
        if connection:
            connection.close()

#listarfamilia()            

@ruta.post("/buscarfamilia")
def buscarfamilia(familia:dict):
    connection = get_connection()
    try:
        print("pintame los producto:",familia)
        cursor = connection.cursor()
        datos = familia['datos']

        print("pintame los datos:",datos)
        
        sql = f"select * from poo_familia f where (upper(nombreproducto) like upper('%{datos}%') or upper(articulo) like upper('%{datos}%') or upper(familia) like upper('%{datos}%'))  order by f.uuid_familia  asc OFFSET 0 ROWS FETCH NEXT 50 ROWS ONLY;"

        print("pintame consulta sql:",sql)
        cursor.execute(sql)
        rs = cursor.fetchall()

        print("pintame consulta rs:",rs)

        lista = []

        for row in rs:
                familia = {
                    "idcategoria": row[0],
                    "idfamilia": row[1],
                    "familia": row[2],
                    "idarticulo": row[3],
                    "articulo": row[4],
                    "codigoproducto":row[5],
                    "nombre": row[6]
                }
                lista.append(familia)
        return lista
    except Exception as e:
        raise HTTPException(status_code=500, detail="Ocurrió un error al obtener las familia")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()