from fastapi import APIRouter, Depends, status, UploadFile, File, Request,Header
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from gdh.schemas.asientosSchema import asientoparamentros_Schema
from gdh.functions.asientosFunction import ExtraerAsientosADRYAN

ruta = APIRouter(
    prefix="/asientos",tags=["Asientos"]
)

@ruta.post("/", status_code=status.HTTP_200_OK)
async def Migrar(info : asientoparamentros_Schema):       
    return ExtraerAsientosADRYAN(info)