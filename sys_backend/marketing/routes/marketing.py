
from fastapi import APIRouter
from marketing.routes import productooriginal


ruta = APIRouter(
    prefix='/marketing',
)

ruta.include_router(productooriginal.ruta)

