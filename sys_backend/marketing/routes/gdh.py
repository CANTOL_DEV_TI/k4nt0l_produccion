from fastapi import APIRouter

from gdh.routes import asientos,empleados
from gdh.routes import puntos

ruta = APIRouter(
    prefix="/gdh",
)

ruta.include_router(asientos.ruta)
ruta.include_router(empleados.ruta)
#puntos
ruta.include_router(puntos.ruta)