from conexiones.conexion_sql_solo import ConectaSQL_GDH
from conexiones.conexion_sap_solo import conexion_SAP_Tecno
from conexiones.conexion_mmsql import conexion_mssql
from gdh.schemas.empleadosSchema import empleado_Schema
from gdh.functions.loginSLDFunction import logintoken
from os import getenv
import requests
import json
import urllib3

def GrabarEmpleados(pEmpleados : empleado_Schema):
    
    try:
        resultado_final = []
        
        urllib3.disable_warnings()

        CiaSAP = ""
        
        for d in pEmpleados:           
            if(d.CiaSAP == "CNT"):
                CiaSAP = "SBO_CANTOL_PRODUCCION"
                #CiaSAP = "APR_PRESUP_01"
            elif(d.CiaSAP == "TNP"):
                CiaSAP = "SBO_TECNO_PRODUCCION"
                #CiaSAP = "APR_PRESUP_01"
            elif(d.CiaSAP == "DTM"):
                CiaSAP = "SBO_DISTRI_PRODUCCION"
                #CiaSAP = "APR_PRESUP_01"

        token = logintoken(CiaSAP)
        
        session = token['SessionId']
        
        for d in pEmpleados:

            Tramafinal = {'CardCode' : "", 'CardType' : "cSupplier" , 'CardName' : "" , 'Currency' : "S/", 'GroupCode' : "113" , 'U_MSSL_BTD' : "1" , 'FederalTaxID' : "" , 'U_MSSL_BTP' : "TPN" ,
                        'U_MSSL_BAP' : "", 'U_MSSL_BAM' : "" , 'U_MSSL_BN1' : "" , 'U_MSSL_BN2' : "" , 'Phone1' : "" , 'Country' : "PE" , 'AdditionalID' : "" , 'DebitorAccount' : "4111102",
                        'DefaultAccount' : "", "DefaultBranch": "S/", "DefaultBankCode": '', "BPBankAccounts" : [{ "Branch": "S/", 'Country': "PE", 'BankCode': "", 'AccountNo': "",           
                                                                                                              'AccountName': "", 'BICSwiftCode' : ""}]}
            
            
            Tramafinal["CardCode"] = d.CardCode
            Tramafinal["CardName"] = d.CardName
            Tramafinal["FederalTaxID"] = d.FederalTaxID
            Tramafinal["U_MSSL_BAP"] = d.U_MSSL_BAP
            Tramafinal["U_MSSL_BAM"] = d.U_MSSL_BAM
            Tramafinal["U_MSSL_BN1"] = d.U_MSSL_BN1
            Tramafinal["U_MSSL_BN2"] = d.U_MSSL_BN2
            Tramafinal["Phone1"] = d.Phone1
            Tramafinal['DefaultAccount'] = d.AccountNo
            Tramafinal['DefaultBankCode'] = d.BankCode
            
            Tramafinal['BPBankAccounts'] = [{'BankCode' : d.BankCode, 'Branch' : "S/",'AccountNo' : d.AccountNo , 'AccountName' : d.AccountName, 'BICSwiftCode' : d.BICSwiftCode}]
        
            payload = json.dumps(Tramafinal)
            
            header = {
                        'Content-Type': 'application/json',
                        'Cookie': f'B1SESSION={session}; ROUTEID=.node3'
                    }
            
            Servidor = getenv("SERVIDOR_SLD")
            url = f"{Servidor}/b1s/v1/BusinessPartners"

            response = requests.request("POST", url, headers=header, data=payload,verify=False)

            resultado = json.loads(response.text)

            resultado_det = {'CardCode' : "", 'Resultado' : ""}

            resultado_det["CardCode"] = d.CardCode
            
            if("error" in resultado):
                resultado_det["Resultado"] = resultado['error']['message']['value']
            else:
                resultado_det["Resultado"] = "Se genero correctamente en el SAP"

            resultado_final.append(resultado_det)

        return resultado_final
                
    except Exception as err:
        return f"Error en la operación : {err}."
    
def empleado_subarea(cod_subarea):
    sql = f" SELECT cod_empleado, nombre FROM empleado emp"
    sql += f" WHERE emp.sw_estado in ('1')"
    if cod_subarea != "0":
        sql += f" AND emp.cod_subarea = '" + cod_subarea + "'"
    sql += f" ORDER BY emp.nombre"

    me_conexion = conexion_mssql()
    resultadosConsulta = me_conexion.consultas_sgc(sql, ())
    return resultadosConsulta

def empleado_subarea_adryan(pTodas,pCod_subarea):
    resultadosADRYAN = []

    sql = "select tc.nombre_cia as empresa, t.apellido_paterno ,t.apellido_materno ,t.nombre,t.numero_documento as dni,"
    sql += "tpc.descripcion_puesto as puesto,tuf.centro_costo as cc_codigo,tuf.nombre_unidad_funcional as cc_nombre from trabajador t "
    sql += "inner join Tbl_compania tc on t.compania = tc.codigo_cia inner join Tbl_unidad_funcional tuf on tc.codigo_cia = tuf.compania and t.unidad_funcional_organica = tuf.unidad_funcional "
    sql += "inner join Tbl_puesto_compania tpc on tc.codigo_cia = tpc.compania and t.puesto_organica = tpc.puesto "
    sql += f" where tc.codigo_cia ='01' and t.fecha_retiro is null and (('S'='{pTodas}') or (rtrim(tuf.centro_costo)= '{pCod_subarea}')) order by 1,2,3"
    print(sql)
    conSQL = ConectaSQL_GDH()
    curAdryan = conSQL.cursor(as_dict = True)

    curAdryan.execute(sql)

    for rowA in curAdryan:        
        lista_fila_a = {"empresa" : rowA['empresa'].strip(),"apellido_paterno" : rowA['apellido_paterno'].strip(),"apellido_materno" : rowA['apellido_materno'].strip(), "nombre" : rowA['nombre'].strip(),
                        "dni" : rowA['dni'].strip(), "puesto" : rowA['puesto'].strip(),"cc_codigo" : rowA['cc_codigo'].strip(),"cc_nombre":rowA['cc_nombre'].strip() }
        resultadosADRYAN.append(lista_fila_a)
    
    conSQL.close()

    return resultadosADRYAN

def CompararEmpleados(pCiaADRYAN,pSAPCia):
    try:
        lista_fila_a = {}

        resultadosSAP = []
        resultadosADRYAN = []
        resultadoFaltantes = []
        resultadoMigrar = []

        sqlADRYAN = f"select ltrim(rtrim(numero_documento)) as DNI,'NO' as Existe  from Trabajador t (nolock) where (fecha_retiro is NULL)  and compania = '{pCiaADRYAN}' UNION ALL "
        sqlADRYAN += f"select ltrim(rtrim(numero_documento)) as DNI,'NO' as Existe from Trabajador t (nolock) where DATEDIFF(MM,fecha_retiro,GETDATE()) <=2 and DATEDIFF(DD,fecha_ingreso_compania,fecha_retiro) < 30 and compania = '{pCiaADRYAN}'"
        
        sqlSAP = f"SELECT \"LicTradNum\" AS DNI FROM {pSAPCia}.ocrd Cli WHERE \"GroupCode\" = 113"

        ###Arreglo ADRYAN###
        conSQL = ConectaSQL_GDH()

        curAdryan = conSQL.cursor(as_dict = True)

        curAdryan.execute(sqlADRYAN)

        for rowA in curAdryan:        
            lista_fila_a = {"DNI" : rowA['DNI'].strip(),"Existe" : rowA['Existe']}
            resultadosADRYAN.append(lista_fila_a)
        
        conSQL.close()
        ####################

        ####Arreglo SAP#####
        conSAP = conexion_SAP_Tecno()

        curSAP = conSAP.cursor()

        curSAP.execute(sqlSAP)

        for row in curSAP:
            ##lista_fila = {"DNI" : }
            resultadosSAP.append(row['DNI'])
                
        conSAP.close()
        ####################
        
        
        for rowI in resultadosADRYAN:
            valor_buscar = rowI['DNI']
            #print(valor_buscar in resultadosSAP)
            if(valor_buscar in resultadosSAP)==False:           
                resultadoFaltantes.append(valor_buscar)
        
        
        if(len(resultadoFaltantes)==1):
            Lista = str(tuple(resultadoFaltantes))

            ListaT = str(Lista).rstrip(Lista[-1]) 
            ListaT2 = str(ListaT).rstrip(ListaT[-1]) 
            ListaT3 = ListaT2 + ")"
        else:
            ListaT3 = str(tuple(resultadoFaltantes))

        if(len(ListaT3) == 0):
            print("NO hay")
        
        #print(ListaT3)

        sqlFaltantes = "select case compania when '01' then 'TNP' when '02' then 'DTM' when '03' then 'CNT' end as CiaSAP, rtrim('E00' + numero_documento) as CardCode, "
        sqlFaltantes += "rtrim( rtrim(apellido_paterno) + ' '  + rtrim(apellido_materno) + ', ' + rtrim(nombre)) as CardName, numero_documento as FederalTaxID,"
        sqlFaltantes += "rtrim(apellido_paterno) as U_MSSL_BAP,rtrim(apellido_materno) as U_MSSL_BAM, "
        sqlFaltantes += "rtrim(nombre) as U_MSSL_BN1,'' as U_MSSL_BN2, isnull(telefono_movil,'')  as Phone1, "
        sqlFaltantes += "'0' + b.codigo_entidad as BankCode, case isnull(t.numero_cuenta_abono,'') when '' then 'X' else t.numero_cuenta_abono end as AccountNo, b.codigo_entidad as BICSwiftCode,'S/' as Branch, 'CUENTA DE AHORROS' as AccountName, 'PE' as Country "
        sqlFaltantes += "from Trabajador t left join Tbl_entidad_financiera b on t.codigo_banco_abono = b.codigo_entidad "
        sqlFaltantes += f"where numero_documento in {ListaT3} and compania='{pCiaADRYAN}'"
        #print(sqlFaltantes)
        conSQLF = ConectaSQL_GDH()

        curAdryanF = conSQLF.cursor(as_dict = True)

        curAdryanF.execute(sqlFaltantes)
        
        #print(curAdryanF)

        for rowF in curAdryanF:        
            #print(rowF)
            lista_fila_f = {"CiaSAP" : rowF['CiaSAP'].strip(),"CardCode" : rowF['CardCode'].strip(), "CardName" : rowF['CardName'].strip(), "FederalTaxID" : rowF['FederalTaxID'].strip(),
                            "U_MSSL_BAP" : rowF['U_MSSL_BAP'].strip(),"U_MSSL_BAM":rowF['U_MSSL_BAM'],"U_MSSL_BN1" : rowF['U_MSSL_BN1'].strip(),"U_MSSL_BN2" : rowF['U_MSSL_BN2'].strip(),
                            "Phone1" : rowF['Phone1'].strip(),"BankCode" : rowF['BankCode'].strip(),"AccountNo" : rowF['AccountNo'].strip(),"BICSwiftCode" : rowF['BICSwiftCode'].strip(),
                            "Branch" : rowF['Branch'].strip(),"AccountName" : rowF['AccountName'],"Country" : rowF['Country']}
            
            resultadoMigrar.append(lista_fila_f)
        
        conSQLF.close()

        return resultadoMigrar
    
    except Exception as Err:
        
        lista_fila_f = {"CiaSAP" : "...","CardCode" : "...", "CardName" : "...", "FederalTaxID" : "...",
                            "U_MSSL_BAP" : "...","U_MSSL_BAM":"...","U_MSSL_BN1" : "...","U_MSSL_BN2" : "...",
                            "Phone1" : "...","BankCode" : "...","AccountNo" : "...","BICSwiftCode" : "...",
                            "Branch" : "...","AccountName" : "...","Country" : "..."}
        
        resultadoMigrar.append(lista_fila_f)

        return resultadoMigrar