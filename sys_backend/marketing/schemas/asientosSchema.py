from decimal import Decimal
from typing import Optional,List
from pydantic import BaseModel

class detalle_Schema(BaseModel):
    ParentKey : Optional[int]
    Line_id	: Optional[int]
    ShortName : Optional[str]
    AccountCode	: Optional[str]
    Debit : Optional[Decimal]
    Credit : Optional[Decimal]    
    CostingCode2 : Optional[str]
    CostingCode : Optional[str]

class asiento_Schema(BaseModel):
    JdtNum : Optional[int]
    ReferenceDate : Optional[str]    
    Memo : Optional[str]    
    JournalEntryLines : Optional[list[detalle_Schema]]

    class Config:
        orm_mode = True

class asientoparamentros_Schema(BaseModel):
    CiaADRYAN : Optional[str]
    TipoPlanilla : Optional[str]
    Periodo : Optional[str]
    Ejercicio : Optional[str]
    Fecha : Optional[str]
    Comentario : Optional[str]


