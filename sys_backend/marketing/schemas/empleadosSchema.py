from typing import Optional, List
from pydantic import BaseModel

class empleado_Schema(BaseModel):
    CiaSAP : Optional[str]
    CardCode : Optional[str]
    CardType : Optional[str]
    CardName : Optional[str]
    Currency : Optional[str]
    GroupCode : Optional[str]
    U_MSSL_BTD : Optional[str]
    FederalTaxID : Optional[str]
    U_MSSL_BTP : Optional[str]
    U_MSSL_BAP : Optional[str]
    U_MSSL_BAM : Optional[str]
    U_MSSL_BN1 : Optional[str]
    U_MSSL_BN2 : Optional[str]
    Phone1 : Optional[str]
    Country : Optional[str]
    AdditionalID :Optional[str]
    DebitorAccount : Optional[str]
    BankCode : Optional[str] 
    AccountNo : Optional[str] 
    AccountName : Optional[str]
    BICSwiftCode : Optional[str]
    Branch : Optional[str]
    Country : Optional[str]

    class Config:
        orm_mode = True