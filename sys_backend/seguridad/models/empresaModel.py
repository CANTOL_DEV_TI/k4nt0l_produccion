from sqlalchemy import Column, Integer, String
from conexiones.conexion_sql import Base


class empresa_Model(Base):
    __tablename__ = 'tEmpresa'
    empresa_id = Column(Integer, primary_key=True)
    empresa_codigo = Column(String)
    empresa_nombre = Column(String)
    empresa_ruc = Column(String)
    empresa_correo = Column(String)     
    serie_pagos_efectuados = Column(Integer)
    serie_pagos_recibidos = Column(Integer)
    serie_boletas = Column(Integer)
    serie_facturas = Column(Integer)
    serie_orden_venta = Column(Integer)
    serie_nota_credito = Column(Integer)
    serie_nota_debito = Column(Integer)
    serie_letras_refinanciadas = Column(Integer)

    def __str__(self):
        return self.empresa_nombre