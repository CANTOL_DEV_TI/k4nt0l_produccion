from sqlalchemy import Column, Integer, String
from conexiones.conexion_sql import Base


class modulo_Model(Base):
    __tablename__ = 'tModulos'
    modulo_id = Column(Integer, primary_key=True)
    modulo_codigo = Column(String)
    modulo_nombre = Column(String)
    modulo_padre = Column(Integer)
    modulo_padre_id = Column(Integer)      
    modulo_pagina = Column(String) 

    def __str__(self):
        return self.modulo_nombre