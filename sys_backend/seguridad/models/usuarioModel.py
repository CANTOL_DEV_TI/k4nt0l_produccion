from sqlalchemy import Column, Integer, String
from conexiones.conexion_sql import Base


class usuario_Model(Base):
    __tablename__ = 'tUsuarios'
    usuario_id = Column(Integer, primary_key=True)
    usuario_codigo = Column(String)
    usuario_password = Column(String)
    usuario_nombre = Column(String)
    usuario_estado = Column(String)  
    usuario_email = Column(String)     

    def __str__(self):
        return self.usuario_nombre