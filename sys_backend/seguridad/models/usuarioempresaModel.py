from sqlalchemy import Column, Integer, String
from conexiones.conexion_sql import Base


class usuarioempresa_Model(Base):
    __tablename__ = 'tAccesoEmpresa'
    usuario_id = Column(Integer, primary_key=True)
    empresa_id = Column(Integer, primary_key=True)                   

    def __str__(self):
        return self.usuario_id