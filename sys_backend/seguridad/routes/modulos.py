from fastapi import APIRouter, Depends, status, UploadFile, File, Request,Header 
from fastapi.responses import FileResponse 
from sqlalchemy.orm.session import Session 
from conexiones.conexion_sql_seguridad import get_db 
 
from seguridad.schemas.modulosSchema import modulos_Schema 
from seguridad.models.modulosModel import modulo_Model 
from seguridad.functions import modulosFunction 
 
 
ruta = APIRouter( 
    prefix="/modulos", tags=["Modulos"]
) 
 
@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK) 
def showFilter(txtFind: str, db: Session = Depends(get_db)): 
    data = db.query(modulo_Model).filter(modulo_Model.modulo_nombre.contains(txtFind.strip())).all() 
    return data 
 
@ruta.get('/Padres/', status_code=status.HTTP_200_OK) 
def showPadres(db: Session = Depends(get_db)): 
    data = db.query(modulo_Model).filter(modulo_Model.modulo_padre_id == "0").all() 
    return data 
 
@ruta.post('/') 
async def grabar(info: modulos_Schema): 
    print(info)     
    return modulosFunction.asignar(info) 
 
@ruta.put('/') 
async def actualizar(info: modulos_Schema): 
    return modulosFunction.actualizar(info) 
 
@ruta.delete('/') 
async def eliminar(info: modulos_Schema): 
    return modulosFunction.quitar(info) 
