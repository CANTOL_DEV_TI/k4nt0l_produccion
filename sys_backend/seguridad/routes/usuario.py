from fastapi import APIRouter, Depends, status, UploadFile, File, Request,Header
from fastapi.responses import FileResponse
from sqlalchemy.orm.session import Session

from conexiones.conexion_sql_seguridad import get_db
from seguridad.models.usuarioModel import usuario_Model
from seguridad.models.modulosModel import modulo_Model
from seguridad.models.empresaModel import empresa_Model
from seguridad.models.usuariomoduloModel import usuariomodulo_Model
from seguridad.schemas.usuarioSchemas import usuario_Schema
from seguridad.schemas.usuarioempresaSchemas import usuarioempresa_Schema
from seguridad.schemas.usuariomoduloSchema import usuariomodulo_Schema
from seguridad.schemas.usuarioactcontraSchema import usuarioactcontrasena_Schema
from seguridad.schemas.usuariomodulovalSchema import usuariomoduloval_Schema
from seguridad.functions import usuarioFunction
from seguridad.functions.tokenFunction import validar_token_existente
from seguridad.functions.reportesFunction import reportes_auditoria
from seguridad.functions.modulosFunction import valida_modulo_acceso
from seguridad.functions.contrasenaFunction import generarclave

ruta = APIRouter(
    prefix="/usuario", tags=["Usuario"]
)

@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK)
def showFilter(txtFind: str, db: Session = Depends(get_db)):
    data = db.query(usuario_Model).filter(usuario_Model.usuario_codigo.contains(txtFind.strip())).all()
    return data

@ruta.get('/Activos/', status_code=status.HTTP_200_OK)
def showActivos(db: Session = Depends(get_db)):
    data = db.query(usuario_Model).filter(usuario_Model.usuario_estado.contains("1")).all()
    return data

@ruta.post('/')
async def grabar(info: usuario_Schema):    
    return usuarioFunction.insert(info)

@ruta.put('/')
async def actualizar(info: usuario_Schema):
    return usuarioFunction.update(info)

@ruta.delete('/')
async def eliminar(info: usuario_Schema):
    return usuarioFunction.delete(info)

@ruta.post('/validar_token/')
async def validar_token(Authorization:str = Header(None)): 
    try:
        Token = Authorization.split(" ")[1]            
        return validar_token_existente(Token,output=True)
    except Exception as Err:
        return f"XXXXXXXX"

@ruta.get('/CiaAsignadas/{pUsuario}',status_code=status.HTTP_200_OK)
def ListaCiasUsuario(pUsuario: int, db: Session = Depends(get_db)):    
    data = usuarioFunction.listado_empresa_usuario(pUsuario)
    return data

@ruta.post('/AgregarCia/')
async def agregarcia(info:usuarioempresa_Schema):
    print(info)
    return usuarioFunction.AgregarCIA(info)

@ruta.delete('/QuitarCia/')
async def quitarcia(info:usuarioempresa_Schema):
    return usuarioFunction.QuitarCIA(info)

@ruta.put('/ActPassword/')
async def actpassword(info:usuarioactcontrasena_Schema):
    return usuarioFunction.ActPassword(info)

@ruta.get('/auditoria_ingreso/')
async def auditoria_ingreso(pCia:str,pUsuario:str,pOrden:str):
    return reportes_auditoria(pCia,pUsuario,pOrden)

@ruta.get('/ListaEmpresas/', status_code=status.HTTP_200_OK)
def ListaEmpresas(db:Session = Depends(get_db)):
    data = db.query(empresa_Model).all()
    return data

@ruta.get('/ListaModulos/', status_code=status.HTTP_200_OK)
def ListaModulos(db:Session = Depends(get_db)):
    data = db.query(modulo_Model).filter(modulo_Model.modulo_padre != 0).all()
    return data

@ruta.get('/ListaSubModulos/{pNivel}', status_code=status.HTTP_200_OK)
def ListaModulos(pNivel: str,db:Session = Depends(get_db)):
    data = db.query(modulo_Model).filter(modulo_Model.modulo_padre_id == pNivel).all()
    return data

@ruta.get('/ListaPermisos/{pUsuario}-{pEmpresa}',status_code=status.HTTP_200_OK)
def listado_permisos_usuario(pUsuario: str, pEmpresa : str, db: Session = Depends(get_db)):    
    data = usuarioFunction.listado_permisos_usuario (pUsuario,pEmpresa)
    return data

@ruta.post('/AgregarPermiso/')
async def agregarpermiso(info:usuariomodulo_Schema):
    data = usuarioFunction.agregar_modulo(info)
    return data

@ruta.delete('/QuitarPermiso/')
async def quitarpermiso(info:usuariomodulo_Schema):
    data = usuarioFunction.quitar_modulo(info)
    return data

@ruta.post('/VerificaPermiso/')
async def verificarpermiso(info:usuariomoduloval_Schema):    
    data = valida_modulo_acceso(info)
    return data

@ruta.patch('/ResetClave/{pUsuario}',status_code=status.HTTP_200_OK)
async def resetclave(pUsuario:str):
    return generarclave(pUsuario)