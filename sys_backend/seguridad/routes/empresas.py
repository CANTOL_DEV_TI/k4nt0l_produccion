from fastapi import APIRouter, Depends, status, UploadFile, File, Request,Header 
from fastapi.responses import FileResponse 
from sqlalchemy.orm.session import Session 
 
from conexiones.conexion_sql_seguridad import get_db 
from seguridad.schemas.empresaSchema import empresa_Schema 
from seguridad.models.empresaModel import empresa_Model 
from seguridad.functions import empresasFunction 
 
ruta = APIRouter( 
    prefix="/empresas", tags=["Empresas"]
) 
 
@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK) 
def showFilter(txtFind: str, db: Session = Depends(get_db)): 
    data = db.query(empresa_Model).filter(empresa_Model.empresa_nombre.contains(txtFind.strip())).all() 
    return data 
 
@ruta.post('/') 
async def grabar(info: empresa_Schema):     
    return empresasFunction.insert(info) 
 
@ruta.put('/') 
async def actualizar(info: empresa_Schema): 
    return empresasFunction.update(info) 
 
@ruta.delete('/') 
async def eliminar(info: empresa_Schema): 
    return empresasFunction.delete(info)

@ruta.get('/listaseries/{xCia}', status_code=status.HTTP_200_OK)
async def ListaSeries(xCia:str):
    return empresasFunction.ListaSeriesDoc(xCia)