from fastapi import APIRouter, Depends, status, UploadFile, File, Request,Header 
from fastapi.responses import FileResponse 
from sqlalchemy.orm.session import Session 
 
from conexiones.conexion_sql_seguridad import get_db 
from seguridad.schemas.grupoSchema import grupo_Schema
from seguridad.schemas.usuariogrupoSchema import grupousuario_Schema
from seguridad.functions import grupoFunction 
 
ruta = APIRouter( 
    prefix="/grupos", tags=["Grupos"]
) 
 
@ruta.get('/{txtFind}', status_code=status.HTTP_200_OK) 
def showFilter(txtFind: str): 
    data = grupoFunction.Listagrupos(txtFind)
    return data 
 
@ruta.post('/') 
async def grabar(info: grupo_Schema):     
    return grupoFunction.insert(info)
 
@ruta.put('/') 
async def actualizar(info: grupo_Schema): 
    return grupoFunction.update(info) 
 
@ruta.delete('/') 
async def eliminar(info: grupo_Schema): 
    return grupoFunction.delete(info)

@ruta.post('/AsignarUsuario/')
async def Asignar(info:grupousuario_Schema):
    return Asignar(info)

@ruta.get('/ListaXUsuario/{pUsuario}',status_code=status.HTTP_200_OK)
async def GruposXUsuario(pUsuario:str):
    return grupoFunction.ListagruposxUsuario(pUsuario)

@ruta.get('/ValidaGrupoUsuario/{pUsuario}-{pGrupo}',status_code=status.HTTP_200_OK)
async def ValidaGU(pUsuario:str, pGrupo:str):
    return grupoFunction.ValidaGruposUsuario(pUsuario,pGrupo)