from fastapi import APIRouter 
 
from seguridad.routes import modulos,empresas,usuario,grupos
 
ruta = APIRouter( 
    prefix="/seguridad"
) 
 
ruta.include_router(usuario.ruta) 
ruta.include_router(modulos.ruta) 
ruta.include_router(empresas.ruta)
ruta.include_router(grupos.ruta)