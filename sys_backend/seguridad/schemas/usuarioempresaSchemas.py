from typing import Optional
from pydantic import BaseModel

class usuarioempresa_Schema(BaseModel):
    usuario_id : Optional[int]
    empresa_id : Optional[int]
    
    class Config:
        orm_mode = True