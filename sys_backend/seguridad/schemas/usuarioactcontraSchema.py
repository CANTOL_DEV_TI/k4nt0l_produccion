from typing import Optional
from pydantic import BaseModel

class usuarioactcontrasena_Schema(BaseModel):
    usuario_login : Optional[str]
    usuario_password_a : Optional[str]
    usuario_password_n : Optional[str]
    usuario_password_v : Optional[str]    
    
    class Config:
        orm_mode = True