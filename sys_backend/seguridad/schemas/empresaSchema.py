from typing import Optional
from pydantic import BaseModel

class empresa_Schema(BaseModel):
    empresa_id : Optional[int]
    empresa_codigo : Optional[str]
    empresa_nombre : Optional[str]
    empresa_ruc : Optional[str]
    empresa_correo : Optional[str]
    serie_pagos_efectuados : Optional[int]
    serie_pagos_recibidos : Optional[int]
    serie_boletas : Optional[int]
    serie_facturas : Optional[int]
    serie_orden_venta : Optional[int]
    serie_nota_credito : Optional[int]
    serie_nota_debito : Optional[int]
    serie_letras_refinanciadas : Optional[int]
    
    class Config:
        orm_mode = True