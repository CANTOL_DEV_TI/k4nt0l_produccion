from typing import Optional
from pydantic import BaseModel

class grupo_Schema(BaseModel):
    Grupo_id : Optional[int]    
    Grupo_Codigo : Optional[str]
    Nombre : Optional[str]
    AvisoInicial : Optional[int]    
    
    class Config:
        orm_mode = True