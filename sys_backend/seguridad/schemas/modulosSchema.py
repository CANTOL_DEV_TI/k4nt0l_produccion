from typing import Optional
from pydantic import BaseModel

class modulos_Schema(BaseModel):
    modulo_id : Optional[int]
    modulo_codigo : Optional[str]
    modulo_nombre : Optional[str]
    modulo_padre : Optional[int]
    modulo_padre_id : Optional[int]
    modulo_pagina : Optional[str]
    
    class Config:
        orm_mode = True