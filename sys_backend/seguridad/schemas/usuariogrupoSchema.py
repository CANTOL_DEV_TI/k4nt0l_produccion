from typing import Optional
from pydantic import BaseModel

class grupousuario_Schema(BaseModel):
    Grupo_Codigo : Optional[str]    
    Grupo_ID : Optional[int]
    Usuario_Codigo : Optional[str]
    Usuario_ID : Optional[int]
      
    
    class Config:
        orm_mode = True