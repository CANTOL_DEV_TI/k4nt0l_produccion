from typing import Optional
from pydantic import BaseModel

class usuariomoduloval_Schema(BaseModel):
    usuario_login : Optional[str]
    empresa_codigo : Optional[str]
    modulo_codigo : Optional[str]

    class Config:
        orm_mode = True