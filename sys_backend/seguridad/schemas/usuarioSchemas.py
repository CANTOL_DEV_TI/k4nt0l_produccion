from typing import Optional
from pydantic import BaseModel

class usuario_Schema(BaseModel):
    usuario_id : Optional[int]
    usuario_codigo : Optional[str]
    usuario_password : Optional[str]
    usuario_nombre : Optional[str]
    usuario_estado : Optional[str]
    usuario_super : Optional[str]
    usuario_cia : Optional[int]
    usuario_sap_empleado : Optional[int]
    nrotelefono : Optional[str]
    usuario_perfil_mobile : Optional[str]
    usuario_email : Optional[str]
    
    class Config:
        orm_mode = True