from conexiones.conexion_mmsql_seguridad import conexion_mssql 
from conexiones import conexion_sql_solo
 
def insert(meData): 
    valores = []     
    valores.append(meData.Grupo_Codigo)
    valores.append(meData.Nombre) 
    valores.append(meData.AvisoInicial)         
     
    sql = "insert into tGrupos(Grupo_Codigo,Nombre,AvisoInicial) values(%s,%s,%s)" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    #print(sql) 
    return respuesta 
 
def update(meData): 
    valores = []        
    valores.append(meData.Nombre) 
    valores.append(meData.AvisoInicial)     
    valores.append(meData.Grupo_Codigo)
 
    sql = "update tGrupos set Nombre=%s, AvisoInicial = %s where Grupo_Codigo=%s" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))     
    return respuesta 
 
def delete(meData): 
    valores = [] 
    valores.append(meData.Grupo_Codigo)
 
    sql = "delete from tGrupos where Grupo_Codigo=%s" 
     
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    #print(sql) 
    return respuesta

def AgregarUsuarioGrupo(meData):
    valores = []
    valores.append(meData.Grupo_ID)
    valores.append(meData.Usuario_ID)

    sql = "insert into tUsuariosGrupos values(%s,%s)"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql,tuple(valores))
    return respuesta

def QuitarUsuarioGrupo(meData):
    valores = []
    valores.append(meData.Grupo_ID)
    valores.append(meData.Usuario_ID)

    sql = "delete from tUsuariosGrupos where Grupo_ID=%s and Usuario_ID=%s"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql,tuple(valores))
    return respuesta

def VerificarUsuarioGrupo(meData):    
    sGrupo = meData.Grupo_ID
    sUsuario = meData.Usuario_ID
    
    sql =   "select count(*) as Acceso from tUsuariosGrupos UG "
    sql +=  "inner join tUsuarios U on U.Usuario_ID = UG.Usuario_ID "
    sql +=  f"inner join tGrupos G on G.Grupo_ID = UG.Grupo_ID where Usuario_Codigo = '{sUsuario}' and Grupo_Codigo = '{sGrupo}' "

    meConexion = conexion_sql_solo.ConectaSQL_Seguridad()

    cur_opciones = meConexion.cursor(as_dict = True)

    cur_opciones.execute(sql)
    
    acceso = 0

    for res in cur_opciones:
        acceso = res['Acceso']
    
    return acceso

def ListagruposxUsuario(pUsuario):
    
    lista = []

    sql =   "select Grupo_Codigo,G.Nombre,G.AvisoInicial from tUsuariosGrupos UG "
    sql +=  "inner join tUsuarios U on U.Usuario_ID = UG.Usuario_ID "
    sql +=  f"inner join tGrupos G on G.Grupo_ID = UG.Grupo_ID where Usuario_Codigo = '{pUsuario}' "

    meConexion = conexion_sql_solo.ConectaSQL_Seguridad()

    cur_opciones = meConexion.cursor(as_dict = True)

    cur_opciones.execute(sql)
    
    acceso = {}

    for res in cur_opciones:
        acceso = { "Grupo_Codigo" : res['Grupo_Codigo'], "Nombre" : res['Nombre'],"AvisoInicial" : res['AvisoInicial']}
        lista.append(acceso)
    
    return lista

def ValidaGruposUsuario(pUsuario,pGrupo):
    
    lista = []

    sql =   "select Grupo_Codigo,G.Nombre,G.AvisoInicial from tUsuariosGrupos UG "
    sql +=  "inner join tUsuarios U on U.Usuario_ID = UG.Usuario_ID "
    sql +=  f"inner join tGrupos G on G.Grupo_ID = UG.Grupo_ID where Usuario_Codigo = '{pUsuario}' and Grupo_Codigo = '{pGrupo}'"

    meConexion = conexion_sql_solo.ConectaSQL_Seguridad()

    cur_opciones = meConexion.cursor(as_dict = True)

    cur_opciones.execute(sql)
    
    acceso = {}

    for res in cur_opciones:
        acceso = { "Grupo_Codigo" : res['Grupo_Codigo'], "Nombre" : res['Nombre'],"AvisoInicial" : res['AvisoInicial']}
        lista.append(acceso)
    
    return lista


def Listagrupos(pGrupo):
    
    lista = []

    if(pGrupo==" "):
        pGrupo=""

    sql =   "select Grupo_ID,Grupo_Codigo,Nombre,AvisoInicial from tGrupos " 
    sql +=  f"where Nombre like '%{pGrupo}%'"

    meConexion = conexion_sql_solo.ConectaSQL_Seguridad()

    cur_opciones = meConexion.cursor(as_dict = True)

    cur_opciones.execute(sql)
    
    acceso = {}

    for res in cur_opciones:
        acceso = { "Grupo_Codigo" : res['Grupo_Codigo'], "Nombre" : res['Nombre'],"AvisoInicial" : res['AvisoInicial']}
        lista.append(acceso)
    
    return lista