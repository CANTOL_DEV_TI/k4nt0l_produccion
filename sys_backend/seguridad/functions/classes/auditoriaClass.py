import enum

class Eventos (enum.Enum):
    Intento_Fallido = 2,
    Ingreso_Exitoso = 1,
    Carga_Menu = 3,
    Salida_Sistema = 4,

