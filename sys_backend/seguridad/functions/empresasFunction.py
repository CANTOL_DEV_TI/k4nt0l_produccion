from conexiones.conexion_mmsql_seguridad import conexion_mssql 
from typing import Union 
from conexiones import conexion_sql_solo , conexion_sap_solo
from fastapi import HTTPException 
from generales.generales import CiaSAP
 
def insert(meData): 
    valores = [] 
    valores.append(meData.empresa_codigo) 
    valores.append(meData.empresa_nombre) 
    valores.append(meData.empresa_ruc)     
    valores.append(meData.empresa_correo)
    valores.append(meData.serie_pagos_efectuados)
    valores.append(meData.serie_pagos_recibidos)
    valores.append(meData.serie_boletas)
    valores.append(meData.serie_facturas)
    valores.append(meData.serie_orden_venta)
    valores.append(meData.serie_nota_credito)
    valores.append(meData.serie_nota_debito)
    valores.append(meData.serie_letras_refinanciadas)
     
    sql = "insert into tEmpresa(empresa_codigo,empresa_nombre,empresa_ruc,empresa_correo,serie_pagos_efectuados,serie_pagos_recibidos,serie_boletas,serie_facturas"
    sql += ",serie_orden_venta,serie_nota_credito,serie_nota_debito,serie_letras_refinanciadas) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)" 
 
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    #print(sql) 
    return respuesta 
 
def update(meData): 
    valores = []     
    valores.append(meData.empresa_nombre) 
    valores.append(meData.empresa_ruc)     
    valores.append(meData.empresa_correo) 
    valores.append(meData.serie_pagos_efectuados)
    valores.append(meData.serie_pagos_recibidos)
    valores.append(meData.serie_boletas)
    valores.append(meData.serie_facturas)
    valores.append(meData.serie_orden_venta)
    valores.append(meData.serie_nota_credito)
    valores.append(meData.serie_nota_debito)
    valores.append(meData.serie_letras_refinanciadas)
    valores.append(meData.empresa_codigo)

 
    sql = "update tEmpresa set empresa_nombre=%s, empresa_ruc = %s, empresa_correo=%s, serie_pagos_efectuados=%s, serie_pagos_recibidos=%s, serie_boletas=%s,"
    sql += "serie_facturas=%s, serie_orden_venta=%s, serie_nota_credito=%s, serie_nota_debito=%s, serie_letras_refinanciadas=%s  where empresa_codigo=%s" 
    #print(sql)
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))     
    return respuesta 
 
def delete(meData): 
    valores = [] 
    valores.append(meData.empresa_codigo) 
 
    sql = "delete from tEmpresa where empresa_codigo=%s" 
     
    meConexion = conexion_mssql() 
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores)) 
    #print(sql) 
    return respuesta

def ListaSeriesDoc(pCia):
    sCia = ""

    
    sCia = CiaSAP(pCia)

    resultado = []
    filanueva = {}

    sql = f"SELECT \"Series\" as codigo,ifnull(\"SeriesName\",'')||'-'|| ifnull(\"Remark\",'') AS nombre FROM {sCia}.NNM1"

    conn = conexion_sap_solo.conexion_SAP_Tecno()

    cur_serie = conn.cursor()

    cur_serie.execute(sql)

    for fila in cur_serie:
        filanueva = {"codigo":fila['codigo'],"nombre":fila['nombre']}
        resultado.append(filanueva)
    
    #print(resultado)

    return resultado