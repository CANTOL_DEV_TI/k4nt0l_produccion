import hashlib
from conexiones.conexion_mmsql_seguridad import conexion_mssql
from typing import Union
from conexiones import conexion_sql_solo
from fastapi import HTTPException

def insert(meData):
    dc_contrasena = hashlib.md5("C4nt0L53#".encode())
    valores = []
    valores.append(meData.usuario_codigo)
    valores.append(meData.usuario_nombre)
    valores.append(meData.usuario_super)
    valores.append(dc_contrasena.hexdigest())
    valores.append(meData.usuario_sap_empleado)
    valores.append(meData.nrotelefono)
    valores.append(meData.usuario_perfil_mobile)
    valores.append(meData.usuario_email)

    sql = "insert into tUsuarios(usuario_codigo,usuario_nombre,usuario_estado,usuario_super,usuario_password,usuario_email) values(%s,%s,'1',%s,%s,%s)"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta


def update(meData):
    valores = []
    valores.append(meData.usuario_nombre)
    valores.append(meData.usuario_super)
    valores.append(meData.usuario_codigo)
    valores.append(meData.usuario_id)
    sql = "update tUsuarios set "
    sql += "usuario_nombre=%s, usuario_super=%s, usuario_codigo=%s , usuario_email=%s "
    sql += "where usuario_id=%s "

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta


def delete(meData):
    valores = []
    valores.append(meData.usuario_id)
    sql = "update tUsuarios set usuario_estado='9' "
    sql += "where usuario_id=%s "

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta

def GetPass(pUsuario: Union[str, None] = ""):
    try:        
        resultado_r = dict()         
        conn = conexion_sql_solo.ConectaSQL_Seguridad()

        listado_w = conn.cursor(as_dict = True)

        cad_sql = f"select Usuario_Password from tUsuarios where Usuario_Codigo = '{pUsuario}'"
        
        print (cad_sql)

        listado_w.execute(cad_sql)

        for row in listado_w:
            resultado_r = (row['Usuario_Password'])
                                                        
        conn.close()    

        return resultado_r
    except:
        return HTTPException(status_code=404,detail="Errores en la consulta, contacte con el administrador")

def ActPassword(meData):
    usuario_login = meData.usuario_login
    dc_contrasena_a = hashlib.md5(meData.usuario_password_a.encode())
    usuario_pass_actual = GetPass(usuario_login)
    
    if(dc_contrasena_a.hexdigest() != usuario_pass_actual):
        return HTTPException(status_code=404,detail="No coincide el password actual ")

    dc_contrasena = hashlib.md5(meData.usuario_password_n.encode())
    valores = []
    valores.append(dc_contrasena.hexdigest())
    valores.append(meData.usuario_login)
    sql = "update tUsuarios set usuario_password=%s where usuario_codigo=%s"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    
    return respuesta

def AgregarCIA(meData):
    valores = []
    valores.append(meData.empresa_id)
    valores.append(meData.usuario_id)
    sql="insert into tAccesoEmpresa(empresa_id,usuario_id,fecha_acceso) values(%s,%s,getdate())"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta

def QuitarCIA(meData):
    valores = []
    valores.append(meData.empresa_id)
    valores.append(meData.usuario_id)
    sql = "delete from tAccesoEmpresa where empresa_id=%s and usuario_id=%s"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta

def listado_empresa_usuario(pusuario: Union[str, None] = ""):
    try:
        ##print("entro")
        reporte_total = []
        reporte_r = dict()         
        conn = conexion_sql_solo.ConectaSQL_Seguridad()

        reporte_w = conn.cursor(as_dict = True)

        ##print("sigue")
        cad_sql = f"select te.Empresa_Codigo, te.Empresa_Nombre , tu.Usuario_Codigo, tae.Usuario_ID ,tae.Empresa_ID from tAccesoEmpresa tae inner join tUsuarios tu on tae.Usuario_ID = tu.Usuario_ID inner join tEmpresa te on tae.Empresa_ID = te.Empresa_ID where tae.usuario_id = {pusuario}"
        
        ##print (cad_sql)

        reporte_w.execute(cad_sql)

        for row in reporte_w:
            reporte_r = {"empresa_codigo":row['Empresa_Codigo'],"empresa_nombre":row['Empresa_Nombre'],"usuario_codigo":row['Usuario_Codigo'],"usuario_id" : row['Usuario_ID'],"empresa_id":row['Empresa_ID'] }
            reporte_total.append(reporte_r)
                                                        
        conn.close()    
        
        return reporte_total
    except:
        return HTTPException(status_code=404,detail="Errores en la consulta, contacte con el administrador")
    
def listado_permisos_usuario(pUsuario : Union[str , None] = "", pEmpresa : Union[str , None] = ""):
    try:

        listado_total = []
        listado_i = dict()

        conn = conexion_sql_solo.ConectaSQL_Seguridad()

        listado_p = conn.cursor(as_dict = True)

        cad_sql = f"select distinct tm.Modulo_Padre_ID,(select modulo_nombre from tmodulos mp where mp.modulo_id=tm.modulo_padre_id) as Padre ,tau.Modulo_ID,Modulo_Codigo,Modulo_Nombre,'S' as concedido "
        cad_sql += "from tAccesosUsuario tau (nolock) "
        cad_sql += "inner join tModulos tm (nolock) on tm.Modulo_ID = tau.Modulo_ID "
        cad_sql += "inner join tUsuarios tu (nolock) on tau.Usuario_ID = tu.Usuario_ID "
        cad_sql += "inner join tEmpresa te (nolock) on te.Empresa_ID = tau.Empresa_ID "
        cad_sql += f"where te.Empresa_Codigo = '{pEmpresa}' and tu.Usuario_Codigo = '{pUsuario}' and modulo_padre_id <> 0 "
        cad_sql += "union all "
        cad_sql += "select modulo_Padre_id,(select mp.modulo_nombre from tmodulos mp where mp.modulo_id=N.modulo_padre_id) as Padre,"
        cad_sql += "modulo_id, Modulo_Codigo,Modulo_Nombre,'N' as concedido "
        cad_sql += "from tModulos (nolock) N where N.modulo_id not in (select tm.modulo_id from tAccesosUsuario tau (nolock) "
        cad_sql += "inner join tModulos tm (nolock) on tm.Modulo_ID = tau.Modulo_ID "
        cad_sql += "inner join tUsuarios tu (nolock) on tau.Usuario_ID = tu.Usuario_ID "
        cad_sql += "inner join tEmpresa te (nolock) on te.Empresa_ID = tau.Empresa_ID "
        cad_sql += f"where te.Empresa_Codigo = '{pEmpresa}' and tu.Usuario_Codigo = '{pUsuario}') and modulo_padre_id <> 0 order by 1,2"
    
        listado_p.execute(cad_sql)

        for row in listado_p:
            listado_i = {"modulo_padre_id":row['Modulo_Padre_ID'],"padre":row['Padre'],"modulo_id":row['Modulo_ID'],"modulo_codigo" : row['Modulo_Codigo'],"modulo_nombre":row['Modulo_Nombre'], "concedido" : row['concedido'] }
            listado_total.append(listado_i)

        conn.close()

        return listado_total
    except:
        return HTTPException(status_code=404,detail="Errores en la consulta, contacte con el administrador")
    

def agregar_modulo(meData):
    valores = []
    valores.append(meData.empresa_id)
    valores.append(meData.usuario_id)
    valores.append(meData.modulo_id)
    print(meData)
    sql = "insert into tAccesosUsuario values(%s,%s,%s)"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    
    return respuesta

def quitar_modulo(meData):
    valores = []
    valores.append(meData.empresa_id)
    valores.append(meData.usuario_id)
    valores.append(meData.modulo_id)

    sql = "delete tAccesosUsuario where empresa_id=%s and usuario_id=%s and modulo_id=%s"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    print(sql)
    return respuesta