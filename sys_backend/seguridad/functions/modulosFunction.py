import hashlib
from conexiones.conexion_mmsql_seguridad import conexion_mssql
from typing import Union
from conexiones import conexion_sql_solo
from fastapi import HTTPException

def asignar(meData):
    valores = []
    valores.append(meData.modulo_codigo)
    valores.append(meData.modulo_nombre)
    valores.append(meData.modulo_padre)    
    valores.append(meData.modulo_padre_id)
    valores.append(meData.modulo_pagina)

    sql = "insert into tModulos(modulo_codigo,modulo_nombre,modulo_padre,modulo_padre_id,modulo_pagina) values(%s,%s,%s,%s,%s)"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    #print(sql)
    return respuesta

def actualizar(meData):
    valores = []
    valores.append(meData.modulo_nombre)
    valores.append(meData.modulo_padre)    
    valores.append(meData.modulo_padre_id)    
    valores.append(meData.modulo_codigo)
    
    sql = "update tModulos set modulo_nombre = %s, modulo_padre=%s,modulo_padre_id=%s  where modulo_codigo=%s"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    #print(sql)
    return respuesta

def quitar(meData):
    valores = []
    valores.append(meData.modulo_id)

    sql = "delete from tModulos where modulo_id=%s"
    
    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    #print(sql)
    return respuesta


def listado_accesos_usuario_empresa(pusuario: Union[str, None] = "", pEmpresa : Union[str , None] = ""):
    try:
        ##print("entro")
        reporte_total = []
        reporte_r = dict()         
        conn = conexion_sql_solo.ConectaSQL_Seguridad()

        reporte_w = conn.cursor(as_dict = True)

        ##print("sigue")
        cad_sql = f"select te.Empresa_Codigo, te.Empresa_Nombre , tu.Usuario_Codigo, tae.Usuario_ID ,tae.Empresa_ID from tAccesoEmpresa tae inner join tUsuarios tu on tae.Usuario_ID = tu.Usuario_ID inner join tEmpresa te on tae.Empresa_ID = te.Empresa_ID where tae.usuario_id = {pusuario}"
        
        ##print (cad_sql)

        reporte_w.execute(cad_sql)

        for row in reporte_w:
            reporte_r = {"empresa_codigo":row['Empresa_Codigo'],"empresa_nombre":row['Empresa_Nombre'],"usuario_codigo":row['Usuario_Codigo'],"usuario_id" : row['Usuario_ID'],"empresa_id":row['Empresa_ID'] }
            reporte_total.append(reporte_r)
                                                        
        conn.close()    
        
        return reporte_total
    except:
        return HTTPException(status_code=404,detail="Errores en la consulta, contacte con el administrador")
    
def valida_modulo_acceso(meData):
    try:
        login = meData.usuario_login
        empresa = meData.empresa_codigo
        modulo = meData.modulo_codigo

        valido = 0
        
        conn = conexion_sql_solo.ConectaSQL_Seguridad()

        reporte_w = conn.cursor(as_dict = True)
        
        cad_sql = "select ISNULL( count(*),0) as Acceso from tAccesosUsuario AU inner join tUsuarios U on U.Usuario_ID = AU.Usuario_ID inner join tEmpresa E on E.Empresa_ID = AU.Empresa_ID "
        cad_sql += f"inner join tModulos M on M.Modulo_ID = AU.Modulo_ID where  U.Usuario_Codigo='{login}' and E.Empresa_Codigo = '{empresa}'  and M.Modulo_Codigo = '{modulo}'"
                
        reporte_w.execute(cad_sql)

        for row in reporte_w:
            valido = {"acceso":row['Acceso']}
                                                                    
        conn.close()    
                       
        return valido
    except:
        return HTTPException(status_code=404,detail="Errores en la consulta, contacte con el administrador")