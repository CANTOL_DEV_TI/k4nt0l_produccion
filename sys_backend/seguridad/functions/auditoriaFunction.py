 
from conexiones.conexion_mmsql_seguridad import conexion_mssql 
from seguridad.functions.classes.auditoriaClass import Eventos 
 
def auditausuarios(pevento: Eventos, pusuario:str, pcia:str, pmensaje:str):    
    try: 
        valores = [] 
        valores.append(pevento) 
        valores.append(pusuario) 
        valores.append(pcia) 
        valores.append(pmensaje) 
        sql_add_evento = f"insert into tauditoria_acceso(aa_tipo,aa_hora,aa_usuario,aa_empresa,aa_mensaje) values({pevento},getdate(),'{pusuario}','{pcia}','{pmensaje}')" 
        #print(sql_add_evento) 
        meConexion = conexion_mssql() 
        respuesta = meConexion.ejecutar_funciones(sql_add_evento, "") 
        #print(respuesta) 
        print(auditultacceso(pusuario)) 
        #print() 
        return respuesta 
    except Exception as err: 
        return f"Error con el registro del login : {err}." 
 
 
def auditultacceso(pusuario:str): 
    try: 
        #valores = [] 
        #valores.append(pusuario) 
 
        sql_add_evento = f"update tUsuarios set Usuario_Ultimo_Acceso=getdate() where Usuario_Codigo='{pusuario}'" 
 
        meConexion = conexion_mssql() 
        respuesta = meConexion.ejecutar_funciones(sql_add_evento,"") 
 
        return respuesta 
    except Exception as err : 
        return f"Error en el registro de accesos : {err}."