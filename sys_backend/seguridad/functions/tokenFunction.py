from os import getenv
from jwt import encode,decode,exceptions
from fastapi.responses import JSONResponse
from datetime import timedelta,datetime

def creacion_token(pcia, pusuario):
    access_token_expiration = timedelta(days= int(getenv("ACCESS_TOKEN_DURATION")))
    expire = datetime.utcnow() + access_token_expiration
    access_token = {"sub":pcia + pusuario,"exp":expire}       
    token = encode(payload={**access_token,"exp" : expire, },key=getenv("SECRET"),algorithm=getenv("ALGORITHM_U"))
    
    return token

def validar_token_existente(token,output:False):
    try:
        
        if output:
            return JSONResponse( decode(token, key = getenv("SECRET"), algorithms=getenv("ALGORITHM_U")) )                                
                    
        decode(token, key = getenv("SECRET"), algorithms=getenv("ALGORITHM_U"))       
    
    except exceptions.DecodeError:
        return JSONResponse(content={"message":"Token invalido"},status_code=401)
    except exceptions.ExpiredSignatureError:
        return JSONResponse(content={"message":"Token expirado vuelva a validar su usuario y contraseña"},status_code=401)