from conexiones import conexion_sql_solo
from fastapi import HTTPException
from typing import Union

def reportes_auditoria(pcia: Union[str, None] = "",pusuario: Union[str, None] = "",porden : Union[str, None] = ""):
    try:
        #print("entro")
        reporte_total = []
        reporte_r = dict()
        ordenar_por = " ASC"    
        conn = conexion_sql_solo.ConectaSQL_Seguridad()

        reporte_w = conn.cursor(as_dict = True)

        if porden!="S":
            ordenar_por = " DESC"
        #print("sigue")
        cad_sql = f"select e.evento_nombre as evento, CONVERT(varchar(20),aa_hora,120) as hora,aa_usuario  as usuario, aa_empresa as cia, aa_mensaje as comentario from tauditoria_acceso aa inner join tEventos e on aa.aa_tipo = e.eventoid where (aa_usuario like '%{pusuario}%') and (aa_empresa like '{pcia}')  order by aa_hora {ordenar_por}"
        
        print (cad_sql)

        reporte_w.execute(cad_sql)

        for row in reporte_w:
            reporte_r = {"evento":row['evento'],"hora":row['hora'],"usuario":row['usuario'],"cia":row['cia'],"comentario":row['comentario'] }            
            reporte_total.append(reporte_r)
                                                        
        conn.close()    
        
        return reporte_total
    except:
        return HTTPException(status_code=404,detail="Errores en la consulta, contacte con el administrador")