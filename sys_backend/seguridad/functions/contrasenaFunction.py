import hashlib 
import random
import string
import ssl
import smtplib 
from email.message import EmailMessage
from conexiones.conexion_mmsql_seguridad import conexion_mssql
from conexiones.conexion_sql_solo import ConectaSQL_Seguridad

def ExtraerCorreoUsuario(pUsuario):
    email = ""
    nombre = ""

    sqlT = f"select usuario_email,usuario_nombre from tusuarios where usuario_codigo='{pUsuario}' or usuario_email='{pUsuario}'"
    
    conn = ConectaSQL_Seguridad()

    cur_email = conn.cursor(as_dict = True)

    cur_email.execute(sqlT)

    for i in cur_email:
        email = i['usuario_email']
        nombre = i['usuario_nombre']
    
    if((email=="")or(email == None)):
        return {"Resultado":0,"Email":None,"Usuario":None}
    
    return {"Resultado":1,"Email": email,"Usuario":nombre}


def actualizarclave(pUsuario,pClaveE):
    valores = []
    valores.append(pClaveE)
    valores.append(pUsuario)
    
    sql = "update tUsuarios set usuario_password=%s where Usuario_Codigo=%s"

    meConexion = conexion_mssql()
    respuesta = meConexion.ejecutar_funciones(sql, tuple(valores))
    #print(sql)
    return respuesta

def enviarcorreo(pUsuario,pClave):
    try:
        usuario_correo = ExtraerCorreoUsuario(pUsuario)
        #print(usuario_correo['Email'])
        destinatario = usuario_correo['Email']
        nombre = usuario_correo['Usuario']
        remitente = 'notificaciones-swc@cantolonsafe.com'

        mensaje = f'Hola {nombre} \n Tu contraseña se ha reseteado debes usar la siguiente {pClave} \r Se requerira que cambie la contraseña. \n En caso de no haber solicitado por favor avisar al area de TI'

        correo = EmailMessage()
        correo['From'] = remitente
        correo['To'] = destinatario
        correo['Subject'] = 'Reseteo de contraseña - SWC'
        correo.set_content(mensaje)
                
        print("creando contexto seguro SSL...")
        Context = ssl.create_default_context()
        print("Seteando valores...")
        print("comenzando conexion...")        
        with smtplib.SMTP_SSL(host='mail.cantolonsafe.com',port=465) as smtp: 
            print("login...")
            smtp.login(remitente,'Zn&(WJ_F*HDc')
            print("enviando correo...=>")
            smtp.sendmail(remitente,destinatario,correo.as_string())
            print("cerrando conexion...")            
    except smtplib.SMTPException as errE :
        print(f"Error E : {errE}")
    except smtplib.SMTPDataError as errE :
        print(f"Error DE : {errE}")
    except smtplib.SMTPConnectError as errE :
        print(f"Error CE : {errE}")
    except Exception as err:
        print(f"Error General : {err}")        
        return f"Error al enviar el mensaje : {err}"
    finally:
        #smtp.quit()
        return f"Correo enviado..."

def generarclave(pUsuario):
    try:
        N = 7

        nueva = ''.join(random.choices(string.ascii_uppercase + string.digits, k = N))

        dc_contrasena = hashlib.md5(str(nueva).encode()).hexdigest()

        actualizarclave(pUsuario,dc_contrasena)

        print(pUsuario,nueva,dc_contrasena)

        resultado = enviarcorreo(pUsuario,nueva)        

        return resultado
    except Exception as err:
        print(err)
        return f"Error al generar : {err}"



