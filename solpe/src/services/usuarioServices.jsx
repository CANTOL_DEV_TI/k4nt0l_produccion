import {Url} from '../constants/global'

// const meServidorBackend = 'http://192.168.5.21:8080'
const meServidorBackend = Url 

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function getFilter_Usuario(txtFind) {
    if (txtFind.trim() === '') {
        txtFind = '%20'
    }
    
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/${txtFind}`,requestOptions)
    const responseJson = await response.json()    
    return responseJson
}


export async function ActPassword(meJson){   
    const requestOptions = {
        method: 'PUT',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/ActPassword/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function getEmpresas() {       
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/ListaEmpresas/`,requestOptions)
    const responseJson = await response.json()    
    return responseJson
}

export async function getEmpresasAsignadas(pUsuario) {       
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/CiaAsignadas/${pUsuario}`,requestOptions)
    const responseJson = await response.json()    
    return responseJson
}

export async function getListaModulos() {       
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/ListaModulos/`,requestOptions)
    const responseJson = await response.json()    
    return responseJson
}

export async function getListaSubModulos(pModuloP) {       
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/ListaSubModulos/${pModuloP}`,requestOptions)
    const responseJson = await response.json()    
    return responseJson
}

export async function getListaPermisosAsignados(pUsuario,pEmpresa) {
    const requestOptions = {
        method : 'GET',
        headers : cabecera
    }

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/ListaPermisos/${pUsuario}-${pEmpresa}/`,requestOptions)
    const responseJson = await response.json()
    return responseJson

}

export async function validaToken(meToken){
    try{
        var myHeaders = new Headers();
            myHeaders.append("Authorization", "Bearer " + meToken);

        const requestOptions = {
            method : 'POST',
            headers : myHeaders,        
        };

        const response = await fetch(`${meServidorBackend}/seguridad/usuario/validar_token/`, requestOptions);
        const responseJson = await response.json();
        return responseJson
    }
    catch{
        return false
    }

}

export async function validaAcceso(meJson){
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };
    
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/VerificaPermiso/`, requestOptions);
    const responseJson = await response.json();    
    return responseJson
}