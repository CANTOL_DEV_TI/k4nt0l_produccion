import React from "react";
import {GrEdit,GrAmazon,GrCodeSandbox,GrTask} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ListadoPermisosAsignados = ({modulo_padre_id,                        
                        padre,
                        modulo_id,                        
                        modulo_codigo,
                        modulo_nombre,
                        concedido}) =>
    (
        <tbody>
            <tr>

                <td className="td-cadena" id={modulo_padre_id}>{padre}</td>                
                <td className="td-cadena" id={modulo_id}>{modulo_codigo}</td>
                <td className="td-cadena" >{modulo_nombre}</td>
                <td className="td-cadena" >{concedido}</td>                
                
            </tr>
        </tbody>
    )

export default ListadoPermisosAsignados;