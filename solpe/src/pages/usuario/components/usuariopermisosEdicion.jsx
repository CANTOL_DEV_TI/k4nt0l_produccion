import React from "react";
import { BotonGuardar, BotonAdd, BotonEliminar, BotonConsultar } from "../../../components/Botones";
import { getEmpresasAsignadas, getListaModulos, getListaSubModulos, getListaPermisosAsignados, agregarAcceso, quitarAcceso } from "../../../services/usuarioServices";
import ListadoPermisosAsignados from "./ListadoPermisosAsignados";

class UsuarioPermisosEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { usuario_id: 0, usuario_codigo: '', empresa_id: 0, empresa_codigo: '', modulo_id: 0, ListAsignadas: [], ListModulos: [], ListSubModulos: [], ListaPermisosAsig: [] }
        } else {
            this.state = { usuario_id: this.props.dataToEdit.usuario_id, usuario_codigo: this.props.dataToEdit.usuario_codigo, empresa_id: this.props.dataToEdit.empresa_id, empresa_codigo: this.props.dataToEdit.empresa_codigo, modulo_id: this.props.dataToEdit.modulo_id, ListAsignadas: [], ListModulos: [], ListSubModulos: [], ListaPermisosAsig: [] }
        }
    }

    handleChange = async (e) => {
        
        if (e.target.name === 'usuario_id') {
            this.setState({ usuario_id: e.target.value })
        }

        if (e.target.name === 'EmpresasAsignadas') {
            const empresa = e.target.value
            const id = empresa.substr(3,1)
            const codigo = empresa.substr(0,3)            
            this.setState({ empresa_codigo: codigo })
            this.setState({ empresa_id: id })
        }

        if (e.target.name === 'Modulos') {
            console.log(e.target.value)
            this.setState({ modulo_id: e.target.value })                    
        }

        if (e.target.name === 'subModulos') {
            console.log(e.target.value)
            this.setState({ modulo_id: e.target.value })                    
        }



    }

    handleRefrescaSM = async (e) => {
        const ListaSM = await getListaSubModulos(this.state.modulo_id);
        this.setState({ ListSubModulos: ListaSM })        
    }


    handleRefrescaPermisos = async (e) => {
        const ListaP = await getListaPermisosAsignados(this.state.usuario_codigo, this.state.empresa_codigo);
        this.setState({ ListaPermisosAsig: ListaP })
    }

    handleAddPermiso = async (e) => {
        const data = this.state
        console.log(data)
        const addModulo = await agregarAcceso(data)
        console.log(addModulo)
        this.handleRefrescaPermisos()
    }

    handleDelPermiso = async (e) => {
        const data = this.state
        console.log(data)
        const delModulo = await quitarAcceso(data)
        console.log(delModulo)
        this.handleRefrescaPermisos()
    }

    render() {
        const { eventOnclick, nameOpe, ListEmpresas, ListModulos, ListSubModulos, ListaPermisosAsig } = this.props

        return (
            <div className="card cardalign w-75rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-2 col-form-label">ID : </div>
                            <div className="col-sm-2 col-form-label"><input type="number" min={0} max={9999} name="usuario_id" autoFocus onChange={this.handleChange} value={this.state.usuario_id} /></div>
                            <div className="col-sm-2 col-form-label">Codigo : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="usuario_codigo" onChange={this.handleChange} value={this.state.usuario_codigo} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                    </div>
                </div>
                
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group row">
                        <div className="col-sm-2 col-form-label">Empresas : </div>                                                
                        <div className="col-sm-2 col-form-label">Modulos : </div>  
                        <div className="col-sm-1 col-form-label"></div>                      
                        <div className="col-sm-1 col-form-label"></div>  
                        <div className="col-sm-3 col-form-label">SubModulos : </div>                                                                       
                     </div>   
                    <div className="form-group row">
                        <div className="col-sm-2 col-form-label">
                            <select name="EmpresasAsignadas" size={3} onChange={this.handleChange}>
                                {
                                    this.state.ListAsignadas.map((v_asignadas) =>
                                        <option key={v_asignadas.empresa_codigo + v_asignadas.empresa_id} value={v_asignadas.empresa_codigo + v_asignadas.empresa_id}>{`${v_asignadas.empresa_nombre}`}</option>
                                    )
                                }
                            </select>
                        </div>
                        <div className="col-sm-2 col-form-label">
                            <select name="Modulos" size={6} onChange={this.handleChange}>
                                {
                                    this.state.ListModulos.map((v_modulos) =>
                                        <option key={v_modulos.modulo_id} value={v_modulos.modulo_id}>{`${v_modulos.modulo_nombre}`}</option>
                                    )
                                }
                            </select>
                            <BotonConsultar textoBoton={"Ver SubModulos"} sw_habilitado={true} eventoClick={this.handleRefrescaSM} />
                        </div>                        
                        <div className="col-sm-1 col-form-label"></div>
                        <div className="col-sm-1 col-form-label"></div>
                        <div className="col-sm-3 col-form-label">
                            <select name="subModulos" size={10} onChange={this.handleChange} >
                                {
                                    this.state.ListSubModulos.map((v_submodulos) =>
                                        <option key={v_submodulos.modulo_id} value={v_submodulos.modulo_id}>{`${v_submodulos.modulo_nombre}`}</option>
                                    )
                                }
                            </select>

                            <BotonAdd textoBoton="Agregar Acceso" eventoClick={this.handleAddPermiso} />
                            <BotonEliminar textoBoton="Quitar Acceso" sw_habilitado={true} eventoClick={this.handleDelPermiso} />
                        </div>
                    </div>                                            
                </div>
                <BotonConsultar textoBoton={"Ver Permisos"} sw_habilitado={true} eventoClick={this.handleRefrescaPermisos} />
                <div className="table-responsive" style={{ height: '200px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>                                
                                <th className="align-middle">Padre</th>
                                <th className="align-middle">Modulo Codigo</th>
                                <th className="align-middle">Modulo Nombre</th>                                
                                <th className="align-middle">Acceso</th>                                
                            </tr>
                        </thead>
                        
                        {this.state.ListaPermisosAsig.map((registro) =>
                            <ListadoPermisosAsignados
                                //key={registro.usuario_id}                                
                                modulo_padre_id={registro.modulo_padre_id}
                                padre={registro.padre}    
                                modulo_codigo={registro.modulo_codigo}                            
                                modulo_nombre={registro.modulo_nombre}
                                concedido={registro.concedido}                                
                            />
                        )}
                    </table>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "usuario_id": this.state.usuario_id, "usuario_codigo": this.state.usuario_codigo, "empresa_id": this.state.empresa_id })} texto={nameOpe} />
            </div >
        )
    }
    async componentDidMount() {
        const ListaEmpresasAsig = await getEmpresasAsignadas(this.state.usuario_id);
        this.setState({ ListAsignadas: ListaEmpresasAsig })
        console.log(this.state.ListAsignadas)

        const ListaModulos = await getListaModulos();
        this.setState({ ListModulos: ListaModulos })
        console.log(this.state.ListModulos)
        /*
                const ListaSubM = await getListaSubModulos(1);
                this.setState({ ListSubModulos: ListaSubM })
                console.log(this.state.ListSubModulos)
        */
    }
}

export default UsuarioPermisosEdicion;