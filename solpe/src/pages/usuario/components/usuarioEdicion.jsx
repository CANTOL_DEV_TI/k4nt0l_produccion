import React from "react";
import { BotonGuardar } from "../../../components/Botones";

class UsuarioEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { usuario_id : 0, usuario_codigo : '', usuario_nombre : '', usuario_estado: ''}
        } else {
            this.state = { usuario_id : this.props.dataToEdit.usuario_id, usuario_codigo: this.props.dataToEdit.usuario_codigo, usuario_nombre: this.props.dataToEdit.usuario_nombre, usuario_estado: this.props.dataToEdit.usuario_estado}
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'usuario_id') {
            this.setState({ usuario_id: e.target.value })
        }

        if (e.target.name === 'usuario_codigo') {
            this.setState({ usuario_codigo: e.target.value })
        }

        if (e.target.name === 'usuario_nombre') {
            this.setState({ usuario_nombre: e.target.value })
        }

        if (e.target.name === 'usuario_estado') {
            this.setState({ usuario_estado: e.target.value })
        }        
    }


    render() {
        const { eventOnclick, nameOpe, listSubAreas, listTiposEquipo } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">ID : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="usuario_id" autoFocus onChange={this.handleChange} value={this.state.usuario_id} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>

                        <div>
                            <div className="form-group row">
                                <div className="col-sm-3 col-form-label">Codigo : </div>
                                <div className="col-sm-2 col-form-label"><input type="text" name="usuario_codigo" onChange={this.handleChange} value={this.state.usuario_codigo} /></div>
                                <div className="col-sm-2 col-form-label"></div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-3 col-form-label">Nombre : </div>
                                <div className="col-sm-2 col-form-label"><input type="text" name="usuario_nombre" onChange={this.handleChange} value={this.state.usuario_nombre} /></div>
                                <div className="col-sm-2 col-form-label"></div>
                            </div>                            
                        </div>
                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "usuario_id": this.state.usuario_id, "usuario_codigo": this.state.usuario_codigo, "usuario_nombre": this.state.usuario_nombre, "usuario_estado": this.state.usuario_estado})} texto={nameOpe} />
            </div>
        )
    }

}

export default UsuarioEdicion;