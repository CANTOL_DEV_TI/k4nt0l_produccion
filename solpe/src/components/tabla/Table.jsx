import TableBody from "./TableBody";
import TableHead from "./TableHead";
import { useSortableTable } from "./useSortableTable";
import "./table.css";

const Table = ({ data, columns }) => {
  const [tableData, handleSorting] = useSortableTable(data, columns);
  return (
    <>
      <div className="table-responsive">
        <table className="table table-hover table-sm table-bordered">
          <TableHead {...{ columns, handleSorting }} />
          <TableBody {...{ columns, tableData }} />
        </table>
      </div>
    </>
  );
};

export default Table;