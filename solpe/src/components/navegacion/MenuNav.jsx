import { NavLink, Link } from "react-router-dom";
import './menuNav.css'
import logo from "./assets/cantol.png";

const MenuNav = () => {
    return (
        <div className="menutop sticky-top mb-2">
            <div className="menu-cab">
                <div className="nav-logo">
                    <NavLink to="/">
                        <img src={logo} width="110" alt="ERP Cantol" className="d-inline-block align-top ms-2" />
                    </NavLink>
                </div>
            </div>
            <input type="checkbox" id="nav-check" />
            <div className="nav-btn">
                <label htmlFor="nav-check">
                    <span></span>
                    <span></span>
                    <span></span>
                </label>
            </div>

            <div className="nav-links">
                <NavLink to="/turno">Turno Laboral</NavLink>
                <NavLink to="/articulo_omitido">Artículo Omitido</NavLink>
                <NavLink to="/produccion_pendiente">Producción Pendiente</NavLink>
                <NavLink to="/periodo_laboral">Período Laboral</NavLink>
                <NavLink to="/articulo_plano">Lista de planos</NavLink>
                <a href="../" >Ir a Principal</a>
            </div>
        </div>
    );
};

export default MenuNav;

/* VERSION 02
    <div className="menutop sticky-top mb-2">
        <div className="menu-cab">
            <div className="nav-logo">
                <NavLink to="/">
                    <img src={logo} width="100" alt="ERP Cantol" className="d-inline-block align-top ms-2" />
                </NavLink>
            </div>
        </div>
        <input type="checkbox" id="nav-check" />
        <div className="nav-btn">
            <label htmlFor="nav-check">
                <span></span>
                <span></span>
                <span></span>
            </label>
        </div>

        <div className="nav-links">
            <NavLink to="/presupuesto_crear" >Cargar Presupesto</NavLink>
            <NavLink to="/presupuesto_aprobar" >Aprobar Presupuesto</NavLink>
            <NavLink to="/presupuesto_ejercicio" >Consulta Anual</NavLink>
            <NavLink to="/presupuesto_periodo" >Consulta Mensual</NavLink>
            <Link to="../" >Ir a Principal</Link>
        </div>
    </div>

    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand >
                        <NavLink to="/">
                            <img src={logo} width="100" alt="ERP Cantol" className="d-inline-block align-top ms-2" />
                        </NavLink>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                            <NavLink className="nav-link" to="/presupuesto_crear" >Cargar Presupesto</NavLink>
                            <NavLink className="nav-link" to="/presupuesto_aprobar" >Aprobar Presupuesto</NavLink>
                            <NavDropdown title="Consulta Presupuesto" id="collasible-nav-dropdown">
                                <NavLink className="dropdown-item" to="/presupuesto_ejercicio" >Consulta Anual</NavLink>
                                <NavLink className="dropdown-item" to="/presupuesto_periodo" >Consulta Mensual</NavLink>
                                <NavDropdown.Divider />
                                <NavLink className="dropdown-item" to="/otra_consulta" >Otros</NavLink>
                            </NavDropdown>
                        </Nav>
                        <a className="nav-link" href="../">Ir a Principal</a>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
*/