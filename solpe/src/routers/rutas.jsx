import { Routes, Route, } from "react-router-dom";
import React from "react";


import LoginV from "../pages/login/login.jsx";
import ActuaPassword from "../pages/login/cambiocontraseña.jsx";


export function MiRutas() {
    return (
        <Routes>
            <Route path="/login" element={<LoginV />} />   
            <Route path="/cambiocontrasena" element={<ActuaPassword />}/>
        </Routes>
    );
}