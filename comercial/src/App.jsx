import { HashRouter } from "react-router-dom";
import NavBarV1 from './components/navegacion/NavbarV1/NavBarV1';
import { MiRutas } from './routers/rutas';

function App() {
return (
    <>
      {/* <HashRouter basename="produccion"> */}
      {/* <HashRouter basename="comercial"> */}
      <HashRouter>
      {/* <HashRouter> */}
          <div>
          <NavBarV1 />
            <div>
              <MiRutas />                    
            </div>
          </div>
      </HashRouter>
    </>
  )
}

export default App
