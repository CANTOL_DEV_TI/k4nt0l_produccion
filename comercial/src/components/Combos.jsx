//por defecto el combo debe recibir id, nombre
export const ComboBox = ({ datosRow, nombre_cbo, manejaEvento, valor, valor_ini, disabled=false, fortable=false}) => {
    return (
        <>
            <select className={`${fortable?"tw-w-44 tw-border tw-border-gray-500 tw-rounded-md":"form-select"}`} id={nombre_cbo} name={nombre_cbo} value={valor} onChange={(x)=>{manejaEvento(x)}} disabled={disabled}>
                <>
                    <option value={'-1'} disabled={false}>
                        {valor_ini}
                    </option>
                    {datosRow?.map((data, index) => {
                    return (
                        <option key={index + 1}  value={data?.canal_code || data?.id}>
                            {data?.canal_name || data?.nombre}
                        </option>
                        );
                    })}
                </>
            </select>
        </>
    );
};