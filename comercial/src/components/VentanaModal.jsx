import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import React from 'react'

const VentanaModal = ({ children, show, handleClose, titulo, size }) => {
    return (
        <Modal show={show} onHide={handleClose} size={size}> { /*backdrop="static" keyboard={false} */}
            { /*size="sm,lg,xl" */}
            <Modal.Header closeButton>
                <Modal.Title>{titulo}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {children}
            </Modal.Body>
            {/* <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
            </Modal.Footer>  */}
        </Modal>
    );
};


const VentanaModal_Cuotas = ({ children, show, handleClose, titulo, size, feet }) => {
    return (
        <Modal show={show} onHide={handleClose} size={size} backdrop="static" keyboard={false}> { /*backdrop="static" keyboard={false} */}
            { /*size="sm,lg,xl" */}
            <Modal.Header closeButton>
                <Modal.Title>{titulo}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {children}
            </Modal.Body>
            {feet && (
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Regresar
                    </Button>
                </Modal.Footer> 
            )
            }
        </Modal>
    );
};



export { VentanaModal,  VentanaModal_Cuotas}