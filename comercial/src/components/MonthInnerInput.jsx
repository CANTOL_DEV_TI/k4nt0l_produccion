import { useState } from 'react';
import { MonthPicker, MonthInput } from 'react-lite-month-picker';

function MonthInnerInput({monthData, setMonthData}) {
    const [isPickerOpen, setIsPickerOpen] = useState(false);
  return (
    <>
    <div>
      <MonthInput
        selected={monthData}
        setShowMonthPicker={setIsPickerOpen}
        showMonthPicker={isPickerOpen}
        lang='es'
        size='small'
      />
      {isPickerOpen ? (
        <MonthPicker
          setIsOpen={setIsPickerOpen}
          selected={monthData}
          onChange={setMonthData}
          lang='es'
          size='small'
        />
      ) : null}
    </div>
  </>
  );
}

export default MonthInnerInput
