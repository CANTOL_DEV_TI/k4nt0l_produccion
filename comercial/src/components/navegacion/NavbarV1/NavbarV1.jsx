import React, { useContext, useState } from "react";
import { useMediaQuery } from "react-responsive";
import { NavLink } from "react-router-dom";
import meLogo from '../assets/cantol.png';
import '../style/NavbarV1.css'
import '../static/fonts.css'
// import { ProductionContext } from "../../../Context/ProduccionContext";

function NavBarV1() {
    const [activeNav, setActiveNav] = useState("nav_menu");
    const isReprocesoMobil = useMediaQuery({ query: '(max-width: 800px)' })
    const [activeChildren, setActiveChildren] = useState("children")
    
    // const {
    //     logoC
    //     } = useContext(ProductionContext);

    const navToggle = () => {
        activeNav === "nav_menu"
            ? setActiveNav("nav_menu nav_active")
            : setActiveNav("nav_menu");
    };

    const childrenToggle = () => {
        activeChildren === "children"
            ? setActiveChildren("children children_active")
            : setActiveChildren("children");

        console.log(activeChildren)
    };

    const handleClear=()=>{
        sessionStorage.clear("")
    }
    // {/* <a href="#" className={"bt-menu"}><span className={"icon-menu"} onClick={navToggle}></span><img src={logoC} className="LogoEmpresa" /></a> */}

  return (
        <header>
            <div className={"menu_bar"} >
                <a href="/" className={"bt-menu"}><span className={"icon-menu"} onClick={()=>{console.log("Hola Mundo")}}/><img src={meLogo} className="LogoEmpresa" /></a>
            </div>

            <nav className={activeNav}>
                <ul>
                    <li className={"submenu"}>
                        <NavLink to="/" onClick={()=>{childrenToggle;}}><span className={"icon-rocket"}/>Aplicaciones<span className={"caret icon-chevron-thin-down"}></span></NavLink>
                        <ul className={activeChildren}>
                            <li className={isReprocesoMobil?"submenu":"parent"}><NavLink to="/reportes" onClick={childrenToggle}>Reportes<span className="expand reprocesoflecha"></span></NavLink> 
                                <ul className={isReprocesoMobil?activeChildren:"child"}>
                                    {/* <li><NavLink to="/reproceso/simular">Simulación</NavLink></li>
                                    <li><NavLink to="/reproceso/aprobar">Aprobaciones</NavLink></li> */}
                                </ul>
                            </li>
                            {/* <li className={isReprocesoMobil?"submenu":"parent"}><NavLink to="/reproceso" onClick={childrenToggle}>Ventas<span className="expand reprocesoflecha"></span></NavLink>  */}
                            <li className={isReprocesoMobil?"submenu":"parent"}><NavLink to="/ventas" onClick={childrenToggle}>Ventas<span className="expand reprocesoflecha"></span></NavLink> 
                                <ul className={isReprocesoMobil?activeChildren:"child"}>
                                    <li><NavLink to="/ventas/cuotas">Cuotas</NavLink></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li className={"submenu"}>
                    <NavLink to="/" onClick={childrenToggle}><span className={"icon-briefcase"}></span>Maestros<span className={"caret icon-chevron-thin-down"}></span></NavLink>
                        {/*<ul className={'children'}>*/}
                        <ul className={activeChildren}>
                            {/* <li><NavLink to="/periodo_laboral">Periodo Laboral<span className={"icon-dot-single"}></span></NavLink></li>
                            <li><NavLink to="/articuloFamilia">Artículo<span className={"icon-dot-single"}></span></NavLink></li> */}
                        </ul>
                    </li>
                    <li className={"submenu"}>
                        <NavLink to="#" onClick={childrenToggle}><span className={"icon-user"}></span>Usuario<span className={"caret icon-chevron-thin-down"}></span></NavLink>
                        {/*<ul className={'children'}>*/}
                        <ul className={activeChildren}>
                            <li><NavLink to="/cambiocontrasena">Cambio de Contraseña<span className={"icon-dot-single"}></span></NavLink></li>                            
                            <li><NavLink to="/login">Inicio de Sesion<span className={"icon-dot-single"}></span></NavLink></li>
                        </ul>
                    </li>
                    {/* <li><NavLink to="http://192.168.5.21/" onClick={handleClear}><span className={"icon-mail"}></span>Salir</NavLink></li> */}
                    <li><a href="http://192.168.5.21/" onClick={handleClear}><span className={"icon-mail"}></span>Salir</a></li>

                </ul>

            </nav>
        </header>
    );
}

export default NavBarV1
