// import { BsCheckSquare, BsPlus, BsPlusSquare, BsSquare, BsX } from "react-icons/bs";
import { BsX } from "react-icons/bs";
import { GoFoldDown } from "react-icons/go";
import { sumArray, truncate } from "../utils/math";
import { ComboBox } from "./Combos";
// import { Button } from "react-bootstrap";


function Tabla_Plan_Venta({ datos_fila, manejaHabilitar, manejaModalCuotas }) {
    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle tw-w-12">Id</th>
                            <th className="align-middle tw-w-56">Ejercicio</th>
                            <th className="align-middle tw-w-56">Periodo</th>
                            <th className="align-middle">Descripción</th>
                            <th className="align-middle tw-w-56">Total Plan Ventas</th>
                            {/* <th className="align-middle tw-w-28">Habilitar</th> */}
                            <th className="align-middle tw-w-28">Cuotas</th>

                        </tr>
                    </thead>
                    <tbody className="list">
                            {datos_fila.length > 0 ?
                            (   
                                datos_fila.map((dato, index) => {
                                    return(
                                        <tr key={index + 1}>
                                        <td className="td-cadena tw-text-lg">{index.toString()}</td>
                                        <td className="td-cadena tw-text-lg">{dato.ejercicio}</td>
                                        <td className="td-cadena tw-text-lg">{dato.periodo}</td>
                                        <td className="td-cadena tw-text-lg">{dato.plan.toUpperCase()}</td>
                                        <td className="td-cadena tw-text-lg">{dato.total.toLocaleString()}</td>
                                        {/* <td className="td-cadena ">
                                            <button data-toggle="tooltip" 
                                            title="Simular Reproceso" 
                                            className="btn btn-outline-dark btn-sm ms-1"
                                            onClick={()=>{manejaHabilitar(dato.id)}}>
                                                {dato.habilitado?<BsCheckSquare />:<BsSquare />}
                                            </button>
                                        </td> */}
                                        <td className='d-flex justify-content-center'>
                                            <button data-toggle="tooltip" 
                                            title="Simular Reproceso" 
                                            className="btn btn-outline-dark btn-sm ms-1"
                                            // onClick={()=>{manejaModalCuotas(true, dato.plan.toUpperCase(), {canal_id: dato.canal_id, periodo: `${dato.ejercicio}-${dato.periodo.toString().padStart(2,'0')}`})}}>
                                            onClick={()=>{manejaModalCuotas(true, dato.plan.toUpperCase(), {canal_id: dato.canal_id, periodo: `${dato.ejercicio}-${dato.periodo.toString()}`})}}>
                                                <GoFoldDown/>
                                            </button>
                                        </td>
                                        </tr>
                                        )
                                }
                            )
                        ) :
                        (
                            <tr>
                                <td className="text-center" colSpan="10">Sin datos</td>
                            </tr>
                        )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}

// function Tabla_Cuotas({ datos_fila, manejaHabilitar, manejaModalCuotas }) {
function Tabla_Cuotas({ datos_fila }) {
    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle tw-w-12">Id</th>
                            <th className="align-middle tw-w-56">Vendedor</th>
                            <th className="align-middle tw-w-56">Porcentaje (%)</th>
                            <th className="align-middle">Cuota</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                            {datos_fila.length > 0 ?
                            (  
                                <>
                                {
                                    datos_fila.map((dato, index) => {
                                        return(
                                            <tr key={index + 1}>
                                            <td className="td-cadena tw-text-lg">{(index+1).toString()}</td>
                                            <td className="td-cadena tw-text-lg">{dato.vendedor}</td>
                                            <td className="td-cadena tw-text-lg">{dato.porcentaje}</td>
                                            <td className="td-cadena tw-text-lg">{dato.zona_cuota.toLocaleString()}</td>
                                            </tr>
                                            )
                                        }
                                    )
                                }
                                <tr>
                                <td className="td-cadena tw-text-lg">{''}</td>
                                <td className="td-cadena tw-text-lg">{''}</td>
                                <td className="td-cadena tw-text-lg">{'Total:'}</td>
                                <td className="td-cadena tw-text-lg">{sumArray(datos_fila.map((item)=>(Number(item.zona_cuota)))).toLocaleString()}</td>
                                </tr>
                                </>
                            ) 
                        :
                        (
                            <tr>
                                <td className="text-center" colSpan="10">Sin datos</td>
                            </tr>
                        )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}

function Tabla_Nueva_Cuota_Ferreteros({ datos_fila , manejaEvento, manejaCuota, manejaNuevaFila, fortable = false, valorCuotaTotal, isAmount}) {
    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle tw-w-20">ID Zona</th>
                            <th className="align-middle tw-w-72">Zona</th>
                            <th className="align-middle tw-w-56">Vendedor</th>
                            <th className="align-middle">{`Cuota (${isAmount ? 'S/.' : '%'})`}</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                            {datos_fila.length > 0 ?
                            (  
                                <>
                                {
                                    datos_fila.map((dato, index) => {
                                        return(
                                            <tr key={index + 1}>
                                            <td className="td-cadena tw-text-lg">{dato.idzona}</td>
                                            <td className="td-cadena tw-text-lg">{dato.zona}</td>
                                            <td className="td-cadena tw-text-md">
                                                <ComboBox
                                                    datosRow={dato.vendedor.vendedores}
                                                    nombre_cbo="xxx"
                                                    manejaEvento={(x)=>{manejaEvento(x, dato.idzona)}}
                                                    valor_ini={"Vendedor"}
                                                    valor={dato.vendedor.index}
                                                    fortable={fortable}
                                                />
                                            </td>
                                            <td className="td-cadena tw-text-lg">                                                
                                                <input
                                                    type="number"
                                                    min="0"
                                                    // max="100"
                                                    step="any"
                                                    value={dato.cuota}
                                                    onChange={(x)=>{manejaCuota(x, dato.idzona)}}
                                                    className={`tw-w-32 tw-border tw-border-gray-500 tw-rounded-md tw-text-center ${dato.vendedor.index === '-1' && 'tw-bg-gray-400'}`}
                                                    disabled={dato.vendedor.index === '-1' && true}
                                                />
                                            </td>
                                            </tr>
                                            )
                                        }
                                    )
                                }
                                <tr>
                                <td className="td-cadena tw-text-lg">{''}</td>
                                <td className="td-cadena tw-text-lg">{''}</td>
                                <td className="td-cadena tw-text-lg">{'Total:'}</td>
                                <td className="td-cadena tw-text-lg">{`${isAmount ? 'S/. ' : ''}${truncate(valorCuotaTotal, 4)}${isAmount ? '' : ' %'}`}</td>
                                </tr>
                                {/* <tr>
                                     <td colSpan="4" style={{ textAlign: 'center'}}>
                                        <button data-toggle="tooltip" 
                                                type="button" 
                                                className="btn btn-outline-dark btn-sm ms-1"
                                                    <BsPlus/>
                                        </button>
                                    </td>
                                </tr> */}
                                </>
                            ) 
                        :
                        (
                            <tr>
                                <td className="text-center" colSpan="10">Sin datos</td>
                            </tr>
                        )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}

function Tabla_Nueva_Cuota_No_Ferreteros({ datos_fila , manejaCuota, valorCuotaTotal, isAmount, deleteSeller}) {
    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle tw-w-20">N°</th>
                            <th className="align-middle tw-w-[420px]">Vendedor</th>
                            <th className="align-middle">{`Cuota (${isAmount ? 'S/.' : '%'})`}</th>
                            <th className="align-middle">❌</th>
                        </tr>
                    </thead>
                    <tbody className="list">
                            {datos_fila.length > 0 ?
                            (  
                                <>
                                {
                                    datos_fila.map((dato, index) => {
                                        return(
                                            <tr key={index + 1}>
                                            <td className="td-cadena tw-text-lg">{index + 1}</td>
                                            <td className="td-cadena tw-text-md">{dato?.vendedor?.vendedores[0]?.nombre}</td>
                                            <td className="td-cadena tw-text-lg">                                                
                                                <input
                                                    // type={'text'}
                                                    type="number"
                                                    min="0"
                                                    // max="100"
                                                    // step="0.0001"
                                                    step="any"
                                                    // value={dato.cuota.toString()}
                                                    value={dato.cuota}
                                                    onChange={(x)=>{manejaCuota(x, dato.idzona)}}
                                                    className={`tw-w-32 tw-border tw-border-gray-500 tw-rounded-md tw-text-center ${dato.vendedor.index === '-1' && 'tw-bg-gray-400'}`}
                                                    disabled={dato.vendedor.index === '-1' && true}
                                                />
                                            </td>
                                            <td className="td-cadena tw-text-md">
                                                <button data-toggle="tooltip"
                                                    type="button" 
                                                    onClick={()=>deleteSeller(dato?.vendedor?.index)}
                                                    className="btn btn-outline-dark btn-sm ms-1">
                                                        <BsX/>
                                                </button>
                                            </td>
                                            </tr>
                                            )
                                        }
                                    )
                                }
                                <tr>
                                <td className="td-cadena tw-text-lg">{''}</td>
                                <td className="td-cadena tw-text-lg">{'Total:'}</td>
                                <td className="td-cadena tw-text-lg">{`${isAmount ? 'S/. ' : ''}${truncate(valorCuotaTotal, 4)}${isAmount ? '' : ' %'}`}</td>
                                </tr>
                                </>
                            ) 
                            :
                        (
                            <tr>
                                <td className="text-center" colSpan="10">Sin datos</td>
                            </tr>
                        )
                        }
                    </tbody>
                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}


// {/* <ComboBox
//     datosRow={dato.vendedor.vendedores}
//     nombre_cbo="xxx"
//     manejaEvento={(x)=>{manejaEvento(x, dato.idzona)}}
//     valor_ini={"Vendedor"}
//     valor={dato.vendedor.index}
//     fortable={fortable}
// /> */}

// {/* <tr>
//      <td colSpan="4" style={{ textAlign: 'center'}}>
//         <button data-toggle="tooltip" 
//                 type="button" 
//                 className="btn btn-outline-dark btn-sm ms-1"
//                 onClick={()=>{manejaNuevaFila()}}>
//                     <BsPlus/>
//         </button>
//     </td>
// </tr> */}
export {Tabla_Plan_Venta, Tabla_Cuotas, Tabla_Nueva_Cuota_Ferreteros, Tabla_Nueva_Cuota_No_Ferreteros}