import { useContext } from 'react'
import {Url} from '../constants/global'

// const meServidorBackend = 'http://192.168.5.21:8080'
const meServidorBackend = Url

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function Login(pLogin,pPass,pCia)
{ 

    const data = ({usuario_login : pLogin, usuario_contraseña : pPass, usuario_empresa : pCia})

    const requestOptions = {
        method: 'POST',
        headers: cabecera,//{ 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    };
    //console.log(requestOptions)

    const respuesta = await fetch(`${meServidorBackend}/login/`,requestOptions)
    const responseJson = await respuesta.json()
    return responseJson
}