import axios from "axios";
import { Url } from "../constants/global";
// const qs = require('qs')

async function getCanalesVenta() {
    try{
        const response = await axios.get(`${Url}/comercial/cuotas/canales_venta`);
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getCanalesVenta) _ ${error}`);
        return error?.response?.status
    }
}

async function getPlanesVenta(params) {
    try{
        const response = await axios.get(`${Url}/comercial/cuotas/plan_venta_canal`,  {
            params
        });

        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getPlanesVenta) _ ${error}`);
        return error?.response?.status
    }
}

async function updatehabilitarPlan(body) {
    try{
        const response = await axios.patch(`${Url}/comercial/cuotas/habilitar`, body);
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getPlanesVenta) _ ${error}`);
        return error?.response?.status
    }
}

async function getCuotas(params) {
    try{
        const response = await axios.get(`${Url}/comercial/cuotas`, {
            params
        });
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getPlanesVenta) _ ${error}`);
        return error?.response?.status
    }
}

async function fetchDataTable(params) {
    try{
        const response = await axios.get(`${Url}/comercial/cuotas/listarzona_vendedor`, {
            params
        });
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getPlanesVenta) _ ${error}`);
        return error?.response?.status
    }
}

async function postPlanVentaCuota(body) {
    try{
        const response = await axios.post(`${Url}/comercial/cuotas/crear_plan_cuota`, body);
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        if(error.response){
            return error.response.data;
        }else if(error.request){
            return {error: 'El servidor no responde.'};
        }else{
            return {error: 'Error indeterminado.'};
        }
    }
}

async function validar_cuota_canal(params) {
    try{
        const response = await axios.get(`${Url}/comercial/cuotas/validar_cuota_canal`, {
            params
        });
        if (!!response.data && response.status === 200){
            return {message: !response.data.message, status: response.status};
        }else
        {
            return [];
        }
    }catch(error){
        if(error.response){
            return error.response.data;
        }else if(error.request){
            return {error: 'El servidor no responde.'};
        }else{
            return {error: 'Error indeterminado.'};
        }
    }
}

// paramsSerializer: params => {
//     return qs.stringify(params, { arrayFormat: 'brackets' });
// }

async function get_zona_defecto(params) {
    try{
        const response = await axios.get(`${Url}/comercial/cuotas/zona_vendedor_defecto?${params.toString() || "lista_codigo=-1"}`);
        if (!!response.data && response.status === 200){
            return {message: response.data.message, status: response.status};
        }else
        {
            return [];
        }
    }catch(error){
        if(error.response){
            return error.response.data;
        }else if(error.request){
            return {error: 'El servidor no responde.'};
        }else{
            return {error: 'Error indeterminado.'};
        }
    }
}

export { getCanalesVenta, getPlanesVenta, updatehabilitarPlan, getCuotas, fetchDataTable, postPlanVentaCuota, validar_cuota_canal, get_zona_defecto}