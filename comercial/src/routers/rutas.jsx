import { Routes, Route, Outlet} from "react-router-dom";
import ProtectedRoute from "../components/utils/ProtectedRoute";
import ActuaPassword from "../pages/login/cambiocontrasena";
import LoginV from "../pages/login/login";
import Planventa from "../pages/ventas/Planventa";

export function MiRutas() {
    return (
        <Routes>
            <Route path="/" element={<LoginV />} />
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/cambiocontrasena" element={<ActuaPassword />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/ventas" element={<div><Outlet/></div>} >
                    <Route path="cuotas" element={<Planventa />}/>
                </Route>
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/reportes" element={<div>Ventana Reportes</div>} />
            </Route>
        </Routes>
    );
}