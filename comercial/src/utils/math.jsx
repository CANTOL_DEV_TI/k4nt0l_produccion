
export function sumArray(arr){
    const sum = arr.reduce((accumulator, currentValue) => {
        return accumulator + currentValue;
    }, 0);
    return sum;
}

export function truncate(number, decimals){
    let decima = Math.pow(10, decimals)
    if(Number.isInteger(decimals)){
        return (Math.floor(number * decima) / decima);
    }else{
        return null;
    }
}

export function cuotaTotal(factor, total){
    return  truncate((factor*0.01) * total,3);
}

export function calculatePercentage(cuota, total){
    let percentage = ( parseFloat(parseFloat(cuota).toFixed(5)) / parseFloat(parseFloat(total).toFixed(5)) ) * 100.000;
    return truncate(percentage, 3);
}