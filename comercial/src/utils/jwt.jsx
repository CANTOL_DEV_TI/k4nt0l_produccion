import { jwtDecode } from "jwt-decode";

export function getUserJWT(token){
    const decoded = jwtDecode(token);
    return decoded.sub.replace(sessionStorage.getItem('Empresa'),"")
}