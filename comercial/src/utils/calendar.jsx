

export const getCurrentMonthYear = () => {
    const today = new Date();
    
    const year = today.getFullYear(); 
    const month = today.getMonth() + 1; 
    return {month: month, year: year}
}

export const getCurrentDate = () => {
    const date = new Date();
    // const year = date.getFullYear().toString().slice(-2); // Get last 2 digits of the year
    const year = date.getFullYear().toString()
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Get month and pad with leading zero if needed
    const day = String(date.getDate()).padStart(2, '0'); // Get day and pad with leading zero if needed
    return `${year}-${month}-${day}`;
}