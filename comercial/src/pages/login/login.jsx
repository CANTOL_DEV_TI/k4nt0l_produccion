import React from "react";
import { Login } from "../../services/loginServices";
import { BotonLogin } from "../../components/Botones";
import Bienvenido from "./bienvenido";
import { NavLink } from "react-router-dom";
import { VentanaModal } from "../../components/VentanaModal";
// import { ProductionContext } from "../../Context/ProduccionContext";

class LoginV extends React.Component {
    // static contextType = ProductionContext
    constructor(props) {
        super(props);
        this.state = { usuario_login: '', password_login: '', cia_login: '', showModal: false, Titulo: "", Mensaje: "" }
    }
    
    handleChange = async (e) => {
        if (e.target.name === 'txtLogin') {
            this.setState({ usuario_login: e.target.value })
        }

        if (e.target.name === 'txtPassword') {
            this.setState({ password_login: e.target.value })
        }

        if (e.target.name === 'cboCia') {
            this.setState({ cia_login: e.target.value })
        }

    }

    handleSubmit = async () => {        
        const responseJson = await Login(this.state.usuario_login, this.state.password_login, this.state.cia_login);
        // console.log(responseJson)
        if (!responseJson.detail) {
            // sessionStorage.setItem("Logo", this.state.cia_login)
            // this.context.refresh_Logo(sessionStorage.getItem("Logo"));
            sessionStorage.setItem("CDTToken", responseJson);
            sessionStorage.setItem("Empresa", this.state.cia_login);
            this.setState({ Mensaje: "Bienvenido " + this.state.usuario_login});
            this.setState({ Titulo: "Sistema Web CANTOL" });            
        } else {            
            sessionStorage.setItem("CDTToken", "");
            this.setState({ Titulo: "Error al iniciar" });
            this.setState({ Mensaje: responseJson.detail });
            this.setState({ showModal: true, tipoOpe: 'Find' })            
        }
                            
    }

    handleClose = async (e) => {
        this.setState({ showModal: false })
    }

    render() {
        const { Titulo, Mensaje } = this.state

        return (
            <React.Fragment>
                <div className="container text-center mt-5">
                    <div className="row justify-content-center">
                        <div className="col-xl-4 col-lg-6">
                            <div className="card mt-4">
                                <div className="card-body p-4 bg-exclamation">
                                    {/*<img src={logocantol} className="img-fluid" width="164" />*/}
                                    <h1 className="h3 mb-3 fw-normal">Ingresar a ERP Cantol.</h1>
                                    <div className="form">
                                        <select className="form-select form-select-lg mb-3" aria-label=".form-select-lg example" name="cboCia" onChange={this.handleChange}>
                                            <option value="NON">Empresa</option>
                                            <option value="TCN">Tecnopress</option>
                                            <option value="CNT">Cantol</option>
                                            <option value="DTM">Distrimax</option>
                                        </select>
                                    </div>
                                    <div className="form-floating">
                                        <p />
                                    </div>
                                    <div className="form-floating">
                                        <input type="text" name="txtLogin" className="form-control" id="floatingInput" placeholder="Usuario..." autoFocus onChange={this.handleChange} value={this.state.usuario_login} />
                                        <label>Usuario</label>
                                    </div>
                                    <div className="form-floating">
                                        <p />
                                    </div>
                                    <div className="form-floating">
                                        <input type="password" name="txtPassword" className="form-control" id="floatingPassword" placeholder="Contraseña..." onChange={this.handleChange} value={this.state.password_login} />
                                        <label>Contraseña</label>
                                    </div>
                                    <div className="form-floating">
                                        <p />
                                    </div>
                                    <div className="form-floating">
                                        <NavLink to={"/"}>
                                            <BotonLogin sw_habilitado={true} texto={"Inicio de Sesion"} eventoClick={this.handleSubmit} />
                                        </NavLink>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <VentanaModal
                    show={this.state.showModal}
                    handleClose={this.handleClose}
                    titulo={this.state.Titulo}
                    setMensaje={this.state.Mensaje}
                    size={"xl"}
                    children={<Bienvenido />}
                >
                </VentanaModal>
            </React.Fragment >
        )
    }

};

export default LoginV;