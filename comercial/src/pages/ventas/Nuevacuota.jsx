import React, { useEffect, useState } from 'react';
import { Form, Button, Spinner } from 'react-bootstrap';
import MonthInnerInput from '../../components/MonthInnerInput';
import { getCurrentDate, getCurrentMonthYear } from '../../utils/calendar';
import { Tabla_Nueva_Cuota_Ferreteros, Tabla_Nueva_Cuota_No_Ferreteros, Tabla_Plan_Venta } from '../../components/Tabla';
import { fetchDataTable, get_zona_defecto, postPlanVentaCuota, validar_cuota_canal } from '../../services/cuotaServices';
import { getUserJWT } from '../../utils/jwt';
import { calculatePercentage, cuotaTotal, truncate } from '../../utils/math';


function NuevaCuota({childBody, handleClose}) {
    const [textInput, setTextInput] = useState(''); //plan venta
    const [numberInput, setNumberInput] = useState({isSave: false, value: ''}); //plan venta total
    const [monthData, setMonthData] = useState( getCurrentMonthYear()); //mes plan de venta total
    const [dataTable, setDataTable] =  useState([]);
    //estado para verificar si un periodo ya fue creado
    const [periodoCreado, setPeriodoCreado] =  useState(true);
    //estado cargando
    const [loading, setLoading] = useState(false);
    //estado para cargar la data como monto o como porcentaje
    const [isAmount, setIsAmount] = useState(false);

    const prefix = 'S/ ';

    //handleNumberInput
    const handleNumberInput = (obj) => setNumberInput({...numberInput, ...obj})
    
    //handleDataTable
    const handleDataTable = (data) => setDataTable(data)

    //handlePeriodoCreado
    const handlePeriodoCreado = (flag) => setPeriodoCreado(flag)

    //handleLoading
    const handleLoading = (state) => setLoading(state)
 
    //fetch DataTable when first stage pass
    useEffect(()=>{
        const fetchData_Table = async () => {
            const response = await fetchDataTable({canal_id: childBody.canal_code});
            let tableDataBody = [];
            if(childBody.canal_code === '01'){
                tableDataBody =  response.zona.map((idx)=>({idzona: idx.id, zona: idx.nombre, vendedor: {index: '-1', vendedores: response?.vendedor}, cuota: ''}))
            }else{
                tableDataBody =  response.vendedor.map((idx, index)=>({idzona: index, zona: null, vendedor: {index: idx.id, vendedores: [{id: idx.id, nombre:idx.nombre}]}, cuota: ''}))
            }
            handleDataTable(tableDataBody)
        } 
        if(numberInput.isSave){
            fetchData_Table();
        }
    },[numberInput.isSave])

    //fetch cambio mes y año
    useEffect(()=>{
        const fetchValidarCuotaCanal = async () => {
            const response = await validar_cuota_canal({anio: monthData.year.toString(), mes: monthData.month.toString().padStart(2,'0'), canal: childBody.canal_code})
            if (response.status === 200){
                handlePeriodoCreado(response.message)
            }
        }
        fetchValidarCuotaCanal()
    },[monthData])

    const usuariosActivosConCero = () => {
        let userActivos = dataTable.filter((idx)=>(idx.vendedor.index !== '-1'))
        let userConCero = userActivos.filter((idy)=>(idy.cuota === 0 || idy.cuota === ''))
        return userConCero
    }

    const getNameZonabyId = (id) => {
        let zona_arr = dataTable.map((idy)=>({idzona: idy.idzona, zona: idy.zona}))
        let zona = zona_arr.find((idx => idx.idzona === id));
        return zona?.zona || null
    }

    const consolidarCuotas = async (bodyDetail) => {
        //idzona, zona
        
        const unique = bodyDetail.reduce((result, obj) => {
            const existingObj = result.find(item => item['cod_vendedor'] === obj['cod_vendedor']);
            if (existingObj) {
                existingObj['cuota_porcent'] += obj['cuota_porcent'];
                existingObj['cuota_soles'] += obj['cuota_soles'];
                existingObj['nom_zona'] = null
                existingObj['cod_zona'] = null
            } else {
                result.push({ ...obj });
            }
            return result;
        }, []);
        
        //solo asigna zona por defecto cuando el canal es ferretero
        if(childBody.canal_code === '01'){
            //asigna la zona por defecto
            
            let unique_null = unique.filter((idw)=>(idw.cod_zona  === null && idw.nom_zona === null))
            //unique_null: vendedor sin zona
            //abajo: lista de codigos de vendedores sin zona
            const params = new URLSearchParams();
            unique_null.forEach(id=>{
                params.append('lista_codigo', id.cod_vendedor.toString())
            })
            
            //response format [{slpCode, idzona},....] : [{codigo[int], zona[string]}] 
            let response = await get_zona_defecto(params)
            if(!!response?.message){
                //filtra lo que no tienen null de zona
                let codesreadytoreplace = response.message.filter(idy=>idy.zona !== null)
                codesreadytoreplace = codesreadytoreplace.map(idw=>({cod_zona: idw.zona, nom_zona: getNameZonabyId(idw.zona), cod_vendedor: idw.codigo}))
                console.log(codesreadytoreplace)
                //AQUI VIENE EL REEMPLAZO †††
                unique.forEach(idx=>{
                    let obj = codesreadytoreplace.find(idw=>idw.cod_vendedor === idx.cod_vendedor)
                    if(!!obj){
                        idx.nom_zona = obj.nom_zona
                        idx.cod_zona = obj.cod_zona
                        idx.cod_vendedor = obj.cod_vendedor

                    }
            })
                console.log(unique)
            }
        }
        
        return unique
    }
    
    const handleSubmit = async (e) => {
        e.preventDefault();
        if(periodoCreado){
            if(!numberInput.isSave){
                if (!!numberInput?.value){
                    const userConfirmation = window.confirm("Al continuar, no podra cambiar el valor del total de ventas\n¿Desea seguir con el registro de cuotas?")
                    if (userConfirmation) {
                        // Aquí puedes manejar la lógia de cambiar de stage
                        handleNumberInput({isSave: true})
                    }
                }else{
                    alert("Debe ingresar un valor valido en total plan ventas")
                }
            }else{
                //suma total de los porcentajes
                let sum = dataTable.reduce((accumulator, currentValue) => {
                    // return accumulator + parseFloat(parseFloat(currentValue.cuota || 0).toFixed(5));
                    return accumulator + Number(currentValue.cuota || 0);
                }, 0.000);

                
                
                console.log(sum)
                console.log(sum.toFixed(3))

                // if (sum < 100) {
                if (sum < 99.9990 && !isAmount) {
                    // alert("Debe alcanzar el 100%")
                    alert("Debe alcanzar como minimo 99.9990%")
                }else if( sum < (parseFloat(parseFloat(numberInput?.value).toFixed(5)) - 0.0001) && isAmount){
                    alert("Debe alcanzar el total del plan de ventas")
                }
                else {
                    //Analiza a los usuarios con 0 el campos de cuota
                    if(!!usuariosActivosConCero().length){
                        alert("Usuarios con 0 %")
                    }else{
                        let activos = dataTable.filter((idw)=>(idw.vendedor.index !== '-1'))

                        let formato_activos = activos.map((idy)=>({
                            nom_zona: idy.zona,
                            //va id zona cuando no es ferretero
                            cod_zona: childBody.canal_code === '01'?idy.idzona:null,
                            cuota_porcent: !isAmount ? idy.cuota : calculatePercentage(idy.cuota, numberInput?.value), 
                            cuota_soles: !isAmount ? cuotaTotal(idy.cuota, parseFloat(parseFloat(numberInput?.value).toFixed(5))) : idy.cuota, 
                            cod_vendedor: Number(idy.vendedor.index),
                            nom_vendedor: idy.vendedor.vendedores.filter((idz)=>(idz.id === Number(idy.vendedor.index)))[0]?.nombre}))

                        
                        let body = {
                            user_date: getCurrentDate(), //[str]
                            user_id: getUserJWT(sessionStorage.getItem('CDTToken')), //[str]
                            pr_name: textInput, //[str]
                            pr_total: Number(numberInput.value), //[float]
                            pr_year: monthData.year.toString(), //[str]
                            pr_month: monthData.month.toString().padStart(2,'0'), //[str] // padStart to add 0 before one character number
                            sc_id: childBody.canal_code, //[str]
                            sc_name: childBody.canal_name, //[str]
                            detalle_cuota: await consolidarCuotas(formato_activos), //list[cuotaDetalle]]
                        }

                        // let response = undefined
                        //falta consolidar la cuota cuando el vendedor aparezca en dos zonas. y que grabe zon por defecto
                        //grabar zona null cuando el vendedor no sea de canal ferretero [ya esta]

                        handleLoading(true)
                        const response = await postPlanVentaCuota(body)
                        handleLoading(false)
                        
                        if (response.status === 500){
                            alert(response.error)
                        }else{
                            alert(response.message)
                            handleClose(true)
                        }
                    };
                }

            }
        }else{
            alert("El periodo ya fue creado")
        }
    };

    //cambia la seleccion de los combos en la columna de vendedores
    const manejaEvento = (e, zona) => {
        let ghostTableDataBody = [...dataTable]
        ghostTableDataBody.forEach(element => {
            //cambia vendedor
            if(element.idzona === zona && e.target.value !== '-1'){
                element.vendedor.index = e.target.value
            //si desactiva vendedor, cuota a cero
            }else if(element.idzona === zona && e.target.value === '-1'){
                element.vendedor.index = e.target.value;
                // element.cuota = 0; --> se cambia
                element.cuota = '';
            }
        });
        handleDataTable([...ghostTableDataBody])
    }

    
    //cambia el valor de los campos en la columna cuota
    const manejaCuota = (e, zona) => {
        //primera condicion
        let target = ''            
        if (e.target.value === '') {
            console.log("llega vacio")
        }else{
            target = parseFloat(e.target.value || 0)
            if(target>100.0 && !isAmount){target=''}
            if(target>Number(numberInput?.value) && isAmount){target=''}
        }
        
        //actualiza ghost table
        let ghostTableDataBod = [...dataTable]
        ghostTableDataBod.forEach(element => {
            if(element.idzona === zona){
                element.cuota = target
            }
        });
        
        //
        //suma total de las cuotas
        let sum = ghostTableDataBod.reduce((accumulator, currentValue) => {
            return accumulator + parseFloat(currentValue.cuota || 0);
        }, 0); 

        //logica para no grabar valores totales mayores al 100%
        // console.log('xD', sum)
        if (!isAmount){
            // if ((100 - sum) < 0){
            if ((100.0 - parseFloat(parseFloat(sum).toFixed(5))) < 0){
                alert('Supero el 100%')
                ghostTableDataBod.forEach(element => {
                    if(element.idzona === zona){
                        element.cuota = ''
                    }
                });
                handleDataTable([...ghostTableDataBod])
            }else if((100.0 - parseFloat(parseFloat(sum).toFixed(5))) >= 0){
                handleDataTable([...ghostTableDataBod])
            }
        }else if(isAmount){
            if ((parseFloat(parseFloat(numberInput?.value).toFixed(5)) - parseFloat(parseFloat(sum).toFixed(5))) < 0){
                alert('Supero el total de plan de ventas')
                ghostTableDataBod.forEach(element => {
                    if(element.idzona === zona){
                        element.cuota = ''
                    }
                });
                handleDataTable([...ghostTableDataBod])
            }else if((parseFloat(parseFloat(numberInput?.value).toFixed(5)) - parseFloat(parseFloat(sum).toFixed(5))) >= 0){
                handleDataTable([...ghostTableDataBod])
            }
        }
    }
    
    //maneja la creacion de una nueva fila
    const manejaNuevaFila = () => {
        const userInput = window.prompt("Ingrese ID Zona que desea utilizar");
        if(!!userInput){
            //busca zona para duplicar
            let ghostTableDataBody = JSON.parse(JSON.stringify(dataTable));
            let idw = ghostTableDataBody.filter((idw)=>(Number(idw.idzona) === Number(userInput)))[0]
            
            //obtener maximo valor de idZona
            const max = ghostTableDataBody.reduce((acc, curr) => {
                return Number(curr.idzona) > acc ? Number(curr.idzona) : acc;
              }, -Infinity);

            if (!!idw){
                idw.idzona = (max + 1).toString()
                idw.vendedor.index = '-1'
                idw.cuota = 0
                //crea el ghost table y actualiza array state
                let ghostTableDataBod = JSON.parse(JSON.stringify(dataTable));
                ghostTableDataBod.push(idw)
                handleDataTable([...ghostTableDataBod])
            }
        }
    }

    //limpia los datos de la tabla
    const cleanDataList = (e) => {
        
        let ghostTableDataBod = [...dataTable]
        ghostTableDataBod.forEach(element => {
                element.cuota = ''
        });
        handleDataTable([...ghostTableDataBod])
        
        setIsAmount(e.target.checked)
    }

    const calculaCuotaTotal = () => {
        return dataTable.reduce((accumulator, currentValue) => {
                return accumulator + parseFloat(currentValue.cuota || 0);
        }, 0);
    }


    const deleteSeller = (indexSeller) => {
        //elimina items siempre que haya mas de uno
        if (dataTable?.length > 1){
        let ghostDataTable = [...dataTable]
        let index = ghostDataTable.findIndex(item=>item?.vendedor?.index === indexSeller)
        ghostDataTable.splice(index, 1)
        handleDataTable([...ghostDataTable])
        }
    }

    let valorCuotaTotal = calculaCuotaTotal()
    
    return (
        <div className="container">
            <div className='tw-flex tw-items-center tw-gap-3'>
            <div className='tw-h-[70px] tw-mb-2'>
                <label>Periodo:</label>
                <MonthInnerInput monthData={monthData} setMonthData={setMonthData}/>
            </div>
            {
                !periodoCreado &&
                (<div className='tw-text-base tw-font-semibold tw-text-red-500'>¡El periodo ya fue creado!</div>)
            }
            </div>
    <Form onSubmit={handleSubmit} className='tw-flex tw-flex-col tw-gap-3'>
        <Form.Group controlId="formTextInput">
            <Form.Label>Plan de venta (Descripción):</Form.Label>
            <Form.Control 
                type="text" 
                placeholder="Ingrese texto" 
                value={textInput} 
                onChange={(e) => setTextInput(e.target.value)}
                autoComplete='off'
            />
        </Form.Group>

        <Form.Group controlId="formNumberInput">
            <Form.Label>Total plan ventas:</Form.Label>
            <Form.Control 
                type="number" 
                placeholder="Ingrese valor" 
                value={numberInput?.value.toString()}
                onChange={(e) => handleNumberInput({value: e.target.value})}
                disabled={numberInput?.isSave}
            />
        </Form.Group>
        {/* childBody.canal_code: 01 , Para canal ferretero */}
        {numberInput?.isSave && (
                (childBody.canal_code === '01')?(
                    <div className='cuotas-table'>
                        <div className='tw-mb-3'>
                            <Form>
                                <Form.Check // prettier-ignore
                                type="switch"
                                id="custom-switch"
                                label="⬅ Porcentaje | Monto ➡"
                                checked={isAmount}
                                // onChange={(e)=>setIsAmount(e.target.checked)}
                                onChange={cleanDataList}
                                />
                            </Form>
                        </div>
                        <Tabla_Nueva_Cuota_Ferreteros
                            datos_fila={dataTable} 
                            manejaEvento={manejaEvento}
                            manejaCuota={manejaCuota}
                            manejaNuevaFila={manejaNuevaFila}
                            fortable={true}
                            valorCuotaTotal={valorCuotaTotal}
                            isAmount={isAmount}
                        />
                    </div>
                ):(
                    <div className='cuotas-table'>
                           <div className='tw-mb-3'>
                            <Form>
                                <Form.Check // prettier-ignore
                                type="switch"
                                id="custom-switch"
                                label="⬅ Porcentaje | Monto ➡"
                                checked={isAmount}
                                // onChange={(e)=>setIsAmount(e.target.checked)}
                                onChange={cleanDataList}
                                />
                            </Form>
                        </div>
                        <Tabla_Nueva_Cuota_No_Ferreteros
                            datos_fila={dataTable} 
                            manejaCuota={manejaCuota}
                            fortable={true}
                            valorCuotaTotal={valorCuotaTotal}
                            isAmount={isAmount}
                            deleteSeller={deleteSeller}
                        />
                    </div>
                )
        )}

        <Button variant="primary" type="submit" disabled={loading}>
            <div className='tw-flex tw-justify-center tw-items-center tw-gap-3'>
                {numberInput?.isSave?'Guardar':'Continuar'}
                {loading && (<Spinner animation="border" variant="light" size='sm' />)}
            </div>
        </Button>
    </Form>
    </div>
  )
}

export default NuevaCuota