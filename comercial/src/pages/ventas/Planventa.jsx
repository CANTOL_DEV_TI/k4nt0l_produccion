import React, { useEffect, useState } from 'react'
import { Validar } from '../../services/ValidaSesion';
import VentanaBloqueo from '../../components/VentanaBloqueo';
import { ComboBox } from '../../components/Combos';
import { getCanalesVenta, getPlanesVenta, updatehabilitarPlan } from '../../services/cuotaServices';
import '../../assets/style/index.css'
import { Tabla_Plan_Venta } from '../../components/Tabla';
import Button from 'react-bootstrap/Button';
import { VentanaModal_Cuotas } from '../../components/VentanaModal';
import Cuotas from './Cuotas';
import NuevaCuota from './NuevaCuota';


export default function Planventa() {
    // seguridad
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
    
    //datos combo
    const [canalesventa, setCanalesVenta] = useState({index: '-1', options: []});
    const handleCanales = (obj) => setCanalesVenta({...canalesventa, ...obj})
    
    //handle modal Cuota
    const [modalCuota, setModalCuota] = useState({show: false, title: '', size: 'lg', feet: true, childBody: {}});
    const handleModalCuota = (obj) => setModalCuota({...modalCuota, ...obj})
    
    //handle modal nuevo Plan Cuota
    //success tiene que ir dentro del childBody
    const [modalPlanCuota, setModalPlanCuota] = useState({show: false, title: '', size: 'lg', feet: false, success: false});
    const handleModalPlanCuota = (obj) => setModalPlanCuota({...modalPlanCuota, ...obj})

    
    //planes venta
    const [planesventa, setPlanesVenta] = useState([]);
    const handlePlanes = (id, campo, valor) => {
        let ghostPlan = [...planesventa];
        ghostPlan.forEach((item, index, array)=>{if(item.id === id){array[index][campo] = valor}});
        setPlanesVenta(ghostPlan);
    }

    //Seguridad
    const Seguridad = async () => {        
        const ValSeguridad = await Validar("COMCUO")
		setVentanaSeguridad(ValSeguridad.Validar)
    }
    
    //Render inicial
    useEffect(()=>{
        Seguridad();

        //Consume datos del combo        
        const getInnerCanalesVenta = async () => {
            const response = await getCanalesVenta()
            handleCanales({options: [...response]})
        }
        getInnerCanalesVenta()
        //termina consumo

    },[])
    
    //Despues del cambio de valor combo
    useEffect(()=>{
        // obtiene planes de venta
        getPV()
    },[canalesventa?.index, modalPlanCuota.success])

    //renderizar tabla despues de crear plan nuevo cuotas
    // useEffect(()=>{
    //     if(modalPlanCuota.success){
    //         getPV()
    //     }
    // },[modalPlanCuota.success])
    
    //getPV
    const getPV = async () => {
        if(canalesventa.index !== '-1'){
            const response = await getPlanesVenta({canal_id: canalesventa?.index})
            setPlanesVenta([...response])
        }
    }
    
    
    //Maneja cambio de habilitar plan
    const manejaHabilitar = async (id) => {
        const response = await updatehabilitarPlan({id: id})
        handlePlanes(id, 'habilitado', response.habilitado)
    }

    //Maneja Evento combo
    const manejaEvento = (e) => {
        if(e.target.id === 'cbo_canales'){
            handleCanales({index: e.target.value})
            }
        }
    
    //Maneja Modal Cuotas        
    const manejaModalCuotas = (id, title, body) => {
        handleModalCuota({show:true, title: `${title} - CUOTAS`, childBody: body})
    }
    
    //Maneja nuevo modal Plan        
    const manejaModalPlan = () => {
        let canalVenta = canalesventa.options.filter((idy)=>(idy.canal_code === canalesventa.index))[0]
        handleModalPlanCuota({show:true, title: `Nueva cuota canal ${canalVenta.canal_name}`, childBody: canalVenta, success: false})
    }

  return (
    <div>
        <div className="col-lg-12">
            {/* INICIO BARRA DE NAVEGACION */}
            <div className="card tw-mx-3">

                <div className="card-header border-0 card-header">
                    <div className="align-items-center gy-3 row">
                        <div className="col-sm">
                            <h4 className="card-title mb-0">{'Plan de ventas'}</h4>
                        </div>
                    </div>
                </div>

            <div className="p-0 card-body">
                {/* BODY HEADER */}
                    <div className="tw-flex">
                    {/* SELECCIONAR CANAL DE VENTA */}
                        <div className='p-2 tw-w-96'>
                                <ComboBox
                                    datosRow={canalesventa?.options}
                                    nombre_cbo="cbo_canales"
                                    manejaEvento={(x)=>manejaEvento(x)}
                                    valor_ini={"Canal"}
                                    valor={canalesventa?.index}
                                    />
                        </div>
                    {/* FIN SELECCIONAR CANAL DE VENTA */}
                    {/* SELECCIONAR CANAL DE VENTA */}
                        <div className='p-2'>
                            <Button className={`${canalesventa.index === '-1' && 'disabled'}`} variant="success" onClick={manejaModalPlan}>Nuevo plan y cuotas</Button>
                        </div>
                    {/* FIN SELECCIONAR CANAL DE VENTA */}
                    </div>
                {/* FIN BODY HEADER */}
                
                { /* INICIO TABLA */}
                    <div className='w-100 p-0 position-relative overflow-hidden' style={{height:'fit-content'}}>
                        <Tabla_Plan_Venta datos_fila={planesventa} manejaHabilitar={manejaHabilitar} manejaModalCuotas={manejaModalCuotas}/>
                    </div>
                {/* FIN TABLA */}
            
            </div>
            </div>
        </div >
        
        {/* Ventana Modal Cuotas */}
        <VentanaModal_Cuotas 
            show={modalCuota.show}
            handleClose={()=>{handleModalCuota({show:false})}}
            titulo={modalCuota.title}
            size={modalCuota.size}
            feet={modalCuota.feet}
        >
            <Cuotas body={modalCuota.childBody}/>
        </VentanaModal_Cuotas>
        {/* Fin Ventana Modal Cuotas */}
        
        {/* Ventana Modal Plan Cuotas */}
        <VentanaModal_Cuotas 
            show={modalPlanCuota.show}
            handleClose={()=>{handleModalPlanCuota({show:false})}}
            titulo={modalPlanCuota.title}
            size={modalPlanCuota.size}
            feet={modalPlanCuota.feet}
        >
            <NuevaCuota
                childBody={modalPlanCuota.childBody}
                handleClose={(success)=>{handleModalPlanCuota({show:false, success: success})}}
            />
        </VentanaModal_Cuotas>
        {/* Fin Ventana Modal Plan Cuotas */}

        {/* seguridad */}
        <VentanaBloqueo show={VentanaSeguridad} />
    </div>
  )
}