import React, { useEffect, useState } from 'react'
import { Tabla_Cuotas } from '../../components/Tabla'
import { getCuotas } from '../../services/cuotaServices'

function Cuotas({body}) {
    const [cuotasPlan, setCuotasPlan] = useState([])

    useEffect(()=>{
        //aqui va el fetch
        const fetchCuotas = async () => {
            const response = await getCuotas(body)
            setCuotasPlan([...response])
        }
        fetchCuotas()
    }, [])

  return (
    <div>
        <Tabla_Cuotas datos_fila={cuotasPlan}/>
    </div>
  )
}

export default Cuotas
