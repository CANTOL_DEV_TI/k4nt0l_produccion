import React from "react";
import { BotonGuardar } from "../../../components/Botones";

class ActualizarGuia extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = {
                DocEntry: "", DocNum: "", DocDate: "", DocDueDate: "", CardCode: "", CardName: "", U_MSSL_NCD: "", U_MSSL_LCD: "", U_MSSL_PVH: "",
                U_MSS_PRFI: "", U_MSS_NUFI: "", U_MSS_PLN: "", U_MSS_FGTO: "", U_MSS_ESTRA: "", U_MSS_ESLI: "", U_MSS_ESTLIQ: "", U_MSS_MONLIQ: 0.00,
                U_MSS_MONEDA: "", U_MSS_NUMOP: "", U_MSS_TIPAG: "", U_MSS_OBSE: ""
            }
        } else {
            this.state = {
                DocEntry: this.props.dataToEdit.DocEntry, DocNum: this.props.dataToEdit.DocNum, DocDate: this.props.dataToEdit.DocDate,
                DocDueDate: this.props.dataToEdit.DocDueDate, CardCode: this.props.dataToEdit.CardCode, CardName: this.props.dataToEdit.CardName,
                U_MSSL_NCD: this.props.dataToEdit.U_MSSL_NCD, U_MSSL_LCD: this.props.dataToEdit.U_MSSL_LCD, U_MSSL_PVH: this.props.dataToEdit.U_MSSL_PVH,
                U_MSS_PRFI: this.props.dataToEdit.U_MSS_PRFI, U_MSS_NUFI: this.props.dataToEdit.U_MSS_NUFI, U_MSS_PLN: this.props.dataToEdit.U_MSS_PLN,
                U_MSS_FGTO: this.props.dataToEdit.U_MSS_FGTO, U_MSS_ESTRA: this.props.dataToEdit.U_MSS_ESTRA, U_MSS_ESLI: this.props.dataToEdit.U_MSS_ESLI,
                U_MSS_ESTLIQ: this.props.dataToEdit.U_MSS_ESTLIQ, U_MSS_MONLIQ: this.props.dataToEdit.U_MSS_MONLIQ, U_MSS_MONEDA: this.props.dataToEdit.U_MSS_MONEDA,
                U_MSS_NUMOP: this.props.dataToEdit.U_MSS_NUMOP, U_MSS_TIPAG: this.props.dataToEdit.U_MSS_TIPAG, U_MSS_OBSE: this.props.dataToEdit.U_MSS_OBSE
            }
        }
    }

    handleChange = (e) => {

        if (e.target.name === 'u_mss_estra') {
            this.setState({ U_MSS_ESTRA: e.target.value })
        }

        if (e.target.name === 'u_mss_esli') {
            this.setState({ U_MSS_ESLI: e.target.value })
        }

        if (e.target.name === 'u_mss_monliq') {
            this.setState({ U_MSS_MONLIQ: e.target.value })
        }

        if (e.target.name === 'u_mss_numop') {
            this.setState({ U_MSS_NUMOP: e.target.value })
        }

        if (e.target.name === 'u_mss_tipag') {
            this.setState({ U_MSS_TIPAG: e.target.value })
        }

        if (e.target.name === 'u_mss_obse') {
            this.setState({ U_MSS_OBSE: e.target.value })
        }


    }


    render() {
        const { eventOnclick, nameOpe } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Nro Interno : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="docentry" readOnly onChange={this.handleChange} value={this.state.DocEntry} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Nro Documento : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="docnum" readOnly onChange={this.handleChange} value={this.state.DocNum} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Serie - Doc : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" name="serie_nro" readOnly onChange={this.handleChange} value={this.state.U_MSS_PRFI + '-' + this.state.U_MSS_NUFI} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Estado de despacho transportista : </div>
                            <div className="col-sm-2 col-form-label">
                                <select name="u_mss_estra" onChange={this.handleChange} value={this.state.U_MSS_ESTRA} >
                                    <option value={"0"}>Seleccione...</option>                                    
                                    <option value={"1"}>Pendiente</option>
                                    <option value={"2"}>Despacho Sin Cobranza</option>
                                    <option value={"3"}>Despacho con Cobranza</option>
                                    <option value={"4"}>Despacho a Currier</option>
                                    <option value={"5"}>No despachado</option>
                                </select>
                            </div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        {this.state.U_MSS_ESTRA === "3" && 
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Estado Liquidacion : </div>
                            <div className="col-sm-2 col-form-label">
                                <select name="u_mss_esli" onChange={this.handleChange} value={this.state.U_MSS_ESLI} >
                                    <option value={"0"}>Seleccione...</option>
                                    <option value={"1"}>Pendiente de liquidación</option>
                                    <option value={"2"}>Liquidado</option>
                                    <option value={"3"}>Rechazado</option>
                                </select>
                            </div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        }
                        {this.state.U_MSS_ESLI === "2" && 
                            <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Tipo de Pago : </div>
                            <div className="col-sm-2 col-form-label">
                                <select name="u_mss_tipag" onChange={this.handleChange} value={this.state.U_MSS_TIPAG} >
                                    <option value={"0"}>Seleccione...</option>
                                    <option value={"1"}>Efectivo</option>
                                    <option value={"2"}>Transferencia/Deposito</option>
                                </select>
                            </div>
                            <div className="col-sm-2 col-form-label"></div>
                            </div>
                        }
                        {this.state.U_MSS_ESLI === "2" &&               
                            <div className="form-group row">
                                <div className="col-sm-3 col-form-label">Monto a Liquidar : </div>
                                <div className="col-sm-2 col-form-label"><input type="text"  name="u_mss_monliq" onChange={this.handleChange} value={this.state.U_MSS_MONLIQ} /></div>
                                <div className="col-sm-2 col-form-label"></div>
                            </div>
                        }
                        {this.state.U_MSS_ESLI === "2" &&
                            this.state.U_MSS_TIPAG === "2" &&
                            <div className="form-group row">
                                <div className="col-sm-3 col-form-label">Nro Operacion : </div>
                                <div className="col-sm-2 col-form-label"><input type="text" name="u_mss_numop" onChange={this.handleChange} value={this.state.U_MSS_NUMOP} /></div>
                                <div className="col-sm-2 col-form-label"></div>
                            </div>
                        }
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Observaciones : </div>
                            <div className="col-sm-2 col-form-label"><textarea rows={5} type="text" name="u_mss_obse" onChange={this.handleChange} value={this.state.U_MSS_OBSE} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>

                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => 
                    eventOnclick({  "DocEntry": this.state.DocEntry, "U_MSS_PRFI" : this.state.U_MSS_PRFI,"U_MSS_NUFI" : this.state.U_MSS_NUFI, "U_MSS_ESTRA" : this.state.U_MSS_ESTRA,
                                    "U_MSS_ESLI": this.state.U_MSS_ESLI, "U_MSS_TIPAG" : this.state.U_MSS_TIPAG ,"U_MSS_MONEDA" : "S/.",
                                    "U_MSS_MONLIQ" : parseFloat(this.state.U_MSS_MONLIQ), "U_MSS_NUMOP" : this.state.U_MSS_NUMOP, "U_MSS_OBSE" : this.state.U_MSS_OBSE })} texto={nameOpe} />
            </div>
        )
    }
    async componentDidMount() {
        //console.log(this.props.dataToEdit)
    }
}

export default ActualizarGuia;