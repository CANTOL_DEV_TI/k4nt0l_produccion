import React, { useState } from "react";
import Title from "../../../components/Titulo";
import { BotonExcel, BotonNuevo, BotonBuscar } from "../../../components/Botones";

class Busqueda extends React.Component {

    constructor(props) {
        super(props);

        this.state = { Fecha: '', Chofer: "" }
    }

    handleChange = (e) => {
        if (e.target.name === "tFecha") {
            this.setState({ Fecha: e.target.value })
        }

        if (e.target.name === "lChoferes") {
            this.setState({ Chofer: e.target.value })
        }

    }


    render() {

        const { handleBusqueda, handleModal, LCh } = this.props

        return (
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Lista de Liquidaciones</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    Seleccione fecha de emision de guia :
                                    <input type="date" name="tFecha"
                                        placeholder="Seleccione..."
                                        onChange={this.handleChange}
                                        value={this.state.Fecha}
                                    />
                                    Seleccione Chofer :
                                    <select name="lChoferes" placeholder="Seleccione..." onChange={this.handleChange} value={this.state.Chofer}>
                                        <option value='0'>Seleccionar Chofer</option>
                                        {   
                                            //console.log(LCh);
                                            LCh !== undefined && 
                                            LCh.map((v_item) =>
                                                <option value={v_item.U_MSS_LICE}>{`${v_item.U_MSS_ACON + ' ' + v_item.U_MSS_NONC}`}</option>
                                            )
                                        }
                                    </select>

                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda(this.state.Fecha,this.state.Chofer)} />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Busqueda;