import React from "react";
import {GrEdit} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({DocEntry,                                                
                        DocNum,                        
                        DocDate,
                        DocDueDate,
                        CardCode,
                        CardName,
                        U_MSSL_NCD,
                        U_MSSL_LCD,
                        U_MSSL_PVH,
                        U_MSS_PRFI,
                        U_MSS_NUFI,
                        U_MSS_PLN,
                        U_MSS_FGTO,
                        U_MSS_ESTRA,
                        U_MSS_ESLI,
                        U_MSS_ESTLIQ,
                        U_MSS_MONLIQ,
                        U_MSS_MONEDA,
                        U_MSS_NUMOP,
                        U_MSS_TIPAG,
                        U_MSS_OBSE,
                        eventoEditar
                        }) =>
    (
        <tbody>
            <tr>
                <td><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>  
                <td className="td-cadena" id={DocEntry}>{DocEntry}</td>                
                <td className="td-cadena" >{DocNum}</td>    
                <td className="td-cadena" >{U_MSS_PRFI}</td>
                <td className="td-cadena" >{U_MSS_NUFI}</td>            
                <td className="td-cadena" >{DocDate}</td>
                <td className="td-cadena" >{DocDueDate}</td>
                <td className="td-cadena" >{CardCode}</td>
                <td className="td-cadena" >{CardName}</td>                
                <td className="td-cadena" >{U_MSSL_PVH}</td>                
                <td className="td-cadena" >{U_MSS_PLN}</td>                                                        
            </tr>
        </tbody>
    )

export default ResultadoTabla;

/*
<td className="td-cadena" >{U_MSS_FGTO}</td>
                <td className="td-cadena" >{U_MSS_ESTRA}</td>
                <td className="td-cadena" >{U_MSS_ESLI}</td>
                <td className="td-cadena" >{U_MSS_ESTLIQ}</td>
                <td className="td-cadena" >{U_MSS_MONLIQ}</td>
                <td className="td-cadena" >{U_MSS_MONEDA}</td>
                <td className="td-cadena" >{U_MSS_NUMOP}</td>
                <td className="td-cadena" >{U_MSS_TIPAG}</td>
                <td className="td-cadena" >{U_MSS_OBSE}</td>
                */
