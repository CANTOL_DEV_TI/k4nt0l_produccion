import React from "react";
import { ListaLiquidaciones } from "../../services/liquidacionServices.jsx";
import Busqueda from "../liquidacion/components/Busqueda.jsx"
import ResultadoTabla from "../liquidacion/components/ResultadoTabla.jsx";
import VentanaModal from "../../components/VentanaModal.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";
import { Validar } from "../../services/ValidaSesion.jsx";
import { ListaChoferes } from "../../services/choferServices.jsx";
import ActualizarGuia from "./components/ActualizarGuia.jsx";
import { UpdateLiqGuia } from "../../services/liquidacionServices.jsx";

class Liquidaciones extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            filtros : {fecha:"",chofer:""}
        }
    }

    handleBusqueda = async (pFecha, pChofer) => {
        console.log(pFecha,pChofer)
        const filtros = {"Desde" : pFecha, "Chofer" : pChofer}
        const responseJson = await ListaLiquidaciones(filtros)
        console.log(responseJson)
        this.setState({ resultados: responseJson, isFetch: false })
        console.log(this.state)
    }

    handleSubmit = async (e) => {
    
        if (this.state.tipoOpe === 'Actualizar') {
            const paquete = {"Guias" : [e]}
            const responseJson = await UpdateLiqGuia(paquete)    
            alert(responseJson[0].mensaje)
        }

    
        this.setState({ showModal: false, tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad, ListChofer } = this.state

        return (
            <React.Fragment>
                
                <Busqueda LCh={this.state.ListChofer} handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} />

                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                            <tr>
                                <th className="align-middle"></th>   
                                <th className="align-middle">D.E.</th>
                                <th className="align-middle">Nro SAP</th>
                                <th className="align-middle">Serie Fiscal</th>
                                <th className="align-middle">Nro Fiscal</th>
                                <th className="align-middle">Fecha Documento</th>
                                <th className="align-middle">Fecha Entrega</th>
                                <th className="align-middle">Codigo S.N.</th>
                                <th className="align-middle">Socio Negocio</th>                                
                                <th className="align-middle">Nro Placa</th>  
                                <th className="align-middle">Nro PLAN</th>                                                              
                                                             
                            </tr>
                        </thead>
                        {resultados !== undefined  &&
                         resultados.map((registro) =>
                            <ResultadoTabla
                                key={registro.DocEntry}
                                DocEntry={registro.DocEntry}
                                DocNum={registro.DocNum}
                                DocDate={registro.DocDate}
                                DocDueDate={registro.DocDueDate}
                                CardCode={registro.CardCode}
                                CardName={registro.CardName}
                                U_MSSL_NCD={registro.U_MSSL_NCD}
                                U_MSSL_LCD={registro.U_MSSL_LCD}
                                U_MSSL_PVH={registro.U_MSSL_PVH}
                                U_MSS_PRFI={registro.U_MSS_PRFI}
                                U_MSS_NUFI={registro.U_MSS_NUFI}
                                U_MSS_PLN={registro.U_MSS_PLN}
                                U_MSS_FGTO={registro.U_MSS_FGTO}
                                U_MSS_ESTRA={registro.U_MSS_ESTRA}
                                U_MSS_ESLI={registro.U_MSS_ESLI}
                                U_MSS_ESTLIQ={registro.U_MSS_ESTLIQ}
                                U_MSS_MONLIQ={registro.U_MSS_MONLIQ}
                                U_MSS_MONEDA={registro.U_MSS_MONEDA}
                                U_MSS_NUMOP={registro.U_MSS_NUMOP}
                                U_MSS_TIPAG={registro.U_MSS_TIPAG}
                                U_MSS_OBSE={registro.U_MSS_OBSE}
                                eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}                                
                            />
                        )}
                        
                    </table>
                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({ showModal: false })}
                    titulo="Verificacion Entregas">
                        <ActualizarGuia 
                            dataToEdit={this.state.dataRegistro}
                            eventOnclick={this.handleSubmit}
                            nameOpe={this.state.tipoOpe}
                        />
                    
                </VentanaModal>
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}

                />
            </React.Fragment>
        )

    }
    async componentDidMount() {
        const ValSeguridad = await Validar("MLDLIQ")
        this.setState({ "VentanaSeguridad": ValSeguridad })        

        const ListChoferes = await ListaChoferes()        
        this.setState({"ListChofer" : ListChoferes})        
    }
}

export default Liquidaciones;