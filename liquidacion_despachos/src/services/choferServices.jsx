import {Url} from '../constants/global'

// const meServidorBackend = 'http://192.168.5.21:8080'
const meServidorBackend = Url + "/liquidespa"

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function ListaChoferes(){   
    const requestOptions = {
        method: 'GET',        
        headers: cabecera,
        //body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/choferes/listado/`, requestOptions);    
    const responseJson = await response.json();    
    return responseJson
}