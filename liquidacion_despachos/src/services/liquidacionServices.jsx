import {Url} from '../constants/global'

// const meServidorBackend = 'http://192.168.5.21:8080'
const meServidorBackend = Url + "/liquidespa"

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function ListaLiquidaciones(meJson){   
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/guias_liquidacion/listado/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function UpdateLiqGuia(meJson){
    const requestOptions = {
        method : "POST",
        headers : cabecera,
        body : JSON.stringify(meJson)
    };

    const response = await fetch (`${meServidorBackend}/guias_liquidacion/actualizacionguias/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}