import React from 'react'

export const TarjetaTotal = ({ str_concepto, obj_imagen, str_monto }) => {
    return (
        <div className="col-lg-3 col-md-6" >
            <div className="card">
                <div className="card-body">
                    <div className="d-flex align-items-center">
                        <div className="avatar-sm flex-shrink-0">
                            <span className="avatar-title bg-warning text-dark rounded-circle fs-3">
                                {obj_imagen}
                            </span>
                        </div>
                        <div className="flex-grow-1 ms-3">
                            <p className="text-uppercase fw-semibold fs-12 text-muted mb-1">{str_concepto}</p>
                            <h4 className=" mb-0"><span className="counter-value">{str_monto}</span></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};