import React from 'react'
import { NavLink } from "react-router-dom";
import {
    BsFileEarmarkExcelFill,
    BsSearchHeartFill,
    BsPlusLg,
    BsArrowLeftCircle,
    BsDatabase,
    BsSave,
    BsCloudCheckFill,
    BsTrash3,
    BsArrowDownCircle,
    BsQuestionCircle
} from "react-icons/bs";
import { CSVLink } from "react-csv";


export const BotonExcel = ({ textoBoton, sw_habilitado, listDatos, nombreArchivo }) => {
    return (
        <>
            <CSVLink data={listDatos} className={sw_habilitado ? 'btn btn-warning btn-sm' : 'btn btn-warning btn-sm disabled'} filename={nombreArchivo}>
                <i className="align-bottom me-1"><BsFileEarmarkExcelFill /></i>
                {textoBoton}
            </CSVLink>
        </>
    );
};

export const BotonNuevo = ({ eventoClick, textoBoton = "", sw_habilitado = true, title = "" }) => {
    return (
        <>
            <button title={title} className={sw_habilitado ? 'btn btn-primary btn-sm' : 'btn btn-primary btn-sm disabled'} onClick={eventoClick}>
                <i className="align-bottom me-1"><BsPlusLg /></i>
                {textoBoton}
            </button>
        </>
    );
};

export const BotonAdd = ({ eventoClick, textoBoton = "", sw_habilitado = true, title = "" }) => {
    return (
        <>
            <button title={title} className={sw_habilitado ? 'btn btn-outline-success btn-sm' : 'btn btn-outline-success btn-sm disabled'} onClick={eventoClick}>
                <i className="align-bottom me-1"><BsPlusLg /></i>
                {textoBoton}
            </button>
        </>
    );
};

export const BotonGrabar = ({ eventoClick, textoBoton = "", sw_habilitado = true, title = "" }) => {
    return (
        <>
            <button title={title} className={sw_habilitado ? 'btn btn-success btn-sm' : 'btn btn-success btn-sm disabled'} onClick={eventoClick}>
                <i className="align-bottom me-1"><BsDatabase /></i>
                {textoBoton}
            </button>
        </>
    );
};

export const BotonAtras = ({ eventoClick, textoBoton = "", sw_habilitado = true, title = "" }) => {
    return (
        <>
            <button title={title} className={sw_habilitado ? 'btn btn-sm btn-dark' : 'btn btn-sm btn-dark disabled'} onClick={eventoClick}>
                <i className="align-bottom me-1"><BsArrowLeftCircle /></i>
                {textoBoton}
            </button>
        </>
    );
};

export const BotonBuscar = ({ textoBoton, sw_habilitado, eventoClick }) => {
    return (
        <button type="button" className={sw_habilitado ? 'btn btn-warning btn-sm' : 'btn btn-warning btn-sm disabled'} onClick={() => eventoClick(true)}>
            <i className="align-bottom me-1"> <BsSearchHeartFill /></i>
            {textoBoton}
        </button>
    );
};

export const BotonGuardar = ({ texto, sw_habilitado, eventoClick }) => {
    return (
        <button type="button" className={sw_habilitado ? 'btn btn-warning btn-sm' : 'btn btn-warning btn-sm disabled'} onClick={() => eventoClick(true)}>
            <i className="align-bottom me-1"> <BsSave /></i>
            {texto}
        </button>
    );
};

export const BotonLogin = ({ texto, sw_habilitado, eventoClick }) => {
    return (
        <button type="button" className={sw_habilitado ? 'btn btn-warning btn-sm' : 'btn btn-warning btn-sm disabled'} onClick={() => eventoClick(true)}>
            <i className="align-bottom me-1"> <BsCloudCheckFill /></i>
            {texto}
        </button>
    );
};

export const BotonEliminar = ({ textoBoton, sw_habilitado, eventoClick }) => {
    return (
        <>
            <button className={sw_habilitado ? 'btn btn-danger btn-sm' : 'btn btn-danger btn-sm disabled'} onClick={() => eventoClick(true)}>
                <i className="align-bottom me-1"> <BsTrash3 /></i>
                {textoBoton}
            </button>
        </>
    );
};

export const BotonConsultar = ({textoBoton, sw_habilitado, eventoClick}) => {
    return (
        <>
            <button type='button' className={sw_habilitado ? 'btn btn-info btn-sm' : 'btn btn-info btn-sm disabled'} onClick={() => eventoClick(true)}>
                <i className="align-bottom me-1"><BsDatabase /></i>
                {textoBoton}
            </button>
        </>
    )
}

export const BotonCerrar = ({textoBoton, sw_habilitado, eventoClick}) => {
    return (
        <>
            <button type='button' className={sw_habilitado ? 'btn btn-info btn-sm' : 'btn btn-info btn-sm disabled'} onClick={() => eventoClick(true)}>
                <i className="align-bottom me-1"><BsArrowDownCircle /></i>
                {textoBoton}
            </button>
        </>
    )
}

export const BotonOlvide = ({ texto, sw_habilitado, eventoClick }) => {
    return (
        <button type="button" className={sw_habilitado ? 'btn btn-warning btn-sm' : 'btn btn-warning btn-sm disabled'} onClick={() => eventoClick(true)}>
            <i className="align-bottom me-1"> <BsQuestionCircle /></i>
            {texto}
        </button>
    );
};