import { Routes, Route, } from "react-router-dom";
import LoginV from "../pages/login/login";
//import Usuarios from "../pages/usuario/usuario.jsx";
import ActuaPassword from "../pages/login/cambiocontraseña";
import Liquidaciones from "../pages/liquidacion/liquidacion";
import React from "react";
import ProtectedRoute from "../components/utils/ProtectedRoute"

export function MiRutas() {
    return (
        <Routes>
            <Route path="/" element={<LoginV />} />
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/cambiocontrasena" element={<ActuaPassword />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/liquidaciones" element={<Liquidaciones />} />
            </Route>
        </Routes>
    );
}