import React, { lazy, Suspense } from 'react'
import { BrowserRouter, Router } from "react-router-dom";
import { MiRutas } from "./routers/rutas"
import NavbarV1 from "./components/navegacion/Navbar_V1/NavbarV1"
import { ProduccionContext } from './Context/ProduccionContext';
import { SnackbarProvider } from 'notistack';

function App() {
  return (
    <BrowserRouter basename="logistica_compras">
      <ProduccionContext>
      <SnackbarProvider maxSnack={3}>
        <div className="page-content">
          <NavbarV1 />
          <div className="container-fluid">
            <MiRutas />
          </div>
        </div>
      </SnackbarProvider>
      </ProduccionContext>
    </BrowserRouter>
  )
}

export default App
