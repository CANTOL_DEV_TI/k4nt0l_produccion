import styled from "styled-components";

export const Wrapper = styled.div`
  
`;



export const Container = styled.div`
  padding: 50px 0 20px 0;
`;


export const Block = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 50px;
`;

export const MeLogo = styled.img`
    
`


export const Titulo1 = styled.h1`
  width: 100%;
  color: #f1f1f1;
  text-align: center;
  opacity: 1;
  font-family: 'Anton', serif;
  font-size: 30px;
  
  font-size: 96px;
  font-weight: 600;
  font-family: Hack, monospace;
  flex-direction: column;
  
  &:hover{
    background-size: 100%;
  }
`;

export const Titulo2 = styled.h2`
  width: 100%;
  color: #052581;
  text-align: center;
  opacity: 1;
  font-family: 'Anton', serif;
  font-size: 30px;
`;

export const LogoContainer = styled.div`
    
`;

export const Menu = styled.ul`
  text-align: center;
  display:inline-block;
  list-style: none;
  vertical-align: middle;
  width: 70%;
  
  &:after{
    content: "";
    display: table;
    clear: both;
  }
`;

export const MenuItem = styled.li`
  float: left;
  width: 22%;
  height: 150px;
  margin: 10px 14px;

  display: flex;
  align-items: center;
  justify-content: center;
  
  @media screen and (max-width: 768px) {
    width: 90%;
  }
  
  
`;

export const MenuItemLink = styled.a`
`;


export const MobileIcon = styled.div`
    
`;


export const PiePagina = styled.footer`
    
`;