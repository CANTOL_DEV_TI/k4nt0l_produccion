import React from "react";
import { BsSearchHeartFill } from "react-icons/bs";
import { DownloadTableExcel } from "react-export-table-to-excel";
import { downloadExcel } from "react-export-table-to-excel";
import { useRef } from "react";
import { BotonExportar } from "../../../components/Botones";

const TableRef = React.createRef()
const ResultadoTabla = ({
    Cia,
    Codigo,
    Nombre,
    ALM16, Stock_16, Comprometido_16, Disponible_16, Pedido_16,
    ALM20, Stock_20, Comprometido_20, Disponible_20, Pedido_20,
    ALM36, Stock_36, Comprometido_36, Disponible_36, Pedido_36,
    ALM01, Stock_01, Comprometido_01, Disponible_01, Pedido_01,
    ALM12, Stock_12, Comprometido_12, Disponible_12, Pedido_12,
    ALM24, Stock_24, Comprometido_24, Disponible_24, Pedido_24,
    eventoVer
}) =>
(
    <div className="table-responsive" style={{ height: '800px', display: "-ms-flexbox" }}>
        <table className="table table-hover table-sm table-bordered table-striped" ref={TableRef} >
            <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                <tr >
                    <th className="align-middle" colSpan={2} ></th>
                    {(!ALM16 == "") ?
                        <th className="align-middle" colSpan={4}>{ALM16}</th> : ""}
                    {(!ALM01 == "") ?
                        <th className="align-middle" colSpan={4}>{ALM01}</th> : ""}
                    {(!ALM12 == "") ?
                        <th className="align-middle" colSpan={4}>{ALM12}</th> : ""}
                    {(!ALM20 == "") ?
                        <th className="align-middle" colSpan={4}>{ALM20}</th> : ""}
                    {ALM36.length > 0 ?
                        <th className="align-middle" colSpan={4}>{ALM36}</th> : ""}
                    {(!ALM24 == "") ?
                        <th className="align-middle" colSpan={4}>{ALM24}</th> : ""}
                    <th className="align-middle" ></th>
                </tr>
                <tr>
                    <th className="align-middle" >Item Codigo</th>
                    <th className="align-middle" >Item Nombre</th>
                    {(ALM16 != '') ?
                        <th className="align-middle" >Stock</th> : ""}
                    {(ALM01 != '') ?
                        <th className="align-middle" >Stock</th> : ""}
                    {(ALM16 != '') ?
                        <th className="align-middle" >Compromentido</th> : ""}
                    {(ALM01 != '') ?
                        <th className="align-middle" >Comprometido</th> : ""}
                    {(ALM16 != '') ?
                        <th className="align-middle" >Disponible</th> : ""}
                    {(ALM01 != '') ?
                        <th className="align-middle" >Disponible</th> : ""}
                    {(ALM16 != '') ?
                        <th className="align-middle" >Pedido</th> : ""}
                    {(ALM01 != '') ?
                        <th className="align-middle" >Pedido</th> : ""}
                    {(ALM20 != '') ?
                        <th className="align-middle" >Stock</th> : ""}
                    {(ALM12 != '') ?
                        <th className="align-middle" >Stock</th> : ""}
                    {(ALM20 != '') ?
                        <th className="align-middle" >Compromentido</th> : ""}
                    {(ALM12 != '') ?
                        <th className="align-middle" >Comprometido</th> : ""}
                    {(ALM20 != '') ?
                        <th className="align-middle" >Disponible</th> : ""}
                    {(ALM12 != '') ?
                        <th className="align-middle" >Disponible</th> : ""}
                    {(ALM20 != '') ?
                        <th className="align-middle" >Pedido</th> : ""}
                    {(ALM12 != '') ?
                        <th className="align-middle" >Pedido</th> : ""}
                    {(ALM36 != '') ?
                        <th className="align-middle" >Stock</th> : ""}
                    {(ALM24 != '') ?
                        <th className="align-middle" >Stock</th> : ""}
                    {(ALM36 != '') ?
                        <th className="align-middle" >Compromentido</th> : ""}
                    {(ALM24 != '') ?
                        <th className="align-middle" >Comprometido</th> : ""}
                    {(ALM36 != '') ?
                        <th className="align-middle" >Disponible</th> : ""}
                    {(ALM24 != '') ?
                        <th className="align-middle" >Disponible</th> : ""}
                    {(ALM36 != '') ?
                        <th className="align-middle" >Pedido</th> : ""}
                    {(ALM24 != '') ?
                        <th className="align-middle" >Pedido</th> : ""}

                    <th className="align-middle" >Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td className="td-cadena " id={Codigo}>{Codigo}</td>
                    <td className="td-cadena " >{Nombre}</td>
                    {(!ALM16 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Stock_16).toLocaleString("en")}</td> : ""}
                    {(!ALM01 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Stock_01).toLocaleString("en")}</td> : ""}
                    {(!ALM16 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Comprometido_16).toLocaleString("en")}</td> : ""}
                    {(!ALM01 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Comprometido_01).toLocaleString("en")}</td> : ""}
                    {(!ALM16 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Disponible_16).toLocaleString("en")}</td> : ""}
                    {(!ALM01 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Disponible_01).toLocaleString("en")}</td> : ""}
                    {(!ALM16 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Pedido_16).toLocaleString("en")}</td> : ""}
                    {(!ALM01 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Pedido_01).toLocaleString("en")}</td> : ""}
                    {(!ALM20 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Stock_20).toLocaleString("en")}</td> : ""}
                    {(!ALM12 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Stock_12).toLocaleString("en")}</td> : ""}
                    {(!ALM20 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Comprometido_20).toLocaleString("en")}</td> : ""}
                    {(!ALM12 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Comprometido_12).toLocaleString("en")}</td> : ""}
                    {(!ALM20 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Disponible_20).toLocaleString("en")}</td> : ""}
                    {(!ALM12 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Disponible_12).toLocaleString("en")}</td> : ""}
                    {(!ALM20 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Pedido_20).toLocaleString("en")}</td> : ""}
                    {(!ALM12 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Pedido_12).toLocaleString("en")}</td> : ""}
                    {(!ALM36 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Stock_36).toLocaleString("en")}</td> : ""}
                    {(!ALM24 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Stock_24).toLocaleString("en")}</td> : ""}
                    {(!ALM36 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Comprometido_36).toLocaleString("en")}</td> : ""}
                    {(!ALM24 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Comprometido_24).toLocaleString("en")}</td> : ""}
                    {(!ALM36 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Disponible_36).toLocaleString("en")}</td> : ""}
                    {(!ALM24 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Disponible_24).toLocaleString("en")}</td> : ""}
                    {(!ALM36 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Pedido_36).toLocaleString("en")}</td> : ""}
                    {(!ALM24 == '') ?
                        <td style={{ textAlign: "right" }} >{Number(Pedido_24).toLocaleString("en")}</td> : ""}
                    <td style={{ textAlign: "center" }}>
                        <button onClick={() => eventoVer(true)} title="Ver Detalle"><BsSearchHeartFill>Ver Detalle </BsSearchHeartFill></button>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>

                    <td>
                        <DownloadTableExcel
                            filename="seguimiento de importaciones"
                            sheet="seguimiento"
                            currentTableRef={TableRef.current}
                        >
                            <BotonExportar textoBoton={"Exportar Resultado"} sw_habilitado={true}></BotonExportar>
                        </DownloadTableExcel>
                    </td>

                </tr>
            </tfoot>
        </table>
    </div>
)

export default ResultadoTabla;