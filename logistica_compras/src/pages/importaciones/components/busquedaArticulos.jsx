import React from "react"; 
import { useEffect, useState } from "react"; 
import { GetListaArticulos } from "../../../services/importacionesServices";
import { BotonSeleccionar, BotonConsultar } from "../../../components/Botones"; 
 
export function BusquedaArticulos({ xCia, ResultadosBusqueda }) { 
    const [listArt, setlistArt] = useState([]) 
 
    useEffect(() => { 
 
    }) 
 
    const handleFind = async (e) => { 
        console.log(xCia)
        const responseJson = await GetListaArticulos(xCia, e.target.value) 
        setlistArt(responseJson) 
    } 
     
    return ( 
        <div> 
            <div className="card-header cardalign w-50rd"> 
                <div className="card-header border border-dashed border-end-0 border-start-0"> 
                    <div className="form-group"> 
                        <div className="form-group row"> 
                            <div className="col col-form-label"> 
                                Articulo : <input type="text" style={{width:400}} name="tArticulo" onChange={handleFind} /> 
                                <BotonConsultar sw_habilitado={true} eventoClick={() => handleFind()} /> 
                            </div> 
                        </div> 
                        <div className="form-group row"> 
                            <div className="table-responsive"> 
                                < table className="table table-hover table-sm table-bordered" > 
                                    <thead className="table-secondary text-center table-sm"> 
                                        <tr> 
                                            <td className="col-sm-auto col-form-label">Codigo</td> 
                                            <td className="col-sm-auto col-form-label">Nombre</td> 
                                            <td className="col-sm-auto"></td> 
                                        </tr> 
                                    </thead> 
                                    <tbody className="list"> 
                                        {listArt.length > 0 && 
                                            listArt.map((registro) => 
                                                <tr> 
                                                    <td className="td-cadena" id={registro.Codigo}>{registro.Codigo}</td> 
                                                    <td className="td-cadena" >{registro.Nombre}</td> 
                                                    <td className="td-cadema" > 
                                                        <span className="actions"> 
                                                            <button onClick={() => ResultadosBusqueda(registro.Codigo, registro.Nombre)}> 
                                                                <BotonSeleccionar /> 
                                                            </button> 
                                                        </span> 
 
                                                    </td> 
                                                </tr> 
                                            )} 
                                    </tbody> 
                                </table> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
    ) 
}