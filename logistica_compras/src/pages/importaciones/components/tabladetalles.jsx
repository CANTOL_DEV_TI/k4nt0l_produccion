import React from "react";
const ResultadoTablaDetalle = ({
    NroPedido,
    Cantidad,
    FechaEntrega,
    Almacen
}) =>
(
    <tbody>
        <tr>
            <td className="td-cadena " id={NroPedido}>{NroPedido}</td>                    
            <td className="td-cadena " >{FechaEntrega}</td>            
            <td style={{ textAlign: "right" }} >{Number(Cantidad).toLocaleString("en")}</td> 
        </tr>
    </tbody>
    
)

export default ResultadoTablaDetalle;