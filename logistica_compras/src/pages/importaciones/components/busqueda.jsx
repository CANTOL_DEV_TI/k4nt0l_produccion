import React, { useState } from "react";
import Title from "../../../components/Titulo";
import { BotonBuscar,BotonConsultar } from "../../../components/Botones";
import VentanaModal from "../../../components/VentanaModal";
import { BusquedaArticulos } from "./busquedaArticulos";

class Busqueda extends React.Component {

    constructor(props) {
        super(props);

        this.state = { cia: this.props.xCia, Codigo : '', Articulo: '', Tipo: '1', showModalA : false }
    }

    handleBusquedaA = (e) => {
        this.setState({ showModalA: true })
    }

    handleSettings = (e) => {
        console.log(this.state)
        if(e.target.name === "tcodarticulo"){
            this.setState({Codigo : e.target.value})
        }

        if (e.target.name === "tarticulo") {
            this.setState({ Articulo: e.target.value })
        }
        if (e.target.name === "stipo") {
            this.setState({ Tipo: e.target.value })
        }

    }

    handleShowModalA = (e) =>{
        this.setState({showModalA : true})
    }

    handleSetArticulo = (pArticulo,pNombre) =>{
        this.setState({Codigo : pArticulo, Articulo : pNombre, showModalA : false})
    }

    render() {

        const { handleBusqueda, handleModal, handleFiltrarPor, xCia } = this.props

        return (
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Reporte de Llegada de Importaciones</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    Codigo : <input name="tcodarticulo" disabled placeholder="Codigo del Articulo..." value={this.state.Codigo} onChange={this.handleSettings}/>
                                    Articulo : <input name="tarticulo" style={{ width: 400 }} disabled placeholder="Nombre de Articulo..." value={this.state.Articulo} onChange={this.handleSettings} />
                                    <BotonConsultar sw_habilitado={true} eventoClick={() => this.handleBusquedaA()}  /> 
                                    <VentanaModal 
                                        size={"xl"} 
                                        titulo={"Seleccione Articulo"} 
                                        show={this.state.showModalA} 
                                        handleClose={()=>this.setState({showModalA : false})} 
                                    > 
                                        <BusquedaArticulos xCia={this.props.xCia} ResultadosBusqueda={this.handleSetArticulo}/> 
                                    </VentanaModal> 
                                    Tipo : <select name="stipo" onChange={this.handleSettings} value={this.state.Tipo}>
                                        <option value={"1"}>NACIONAL</option>
                                        <option value={"2"}>IMPORTADO</option>
                                    </select>
                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => this.props.handleFiltrarPor(this.state)} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Busqueda; 