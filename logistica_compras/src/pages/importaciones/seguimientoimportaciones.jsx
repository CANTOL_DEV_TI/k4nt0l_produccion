import React, { useRef } from "react";
import Busqueda from "./components/busqueda";
import { GetListado, GetDetallexArticulo } from "../../services/importacionesServices";
import ResultadoTabla from "./components/tablaprincipal";
import { Validar } from "../../services/ValidaSesion";
import VentanaBloqueo from "../../components/VentanaBloqueo";
import VentanaModal from "../../components/VentanaModal";
import ResultadoTablaDetalle from "./components/tabladetalles";
import { DownloadTableExcel } from "react-export-table-to-excel";
import { downloadExcel } from "react-export-table-to-excel";
import { BotonExcel, BotonExportar } from "../../components/Botones";

//const tableRef = useRef(null);

class SeguimientoImportaciones extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: false,
            showModal: false,
            tipoOpe: 'Find',
            resultadosDetalle: [],
            almacen: "",
            TotalDet: 0,
            Cia: "",
            TableRef: React.createRef()            
        }


    }

    handleConsultar = async (pFiltro) => {
        console.log(pFiltro)

        console.log(this.state.Cia)

        let pArticulo = pFiltro.Codigo
        let pTipo = pFiltro.Tipo

        this.setState({ resultados: [], isFetch: true })
        const Reporte = await GetListado(this.state.Cia, pArticulo, pTipo)
        this.setState({ resultados: Reporte, isFetch: false })
        console.log(Reporte)
    }

    handleVerDetalle = async (pCia, pCodigo, pAlmacen) => {
        const ReporteDetalle = await GetDetallexArticulo(pCia, pCodigo, pAlmacen)
        this.setState({ resultadosDetalle: ReporteDetalle, showModal: true, almacen: pAlmacen })
        let c_Det = ReporteDetalle.reduce((dTotal, ActValor) => dTotal + Number(ActValor.Cantidad), 0);
        this.setState({ TotalDet: c_Det })

    }

    render() {
        const { isFetch, resultados, resultadosDetalle, showModal, VentanaSeguridad } = this.state;

        return (
            <React.Fragment>
                <Busqueda handleFiltrarPor={this.handleConsultar} xCia={this.state.Cia} />
                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Información'}
                { resultados.length > 0 ?
                    resultados.map((registro) =>
                        <ResultadoTabla
                            key={registro.ItemCodigo}
                            Cia={registro.CIA}
                            Codigo={registro.ItemCodigo}
                            Nombre={registro.ItemNombre}
                            ALM16={registro.ALM16}
                            Stock_16={registro.Stock_16}
                            Disponible_16={registro.Disponible_16}
                            Comprometido_16={registro.Comprometido_16}
                            Pedido_16={registro.Pedido_16}
                            ALM01={registro.ALM01}
                            Stock_01={registro.Stock_01}
                            Disponible_01={registro.Disponible_01}
                            Comprometido_01={registro.Comprometido_01}
                            Pedido_01={registro.Pedido_01}
                            ALM20={registro.ALM20}
                            Stock_20={registro.Stock_20}
                            Disponible_20={registro.Disponible_20}
                            Comprometido_20={registro.Comprometido_20}
                            Pedido_20={registro.Pedido_20}
                            ALM12={registro.ALM12}
                            Stock_12={registro.Stock_12}
                            Disponible_12={registro.Disponible_12}
                            Comprometido_12={registro.Comprometido_12}
                            Pedido_12={registro.Pedido_12}
                            ALM36={registro.ALM36}
                            Stock_36={registro.Stock_36}
                            Disponible_36={registro.Disponible_36}
                            Comprometido_36={registro.Comprometido_36}
                            Pedido_36={registro.Pedido_36}
                            ALM24={registro.ALM24}
                            Stock_24={registro.Stock_24}
                            Disponible_24={registro.Disponible_24}
                            Comprometido_24={registro.Comprometido_24}
                            Pedido_24={registro.Pedido_24}

                            eventoVer={() => this.handleVerDetalle(this.state.Cia, registro.ItemCodigo, this.state.Cia == "TCN" ? 'ALM36' : 'ALM14')} />
                    ):
                    ''
                }
                <VentanaModal titulo={"Ordenes de Compra vinculadas al almacen " + this.state.almacen} size={"lg"} handleClose={() => this.setState({ showModal: false })} show={this.state.showModal}>
                    <div className="table-responsive" style={{ height: '400px', display: "-ms-flexbox" }}>
                        <table className="table table-hover table-sm table-bordered table-striped" >
                            <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                                <tr><th colSpan={3}> </th></tr>
                                <tr >
                                    <th className="align-middle" >Nro Orden</th>
                                    <th className="align-middle" >Fecha Entrega</th>
                                    <th className="align-middle" >Cantidad</th>
                                </tr>
                            </thead>
                            {
                                resultadosDetalle.map((registrod) =>
                                    <ResultadoTablaDetalle
                                        key={registrod.NroPedido}
                                        NroPedido={registrod.NroPedido}
                                        Cantidad={registrod.Cantidad}
                                        FechaEntrega={registrod.FechaEntrega}
                                    />
                                )
                            }
                            <tfoot>
                                <th className="align-middle" ></th>
                                <th className="align-middle" ></th>
                                <th style={{ textAlign: "right" }} >{Number(this.state.TotalDet).toLocaleString("en")}</th>
                            </tfoot>

                        </table>
                    </div>
                </VentanaModal>
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}
                />

            </React.Fragment>
        )
    }
    async componentDidMount() {
        const ValSeguridad = await Validar("LOGIMP")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar, "Cia": ValSeguridad.cia, "Usuario": ValSeguridad.usuario })
        //console.log(ValSeguridad)
        //this.handleCiaNombre(ValSeguridad.cia)
    }
}

export default SeguimientoImportaciones;