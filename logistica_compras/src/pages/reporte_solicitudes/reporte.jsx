import React from "react";
import Busqueda from "./components/busqueda";
import { Validar } from "../../services/ValidaSesion";
import VentanaBloqueo from "../../components/VentanaBloqueo";
import VentanaModal from "../../components/VentanaModal";
import { ListadoReporteSol } from "../../services/confisolrqServices";
import ResultadoTabla from "./components/resultados";

class ReporteSolicitudes extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: false,
            showModal: false,
            Cia: "",
            VentanaSeguridad: false,
            Usuario: ""
        }
    }

    handleConsultar = async (pFiltro) => {
        console.log(pFiltro)
        let filtro = { cia: this.state.Cia, usuario: pFiltro.usuario, desde: pFiltro.desde, hasta: pFiltro.hasta, estado: pFiltro.estado, aprobador: pFiltro.aprobador }
        this.setState({ resultados: [], isFetch: true })
        const reporte = await ListadoReporteSol(filtro)
        this.setState({ resultados: reporte, isFetch: false })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad } = this.state;

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleConsultar} xCia={this.state.Cia} />
                {isFetch && 'Cargando...'}
                {(!isFetch && !resultados.length) && 'Sin Información'}
                <div className="table-responsive" style={{ height: '800px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered table-striped" >
                        <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                            <tr >
                                <th className="align-middle">#RQ</th>
                                <th className="align-middle" colSpan={2} >Centro Costo</th>
                                <th className="align-middle" >Pedido</th>
                                <th className="align-middle" >Necesario</th>
                                <th className="align-middle" >Vencimiento</th>
                                <th className="align-middle" >Registro</th>
                                <th className="align-middle" >Usuario</th>
                                <th className="align-middle" >SAP #Interno</th>
                                <th className="align-middle" >SAP #Documento</th>
                                <th className="align-middle" >Estado</th>
                            </tr>
                        </thead>
                        {resultados.length > 0 ?
                            resultados.map((registro) =>
                                <ResultadoTabla
                                    cia={registro.cia}
                                    codigo_rq={registro.codigo_rq}
                                    centro_costo_id={registro.centro_costo_id}
                                    cc_desc={registro.cc_desc}
                                    fecha_pedido={registro.fecha_pedido}
                                    fecha_necesaria={registro.fecha_necesaria}
                                    fecha_vencimiento={registro.fecha_vencimiento}
                                    fecha_registro={registro.fecha_registro}
                                    usuario={registro.usuario}
                                    sap_docentry={registro.SAP_DocEntry}
                                    sap_docnum={registro.SAP_DocNum}
                                    estado_desc={registro.estado_desc}
                                />
                            ) :
                            ''
                        }
                    </table>
                </div>
                <VentanaBloqueo show={this.state.VentanaSeguridad} />
            </React.Fragment >
        )
    }
    async componentDidMount() {
        const ValSeguridad = await Validar("LOGLIS")
        this.setState({ VentanaSeguridad: ValSeguridad.Validar, Cia: ValSeguridad.cia, Usuario: ValSeguridad.usuario })
    }
}

export default ReporteSolicitudes;