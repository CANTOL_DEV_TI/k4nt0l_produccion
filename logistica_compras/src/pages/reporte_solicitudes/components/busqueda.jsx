import React, { useState } from "react";
import Title from "../../../components/Titulo";
import { BotonBuscar, BotonConsultar, BotonNuevo } from "../../../components/Botones";

class Busqueda extends React.Component {

    constructor(props) {
        super(props);

        console.log(this.props.xCia)

        this.state = { cia: this.props.xCia, usuario: '', desde: '', hasta: '', estado: 'P', aprobador: '' }
    }

    handleSettings = (e) => {

        if (e.target.name === "tusuario") {
            this.setState({ usuario: e.target.value })
        }

        if (e.target.name === "taprobador") {
            this.setState({ aprobador: e.target.value })
        }

        if (e.target.name === "tdesde") {
            this.setState({ desde: e.target.value })
        }

        if (e.target.name === "thasta") {
            this.setState({ hasta: e.target.value })
        }

        if (e.target.name === "sestado") {
            this.setState({ estado: e.target.value })
        }

    }

    render() {

        const { handleBusqueda, handleModal, handleNuevo, xCia } = this.props;

        return (
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Listado de Requerimientos</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    Usuario : <input name="tusuario" placeholder="Ingrese Usuario..." value={this.state.usuario} onChange={this.handleSettings} />
                                    Aprobador : <input name="taprobador" placeholder="Ingrese Aprobador..." value={this.state.aprobador} onChange={this.handleSettings} />
                                    Desde : <input name="tdesde" placeholder="Desde..." type="date" value={this.state.desde} onChange={this.handleSettings} />
                                    Hasta : <input name="thasta" placeholder="Hasta..." type="date" value={this.state.hasta} onChange={this.handleSettings} />
                                    Estado : <select name="sestado" onChange={this.handleSettings} value={this.state.estado}>
                                        <option value={"P"}>Pendiente</option>
                                        <option value={"A"}>Aprobado</option>
                                        <option value={"E"}>Eliminado</option>
                                        <option value={"M"}>Migrado</option>
                                    </select>
                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => this.props.handleBusqueda(this.state)} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Busqueda; 