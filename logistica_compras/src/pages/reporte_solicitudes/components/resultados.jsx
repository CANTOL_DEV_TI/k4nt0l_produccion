import React from "react";

const ResultadoTabla = ({
    cia,
    codigo_rq,
    centro_costo_id,
    cc_desc,
    fecha_pedido,
    fecha_necesaria,
    fecha_vencimiento,
    fecha_registro,
    usuario,
    sap_docentry,
    sap_docnum,
    estado_desc
}) =>
(
    <tr >
        <th className="align-middle"  >{codigo_rq}</th>
        <th className="align-middle"  >{centro_costo_id}</th>
        <th className="align-middle"  >{cc_desc}</th>
        <th className="align-middle"  >{fecha_pedido}</th>
        <th className="align-middle"  >{fecha_necesaria}</th>
        <th className="align-middle"  >{fecha_vencimiento}</th>
        <th className="align-middle"  >{fecha_registro}</th>
        <th className="align-middle"  >{usuario}</th>
        <th className="align-middle"  >{sap_docentry}</th>
        <th className="align-middle"  >{sap_docnum}</th>
        <th className="align-middle"  >{estado_desc}</th>
    </tr>
)

export default ResultadoTabla;