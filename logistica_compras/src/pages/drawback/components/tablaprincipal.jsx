import React, { Component } from "react"; 
import { AgGridReact } from 'ag-grid-react'; 
import "ag-grid-community"; 
import 'ag-grid-community/styles/ag-grid.css'; 
import 'ag-grid-community/styles/ag-theme-alpine.css';
import 'ag-grid-community/styles/ag-theme-quartz.css';
//import { GroupCellRenderer } from "ag-grid-community"; 
import 'ag-grid-enterprise'; 

//import DetalleHijo from "./detallehijo";
 
class TablaPrincipal extends Component { 
    constructor(props) { 
        super(props); 
        this.state = { 
            columnDefs: [ 
                { headerName: "NroOrden", field: "nroorden" }, 
                { headerName: "Nivel_S", field: "nivel_s" ,hide:true}, 
                { headerName: "Codigo_Det", field: "codigo_det"/*, cellRenderer:'agGroupCellRenderer' */}, 
                { headerName: "Nombre_Det", field: "nombre_det" }, 
                { headerName: "Area", field: "area" }, 
                { headerName: "Procedencia", field: "procedencia"},
                { headerName: "Tipo", field: "tipo" }, 
                { headerName: "Ultimo Proveedor", field: "ultimo_proveedor" }, 
                { headerName: "Cantidad", field: "cantidad" }, 
                { headerName: "Precio", field: "precio" }, 
                { headerName: "Moneda", field: "moneda" }, 
                { headerName: "Hijos", field: "hijos" ,hide:true}                
            ], 
            rowData: this.props.Datos, 
            onGridReady : false, 
            defColumnsDefs : {flex : 1} 
        } 
 
        const defColumnsDefs = {flex : 1}; 
         
    } 
     
    render() { 
        const { Datos } = this.props 
 
        return ( 
            <div className="ag-theme-quartz" 
                style={{ 
                    height: "100%", 
                    width: "100%" 
                }}> 
                <AgGridReact 
                    columnDefs={this.state.columnDefs} 
                    rowData={this.props.Datos}  
                    defaultColDef={{flex : 1, filter : true}} 
                    onGridReady={true} 
                    masterDetail={true} 
                    //detailCellRenderer={(props) => <DetalleHijo{...props}/>}    
                    sideBar={false}         
                    detailRowHeight={600}                            
                    
                />             
            </div> 
        ) 
    } 
} 
 
export default TablaPrincipal;