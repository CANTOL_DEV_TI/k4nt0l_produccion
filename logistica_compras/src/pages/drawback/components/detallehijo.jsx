import { AgGridReact } from "ag-grid-react";
import React from "react";
import DetalleNieto from "./detallenieto";

const DetalleHijo = (props) => {

    const { data } = props;
    console.log(data.sublista)
    const columns = [
        { headerName: "NroOrden", field: "nroorden" },
        { headerName: "Nivel_S", field: "nivel_s" ,hide:true},
        { headerName: "Codigo_Det", field: "codigo_det", cellRenderer: 'agGroupCellRenderer' },
        { headerName: "Nombre_Det", field: "nombre_det" },
        { headerName: "Area", field: "area" }, 
        { headerName: "Procedencia", field: "procedencia"},
        { headerName: "Tipo", field: "tipo" }, 
        { headerName: "Ultimo Proveedor", field: "ultimo_proveedor" }, 
        { headerName: "Cantidad", field: "cantidad" },
        { headerName: "Precio", field: "precio" },
        { headerName: "Moneda", field: "moneda" },
        { headerName: "Hijos", field: "hijos" ,hide:true},
        { headerName: "SubLista", field: "sublista", hide: true }
    ]

    return (

        <div className="ag-theme-quartz"
            style={{
                height: "100%",
                width: "100%"
            }}>
            <AgGridReact
                rowData={data.sublista}
                columnDefs={columns}
                defaultColDef={{ flex: 1, filter:true }}                
                onGridReady={true}
                masterDetail={true}
                detailCellRenderer={(props) => <DetalleNieto{...props}/>}
                detailRowHeight={500}                            
            />
        </div>
    );
};

export default DetalleHijo;
