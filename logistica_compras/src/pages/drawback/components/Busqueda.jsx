import React, { useState } from "react"; 
import Title from "../../../components/Titulo"; 
import { BotonExcel, BotonConsultar, BotonBuscar } from "../../../components/Botones"; 
import VentanaModal from "../../../components/VentanaModal"; 
import { BusquedaArticulos } from "./BusquedaArticulos"; 
 
class Busqueda extends React.Component { 
 
    constructor(props) { 
        super(props); 
 
        this.state = { Codigo : '', Articulo: '', showModalA: false } 
    } 
     
    handleBusquedaA = (e) => { 
        this.setState({ showModalA: true }) 
    } 
 
    handleSettings = (xCodigo, xNombre) => { 
        this.setState({Codigo : xCodigo, Articulo : xNombre, showModalA:false}) 
         
    } 
 
 
    render() { 
 
        const { handleBusqueda, handleModal, handleFiltrarPor } = this.props 
 
        return ( 
            <div className="col-lg-12"> 
 
                <div className="card"> 
                    {/* INICIO BARRA DE NAVEGACION */} 
                    <div className="card-header border border-dashed border-end-0 border-start-0"> 
                        <div className="row align-items-center gy-3"> 
                            <div className="col-sm"> 
                                <Title>Reporte DrawBack</Title> 
                            </div> 
                            <div className="col-sm-auto"> 
                                <div className="d-flex flex-wrap gap-1"> 
                                    Codigo : <input name="tCodigo" style={{width:150}} placeholder="Codigo de Articulo..." readOnly value={this.state.Codigo} /> 
                                    Articulo : <input name="tArticulo" style={{width:600}} placeholder="Nombre de Articulo..." readOnly value={this.state.Articulo} /> 
                                    <BotonConsultar sw_habilitado={true} eventoClick={() => this.handleBusquedaA()}  /> 
                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={()=>this.props.handleFiltrarPor(this.state.Codigo)}/>  
                                    <VentanaModal 
                                        size={"xl"} 
                                        titulo={"Seleccione Articulo"} 
                                        show={this.state.showModalA} 
                                        handleClose={false} 
                                    > 
                                        <BusquedaArticulos ResultadosBusqueda={this.handleSettings}/> 
                                    </VentanaModal> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        ) 
    } 
 
} 
 
export default Busqueda; 
 
//eventoClick={() => handleBusquedaA()} />