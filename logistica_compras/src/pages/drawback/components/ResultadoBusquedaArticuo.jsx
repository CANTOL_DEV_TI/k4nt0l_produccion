import React from "react"; 
import {GrEdit,GrAmazon,GrCodeSandbox,GrTask} from "react-icons/gr"; 
import {AiFillDelete} from "react-icons/ai"; 
 
 
const ResultadoBusqueda = ({cod_articulo,                         
                        articulo,                                                                         
                        eventoSeleccionar                     
                        }) => 
    ( 
        <tbody> 
            <tr> 
                <td className="td-cadena" id={cod_articulo}>{cod_articulo}</td>                 
                <td className="td-cadena" >{articulo}</td>                                 
                <td style={{textAlign:"center"}}><button onClick={() => eventoSeleccionar(true)}><GrEdit/></button></td> 
                 
            </tr> 
        </tbody> 
    ) 
 
export default ResultadoBusqueda;