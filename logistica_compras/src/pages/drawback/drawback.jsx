import React from "react"; 
import { GetConsulta } from "../../services/drawbackServices"; 
import VentanaModal from "../../components/VentanaModal"; 
import VentanaBloqueo from "../../components/VentanaBloqueo"; 
import { Validar } from "../../services/ValidaSesion"; 
import TablaPrincipal from "./components/tablaprincipal"; 
import Busqueda from "./components/Busqueda"; 
 
class ReporteItems extends React.Component  
{ 
    constructor(props) { 
        super(props); 
 
        this.state = { 
            resultados : [], 
            isFetch : false, 
            dataRegistro : null, 
            showModal : false, 
            tipoOpe : 'Find'             
        } 
    } 
 
    handleBusqueda = async (pCodigo) => { 
        this.setState({isFetch : true})
        const responseJson = await GetConsulta(pCodigo); 
        console.log(pCodigo) 
        this.setState({resultados : responseJson}); 
        console.log(this.state.resultados) 
        this.setState({isFetch : false})
    } 
 
    render(){ 
        const{isFetch, resultados, showModal,VentanaSeguridad} = this.state 
 
        return ( 
            <React.Fragment> 
 
                {isFetch && 'Cargando'} 
                {(!isFetch && !resultados.length) && 'Sin Información'} 
                <Busqueda handleFiltrarPor={this.handleBusqueda} /> 
                <div className="table-responsive" style={{ height : '800px', display: "-ms-flexbox"}}> 
                    <TablaPrincipal Datos={this.state.resultados} /> 
                </div> 
                <VentanaBloqueo
                    show={this.state.VentanaSeguridad}
                />
            </React.Fragment> 
        ) 
    }
    async componentDidMount() {
        const ValSeguridad = await Validar("LOGDRA")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar, "Cia": ValSeguridad.cia, "Usuario": ValSeguridad.usuario })
        this.handleCiaNombre(ValSeguridad.cia)
    } 
} 
 
export default ReporteItems;