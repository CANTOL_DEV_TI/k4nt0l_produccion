import React, { useEffect, useState } from 'react'
import { getIndexOfList, moveItemFirstPlace, ordenarListaAlfabeticamente} from './Utils/array';
import { getCostoCenters, getCtgyxCuenta, getCuentaItem, getProjects, getPymtCond, getRqData_service, getUserData, postNewRequirement, updateRequirement } from './services/rqServices';
import { NuevoRequerimiento_UI } from './UI/nuevo';
import { makeNewRequestBody } from './utils';
import { useLocation, useNavigate } from 'react-router-dom';

const estado_text = {
    0: 'solicitar',
    1: 'aprobar'
  }


function NuevoRequerimiento() {
    const navigate = useNavigate()
    const {state} = useLocation();
    const {rqId, usrMode, editMode, estado_} = state || {rqId: undefined, usrMode: undefined, editMode: false, estado_: 'P'}; //estado es de la bandeja, se guarda para poder retornar

    const [newItemModal, setNewItemModal] = useState({title: 'Agregar Item', feetTitle: 'Agregar', show: {action: false, change: false}, size: 'md', isEdit: false}) // default isNew mode
    const [itemSelected, setItemSelected] = useState({idx:null, itemCode: '', itemName: '', rqDate: '', qty: 0, UOM: '', precio_sin_igv: 0})
    const [ mainRequest, setMainRequest ] = useState({userName: '', userEmail: '', usuario_empleadosap : null, cc: [], comments: '', items: [], provider: {
        cardCode: '',
        ruc: '',
        cardName: '',
        condPagoId: '-2',
        condPagoDesc: '',
    }})
    const [paymentsCond, setPaymentsCond] = useState({selected: '-2', options: []})
    const [currencyCond, setCurrencyCond] = useState({selected: 'S/', options: [{cy_codigo: '-1', cy_nombre: '--Select an option--'}].concat([{cy_codigo: 'S/', cy_nombre: 'S/'}, {cy_codigo: 'US$', cy_nombre: 'US$'}])})
    const [isLoading, setIsLoading] = useState(false)
    const [ buscadoraText, setBuscadoraText] = useState('')
    const [ bBuscadoraAtributos, setBBuscadoraAtributos] = useState({isLoading: false, isNameSelected: false, inputTextDisabled: false})

    //Estado para forzar ingreso de tipo de moneda
    const [currencyRequired, setCurrencyRequired] = useState(false)

    const handleNewItemModal = (obj) => setNewItemModal({...newItemModal, ...obj})
    
    const handleNewItem = (obj) => {
        setItemSelected({...itemSelected, ...obj});
    }

    const handleEditItem = (obj, itemCode, idx=null) =>{
        //idx: indice real del objeto seleccionado, viene de la tabla
            if (idx !== null) {
                handleNewItem({...mainRequest.items[idx], ...{idx: idx}});setNewItemModal({...newItemModal, ...obj});
            }
        }
    const handleMainRequest = (obj) => setMainRequest({...mainRequest, ...obj})
    const handlePaymentsCond = (obj) => setPaymentsCond({...paymentsCond, ...obj})
    const handleCurrencyCond = (obj) => setCurrencyCond({...currencyCond, ...obj})
    const handleBBuscadorAtributos = (obj) => setBBuscadoraAtributos({...bBuscadoraAtributos, ...obj}) //buscadora proveedores
    

    /**
     * Ventana de seguridad
     */

    // ventana seguridad
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
    const Seguridad = async () => {
    //   let meToken = sessionStorage.getItem("CDTToken");
    //   if (!!meToken){
    //     const ValSeguridad = await Validar("PROSIM");
    //     const nmd = await validaToken(meToken);
    //     const ValRol = await ValidaRol_by_UserId(nmd.sub.substring(3),4);
    //     setVentanaSeguridad(!ValRol || ValSeguridad.Validar);
    //   }else{
    //     setVentanaSeguridad(true);
    //   }
  }
  //
    
    /**
     * Delete one item.
     */
    const deleteItem = (itemCode, idx) => {
        // let itemIndex = getIndexOfList(mainRequest.items, itemCode, 'itemCode')
        const newItems = [...mainRequest.items];
        newItems.splice(idx, 1);
        handleMainRequest({items: newItems})
    }

    /**
     * Navegar a ventana anterior venta nuevo/editar RQ
     */
    const handleBackWindow = () => {
        // navigate('/bandejasolicitudes', {state: {rqId: rqId, usrMode: usrMode, estado: estado_}})
        navigate(`/bandeja/${estado_text[usrMode]}`, {state: {rqId: rqId, usrMode: usrMode, estado: estado_}})
    }

    /**
    * Change option selector Centro de costo.
    */
    const handleSelectorChange = (cc_id) => {
        // e.target.value: centro de costo codigo
        let unShiftedList = moveItemFirstPlace([...mainRequest.cc], cc_id, 'centro_costo')
        handleMainRequest({usuario_empleadosap: unShiftedList[0].usuario_empleadosap, cc: unShiftedList})
    }
    
    /**
     * On payment condition change
    */
   const handlePaymentCondChange = (e) => {
        // Aqui realiza dos actualizaciones
        handlePaymentsCond({selected: e.target.value.toString() }) //actualiza estado del selector
        let pymtSelected =  paymentsCond.options.filter((idy)=>idy.cp_codigo === Number(e.target.value))
        handleMainRequest({provider: {...mainRequest.provider, condPagoId: e.target.value.toString(), condPagoDesc: pymtSelected[0]?.cp_nombre || ''}}) //cambia cp del proveedor
    }
  
    /**
     * On payment condition change
    */
   const handleCurrencyCondChange = (e) => {
        // Aqui realiza dos actualizaciones
        if(e.target.value.toString() !== '-1'){
            setCurrencyRequired(false)
        }
        handleCurrencyCond({selected: e.target.value.toString()}) //actualiza estado del selector
        handleMainRequest({provider: {...mainRequest.provider, moneda: e.target.value.toString()}}) //cambia cp del proveedor
    }
  
    /**
     * On provider selected
    */
    const onProviderSelected = (obj) => {
        let money =  (typeof(obj?.moneda === 'string') && (obj.moneda !== '##')) ? obj?.moneda?.toString() || '-1' : '-1';
        // ## : condicion acepta todos los tipos de moneda
        handlePaymentsCond({selected: obj.condPagoId.toString()}) //actualiza condicion de pago
        if (typeof(obj?.moneda === 'string') && (obj.moneda !== '##')) {
            setCurrencyRequired(false)
            handleCurrencyCond({selected: money})
        }else if(typeof(obj?.moneda === 'string') && (obj.moneda === '##')) {
            setCurrencyRequired(true)
            handleCurrencyCond({selected: money})
        }
        handleMainRequest({provider: {...obj, ...{condPagoId: obj?.condPagoId.toString(), moneda: money}}}) //cambia cp del proveedor
    }

    /**
     * Save new requirement
     */
    const saveRequirement = async () => {
        if (mainRequest.items.length > 0 && mainRequest.cc[0].centro_costo !== '-1'){
            if(!editMode){
                setIsLoading(true)
                let newBodyRequest = makeNewRequestBody(rqId, mainRequest)
                const {data, status} = await postNewRequirement(newBodyRequest)
                setIsLoading(false)
                // // redirije
                // status === 201 && navigate('/bandejasolicitudes')
                status === 201 && navigate(-1)
            }else{
                setIsLoading(true)
                let newBodyRequest = makeNewRequestBody(rqId, mainRequest)
                const {data, status} = await updateRequirement(newBodyRequest)
                alert(data)
                setIsLoading(false)
                // redirije
                // status === 202 && navigate('/bandejasolicitudes')
                status === 202 && navigate(-1)
            }
        }else{
            mainRequest.cc[0].centro_costo === '-1' && alert('Debe seleccionar centro de costo')
            mainRequest.items.length === 0 && alert('Debe registrar artículos')
        }
    }

     /**
     * Initial render.
     */
    useEffect(()=>{
        const cargarDatos_CC = async (rqId_) => {
            const response = await getCostoCenters();
            const userData = await getUserData();
            const pymtCond = await getPymtCond();

            const pymt_Copied = ordenarListaAlfabeticamente([...pymtCond], 'cp_nombre') //lista que ordena alfabeticamente, primero van numeros
            const cc_Copied = [...response]
            
            if(editMode){
                const response = await getRqData_service(rqId_);
                
                // console.log('vista', response)
                let unShiftedList = moveItemFirstPlace([{centro_costo: '-1', cc_desc: '-- Seleccione una opción --', usuario_empleadosap: null}].concat(cc_Copied), response.centro_costo_id, 'centro_costo')
                let items = await Promise.all(response?.detalle?.map(async(item)=>{
                    //aca tiene que hacer la consulta de la cta contable
                    //aca tiene que hacer la consulta de la ctgy presupuestal
                    let ctacontable_codigo = item?.cuenta_linea === 'None' ? '-1' : item?.cuenta_linea
                    let catpre_codigo = item?.catpre_codigo === 'None' ? '-1' : item?.catpre_codigo
                    let proy_codigo = item?.proyecto === null ? '-1' : item?.proyecto
                    // console.log(item?.proyecto)

                    let ctascontable_item = await getCuentaItem(item?.itemcode) || [] //consulta opciones para cuentas contables
                    let catexcontable = await getCtgyxCuenta(ctacontable_codigo) || [] //consulta opciones para cate x contable
                    let proyectos = await getProjects() || [] // consulta opciones para proyecto

                    let fecha = new Date(item?.fecha_necesaria.toString())
                    fecha.setDate(fecha.getDate() + 1);
                    return {
                        UOM: item?.unidad_medida_id,
                        itemCode: item?.itemcode,
                        itemName: item?.itemname,
                        qty: item?.cantidad_requerida,
                        rqDate: fecha,
                        precio_sin_igv: item?.precio_sin_igv,
                        ctacontable: {selected: ctacontable_codigo || '-1', options: [{Cuenta:'-1', CuentaNombre: '--Select an option--'}].concat([...ctascontable_item])},
                        ctgyBudget: {selected: catpre_codigo || '-1', options: [{Cate_Codigo: '-1', Cate_Desc: '--Select an option--'}].concat([...catexcontable])},
                        project: {selected: proy_codigo || '-1', options: [{Proy_Codigo: '-1', Proy_Nombre: '--Select an option--'}].concat([...proyectos])}
                    }
                }))
                handleMainRequest({
                    userName: response.usuario_nombre, 
                    userEmail: response?.usuario_email || '', 
                    usuario_empleadosap: unShiftedList[0].usuario_empleadosap || null, 
                    cc: unShiftedList, 
                    comments: response.comentario,
                    items: items,
                    provider: {
                        cardCode:response?.proveedor_id,
                        ruc:response?.ruc_proveedor || '123456789',
                        cardName:response?.proveedor_nombre,
                        condPagoId: response?.forma_pago_id ||'-2',
                        condPagoDesc: response?.forma_pago_descripcion || '',
                        moneda: response?.moneda || '-1',
                    }
                });
                handlePaymentsCond({selected: response?.forma_pago_id || '-2', options: [{cp_codigo: '-2', cp_nombre: '-- Seleccione una opción --'}].concat(pymt_Copied)})
                handleCurrencyCond({selected: response?.moneda || 'S/', options: [{cy_codigo: '-1', cy_nombre: '--Select an option--'}].concat([{cy_codigo: 'S/', cy_nombre: 'S/'}, {cy_codigo: 'US$', cy_nombre: 'US$'}])})
            }else{
                handlePaymentsCond({options: [{cp_codigo: '-2', cp_nombre: '-- Seleccione una opción --'}].concat(pymt_Copied)})
                handleMainRequest({userName: userData.Usuario, userEmail: userData.Email, cc: [{centro_costo: '-1', cc_desc: '-- Seleccione una opción --', usuario_empleadosap: null}].concat(cc_Copied)})
            }
        }
        Seguridad()
        cargarDatos_CC(rqId)
    },[])

    /**
     * capta el cambio en el input de la buscadora cuando existe cardCode y cardName
     */
        useEffect(()=>{
            if((!!mainRequest?.provider?.ruc && !!mainRequest?.provider?.cardName)){
                setBuscadoraText(`${mainRequest?.provider?.ruc}-${mainRequest?.provider?.cardName}`)
                handleBBuscadorAtributos({inputTextDisabled: true})
            }else{
                setBuscadoraText('')
            }
        },[mainRequest.provider.cardCode])

    
    // console.log(mainRequest)
    //useEffect: 
    //caso 1: cuando ingresa nuevo item
    //caso 2: cuando edita item
    useEffect(()=>{

        const condiciones_iniciales = async () => {
        //verifica:
        //Exista itemName en itemSelected
        //Show false del newItemModal
        //Qty mayor a cero en itemSelected
        if(!!newItemModal?.show?.change && !newItemModal?.show?.action ){ //cuando existe cambio en nuevo item y cierra ventana
            if(!!itemSelected.itemName && itemSelected.qty > 0 &&  !newItemModal?.show?.action){
                let itemSelected_Copied = {...itemSelected} //item activo, copia
                let items_Copied = [...mainRequest.items] // lista items, copia
                if(!newItemModal.isEdit){ //Agrega
                    items_Copied.push(itemSelected_Copied)
                }else if(!!newItemModal.isEdit){ //Edita
                    //itemSelected.idx: este valor viene desde el inicio antes de que se abra la ventana editar
                    items_Copied.splice(itemSelected.idx, 1, itemSelected_Copied)
                }
                handleMainRequest({items: items_Copied})
                handleNewItem({itemCode: '', itemName: '', rqDate: new Date(), qty: 0, UOM: '', precio_sin_igv: 0, 
                ctacontable: {selected: '-1', options: [{Cuenta: '-1', CuentaNombre: '--Select an option--'}]},
                ctgyBudget: {selected: '-1', options: [{Cate_Codigo: '-1', Cate_Desc: '--Select an option--'}]}
                })
            }else if((!itemSelected.itemName || itemSelected.qty === 0) &&  !newItemModal?.show?.action){
                handleNewItem({itemCode: '', itemName: '', rqDate: new Date(), qty: 0, UOM: '', precio_sin_igv: 0, 
                ctacontable: {selected: '-1', options: [{Cuenta: '-1', CuentaNombre: '--Select an option--'}]}, 
                ctgyBudget: {selected: '-1', options: [{Cate_Codigo: '-1', Cate_Desc: '--Select an option--'}]}})
            }
        }else if(!newItemModal?.show?.action && !newItemModal?.show?.change){ //cuando no existe, setea todo a condiciones iniciales
            const proyectos = await getProjects()
            handleNewItem({itemCode: '', itemName: '', rqDate: new Date(), qty: 0, UOM: '', precio_sin_igv: 0, 
            ctacontable: {selected: '-1', options: [{Cuenta: '-1', CuentaNombre: '--Select an option--'}]},
            ctgyBudget: {selected: '-1', options: [{Cate_Codigo: '-1', Cate_Desc: '--Select an option--'}]},
            project: {selected: '-1', options: [{Proy_Codigo: '-1', Proy_Nombre: '--Select an option--'}].concat(proyectos)}
            })
        }
    }

        condiciones_iniciales()
    }, [newItemModal?.show?.action])

    
  return (
    <NuevoRequerimiento_UI
        handleNewItemModal={handleNewItemModal}
        handleNewItem={handleNewItem}
        handleEditItem={handleEditItem}
        handleMainRequest={handleMainRequest}
        deleteItem={deleteItem}
        handleSelectorChange={handleSelectorChange}
        saveRequirement={saveRequirement}
        handlePaymentCondChange={handlePaymentCondChange}
        handleCurrencyCondChange={handleCurrencyCondChange}
        onProviderSelected={onProviderSelected}
        setBuscadoraText={setBuscadoraText}
        handleBBuscadorAtributos={handleBBuscadorAtributos}
        handleBackWindow={handleBackWindow}
        mainRequest={mainRequest}
        newItemModal={newItemModal}
        itemSelected={itemSelected}
        paymentsCond={paymentsCond}
        currencyCond={currencyCond}
        editMode={editMode}
        isLoading={isLoading}
        buscadoraText={buscadoraText}
        bBuscadoraAtributos={bBuscadoraAtributos}
        currencyRequired={currencyRequired}
    />
  )
}

export default NuevoRequerimiento