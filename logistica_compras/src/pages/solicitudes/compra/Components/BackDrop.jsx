import { Backdrop, CircularProgress } from '@mui/material'
import React from 'react'

function MyBackDrop({show, msg}) {
  return (
    <div>
      <Backdrop
        sx={(theme) => ({ color: '#fff', zIndex: theme.zIndex.drawer + 1 })}
        open={show}
      >
        <div className='tw-flex tw-flex-col tw-items-center tw-gap-3'>
            <CircularProgress color="inherit" />
            {msg}
        </div>
      </Backdrop>
    </div>
  )
}

export {MyBackDrop} 