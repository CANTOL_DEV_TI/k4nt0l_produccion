import React, { useState } from 'react'
import { Dropdown, DropdownButton, Form, InputGroup } from 'react-bootstrap'
import DatePicker from 'react-datepicker'
import { es } from 'date-fns/locale';
import { BarraBuscadora } from './BarraBuscadora';
import { getCtgyxCuenta, getCuentaItem, getItemSap } from '../services/rqServices';
import { Box, Tab, Tabs } from '@mui/material';
import PropTypes from 'prop-types';
import { Selector } from './Combos';

function CustomTabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && <Box sx={{ py: 3 }}>{children}</Box>}
      </div>
    );
  }
  
  CustomTabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  };


function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

function NuevoItem({handleNewItem, item}) {
    const [results, setResults] = useState([])
    const [ bBuscadoraAtributos, setBBuscadoraAtributos] = useState({isLoading: false, isNameSelected: false})
    const handleBBuscadorAtributos = (obj) => setBBuscadoraAtributos({...bBuscadoraAtributos, ...obj})
    const [value, setValue] = useState(0); //control de tabs
    
    //cuando selecciona un item dentro de las opciones de la buscadora
    const handleOptionSelected = async (obj) => {
        const response = await getCuentaItem(obj?.itemCode) //obtiene cuenta contable
        if(!!response){
            // handleNewItem({...obj, ...{ctacontable: {selected: '-1', options: [{Cuenta: '-1', CuentaNombre: '--Select an option--'}].concat(response.map(
            //     (item) => ({Cuenta: item.Cuenta, CuentaNombre: `${item.Cuenta}-${item.CuentaNombre}`})))
            // }, 
            handleNewItem({...obj, ...{ctacontable: {selected: '-1', options: [{Cuenta: '-1', CuentaNombre: '--Select an option--'}].concat(response.map(
                (item) => ({Cuenta: item.Cuenta, CuentaNombre: `${item.CuentaNombre}`})))
            }, 
            ctgyBudget: {selected: '-1', options: [{Cate_Codigo: '-1', Cate_Desc: '--Select an option--'}]},
            }})
        }else{
            handleNewItem(obj)
        }
        //agrega la lista de cuenta contable
        setResults([])
    }

    //cuando escriben en la barra de busqueda
    const handleTextInputChange = async (e) => {
        const nameValue = e.target.value;
        handleNewItem({itemCode: nameValue.toUpperCase()})
        handleBBuscadorAtributos({isNameSelected: false})
        setResults([]);
        if (nameValue.length > 1) {
            handleBBuscadorAtributos({isLoading: true})
            let response = await getItemSap(nameValue.toUpperCase())
            setResults(response.slice(0,100)); //muestra los 10 primeros resultados
            handleBBuscadorAtributos({isLoading: false})
        }
    }

    //cuando cambia el valor de selector de cuenta contable
    const handleCtaContableSelectorChange = async (ctaContableCodigo) => {
        //aqui tiene que hacer la consulta
        const response = await getCtgyxCuenta(ctaContableCodigo) //consulta la categoria presupuestal para ctaContableCodigo
        handleNewItem({ctgyBudget: {selected: '-1', options: [{Cate_Codigo: '-1', Cate_Desc: '--Select an option--'}].concat([...response])}, ctacontable: {selected: ctaContableCodigo, options: item?.ctacontable?.options}})
    }
    
    //cuando cambia el valor de selector de la categoria presupuestal
    const handleCtgyPresupuestalSelectorChange = async (ctgyPresupuestal) => {
        handleNewItem({ctgyBudget: {selected: ctgyPresupuestal, options: item?.ctgyBudget?.options}})
        // handleNewItem({ctgyBudget: {selected: '-1', options: [{Cate_Codigo: '-1', Cate_Desc: '--Select an option--'}].concat([...response])}, ctacontable: {selected: ctaContableCodigo, options: item?.ctacontable?.options}})
    }
    
    //cuando cambia el valor de selector del proyecto
    const handleProyecto = async (Proy_Codigo) => {
        handleNewItem({project: {selected: Proy_Codigo, options: item?.project?.options}})
    }

    // actualizar cantidad
    const updateQty = async (e) => {
        let qty = e.target.value;
        handleNewItem({qty: Number(qty || 0)})
    }
    
    // actualizar precio
    const updatePrice = async (e) => {
        let price = e.target.value;
        handleNewItem({precio_sin_igv: Number(price || 0)})
    }
    
    // actualizar fecha necesaria
    const handleDateChange = (date) => {
        handleNewItem({rqDate: date})
    };

    //control de tabs
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
    <div>
        <Box sx={{ width: '100%', height: '410px' }}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                    <Tab label="General" {...a11yProps(0)} />
                    <Tab label="Finanzas" {...a11yProps(1)} />
                </Tabs>
            </Box>
            <CustomTabPanel value={value} index={0}>
                <div className='tw-w-full tw-px-3'>
                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Código de artículo:</Form.Label>
                        <BarraBuscadora 
                            dataFounded={results} 
                            bBuscadoraAtributos={bBuscadoraAtributos}
                            handleOptionSelected={handleOptionSelected}
                            handleTextInputChange={handleTextInputChange}
                            value={item.itemCode}
                        />
                    </Form.Group>

                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Descripción de artículo:</Form.Label>
                        <Form.Control size='sm' value={item.itemName} readOnly={true}  disabled/>
                    </Form.Group>

                    <Form.Group className="mb-2 tw-flex tw-flex-col" controlId="formDate">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Fecha necesaria:</Form.Label>
                        <DatePicker
                            selected={item.rqDate}
                            onChange={handleDateChange}
                            locale={es}
                            className="form-control tw-w-full tw-h-8 !tw-text-sm" // Aplica el estilo de Bootstrap
                            dateFormat="dd/MM/yyyy" // Define el formato de la fecha
                            placeholderText="Seleccionar fecha"
                            size='sm'
                        />
                    </Form.Group>
                    
                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Cantidad requerida:</Form.Label>
                        <Form.Control placeholder="012..."  size='sm' type='number' value={item.qty.toString()} onChange={updateQty}/>
                    </Form.Group>
                    
                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Unidad de medida:</Form.Label>
                        <Form.Control size='sm' disabled value={item.UOM} readOnly={true}/>
                    </Form.Group>
                </div>
            </CustomTabPanel>
            <CustomTabPanel value={value} index={1}>
                <div className='tw-w-full tw-px-3'>
                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Cuenta contable:</Form.Label>
                        <Selector 
                            options={!!item?.ctacontable ? item?.ctacontable?.options : []} 
                            curIndex={item?.ctacontable?.selected || '-1'} 
                            id={'Cuenta'} 
                            dsc={'CuentaNombre'}
                            handleSelectChange={(e)=>handleCtaContableSelectorChange(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Categoria presupuestal:</Form.Label>
                        <Selector 
                            options={!!item?.ctgyBudget ? item?.ctgyBudget?.options : []} 
                            curIndex={item?.ctgyBudget?.selected || '-1'} 
                            id={'Cate_Codigo'} 
                            dsc={'Cate_Desc'}
                            handleSelectChange={(e)=>handleCtgyPresupuestalSelectorChange(e.target.value)}
                        />
                    </Form.Group>
              
                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Proyecto:</Form.Label>
                        <Selector 
                            options={!!item?.project ? item?.project?.options : []} 
                            curIndex={item?.project?.selected || '-1'}
                            id={'Proy_Codigo'}
                            dsc={'Proy_Nombre'}
                            handleSelectChange={(e)=>handleProyecto(e.target.value)}
                        />
                    </Form.Group>
                    
                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'>Precio sugerido (sin IGV):</Form.Label>
                        <Form.Control size='sm' type='number' value={item?.precio_sin_igv?.toString()} onChange={updatePrice} />
                    </Form.Group>

                </div>
            </CustomTabPanel>
        </Box>
    </div>
  )
}

export default NuevoItem
