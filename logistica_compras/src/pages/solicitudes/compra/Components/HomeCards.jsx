import React from 'react'
import { BsCheckCircle, BsClipboard2Check, BsFileEarmarkX, BsPaperclip, BsXCircle} from 'react-icons/bs'
import { Card } from 'react-bootstrap'
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import PropTypes from 'prop-types';
import { Badge } from '@mui/material';
import { useMediaQuery } from 'react-responsive';


function CustomTabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && 
            <div className='!tw-pt-2 !tw-flex !tw-justify-start'> 
                {children} 
            </div>
        }
      </div>
    );
  }

  /**
   * 
   */
  
  CustomTabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  };

  /**
   * 
   */
  
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

const dataTargetas_Claimer = [
    {
        icon: <BsPaperclip size={40} color='white'/>, 
        cardTitle: 'Pendientes', 
        cardIconColor: 'warning',
        ref: 'P'
    },
    {
        icon: <BsCheckCircle size={40} color='white'/>, 
        cardTitle: 'Aprobados', 
        cardIconColor: 'success',
        ref: 'A'
    },
    {
        icon: <BsFileEarmarkX size={40} color='white'/>,
        cardTitle: 'Rechazados', 
        cardIconColor: 'secondary',
        ref: 'R',
    },
    {
        icon: <BsClipboard2Check size={40} color='white'/>, 
        cardTitle: 'Migrado a SAP', 
        cardIconColor: 'info',
        ref: 'M'
    },
    {
        icon: <BsXCircle size={40} color='white'/>, 
        cardTitle: 'Eliminados', 
        cardIconColor: 'danger',
        ref: 'E'
    },
]

const dataTargetas_Approver = [
    {
        icon: <BsPaperclip size={40} color='white'/>, 
        cardTitle: 'Pendientes', 
        cardIconColor: 'warning',
        ref: 'P'
    },
    {
        icon: <BsCheckCircle size={40} color='white'/>, 
        cardTitle: 'Aprobados', 
        cardIconColor: 'success',
        ref: 'A'
    },
    {
        icon: <BsFileEarmarkX size={40} color='white'/>, 
        cardTitle: 'Rechazados', 
        cardIconColor: 'secondary',
        ref: 'R'
    },
    {
        icon: <BsClipboard2Check size={40} color='white'/>, 
        cardTitle: 'Migrado a SAP', 
        cardIconColor: 'info',
        ref: 'M'
    },
    {
        icon: <BsXCircle size={40} color='white'/>, 
        cardTitle: 'Eliminados', 
        cardIconColor: 'danger',
        ref: 'E'
    },
    // {
        //     icon: <BsDashCircle size={40} color='white'/>, 
        //     cardTitle: 'No migrados', 
        //     cardIconColor: 'danger',
        //     ref: 'N'
        // }
]

const dataCard_keys = {
     A: 'Aprobados',
     E: 'Eliminados',
     M: 'Migrados',
     P: 'Pendientes',
     R: 'Rechazados',
}

function HomeCards({handlerFilter, usrMode, handlerUserMode, filter, dataCard}) {
    const isReprocesoMobil = useMediaQuery({ query: '(max-width: 570px)'})
    return (
        <div className="tw-w-full tw-py-1 tw-gap-3">
            <div className='tw-h-fit'>
                {/* <Tabs value={usrMode} onChange={()=>{'disabled'}} aria-label="basic tabs example"> */}
                <Tabs value={0} onChange={()=>{'disabled'}} aria-label="basic tabs example">
                    {!usrMode ? (
                        <Tab label="Requerimientos" {...a11yProps(0)} />
                    ):
                    (
                        <Tab label="Aprobaciones" {...a11yProps(1)} />
                    )}
                </Tabs>
            </div>

        {!usrMode ? (
        
        <CustomTabPanel value={usrMode} index={0}>
            <div className={`tw-w-full tw-py-1 tw-flex ${isReprocesoMobil ? 'tw-justify-center' : ''} tw-gap-3 tw-flex-wrap`}>
                {
                    dataTargetas_Claimer.map((data, index)=>{
                        return (
                            // <Badge color="info" variant="dot" invisible={data?.ref !== filter?.estado} key={data.cardTitle}>
                                <Tarjeta
                                    key={data.cardTitle}
                                    handlerFilter={handlerFilter} 
                                    ref_={data.ref} 
                                    icon={data.icon} 
                                    cardTitle={data.cardTitle} 
                                    cardIconColor={data.cardIconColor} 
                                    value={!!dataCard ? dataCard[dataCard_keys[data?.ref]] : 0}
                                    invisible={data?.ref !== filter?.estado}
                                />
                            // </Badge>
                        )
                    })
                }
            </div>
        </CustomTabPanel> ) : (

        <CustomTabPanel value={usrMode} index={1}>
            <div className={`tw-w-full tw-py-1 tw-flex ${isReprocesoMobil ? 'tw-justify-center' : ''} tw-gap-3 tw-flex-wrap`}>
                {
                    dataTargetas_Approver.map((data, index)=>(
                        // <Badge color="info" variant="dot" invisible={data?.ref !== filter?.estado} key={data.cardTitle}>
                            <Tarjeta
                                key={data.cardTitle}
                                handlerFilter={handlerFilter} 
                                ref_={data.ref} icon={data.icon} 
                                cardTitle={data.cardTitle} 
                                cardIconColor={data.cardIconColor} 
                                value={!!dataCard ? dataCard[dataCard_keys[data?.ref]] : 0}
                                invisible={data?.ref !== filter?.estado}
                            />
                        // </Badge>
                    ))
                }
            </div>
        </CustomTabPanel>
        )}
    </div>
  )
}


function Tarjeta ({handlerFilter, ref_, icon, cardTitle, cardIconColor, value = 0, invisible}){
    return (
        // <Card className='tw-rounded-lg card_shadow tw-flex tw-flex-1' style={{minWidth: '320px', cursor: 'pointer', width: '100%'}} >
        <Card className='tw-rounded-lg card_shadow tw-flex tw-flex-1' style={{minWidth: '250px', maxWidth: '320px'}}>
            <div className={`tw-h-[88px] ${!invisible ? "tw-bg-[rgba(60,141,188,0.5)]" : ''} tw-rounded-lg tw-flex tw-items-center tw-gap-2`}
                style={{padding: '16px 30px 16px 16px'}}
                onClick={()=>{!!handlerFilter && handlerFilter({estado: ref_})}}
            >
                <div className={`icon tw-w-16 tw-h-16 bg-${cardIconColor} tw-rounded-lg tw-flex tw-justify-center tw-items-center`}>
                    {icon}
                </div>
                <div className='tw-py-2 tw-flex tw-flex-col tw-justify-center'>
                    <h2 className='tw-text-xl tw-font-semibold tw-mb-0 tw-text-gray-800 tw-font-sans'>{value}</h2>
                    <p className='tw-py-0 tw-mb-0'>{cardTitle}</p>
                </div>
            </div>
        </Card>
    )
}

export {HomeCards}


