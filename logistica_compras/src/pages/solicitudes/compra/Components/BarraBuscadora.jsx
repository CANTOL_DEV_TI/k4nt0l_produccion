import React, { useState } from 'react'
import { Form, ListGroup, Spinner } from 'react-bootstrap'
import { getProviderSap } from '../services/rqServices'
import '../style/BarraBuscadora.css'

function BarraBuscadora({dataFounded: results, bBuscadoraAtributos, handleOptionSelected, handleTextInputChange, value}) {
    let results_ = Array.isArray(results) ? results : [];
    return (
    <Form.Group className="typeahead-form-group">
        <Form.Control type="text" autoComplete="off" size='sm' value={value} onChange={handleTextInputChange} placeholder='Buscar por código o nombre'/>
        <ListGroup className="typeahead-list-group">
            {!bBuscadoraAtributos.isNameSelected &&
            results_.length > 0 && 
            <div className='tw-h-80 tw-overflow-y-scroll'>
                {
            results_.map((result) => (
                <ListGroup.Item
                key={result.ItemCode}
                className="typeahead-list-group-item"
                style={{padding: '5px 10px'}}
                onClick={() => handleOptionSelected({
                    itemCode: result.ItemCode,
                    itemName: result.ItemName,
                    UOM: result.Und,
                    precio_sin_igv: result?.UltimoPrecio
                })}
                >
                {`${result.ItemCode}-${result.ItemName}`}
                </ListGroup.Item>
            ))
                }
            </div>
            }
            {!results_.length && bBuscadoraAtributos.isLoading && (
            <div className="typeahead-spinner-container">
                <Spinner animation="border" variant='info'/>
            </div>
            )}
        </ListGroup>
    </Form.Group>
  )
}

function BarraBuscadora_proveedores({handleBBuscadorAtributos, bBuscadoraAtributos, buscadoraText, setBuscadoraText, onProviderSelected}) {
    const [results, setResults] = useState([])

    const handleProvider = (obj) => onProviderSelected(obj)

    // const handleSelectedItem = (obj) => setBBuscadoraAtributos({...bBuscadoraAtributos, ...obj})

    //cuando escriben en la barra de busqueda
    const handleTextInputChange = async (e) => {
        const nameValue = e.target.value;
        setBuscadoraText(nameValue.toUpperCase())
        handleBBuscadorAtributos({isNameSelected: false})
        setResults([]);
        if (nameValue.length > 1) {
            handleBBuscadorAtributos({isLoading: true})
            let response = await getProviderSap(nameValue.toUpperCase())
            setResults(response.slice(0,10)); //muestra los 10 primeros resultados
            handleBBuscadorAtributos({isLoading: false})
        }
    }

    const handleOptionSelected = (obj) => {
        handleProvider(obj)
        setBuscadoraText(`${obj.ruc}-${obj.cardName}`)
        handleBBuscadorAtributos({inputTextDisabled: true})
        setResults([])
    }

    return (
    <Form.Group className="typeahead-form-group tw-w-3/4">
        <Form.Control type="text" autoComplete="off" size='sm' value={buscadoraText} onChange={handleTextInputChange} placeholder='Buscar por ruc o razon social' disabled={bBuscadoraAtributos.inputTextDisabled}/>
        <ListGroup className="typeahead-list-group">
            {!bBuscadoraAtributos.isNameSelected &&
            results.length > 0 &&
            results.map((result) => (
            <ListGroup.Item
                key={result.CardCode}
                className="typeahead-list-group-item"
                style={{padding: '5px 10px'}}
                onClick={() => handleOptionSelected({
                    cardCode: result.CardCode,
                    ruc: result.RUC,
                    cardName: result.CardName,
                    condPagoId: result.CondPagoID,
                    condPagoDesc: result.CondPagoDes,
                    moneda: result.Moneda
                })}
            >
                {`${result.RUC}-${result.CardName}`}
            </ListGroup.Item>
            ))}
            {!results.length && bBuscadoraAtributos.isLoading && (
            <div className="typeahead-spinner-container">
                <Spinner animation="border" variant='info'/>
            </div>
            )}
        </ListGroup>
    </Form.Group>
  )
}

export {BarraBuscadora, BarraBuscadora_proveedores} 