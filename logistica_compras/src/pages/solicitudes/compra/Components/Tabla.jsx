import { Button, Table } from "react-bootstrap";
import { BsEye, BsPencilFill, BsPlusSquareFill, BsX, BsXSquareFill } from "react-icons/bs";
import { getCurrentDate, getCurrentDate_YYYYMMDD2DD_MM_YYYY } from "../Utils/calendar";
import { Checkbox, Tooltip, tooltipClasses } from "@mui/material";
import { styled } from '@mui/material/styles';
import { arrayToString } from "../Utils/array";

const CustomWidthTooltip = styled(({ className, ...props }) => (
    <Tooltip {...props} classes={{ popper: className }} />
  ))({
    [`& .${tooltipClasses.tooltip}`]: {
      maxWidth: 500,
      whiteSpace: 'pre-line',
    },
  });

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };


function Tabla_Solicitudes({ usrMode, datos_fila, openRqView, openRqEdit, openRqDelete, enButtons, massiveMode, checkRq, filter}) {
    // RETORNAR INFORMACIÓN
    const {editBtn, deleteBtn} = enButtons;
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle tw-w-12 tw-text-sm">ID</th>
                            {/* analiza solo cuando esta migrado */}
                            {filter.estado === 'M' && <th className="align-middle tw-w-24 tw-text-sm">Doc. SAP</th>} 
                            <th className="align-middle tw-w-96 tw-text-sm">Centro de costo</th>
                            {!!usrMode && (<th className="align-middle tw-w-60 tw-text-sm">Solicitante</th>)}
                            <th className="align-middle tw-w-56 tw-text-sm">F. Registro</th>
                            <th className="align-middle tw-w-56 tw-text-sm">F. Necesaria</th>
                            <th className="align-middle tw-w-56 tw-text-sm">Estado</th>
                            {
                                !massiveMode ?
                                (<th className="align-middle tw-text-sm" style={{width: '50px'}}>Operación</th>):
                                (<th className="align-middle tw-text-sm" style={{width: '20px'}}>✔</th>)

                            }
                        </tr>
                    </thead>

                    <tbody className="list">
                            {datos_fila.length > 0 ?
                            (   
                                datos_fila.map((dato, index) => {
                                    return(
                                        <tr key={index + 1}>
                                            <td className="td-cadena py-2"><span className="tw-text-sm">{dato?.codigo_rq}</span></td>
                                            {/* analiza solo cuando esta migrado */}
                                            {filter.estado === 'M' && <td className="td-cadena py-2"><span className="tw-text-sm">{dato?.sap_docnum}</span></td>}
                                            <td className="td-cadena"><span className="tw-text-sm">{dato?.centro_costo_desc}</span></td>
                                            {!!usrMode && (<td className="td-cadena"><span className="tw-text-sm">{dato?.usuario}</span></td>)}
                                            <td className="td-cadena"><span className="tw-text-sm">{getCurrentDate_YYYYMMDD2DD_MM_YYYY(dato.fecha_pedido)}</span></td>
                                            <td className="td-cadena"><span className="tw-text-sm">{getCurrentDate_YYYYMMDD2DD_MM_YYYY(dato.fecha_necesaria)}</span></td>
                                            <td className="td-cadena"><span className="tw-text-sm">{dato?.estado_des?.toUpperCase()}</span></td>


                                            {
                                                !massiveMode ?
                                                (
                                                <td className="td-cadena">
                                                    <div className="tw-flex tw-justify-center tw-gap-2">
                                                        <CustomWidthTooltip title={arrayToString(dato?.detalles, ['itemcode','itemname','cantidad_requerida'])} placement="left">
                                                            <Button variant="outline-info" className="!tw-px-2" onClick={()=>{openRqView(dato?.codigo_rq)}}><BsEye size={16}/></Button>
                                                        </CustomWidthTooltip>
                                                        <Button variant={deleteBtn?"outline-warning":"dark"} className="!tw-px-2" disabled={!deleteBtn} onClick={()=>openRqEdit(dato?.codigo_rq)}><BsPencilFill variant='danger' size={16}/></Button>
                                                        <Button variant={deleteBtn?"outline-danger":"dark"} className="!tw-px-2" disabled={!deleteBtn} onClick={()=>openRqDelete(dato?.codigo_rq, dato?.centro_costo_id)}><BsX variant='danger' size={16}/></Button>
                                                    </div>
                                                </td>):
                                                (
                                                    <Checkbox {...label} checked={dato?.isChecked} onChange={()=>checkRq(dato?.codigo_rq)} color="success" />
                                                )
                                            }
                                        </tr>
                                        )
                                }
                            )
                        ) :
                        (
                            <tr>
                                <td className="text-center" colSpan="10">Sin datos</td>
                            </tr>
                        )
                        }
                    </tbody>

                </table>
            </div>
            {/* FIN TABLA */}
        </>
    );
}

function Tabla_Contenido({ datos_fila, handleNewItem, deleteItem, handleEditItem}) {
    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <div className="table-responsive">
                <Table  className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm">
                        <tr>
                            <th className="align-middle tw-w-48 tw-text-sm">Cod. SAP</th>
                            <th className="align-middle tw-w-96 tw-text-sm">Descripción</th>
                            <th className="align-middle tw-w-40 tw-text-sm">Cant. Requerida</th>
                            <th className="align-middle tw-w-44 tw-text-sm">Unidad de medida</th>
                            <th className="align-middle tw-w-44 tw-text-sm">F. Necesaria</th>
                            <th className="align-middle tw-w-36 tw-text-sm">Operación</th>
                        </tr>
                    </thead>

                    <tbody className="list">
                            {datos_fila?.length > 0 ?
                            (   
                                datos_fila.map((dato, index) => {
                                    return(
                                        <tr key={index + 1}>
                                            <td className="td-cadena py-2"><span className="tw-text-sm">{dato.itemCode}</span></td>
                                            <td className="td-cadena"><span className="tw-text-sm">{dato.itemName}</span></td>
                                            <td className="td-cadena"><span className="tw-text-sm">{dato.qty}</span></td>
                                            <td className="td-cadena"><span className="tw-text-sm">{dato.UOM}</span></td>
                                            <td className="td-cadena"><span className="tw-text-sm">{getCurrentDate(dato.rqDate, '/')}</span></td>
                                            <td className="td-cadena">
                                                <div className="tw-flex tw-justify-center tw-gap-2">
                                                    {/* <Button variant="outline-warning" className="!tw-px-2" onClick={()=>handleEditItem({show: {action: true, change: false}, title: 'Editar item', feetTitle: 'Grabar', isEdit: true}, dato.itemCode)}><BsPencilFill variant='danger' size={16}/></Button> */}
                                                    <Button variant="outline-warning" className="!tw-px-2" onClick={()=>handleEditItem({show: {action: true, change: false}, title: 'Editar item', feetTitle: 'Grabar', isEdit: true}, dato.itemCode, index)}><BsPencilFill variant='danger' size={16}/></Button>
                                                    <Button variant="outline-danger" className="!tw-px-2" onClick={()=>{deleteItem(dato.itemCode, index)}}><BsXSquareFill variant='danger' size={16}/></Button>
                                                </div>
                                            </td>
                                        </tr>
                                        )
                                }
                            )
                        ) :
                        (
                            <tr>
                                <td className="text-center" colSpan="10">Sin datos</td>
                            </tr>
                        )
                        }
                        <tr className="tw-relative">
                            <td className="text-center tw-p-0" style={{padding: '0px', height: '37px'}} colSpan="6">
                                <Button className="tw-absolute tw-top-0" variant="success" 
                                style={{borderRadius: '0px 0px 8px 8px', height: '25px', lineHeight:'20px', fontSize: '15px', padding: '2px 10px'}}
                                onClick={handleNewItem}
                                >
                                    <BsPlusSquareFill/>
                                </Button>
                            </td>
                        </tr>
                    </tbody>

                </Table >
            </div>
            {/* FIN TABLA */}
        </>
    );
}

export {Tabla_Solicitudes, Tabla_Contenido}