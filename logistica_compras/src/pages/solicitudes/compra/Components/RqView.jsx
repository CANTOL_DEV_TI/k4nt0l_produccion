import React, { useEffect, useState } from 'react'
import { Button, FloatingLabel, Form} from 'react-bootstrap';
import { useLocation, useNavigate } from 'react-router-dom';
import { getRqData_service, rjctRq_service, upGradeRq_service} from '../services/rqServices';
import { getCurrentDate_YYYY_MM_DD2DD_MM_YYYY } from '../Utils/calendar';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Badge } from '@mui/material';
import { get_User_Company } from '../Utils/jwt';
import {VentanaModal} from './VentanaModal';
// import 'bootstrap/dist/css/bootstrap.min.css'; //esto esta interfiriendo


const estados = {
    'P': 'PENDIENTE DE APROBACIÓN',
    'A': 'APROBADO',
    'R': 'RECHAZADO',
    'M': 'MIGRADO',
    'E': 'ELIMINADO',
}
const empresas = {
    'DTM': 'DISTRIMAX',
    'CNT': 'CANTOL',
    'TCN': 'TECNOPRESS'

}

const estado_color = {
  'P': 'warning',
  'A': 'success',
  'R': 'error',
}

const estado_text = {
  0: 'solicitar',
  1: 'aprobar'
}

// function delay(ms) {
//   return new Promise(resolve => setTimeout(resolve, ms));
// }

function RqView() {
    //aqui se tiene que pasar el id del rq
    const navigate = useNavigate();
    const {state} = useLocation();
    const {rqId, usrMode, estado_} = state;

    const [ rqReview, setRqReview ] = useState(null)
    const [ commetsModal, setCommetsModal] = useState({title: 'Ingresar observaciones', feetTitle: 'Aprobar', show: false, size: 'md', btnFeetVariant: 'success', data: {rqId: null},
    isLoadingOk: false,
    isLoadingRej: false,
    })
    const [ upgraderComments, setUpgraderComments ] = useState('')

    //handlers
    const handleCommentModal = (obj) => setCommetsModal({...commetsModal, ...obj})
    const openCommentTxtArea = (codigo_rq) => {setUpgraderComments(''),handleCommentModal({show: true, data: {rqId: codigo_rq}})}
    const onTextAreaCommentsChange = (e) => setUpgraderComments(e.target.value)
    
    /**
     * Valida Rq por usuario activo
     * @param {integer} data2Modal 
     */
    const doValidation = async (data2Modal) => {
      handleCommentModal({isLoadingOk: true})
      const response = await upGradeRq_service(data2Modal?.rqId, upgraderComments)
      if(response){
        alert(`Requerimiento ${data2Modal?.rqId}, aprobado con éxito`)
      }else{
        alert(`Requerimiento ${data2Modal?.rqId}, no se pudo aprobar`)
      }
      handleCommentModal({show: false, isLoadingOk: false})
    } 
    
    /**
     * Rechaza Rq por usuario activo
     * @param {integer} data2Modal 
    */
   const rjctAction = async (data2Modal) => {
     handleCommentModal({isLoadingRej: true})
     const response = await rjctRq_service(data2Modal?.rqId, upgraderComments)
     if(response){
       alert(`Requerimiento ${data2Modal?.rqId}, rechazado con éxito`)
     }else{
       alert(`Requerimiento ${data2Modal?.rqId}, no se pudo rechazar`)
     }
     handleCommentModal({show: false, isLoadingRej: false})
    } 
    /**
     * First render
     * projete la ruta cuando no existe rqId
     */
    useEffect(()=>{
        const getRqData = async (rqId_) => {
            const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
            const response = await getRqData_service(rqId_); // trae los datos del rq
            //evalua si usuario tiene rol de aprobador para este RQ
            // let nAprobaciones = response.aprobaciones.map((aprobador) => ({...aprobador, enabled: user === aprobador?.usuario_aprueba, isPending: aprobador?.estado === 'P'})) //!REGRESAR!
            let nAprobaciones = response?.aprobaciones.map((aprobador) => ({...aprobador, enabled: ('EPUGA' === aprobador?.usuario_aprueba && aprobador?.estado === 'P'), isPending: aprobador?.estado === 'P', obs: ''}))
            let nResponse = {...response, ...{aprobaciones: nAprobaciones}}
            setRqReview({...nResponse})
        }
        if (!!rqId && !commetsModal.show){
            getRqData(rqId)
        }else if(!rqId){
            navigate(-1)
        }
    },[commetsModal.show])

  return (
    <>
    {/* Modal para comentario de aprobacion */}
    <VentanaModal show={commetsModal.show} handleClose={()=>handleCommentModal({show:false})} titulo={commetsModal.title} size={commetsModal.size} feet={true} 
      feetTitle={commetsModal.feetTitle} 
      btnFeetVariant={commetsModal.btnFeetVariant}
      buttonAction={doValidation}
      rjctAction={rjctAction}
      data2Modal={commetsModal.data}
      isLoadingOk={commetsModal.isLoadingOk}
      isLoadingRej={commetsModal.isLoadingRej}
    >
      <Form.Group controlId="exampleForm.ControlTextarea1">
        <FloatingLabel controlId="floatingArea" label="Este campo es opcional">
          <Form.Control as="textarea" rows={3} value={upgraderComments} onChange={onTextAreaCommentsChange}/>
        </FloatingLabel>
      </Form.Group>
    </VentanaModal>

    <div className='tw-w-1/2 tw-h-fit tw-mx-auto tw-bg-slate-50 tw-rounded-lg card_shadow'>
        <div className='card tw-w-full'>
                <div className="card-header border-0 rounded-0">
                        <div className="align-items-center gy-3 row rounded-0">
                            <div className="col-sm rounded-0">
                                <h4 className="card-title mb-0 rounded-0">{`Resumen requerimiento`}</h4>
                            </div>
                        </div>
                </div>
        </div>

        <div className='tw-w-full tw-px-4 tw-py-4 tw-min-h-56'>
            <div className='tw-grid tw-grid-cols-2 tw-px-10'>
                <div>
                    <Label_Text title={'Codigo RQ:'} text={rqReview?.codigo_rq}/>
                    <Label_Text title={'Solicitante:'} text={rqReview?.usuario_nombre}/>
                    <Label_Text title={'Empresa:'} text={empresas[rqReview?.cia]}/>
                    <Label_Text title={'Comentarios:'} text={rqReview?.comentario}/>
                </div>
                <div>
                    <Label_Text title={'Estado:'} text={estados[rqReview?.estado]}/>
                    <Label_Text title={'Doc. SAP:'} text={rqReview?.sap_docnum || 'PENDIENTE'}/>
                    <Label_Text title={'F. Pedido:'} text={getCurrentDate_YYYY_MM_DD2DD_MM_YYYY(rqReview?.fecha_pedido)}/>
                    <Label_Text title={'F. Necesaria:'} text={getCurrentDate_YYYY_MM_DD2DD_MM_YYYY(rqReview?.fecha_necesaria)}/>
                </div>
            </div>
            <Divider text={'Datos opcionales'}/>
            <div className='tw-grid tw-grid-cols-2 tw-px-2'>
                <div>
                    <Label_Text title={'Proveedor:'} text={rqReview?.proveedor_nombre || 'No especifica'}/>
                    <Label_Text title={'Tipo de moneda:'} text={rqReview?.moneda || 'Por defecto'}/>
                </div>
                <div>
                    <Label_Text title={'Condición de pago:'} text={rqReview?.forma_pago_descripcion || 'Por defecto'}/>
                </div>
            </div>
        </div>
        <div>
            <CardSection cardTitle={'Contenido'} cardHeight={52}>
                    <Table_RqView data={rqReview?.detalle || []}/>
            </CardSection>

            <CardSection cardTitle={'Historial de aprobación'}>
                    <Table_Aprobador doValidation={openCommentTxtArea} usrMode={usrMode} data={rqReview?.aprobaciones || []}/>
            </CardSection>

            {/* Inicio de boton */}
            <div className='d-grid p-2 tw-mt-4'>
                {/* <Button size='md' variant='success' onClick={()=>navigate('/bandejasolicitudes', {state: {rqId: rqId, usrMode: usrMode, estado: estado_}})}>Retornar</Button> */}
                <Button size='md' variant='success' onClick={()=>navigate(`/bandeja/${estado_text[usrMode]}`, {state: {rqId: rqId, usrMode: usrMode, estado: estado_}})}>Retornar</Button>
            </div>
            {/* Fin de boton */}
        </div>  
    </div>
    </>
  )
}

export { RqView }


function Label_Text({title, text}) {
    return(
        <Form.Group className="mb-2 tw-grid tw-grid-cols-2">
            <div className='tw-flex tw-justify-end'>
                <Form.Label className='mb-1 tw-text-sm tw-font-semibold tw-mr-2'> {title}</Form.Label>
            </div>
            <Form.Label className='mb-1 tw-text-sm tw-break-words'> {text}</Form.Label>
        </Form.Group>
    )
}

function Divider({text}){
    return(
        <div style={{ position: 'relative', textAlign: 'center', margin: '20px 0' }}>
      <hr style={{ border: 'none', borderTop: '2px solid #000', margin: 0 }} />
      <span
        style={{
          position: 'absolute',
          top: '-12px', // Adjust this value to position the text vertically
          left: '50%',
          transform: 'translateX(-50%)',
          backgroundColor: 'white',
          padding: '0 10px',
        }}
      >
        {text}
      </span>
    </div>
    )
}

function Table_RqView ({data}) {
    return (
        <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
      {/* <Table sx={{ minWidth: 650 }} aria-label="a dense table"> */}
        <TableHead>
          <TableRow>
            <TableCell className='!tw-font-semibold' >Código SAP</TableCell>
            <TableCell align="center" className='!tw-font-semibold'>Articulo/Servicio</TableCell>
            <TableCell align="center" className='!tw-font-semibold'>F. Necesaria</TableCell>
            <TableCell align="center" className='!tw-font-semibold'>Cantidad</TableCell>
            <TableCell align="center" className='!tw-font-semibold'>Unidad</TableCell>
            <TableCell align="center" className='!tw-font-semibold'>P.Sugerido</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row) => (
            <TableRow
              key={row.itemcode}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">{row.itemcode}</TableCell>
              <TableCell align="center">{row.itemname}</TableCell>
              <TableCell align="center">{getCurrentDate_YYYY_MM_DD2DD_MM_YYYY(row.fecha_necesaria)}</TableCell>
              <TableCell align="center">{row.cantidad_requerida}</TableCell>
              <TableCell align="center">{row.unidad_medida_id}</TableCell>
              <TableCell align="center">{row.precio_sin_igv}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    )
    }

function Table_Aprobador ({doValidation, usrMode, data}) {
    return (
        <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
      {/* <Table sx={{ minWidth: 650 }} aria-label="a dense table"> */}
        <TableHead>
          <TableRow>
            <TableCell className='!tw-font-semibold' >Usuario</TableCell>
            <TableCell align="center" className='!tw-font-semibold'>F. Actualización</TableCell>
            <TableCell align="center" className='!tw-font-semibold'>Observaciones</TableCell>
            <TableCell align="center" className='!tw-font-semibold'>Estado</TableCell>
            {!!usrMode && (<TableCell align="center" className='!tw-font-semibold !tw-text-xs'>Aprobar/Rechazar</TableCell>)}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row, index) => (
            <TableRow
              key={(index+1).toString()}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">{row?.usuario_aprueba_nombre.toUpperCase()}</TableCell>
              <TableCell align="center">{getCurrentDate_YYYY_MM_DD2DD_MM_YYYY(row.fecha_actualizacion, true)}</TableCell>
              <TableCell align="center">{row?.observaciones || 'No precisa'}</TableCell>
              <TableCell align="center">
                <Badge color={estado_color[row.estado]} variant="dot">
                  {estados[row.estado]}
                </Badge>
              </TableCell>
              {/* {!!usrMode && (<TableCell align="center"><input type="checkbox" name="myCheckbox" checked={!row?.isPending} disabled={!row?.enabled} onChange={()=>doValidation(row?.codigo_rq)}/></TableCell>)} */}
              {/* {!!usrMode && (<TableCell align="center"><input type="checkbox" name="myCheckbox" checked={!row?.isPending} disabled={!!row?.enabled} onChange={()=>doValidation(row?.codigo_rq)}/></TableCell>)} */}
              {!!usrMode && (<TableCell align="center"><input type="checkbox" name="myCheckbox" checked={!row?.isPending} disabled={!row?.isPending} onChange={()=>doValidation(row?.codigo_rq)}/></TableCell>)}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    )
    }

function CardSection({cardTitle, children, cardHeight = 0}){
    return (
            <div className='card tw-w-full'>
                <div className="card-header border-0 rounded-0">
                        <div className="align-items-center gy-3 row rounded-0">
                            <div className="col-sm rounded-0">
                                <h4 className="card-title mb-0 rounded-0">{cardTitle}</h4>
                            </div>
                        </div>
                </div>
                <div className={`p-0 card-body tw-min-h-${cardHeight} tw-bg-gray-100`}>
                {/* <div className={`p-0 card-body tw-min-h-52 tw-bg-gray-100`}> */}
                    {children}
                </div>
            </div>
    )
}
