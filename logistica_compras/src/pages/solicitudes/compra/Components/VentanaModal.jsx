import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import React from 'react'
import Spinner from 'react-bootstrap/Spinner';
import { useNavigate } from 'react-router-dom';

const VentanaModal = ({ children, show, handleClose, titulo, size, feet = false, feetTitle= 'Close', btnFeetVariant, buttonAction, rjctAction = null, data2Modal, isLoadingOk, isLoadingRej}) => {
    return (
        // No importa
        <Modal show={show} onHide={()=>{handleClose({change: false})}} size={size} backdrop="static"> 
            { /*size="sm,lg,xl" */}
            <Modal.Header closeButton>
                <Modal.Title>{titulo}</Modal.Title>
            </Modal.Header>
            <Modal.Body className='!tw-px-2 !tw-pt-0'>
                {children}
            </Modal.Body>
            {
                !!feet && (
                    <Modal.Footer>
                        {/* <div className='tw-w-full tw-flex tw-justify-between'> */}
                        <div className={`tw-w-full tw-flex tw-justify-${!!rjctAction ? 'between' : 'end'}`}>
                            {!!rjctAction && (
                                <Button variant={'danger'} onClick={()=>((!!rjctAction && rjctAction(data2Modal)) || handleClose({change: false}))} disabled={isLoadingRej}>
                                    Rechazar
                                    {isLoadingRej &&
                                    (<div className='tw-inline-block tw-ml-2'><Spinner animation="border" variant="light" size='sm'/></div>)
                                    }
                                </Button>
                            )}
                            <Button variant={btnFeetVariant || 'secondary'} onClick={()=>((!!buttonAction && buttonAction(data2Modal)) || handleClose({change:true}))} disabled={isLoadingOk}>
                                {feetTitle}
                                {isLoadingOk &&
                                (<div className='tw-inline-block tw-ml-2'><Spinner animation="border" variant="light" size='sm'/></div>)
                                }
                            </Button>
                        </div>
                    </Modal.Footer>
                )
            }
        </Modal>
    );
};

const VentanaBloqueo = ({ show, handleClose}) => {
    //let history = useHistory();
    const Navegacion = useNavigate();

    return (
        <Modal show={show} onHide={handleClose} size={'xl'}> { /*backdrop="static" keyboard={false} */}
            { /*size="sm,lg,xl" */}
            <Modal.Header closeButton>
                <Modal.Title>{"Seguridad"}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {                                        
                    <>
                        <p>No tiene permiso, volver a la ventana anterior <button onClick={() => Navegacion(-1)}>Retroceder</button></p>
                    </>
                }                    
            </Modal.Body>
        </Modal>
    );
};

export {VentanaModal, VentanaBloqueo}