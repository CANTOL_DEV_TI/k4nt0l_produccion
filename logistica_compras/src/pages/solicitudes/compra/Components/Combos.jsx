import { Form } from "react-bootstrap";

export const Selector = ({options, curIndex, handleSelectChange, id, dsc}) => {
    return (
        // <></>
    <Form.Select
            aria-label="Select an option"
            value={curIndex || '-1'}
            onChange={handleSelectChange}
            size="sm"
        >
            {
                options.map((idx, index)=>(
                <option key={(index + 1).toString()} value={idx[id || 'centro_costo']}>
                    {idx[dsc || 'cc_desc']}
                </option>
            ))
        }
    </Form.Select>
    )
}