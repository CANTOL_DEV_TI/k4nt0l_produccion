// import { rjctRq_service, upGradeRq_service } from "../services/rqServices";
import { rjctRq_service, upGradeRq_service } from "../services/rqServices";
import { delay } from "./delay";

function deepCopy(arr) {
    return arr.map(item => {
      if (item && typeof item === 'object') {
        if (item instanceof Date) {
          return new Date(item); // Handle Date objects
        }
        return deepCopy(Object.values(item)); // Recursively copy nested objects
      }
      return item; // Primitive value, just return
    });
  }

  /**
 * Adds two numbers.
 * @param {array} myList - list of items.
 * @param {string} target - tarjet.
 * @param {string} key - key name of target.
 * @returns {number} - index.
 */
  function getIndexOfList(myList, target, key){
    return myList.findIndex(item => item[key] === target);
  }

  /**
 * move item to firt place.
 * @param {array} myList - list of items.
 * @param {string} target - tarjet.
 * @param {string} key - key name of target.
 * @returns {array} - index.
 */
  function moveItemFirstPlace(myList, target, key){
    const index = getIndexOfList(myList, target, key);
    if (index > -1) {
      // Remove the item from its current position and add it to the front
      let item = myList.splice(index, 1); // Removes the item
      myList.unshift(item[0]); // Adds the item to the beginning
    }
    return [...myList]
  }

  /**
   * change array to string
   * valores que se tomanran, 'itemcode' 'itemname' 'cantidad_requerida'
   */
  function arrayToString(arr, keys = ['itemcode','itemname','cantidad_requerida']) {
    let str = '';
    arr?.forEach(item => {
      str += `✅Código: ${item[keys[0]]}| Nombre: ${item[keys[1]]}| Cant: ${
        item[keys[2]]
        } \n`;
        });
        return str;   
  }

  /**
   * recorrer un array para aprobar todos los rqs
   */
  function aprobacionMasiva(arr) {
    return new Promise(async (resolve, reject) => {
      try {
        // Usamos un forEach para iterar, pero necesitamos convertirlo en un ciclo asíncrono.
        // La forma correcta es usar un for...of o map + await
        for (let item of arr) {
          // Simulando una tarea asincrónica para cada item (por ejemplo, un retraso)
          await delay(150);  // Espera 150 ms
          let response = await upGradeRq_service(item?.codigo_rq, 'masivo')
          if(!response){throw new Error("Voluntary error");}else{console.log(`Aprobando rq: ${item?.codigo_rq}`);}
        }
        resolve("Todos los rqs fueron aprobados");
      } catch (error) {
        reject("Error durante aprobación");
      }
    });
  }

  /**
   * recorrer un array para aprobar todos los rqs
   */
  function rechazoMasivo(arr) {
    return new Promise(async (resolve, reject) => {
      try {
        // Usamos un forEach para iterar, pero necesitamos convertirlo en un ciclo asíncrono.
        // La forma correcta es usar un for...of o map + await
        for (let item of arr) {
          // Simulando una tarea asincrónica para cada item (por ejemplo, un retraso)
          await delay(150);  // Espera 150 ms
          let response = await rjctRq_service(item?.codigo_rq, 'masivo')
          if(!response){throw new Error("Voluntary error");}else{console.log(`Rechazando rq: ${item?.codigo_rq}`);}
        }
        resolve("Todos los rqs fueron rechazados");
      } catch (error) {
        reject("Error durante rechazo masivo");
      }
    });
  }

  /**
   * ordena alfabeticamente lista de objetos
   * @param {String} key - llave de objecto para ordenar
   * @returns {array} - lita ordenada.
   */
  function ordenarListaAlfabeticamente(lista, key) {
    return lista.sort((a, b) => a[key].localeCompare(b[key]));
  }



  export { deepCopy, getIndexOfList, moveItemFirstPlace, arrayToString, aprobacionMasiva, ordenarListaAlfabeticamente, rechazoMasivo}