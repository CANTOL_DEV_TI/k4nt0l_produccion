export const getCurrentMonthYear = () => {
    const today = new Date();
    
    const year = today.getFullYear(); 
    const month = today.getMonth() + 1; 
    return {month: month, year: year}
}

/**
 * Si no envia date_param, selecciona el presente dia
 * Adds two params.
 * @param {Date} date - input date if exits.
 * @param {string} separator - date separator.
 * @returns {string} - formatted date string.
 */
export const getCurrentDate = (date_param, separator_param) => {
    const date = !!date_param ? date_param : new Date();
    const year = date?.getFullYear()?.toString();
    const month = String(date?.getMonth() + 1).padStart(2, '0'); // Get month and pad with leading zero if needed
    const day = String(date?.getDate()).padStart(2, '0'); // Get day and pad with leading zero if needed
    return !!separator_param ? `${day + separator_param + month + separator_param + year}` : `${year}-${month}-${day}`;
}

/**
 * Si no envia date_param, selecciona el presente dia
 * Adds two params.
 * @param {Date} monthsAgo - Monts ago.
 * @param {string} separator - date separator.
 * @returns {string} - formatted date string.
 */
export const getCurrentDate_object_format = (monthsAgo=0) => {
    let curDate = new Date()
    !!monthsAgo ?  curDate.setMonth(curDate.getMonth() - monthsAgo) : curDate;
    return curDate
}

/**
 * Si no envia date_param, selecciona el presente dia
 * Adds two params.
 * @param {Date} date_param - input date if exits.
 * @param {string} separator_param - date separator.
 * @returns {string} - formatted date string.
 */
export const getCurrentDate_YY_MM_DD = (date_param, separator_param) => {
    const date = !!date_param ? date_param : new Date();
    const year = date.getFullYear().toString();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Get month and pad with leading zero if needed
    const day = String(date.getDate()).padStart(2, '0'); // Get day and pad with leading zero if needed
    return `${year + separator_param +  month + separator_param + day}`;
}

/**
 * Transforma string YYYYMMDD a DD/MM/YYYY
 * Adds two params.
 * @param {string} fechaString - formato YYYYMMDD.
 * @returns {string} - formatted date string DD/MM/YYYY.
 */
export const getCurrentDate_YYYYMMDD2DD_MM_YYYY = (fechaString) => {
    const anio = fechaString?.substring(0, 4);  // Los primeros 4 caracteres son el año
    const mes = fechaString?.substring(4, 6);   // Los siguientes 2 caracteres son el mes
    const dia = fechaString?.substring(6, 8);   // Los últimos 2 caracteres son el día
    return `${dia}/${mes}/${anio}`;
}

/**
 * Transforma string YYYYMMDD a DD/MM/YYYY
 * Adds two params.
 * @param {string} fechaString - formato 2024-12-10T00:00:00
 * @returns {string} - formatted date string DD/MM/YYYY.
 */
export const getCurrentDate_YYYY_MM_DD2DD_MM_YYYY = (fechaString, special_format = false) => {
    let date_ = new Date(fechaString)
    if (!special_format) {
        date_.setDate(date_.getDate() + 1); // esto es una correcion, esta fallando el constructor new Date
    }
    const year = date_.getFullYear().toString();
    const month = String(date_.getMonth() + 1).padStart(2, '0'); // Get month and pad with leading zero if needed
    const day = String(date_.getDate()).padStart(2, '0'); // Get day and pad with leading zero if needed

    return `${day}/${month}/${year}`;
}