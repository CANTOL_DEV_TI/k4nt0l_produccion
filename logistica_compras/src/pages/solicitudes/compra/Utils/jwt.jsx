import { jwtDecode } from "jwt-decode";

export function getUserJWT(token){
    const decoded = jwtDecode(token);
    return decoded.sub.replace(sessionStorage.getItem('Empresa'),"")
}


export function get_User_Company(token){
    const { sub, exp } = jwtDecode(token);
    return {user: sub.substring(3) , company: sub.substring(0,3)}
}