import React, { useEffect, useState } from 'react'
import { Tabla_Solicitudes } from './Components/Tabla'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import { getCurrentDate_object_format } from './Utils/calendar';
import { deleteRequirement, getDataCard, getPendingRq, getWholeRequirements } from './services/rqServices'
import { es } from 'date-fns/locale';
import DatePicker from 'react-datepicker'
import { HomeCards } from './Components/HomeCards'
import { Button, ButtonGroup, CircularProgress, Pagination } from '@mui/material'
import SendIcon from '@mui/icons-material/Send';
import { aprobacionMasiva, rechazoMasivo } from './Utils/array'
import { MyBackDrop } from './Components/BackDrop';
import { Form } from 'react-bootstrap'
import './style/home.css'
import "react-datepicker/dist/react-datepicker.css";
import { VentanaBloqueo } from './Components/VentanaModal';

const modoUsuario = {
    solicitar: 0,
    aprobar: 1
}

function Home() {
    const { tipo } = useParams();
    const {state} = useLocation();
    const navigate = useNavigate();
    const [filter, setFilter] = useState({startDate: getCurrentDate_object_format(1), endDate: getCurrentDate_object_format(0), estado: state?.estado || 'P', nroSol: ''}) //Filtro, F. Registro endDate: hoy startDate: mes pasado
    const [rqList, setRqList] = useState([])

    /**
     * estados para modos de trabajo
     */
    // const [usrMode, setUsrMode] = useState(state?.usrMode || 0) //0: solicitante, 1: aprobador
    // const [massiveMode, setMassiveMode] = useState(state?.usrMode || 0) //0: no massivo, 1: si massivo
    const [usrMode, setUsrMode] = useState(modoUsuario[tipo] || 0) //0: solicitante, 1: aprobador
    const [massiveMode, setMassiveMode] = useState(modoUsuario[tipo] || 0) //0: no massivo, 1: si massivo

    const [dataCard, setDataCard] = useState(null)
    const [refreshTable, setRefreshTable] = useState(false)
    const [isValidating, setIsValidating] = useState(false)
    const [isValidating_rjct, setIsValidating_rjct] = useState(false)

    // const [txtNumber, setTxtNumber] = useState(0)

    const handlerFilter = (obj) => setFilter({...filter, ...obj})
    const handlerRqList = (lx) =>  setRqList(lx)
    // ****
    const handlerUserMode = (event, newValue) =>{setUsrMode(newValue);handlerFilter({estado: 'P'})}  //0: solicitante, 1: aprobador
    const handlerUserMode_ = (newValue) =>{setUsrMode(newValue);handlerFilter({estado: 'P'})}  //0: solicitante, 1: aprobador
    // ****
    const [page, setPage] = useState(1); // Página inicial
    const rqsLimitOnPage = 15;


    /**
     * Ventana de seguridad
     */

    // ventana seguridad
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);
    const Seguridad = async () => {
    //   let meToken = sessionStorage.getItem("CDTToken");
    //   if (!!meToken){
    //     const ValSeguridad = await Validar("PROSIM");
    //     const nmd = await validaToken(meToken);
    //     const ValRol = await ValidaRol_by_UserId(nmd.sub.substring(3),4);
    //     setVentanaSeguridad(!ValRol || ValSeguridad.Validar);
    //   }else{
    //     setVentanaSeguridad(true);
    //   }
  }
  //

    /**
     * Activa cuando actualiza el param tipo del URL: aprobar, solicitar
     * @param {string} tipo - P: Pendiente, A:Aprobado, R: Rechazado
     */
    useEffect(()=>{
        const actualizaTipoParamURL = async () => {
            handlerUserMode_(modoUsuario[tipo])
        }
        !state?.estado && actualizaTipoParamURL() //verifica que no existe estado (P, R, A, M) en state para que filtre por tipo url
    },[tipo])

    /**
     * Obtiene los requerimientos desde service
     * @param {string} tipo - P: Pendiente, A:Aprobado, R: Rechazado
     */
    const obtenerRequerimientos = async () => {
        const response = await getWholeRequirements(filter)
        const dataCardResponse = await getDataCard(filter, 'S')
        setMassiveMode(false)
        setDataCard({...dataCardResponse})
        handlerRqList([...response])
    }

    /**
     * Split data list requests according page of pagination
    */
   const dataPage = (rqList_) => {
    const limit = rqsLimitOnPage // cantidad de items por página
    const offset = (page - 1) * limit // posición inicial de la página
    const data = rqList_.slice(offset, offset + limit)
    return data
    }

    /**
     * Valida todos los Rqs de la lista pendientes
     */
    const validarTodosRqs = async () => {
        //Debe cambiar de estado a validacion automatica
        //-Al inicio debe comenzar con desactivado
        //-bloquea cambio de tarjetas ->no es necesario por que se reinicia
        //-bloquea botones de operacion de rq [ya esta]
        //-bloquea transicion entre solicitudes y aprobaciones
        //-bloquea cambio de fechas[ya esta]

        let rqsForvalidation = rqList.filter(rq => rq.isChecked)
        // let rqsForvalidation = [1,2,3]

        if(confirm(`${rqsForvalidation.length} rq's sera${rqsForvalidation.length>1 ? 'n': ''} validado${rqsForvalidation.length>1 ? 's': ''}, \n¿Esta conforme?`)){
            setIsValidating(true)
            let response = await aprobacionMasiva(rqsForvalidation)
            alert(response)
            setIsValidating(false)
            setRefreshTable(!refreshTable)
        }else{
            console.log('Rechaza aprobación masiva')
        }
    }

    /**
     * Rechaza todos los Rqs de la lista pendientes
     */
    const rechazarTodosRqs = async () => {
        //Debe cambiar de estado a validacion automatica
        //-Al inicio debe comenzar con desactivado
        //-bloquea cambio de tarjetas ->no es necesario por que se reinicia
        //-bloquea botones de operacion de rq [ya esta]
        //-bloquea transicion entre solicitudes y aprobaciones
        //-bloquea cambio de fechas[ya esta]

        let rqsForvalidation = rqList.filter(rq => rq.isChecked)
        // let rqsForvalidation = [1,2,3]

        if(confirm(`${rqsForvalidation.length} rq's sera${rqsForvalidation.length>1 ? 'n': ''} rechazado${rqsForvalidation.length>1 ? 's': ''}, \n¿Esta conforme?`)){
            setIsValidating_rjct(true)
            let response = await rechazoMasivo(rqsForvalidation)
            alert(response)
            setIsValidating_rjct(false)
            setRefreshTable(!refreshTable)
        }else{
            console.log('Rechazo masivo abortado')
        }
    }

    /**
     * Obtiene los requerimientos desde service
     * @param {string} tipo - P: Pendiente, A:Aprobado, R: Rechazado
     */
    const obtenerRqXAprobar = async () => {
        const response = await getPendingRq(filter)
        let new_response = response?.map(item=>({...item, isChecked: false})) // aqui se agrega el campo de checked
        const dataCardResponse = await getDataCard(filter, 'A')
        setMassiveMode(false)
        setDataCard({...dataCardResponse})
        handlerRqList([...new_response])

    }

    /**
     * Reiniciar a check false toda la lista, para aprobacion
     */
    const reiniciarCheck = () => {
        handlerRqList(rqList.map(item => ({...item, isChecked: false})))
    }

    /**
     * Check rq
     */
    const checkRq = (rq) => {
        handlerRqList(rqList.map(item => item.codigo_rq === rq ? {...item,
            isChecked: !item.isChecked
            } : item))
    }

    /**
     * Ingresa el modo ver RQ
     * @param {string} rqId - Id del requerimiento
     */
    const openRqView = (rqId) => {navigate('/requerimiento', {state: {rqId: rqId, usrMode: usrMode, estado_: filter?.estado || 'P'}})} //Redirige a la vista del requerimiento
    
    /**
     * Ingresa el modo editar RQ
     * @param {string} rqId - Id del requerimiento
     */
    const openRqEdit = (rqId) => {navigate('/editarrequerimiento', {state: {rqId: rqId, usrMode: usrMode, editMode: true, estado_: filter?.estado || 'P'}})} //Redirige a la vista editar requerimiento
    
    /**
     * Abre el modal de confirmacion para eliminar Rq
     * @param {string} rqId - Id del requerimiento
     */
    const deleteRq = async (rqId_, centro_costo_id) => {
        if(confirm(`Esta seguro de eliminar RQ ${rqId_.toString()}`)){
            const response = await deleteRequirement(rqId_, centro_costo_id)
            const {data, status} = response || {data: undefined, status: undefined}
            if(status === 200){
                alert(`Rq eliminado correctamente \nMensaje servidor: ${JSON.stringify(data)}`)
                setRefreshTable(!refreshTable)
            }else{
                alert('Rq no eliminado')
            }
        }
    }
    
    /**
     * First render y cuando filtro fecha cambia
     */
    useEffect(()=>{
        Seguridad()
        if(!usrMode){
            obtenerRequerimientos()
            setPage(1) // Inicializa la página cuando actualiza el filtro
        }else{
            obtenerRqXAprobar()
            setPage(1) // Inicializa la página cuando actualiza el filtro
        }

    },[filter.startDate, filter.endDate, filter.estado, filter.nroSol , usrMode, refreshTable])

    /**
     * Desactivar botones eliminar y editar
     */
    const activarBotones = () =>{
        return {editBtn: !!(filter?.estado === 'P'), deleteBtn: !!(filter?.estado === 'P')}
    }

    const enButtons = activarBotones()

    return (
    <>
    {/* <MyBackDrop show={isValidating} msg={`Aprobando...(${txtNumber.toString()})`}/> */}
    <MyBackDrop show={isValidating} msg={`Aprobando...`}/>
    <div className='tw-w-full tw-h-screen tw-px-3 tw-flex tw-flex-col tw-gap-3 tw-relative'>
    <HomeCards handlerFilter={handlerFilter} usrMode={usrMode} handlerUserMode={handlerUserMode} filter={filter} dataCard={dataCard}/>
        <div className="card tw-w-full">
        {/* INICIO BARRA DE NAVEGACION */}
            <div className="card-header border-0 card-header">
                <div className="align-items-center gy-3 row">
                    <div className="col-sm">
                        <h4 className="card-title mb-0">{`Mis requerimientos`}</h4>
                    </div>
                </div>
            </div>

            <div className="p-0 card-body">
                    <div className='tw-px-3 tw-py-1'>
                        <div className='tw-flex tw-items-center tw-mr-2'><h6 className='tw-my-0'>Filtrar por F. Registro:</h6></div>
                    </div>
                    <div className='d-flex flex-wrap tw-w-full'>
                        <div className='px-3 py-1 tw-mb-2 tw-flex tw-items-center tw-w-full tw-justify-between'>
                                <div className='tw-flex tw-gap-3 tw-items-center tw-flex-wrap'>
                                    <div className='tw-mr-2'>
                                        <span className='tw-text-sm'>Desde:</span>
                                        <DatePicker 
                                            className='tw-border tw-border-2 tw-rounded-lg' 
                                            selected={filter.startDate} 
                                            onChange={(date) => handlerFilter({startDate: date})}
                                            dateFormat="dd/MM/yyyy" // Define el formato de la fecha
                                            placeholderText="Seleccionar fecha"
                                            locale={es}
                                            disabled={!!massiveMode}
                                        />
                                    </div>
                                    <div>
                                        <span className='tw-text-sm'>hasta:</span>
                                        <DatePicker 
                                            className='tw-border tw-border-2 tw-rounded-lg' 
                                            selected={filter.endDate} 
                                            onChange={(date) => handlerFilter({endDate: date})}
                                            dateFormat="dd/MM/yyyy" // Define el formato de la fecha
                                            placeholderText="Seleccionar fecha"
                                            locale={es}
                                            disabled={!!massiveMode}
                                        />
                                    </div>
                                    <div className='tw-flex tw-items-center tw-flex-wrap'>
                                        <span className='tw-text-sm tw-mr-1'>ID:</span>
                                        <Form.Control type="number" size='xs' className='tw-h-7 !tw-min-w-3 !tw-w-fit' onChange={(x)=>handlerFilter({nroSol: x.target.value})}/>
                                    </div>
                                </div>
                                <div>
                                    {
                                        !!usrMode ? (
                                                (filter.estado === 'P') && (
                                                    (!!massiveMode) ? (
                                                        // <ButtonGroup variant="contained" color='success' aria-label="Basic button group" diksabled={true}>
                                                        <ButtonGroup variant="contained" color='success' aria-label="Basic button group" disabled={isValidating}>
                                                            <Button onClick={rechazarTodosRqs}>
                                                                Rechazar
                                                                {
                                                                    isValidating_rjct && (
                                                                        <CircularProgress color="inherit" size={18} className='tw-ml-2'/>
                                                                    )
                                                                }
                                                            </Button>
                                                            <Button onClick={()=>{reiniciarCheck();setMassiveMode(!massiveMode)}} variant='lime'>Regresar</Button>
                                                            <Button onClick={validarTodosRqs}>
                                                                Aprobar
                                                                {
                                                                    isValidating && (
                                                                        <CircularProgress color="inherit" size={18} className='tw-ml-2'/>
                                                                    )
                                                                }
                                                            
                                                            </Button>
                                                        </ButtonGroup>
                                                    ):
                                                    (
                                                        <Button className='!tw-mr-2' variant="contained" color='success' size='xs' onClick={()=>{setMassiveMode(!massiveMode)}} disabled={isValidating}>
                                                                Aprobación / Rechazo masivo
                                                        </Button>
                                                    )
                                                )
                                            )
                                            :
                                            (
                                                <Button variant="contained" endIcon={<SendIcon />} onClick={()=>{navigate('/nuevasolicitud')}}>
                                                    Crear RQ
                                                </Button>
                                            )
                                        }
                                </div>
                        </div>

                        <div className='w-100 p-0 position-relative overflow-hidden' style={{height:'fit-content'}}>
                                <Tabla_Solicitudes 
                                    usrMode={usrMode} 
                                    datos_fila={dataPage(rqList)} 
                                    openRqView={openRqView} 
                                    openRqEdit={openRqEdit} 
                                    openRqDelete={deleteRq} 
                                    enButtons={enButtons}
                                    massiveMode={massiveMode}
                                    checkRq={checkRq}
                                    filter={filter} //se pasa para mostrar el docNum de SAP
                                />
                                <div className='tw-flex tw-justify-end tw-pb-5 tw-pr-4'>
                                    <Pagination count={Math.ceil(rqList.length / rqsLimitOnPage)} shape="rounded" variant="outlined"  page={page} onChange={(event, value)=>setPage(value)}/>
                                </div>
                        </div>
                    </div>
            </div>
        </div>
        </div>
            <VentanaBloqueo show={VentanaSeguridad}/>
        </>
  )
}

export default Home