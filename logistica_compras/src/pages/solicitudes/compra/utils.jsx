import { getCurrentDate_YY_MM_DD } from "./Utils/calendar";
import { get_User_Company } from "./Utils/jwt";


export const makeNewRequestBody = (rqId, mainRequest) => {

    //buscar la mayor fecha necesaria
    let index = mainRequest?.items.reduce((maxIndex, rqLine, currentIndex, array) => {
        return rqLine.rqDate > array[maxIndex].rqDate ? currentIndex : maxIndex;
      }, 0);

    //obtiene datos de usuario y empresa
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    return({
        cia: company,
        codigo_rq: Number(rqId) || 0,
        centro_costo_id: mainRequest.cc[0].centro_costo,
        fecha_pedido: getCurrentDate_YY_MM_DD(undefined ,'') || '',
        fecha_necesaria: getCurrentDate_YY_MM_DD(mainRequest.items[index]?.rqDate ,'') || '',
        fecha_vencimiento: getCurrentDate_YY_MM_DD(mainRequest.items[index]?.rqDate ,'') || '',
        usuario: user,
        empleado_sap: mainRequest.usuario_empleadosap,
        estado: "P", //P: Pendiente
        proveedor_id: mainRequest.provider.cardCode,
        proveedor_nombre: mainRequest.provider.cardName,
        forma_pago_id: !!mainRequest.provider.cardCode ? Number(mainRequest.provider.condPagoId) === -2 ? 0 : Number(mainRequest.provider.condPagoId) : 0,
        forma_pago_descripcion: !!mainRequest.provider.cardCode ? mainRequest.provider.condPagoDesc : '',
        moneda: mainRequest?.provider?.moneda !== '-1' ? (mainRequest?.provider?.moneda || 'S/') : '',
        comentario: mainRequest.comments,
        lineas: mainRequest.items.map((item, index) =>(
                {
                 cia: company,
                 codigo_rq: Number(rqId) || 0,
                 linea: index,
                 itemcode: item?.itemCode,
                 itemname: item?.itemName,
                 fecha_necesaria: getCurrentDate_YY_MM_DD(item?.rqDate ,''),
                 cantidad_requerida: item?.qty,
                 precio_sin_igv: item?.precio_sin_igv || 0,
                 unidad_medida_id: item?.UOM,
                 comentario_linea: "",
                 cuenta_linea: item?.ctacontable?.selected !== '-1' ? item?.ctacontable?.selected : '',
                 proyecto: item?.project?.selected !== '-1' ? item?.project?.selected : '',
                 catpre_codigo: item?.ctgyBudget?.selected !== '-1' ? item?.ctgyBudget?.selected : '',
                 catpre_nombre: item?.ctgyBudget?.selected !== '-1' ? 
                                    item?.ctgyBudget?.options?.filter((item_)=>(item_?.Cate_Codigo === item?.ctgyBudget?.selected))[0]['Cate_Desc'] :
                                     '',
                    }
                )
        )
    })
}