import axios from "axios";
import { Url } from "../../../../constants/global"
import { get_User_Company } from "../Utils/jwt";
import { getCurrentDate_YY_MM_DD } from "../Utils/calendar";



async function getItemSap(filtro) {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    let params = {
        pCia: company,
        pNombre: filtro
    }

    try{
        const response = await axios.get(`${Url}/requerimientos/itemsap/${params.pCia}/${params.pNombre}`);
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getItemSap) _ ${error}`);
        return error?.response?.status
    }
}

async function getCostoCenters() {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    try{
        // const response = await axios.get(`${Url}/requerimientos/centrocostoxusuario/${company}/${'RAGUIRRE'); DESARROLLO
        const response = await axios.get(`${Url}/requerimientos/centrocostoxusuario/${company}/${user}`); //PRODUCCION
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getCostoCenters) _ ${error}`);
        return error?.response?.status
    }
}

async function postNewRequirement(body) {
    try{
        const response = await axios.post(`${Url}/requerimientos/nuevo/`, body);
        if (!!response.data && response.status === 201){
            return {data: response.data, status: response.status};
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (postNewRequirement) _ ${error}`);
        return error?.response?.status
    }
}

async function updateRequirement(body){
    try{
        const response = await axios.put(`${Url}/requerimientos/actualizar/`, body);
        if (!!response.data && response.status === 202){
            return {data: response.data, status: response.status};
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (updateRequirement) _ ${error}`);
        return error?.response?.status
        }
}


async function deleteRequirement(rq_, centro_costo_id){
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    let body = {
        "cia": company,
        "codigo_rq": Number(rq_),
        "centro_costo_id": centro_costo_id ? centro_costo_id.toString() : null,
        "usuario": user,
        "lineas": []
    }
    try{
        const response = await axios.delete(`${Url}/requerimientos/eliminar/`, {data: body});
        if (!!response.data && response.status === 200){
            return {data: response.data, status: response.status};
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (deleteRequirement) _ ${error}`);
        return error?.response?.status
    }
}

async function getUserData() {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    try{
        const response = await axios.get(`${Url}/requerimientos/email/${user}/`);
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getUserData) _ ${error}`);
        return error?.response?.status
    }
}

async function getPymtCond() {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    try{
        const response = await axios.get(`${Url}/requerimientos/proveedorcondpago/${company}/`);
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getPymtCond) _ ${error}`);
        return error?.response?.status
    }
}

async function getProviderSap(filtro) {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    try{
        const response = await axios.get(`${Url}/requerimientos/proveedoressap/${company}/${filtro}`); //PRODUCCION
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getProviderSap) _ ${error}`);
        return error?.response?.status
    }
}

async function getDataCard(filtro, tipo = 'S') { //S: Solicitante, A: Aprobador
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    let body = {
        cia: company,
        desde: getCurrentDate_YY_MM_DD(filtro.startDate, ''),
        hasta: getCurrentDate_YY_MM_DD(filtro.endDate, ''),
        //usuario: 'EPUGA', //!REGRESAR! //DESARROLLO
        usuario: user, //!REGRESAR! //PRODUCCION
    }
    try{
        if(tipo === 'S'){
            const response = await axios.post(`${Url}/requerimientos/totales/`, body); //SOLICITANTE
            if (!!response.data && response.status === 200){
                return response.data;
            }else
            {
                return [];
            }
        }else if(tipo === 'A'){
            const response = await axios.post(`${Url}/requerimientos/totalesaprobadores/`, body); //APROBADOR
            if (!!response.data && response.status === 200){
                return response.data;
            }else
            {
                return [];
            }
        }
    }catch(error){
        console.log(`An Error ocurred: (getWholeRequirements) _ ${error}`);
        return error?.response?.status
    }
}

async function getWholeRequirements(filtro) {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    let body = {
        cia: company,
        desde: getCurrentDate_YY_MM_DD(filtro.startDate, ''),
        hasta: getCurrentDate_YY_MM_DD(filtro.endDate, ''),
        estado: filtro?.estado,
        usuario: user,
        nro_sol: filtro?.nroSol,
    }
    try{
        const response = await axios.post(`${Url}/requerimientos/listado/`, body); //PRUEBA
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getWholeRequirements) _ ${error}`);
        return error?.response?.status
    }
}

async function getPendingRq(filtro) {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    let body = {
        cia: company,
        desde: getCurrentDate_YY_MM_DD(filtro.startDate, ''),
        hasta: getCurrentDate_YY_MM_DD(filtro.endDate, ''),
        estado: filtro.estado,
        // usuario: 'RAGUIRRE' //DESARROLLO
        usuario: user, //PRODUCCION
        nro_sol: filtro?.nroSol,
    }
    try{
        const response = await axios.post(`${Url}/requerimientos/bandejaaprobacion/`, body);
        if (!!response.data && response.status === 202){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getPendingRq) _ ${error}`);
        return error?.response?.status
    }
}

async function getRqData_service(rqId) {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    try{
        const response = await axios.get(`${Url}/requerimientos/${company}/${rqId}`); 
        if (!!response.data && response.status === 202){
            return response.data;
        }else
        {
            return [];
        }
    }catch(error){
        console.log(`An Error ocurred: (getRqData_service) _ ${error}`);
        return error?.response?.status
    }
}

async function upGradeRq_service(codigo_rq, upgraderComments) {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    let body = {
        cia: company,
        codigo_rq: codigo_rq.toString(),
        usuario_aprueba: user, //PRODUCCION
        // usuario_aprueba: 'RAGUIRRE', //DESARROLLO
        observaciones: upgraderComments
    }

    try{
        const response = await axios.post(`${Url}/requerimientos/aprobar/`, body);
        if (!!response.data && response.status === 202){
            return true;
        }else
        {
            return false;
        }
    }catch(error){
        console.log(`An Error ocurred: (upGradeRq_service) _ ${error}`);
        return false
    }
    return false
}

async function rjctRq_service(codigo_rq, upgraderComments) {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    let body = {
        cia: company,
        codigo_rq: codigo_rq.toString(),
        usuario_aprueba: user, //PRODUCCION
        // usuario_aprueba: 'EPUGA', //DESARROLLO
        observaciones: upgraderComments
    }
    try{
        const response = await axios.delete(`${Url}/requerimientos/rechazar/`, {data: body});
        if (!!response.data && response.status === 202){
            return true;
        }else
        {
            return false;
        }
    }catch(error){
        console.log(`An Error ocurred: (rjctRq_service) _ ${error}`);
        return false
    }
    return false
}

async function getCuentaItem(itemCode) {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    try{
        const response = await axios.get(`${Url}/requerimientos/listacuentasxitems/${company}/${itemCode}`);
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return null;
        }
    }catch(error){
        console.log(`An Error ocurred: (getCuentaItem) _ ${error}`);
        return null
    }
}

async function getCtgyxCuenta(ctaCode) {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    try{
        const response = await axios.get(`${Url}/requerimientos/listacatexcuenta/${company}/${ctaCode}`);
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return null;
        }
    }catch(error){
        console.log(`An Error ocurred: (getCtgyxCuenta) _ ${error}`);
        return null
    }
}

async function getProjects() {
    const {user, company} = get_User_Company(sessionStorage.getItem('CDTToken'))
    try{
        const response = await axios.get(`${Url}/requerimientos/listaproyectos/${company}/%20`);
        if (!!response.data && response.status === 200){
            return response.data;
        }else
        {
            return null;
        }
    }catch(error){
        console.log(`An Error ocurred: (getProjects) _ ${error}`);
        return null
    }
}

export {getItemSap, 
        getCostoCenters, 
        getProviderSap, 
        getUserData, 
        postNewRequirement, 
        getWholeRequirements, 
        getRqData_service, 
        getPendingRq, 
        upGradeRq_service, 
        getPymtCond, 
        rjctRq_service, 
        updateRequirement,
        getDataCard,
        deleteRequirement,
        getCuentaItem,
        getCtgyxCuenta,
        getProjects
    }