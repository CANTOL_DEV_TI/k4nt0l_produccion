import React from 'react'
import { Alert, Button, Form, InputGroup, Spinner, Tab, Tabs } from 'react-bootstrap'
// import { Dropdown } from 'react-bootstrap'
import { Tabla_Contenido } from '../Components/Tabla';
import {VentanaModal} from '../Components/VentanaModal';
import NuevoItem from '../Components/NuevoItem';
import { Selector } from '../Components/Combos';
import { BsArrowDown, BsArrowLeft, BsSearch } from 'react-icons/bs';
import { BarraBuscadora_proveedores } from '../Components/BarraBuscadora';
import { useMediaQuery } from 'react-responsive';
// import 
import '../style/home.css'


function NuevoRequerimiento_UI({
    handleNewItemModal,
    handleNewItem,
    handleEditItem,
    handleMainRequest,
    deleteItem,
    handleSelectorChange,
    saveRequirement,
    handlePaymentCondChange,
    handleCurrencyCondChange,
    onProviderSelected,
    setBuscadoraText,
    handleBBuscadorAtributos,
    handleBackWindow,
    mainRequest,
    newItemModal,
    itemSelected,
    paymentsCond,
    currencyCond,
    editMode,
    isLoading,
    buscadoraText,
    bBuscadoraAtributos,
    currencyRequired
}) {
    const isReprocesoMobil = useMediaQuery({ query: '(max-width: 570px)'})

    const handleSearchAgain = () => {
        handleBBuscadorAtributos({inputTextDisabled: false});
        handleMainRequest({provider: {
            cardCode: '',
            ruc: '',
            cardName: '',
            condPagoId: '-2',
            condPagoDesc: '',
        }})
        setBuscadoraText('');
    }

  return (
    <div className={`${isReprocesoMobil ? '' : 'tw-w-1/2'} tw-h-fit tw-mx-auto tw-bg-slate-50 tw-rounded-lg card_shadow`}>
        <div className='card tw-w-full'>
                <div className="card-header border-0 rounded-0">
                        <div className="align-items-center gy-3 row rounded-0">
                            <div className="col-sm rounded-0">
                                <h4 className="card-title mb-0 rounded-0">{!editMode ? `Nueva RQ` : 'Editar RQ'}</h4>
                            </div>
                        </div>
                </div>
        </div>
        <Tabs
            defaultActiveKey="mandatory"
            id="uncontrolled-tab-example"
            className="mb-3"
            size='sm'
        >
            <Tab eventKey="mandatory" title="Obligatorio" size='sm'>
                <div className='tw-w-full tw-px-4 tw-pb-4 tw-h-80'>
                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Nombre de usuario:</Form.Label>
                        <Form.Control placeholder=" input" disabled value={mainRequest.userName} size='sm'/>
                    </Form.Group>
                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Correo electrónico:</Form.Label>
                        <Form.Control disabled placeholder="@.."  size='sm' value={mainRequest.userEmail}/>
                    </Form.Group>
                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Centro de costo:</Form.Label>
                        {/* aqui ataco */}
                        <Selector options={mainRequest.cc} curIndex={mainRequest.cc[0]?.centro_costo} handleSelectChange={(e)=>handleSelectorChange(e.target.value)}/>
                    </Form.Group>
                    <Form.Group className="mb-2">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Comentario:</Form.Label>
                        <Form.Control
                        size='sm'
                        as="textarea"
                        style={{ height: '70px' }}
                        value={mainRequest.comments}
                        onChange={(e)=>handleMainRequest({comments: e.target.value})}
                        />
                    </Form.Group>
                </div>
            </Tab>
            <Tab eventKey="optional" title="Opcional">
                <div className='tw-w-full tw-flex tw-flex-col tw-gap-3 tw-px-4 tw-pb-4 tw-h-80'>
                    <Form.Group className="tw-flex tw-flex-col" controlId="formDate">
                        <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Proveedor recomendado:</Form.Label>
                        <InputGroup className="mb-1">
                            <BarraBuscadora_proveedores 
                                handleBBuscadorAtributos={handleBBuscadorAtributos} 
                                bBuscadoraAtributos={bBuscadoraAtributos}
                                buscadoraText={buscadoraText}
                                setBuscadoraText={setBuscadoraText}
                                onProviderSelected={onProviderSelected}
                            />
                            <Button variant="outline-secondary" id="button-addon2" className='tw-h-8 tw-py-0' onClick={handleSearchAgain}><BsSearch className='!tw-m-0 !tw-p-0'/></Button>
                        </InputGroup>
                    </Form.Group>
                    <div>
                        <Form.Group className="mb-1 tw-flex tw-flex-col tw-w-3/4">
                            <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Condición de pago:</Form.Label>
                            <Selector options={paymentsCond.options} curIndex={paymentsCond?.selected} handleSelectChange={(e)=>{handlePaymentCondChange(e)}} id={'cp_codigo'} dsc={'cp_nombre'}/>
                        </Form.Group>
                    </div>
                    
                    <div>
                        <Form.Group className="mb-1 tw-flex tw-flex-col tw-w-3/4">
                            <Form.Label className='mb-1 tw-text-sm tw-font-semibold'> Tipo de Moneda:</Form.Label>
                            {/* <Alert show={true} variant={'warning'} size='sm' className='alert-sm tw-mt-2'> */}
                            <Alert show={currencyRequired} variant={'danger'} size='sm' className='tw-px-2 tw-py-1 tw-mt-1 tw-mb-2 tw-text-sm'>
                                {'Proveedor con más de una moneda asociada. Debe seleccionar una opción. ***¡OBLIGATORIO!***'}
                            </Alert>    
                            <Selector options={currencyCond.options} curIndex={currencyCond?.selected} handleSelectChange={(e)=>{handleCurrencyCondChange(e)}} id={'cy_codigo'} dsc={'cy_nombre'}/>
                        </Form.Group>

                    </div>
                </div>
            </Tab>
        </Tabs>

        <div>
            {/* Inicio tabla de items */}
            <div className='card tw-w-full'>
                <div className="card-header border-0 rounded-0">
                        <div className="align-items-center gy-3 row rounded-0">
                            <div className="col-sm rounded-0">
                                <h4 className="card-title mb-0 rounded-0">{`Contenido`}</h4>
                            </div>
                        </div>
                </div>
                <div className="p-0 card-body">
                        <div className='d-flex flex-wrap'>
                            <div className='w-100 p-0 position-relative overflow-hidden' style={{height:'fit-content'}}>
                                    <Tabla_Contenido 
                                        datos_fila={mainRequest.items}
                                        deleteItem={deleteItem}
                                        objSubArea={undefined}
                                        handleNewItem={()=>{
                                            handleNewItem({rqDate: new Date()})
                                            handleNewItemModal({show: {action: true, change: false}, isEdit: false, title: 'Agregar item', feetTitle: 'Agregar'});
                                        }}
                                        handleEditItem={handleEditItem}
                                    />
                            </div>
                        </div>
                </div>
            </div>
            {/* Fin tabla de items */}

            {/* Inicio de boton */}
            <div className='d-grid p-2'>
                <div className='tw-bg-red-50 tw-grid tw-grid-cols-3'>
                    <div className='tw-w-full'>
                        <Button size='md' style={{border: '0', width: '100%'}} onClick={handleBackWindow} disabled={isLoading} variant='success'>
                            <div className='tw-flex tw-justify-center tw-items-center tw-gap-2'>
                                <BsArrowLeft/>
                                Retornar
                            </div>
                        </Button>
                    </div>
                    <div></div>
                    <div className='tw-w-full'>
                        <Button size='md' style={{backgroundColor: '#3C8DBC', border: '0', width: '100%'}} onClick={saveRequirement} disabled={isLoading}>
                            <div className='tw-flex tw-justify-center tw-items-center tw-gap-2'>
                                {!editMode?'Guardar RQ':'Grabar RQ'}
                                {
                                    isLoading ? (
                                        <Spinner animation="border" variant="light" size='sm' className='tw-ml-3'/>
                                    ):(
                                        <BsArrowDown/>
                                    )
                                }
                            </div>
                        </Button>
                    </div>
                </div>
            </div>
            {/* Fin de boton */}
        </div>
        <VentanaModal show={newItemModal?.show?.action} handleClose={(obj)=>{handleNewItemModal({show: {...{action: false}, ...obj}})}} titulo={newItemModal.title} size={newItemModal.size} feet={true} feetTitle={newItemModal.feetTitle}>
            <NuevoItem handleNewItem={handleNewItem} item={itemSelected} />
        </VentanaModal>      
    </div>
  )
}

export {NuevoRequerimiento_UI} 
