import React from "react";
import { Validar } from "../../services/ValidaSesion";
import VentanaBloqueo from "../../components/VentanaBloqueo";
import VentanaModal from "../../components/VentanaModal";
import Busqueda from "./components/busqueda";
import ResultadoTabla from "./components/resultados";
import { ListaCCxUsuarioxCia } from "../../services/centrocostoServices";
import ConfigCC_Edicion from "./components/editarconfiguracion";
import { AgregarCCxUsuario, QuitarCCxUsuario, ActualizarCCxUsuario, AgregarTodos } from "../../services/confisolrqServices";

class ConfiguracionWRQ extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: false,
            showModal: false,
            tipoOpe: 'Find',
            Cia: "",
            dataFila: {},
            Titulo: "",
            filtro: {},
            VentanaSeguridad: false
        }
    }

    handleConsultar = async (pFiltro) => {

        let busqueda = { "cia": this.state.Cia, "usuario": pFiltro }
        let resultado = await ListaCCxUsuarioxCia(busqueda)
        this.setState({ resultados: resultado, filtro: busqueda })

    }

    handleNuevo = async () => {
        this.setState({ showModal: true, tipoOpe: 'Nuevo', dataFila: {}, Titulo: "Edicion Configuracion [Nuevo]" })
    }

    handleEditar = async (fila) => {
        this.setState({ showModal: true, tipoOpe: 'Editar', dataFila: fila, Titulo: "Edicion Configuracion [Actualizar]" })
    }

    handleEliminar = async (fila) => {
        let trama = {
            "cia": fila.cia, "usuario": fila.usuario, "centrocosto_id": fila.centro_costo, "centrocosto_nombre": fila.cc_desc,
            "rol_requerimiento": fila.rol_requerimiento, "empleado_sap": fila.usuario_empleadosap
        }

        if (confirm("Desea eliminar este registro?")) {
            let sEliminar = await QuitarCCxUsuario(trama)
            mensaje = sEliminar
        }
    }

    handleGuardar = async (data) => {
        console.log(data)

        let mensaje = "";

        let trama = {
            "cia": data.cia, "usuario": data.usuario, "centrocosto_id": data.centro_costo, "centrocosto_nombre": data.cc_desc,
            "rol_requerimiento": data.rol_requerimiento, "empleado_sap": data.usuario_empleadosap
        }

        console.log(trama)

        if (this.state.tipoOpe === 'Nuevo') {
            let sGuardar = await AgregarCCxUsuario(trama)
            mensaje = sGuardar
        }

        if (this.state.tipoOpe === 'Editar') {
            let sActualizar = await ActualizarCCxUsuario(trama)
            mensaje = sActualizar
        }

        alert(mensaje)

        this.setState({ showModal: false })

        this.handleConsultar(this.state.filtro)
    }

    handleGrabarTodos = async (data) => {
        let trama = {
            "cia": data.cia, "usuario": data.usuario, "centrocosto_id": data.centro_costo, "centrocosto_nombre": data.cc_desc,
            "rol_requerimiento": data.rol_requerimiento, "empleado_sap": data.usuario_empleadosap
        }

        let sGuardarTodos = await AgregarTodos(trama)
        mensaje = sGuardarTodos
        print(sGuardarTodos)
        print(mensaje)
        this.setState({ showModal: false })
        alert(mensaje)
        this.handleConsultar(this.state.filtro)

    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad } = this.state;

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleConsultar} xCia={this.state.Cia} handleNuevo={this.handleNuevo} />
                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Información'}
                <div className="table-responsive" style={{ height: '800px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered table-striped"  >
                        <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                            <tr>
                                <th className="align-middle" >Usuario</th>
                                <th className="align-middle" >Centro Costo</th>
                                <th className="align-middle" >Centro Costo Descripcion</th>
                                <th className="align-middle" >Rol</th>
                                <th className="align-middle" >U. Sap</th>
                                <th colSpan="2" className="align-middle" >Acciones</th>
                            </tr>
                        </thead>
                        {resultados.length > 0 ?
                            resultados.map((registro, index) =>
                                <ResultadoTabla
                                    key={index}
                                    cia={registro.cia}
                                    usuario={registro.usuario}
                                    centro_costo={registro.centro_costo}
                                    cc_desc={registro.cc_desc}
                                    rol_requerimiento={registro.rol_requerimiento}
                                    rol_descripcion={registro.rol_descripcion}
                                    usuario_empleadosap={registro.usuario_empleadosap}
                                    eventoEditar={() => this.handleEditar(registro)}
                                    eventoEliminar={() => this.handleEliminar(registro)}
                                />
                            ) : ''}

                    </table>
                </div>
                <VentanaModal size={"xl"} titulo={this.state.Titulo} show={this.state.showModal} handleClose={() => this.setState({ showModal: false })}>
                    <ConfigCC_Edicion dataToEdit={this.state.dataFila} eventOnClickMasivo={this.handleGrabarTodos} nameOpe={this.state.tipoOpe} eventOnClick={this.handleGuardar} />
                </VentanaModal>
                <VentanaBloqueo show={this.state.VentanaSeguridad} />
            </React.Fragment >
        )
    }

    async componentDidMount() {
        let ValSeguridad = await Validar("LOGWRQ")
        this.setState({ VentanaSeguridad: ValSeguridad.Validar, Cia: ValSeguridad.cia, Usuario: ValSeguridad.usuario })
    }
}

export default ConfiguracionWRQ;