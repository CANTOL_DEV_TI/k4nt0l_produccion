import React, { useState } from "react";
import Title from "../../../components/Titulo";
import { BotonBuscar, BotonConsultar, BotonNuevo } from "../../../components/Botones";
import { ListaEmpSAP } from "../../../services/empleadossapServices";

class BusquedaEmpleadoSAP extends React.Component {

    constructor(props) {
        super(props);

        this.state = { cia: this.props.xCia, empleadosap: '', resultadoBusqueda: [] }
    }

    handleSettings = (e) => {

        if (e.target.name === "templeado") {
            this.setState({ empleadosap: e.target.value })
        }

    }

    handleBuscarenSAP = async (pCia, pFiltro) => {
        let BusquedaSAP = await ListaEmpSAP(pCia, pFiltro)
        this.setState({ resultadoBusqueda: BusquedaSAP })
    }

    render() {

        const { handleBusqueda, handleModal, handleSeleccionar, xCia } = this.props;

        return (
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Empleados SAP</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    Usuario : <input name="templeado" placeholder="Ingrese Usuario..." value={this.state.usuariosuario} onChange={this.handleSettings} />
                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => this.handleBuscarenSAP(this.state.cia, this.state.empleadosap)} />
                                </div>
                            </div>
                        </div>
                        <div className="row align-items-center gy-3">
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    <div className="table-responsive" style={{ height: '400px', display: "-ms-flexbox" }}>
                                        <table className="table table-hover table-sm table-bordered table-striped"  >
                                            <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                                                <tr>
                                                    <td>Cod. SAP</td>
                                                    <td>Apellidos y Nombres</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.resultadoBusqueda.length > 0 ?
                                                    this.state.resultadoBusqueda.map((registro) =>
                                                        <tr>
                                                            <td>{registro.empID}</td>
                                                            <td>{registro.Empleado}</td>
                                                            <td><BotonConsultar sw_habilitado={true} eventoClick={() => handleSeleccionar(registro.empID)} /></td>
                                                        </tr>
                                                    )
                                                    : ''
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default BusquedaEmpleadoSAP; 