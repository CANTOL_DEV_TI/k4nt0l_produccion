import React from "react";
import { GrEdit } from "react-icons/gr";
import { AiFillDelete } from "react-icons/ai";

const ResultadoTabla = ({ cia,
    usuario,
    centro_costo,
    cc_desc,
    rol_requerimiento,
    usuario_empleadosap,
    rol_descripcion,
    eventoEditar,
    eventoEliminar
}) =>
(
    <tbody>
        <tr>
            <td className="td-cadena" id={cia}>{usuario}</td>
            <td className="td-cadena" >{centro_costo}</td>
            <td className="td-cadena" >{cc_desc}</td>
            <td className="td-cadena" >{rol_descripcion}</td>
            <td className="td-cadena" >{usuario_empleadosap}</td>
            <td><button onClick={() => eventoEditar(true)}><GrEdit /></button></td>
            <td><button onClick={() => eventoEliminar(true)}><AiFillDelete /></button></td>
        </tr>
    </tbody>
)

export default ResultadoTabla;