import React from "react";
import { BotonConsultar, BotonGrabar, BotonGuardar } from "../../../components/Botones";
import VentanaModal from "../../../components/VentanaModal";
import BusquedaEmpleadoSAP from "./busquedaempleadosSAP";
import { ListaEmpSAPAll } from "../../../services/empleadossapServices";
import { Validar } from "../../../services/ValidaSesion";
import BusquedaUsuarioSWC from "./busquedaUsuarioSWC";
import BusquedaCCSAP from "./busquedaCCSAP";

class ConfigCC_Edicion extends React.Component {
    constructor(props) {
        super(props);

        console.log(this.props.dataToEdit.cia)

        if ((this.props.dataToEdit.cia === null) || (this.props.dataToEdit.cia === undefined) || this.props.dataToEdit.cia === '') {
            this.state = {
                cia: this.props.Cia, usuario: '', centro_costo: '', cc_desc: '', rol_requerimiento: 'S', usuario_empleadosap: 0,
                showBE: false, cod_emp: "", nom_emp: "", listaEmpAll: [], showBS: false, showBCC: false
            }
        } else {
            this.state = {
                cia: this.props.dataToEdit.cia, usuario: this.props.dataToEdit.usuario,
                centro_costo: this.props.dataToEdit.centro_costo, cc_desc: this.props.dataToEdit.cc_desc,
                rol_requerimiento: this.props.dataToEdit.rol_requerimiento,
                usuario_empleadosap: this.props.dataToEdit.usuario_empleadosap,
                showBE: false, cod_emp: "", nom_emp: "", listaEmpAll: [], showBS: false, showBCC: false
            }
        }
        console.log(this.state)
    }

    handleChange = (e) => {
        if (e.target.name === 'tcc_codigo') {
            this.setState({ centro_costo: e.target.value })
        }

        if (e.target.name === 'tcc_nombre') {
            this.setState({ cc_desc: e.target.value })
        }

        if (e.target.name === 'srol') {
            this.setState({ rol_requerimiento: e.target.value })
        }
    }

    handleBuscarEmp = () => {
        this.setState({ showBE: true })
    }

    handleBuscarSWC = () => {
        this.setState({ showBS: true })
    }

    handleSeleccionarEMP = (pID) => {
        console.log("EMP")
        console.log(pID)
        this.setState({ usuario_empleadosap: pID, showBE: false })

    }

    handleSeleccionarSWC = (pID) => {
        this.setState({ usuario: pID, showBS: false })
    }

    handleBuscarCC = () => {
        this.setState({ showBCC: true })
    }

    handleSeleccionarCC = (pCC, pCCDes) => {
        this.setState({ centro_costo: pCC, cc_desc: pCCDes, showBCC: false })
    }

    render() {
        const { dataToEdit, eventOnClickMasivo, eventOnClick, nameOpe, Cia } = this.props;
        return (
            <React.Fragment>
                <div className="card cardalign w-50rd">
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="form-group">
                            <div className="form-group row">
                                <div className="col-sm-2 col-form-label">Usuario</div>
                                <div className="col-sm-3 col-form-label">
                                    <input type="text" name="tusuario" disabled value={this.state.usuario} />
                                    <BotonConsultar sw_habilitado={nameOpe === 'Nuevo' ? true : false} eventoClick={this.handleBuscarSWC} />
                                </div>

                            </div>
                            <div className="form-group row">
                                <div className="col-sm-2 col-form-label">C. Costo Codigo</div>
                                <div className="col-sm-3 col-form-label">
                                    <input type="text" name="tcc_codigo" value={this.state.centro_costo} onChange={this.handleChange} />
                                    <BotonConsultar sw_habilitado={nameOpe === 'Nuevo' ? true : false} eventoClick={this.handleBuscarCC} />
                                </div>
                                <div className="col-sm-2 col-form-label">C. Costo Nombre</div>
                                <div className="col-sm-5 col-form-label">
                                    <input className="col-sm-10 " type="text" name="tcc_nombre" value={this.state.cc_desc} onChange={this.handleChange} />
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-sm-2 col-form-label">Rol</div>
                                <div className="col-sm-3 col-form-label">
                                    <select name="srol" onChange={this.handleChange} value={this.state.rol_requerimiento}>
                                        <option value={"S"}>Solicitador</option>
                                        <option value={"A"}>Aprobador</option>
                                        <option value={"M"}>Solicitador/Aprobador</option>
                                    </select>
                                </div>
                                <div className="col-sm-2 col-form-label">Empleado SAP</div>
                                <div className="col-sm-4 col-form-label">
                                    <select name="sempleadosap" disabled className="col-sm-10 col-form-label" components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }} onChange={this.handleChange} value={this.state.usuario_empleadosap}>
                                        <option value={""}>No asignado</option>
                                        {this.state.listaEmpAll.map((regemp) =>
                                            <option value={regemp.empID}>{regemp.Empleado}</option>)

                                        }
                                    </select>
                                    <BotonConsultar sw_habilitado={true} eventoClick={this.handleBuscarEmp} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <BotonGuardar texto={"Guardar"} sw_habilitado={true} eventoClick={() => eventOnClick(this.state)} />
                    {nameOpe === 'Nuevo' ?
                        <BotonGrabar textoBoton="Grabar para todos los centros de costo" sw_habilitado={true} eventoClick={() => eventOnClickMasivo(this.state)} /> : ''
                    }
                </div>
                <VentanaModal show={this.state.showBE} handleClose={() => this.setState({ showBE: false })}>
                    <BusquedaEmpleadoSAP handleSeleccionar={this.handleSeleccionarEMP} xCia={this.state.cia} />
                </VentanaModal>
                <VentanaModal show={this.state.showBS} handleClose={() => this.setState({ showBS: false })}>
                    <BusquedaUsuarioSWC handleSeleccionar={this.handleSeleccionarSWC} xCia={this.state.cia} />
                </VentanaModal>
                <VentanaModal show={this.state.showBCC} handleClose={() => this.setState({ showBCC: false })}>
                    <BusquedaCCSAP handleSeleccionar={this.handleSeleccionarCC} xCia={this.state.cia} />
                </VentanaModal>
            </React.Fragment>
        )
    }

    async componentDidMount() {
        let ValSeguridad = await Validar("LOGWRQ")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar, cia: ValSeguridad.cia, "Usuario": ValSeguridad.usuario })
        let ListaESAll = await ListaEmpSAPAll(ValSeguridad.cia)
        this.setState({ listaEmpAll: ListaESAll })
        //console.log(ListaESAll)
    }
}

export default ConfigCC_Edicion;
