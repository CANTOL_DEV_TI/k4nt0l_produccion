import React, { useState } from "react";
import Title from "../../../components/Titulo";
import { BotonBuscar, BotonConsultar, BotonNuevo } from "../../../components/Botones";

class Busqueda extends React.Component {

    constructor(props) {
        super(props);

        console.log(this.props.xCia)

        this.state = { cia: this.props.xCia, usuario: '' }
    }

    handleSettings = (e) => {

        if (e.target.name === "tusuario") {
            this.setState({ usuario: e.target.value })
        }

    }

    render() {

        const { handleBusqueda, handleModal, handleNuevo, xCia } = this.props;

        return (
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Configuracion de Centros de Costo</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    Usuario : <input name="tusuario" placeholder="Ingrese Usuario..." value={this.state.usuariosuario} onChange={this.handleSettings} />
                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => this.props.handleBusqueda(this.state.usuario)} />
                                    <BotonNuevo textoBoton="Nuevo" sw_habilitado={true} eventoClick={() => this.props.handleNuevo()} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Busqueda; 