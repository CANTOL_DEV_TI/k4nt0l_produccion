import React from "react";
import Title from "../../../components/Titulo";
import { BotonBuscar, BotonConsultar } from "../../../components/Botones";
import { ListaCCBusca } from "../../../services/centrocostoServices";

class BusquedaCCSAP extends React.Component {
    constructor(props) {
        super(props);
        this.state = { cia: this.props.xCia, cc_busqueda: '', cc_sap: '', cc_desc_sap: '', resultadoBusqueda: [] }
    }

    handleSettings = (e) => {
        if (e.target.name === 'tnombrecc') {
            this.setState({ cc_busqueda: e.target.value })
        }
    }

    handleBuscarenSAP = async (pCia, pFiltro) => {       
        let BusquedaSAP = await ListaCCBusca(pCia, pFiltro)        
        this.setState({ resultadoBusqueda: BusquedaSAP })
    }

    render(){
        const{handleBusqueda,handleModal,handleSeleccionar, xCia} = this.props;

        return(
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Centros de Costo SAP</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    Usuario : <input name="tnombrecc" placeholder="Ingrese centro de costo..." value={this.state.cc_busqueda} onChange={this.handleSettings} />
                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => this.handleBuscarenSAP(this.state.cia,this.state.cc_busqueda)} />
                                </div>
                            </div>
                        </div>
                        <div className="row align-items-center gy-3">
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    <div className="table-responsive" style={{ height: '400px', display: "-ms-flexbox" }}>
                                        <table className="table table-hover table-sm table-bordered table-striped"  >
                                            <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                                                <tr>
                                                    <td>CC SAP</td>
                                                    <td>CC Nombre</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.resultadoBusqueda.length > 0 ?
                                                    this.state.resultadoBusqueda.map((registro) =>
                                                        <tr>
                                                            <td>{registro.CC_Codigo}</td>
                                                            <td>{registro.CC_Nombre}</td>
                                                            <td><BotonConsultar sw_habilitado={true} eventoClick={()=>handleSeleccionar(registro.CC_Codigo,registro.CC_Nombre)}/></td>
                                                        </tr>
                                                    )
                                                    : ''
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default BusquedaCCSAP;