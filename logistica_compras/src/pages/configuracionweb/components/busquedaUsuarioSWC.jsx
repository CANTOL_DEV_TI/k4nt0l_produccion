import React from "react";
import Title from "../../../components/Titulo";
import { BotonBuscar, BotonConsultar } from "../../../components/Botones";
import { ListadoUsuariosSWC } from "../../../services/confisolrqServices";


class BusquedaUsuarioSWC extends React.Component {
    constructor(props) {
        super(props);

        this.state = { cia: this.props.xCia, usuarioswc: '', resultadoBusqueda: [] }
    }

    handleSettings = (e) => {
        this.setState({ usuarioswc: e.target.value })
    }

    handleBuscarenSWC = async (pCia, pFiltro) => {
        console.log(pCia)
        console.log(pFiltro)
        
        let BusquedaSWC = await ListadoUsuariosSWC(pCia, pFiltro)
        console.log(BusquedaSWC)
        this.setState({ resultadoBusqueda: BusquedaSWC })
    }

    render() {
        const { handleBusqueda, handleModal, handleSeleccionar } = this.props;

        return (
            <React.Fragment>
                <div className="col-lg-12">
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-4">
                            <div className="col-sm">
                                <Title>Usuarios SWC</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    Usuario : <input name="tusuario" placeholder="Ingrese Usuario..." value={this.state.usuarioswc} onChange={this.handleSettings} />
                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => this.handleBuscarenSWC(this.state.cia, this.state.usuarioswc)} />
                                </div>
                            </div>
                        </div>
                        <div className="row align-items-center gy-4">
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    <div className="table-responsive" style={{ height: '400px', display: "-ms-flexbox" }}>
                                        <table className="table table-hover table-sm table-bordered table-striped"  >
                                            <thead className="table-secondary text-center" style={{ position: "sticky", top: 0 }}>
                                                <tr>
                                                    <td>Usuario SWC</td>
                                                    <td>Apellidos y Nombres</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.resultadoBusqueda.length > 0 ?
                                                    this.state.resultadoBusqueda.map((registro) =>
                                                        <tr>
                                                            <td>{registro.Usuario_Codigo}</td>
                                                            <td>{registro.Usuario_Nombre}</td>
                                                            <td><BotonConsultar sw_habilitado={true} eventoClick={() => handleSeleccionar(registro.Usuario_Codigo)} /></td>
                                                        </tr>
                                                    )
                                                    : ''
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment >
        )

    }

}

export default BusquedaUsuarioSWC;