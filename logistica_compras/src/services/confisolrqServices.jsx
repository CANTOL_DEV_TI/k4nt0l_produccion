import { Url } from "../constants/global";

const meServidorBackend = Url;

const url = `${meServidorBackend}/requerimientos`;

const cabecera = { 'Content-type': 'application/json; charset=UTF-8' }

export async function AgregarCCxUsuario(pData) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(pData)
    }
    const response = await fetch(`${url}/centrocostorq/agregar/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function QuitarCCxUsuario(pData) {
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,
        body: JSON.stringify(pData)
    }
    const response = await fetch(`${url}/centrocostorq/remover/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function ActualizarCCxUsuario(pData) {
    const requestOptions = {
        method: 'PATCH',
        headers: cabecera,
        body: JSON.stringify(pData)
    }
    const response = await fetch(`${url}/centrocostorq/actualizar/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function ListadoUsuariosSWC(pCia, pFiltro) {

    if (pFiltro === "") {
        pFiltro = "%20"
    }

    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }

    const response = await fetch(`${url}/listausuariosxcia/${pCia}/${pFiltro}`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function ListadoReporteSol(pFiltro) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(pFiltro)
    }

    const response = await fetch(`${meServidorBackend}/compras_logistica/listado_solicitudes/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function AgregarTodos(pData) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(pData)
    }

    const response = await fetch(`${url}/centrocostorq/guardartodos/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}