import { Url } from "../constants/global";

const meServidorBackend = Url;

const url = `${meServidorBackend}/requerimientos`;

const cabecera = { 'Content-type': 'application/json; charset=UTF-8' }

export async function ListaCCxUsuarioxCia(pData) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(pData)
    }

    const response = await fetch(`${url}/listaccxusuarioxcia/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function ListaCCBusca(pCia, pFiltro) {
    if (pFiltro === "") {
        pFiltro = "%20"
    }
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }

    const response = await fetch(`${url}/centrocostosaplista/${pCia}/${pFiltro}`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}