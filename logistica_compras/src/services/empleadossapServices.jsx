import { Url } from "../constants/global";

const meServidorBackend = Url;

const url = `${meServidorBackend}/requerimientos`;

const cabecera = { 'Content-type': 'application/json; charset=UTF-8' }

export async function ListaEmpSAP(pCia,pFiltro) {
    if (pFiltro === "") {
        pFiltro = "%20"
    }
    
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }    
    const response = await fetch(`${url}/listasapempleados/${pCia}/${pFiltro}`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function ListaEmpSAPAll(pCia) {
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }    
    const response = await fetch(`${url}/listasapempleadosall/${pCia}/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}