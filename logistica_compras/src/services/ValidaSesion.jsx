import { validaToken, validaAcceso } from "./usuarioServices";

export async function Validar(Opcion) {
    try {
        const Val = { Validar: false, cia: "", usuario: "" };

        const token = sessionStorage.getItem("CDTToken", "XXXXX");

        if (token === "") {
            Val.Validar = true

        } else {
            if (token === null) {
                Val.Validar = true

            } else {

                const valor = await validaToken(token);
                const usuario_login = valor.sub.substr(3)
                const usuario_empresa = valor.sub.substr(0, 3)

                const evaluar = { "usuario_login": usuario_login, "empresa_codigo": usuario_empresa, "modulo_codigo": Opcion }

                const responseAcc = await validaAcceso(evaluar)
                //console.log(responseAcc)
                if (responseAcc.acceso === 0) {
                    if (Opcion !== "") {
                        Val.Validar = true
                        Val.cia = usuario_empresa
                        Val.usuario = ""
                    } else {
                        Val.Validar = true
                        Val.cia = usuario_empresa
                        Val.usuario = usuario_login
                    }
                } else {
                    Val.Validar = false
                    Val.cia = usuario_empresa
                    Val.usuario = usuario_login
                }
            }
        }

        return Val
    }
    catch
    {
        return { Validar: true, cia: "", usuario: "" }
    }
};