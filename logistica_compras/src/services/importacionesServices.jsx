import {Url} from "../constants/global"; 
 
const meServidorBackend = Url; 
 
const url = `${meServidorBackend}/compras_logistica`; 
 
const cabecera = {'Content-type': 'application/json; charset=UTF-8'} 

export async function GetListado(pCia,pFiltro,pTipo) {
    if(pFiltro.trim()===""){
        pFiltro="%20"
    }

    const requestOptions = {
        method : 'GET',
        headers : cabecera
    }

    const response = await fetch(`${url}/importaciones/reporte/${pCia}/${pFiltro}/${pTipo}`,requestOptions)
    const responseJson = await response.json()
    return responseJson

}

export async function GetDetallexArticulo(pCia,pItem,pAlmacen) {
    const requestOptions = {
        method : 'GET',
        headers : cabecera
    }

    const response = await fetch(`${url}/importaciones/reporte/detalle/${pCia}/${pItem}/${pAlmacen}`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function GetListaArticulos(pCia,pFiltro){
    const requestOptions = {
        method : 'GET',
        headers : cabecera
    }

    const response = await fetch(`${url}/importaciones/listadoarticulos/${pCia}/${pFiltro}`,requestOptions)
    const responseJson = await response.json()
    return responseJson
}