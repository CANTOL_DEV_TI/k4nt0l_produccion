import {Url} from "../constants/global"; 
 
const meServidorBackend = Url; 
 
const url = `${meServidorBackend}/compras_logistica`; 
 
const cabecera = {'Content-type': 'application/json; charset=UTF-8'} 
 
export async function GetConsulta(pCodigo) 
{ 
    const requestOptions = { 
        method: 'GET', 
        headers: cabecera 
    } 
    const response = await fetch(`${url}/drawback/${pCodigo}`,requestOptions) 
    const responseJson = await response.json()     
    return responseJson 
} 
 
export async function GetArticulos(pFiltro) 
{ 
    const requestOptions = { 
        method : 'GET', 
        headers : cabecera 
    } 
 
    const response = await fetch(`${url}/drawback/ListaArticulos/${pFiltro}`,requestOptions) 
    const responseJson = await response.json()     
    return responseJson 
 
}