import React from 'react'
import Modal from 'react-bootstrap/Modal';

const VentanaModal = ({ children, show, handleClose, titulo, size }) => {
    return (
        <Modal show={show} onHide={handleClose} size={size}> { /*backdrop="static" keyboard={false} */}
            { /*size="sm,lg,xl" */}
            <Modal.Header closeButton>
                <Modal.Title>{titulo}</Modal.Title>
            </Modal.Header>
            {titulo !== "Error al iniciar" &&
                <Modal.Body>
                    {children}
                </Modal.Body>
            }
            {/* <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
            </Modal.Footer>  */}
        </Modal>
    );
};
export default VentanaModal