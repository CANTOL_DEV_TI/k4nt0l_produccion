import {Url} from '../constants/global'

// const meServidorBackend = 'http://192.168.5.21:8080'
const meServidorBackend = Url

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function getFilter_Modulos(txtFind) {
    if (txtFind.trim() === '') {
        txtFind = '%20'
    }
    
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/modulos/${txtFind}`,requestOptions)
    const responseJson = await response.json()    
    return responseJson
}

export async function getModulosPadre() {       
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/modulos/Padres/`,requestOptions)
    const responseJson = await response.json()    
    return responseJson
}

export async function add_Modulo(meJson){   
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };
    console.log(meJson)
    const response = await fetch(`${meServidorBackend}/seguridad/modulos/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function upd_Modulo(meJson){   
    const requestOptions = {
        method: 'PUT',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };
    
    const response = await fetch(`${meServidorBackend}/seguridad/modulos/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function del_Modulo(meJson){   
    const requestOptions = {
        method: 'DELETE',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/seguridad/modulos/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}