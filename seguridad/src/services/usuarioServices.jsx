import { Url } from '../constants/global'

// const meServidorBackend = 'http://192.168.5.21:8080'
const meServidorBackend = Url

const cabecera = { 'Content-type': 'application/json; charset=UTF-8' }

export async function getFilter_Usuario(txtFind) {
    if (txtFind.trim() === '') {
        txtFind = '%20'
    }

    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/${txtFind}`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function add_Usuario(meJson) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function upd_Usuario(meJson) {
    const requestOptions = {
        method: 'PUT',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function del_Usuario(meJson) {
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function add_Cia_Usuario(meJson) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/AgregarCia/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function del_Cia_Usuario(meJson) {
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };
    console.log(meJson)
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/QuitarCia/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function ActPassword(meJson) {
    const requestOptions = {
        method: 'PUT',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/ActPassword/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function getEmpresas() {
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/ListaEmpresas/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getEmpresasAsignadas(pUsuario) {
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/CiaAsignadas/${pUsuario}`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getListaModulos() {
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/ListaModulos/`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getListaSubModulos(pModuloP) {
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/ListaSubModulos/${pModuloP}`, requestOptions)
    const responseJson = await response.json()
    return responseJson
}

export async function getListaPermisosAsignados(pUsuario, pEmpresa) {
    const requestOptions = {
        method: 'GET',
        headers: cabecera
    }

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/ListaPermisos/${pUsuario}-${pEmpresa}/`, requestOptions)
    const responseJson = await response.json()
    return responseJson

}

export async function agregarAcceso(meJson) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/AgregarPermiso/`, requestOptions);
    const responseJson = await response.json();
    return responseJson

}

export async function quitarAcceso(meJson) {
    const requestOptions = {
        method: 'DELETE',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };
    console.log(meJson)
    const response = await fetch(`${meServidorBackend}/seguridad/usuario/QuitarPermiso/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function validaToken(meToken) {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer " + meToken);

    const requestOptions = {
        method: 'POST',
        headers: myHeaders,
    };

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/validar_token/`, requestOptions);
    const responseJson = await response.json();
    return responseJson

}

export async function validaAcceso(meJson) {
    const requestOptions = {
        method: 'POST',
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/VerificaPermiso/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function ResetClave(pData) {
    const requestOptions = {
        method: 'PATCH',
        headers: cabecera
    };

    const response = await fetch(`${meServidorBackend}/seguridad/usuario/ResetClave/${pData}`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}