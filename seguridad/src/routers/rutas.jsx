import { Routes, Route, } from "react-router-dom";

import LoginV from "../pages/login/login";
import Usuarios from "../pages/usuario/usuario.jsx";
import ActuaPassword from "../pages/login/cambiocontraseña";
import Empresas from "../pages/empresa/empresa.jsx"
import Modulos from "../pages/modulo/modulo.jsx"
import React from "react";
import ProtectedRoute from "../components/utils/ProtectedRoute.jsx"


export function MiRutas() {
    return (
        <Routes>
            <Route path="/" element={<LoginV />} />
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/usuarios" element={<Usuarios />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/empresas" element={<Empresas />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/cambiocontrasena" element={<ActuaPassword />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/modulos" element={<Modulos />} />
            </Route>
        </Routes>
    );
}