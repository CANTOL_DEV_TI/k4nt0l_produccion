import React from "react";
import { BotonGuardar } from "../../../components/Botones";
import { Tab, Tabs } from "react-bootstrap-tabs";
import { lista_SeriesDoc } from "../../../services/empresasServices";
import { ComboBoxForm } from "../../../components/ComboBox.jsx"


class EmpresaEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = {
                empresa_id: 0, empresa_codigo: '', empresa_nombre: '', empresa_ruc: '', empresa_correo: '', SN: [],
                serie_pagos_efectuados: '', serie_pagos_recibidos: '', serie_boletas: '', serie_facturas: '',
                serie_orden_venta: '', serie_nota_credito: '', serie_nota_debito: '', serie_letras_refinanciadas: ''
            }
        } else {
            this.state = {
                empresa_id: this.props.dataToEdit.empresa_id, empresa_codigo: this.props.dataToEdit.empresa_codigo,
                empresa_nombre: this.props.dataToEdit.empresa_nombre, empresa_ruc: this.props.dataToEdit.empresa_ruc,
                empresa_correo: this.props.dataToEdit.empresa_correo, SN: [],
                serie_pagos_efectuados: this.props.dataToEdit.serie_pagos_efectuados, serie_pagos_recibidos: this.props.dataToEdit.serie_pagos_recibidos,
                serie_boletas: this.props.dataToEdit.serie_boletas, serie_facturas: this.props.dataToEdit.serie_facturas,
                serie_orden_venta: this.props.dataToEdit.serie_orden_venta, serie_nota_credito: this.props.dataToEdit.serie_nota_credito,
                serie_nota_debito: this.props.dataToEdit.serie_nota_debito, serie_letras_refinanciadas: this.props.dataToEdit.serie_letras_refinanciadas
            }
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'empresa_id') {
            this.setState({ empresa_id: e.target.value })
        }

        if (e.target.name === 'empresa_codigo') {
            this.setState({ empresa_codigo: e.target.value })
        }

        if (e.target.name === 'empresa_nombre') {
            this.setState({ empresa_nombre: e.target.value })
        }

        if (e.target.name === 'empresa_ruc') {
            this.setState({ empresa_ruc: e.target.value })
        }

        if (e.target.name === 'empresa_correo') {
            this.setState({ empresa_correo: e.target.value })
        }

        if (e.target.name === 'spagosefectuados') {
            this.setState({ serie_pagos_efectuados: e.target.value })
        }

        if (e.target.name === 'spagosrecibidos') {
            this.setState({ serie_pagos_recibidos: e.target.value })
        }

        if (e.target.name === 'sboletas') {
            this.setState({ serie_boletas: e.target.value })
        }

        if (e.target.name === 'sfacturas') {
            this.setState({ serie_facturas: e.target.value })
        }

        if (e.target.name === 'snotacredito') {
            this.setState({ serie_nota_credito: e.target.value })
        }

        if (e.target.name === 'snotadebito') {
            this.setState({ serie_nota_debito: e.target.value })
        }

        if (e.target.name === 'sordenventa') {
            this.setState({ serie_orden_venta: e.target.value })
        }

        if (e.target.name === 'sletrasrefinanciadas') {
            this.setState({ serie_letras_refinanciadas: e.target.value })
        }
    }


    render() {
        const { eventOnclick, nameOpe, xCia } = this.props;

        return (
            <div className="card cardalign w-50rd">
                <Tabs>
                    <Tab label="Datos Principales">
                        <div className="card-header border border-dashed border-end-0 border-start-0">
                            <div className="form-group">
                                <div className="form-group row">
                                    <div className="col-sm-3 col-form-label">ID : </div>
                                    <div className="col-sm-2 col-form-label"><input type="text" name="empresa_id" autoFocus disabled onChange={this.handleChange} value={this.state.empresa_id} /></div>
                                    <div className="col-sm-2 col-form-label"></div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-3 col-form-label">Codigo : </div>
                                    <div className="col-sm-2 col-form-label"><input type="text" name="empresa_codigo" onChange={this.handleChange} value={this.state.empresa_codigo} /></div>
                                    <div className="col-sm-2 col-form-label"></div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-3 col-form-label">Nombre : </div>
                                    <div className="col-sm-2 col-form-label"><input type="text" name="empresa_nombre" onChange={this.handleChange} value={this.state.empresa_nombre} /></div>
                                    <div className="col-sm-2 col-form-label"></div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-3 col-form-label">R.U.C. : </div>
                                    <div className="col-sm-2 col-form-label"><input type="text" name="empresa_ruc" onChange={this.handleChange} value={this.state.empresa_ruc} /></div>
                                    <div className="col-sm-2 col-form-label"></div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-3 col-form-label">Correo : </div>
                                    <div className="col-sm-4 col-form-label"><input type="text" name="empresa_correo" onChange={this.handleChange} value={this.state.empresa_correo} /></div>
                                    <div className="col-sm-1 col-form-label"></div>
                                </div>

                            </div>
                        </div>
                    </Tab>
                    <Tab label="Series de Documentos">
                        <div className="card-header border border-dashed border-end-0 border-start-0">
                            <div className="form-group">
                                <div className="form-group row">
                                    <div className="col-sm-3 col-form-label">Pagos Efectuados : </div>
                                    <div className="col-sm-3 col-form-label">
                                        <ComboBoxForm datosRow={this.state.SN} nombre_cbo={"spagosefectuados"} valor_ini={"Seleccionar..."} valor={this.state.serie_pagos_efectuados} manejaEvento={this.handleChange} />
                                    </div>
                                    <div className="col-sm-3 col-form-label">Pagos Recibidos : </div>
                                    <div className="col-sm-3 col-form-label">
                                        <ComboBoxForm datosRow={this.state.SN} nombre_cbo={"spagosrecibidos"} valor_ini={"Seleccionar..."} valor={this.state.serie_pagos_recibidos} manejaEvento={this.handleChange} />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-3 col-form-label">Boletas : </div>
                                    <div className="col-sm-3 col-form-label">
                                        <ComboBoxForm datosRow={this.state.SN} nombre_cbo={"sboletas"} valor_ini={"Seleccionar..."} valor={this.state.serie_boletas} manejaEvento={this.handleChange} />
                                    </div>
                                    <div className="col-sm-3 col-form-label">Facturas : </div>
                                    <div className="col-sm-3 col-form-label">
                                        <ComboBoxForm datosRow={this.state.SN} nombre_cbo={"sfacturas"} valor_ini={"Seleccionar..."} valor={this.state.serie_facturas} manejaEvento={this.handleChange} />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-3 col-form-label">Nota de Credito : </div>
                                    <div className="col-sm-3 col-form-label">
                                        <ComboBoxForm datosRow={this.state.SN} nombre_cbo={"snotacredito"} valor_ini={"Seleccionar..."} valor={this.state.serie_nota_credito} manejaEvento={this.handleChange} />
                                    </div>
                                    <div className="col-sm-3 col-form-label">Nota de Debito : </div>
                                    <div className="col-sm-3 col-form-label">
                                        <ComboBoxForm datosRow={this.state.SN} nombre_cbo={"snotadebito"} valor_ini={"Seleccionar..."} valor={this.state.serie_nota_debito} manejaEvento={this.handleChange} />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-3 col-form-label">Orden de Venta : </div>
                                    <div className="col-sm-3 col-form-label">
                                        <ComboBoxForm datosRow={this.state.SN} nombre_cbo={"sordenventa"} valor_ini={"Seleccionar..."} valor={this.state.serie_orden_venta} manejaEvento={this.handleChange} />
                                    </div>
                                    <div className="col-sm-3 col-form-label">Letras Refinan. : </div>
                                    <div className="col-sm-3 col-form-label">
                                        <ComboBoxForm datosRow={this.state.SN} nombre_cbo={"sletrasrefinanciadas"} valor_ini={"Seleccionar..."} valor={this.state.serie_letras_refinanciadas} manejaEvento={this.handleChange} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Tab>
                </Tabs>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "empresa_id": this.state.empresa_id, "empresa_codigo": this.state.empresa_codigo, 
                    "empresa_nombre": this.state.empresa_nombre, "empresa_ruc": this.state.empresa_ruc, "empresa_correo": this.state.empresa_correo ,
                    "serie_pagos_efectuados" : this.state.serie_pagos_efectuados, "serie_pagos_recibidos":this.state.serie_pagos_recibidos,"serie_boletas":this.state.serie_boletas,
                    "serie_facturas" : this.state.serie_facturas, "serie_nota_credito" : this.state.serie_nota_credito, "serie_nota_debito":this.state.serie_nota_debito,
                    "serie_orden_venta" : this.state.serie_orden_venta, "serie_letras_refinanciadas" : this.state.serie_letras_refinanciadas
                    })} texto={nameOpe} />
            </div>
        )
    }
    async componentDidMount() {
        console.log(this.props.dataToEdit.empresa_codigo)
        let tCia = this.props.dataToEdit.empresa_codigo;
        let ListaSN = await lista_SeriesDoc(tCia);
        this.setState({ SN : ListaSN });
    }
}

export default EmpresaEdicion;