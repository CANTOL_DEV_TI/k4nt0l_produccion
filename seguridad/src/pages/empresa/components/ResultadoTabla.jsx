import React from "react";
import {GrEdit,GrAmazon,GrCodeSandbox,GrTask} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({empresa_id,                        
                        empresa_codigo,
                        empresa_nombre,                        
                        empresa_ruc,
                        empresa_correo,
                        eventoEditar,
                        eventoEliminar                       
                        }) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena" id={empresa_id}>{empresa_codigo}</td>                
                <td className="td-cadena" >{empresa_nombre}</td>
                <td className="td-cadena" >{empresa_ruc}</td>                
                <td className="td-cadena" >{empresa_correo}</td>                
                <td style={{textAlign:"center"}}><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td style={{textAlign:"center"}}><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                
            </tr>
        </tbody>
    )

export default ResultadoTabla;