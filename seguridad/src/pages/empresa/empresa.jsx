import React from "react";

import { getFilter_Empresas,add_Empresa,upd_Empresa,del_Empresa,lista_SeriesDoc } from "../../services/empresasServices";

import Busqueda from "../empresa/components/Busqueda";
import ResultadoTabla from "../empresa/components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal";
import EmpresaEdicion from "./components/empresaEdicion";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo";

class Empresas extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            Cia : ''
        }
    }

    handleBusqueda = async (txtFind) => {
        const responseJson = await getFilter_Empresas(txtFind)        
        this.setState({ resultados: responseJson, isFetch: false })    
        console.log(responseJson)    
    }

    handleSubmit = async (e) => {
        
        if (this.state.tipoOpe === 'Grabar') {
            const responseJson = await add_Empresa(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar') {
            const responseJson = await upd_Empresa(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar') {
            const responseJson = await del_Empresa(e)
            console.log(responseJson)
        }


        this.setState({ showModal: false, tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad} = this.state

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} />


                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>                                
                                <th className="align-middle">Codigo</th>
                                <th className="align-middle">Nombre</th>
                                <th className="align-middle">RUC</th>                                
                                <th className="align-middle">Correo</th>                                                                
                                <th className="align-middle">Editar</th>
                                <th className="align-middle">Eliminar</th>                                
                            </tr>
                        </thead>
                        
                        {resultados.map((registro) =>
                            <ResultadoTabla
                                key={registro.empresa_id}                                
                                empresa_codigo={registro.empresa_codigo}
                                empresa_nombre={registro.empresa_nombre}    
                                empresa_ruc={registro.empresa_ruc}     
                                empresa_correo={registro.empresa_correo}                                    
                                eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}                                
                            />
                        )}
                    </table>
                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({ showModal: false })}
                    titulo="Edición Empresas">
                    <EmpresaEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={this.state.tipoOpe}
                        xCia={this.state.Cia}
                    />
                </VentanaModal>    
                <VentanaBloqueo show={VentanaSeguridad} />
            </React.Fragment>
        )

    }
    async componentDidMount() {
        const ValSeguridad = await Validar("SEGEMP")
        console.log(ValSeguridad)
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })        
    }
}

export default Empresas;