import React from "react";

import { getFilter_Usuario,add_Usuario,upd_Usuario,del_Usuario,getEmpresas,getEmpresasAsignadas } from "../../services/usuarioServices";

import Busqueda from "../usuario/components/Busqueda";
import ResultadoTabla from "../usuario/components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal";
import UsuarioEdicion from "./components/usuarioEdicion.jsx";
import UsuarioCiasEdicion from "./components/usuariocompaniasEdicion";
import UsuarioPermisosEdicion from "./components/usuariopermisosEdicion";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo";

class Usuarios extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            showModalA: false,
            showModalP : false,
            tipoOpe: 'Find',
            listaEmp : null            
        }
    }

    handleBusqueda = async (txtFind) => {
        const responseJson = await getFilter_Usuario(txtFind)        
        this.setState({ resultados: responseJson, isFetch: false })      
        console.log(responseJson)  
    }

    handleSubmit = async (e) => {
        
        if (this.state.tipoOpe === 'Grabar') {
            const responseJson = await add_Usuario(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar') {
            const responseJson = await upd_Usuario(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar') {
            const responseJson = await del_Usuario(e)
            console.log(responseJson)
        }


        this.setState({ showModal: false, showModalA: false, showModalP: false,tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal ,showModalA, showModalP, VentanaSeguridad} = this.state

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} />


                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>                                
                                <th className="align-middle">Login</th>
                                <th className="align-middle">Nombre</th>
                                <th className="align-middle">Estado</th>                                
                                <th className="align-middle">Editar</th>
                                <th className="align-middle">Eliminar</th>
                                <th className="align-middle">Compañias</th>
                                <th className="align-middle">Modulos</th>
                            </tr>
                        </thead>
                        
                        {resultados.map((registro) =>
                            <ResultadoTabla
                                key={registro.usuario_id}                                
                                usuario_codigo={registro.usuario_codigo}
                                usuario_nombre={registro.usuario_nombre}    
                                usuario_estado={registro.usuario_estado}                            
                                total_turnos={registro.total_turnos}
                                eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}
                                eventoAcceso={() => this.setState({ showModalA: true, dataRegistro: registro, tipoOpe: 'Acceso' })}
                                eventoModulos={() => this.setState({ showModalP: true, dataRegistro: registro, tipoOpe: 'Permisos' })}
                            />
                        )}
                    </table>
                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({ showModal: false })}
                    titulo="Edición Usuarios">
                    <UsuarioEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={this.state.tipoOpe}
                    />
                </VentanaModal>
                <VentanaModal
                    show={showModalA}
                    size="lg"
                    handleClose={() => this.setState({ showModalA: false })}
                    titulo="Acceso a Empresa">
                    <UsuarioCiasEdicion
                        dataToEdit = {this.state.dataRegistro}                        
                        nameOpe={"Accesos"}
                        ListEmpresas={this.state.listaEmp}                        
                    />
                </VentanaModal>
                <VentanaModal 
                    show={showModalP}
                    size = "lg"
                    handleClose={() => this.setState({ showModalP: false })}
                    titulo="Acceso a Modulos"
                >
                    <UsuarioPermisosEdicion
                        dataToEdit={this.state.dataRegistro}
                        nameOpe={"Modulos"}
                        ListEmpresas={this.state.listaEmp} />

                </VentanaModal>
                <VentanaBloqueo show={VentanaSeguridad} />

            </React.Fragment>
        )

    }
    async componentDidMount(){
        const ValSeguridad = await Validar("SEGUSU")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
        
        const ListaEmpresas = await getEmpresas();        
        this.setState({listaEmp : ListaEmpresas})        
    }
}

export default Usuarios;