import React from "react";
import { BotonGuardar } from "../../../components/Botones";


class ModuloEdicion extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.dataToEdit == null) {
            this.state = { modulo_id: 0, modulo_codigo: '', modulo_nombre: '', modulo_padre: 0, modulo_padre_id: 0 }
        } else {
            this.state = { modulo_id: this.props.dataToEdit.modulo_id, modulo_codigo: this.props.dataToEdit.modulo_codigo, modulo_nombre: this.props.dataToEdit.modulo_nombre, modulo_padre: this.props.dataToEdit.modulo_padre, modulo_padre_id: this.props.dataToEdit.modulo_padre_id }
        }
    }

    handleChange = (e) => {
        if (e.target.name === 'modulo_id') {
            this.setState({ modulo_id : e.target.value })
        }

        if (e.target.name === 'modulo_codigo') {
            this.setState({ modulo_codigo: e.target.value })
        }

        if (e.target.name === 'modulo_nombre') {
            this.setState({ modulo_nombre: e.target.value })
        }

        if (e.target.name === 'modulo_padre') {
            this.setState({ modulo_padre: e.target.value })
        }

        if (e.target.name === 'modulo_padre_id') {
            this.setState({ modulo_padre_id: e.target.value })
        }
    }


    render() {
        const { eventOnclick, nameOpe, listaPadres } = this.props

        return (
            <div className="card cardalign w-50rd">
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="form-group">
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">ID : </div>
                            <div className="col-sm-2 col-form-label"><input type="text" className="form-control form-control-sm" name="modulo_id" autoFocus onChange={this.handleChange} value={this.state.modulo_id} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Codigo : </div>
                            <div className="col-sm-3 col-form-label"><input type="text" className="form-control form-control-sm" name="modulo_codigo" onChange={this.handleChange} value={this.state.modulo_codigo} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Nombre : </div>
                            <div className="col-sm-6 col-form-label"><input type="text" className="form-control form-control-sm" name="modulo_nombre" onChange={this.handleChange} value={this.state.modulo_nombre} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Principal : </div>
                            <div className="col-sm-2 col-form-label"><input type="number" className="form-control form-control" name="modulo_padre" onChange={this.handleChange} value={this.state.modulo_padre} /></div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-3 col-form-label">Menu Principal : </div>
                            <div className="col-sm-4 col-form-label">
                            <select className='form-control form-control' name="modulo_padre_id" onChange={this.handleChange}>
                                    <option value='0'>Seleccionar Modulo Padre</option>
                                    {
                                        listaPadres.map((v_padres) =>
                                            this.state.modulo_padre_id === v_padres.modulo_id ?
                                                <option selected value={v_padres.modulo_id}>{`${v_padres.modulo_nombre}`}</option>
                                                :
                                                <option value={v_padres.modulo_id}>{`${v_padres.modulo_nombre}`}</option>
                                        )
                                    }
                                </select>
                                </div>
                            <div className="col-sm-2 col-form-label"></div>
                        </div>

                    </div>
                </div>
                <BotonGuardar sw_habilitado={true} eventoClick={() => eventOnclick({ "modulo_id": this.state.modulo_id, "modulo_codigo": this.state.modulo_codigo, "modulo_nombre": this.state.modulo_nombre, "modulo_padre": this.state.modulo_padre, "modulo_padre_id" : this.state.modulo_padre_id })} texto={nameOpe} />
            </div>
        )
    }
    
}

export default ModuloEdicion;