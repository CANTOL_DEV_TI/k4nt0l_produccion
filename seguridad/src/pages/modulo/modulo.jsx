import React from "react";

import { getFilter_Modulos,add_Modulo,upd_Modulo,del_Modulo } from "../../services/modulosServices";

import Busqueda from "./components/Busqueda";
import ResultadoTabla from "./components/ResultadoTabla";
import VentanaModal from "../../components/VentanaModal";
import ModuloEdicion from "./components/moduloEdicion";
import { getModulosPadre } from "../../services/modulosServices";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo";

class Modulos extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            listpadres : []
        }
    }

    handleBusqueda = async (txtFind) => {
        const responseJson = await getFilter_Modulos(txtFind)        
        this.setState({ resultados: responseJson, isFetch: false })        
    }

    handleSubmit = async (e) => {
        
        if (this.state.tipoOpe === 'Grabar') {
            const responseJson = await add_Modulo(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Actualizar') {
            const responseJson = await upd_Modulo(e)
            console.log(responseJson)
        }

        if (this.state.tipoOpe === 'Eliminar') {
            const responseJson = await del_Modulo(e)
            console.log(responseJson)
        }


        this.setState({ showModal: false, tipoOpe: 'Find' })
    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad} = this.state

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda} handleModal={() => this.setState({ showModal: true, dataRegistro: null, tipoOpe: 'Grabar' })} />


                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{position:"sticky", top:0}}>
                            <tr>                                
                                <th className="align-middle">Codigo</th>
                                <th className="align-middle">Nombre</th>
                                <th className="align-middle">Modulo Padre</th>  
                                <th className="align-middle">Modulo Padre ID</th>                                                                                                
                                <th className="align-middle">Editar</th>
                                <th className="align-middle">Eliminar</th>                                
                            </tr>
                        </thead>
                        
                        {resultados.map((registro) =>
                            <ResultadoTabla
                                key={registro.modulo_id}                                
                                modulo_codigo={registro.modulo_codigo}
                                modulo_nombre={registro.modulo_nombre}    
                                modulo_padre={registro.modulo_padre}     
                                modulo_padre_id={registro.modulo_padre_id}                                    
                                eventoEditar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Actualizar' })}
                                eventoEliminar={() => this.setState({ showModal: true, dataRegistro: registro, tipoOpe: 'Eliminar' })}                                
                            />
                        )}
                    </table>
                </div>

                <VentanaModal
                    show={showModal}
                    size="lg"
                    handleClose={() => this.setState({ showModal: false })}
                    titulo="Edición Empresas">
                    <ModuloEdicion
                        dataToEdit={this.state.dataRegistro}
                        eventOnclick={this.handleSubmit}
                        nameOpe={this.state.tipoOpe}                        
                        listaPadres={this.state.listpadres}
                    />
                </VentanaModal>                
                <VentanaBloqueo show={VentanaSeguridad} />
            </React.Fragment>
        )

    }
    async componentDidMount(){

        const ValSeguridad = await Validar("SEGMOL")
        this.setState({ "VentanaSeguridad": ValSeguridad.Validar })
        
        const ListaPadres = await getModulosPadre();  
        
        this.setState({listpadres: ListaPadres})
        
    }
}

export default Modulos;