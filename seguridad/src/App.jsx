import React, { useState, useEffect } from 'react'
import { BrowserRouter } from "react-router-dom";
import { MiRutas } from "./routers/rutas"
//import MiRutas from './routers/rutas';
import MenuNav from "./components/navegacion/MenuNav"
// import NavbarV4 from "./components/navegacion/Navbar_V4/NavbarV4";
import NavbarV1 from "./components/navegacion/Navbar_V1/NavbarV1"

function App() {

  const [validSesion, setvalidSesion] = useState(true);
  const Seguridad = async () => {
    const token = sessionStorage.getItem("CDTToken");
    
    console.log(token)
    if (token === "") {
      setvalidSesion(false)
    } /*else {
      //const valor = await validaToken(token);
      const valor = await validaToken(token);
      //console.log(valor)
      const usuario_login = valor.sub.substr(3)
      const usuario_empresa = valor.sub.substr(0, 3)

      const evaluar = { "usuario_login": usuario_login, "empresa_codigo": usuario_empresa, "modulo_codigo": "PROARF" }

      //const responseAcc = await validaAcceso(evaluar)
      const responseAcc = await validaAcceso(evaluar)

      if (responseAcc.acceso === 0) {
        setVentanaSeguridad(true)
        //console.log("No tiene permiso")
      }
    }*/
    console.log(validSesion)
  };
  useEffect(() => {    
    Seguridad();
  }, [])


  return (
    <BrowserRouter basename="seguridad">
      <div className="page-content">          
          <NavbarV1 />                
        <div className="container-fluid">
          <MiRutas />
        </div>
      </div>
    </BrowserRouter>
  )
}

export default App