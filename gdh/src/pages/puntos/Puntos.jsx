import React, { useState, useEffect } from 'react';
import Busqueda from "./componentes/BusquedaPuntos.jsx";
import {ResultadoTablaPuntos} from './componentes/ResultadoTabla.jsx';
import {InsertarPuntos, InsertarPuntosMasivo, BuscarUsuario, ListarPuntosConsolidado } from './services/PuntosService.jsx'
import ModalInsertarPuntos from './modal/puntos/ModalAgregar.jsx';
import UploadButton from './modal/puntos/ModalAgregarMasivo.jsx';
import { ObtenerCampanias } from './services/CampaniaService.jsx';
import { ModalDetallePuntos } from './modal/puntos/ModalDetallePuntos.jsx';
import { ModalDetalleCardex } from './modal/puntos/ModalDetalleCardex.jsx';
import {Validar} from '../../../src/services/ValidaSesion.jsx'
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"

function Puntos() {
    const [resultados, setResultados] = useState([]);
    const [showInsertarModal, setShowInsertarModal] = useState(false);
    const [showMasivoModal, setShowMasivoModal] = useState(false);
    const [showDetalleModal, setShowDetalleModal] = useState(false);
    const [showDetalleCardex, setShowDetalleCardex] = useState(false);
    const [modalData, setModalData] = useState(null);
    const [campañasData, setCampañasData] = useState(null);
    const [estado, setEstado] = useState('');
    const [dniUsuario, setDniUsuario] = useState(null);
    const [isFetch, setIsFetch] = useState(true);
    const [tipoOpe, setTipoOpe] = useState('Find')
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);

    const Seguridad = async () => {
        const ValSeguridad = await Validar("GDHLPU")
        console.log("ValSeguridad:",ValSeguridad)
        setVentanaSeguridad(ValSeguridad)
    }


    useEffect(() => {
        const token = sessionStorage.getItem('CDTToken');
        if (token) {
            Seguridad()
            listarConsolidado();
            obtenerCampanias();
        }
    }, []);

    const obtenerCampanias = async () => {
        //     //const idempresa = localStorage.getItem("idempresa");
        const responseJson = await ObtenerCampanias();
        setCampañasData(responseJson);
    }

    const listarConsolidado = async () => {
        //const idempresa = localStorage.getItem("idempresa");
        //const responseJson = await ListarPuntosConsolidado(idempresa);
        const responseJson = await ListarPuntosConsolidado();
        console.log("muestrame consolidado:",responseJson)
        setResultados(responseJson);
        setIsFetch(false);
    }

    const handleBusqueda = async (txtFind) => {
        //const idempresa = localStorage.getItem("idempresa");
        const texto = { "datos": txtFind };
        const responseJson = await BuscarUsuario(texto);
        setResultados(responseJson);
        setIsFetch(false);
    }

    const handleSubmit = async (e) => {
        if (tipoOpe === 'Masivo') {
            try {
                const responseJson = await InsertarPuntosMasivo(e);
                console.log("muestrame status:",responseJson)
                setEstado(responseJson.status);
                await listarConsolidado();
                return responseJson
            } catch (error) {
                console.error('Error al insertar puntos:', error);
            }
        }

        if (tipoOpe === 'Insertar') {
            try {
                const idempresa = localStorage.getItem("idempresa");
                const newPuntos = [{
                    ...e,
                    idEmpresa: idempresa,
                }];
                const responseJson = await InsertarPuntos(newPuntos);
                if (responseJson.status === 200) {
                    handleCloseModalInsertar();
                }
                await listarConsolidado();
            } catch (error) {
                console.error('Error al insertar puntos:', error);
            }
        }
    }

    const handleOpenModalInsertar = (registro, tipoOpe) => {
        setShowInsertarModal(true);
        setModalData(registro);
        setTipoOpe(tipoOpe)
    }

    const handleCloseModalInsertar = () => {
        setShowInsertarModal(false);
    }

    const handleOpenModalMasivo = (registro, tipoOpe) => {
        setShowMasivoModal(true);
        setModalData(registro);
        setTipoOpe(tipoOpe)
    }

    const handleCloseModalMasivo = () => {
        setShowMasivoModal(false);
    }

    const handleOpenModalDetalle = async (dni) => {
        setShowDetalleModal(true);
        setDniUsuario(dni);
    }

    const handleCloseModalDetalle = () => {
        setShowDetalleModal(false);
    }

    const handleOpenModalCardex = async (dni) => {
        setShowDetalleCardex(true);
        setDniUsuario(dni);
    }

    const handleCloseModalCardex = () => {
        setShowDetalleCardex(false);
    }

    return (
        <React.Fragment>
            <Busqueda handleBusqueda={handleBusqueda} 
            handleModal={() => handleOpenModalInsertar(showInsertarModal, 'Insertar')} 
            handelModalMasivo={() => handleOpenModalMasivo(showMasivoModal, 'Masivo')} 
            />

            {isFetch && 'Cargando'}
            {!isFetch && !resultados.length && 'Sin Informacion'}

            <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                <table className="table table-hover table-sm table-bordered">
                    <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                        <tr>
                            <th className="align-middle">Detalle consolidado</th>
                            <th className="align-middle">Empresa</th>
                            <th className="aling-middle">Centro de costo</th>
                            <th className="align-middle">Dni</th>
                            <th className="align-middle">Usuario</th>
                            <th className="align-middle">Puntos actuales</th>
                            <th className="align-middle">Detalle cardex</th>
                        </tr>
                    </thead>

                    {resultados.length > 0 &&
                        resultados.map((registro) =>
                            <ResultadoTablaPuntos
                                key={registro.id}
                                eventoDetalle={() => handleOpenModalDetalle(registro.dni)}
                                carcon_empresa={registro.empresa}
                                carcon_centro_costo={registro.area}
                                carcon_dni={registro.dni}
                                carcon_usuario={registro.nombre}
                                carcon_puntosactuales={registro.puntos}
                                eventoCardex={() => handleOpenModalCardex(registro.dni)}
                            />
                        )
                    }
                </table>
            </div>
            <VentanaBloqueo show={VentanaSeguridad} />
            <ModalInsertarPuntos
                showModal={showInsertarModal}
                modalData={modalData}
                onClose={handleCloseModalInsertar}
                handleSubmit={handleSubmit}
                campañasData={campañasData}
            />

            <ModalDetallePuntos
                showModal={showDetalleModal}
                dniUsuario={dniUsuario}
                onClose={handleCloseModalDetalle}
            />

            <ModalDetalleCardex
                showModal={showDetalleCardex}
                dniUsuario={dniUsuario}
                onClose={handleCloseModalCardex}
            />

            <UploadButton
                showMasivoModal={showMasivoModal}
                modalData={modalData}
                onClose={handleCloseModalMasivo}
                handleSubmit={handleSubmit}
            />

        </React.Fragment>
    );
}

export default Puntos;
