import React, { useState, useEffect } from 'react';
import { Mostrarhtml, ActualizarHtml } from './services/TerminosService';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import '../puntos/assets/css/terminos.css'
import {Validar} from '../../../src/services/ValidaSesion.jsx'
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"


const Terminos = () => {
    const [htmlStructures, setHtmlStructures] = useState([]);
    const [editingStructureId, setEditingStructureId] = useState(null);
    const [editedHtml, setEditedHtml] = useState('');
    const [VentanaSeguridad, setVentanaSeguridad] = useState(false);

    const Seguridad = async () => {
        const ValSeguridad = await Validar("GDHLPU")
        console.log("ValSeguridad:",ValSeguridad)
        setVentanaSeguridad(ValSeguridad)
    }


    useEffect(() => {
        Seguridad()
        mostrarestructuraHtml();
    }, []);

    const mostrarestructuraHtml = async () => {
        const responseJson = await Mostrarhtml();
        setHtmlStructures(responseJson.map((structure, index) => ({
            id: index + 1,
            html: structure,
            isEditing: false
        })));
    };

    const handleEdit = (structureId) => {
        const structure = htmlStructures.find(s => s.id === structureId);
        setEditingStructureId(structureId);
        setEditedHtml(structure.html);
        setHtmlStructures(prevState =>
            prevState.map(s => s.id === structureId ? { ...s, isEditing: true } : s)
        );
    };

    const handleSave = async (structureId) => {
        try {
            await ActualizarHtml(structureId, { id: structureId, structure_html: editedHtml });
            setHtmlStructures(prevState =>
                prevState.map(s => s.id === structureId ? { ...s, html: editedHtml, isEditing: false } : s)
            );
            setEditingStructureId(null); // Salir del modo de edición después de guardar
        } catch (error) {
            console.error('Error al actualizar HTML structure:', error);
        }
    };

    return (
        <>
            {htmlStructures.map((structure) => (
                
                <div className="container-term" key={structure.id}>
                    
                    {structure.isEditing ? (
                        <div>
                            <CKEditor
                                editor={ClassicEditor}
                                data={editedHtml}
                                onChange={(event, editor) => {
                                    const data = editor.getData();
                                    setEditedHtml(data);
                                }}
                                config={{
                                    toolbar: ['heading', '|',
                                     'bold', 'italic', 'link', '|', 
                                     'undo', 'redo', '|', 'fontSize', '|', 
                                     'alignment', '|',
                                     'numberedList', 'bulletedList',
                                    ],
                                    fontSize: {
                                        options: [10, 12, 14, 16, 'default', 18, 20, 22, 24, 26, 28, 30],
                                    },
                                    alignment: {
                                        options: ['left', 'center', 'right', 'justify'],
                                    },
                                }}
                            />
                            <br />
                            <button className="btn btn-warning btn-sm" onClick={() => handleSave(structure.id)}>Guardar</button>
                        </div>
                        
                    ) : (
                        <div>
                            <div className="container-term" dangerouslySetInnerHTML={{ __html: structure.html }}></div>
                            <button className="btn btn-warning btn-sm" onClick={() => handleEdit(structure.id)}>Editar</button>
                        </div>
                    )}
                    <VentanaBloqueo show={VentanaSeguridad} />
                </div>
                
            ))}
        </>
        
    );
};

export default Terminos;
