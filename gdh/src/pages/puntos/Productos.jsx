import React from 'react'
import Busqueda from "./componentes/BusquedaProducto.jsx";
import {ResultadoTablaProducto} from './componentes/ResultadoTabla.jsx';
import ModalEditar from './modal/producto/ModalEditar.jsx';
//import ActualizarProducto from '../../servicios/ProductoService.jsx';
import { ActualizarProducto, AgregarProducto, DescontarProducto, InsertarProducto, ListarProducto, BuscarProducto, EliminarCampania } from './services/ProductoService.jsx';

import ModalAgregarCantidad from './modal/producto/ModalAgregar.jsx';
import ModalDescontarCantidad from './modal/producto/ModalDescontar.jsx';
import ModalInsertarProducto from './modal/producto/ModalInsertar.jsx';
import ModalEliminar from './modal/producto/ModalEliminar.jsx';

import {Validar} from '../../../src/services/ValidaSesion.jsx'
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"

class Productos extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            //showModal: false,
            showActualizarModal: false, // Nueva variable de estado para el modal de actualización
            showAgregarModal: false, // Nueva variable de estado para el modal de agregar
            showDescontarModal: false, // Nueva variable de estado para el modal de descontar
            showInsertarModal: false,
            showEliminarModal: false,
            tipoOpe: 'Find',
            modalData: null,
            _isMounted: false,
            VentanaSeguridad:false
        }

        const token = sessionStorage.getItem('CDTToken')
        if (token) {
            this.Seguridad()
            this.Listar()
        }
    }

    Seguridad = async () => {
        const ValSeguridad = await Validar("GDHLPU")
        console.log("ValSeguridad:",ValSeguridad)
        this.setState({VentanaSeguridad:ValSeguridad})
    }


    componentDidMount() {
        this.setState({ _isMounted: true })
    }

    componentWillUnmount() {
        this.setState({ _isMounted: false })
    }


    Listar = async () => {
        const responseJson = await ListarProducto()
        //this.setState({ resultados: responseJson, loading: false })
        if (this.state._isMounted) {
            this.setState({ resultados: responseJson, loading: false })
        }
    }



    handleBusqueda = async (txtFind) => {
        console.log("entras a realizar busqueda", txtFind)

        const texto = {
            "nombre": txtFind
        }
        const responseJson = await BuscarProducto(texto)
        this.setState({ resultados: responseJson, loading: false })
    }

    handleSubmit = async (e) => {
        console.log("que traes:", e)
        if (this.state.tipoOpe === 'Insertar') {
            try {
                const responseJson = await InsertarProducto(e);
                console.log(responseJson);
                await this.Listar(); //Refresca        
            } catch (error) {
                console.error('Error al insertar el producto', error);
            }
        }

        if (this.state.tipoOpe === 'Actualizar') {
            try {
                console.log("entras a actualizar xd", e)
                const responseJson = await ActualizarProducto(e);
                console.log(responseJson);
                await this.Listar(); //Refresca
            } catch (error) {
                console.error('Error al actualizar el producto:', error);
            }
        }

        if (this.state.tipoOpe === 'Agregar') {
            try {

                const nuevoProducto = {
                    // Copia todas las propiedades del producto original
                    id_producto: e.id, // Asigna el valor de "id" a "id_producto"
                    cantidad_agregar: e.cantidad, // Elimina la clave "id" del nuevo objeto si es necesario
                    tipo: 'ENTRADA'
                };
                console.log("pintame producto agregar:", nuevoProducto)
                const response = await AgregarProducto(nuevoProducto);
                await this.Listar(); //Refresca
                console.log(response);
            } catch (error) {
                console.error('Error al agregar el producto:', error);
            }
        }

        if (this.state.tipoOpe === 'Descontar') {
            try {
                console.log("entras a descontar", this.state.tipoOpe)
                const nuevoProducto = {
                    // Copia todas las propiedades del producto original
                    id_producto: e.id, // Asigna el valor de "id" a "id_producto"
                    cantidad_descontar: e.cantidad, // Elimina la clave "id" del nuevo objeto si es necesario
                    tipo: 'SALIDA'
                };
                console.log("pintame producto descontar:", nuevoProducto)
                const response = await DescontarProducto(nuevoProducto);
                await this.Listar(); //Refresca
                console.log(response);
            } catch (error) {
                console.error('Error al agregar el producto:', error);
            }
        }


        if (this.state.tipoOpe === 'Eliminar') {
            console.log("entras a eliminar", this.state.tipoOpe)
            try {
                const responseJson = await EliminarCampania(e);
                console.log(responseJson)
                this.handleCloseModalDelete()
                this.Listar();
            } catch (error) {
                console.error('Error al eliminar el campania:', error);
            }
        }
    }


    handleOpenModal = (registro, tipoOpe) => {
        this.setState({ showActualizarModal: true, modalData: registro, tipoOpe });
    }


    handleCloseModal = () => {
        this.setState({ showActualizarModal: false });
    }

    handleOpenModalAdd = (registro, tipoOpe) => {
        this.setState({ showAgregarModal: true, modalData: registro, tipoOpe });
    }


    handleCloseModalAdd = () => {
        this.setState({ showAgregarModal: false });
    }

    handleOpenModalDesc = (registro, tipoOpe) => {
        this.setState({ showDescontarModal: true, modalData: registro, tipoOpe });
    }

    handleCloseModalDesc = () => {
        this.setState({ showDescontarModal: false });
    }

    handleOpenModalInsertar = (registro, tipoOpe) => {
        this.setState({ showInsertarModal: true, modalData: registro, tipoOpe });
    }

    handleCloseModalInsertar = () => {
        this.setState({ showInsertarModal: false });
    }

    handleOpenModalDelete = (registro, tipoOpe) => {
        this.setState({ showEliminarModal: true, modalData: registro, tipoOpe });
    }

    handleCloseModalDelete = () => {
        this.setState({ showEliminarModal: false });
    }



    handleChange = (e, fieldName) => {
        console.log("cuanto actualizas entras aqui:", e.target)
        const { modalData } = this.state;
        const updatedModalData = { ...modalData, [fieldName]: e.target.value };
        this.setState({ modalData: updatedModalData }, () => {
            console.log("data change:", this.state.modalData);
        });
    }


    render() {
        const { isFetch, resultados, showActualizarModal, showAgregarModal, showDescontarModal, showEliminarModal, modalData, showInsertarModal, VentanaSeguridad } = this.state



        return (
            <React.Fragment>
                <Busqueda handleBusqueda={
                    this.handleBusqueda
                }
                    handleModal={
                        () => this.handleOpenModalInsertar(showInsertarModal, 'Insertar')
                    }


                />


                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                            <tr>
                                <th className="align-middle">Nombre</th>
                                <th className="align-middle">Precio</th>
                                <th className="align-middle">Puntos</th>
                                <th className="align-middle">Fecha</th>
                                <th className="align-middle">Cantidad</th>
                                <th className="align-middle">Estado</th>
                                <th className="align-middle">Editar</th>
                                <th className="align-middle">Agregar</th>
                                <th className="align-middle">Descontar</th>
                                <th className="align-middle">Eliminar</th>
                            </tr>
                        </thead>

                        {resultados.length > 0 &&
                            resultados.map((registro) =>
                                <ResultadoTablaProducto
                                    key={registro.id}
                                    carcon_codigo={registro.carcon_codigo}
                                    carcon_nombre={registro.nombre}
                                    carcon_precio={registro.precio}
                                    carcon_puntos={registro.cantidadpuntos}
                                    carcon_fecha={registro.fecharegistro}
                                    carcon_cantidad={registro.cantidad}
                                    carcon_estado={registro.estado == 0 ? 'INACTIVO' : 'ACTIVO'}

                                    eventoEditar={() => this.handleOpenModal(registro, 'Actualizar')}
                                    eventoAgregar={() => this.handleOpenModalAdd(registro, 'Agregar')}
                                    eventoDescontar={() => this.handleOpenModalDesc(registro, 'Descontar')}
                                    eventoEliminar={() => this.handleOpenModalDelete(registro, 'Eliminar')}


                                />
                            )}
                    </table>
                </div>
                <VentanaBloqueo show={VentanaSeguridad} />
                <ModalInsertarProducto
                    showModal={showInsertarModal}
                    modalData={modalData}
                    onClose={this.handleCloseModalInsertar}
                    handleSubmit={this.handleSubmit}
                    handleChange={this.handleChange}
                />
                <ModalEditar
                    showModal={showActualizarModal}
                    modalData={modalData}
                    onClose={this.handleCloseModal}
                    handleSubmit={this.handleSubmit}
                    handleChange={this.handleChange}
                />
                <ModalAgregarCantidad
                    showModal={showAgregarModal}
                    modalData={modalData}
                    onClose={this.handleCloseModalAdd}
                    handleSubmit={this.handleSubmit}
                    handleChange={this.handleChange}
                />
                <ModalDescontarCantidad
                    showModal={showDescontarModal}
                    modalData={modalData}
                    onClose={this.handleCloseModalDesc}
                    handleSubmit={this.handleSubmit}
                    handleChange={this.handleChange}
                />
                <ModalEliminar
                    showModal={showEliminarModal}
                    modalData={modalData}
                    onClose={this.handleCloseModalDelete}
                    handleSubmit={this.handleSubmit}
                    handleChange={this.handleChange}
                />
            </React.Fragment>
        )

    }
 
}


export default Productos;