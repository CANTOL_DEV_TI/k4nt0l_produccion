
import axios from 'axios';
import {Url_Puntos} from '../../../../../gdh/src/constants/global'
const BASE_URL = Url_Puntos




const API_URL_LISTAR = BASE_URL + '/puntos/puntos/listarUsuarios/';
const API_URL_INSERTAR = BASE_URL + '/puntos/puntos/agregarPuntosUsuario';
const API_URL_MASIVO = BASE_URL + '/puntos/puntos/agregarPuntosMasivo';
const API_URL_LISTAR_USUARIO = BASE_URL + '/puntos/puntos/buscar/';
const API_URL_LISTAR_USERS = BASE_URL + '/puntos/puntos/listarUsuariosDni/';
const API_URL_LISTAR_CONSOLIDADO = BASE_URL + '/puntos/puntos/listarUsuarioConsolidado/';
const API_URL_LISTAR_CARDEX = BASE_URL + '/puntos/puntos/listarUsuariosCardex/'
const API_URL_ACTUALIZAR_CARDEX = BASE_URL + '/puntos/puntos/actualizarcardex'



const ListarUsuariosCardex = async (dniUsuario) => {
    try{
        const response = await axios.post(`${API_URL_LISTAR_CARDEX}?dniUsuario=${dniUsuario}`);
        return response.data;

    }catch(error){
        throw new Error('Error al listar cardex', error); // Maneja el error correctamente
    }
}

const BuscarUsuario = async(e) => {
    try {
        const response = await axios.post(API_URL_LISTAR_USUARIO,e)
        console.log('pintame response:',response)
        return response.data;
    } catch (error) {
        throw new Error('Error al descontar el producto');
    }
}


/*const ListarProducto = async () => {
    try {
        console.log("Entras a listar productos:");
        const response = await axios.get(API_URL_LISTAR);
        console.log("pintame response:",response)
        return response.data; // Devuelve los datos de la lista de puntos
    } catch (error) {
        throw new Error('Error al listar productos', error); // Maneja el error correctamente
    }
};*/



const ListarPuntosConsolidado = async () => {
    try {
  
        //const response = await axios.post(`${API_URL_LISTAR_CONSOLIDADO}?idEmpresa=${idEmpresa}`);
        const response = await axios.get(API_URL_LISTAR_CONSOLIDADO);

        console.log("pintame response:",response)

        return response.data; // Devuelve los datos de la lista de puntos
    } catch (error) {
        throw new Error('Error al listar puntos consolidado', error); // Maneja el error correctamente
    }
};



const ListarPuntos = async (idEmpresa) => {
    try {
        console.log("Entras a listar puntos:", idEmpresa);

        //const response = await axios.post(API_URL_LISTAR, idEmpresa);
        const response = await axios.post(`${API_URL_LISTAR}?idEmpresa=${idEmpresa}`);
        console.log("pintame response:",response)

        return response.data; // Devuelve los datos de la lista de puntos
    } catch (error) {
        throw new Error('Error al listar puntos', error); // Maneja el error correctamente
    }
};

const InsertarPuntosMasivo = async(e) => {
    
    try {
        const response = await axios.post(API_URL_MASIVO, e, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        console.log("PuntoService:",response.data);
        return response.data
        //onClose(); // Cerrar el modal después de cargar el archivo
    } catch (error) {
        throw new error('Error al cargar el archivo:', error);
    }
}


const InsertarPuntos = async(e) => {
    try{
        console.log("Entras insertar producto:", e);
        const response = await axios.post(API_URL_INSERTAR,e);
        return response.data;

    }catch(error){
        throw new error('Error al actualizar producto');  
    }
}


const ListarPuntosUsuario = async (dniUsuario) => {
    try {
        console.log("Entras a listar puntos:", dniUsuario);

        //const response = await axios.post(API_URL_LISTAR, idEmpresa);
        const response = await axios.post(`${API_URL_LISTAR_USERS}?dniUsuario=${dniUsuario}`);
        console.log("pintame response:",response)

        return response.data; // Devuelve los datos de la lista de puntos
    } catch (error) {
        throw new Error('Error al listar puntos', error); // Maneja el error correctamente
    }
};

const ActualizarCardex = async (e) => {
    console.log("entras a actualizar:",e)
    try {
        const response = await axios.post(API_URL_ACTUALIZAR_CARDEX, e);
        return response.data;

    } catch (error) {
        console.log("Error al cambiar el estado",error)
    }
}

export { ListarPuntos, InsertarPuntos, InsertarPuntosMasivo, BuscarUsuario, ListarPuntosUsuario, ListarPuntosConsolidado, ListarUsuariosCardex, ActualizarCardex };



