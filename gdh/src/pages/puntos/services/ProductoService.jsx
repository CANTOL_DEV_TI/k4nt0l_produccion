import axios from 'axios';
import {Url_Puntos} from '../../../../../gdh/src/constants/global'
const BASE_URL = Url_Puntos


const API_URL_INSERTAR = BASE_URL + '/puntos/producto/registrarProducto';
const API_URL_ACTUALIZAR = BASE_URL + '/puntos/producto/actualizarproducto';
const API_URL_AGREGAR_PRODUCTO = BASE_URL + '/puntos/producto/agregarStock';
const API_URL_DESCONTAR_PRODUCTO = BASE_URL + '/puntos/producto/descontarStock';
const API_URL_LISTAR = BASE_URL + '/puntos/producto/listarProducto';
const API_BUSCAR_PRODUCTO = BASE_URL + '/puntos/producto/buscar';
const API_ELIMINAR_PRODUCTO = BASE_URL + '/puntos/producto/cambiarestado';


const EliminarCampania = async (e) => {
    try{
        console.log("Entras eliminar campania:", e);
        const response = await axios.post(API_ELIMINAR_PRODUCTO,e)
        return response.data;
    }catch(error){
        throw new Error('Error al eliminar campania')
    }

}

const BuscarProducto = async(e) => {
    try {
        const response = await axios.post(API_BUSCAR_PRODUCTO,e)
        console.log('pintame response:',response)
        return response.data;
    } catch (error) {
        throw new Error('Error al buscar el producto');
    }
}

const ListarProducto = async () => {
    try {
        console.log("Entras a listar productos:");
        const response = await axios.get(API_URL_LISTAR);
        console.log("pintame response:",response)
        return response.data; // Devuelve los datos de la lista de puntos
    } catch (error) {
        throw new Error('Error al listar productos', error); // Maneja el error correctamente
    }
};

const InsertarProducto = async (e) => {
    try {
        console.log("Entras insertar producto:", e);
        const response = await axios.post(API_URL_INSERTAR,e);
        return response.data; // Devuelve los datos del producto actualizado
    } catch (error) {
        throw new Error('Error al actualizar el producto'); // Maneja el error correctamente
    }
};



const ActualizarProducto = async (e) => {
    try {
        console.log("Entras actualizar producto:", e);
        const response = await axios.post(API_URL_ACTUALIZAR, e);
        return response.data; // Devuelve los datos del producto actualizado
    } catch (error) {
        throw new Error('Error al actualizar el producto'); // Maneja el error correctamente
    }
};


const AgregarProducto = async (producto) => {
    try {
        const response = await axios.post(API_URL_AGREGAR_PRODUCTO, producto);
        return response.data;
    } catch (error) {
        throw new Error('Error al agregar el producto');
    }
};


const DescontarProducto = async (producto) => {
    try {
        const response = await axios.post(API_URL_DESCONTAR_PRODUCTO, producto);
        return response.data;
    } catch (error) {
        throw new Error('Error al descontar el producto');
    }
};



export default ActualizarProducto; // Exporta directamente la función ActualizarProducto
export {InsertarProducto, ActualizarProducto, AgregarProducto, DescontarProducto, ListarProducto,BuscarProducto, EliminarCampania };


