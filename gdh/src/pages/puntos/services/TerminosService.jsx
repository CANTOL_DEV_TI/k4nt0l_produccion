import axios from 'axios'
import {Url_Puntos} from '../../../../../gdh/src/constants/global'
const BASE_URL = Url_Puntos


const API_URL_LISTAR = BASE_URL + '/puntos/terminos/obtenerHtml'
const API_URL_ACTUALIZAR = BASE_URL + '/puntos/terminos/actualizarHtml/1'

const Mostrarhtml = async () => {
    try{
        const response = await axios.get(API_URL_LISTAR)
        console.log("pintame response:", response)
        return response.data
    }catch(error){
        throw new Error('Error al listar puntos consolidado', error); // Maneja el error correctamente
    }
}

const ActualizarHtml = async (structureId, htmlStructure) => {
    try {
        console.log("pintame structureId:",structureId)
        console.log("pintame htmlStructure:", htmlStructure)
        await axios.post(`${API_URL_ACTUALIZAR}`,htmlStructure)
        //await axios.post(`http://localhost:8000/terminos/terminos/actualizarHtml/1`, htmlStructure);
    } catch (error) { // Asegúrate de definir el parámetro error
        console.error('Error al actualizar HTML:', error);
        throw error; // Lanza el error nuevamente para que pueda ser manejado donde se llama esta función
    }
};



export {Mostrarhtml, ActualizarHtml}