import axios from 'axios';
import {Url_Puntos} from '../../../../../gdh/src/constants/global'
const BASE_URL = Url_Puntos


const API_URL_LISTAR = BASE_URL +'/puntos/campania/listarCampania';
const API_URL_INSERTAR = BASE_URL +'/puntos/campania/insertar';
const API_BUSCAR_CAMPANIA = BASE_URL + '/puntos/campania/buscar';
const API_ELIMINAR_CAMPANIA = BASE_URL + '/puntos/campania/cambiarestado';
const API_URL_ACTUALIZAR = BASE_URL + '/puntos/campania/actualizarcampania';
const API_URL_OBTENER_CODIGO = BASE_URL + '/puntos/campania/obtenercodigo';
const API_URL_OBTENER_CAMPANIA = BASE_URL + '/puntos/campania/obtenercampanias';


const EliminarCampania = async (e) => {
    try{
        console.log("Entras eliminar campania:", e);
        const response = await axios.post(API_ELIMINAR_CAMPANIA,e)
        return response.data;
    }catch(error){
        throw new Error('Error al eliminar campania')
    }

}

const ActualizarCampania = async (e) => {
    try {
        console.log("Entras actualizar campania:", e);
        const response = await axios.post(API_URL_ACTUALIZAR, e);
        return response.data; // Devuelve los datos del producto actualizado
    } catch (error) {
        throw new Error('Error al actualizar campania'); // Maneja el error correctamente
    }
};

const BuscarCampania = async(e) => {
    try {
        const response = await axios.post(API_BUSCAR_CAMPANIA,e)
        console.log('pintame response:',response)
        return response.data;
    } catch (error) {
        throw new Error('Error al buscar el campania');
    }
}


const ListarCampania = async () => {
    try {
        console.log("Entras a listar campaña:");
        const response = await axios.get(API_URL_LISTAR);
        return response.data; // Devuelve los datos del producto actualizado
    } catch (error) {
        throw new Error('Error al listar campaña', error); // Maneja el error correctamente
    }
};




const InsertarCampania = async (e) => {
    try {
        console.log("Entras insertar campania:", e);
        const response = await axios.post(API_URL_INSERTAR, e);
        return response.data; // Devuelve los datos del producto actualizado
    } catch (error) {
        throw new Error('Error al insertar campania'); // Maneja el error correctamente
    }
};


const ObtenerCodigo = async () => {
    try {
        console.log("Entras a obtener codigo de campaña:");
        const response = await axios.get(API_URL_OBTENER_CODIGO);
        return response.data; // Devuelve los datos del producto actualizado
    } catch (error) {
        throw new Error('Error al listar codigo', error); // Maneja el error correctamente
    }
};


const ObtenerCampanias = async () => {
    try {
        const response = await axios.get(API_URL_OBTENER_CAMPANIA);
        return response.data; // Devuelve los datos del producto actualizado
    } catch (error) {
        throw new Error('Error al listar camapañas', error); // Maneja el error correctamente
    }
};




export  {ListarCampania, ActualizarCampania, InsertarCampania,BuscarCampania, EliminarCampania, ObtenerCodigo,ObtenerCampanias };