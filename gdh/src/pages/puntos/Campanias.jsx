import React from 'react'
import Busqueda from "./componentes/BusquedaCampanias.jsx";
import {ResultadoTablaCampania} from './componentes/ResultadoTabla.jsx';
import { ListarCampania, ActualizarCampania, InsertarCampania, BuscarCampania, EliminarCampania, ObtenerCodigo } from './services/CampaniaService.jsx';
import ModalEditar from './modal/campania/ModalEditar.jsx';
import ModalInsertarCampania from './modal/campania/ModalInsertarCampania.jsx';
import ModalEliminar from './modal/campania/ModalEliminar.jsx';
import {Validar} from '../../../src/services/ValidaSesion.jsx'
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"

/*const Productos = () => {
  return (
    <div>Productos</div>
  )
}*/

class Campanias extends React.Component {
    constructor(props) {
        super(props);



        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            //showModal: false,
            showInsertarModal: false,
            showActualizarModal: false,
            showEliminarModal: false,
            tipoOpe: 'Find',
            modalData: null,
            codigoData: null,
            _isMounted: false //Agregar la propiedad _isMounted

        }

        const token = sessionStorage.getItem('CDTToken')
        if (token) {
            this.Seguridad()
            this.Listar()
            this.ObtenerCodigo()
        }
    }

    Seguridad = async () => {
        const ValSeguridad = await Validar("GDHLPU")
        console.log("ValSeguridad:",ValSeguridad)
        this.setState({VentanaSeguridad:ValSeguridad})
    }

    componentDidMount() {
        this.setState({ _isMounted: true })
    }

    componentWillUnmount() {
        this.setState({ _isMounted: false });
    }

    Listar = async () => {
        console.log("entras a mostrar la lista")
        const responseJson = await ListarCampania()
        if (this.state._isMounted) {
            this.setState({ resultados: responseJson, loading: false });
        }
    }

    ObtenerCodigo = async () => {
        const responseJson = await ObtenerCodigo()
        if (this.state._isMounted) {
            this.setState({ codigoData: responseJson['codigoCampaña'] })

        }
    }





    handleBusqueda = async (txtFind) => {
        const texto = {
            "datos": txtFind
        }
        const responseJson = await BuscarCampania(texto)
        console.log("pintame response:", responseJson)
        this.setState({ resultados: responseJson, loading: false })

    }

    handleSubmit = async (e) => {

        console.log('que datos traes', e)
        console.log('que estado muestras', this.state.tipoOpe)

        if (this.state.tipoOpe === 'Actualizar') {
            try {
                const nuevoCampania = {
                    ...e,// Copia todas las propiedades del producto original
                    codigo: e.codigoCampania, // Asigna el valor de "id" a "id_producto"
                    //empresasSeleccionadas: e.empresas
                };

                const responseJson = await ActualizarCampania(nuevoCampania);
                this.handleCloseModalEditar()
                console.log(responseJson);
                this.Listar(); //Refresca
            } catch (error) {
                console.error('Error al actualizar la campania:', error);
            }
        }

        if (this.state.tipoOpe === 'Eliminar') {
            try {
                const responseJson = await EliminarCampania(e);
                console.log(responseJson)
                this.handleCloseModalEliminar()
                this.Listar();
            } catch (error) {
                console.error('Error al eliminar el campania:', error);
            }
        }

        if (this.state.tipoOpe === 'Insertar') {
            try {
                const responseJson = await InsertarCampania(e);
                console.log(responseJson);
                this.handleCloseModalInsertar()
                this.Listar(); //Refresca
            } catch (error) {
                console.error('Error al insertar la campania:', error);
            }
        }

    }

    handleOpenModalInsertar = (registro, tipoOpe) => {
        this.setState({ showInsertarModal: true, modalData: registro, tipoOpe });
        this.ObtenerCodigo()
    }

    handleCloseModalInsertar = () => {
        this.setState({ showInsertarModal: false });
    }




    handleOpenModalEditar = (registro, tipoOpe) => {
        this.setState({ showActualizarModal: true, modalData: registro, tipoOpe });
    }

    handleCloseModalEditar = () => {
        this.setState({ showActualizarModal: false });
    }


    handleOpenModalEliminar = (registro, tipoOpe) => {
        console.log("estado:", registro)
        this.setState({ showEliminarModal: true, modalData: registro, tipoOpe: "Eliminar" })
    }

    handleCloseModalEliminar = () => {
        this.setState({ showEliminarModal: false });
    }



    handleChange = (e, fieldName) => {
        const { modalData } = this.state;
        const updatedModalData = { ...modalData, [fieldName]: e.target.value };
        this.setState({ modalData: updatedModalData }, () => {
            console.log("data change:", this.state.modalData);
        });
    }


    render() {
        const { isFetch, resultados, showInsertarModal, showActualizarModal, showEliminarModal, modalData, codigoData, VentanaSeguridad } = this.state

        return (
            <React.Fragment>
                <Busqueda handleBusqueda={this.handleBusqueda}
                    handleModal={
                        () => this.handleOpenModalInsertar(showInsertarModal, 'Insertar')
                    } />


                {isFetch && 'Cargando'}
                {(!isFetch && !resultados.length) && 'Sin Informacion'}


                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                            <tr>
                                <th className="align-middle">Codigo</th>
                                <th className="align-middle">Tipo</th>
                                <th className="align-middle">Categoria</th>
                                <th className="align-middle">Nombre</th>
                                <th className="align-middle">Puntos</th>
                                <th className="align-middle">Estado</th>
                                <th className="align-middle">Editar</th>
                                <th className="align-middle">Eliminar</th>
                            </tr>
                        </thead>

                        {resultados.length > 0 &&
                            resultados.map((registro) =>
                                <ResultadoTablaCampania
                                    key={registro.codigoCampania}
                                    carcon_codigo={registro.codigoCampania}
                                    carcon_tipo={registro.tipo}
                                    carcon_categoria={registro.categoria}
                                    carcon_nombre={registro.nombre}
                                    carcon_puntos={registro.puntos}
                                    carcon_estado={registro.estado == 0 ? 'INACTIVO' : 'ACTIVO'}
                                    eventoEditar={() => this.handleOpenModalEditar(registro, 'Actualizar')}
                                    eventoEliminar={() => this.handleOpenModalEliminar(registro, 'Eliminar')}
                                />
                            )}
                    </table>
                </div>
                <VentanaBloqueo show={VentanaSeguridad} />
                <ModalEditar
                    showActualizarModal={showActualizarModal}
                    modalData={modalData}
                    onClose={this.handleCloseModalEditar}
                    handleSubmit={this.handleSubmit}
                    handleChange={this.handleChange}
                />
                <ModalInsertarCampania
                    showInsertarModal={showInsertarModal}
                    modalData={modalData}
                    onClose={this.handleCloseModalInsertar}
                    handleSubmit={this.handleSubmit}
                    handleChange={this.handleChange}
                    codigo={codigoData}
                />
                <ModalEliminar
                    showEliminarModal={showEliminarModal}
                    modalData={modalData}
                    onClose={this.handleCloseModalEliminar}
                    handleSubmit={this.handleSubmit}
                    handleChange={this.handleChange}

                />


            </React.Fragment>
        )

    }

}


export default Campanias;