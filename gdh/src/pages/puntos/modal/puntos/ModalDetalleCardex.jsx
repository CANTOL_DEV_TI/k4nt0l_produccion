import React, { useState, useEffect } from 'react';
import { ListarUsuariosCardex } from '../../services/PuntosService';
import { GrDocumentStore } from 'react-icons/gr';
import ModalEntregar from './ModalEntregar';
import { ActualizarCardex } from '../../services/PuntosService';

export const ModalDetalleCardex = ({ showModal, onClose, dniUsuario }) => {
    const [data, setData] = useState([]);
    const [searchCardex, setSearchCardex] = useState('');
    const [loading, setLoading] = useState(true);
    const [selectedItem, setSelectedItem] = useState(null);


    /*useEffect(() => {
        const fetchData = async () => {
            try {
                const responseJson = await ListarUsuariosCardex(dniUsuario);
                setData(responseJson);
                setLoading(false);
            } catch (error) {
                console.error("Error al obtener los puntos del usuario:", error);
            }
        };
        fetchData();
    }, [dniUsuario]);*/

    useEffect(() => {
        listarUsuarioCardex();
    }, [dniUsuario]);


    const listarUsuarioCardex = async () => {
        try {
            const responseJson = await ListarUsuariosCardex(dniUsuario)
            setData(responseJson)
            setLoading(false)
        } catch (error) {
            console.error("Error al obtener los puntos del usuario:", error);
        }

    }


    const handleSearchChange = (e) => {
        setSearchCardex(e.target.value);
    };

    const filteredData = data.filter(item => {
        return item.producto.toLowerCase().includes(searchCardex.toLowerCase());
    });

    const handleOpenModalCardex = (item) => {
        console.log("pintame data:", item)
        setSelectedItem(item);
    };

    const handleCloseModalCardex = () => {
        console.log("entras aqui a cerrar:")
        setSelectedItem(null);
    };

    const handleSubmit = async (e) => {
        try {
            console.log("entras aqui:",e)
            const responseJson = await ActualizarCardex(e)
            console.log("actualiza cardex:",responseJson)
            await listarUsuarioCardex();
        } catch (error) {
            console.error('Error al actualizar', error);
        }
    }

    return (
        <div className="modal-detail" style={{ display: showModal ? 'block' : 'none' }}>
            <div className="modal-content-detail">
                <span className="close" onClick={onClose}>&times;</span>
                <div className="search-container" style={{ textAlign: 'right', marginRight: '20px', marginBottom: '5px' }}>
                    <input
                        type="text"
                        placeholder="Buscar producto..."
                        value={searchCardex}
                        onChange={handleSearchChange}
                    />
                </div>
                {loading ? (
                    <div>Cargando...</div>
                ) : (
                    <div className="table-container">
                        <table className="table table-bordered table-hover table-sm">
                            <thead className="table-secondary text-center table-sm">
                                <tr>
                                    <th>Producto</th>
                                    <th>Fecha</th>
                                    <th>Estado</th>
                                    <th>Cantidad</th>
                                    <th>Cantidad puntos por producto</th>
                                    <th>Cantidad puntos total producto</th>
                                    <th>Fecha de entrega</th>
                                    <th>Estado de entrega</th>
                                    <th>Cambiar estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                {filteredData.map((item, index) => (
                                    <tr key={index}>
                                        <td>{item.producto}</td>
                                        <td>{item.fecharegistro}</td>
                                        <td>{item.estado}</td>
                                        <td>{item.cantidad}</td>                                        
                                        <td>{item.puntosxproducto}</td>
                                        <td>{item.puntostotalproducto}</td>
                                        <td>{item.fechaentrega}</td>
                                        <td>{item.estadoentrega == 0 ? 'SIN ENTREGAR' : 'ENTREGADO'}</td>
                                        <td style={{ textAlign: "center" }}>
                                            <button  onClick={() => handleOpenModalCardex(item)} disabled = {item.estadoentrega==1}><GrDocumentStore /></button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                )}
            </div>
            <ModalEntregar
                showEntregarModal={selectedItem !== null}
                handleSubmit = {handleSubmit}
                modalData={selectedItem}
                onCloseEntregarModal={handleCloseModalCardex}
            />
        </div>
    );
}

export default ModalDetalleCardex;
