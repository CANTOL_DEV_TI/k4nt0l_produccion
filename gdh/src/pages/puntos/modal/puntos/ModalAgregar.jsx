import React, { useEffect, useState } from 'react';
//import puntos\src\assets\css\modal.css
import '../../assets/css/modal.css'

const ModalInsertarPuntos = ({ showModal, onClose, handleSubmit, campañasData }) => {
    const inititalFormData = {
        dniusuario: '',
        codigocampania: '',
        tipomovimiento: 'INGRESO',
        puntosgenerados: '',
        estado: 'VIGENTE'
    }

    const [formData, setFormData] = useState(inititalFormData)
    const [campaignOptions, setCampaignOptions] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [generatedPoints, setGeneratedPoints] = useState(0);
    const [currentPoints, setCurrentPoints] = useState(0)
    const [message, setMessage] = useState("")

    useEffect(() => {
        if (campañasData) {
            // Listar campañas
            setCampaignOptions(campañasData);
        }
    }, [campañasData])

    useEffect(() => {
        if (!showModal) {
            setFormData(inititalFormData);
            setGeneratedPoints(0);
            setCurrentPoints(0);
            setMessage("");
        }
    }, [showModal])

    useEffect(() => {
        if (showModal) {
            // Limpiar el mensaje cuando el modal se abre
            setMessage("");
        }
    }, [showModal]);



    const handleChange = (e, fieldName) => {
        const value = e.target.value;
        if (fieldName === 'codigocampania') {
            const selectedCampaign = campaignOptions.find(campaign => campaign.codigocampania === value);
            if (selectedCampaign) {
                setFormData({ ...formData, [fieldName]: value, puntosgenerados: selectedCampaign.puntos });
            }
        } else {
            setFormData({ ...formData, [fieldName]: value });
        }
    };


    const handleSave = async () => {
        if (!formData.dniusuario || !formData.codigocampania || !formData.puntosgenerados) {
            alert('Por favor complete todos los campos obligatorios.');
            return;
        }
        try {
            const response = await handleSubmit(formData);
            if (response && response.status === 200) {
                setFormData(inititalFormData);
                setGeneratedPoints(0);
                setCurrentPoints(0);
                setMessage("");
                onClose();
            } else {
                setMessage("¡Dni no encontrado!");
            }
        } catch (error) {
            console.error('Error al insertar puntos:', error);
            setMessage("Hubo un error al intentar insertar los puntos.");
        }
    };

    const handleClose = () => {
        setFormData(inititalFormData);
        setGeneratedPoints(0);
        setCurrentPoints(0);
        setMessage(null)
        onClose();
    };

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: showModal ? 'block' : 'none' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Agregar Puntos</h5>
                        <button type="button" className="btn-close" onClick={handleClose}></button>
                    </div>
                    <div className="modal-body">
                        <div className="form-group">
                            <label htmlFor="dniusuario">Dni:</label>
                            <input type="number" className="form-control" id="dniusuario" value={formData.dniusuario} onChange={(e) => handleChange(e, 'dniusuario')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="codigocampania">Campaña:</label>
                            <select
                                className="form-control"
                                id="codigocampania"
                                onChange={(e) => handleChange(e, 'codigocampania')}
                                value={formData.codigocampania}
                            >
                                <option value="">Selecciona una campaña</option>
                                {campaignOptions
                                    .filter(option => option.nombre.toLowerCase().includes(searchTerm.toLowerCase()))
                                    .map(option => (
                                        <option key={option.id} value={option.codigocampania}>{option.nombre}</option>
                                    ))
                                }
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="puntosgenerados">Puntos generados:</label>
                            <input type="number" className="form-control" id="puntosgenerados" value={formData.puntosgenerados} onChange={(e) => handleChange(e, 'puntosgenerados')} />
                        </div>
                        {/* Agrega más campos según tus datos */}
                    </div>
                    <div className="modal-footer">
                        <div className="message-container">
                            <p><span>{message}</span></p>
                        </div>
                        <button type="button" className="btn btn-warning btn-sm" onClick={handleSave} disabled={formData.dniusuario <= 0 || formData.dniusuario === ''} >Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ModalInsertarPuntos;
