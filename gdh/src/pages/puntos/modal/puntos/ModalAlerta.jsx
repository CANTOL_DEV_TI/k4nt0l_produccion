import React, {useState} from 'react';


const DnisFalsosModal = ({ isOpen, onRequestClose, dnisFalsos }) => {
    if (!isOpen ) return null;


    const CloseModal = () => {
      onRequestClose();
  }

  

  return (
    <div className="modal" tabIndex="-1" style={{ display: isOpen ? 'block' : 'none' }}>
      <div className="modal-dialog" role="document">
        <div className="modal-content" style={{padding:'41px', maxWidth:'415px'}}>
          <div className="modal-header">
            <h5 className="modal-title">Datos inexistentes</h5>
            <button type="button" className="btn-close" onClick={CloseModal}></button>
          </div>
          <div className="modal-body">
            <ul>
              {dnisFalsos.map((dni, index) => (
                <li key={index}>{dni}</li>
              ))}
            </ul>
          </div>
          <div className="modal-footer">
            <p style={{marginRight:'auto', color:'red'}}>¡Campaña o DNI no econtrados!</p>
            {/*<button type="button" className="btn btn-primary" onClick={onRequestClose}>Cerrar</button>*/}
          </div>
        </div>
      </div>
       
    </div>


  );
};

export default DnisFalsosModal;
