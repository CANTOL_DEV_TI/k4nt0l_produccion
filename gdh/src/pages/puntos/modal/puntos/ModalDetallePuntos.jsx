import React, { useState, useEffect } from 'react';
import '../../assets/css/modal-detail.css'
import { ListarPuntosUsuario } from '../../services/PuntosService';


export const ModalDetallePuntos = ({ showModal, onClose, dniUsuario }) => {
    //if (!showModal) return null;

    console.log("pintame showModal:", showModal)
    console.log("pintame DNI:", dniUsuario)

    const [data, setData] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [loading, setLoading] = useState(true)

    /*const listar = async () => {
        //let dniUsuario = localStorage.getItem("dni")
        
        const responseJson = await ListarPuntosUsuario(dniUsuario)
        console.log("muestrame data:",responseJson)
        setData(responseJson)
    }*/

    useEffect(() => {
        //listar()

        const listar = async () => {
            try {
                // Realiza la solicitud para obtener los puntos del usuario
                const responseJson = await ListarPuntosUsuario(dniUsuario);
                // Establece los datos y detiene la carga
                setData(responseJson);
                setLoading(false);
            } catch (error) {
                console.error("Error al obtener los puntos del usuario:", error);
                // Maneja el error aquí, por ejemplo, mostrando un mensaje al usuario
            }
        };
        listar();
    }, [dniUsuario]);



    if (!showModal) return null;



    const handleSearchChange = (e) => {
        setSearchTerm(e.target.value);
    };


    const filteredData = data.filter(item => {
        // Filtrar por término de búsqueda
        return item.tipomovimiento.toLowerCase().includes(searchTerm.toLowerCase()) ||
            item.campaña.toLowerCase().includes(searchTerm.toLowerCase());
    });



    return (
        <div className="modal-detail" style={{ display: 'block' }}>
            <div className="modal-content-detail">
                <span className="close" onClick={onClose}>&times;</span>
                <div className="search-container" style={{ textAlign: 'right', marginRight: '20px', marginBottom: '5px' }}>
                    <input
                        type="text"
                        placeholder="Buscar campaña..."
                        value={searchTerm}
                        onChange={handleSearchChange}
                    />
                </div>
                {loading ? (
                    <div>Cargando...</div>
                ):(

                
                <div className="table-container">
                    {/*<table className="table table-bordered" ble-hover table-sm ta>*/}
                    <table className="table table-bordered table-hover table-sm" style={{textAlign: 'center'}}>

                        <thead className="table-secondary text-center table-sm">
                            <tr>
                                <th>Tipo</th>
                                <th>Campaña</th>
                                <th>Puntos generados</th>
                                <th>Puntos consumidos</th>
                                <th>Puntos actuales</th>
                                <th>Inicio beneficio</th>
                                <th>Fin de beneficio</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredData.map((item, index) => (
                                <tr key={index}>
                                    <td>{item.tipomovimiento}</td>
                                    <td>{item.campaña}</td>
                                    <td>{item.puntosgenerados}</td>
                                    <td>{item.puntosconsumidos}</td>
                                    <td>{item.puntosactuales}</td>
                                    <td>{item.fechabeneficio}</td>
                                    <td>{item.fechabeneficiofin}</td>
                                    <td>{item.estado}</td>
                                </tr>
                            ))}

                        </tbody>
                    </table>
                </div>
                )}
            </div>
        </div>
    );
};

export default ModalDetallePuntos;
