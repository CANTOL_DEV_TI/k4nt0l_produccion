import React, {useState} from 'react'
import '../../assets/css/modal.css'

const ModalEntregar = ({showEntregarModal, onCloseEntregarModal, modalData, handleSubmit}) => {

    if (!showEntregarModal) return null;
   
    const [buttonDisabled, setButtonDisabled] = useState(false);

  const actualizar = async () => {
    const cardex = {
        "id": modalData.id
    }
    
    const responseJson =  await handleSubmit(cardex);
    console.log(responseJson)
    
    setButtonDisabled(true); // Desactivar el botón al hacer clic
    setTimeout(()=>{
        onCloseEntregarModal()
        setButtonDisabled(false)
    },500)
    
  }





  return (
    <div className={`modal ${showEntregarModal ? 'show' : ''}`} role="dialog">
    {/*<div className="modal" tabIndex="-1" role = "dialog" style={{display: 'block'}}>*/}
    
    
        {/*<div className="modal-dialog" role="document">*/}
        <div className="modal-dialog-puntos" role="document">
            <div className="modal-content-puntos">
                <h5>¿Estás seguro de entregar el producto?</h5>
                <div className="modal-buttons">
                    <button className="btn btn-primary btn-sm" onClick={actualizar}  disabled={buttonDisabled}>si</button>
                    <button className="btn btn-warning btn-sm" onClick={onCloseEntregarModal}>no</button>
                </div>

            </div>
        </div>
    </div>
  )
}

export default ModalEntregar;