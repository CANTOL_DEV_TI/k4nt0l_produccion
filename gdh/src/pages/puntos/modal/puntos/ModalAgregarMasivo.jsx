import React, { useState, useEffect } from 'react';
import axios from 'axios';
import DnisFalsosModal  from './ModalAlerta';



const UploadButton = ({ showMasivoModal , onClose, handleSubmit, estado }) => { // Removido handleSubmit y handleChange
    if (!showMasivoModal ) return null;

    const [file, setFile] = useState(null);
    const [estados, setEstados] = useState(null);
    const [showDnisFalsosModal, setShowDnisFalsosModal] = useState(false);
    const [dnisFalsos, setDnisFalsos] = useState([]);

    const handleFileChange = (event) => {
        setFile(event.target.files[0]);
        console.log("pintame file:",file)
    };

    
    const handleUpload = async () => {
        console.log("pintame showMasivoModal xd:",showMasivoModal)
        const formData = new FormData();
        formData.append('file', file);
        console.log("pintame formData:",file)
        
        try {
            const response = await handleSubmit(formData);
            console.log("pintame response excel:", response.status);
            if (response && response.status === 200) {
                console.log("puntos registrado correctamente");
                onClose();
            } else {

                const dnisFalsos = response.data.map(item => item.dniFalse);
                setDnisFalsos(dnisFalsos);
                setShowDnisFalsosModal(true);
                
                console.log("levantas modal:", showDnisFalsosModal)
                console.log("levantas dni falsos:", dnisFalsos)             
            }
            
            //onClose();
        } catch (error) {
            // Manejo de errores
        }
    }

    const CloseModal = () => {
        onClose();
    }


    const handleCloseDnisFalsosModal = () => {
        setShowDnisFalsosModal(false);
    };

    

    return (
       <>
        <div className="modal" tabIndex="-1" style={{ display: showMasivoModal ? 'block' : 'none' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    {/* Eliminado span.close */}
                    <div className="modal-header">
                        <h5 className="modal-title">Agregar Puntos Masivo</h5>
                        <button type="button" className="btn-close" onClick={CloseModal}></button>
                    </div>
                    <div className="modal-body">
                        <h2>Cargar archivo Excel</h2>
                        <input type="file" onChange={handleFileChange} />
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-warning btn-sm" onClick={handleUpload} disabled={file==null}>Subir archivo</button>
                    </div>
                </div>
            </div>
        </div>
            <DnisFalsosModal
                isOpen={showDnisFalsosModal}
                onRequestClose={handleCloseDnisFalsosModal}
                dnisFalsos={dnisFalsos} />
            </>
    );
};

export default UploadButton;
