import React, { useState, useEffect } from 'react';

const ModalEditar = ({ showActualizarModal, modalData, tipoOpe, onClose, handleSubmit, handleChange }) => {
    if (!showActualizarModal) return null;
    console.log("modal data:", modalData)

    /*useEffect(() => {
        if (modalData && modalData.empresas) {
            setEmpresasSeleccionadas(modalData.empresas);
        }
    }, [modalData]);*/


    //if (!showActualizarModal) return null;

    


    const handleFormSubmit = () => {
        const modalDataActualizado = { ...modalData };
        handleSubmit(modalDataActualizado);
    };

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Modal de Edición</h5>
                        <button type="button" className="btn-close" onClick={onClose}></button>
                    </div>
                    <div className="modal-body">
                        <div className="form-group">
                            {/* Contenido del formulario */}
                            <label className="col-sm-3 col-form-label" htmlFor="codigo">Codigo:</label>
                            <input type="text" className="form-control" id="codigo" value={modalData ? modalData.codigoCampania : ''} onChange={(e) => handleChange(e, 'codigoCampania')} disabled />
                            <label className="col-sm-3 col-form-label" htmlFor="tipo">Tipo:</label>
                            <input type="text" className="form-control" id="tipo" value={modalData ? modalData.tipo : ''} onChange={(e) => handleChange(e, 'tipo')} />
                            <label className="col-sm-3 col-form-label" htmlFor="categoria">Categoria:</label>
                            <input type="text" className="form-control" id="categoria" value={modalData ? modalData.categoria : ''} onChange={(e) => handleChange(e, 'categoria')} />
                            <label className="col-sm-3 col-form-label" htmlFor="nombre">Nombre:</label>
                            <input type="text" className="form-control" id="nombre" value={modalData ? modalData.nombre : ''} onChange={(e) => handleChange(e, 'nombre')} />
                            <label className="col-sm-3 col-form-label" htmlFor="puntos">Puntos:</label>
                            <input type="number" className="form-control" id="puntos" value={modalData ? modalData.puntos : ''} onChange={(e) => handleChange(e, 'puntos')} />
                            <label className="col-sm-3 col-form-label" htmlFor="estado">Estado:</label>

                            <select className="form-control" id="estado" value={modalData ? modalData.estado : ''} onChange={(e) => handleChange(e, 'estado')}>
                                <option value="1">ACTIVO</option>
                                <option value="0">INACTIVO</option>
                            </select>
                     
                        </div>
                    
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-warning btn-sm" onClick={handleFormSubmit} disabled={modalData.puntos <=0}>Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ModalEditar;
