import React, { useState, useEffect } from 'react';


const ModalInsertarCampania = ({ showInsertarModal, onClose, handleSubmit, codigo }) => {
    if (!showInsertarModal) return null;

    const [formData, setFormData] = useState({
        codigoCampania: codigo,
        tipo: '',
        categoria: '',
        nombre: '',
        puntos: '',
    });

    useEffect(() => {
        console.log("pintame codigo:",codigo)
        // Refrescar el form data
        setFormData(prevFormData => ({
            ...prevFormData,
            codigoCampania: codigo
        }));
    }, [codigo]);

    //if (!showInsertarModal) return null;

    
    // Función para manejar el cambio en los campos del formulario
    const handleChange = (e, fieldName) => {
        setFormData({ ...formData, [fieldName]: e.target.value });
    };

    const handleSave = () => {
        // Verificar si los campos obligatorios están llenos
        if (!formData.puntos) {
            alert("Por favor, asegúrate de ingresar los puntos antes de guardar.");
            return; // No proceder con el guardado si falta alguno de los campos obligatorios
        }

        const formDataNew = { ...formData }
        console.log("formDataNew", formDataNew)
        handleSubmit(formDataNew);
        
    };


    const handleClose = () => {
        onClose()
    }
    

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Insertar Campaña</h5>
                        <button type="button" className="btn-close" onClick={handleClose}></button> 
                    </div>

                    {/* Modal body */}

                    <div className="modal-body">
                        <div className="form-group">
                            <label className="col-sm-3 col-form-label" htmlFor="codigoCampania">Codigo:</label>
                            <input type="text" className="form-control" id="codigoCampania" value={codigo} onChange={(e) => handleChange(e, 'codigoCampania')} disabled/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="precio">Tipo:</label>
                            <input type="text" className="form-control" id="tipo" onChange={(e) => handleChange(e, 'tipo')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="categoria">Categoria:</label>
                            <input type="text" className="form-control" id="categoria" onChange={(e) => handleChange(e, 'categoria')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="nombre">Nombre:</label>
                            <input type="text" className="form-control" id="nombre" onChange={(e) => handleChange(e, 'nombre')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="puntos">Puntos:</label>
                            <input type="number" className="form-control" id="puntos" onChange={(e) => handleChange(e, 'puntos')} />
                        </div>
                    </div>

                    

                    <div className="modal-footer">
                        {/*<button type="button" className="btn btn-primary btn-sm" onClick={onClose}>Cerrar</button>*/}
                        <button type="button" className="btn btn-warning btn-sm" onClick={handleSave} disabled={formData.puntos <=0 ||  formData.puntos ==''}>Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ModalInsertarCampania;