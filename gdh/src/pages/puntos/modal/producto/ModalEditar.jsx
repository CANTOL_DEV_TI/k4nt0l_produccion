import React, { useState, useEffect } from "react";


const ModalEditar = ({ showModal, modalData, tipoOpe, onClose, handleSubmit, handleChange }) => {
    if (!showModal) return null;
    console.log("modal data:", modalData)

    const [formData, setFormData] = useState({
        //id:modalData.id,
        nombre: '',
        precio: '',
        cantidadpuntos: '',
        estado: '',
        file: null,
        descripcion: ''
    });

    const [formCompleted, setFormCompleted] = useState(false);

    useEffect(() => {
        // Verificar si los campos cantidadpuntos y precio tienen información
        console.log("pintame modal data:", modalData)
        const isFormCompleted = modalData?.cantidadpuntos !== '' && modalData?.precio !== '' && modalData?.precio > 0 && modalData?.cantidadpuntos > 0;
        setFormCompleted(isFormCompleted);
    }, [modalData]);

    //if (!showModal) return null;


    const handleImageChange = (e) => {
        console.log("pintame imagen:",e.target.files[0])
        setFormData({ ...formData, file: e.target.files[0] });
    }

    const handleEdit = async (e) => {
        console.log("muestrame información", e)
        const formDataToSend = new FormData();
        formDataToSend.append('id', e.id)
        formDataToSend.append('nombre', e.nombre)
        formDataToSend.append('precio', e.precio)
        formDataToSend.append('cantidadpuntos', e.cantidadpuntos)
        formDataToSend.append('estado', e.estado)
        formDataToSend.append('imagen', formData.file);
        formDataToSend.append('descripcion', e.descripcion)
        handleSubmit(formDataToSend)
        onClose();
    }


    return (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Modal de Edición</h5>
                        <button type="button" className="btn-close" onClick={onClose}></button>
                    </div>
                    <div className="modal-body">
                        <div className="form-group">
                            <label htmlFor="nombre">Nombre:</label>
                            <input type="text" className="form-control" id="nombre" value={modalData ? modalData.nombre : ''} onChange={(e) => handleChange(e, 'nombre')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="precio">Precio:</label>
                            <input type="number" className="form-control" id="precio" value={modalData ? modalData.precio : ''} onChange={(e) => handleChange(e, 'precio')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="cantidadPuntos">Puntos:</label>
                            <input type="number" className="form-control" id="cantidadpuntos" value={modalData ? modalData.cantidadpuntos : ''} onChange={(e) => handleChange(e, 'cantidadpuntos')} />
                        </div>
                        <label className="col-sm-3 col-form-label" htmlFor="estado">Estado:</label>
                        <select className="form-control" id="estado" value={modalData ? modalData.estado : ''} onChange={(e) => handleChange(e, 'estado')}>
                            <option value="1">ACTIVO</option>
                            <option value="0">INACTIVO</option>
                        </select>
                        <div className="form-group">
                            <label htmlFor="descripcion">Descripcion:</label>
                            <textarea type="text" className="form-control" id="descripcion" value={modalData ? modalData.descripcion: ''} onChange={(e)=> handleChange(e, 'descripcion')}></textarea>

                        </div>

                        <div className="form-group">
                            <label htmlFor="file">Imagen:</label>
                            <input type="file" className="form-control" id="file" onChange={handleImageChange} />
                            {formData.file ? (
                            <img src={URL.createObjectURL(formData.file)} alt="Previsualización de la imagen" style={{ maxWidth: '150px', marginTop: '10px' }} />
                            ):
                            (<img src={`data:image/jpeg;base64,${(modalData.imagen)}`} alt="Imagen del producto" style={{ maxWidth: '150px', marginTop: '10px' }} />)
                            }
                        </div>


                    </div>
                    <div className="modal-footer">
                        {/*<button type="button" className="btn btn-primary btn-sm" onClick={onClose}>Cerrar</button>*/}
                        <button type="button" className="btn btn-warning btn-sm" onClick={() => handleEdit(modalData)} disabled={!formCompleted} >Guardar</button>
                        {/*<button type="button" className="btn btn-warning btn-sm" onClick={() => handleEdit(modalData)}>Guardar</button>*/}

                    </div>
                </div>
            </div>
        </div>
    );
};

export default ModalEditar

