import React, { useEffect, useState } from 'react';

const ModalInsertarProducto = ({ showModal, onClose, handleSubmit }) => {
    if (!showModal) return null;

    const [formData, setFormData] = useState({
        nombre: '',
        precio: '',
        cantidad: '',
        cantidadPuntos: '',
        file: null,
        descripcion: ''
    });

    //Estado para ver si los campos estan completos
    const[formCompleted, setFormCompleted] = useState(false)

    useEffect(()=>{
        // Verificar si todos los campos están completos, incluida la imagen
        const isFormCompleted = Object.values(formData).every(value => value !== '' && value !== null);
        setFormCompleted(isFormCompleted && formData.file !== null && formData.cantidadPuntos>0 && formData.cantidad>0);
    },[formData]);

    //if (!showModal) return null;


    const handleChange = (e, fieldName) => {
        const updatedFormData = {...formData, [fieldName]: e.target.value};
        setFormData(updatedFormData)
    };


    const handleImageChange = (e) => {
        setFormData({ ...formData, file: e.target.files[0] });
    };

    const handleSave = async () => {
        /*handleSubmit(formData);
        onClose();*/
        
        const formDataToSend = new FormData();
        formDataToSend.append('nombre', formData.nombre);
        formDataToSend.append('precio', formData.precio);
        formDataToSend.append('cantidad', formData.cantidad);
        formDataToSend.append('cantidadPuntos', formData.cantidadPuntos);
        formDataToSend.append('imagen', formData.file);
        formDataToSend.append('descripcion', formData.descripcion)

        handleSubmit(formDataToSend);
        onClose();
    };

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Insertar Producto</h5>
                        <button type="button" className="btn-close" onClick={onClose}></button>
                    </div>
                    <div className="modal-body">
                        <div className="form-group">
                            <label htmlFor="nombre">Nombre:</label>
                            <input type="text" className="form-control" id="nombre" onChange={(e) => handleChange(e, 'nombre')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="precio">Precio:</label>
                            <input type="number" className="form-control" id="precio" onChange={(e) => handleChange(e, 'precio')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="cantidad">Cantidad:</label>
                            <input type="number" className="form-control" id="cantidad" onChange={(e) => handleChange(e, 'cantidad')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="cantidadPuntos">Puntos:</label>
                            <input type="number" className="form-control" id="cantidadPuntos" onChange={(e) => handleChange(e, 'cantidadPuntos')} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="descripcion">Descripcion</label>
                            <textarea type="text" className="form-control" id = "descripcion" onChange={(e) => handleChange(e, 'descripcion')}></textarea>
                        </div>
                        <div className="form-group">
                            <label htmlFor="file">Imagen:</label>
                            <input type="file" className="form-control" id="file" onChange={handleImageChange} />
                            {formData.file && (
                                <img src={URL.createObjectURL(formData.file)} alt="Previsualización de la imagen" style={{ maxWidth: '150px', marginTop: '10px' }} />
                            )}
                        </div>
                    </div>
                    <div className="modal-footer">
                        {/*<button type="button" className="btn btn-primary btn-sm" onClick={onClose}>Cerrar</button>*/}
                        <button type="button" className="btn btn-warning btn-sm" onClick={handleSave} disabled={!formCompleted}>Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ModalInsertarProducto;
