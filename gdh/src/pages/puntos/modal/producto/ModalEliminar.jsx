
import React, { useEffect, useState } from "react";

const ModalEliminar = ({ showModal, modalData, tipoOpe, onClose, handleSubmit, handleChange }) => {
    if (!showModal) return null;


    const handleForSubmit = () => {
        const campania = {
            "id": modalData.id
        }
        handleSubmit(campania)
    }

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
            {/*<div className="modal-dialog" role="document">*/}
            <div className="modal-dialog-puntos" role="document">
                <div className="modal-content-puntos">
                    <h5>¿Estás seguro de inhabilitar el producto?</h5>
                    <div className="modal-buttons">
                        <button className="btn btn-primary btn-sm" onClick={handleForSubmit}>Sí</button>
                        <button className="btn btn-warning btn-sm" onClick={onClose}>No</button>
                    </div>
                </div>
            </div>
        </div>
    );

};


export default ModalEliminar;