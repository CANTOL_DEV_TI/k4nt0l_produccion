import React,{useState, useEffect} from 'react';

const ModalDescontarCantidad = ({ showModal, modalData, onClose, handleSubmit, handleChange }) => {
    if (!showModal) return null;

    const[formCompleted,setFormCompleted] = useState(false);

    useEffect(()=>{
        const isFormCompleted = modalData?.cantidad > 0
        setFormCompleted(isFormCompleted);
    },[modalData])

    //if (!showModal) return null;

    const handleDesc = async (e) =>{
        handleSubmit(e)
        onClose();
    }

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={{ display: 'block' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Descontar Cantidad</h5>
                        <button type="button" className="btn-close" onClick={onClose}></button>
                    </div>
                    <div className="modal-body">
                        <div className="form-group">
                            <label className="col-sm-3 col-form-label" htmlFor="nombre">Nombre:</label>
                            <input type="text" className="form-control" id="nombre" value={modalData ? modalData.nombre : ''} disabled />
                            <label className="col-sm-3 col-form-label" htmlFor="cantidad">Cantidad:</label>
                            <input type="number" className="form-control" id="cantidad" value={modalData ? modalData.cantidad : ''} onChange={(e) => handleChange(e, 'cantidad')} />
                        </div>
                    </div>
                    <div className="modal-footer">
                        {/*<button type="button" className="btn btn-primary btn-sm" onClick={onClose}>Cerrar</button>*/}
                        <button type="button" className="btn btn-warning btn-sm" onClick={()=> handleDesc(modalData)} disabled={!formCompleted}>Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ModalDescontarCantidad;
