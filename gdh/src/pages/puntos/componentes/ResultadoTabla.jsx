import React from "react";
import {GrEdit,GrAmazon,GrCodeSandbox,GrTask,GrImage,GrAdd,GrDescend,GrAddCircle,GrStatusGood,GrDocumentStore} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTablaPuntos = ({
                        eventoDetalle,
                        carcon_empresa,
                        carcon_centro_costo,  
                        carcon_dni,                        
                        carcon_usuario,
                        carcon_puntosactuales,
                        eventoCardex
                        //eventoEliminar                       
                        }) =>
    (
        <tbody>
            <tr>
                {/*<td className="td-cadena" id={carcon_codigo}>{carcon_codigo}</td>*/}
                <td style={{textAlign:"center"}}><button onClick={() => eventoDetalle(true)}><GrStatusGood/></button></td>
                <td className="td-cadena">{carcon_empresa}</td>
                <td className="td-cadena">{carcon_centro_costo}</td>
                <td className="td-cadena" >{carcon_dni}</td>
                <td className="td-cadena" >{carcon_usuario}</td>  
                <td className="td-cadena" >{carcon_puntosactuales}</td>
                <td style={{textAlign:"center"}}><button onClick={() => eventoCardex(true)}><GrDocumentStore/></button></td>
                       
                {/*<td style={{textAlign:"center"}}><button onClick={() => eventoEditar(true)}><GrEdit/></button></td> */}
                {/*<td style={{textAlign:"center"}}><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>*/}
            </tr>
        </tbody>
    )


const ResultadoTablaCampania = ({ carcon_codigo,
    carcon_tipo,
    carcon_categoria,
    carcon_nombre,
    carcon_puntos,
    carcon_estado,
    eventoEditar,
    eventoEliminar
}) =>
(
    <tbody>
        <tr>
            {/*<td className="td-cadena" id={carcon_codigo}>{carcon_codigo}</td>*/}
            <td className="td-cadena" >{carcon_codigo}</td>
            <td className="td-cadena" >{carcon_tipo}</td>
            <td className="td-cadena" >{carcon_categoria}</td>
            <td className="td-cadena" >{carcon_nombre}</td>
            <td className="td-cadena" >{carcon_puntos}</td>
            <td className="td-cadena" >{carcon_estado}</td>
            <td style={{ textAlign: "center" }}><button onClick={() => eventoEditar(true)}><GrEdit /></button></td>
            <td style={{ textAlign: "center" }}><button onClick={() => eventoEliminar(true)}><AiFillDelete /></button></td>
        </tr>
    </tbody>
)

const ResultadoTablaProducto = ({ carcon_nombre,
    carcon_precio,
    carcon_puntos,
    carcon_fecha,
    carcon_cantidad,
    carcon_estado,
    eventoEditar,
    eventoAgregar,
    eventoDescontar,
    //eventoMostrar,
    eventoEliminar
}) =>
(
    <tbody>
        <tr>
            {/*<td className="td-cadena" id={carcon_codigo}>{carcon_codigo}</td>*/}
            <td className="td-cadena" >{carcon_nombre}</td>
            <td className="td-cadena" >{carcon_precio}</td>
            <td className="td-cadena" >{carcon_puntos}</td>
            <td className="td-cadena" >{carcon_fecha}</td>
            <td className="td-cadena" >{carcon_cantidad}</td>
            <td className="td-cadena" >{carcon_estado}</td>
            <td style={{ textAlign: "center" }}><button onClick={() => eventoEditar(true)}><GrEdit /></button></td>
            <td style={{ textAlign: "center" }}><button onClick={() => eventoAgregar(true)}><GrAdd /></button></td>
            <td style={{ textAlign: "center" }}><button onClick={() => eventoDescontar(true)}><GrDescend /></button></td>
            {/*<td style={{textAlign:"center"}}><button onClick={() => eventoMostrar(true)}><GrImage/></button></td>*/}
            <td style={{ textAlign: "center" }}><button onClick={() => eventoEliminar(true)}><AiFillDelete /></button></td>
        </tr>
    </tbody>
)




    

export  {ResultadoTablaPuntos, ResultadoTablaCampania, ResultadoTablaProducto} ;