import React, { useState } from "react";
import { BotonExcel, BotonNuevo, BotonBuscar } from "../../../components/Botones.jsx";
import Title from   "../../../components/Titulo.jsx";

class Busqueda extends React.Component {

    constructor(props) {
        super(props);

        this.state = { txtInput: '' }
    }

    handleChange = (e) => {
        this.setState({ txtInput: e.target.value })
    }

    //escucha el teclado un filtra replicar a los demas modulos - mñn
    handleKeyUp = (e) => {
        this.props.handleBusqueda(this.state.txtInput)
    }


    render() {

        const { handleBusqueda, handleModal } = this.props

        return (
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Lista de productos</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    <input
                                        placeholder="Ingrese producto"
                                        onChange={this.handleChange}
                                        onKeyUp={this.handleKeyUp}
                                        value={this.state.txtInput}
                                    />

                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda(this.state.txtInput)} />
                                                                                                        
                                    <BotonNuevo textoBoton={"Crear Nuevo"} sw_habilitado={true} eventoClick={() => handleModal(true)} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Busqueda;