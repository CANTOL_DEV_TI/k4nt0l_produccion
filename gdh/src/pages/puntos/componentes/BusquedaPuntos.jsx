import React, { useState } from "react";
import { BotonExcel, BotonNuevo, BotonBuscar, BotonExcelNuevo } from "../../../components/Botones.jsx";
import Title from   "../../../components/Titulo.jsx";




import {
    BsFileEarmarkExcelFill,
} from "react-icons/bs";

class Busqueda extends React.Component {

    constructor(props) {
        super(props);

        this.state = { txtInput: '' }
    }

    
    handleChange = (e) => {
        this.setState({ txtInput: e.target.value })
    }

    handleKeyUp = (e) => {
        this.props.handleBusqueda(this.state.txtInput)
    }



    render() {

        const format_excel = [{
            cod_articulo: "PT0101030007",
            cantidad: "0",
        }];
    

        const { handleBusqueda, handleModal, handelModalMasivo } = this.props

        return (
            <div className="col-lg-12">

                <div className="card">
                    {/* INICIO BARRA DE NAVEGACION */}
                    <div className="card-header border border-dashed border-end-0 border-start-0">
                        <div className="row align-items-center gy-3">
                            <div className="col-sm">
                                <Title>Lista de puntos</Title>
                            </div>
                            <div className="col-sm-auto">
                                <div className="d-flex flex-wrap gap-1">
                                    <input
                                        placeholder="Ingrese dni"
                                        onChange={this.handleChange}
                                        onKeyUp={this.handleKeyUp}
                                        value={this.state.txtInput}
                                    />

                                    <BotonBuscar textoBoton={"Buscar"} sw_habilitado={true} eventoClick={() => handleBusqueda(this.state.txtInput)} />
                                                                                                        
                                    <BotonNuevo textoBoton={"Agregar puntos"} sw_habilitado={true} eventoClick={() => handleModal(true)} />

                                    <BotonExcelNuevo textoBoton={"masivo puntos"} sw_habilitado={true} eventoClick={() => handelModalMasivo(true)} />

                                    
                                    {/*<BotonExcel textoBoton={"Formato"} sw_habilitado={format_excel.length > 0 ? true : false} listDatos={format_excel} nombreArchivo={"puntos"} />*/}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Busqueda;