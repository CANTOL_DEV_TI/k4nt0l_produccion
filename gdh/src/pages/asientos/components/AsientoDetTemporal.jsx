import React from "react";
import {GrEdit,GrAmazon,GrCodeSandbox,GrTask} from "react-icons/gr";
import {AiFillDelete} from "react-icons/ai";


const ResultadoTabla = ({modulo_id,                        
                        modulo_codigo,
                        modulo_nombre,                        
                        modulo_padre,
                        modulo_padre_id,
                        eventoEditar,
                        eventoEliminar                       
                        }) =>
    (
        <tbody>
            <tr>
                <td className="td-cadena" id={modulo_id}>{modulo_codigo}</td>                
                <td className="td-cadena" >{modulo_nombre}</td>
                <td className="td-cadena" >{modulo_padre}</td>                
                <td className="td-cadena" >{modulo_padre_id}</td>                
                <td style={{textAlign:"center"}}><button onClick={() => eventoEditar(true)}><GrEdit/></button></td>
                <td style={{textAlign:"center"}}><button onClick={() => eventoEliminar(true)}><AiFillDelete/></button></td>                
            </tr>
        </tbody>
    )

export default ResultadoTabla;