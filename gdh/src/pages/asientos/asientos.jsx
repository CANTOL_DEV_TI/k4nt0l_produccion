import React from "react";

import { migraAsiento } from "../../services/asientoServices";
import Title from "../../components/Titulo";
import { BotonExcel, BotonNuevo, BotonBuscar, BotonConsultar } from "../../components/Botones";
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx"


class MigrarAsientos extends React.Component {
    constructor(props) {
        super(props);
        /*
        this.state = {
            resultados: [],
            isFetch: true,
            dataRegistro: null,
            showModal: false,
            tipoOpe: 'Find',
            
        }
        */
        this.state = { CiaADRYAN: '', TipoPlanilla: '', Periodo: '', Ejercicio: '', Fecha: '', Comentario: '' }

    }

    handleSubmit = async (e) => {
        console.log(this.state)
        const responseJson = await migraAsiento(e)
        console.log(responseJson)
        alert(responseJson)
        /*
                if (this.state.tipoOpe === 'Grabar') {
                    const responseJson = await add_Modulo(e)
                    console.log(responseJson)
                }
        
                this.setState({ showModal: false, tipoOpe: 'Find' })
        */
    }
    handleChange = (e) => {
        if (e.target.name === 'cboCia') {
            this.setState({ CiaADRYAN: e.target.value })
        }

        if (e.target.name === "cboTPla") {
            this.setState({ TipoPlanilla: e.target.value })
        }

        if (e.target.name === 'tPeriodo') {
            this.setState({ Periodo: e.target.value })
        }

        if (e.target.name === 'tEjercicio') {
            this.setState({ Ejercicio: e.target.value })
        }

        if (e.target.name === 'tFecha') {
            this.setState({ Fecha: e.target.value })
        }

        if (e.target.name === 'tComentario') {
            this.setState({ Comentario: e.target.value })
        }

    }

    render() {
        const { isFetch, resultados, showModal, VentanaSeguridad } = this.state

        return (
            <React.Fragment>

                <div className="col-lg-12">

                    <div className="card">
                        {/* INICIO BARRA DE NAVEGACION */}
                        <div className="card-header border border-dashed border-end-0 border-start-0">
                            <div className="row align-items-center gy-3">
                                <div className="col-sm" aria-colspan={6}>
                                    <Title>Migracion de Asientos a SAP</Title>
                                </div>
                            </div>


                            <div className="row align-items-center gy-3">
                                <div className="col-sm-2">
                                    <div className="d-flex flex-wrap gap-1">
                                        Compañia :
                                        <select className="form-select form-select-lg mb-1" name="cboCia" onChange={this.handleChange} >
                                            <option value="00">Seleccione Compañia</option>
                                            <option value="01">Tecnopress</option>
                                            <option value="03">Cantol</option>
                                            <option value="02">Distrimax</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-sm-2">
                                    <div className="d-flex flex-wrap gap-1">
                                        Tipo de Planilla :
                                        <select className="form-select form-select-lg mb-1" name="cboTPla" onChange={this.handleChange}>
                                            <option value="00">Seleccione Tipo</option>
                                            <option value="01">01 - Planilla Mensual</option>
                                            <option value="10">10 - Planilla Liquidacion</option>
                                            <option value="06">06 - Planilla Provision</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-sm-1">
                                    <div className="d-flex flex-wrap gap-1">
                                        Periodo :
                                        <select className="form-control" name="tPeriodo" placeholder="Ingrese Periodo" onChange={this.handleChange}>
                                            <option value={"00"}>Seleccione...</option>
                                            <option value={"01"}>ENERO</option>
                                            <option value={"02"}>FEBRERO</option>
                                            <option value={"03"}>MARZO</option>
                                            <option value={"04"}>ABRIL</option>
                                            <option value={"05"}>MAYO</option>
                                            <option value={"06"}>JUNIO</option>
                                            <option value={"07"}>JULIO</option>
                                            <option value={"08"}>AGOSTO</option>
                                            <option value={"09"}>SETIEMBRE</option>
                                            <option value={"10"}>OCTUBRE</option>
                                            <option value={"11"}>NOVIEMBRE</option>
                                            <option value={"12"}>DICIEMBRE</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-sm-1">
                                    <div className="d-flex flex-wrap gap-1">
                                        Ejercicio :
                                        <input type="number" className="form-control" name="tEjercicio"
                                            placeholder="Ingrese Ejercicio" onChange={this.handleChange}
                                        />
                                    </div>
                                </div>
                                <div className="col-sm-1">
                                    <div className="d-flex flex-wrap gap-1">
                                        Fecha :
                                        <input type="date" className="form-control" name="tFecha"
                                            placeholder="Ingrese Fecha" onChange={this.handleChange}
                                        />
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="d-flex flex-wrap gap-1">
                                        Glosa :
                                        <input className="form-control" name="tComentario"
                                            placeholder="Ingrese Comentario" onChange={this.handleChange}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className="col-sm-auto" aria-colspan={6}>
                                <div className="d-flex flex-wrap gap-1">
                                    <BotonConsultar textoBoton={"Migrar a SAP"} sw_habilitado={true} eventoClick={() => this.handleSubmit(this.state)} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <VentanaBloqueo
                        show={this.state.VentanaSeguridad}
                    />
                </div>

            </React.Fragment >
        )

    }
    async componentDidMount() {
        const ValSeguridad = await Validar("GDHASI")
        this.setState({ "VentanaSeguridad": ValSeguridad })
    }
}

export default MigrarAsientos;