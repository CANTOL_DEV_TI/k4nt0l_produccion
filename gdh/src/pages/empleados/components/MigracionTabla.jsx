export function MigracionTabla({ datos_fila, eventoClick }) {

    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <tbody className="list">
                {datos_fila.length > 0 ?
                    (
                        datos_fila.map((datos, index) => {
                            return <tr key={index + 1}>
                                <td className="td-cadena" id={datos.CardCode}>{datos.CardCode}</td>
                                <td className="td-cadena" >{datos.Resultado}</td>                                
                            </tr>
                        })
                    ) :
                    (
                        <tr>
                            <td className="text-center" colSpan="8">Sin datos</td>
                        </tr>
                    )
                }
            </tbody>

            {/* FIN TABLA */}
        </>
    );
}