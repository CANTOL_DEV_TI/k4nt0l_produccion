import React, { useState, useEffect } from 'react'
import { BsInfoCircle } from "react-icons/bs";

export function ResultadoTabla({ datos_fila, eventoClick }) {

    // RETORNAR INFORMACIÓN
    return (
        <>
            {/* INICIO TABLA */}
            <tbody className="list">
                {datos_fila.length > 0 ?
                    (
                        datos_fila.map((datos, index) => {
                            return <tr key={index + 1}>
                                <td className="td-cadena" id={datos.CiaSAP}>{datos.CardCode}</td>
                                <td className="td-cadena" >{datos.CardName}</td>
                                <td className="td-cadena" >{datos.FederalTaxID}</td>
                                <td className="td-cadena" >{datos.U_MSSL_BAP}</td>
                                <td className="td-cadena" >{datos.U_MSSL_BAM}</td>
                                <td className="td-cadena" >{datos.U_MSSL_BN1}</td>
                                <td className="td-cadena" >{datos.U_MSSL_BN2}</td>
                                <td className="td-cadena" >{datos.Phone1}</td>     
                                <td className="td-cadena" >{datos.BankCode}</td>
                                <td className="td-cadena" >{datos.AccountNo}</td>
                                <td className="td-cadena" >{datos.BICSwiftCode}</td>
                                <td className="td-cadena" >{datos.Branch}</td>
                                <td className="td-cadena" >{datos.AccountName}</td>
                                <td className="td-cadena" >{datos.Country}</td>                                
                            </tr>
                        })
                    ) :
                    (
                        <tr>
                            <td className="text-center" colSpan="14">Sin datos</td>
                        </tr>
                    )
                }
            </tbody>

            {/* FIN TABLA */}
        </>
    );
}

/*
'CardCode' : datos.codigo, 'CardName' : datos.nombre_completo , 'FederalTaxID' : datos.dni ,  'U_MSSL_BAP' : datos.apellido_paterno , 
                                 'U_MSSL_BAM' : datos.apellido_materno , 'U_MSSL_BN1' : datos.nombre_1 , 'U_MSSL_BN2' : datos.nombre_2 , 'Phone1' : datos.telefono
*/