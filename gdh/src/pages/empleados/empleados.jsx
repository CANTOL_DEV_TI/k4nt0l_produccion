
import { add_Empleado, listar_Empleados } from "../../services/empleadosServices";
import React, { useState, useEffect } from 'react'
import { BsFileExcel } from "react-icons/bs";
import { BotonExcel, BotonNuevo, BotonBuscar } from "../../components/Botones";
import { ResultadoTabla } from "./components/ResultadoTabla";
import Title from "../../components/Titulo";
import * as XLSX from 'xlsx';
import { Validar } from "../../services/ValidaSesion.jsx";
import VentanaBloqueo from "../../components/VentanaBloqueo.jsx";
import { MigracionTabla } from "./components/MigracionTabla.jsx";
//import { Await } from "react-router-dom";

export function Empleados() {

    const Seguridad = async () => {
        const ValSeguridad = await Validar("GDHEMP")
        setVentanaSeguridad(ValSeguridad.Validar)
    }
    // VARIABLES
    const format_empleados = [{
        CardCode: "E00XXDNIXX",
        CardName: "XXXNOMBRE COMPLETOXXX",
        FederalTaxID: "XXXDNIXX",
        U_MSSL_BAP: "XXAPELLIDO PATERNOXX",
        U_MSSL_BAM: "XXAPELLIDO MATERNOXX",
        U_MSSL_BN1: "XXNOMBRE 1XX",
        U_MSSL_BN2: "XXNOMBRE 2XX",
        Phone1: "XXTELEFONOXX",
        CiaSAP: "XXCIAXX"
    }];

    const [refrescar, setRefrescar] = useState(false);
    const [VentanaSeguridad, setVentanaSeguridad] = useState(true);

    // CONSUMIR DATOS
    const [totalPlan, setTotalPlan] = useState(0);
    const [listDatos, setListDatos] = useState([]);
    const [ciaADRYAN, setciaADRYAN] = useState("00");
    const [ciaSAP, setciaSAP] = useState("XXXX");

    const [listResultados, setListResultados] = useState([])
    useEffect(() => {
        Seguridad();
        setRefrescar(false);
        //getData();
    }, [refrescar])

    // importar excel desde archivo
    const [typeError, setTypeError] = useState(null);
    const handleFile = (e) => {
        let fileTypes = ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'text/csv'];
        let selectedFile = e.target.files[0];
        if (selectedFile) {
            if (selectedFile && fileTypes.includes(selectedFile.type)) {
                setTypeError(null);
                let reader = new FileReader();
                reader.readAsArrayBuffer(selectedFile);
                reader.onload = (e) => {
                    // leer archivo
                    const workbook = XLSX.read(e.target.result, { type: 'buffer' });
                    const worksheetName = workbook.SheetNames[0];
                    const worksheet = workbook.Sheets[worksheetName];
                    const data = XLSX.utils.sheet_to_json(worksheet);
                    console.log(data)
                    setListDatos(data)
                }
            }
            else {
                setTypeError('Seleccione solo tipos de archivos de Excel.');
            }
        }
        else {
            setTypeError('Por favor seleccione su archivo.');
        }
    }

    const handleAddSAP = async (e) => {
        console.log(listDatos)
        const responseJson = await add_Empleado(listDatos)
        //const responseJson = await add_Empleado("SBO_CANTOL_PRODUCCION", listDatos)
        setListResultados(responseJson)
    }

    const handleFind = async (e) => {
        const responseJson = await listar_Empleados(ciaADRYAN, ciaSAP)
        setListDatos(responseJson)
    }

    const handleChange = async (e) => {
        setciaADRYAN(e.target.value)

        if (e.target.value === "01") {
            setciaSAP("SBO_TECNO_PRODUCCION")
        }

        if (e.target.value === "02") {
            setciaSAP("SBO_DISTRI_PRODUCCION")
        }

        if (e.target.value === "03") {
            setciaSAP("SBO_CANTOL_PRODUCCION")
        }

    }

    // INSERTAR DATOS

    // RETORNAR INFORMACIÓN
    return (
        <div className="col-lg-12">
            <div className="card">
                {/* INICIO BARRA DE NAVEGACION */}
                <div className="card-header border border-dashed border-end-0 border-start-0">
                    <div className="row align-items-center gy-3">
                        <div className="col-sm">
                            <Title>Cargar Empleados Nuevos a SAP</Title>
                        </div>
                        <div className="col-sm">
                            <div className="d-flex justify-content-end gap-1">
                                Compañia 
                                <select className="form-select form-select-sm mb-1" name="cboCia" onChange={handleChange} >
                                    <option value="00">Seleccione Compañia</option>
                                    <option value="01">Tecnopress</option>
                                    <option value="03">Cantol</option>
                                    <option value="02">Distrimax</option>
                                </select>
                            </div>
                        </div>
                        <div className="col-sm">
                            <div className="d-flex justify-content-end gap-1">
                                <BotonExcel textoBoton={"Exportar Excel"} sw_habilitado={listDatos.length > 0 ? false : false} listDatos={listDatos} nombreArchivo={"Plan venta"} />
                                <label htmlFor="file_excel" className="btn btn-success btn-sm" >
                                    <i className="align-bottom me-1"><BsFileExcel /></i> Cargar Excel</label>
                                <input type="file" id="file_excel" name="file_excel" accept=".xlsx, .xls, .csv" style={{ visibility: 'hidden', maxWidth: '1px' }} onChange={handleFile} />
                                <BotonExcel textoBoton={"Formato"} sw_habilitado={format_empleados.length > 0 ? false : false} listDatos={format_empleados} nombreArchivo={"Formato_carga"} />
                                <BotonBuscar textoBoton={"Comparar"} sw_habilitado={true} eventoClick={handleFind} />
                                <BotonNuevo textoBoton={"Enviar a SAP"} sw_habilitado={true} eventoClick={handleAddSAP} />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                            <tr>
                                <th className="align-middle">Codigo</th>
                                <th className="align-middle">Nombre Completo</th>
                                <th className="align-middle">DNI</th>
                                <th className="align-middle">Apellido Paterno</th>
                                <th className="align-middle">Apellido Materno</th>
                                <th className="align-middle">Nombre 1</th>
                                <th className="align-middle">Nombre 2</th>
                                <th className="align-middle">Telefono</th>
                                <th className="align-middle">C.Banco</th>
                                <th className="align-middle">Nro Cuenta</th>
                                <th className="align-middle">Codigo B.</th>
                                <th className="align-middle">Moneda</th>
                                <th className="align-middle">Nombre Cta.</th>
                                <th className="align-middle">Pais</th>
                            </tr>
                        </thead>

                        { /* INICIO TABLA */}
                        <ResultadoTabla datos_fila={listDatos} />

                        {/* FIN TABLA */}
                    </table>

                </div>
                <div className="table-responsive" style={{ height: '600px', display: "-ms-flexbox" }}>
                    <table className="table table-hover table-sm table-bordered">
                        <thead className="table-secondary text-center table-sm" style={{ position: "sticky", top: 0 }}>
                            <tr>
                                <th className="align-middle">Codigo</th>
                                <th className="align-middle">Resultado</th>
                            </tr>
                        </thead>
                        <MigracionTabla datos_fila={listResultados} />
                    </table>
                </div>

            </div>
            <VentanaBloqueo show={VentanaSeguridad} />
        </div>
    );
}