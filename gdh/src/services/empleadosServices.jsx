import {Url} from '../constants/global'

// const meServidorBackend = 'http://192.168.5.21:8080'
const meServidorBackend = Url

const cabecera = {'Content-type': 'application/json; charset=UTF-8'}

export async function add_Empleado(meJson){   
    const requestOptions = {
        method: 'POST',        
        headers: cabecera,
        body: JSON.stringify(meJson)
    };

    const response = await fetch(`${meServidorBackend}/gdh/empleados/`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}

export async function listar_Empleados(pciaAdrian,pciaSAP){
    const requestOptions = {
        method : 'GET',
        headers : cabecera
    };

    const response = await fetch(`${meServidorBackend}/gdh/empleados/listadoADRYANSAP?pSap=${pciaSAP}&pAdryan=${pciaAdrian}`, requestOptions);
    const responseJson = await response.json();
    return responseJson
}