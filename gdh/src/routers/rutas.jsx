import { Routes, Route, } from "react-router-dom";

import { Empleados } from "../pages/empleados/empleados.jsx";
import MigrarAsientos from "../pages/asientos/asientos.jsx";
import LoginV from "../pages/login/login.jsx";
import ActuaPassword from "../pages/login/cambiocontraseña.jsx";
import React from "react";
import ProtectedRoute from "../components/utils/ProtectedRoute.jsx";
/*
import Puntos from "../pages/puntos/Puntos.jsx"
import Campanias from "../pages/puntos/Campanias.jsx"
import Productos from "../pages/puntos/Productos.jsx"
import Terminos from "../pages/puntos/Terminos.jsx"
*/
export function MiRutas() {
    return (
        <Routes>
            <Route path="/" element={<LoginV />} />
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/empleados" element={<Empleados />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/asientos" element={<MigrarAsientos />} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/"} />} >
                <Route path="/cambiocontrasena" element={<ActuaPassword />} />
            </Route>
            {/*
            <Route element={<ProtectedRoute Redirige={"/login"} />} >
                <Route path="/puntos" element={<Puntos/>} />
            </Route>
            <Route element={<ProtectedRoute Redirige={"/login"} />} >
                <Route path="/campanias" element={<Campanias/>} />
            </Route>

            <Route element={<ProtectedRoute Redirige={"/login"} />} >
                <Route path="/productos" element={<Productos/>} />
            </Route>

            <Route element={<ProtectedRoute Redirige={"/login"} />} >
                <Route path="/terminos" element={<Terminos/>} />
            </Route>
*/ }
        </Routes>
    );
}